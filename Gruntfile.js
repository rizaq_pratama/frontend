module.exports = function exports(grunt) {
  // for convenient helper functions
  // eslint-disable-next-line global-require
  const _ = require('lodash');

  // md5 hash function used for cache-busting
  // eslint-disable-next-line global-require
  const md5 = require('crypto-js/md5');

  // used to serve static files
  // eslint-disable-next-line global-require
  const serveStatic = require('serve-static');

  //manual rev value
  const revValue = "002";

  // display execution time of grunt tasks
  // eslint-disable-next-line global-require
  require('time-grunt')(grunt);

  // lazy load grunt tasks
  // eslint-disable-next-line global-require
  require('jit-grunt')(grunt, {
    useminPrepare: 'grunt-usemin',
    ngtemplates: 'grunt-angular-templates',
    replace: 'grunt-text-replace',
  });

  grunt.initConfig({

    clean: {
      server: ['.tmp'],
      dist: ['.tmp', 'dist'],
      bootstrap: ['dist/index.html'], // see the bootstrap task
      reports: ['reports'],
    },

    connect: {
      options: {
        port: 7000,
        protocol: 'http',
        // change this to '0.0.0.0' to access the server from outside
        // we run the server on localhost but use nginx to create a proxy pass to *.ninjavan.co
        // so that we can get around auth, which stores access_token cookies on *.ninjavan domain
        hostname: 'localhost',
        open: 'http://operatorv2-local.ninjavan.co:' + '<%= connect.options.port %>',
        livereload: '<%= watch.options.livereload.port %>',
      },
      server: {
        options: {
          middleware: function middleware(/* connect */) {
            return [
              // if the file exists in both folders, the first version found takes precedence,
              // i.e. the order is files in `.tmp` are served before files in `app`
              serveStatic('.tmp'), // serves the compiled js and css files from the .tmp folder
              serveStatic('app'),  // serves all the other app files
              serveStatic('app/lib/node_modules/@nv/angular-commons/dist'),
            ];
          },
        },
      },
      dist: {
        options: {
          middleware: function middleware(/* connect */) {
            return [
              serveStatic('dist'),
            ];
          },
        },
      },
    },

    watch: {
      options: {
        livereload: {
          port: 35729,
          hostname: 'localhost',
        },
      },
      html: {
        files: ['app/**/*.html', '!app/lib/node_modules/**'],
      },
      js: {
        files: ['app/**/*.js', '!app/lib/node_modules/**', '!app/lib/ninja-commons/tests/**'],
      },
      compass: {
        files: ['app/**/*.scss', '!app/lib/node_modules/**'],
        tasks: ['compass:server', 'postcss:server'],
      },
      css: {
        files: ['.tmp/styles/**/*.css'],
      },
      json: {
        files: ['app/resources/**/*.json'],
      },
      npm: {
        files: ['package.json'],
        tasks: ['shell:npm-install'],
      },
      dist: {
        files: ['dist/**'],
      },
    },

    babel: {
      options: {
        sourceMap: true,
        presets: ['babel-preset-env'],
        plugins: ['transform-object-rest-spread'],
      },
      dist: {
        options: {
          sourceMap: false,
        },
        files: [
          {
            expand: true,
            cwd: 'app',
            src: ['**/*.js', '!lib/node_modules/**', '!lib/ninja-commons/tests/**'],
            dest: '.tmp/babel',
          },
        ],
      },
    },

    compass: {
      options: {
        cacheDir: '.tmp/.sass-cache',
        relativeAssets: false,
        assetCacheBuster: false,
        trace: true,
        raw: 'Sass::Script::Number.precision = 10\n',
      },
      server: {
        options: {
          environment: 'development',
          sourcemap: true,
          sassDir: 'app/styles',
          cssDir: '.tmp/styles',
        },
      },
      dist: {
        options: {
          environment: 'production',
          sourcemap: false,
          sassDir: 'app/styles',
          cssDir: '.tmp/styles',
          imagesDir: 'app/assets/images',
        },
      },
    },

    postcss: {
      server: {
        src: ['.tmp/styles/app.css', '!.tmp/styles/vendor.css'],
        options: {
          map: true,
          processors: [
            // add fallbacks for rem units
            // eslint-disable-next-line global-require
            require('pixrem')({ rootValue: 10 }),
            // add vendor prefixes
            // eslint-disable-next-line global-require
            require('autoprefixer')({ browsers: 'last 2 versions' }),
          ],
        },
      },
      dist: {
        src: '.tmp/**/*.css',
        options: {
          map: false,
          processors: [
            // add fallbacks for rem units
            // eslint-disable-next-line global-require
            require('pixrem')({ rootValue: 10 }),
            // add vendor prefixes
            // eslint-disable-next-line global-require
            require('autoprefixer')({ browsers: 'last 2 versions' }),
          ],
        },
      },
    },

    copy: {
      dist: {
        files: [
          { // copies html files (index.html) from the app folder to dist
            expand: true,
            cwd: 'app',
            src: ['*.html'],
            dest: 'dist',
          },
          { // copies image assets from the app folder to dist
            expand: true,
            cwd: 'app/assets/images',
            src: ['*'],
            dest: 'dist/assets/images',
          },
          { // copies assets from the lib/ninja-commons folder to dist
            expand: true,
            cwd: 'app/lib/ninja-commons/assets',
            src: ['**/*'],
            dest: 'dist/assets',
          },
          { // copies compiled css styles from the .tmp folder to dist
            expand: true,
            cwd: '.tmp/styles',
            src: ['*'],
            dest: 'dist/styles',
          },
          { // copies font-awesome fonts from the lib/node_modules folder to dist
            expand: true,
            cwd: 'app/lib/node_modules/font-awesome/fonts',
            src: ['*'],
            dest: 'dist/assets/fonts',
          },
          { // copies language localization files to dist
            expand: true,
            cwd: 'app/resources/languages',
            src: ['*.json'],
            dest: 'dist/resources/languages',
          },
          { // copies json resource files to dist
            expand: true,
            cwd: 'app/resources/json',
            src: ['*.json'],
            dest: 'dist/resources/json',
          },
          { // copies sound files to dist
            expand: true,
            cwd: 'app/resources/sounds',
            src: ['*.mp3', '*.ogg', '*.wav'],
            dest: 'dist/resources/sounds',
          },
          { // copies ace ui mode files to dist
            expand: true,
            cwd: 'app/lib/node_modules/ace-builds/src-min-noconflict',
            src: ['mode-javascript.js', 'theme-twilight.js'],
            dest: 'dist/assets/ace-ui',
          },
          { // copies mapbox images to dist
            expand: true,
            cwd: 'app/lib/node_modules/mapbox.js/dist/images',
            src: ['*'],
            dest: 'dist/assets/images/mapbox',
          },
          { // copies leaflet-draw images to dist
            expand: true,
            cwd: 'app/lib/node_modules/leaflet-draw/dist/images',
            src: ['*'],
            dest: 'dist/assets/images/leaflet-draw',
          },
        ],
      },
      'dist-nv': {
        files: [
          { // copies image assets from the @nv/angular-commons folder to dist
            expand: true,
            cwd: 'app/lib/node_modules/@nv/angular-commons/dist/assets/images',
            src: ['*'],
            dest: 'dist/assets/images',
          }
        ]
      },
      bootstrap: { // see the bootstrap task
        files: [
          {
            expand: true,
            cwd: 'dist',
            src: ['index.html'],
            dest: 'dist/bootstrap',
          },
        ],
      },
      'reverse-bootstrap': {
        files: [
          {
            expand: true,
            cwd: 'dist/bootstrap',
            src: ['index.html'],
            dest: 'dist',
          },
        ],
      },
    },

    useminPrepare: {
      'index.html': 'app/index.html',
      options: {
        flow: {
          'index.html': {
            steps: {
              // generate these grunt tasks based on the blocks specified in the html
              js: ['concat', 'uglify'],
              css: ['concat', 'cssmin'],
              concat: ['concat'],
            },
            post: {},
          },
        },
      },
    },

    usemin: {
      html: ['dist/*.html'],
      css: ['dist/styles/*.css'],
      js: ['dist/scripts/*.js', 'dist/*.js'],
      options: {
        blockReplacements: {
          concat: function concat(block) {
            return `<script src="${block.dest}"></script>`;
          },
        },
        assetsDirs: [ // see the 'copy' task for the paths
          'dist',
          'dist/assets/images',
          'dist/assets/ico/sg', // uses sg folder as the base reference folder
          'dist/assets/ico/dev',
          'dist/assets/markers',
          'dist/assets/fonts',
          'dist/resources/languages',
          'dist/resources/json',
          'dist/resources/sounds',
          'dist/assets/images/mapbox',
          'dist/assets/images/leaflet-draw',
        ],
        patterns: {
          // each pattern array contains:
          // 1. regex representing the file reference to replace
          // 2. logging string
          // 3. filter-in: receives the matched group, and returns the file path
          //    of the asset (optional)
          // 4. filter-out: receives the revved path of the asset, and returns the url path
          //    from which it can be reached on the web server (optional)
          js: [
            [
              /assets\/images\/([^'"]*\.(png|jpg|jpeg|gif|webp|svg))/g,
              'Replacing references to app images',
            ],
            [
              /(lib\/ninja\-commons\/assets\/images\/[^'"]*\.(png|jpg|jpeg|gif|webp|svg))/g,
              'Replacing references to lib/ninja-commons images',
              _.partial(trim, 'lib/ninja-commons/assets/images/'),
              _.partial(prefix, 'assets/images/'),
            ],
            [
              /(lib\/ninja\-commons\/assets\/markers\/)/g,
              'Replacing references to lib/ninja-commons/assets/markers folders',
              _.partial(trim, 'lib/ninja-commons/assets/markers/'),
              _.partial(prefix, 'assets/markers/'),
            ],
            [
              /(lib\/ninja\-commons\/assets\/ico\/)/g,
              'Replacing references to lib/ninja-commons/assets/ico folders',
              _.partial(trim, 'lib/ninja-commons/assets/ico/'),
              _.partial(prefix, 'assets/ico/'),
            ],
            [
              /(favicon[\w-]*\.(ico|png))/g,
              'Replacing references to lib/ninja-commons/assets/ico icons',
            ],
            [
              /resources\/languages\/([^'"]*\.json)/g,
              'Replacing references to localization files',
            ],
            [
              /resources\/json\/([^'"]*\.json)/g,
              'Replacing references to json files',
            ],
            [
              /resources\/sounds\/([^'"]*\.(wav|ogg|mp3))/g,
              'Replacing references to sounds files',
            ],
            [
              /(lib\/node_modules\/mapbox\.js\/dist\/images\/[^'"]*\.(png|jpg|jpeg|gif|webp|svg))/g,
              'Replacing references to mapbox images',
              _.partial(trim, 'lib/node_modules/mapbox.js/dist/images/'),
              _.partial(prefix, 'assets/images/mapbox/'),
            ],
          ],
          css: [
            [
              /(lib\/node_modules\/font\-awesome\/fonts\/[^'"]*?\.(otf|eot|svg|ttf|woff2|woff))/g,
              'Replacing references to font-awesome fonts',
              _.partial(trim, 'lib/node_modules/font-awesome/fonts/'),
              _.partial(prefix, 'assets/fonts/'),
            ],
            [
              /(lib\/ninja\-commons\/assets\/images\/[^'"]*?\.(png|jpg|jpeg|gif|webp|svg))/g,
              'Replacing references to ninja-commons images',
              _.partial(trim, 'lib/ninja-commons/assets/images/'),
              _.partial(prefix, 'assets/images/'),
            ],
            [
              /(lib\/node_modules\/mapbox\.js\/dist\/images\/[^'"]*?\.(png|jpg|jpeg|gif|webp|svg))/g,
              'Replacing references to mapbox images',
              _.partial(trim, 'lib/node_modules/mapbox.js/dist/images/'),
              _.partial(prefix, 'assets/images/mapbox/'),
            ],
            [
              /(lib\/node_modules\/leaflet-draw\/dist\/images\/[^'"]*?\.(png|jpg|jpeg|gif|webp|svg))/g,
              'Replacing references to leaflet draw images',
              _.partial(trim, 'lib/node_modules/leaflet-draw/dist/images/'),
              _.partial(prefix, 'assets/images/leaflet-draw/'),
            ],
          ],
        },
      },
    },

    concat: {
      options: {
        // there's no easy way to set the separator differently between JS and CSS
        process: function process(src, filepath) {
          if (filepath.split(/\./).pop() === 'js') {
            return `${src}${grunt.util.linefeed};${grunt.util.linefeed}`;
          }
          return src;
        },
      },
    },

    ngtemplates: {
      dist: {
        options: {
          module: 'nvOperatorApp.templates',
          htmlmin: '<%= htmlmin.dist.options %>',
          usemin: 'scripts/templates.js',
        },
        cwd: 'app',
        src: [
          'views/**/*.html',
          'directives/**/*.html',
          'lib/ninja-commons/directives/**/*.html',
          'lib/ninja-commons/services/**/*.html',
          'lib/ninja-commons/templates/**/*.html',
        ],
        dest: '.tmp/templateCache.js',
      },
    },

    filerev: {
      options: {
        length: 16,
      },
      dist: {
        src: [
          'dist/scripts/**/*.js',
          'dist/styles/*.css',
          'dist/assets/images/**/*.{png,jpg,jpeg,gif,webp,svg}',
          'dist/assets/fonts/*',
          'dist/resources/languages/*.json',
          'dist/resources/json/*.json',
        ],
      },
      'dist-config': {
        src: [
          'dist/*.js',
        ],
        options: {
          process: function process(basename, revision, extension) {
            // we generate a custom revision string every time we make a build because
            // Ansible replaces the config variables during deployment, and those variables
            // could have changed even though the config file remains unchanged
            const hashWithTimestamp = revision + Date.now();
            const hash = md5(hashWithTimestamp).toString().substring(0, 16);
            return `${basename}.${hash}.${extension}`;
          },
        },
      },
      'dist-ico': {
        src: [
          'dist/assets/ico/**/*.{ico,png}',
        ],
        options: {
          process: function process(basename, revision, extension) {
            // the favicon assets don't change often so we generate a simple num-string as the
            // 'hash'; and this num-string needs to be manually incremented when the favicon
            // assets change, i.e. favicon.001.ico -> favicon.002.ico
            return `${basename}.${revValue}.${extension}`;
          },
        },
      },
      'dist-sound': {
        src: [
          'dist/resources/sounds/**/*.{wav,mp3,ogg}',
        ],
        options: {
          process: function process(basename, revision, extension) {
            // change the rev hash manually
            return `${basename}.${revValue}.${extension}`;
          },
        },
      },
    },

    htmlmin: {
      dist: {
        options: {
          // see list of options at https://github.com/kangax/html-minifier#options-quick-reference
          removeComments: true,
          collapseWhitespace: true,
          conservativeCollapse: true,
          removeCommentsFromCDATA: true,
        },
        files: [
          {
            expand: true,
            cwd: 'dist',
            src: ['*.html'],
            dest: 'dist',
          },
        ],
      },
    },

    replace: {
      'dist-config': { // config variable replacements
        src: ['dist/config.*.js'],
        overwrite: true,
        replacements: [
          config(/(env:["'])(.*?)(["'])/, '$env'),
          config(/(gaTrackingId:["'])(.*?)(["'])/, '$gaTrackingId'),
          config(/(authlogin:["'])(.*?)(["'])/, '$servers.authlogin'),
          config(/(dpms:["'])(.*?)(["'])/, '$servers.dpms'),
          config(/(engine:["'])(.*?)(["'])/, '$servers.engine'),
          config(/(core:["'])(.*?)(["'])/, '$servers.core'),
          config(/(["']core-kafka["']:["'])(.*?)(["'])/, '$servers.core-kafka'),
          config(/(route:["'])(.*?)(["'])/, '$servers.route'),
          config(/(driver:["'])(.*?)(["'])/, '$servers.driver'),
          config(/(["']script-engine["']:["'])(.*?)(["'])/, '$servers.script-engine'),
          config(/(shipper:["'])(.*?)(["'])/, '$servers.shipper'),
          config(/(["']shipper-search["']:["'])(.*?)(["'])/, '$servers.shipper-search'),
          config(/(image:["'])(.*?)(["'])/, '$servers.image'),
          config(/(ticketing:["'])(.*?)(["'])/, '$servers.ticketing'),
          config(/(addressing:["'])(.*?)(["'])/, '$servers.addressing'),
          config(/(hub:["'])(.*?)(["'])/, '$servers.hub'),
          config(/(vrp:["'])(.*?)(["'])/, '$servers.vrp'),
          config(/(["']shipper-dashboard["']:["'])(.*?)(["'])/, '$servers.shipper-dashboard'),
          config(/(auth:["'])(.*?)(["'])/, '$servers.auth'),
          config(/(reservation:["'])(.*?)(["'])/, '$servers.reservation'),
          config(/(lighthouse:["'])(.*?)(["'])/, '$servers.lighthouse'),
          config(/(overwatch:["'])(.*?)(["'])/, '$servers.overwatch'),
          config(/(events:["'])(.*?)(["'])/, '$servers.events'),
          config(/(checkout:["'])(.*?)(["'])/, '$servers.checkout'),
          config(/(["']url-shortener["']:["'])(.*?)(["'])/, '$servers.url-shortener'),
          config(/(["']order-create["']:["'])(.*?)(["'])/, '$servers.order-create'),
          config(/(["']order-search["']:["'])(.*?)(["'])/, '$servers.order-search'),
          config(/(vault:["'])(.*?)(["'])/, '$servers.vault'),
          config(/(shipperpanel:\s?["'])(.*?)(["'])/, '$servers.shipperpanel'),
          config(/(dash:["'])(.*?)(["'])/, '$servers.dash'),
          config(/(dp:\s?["'])(.*?)(["'])/, '$servers.new-dp'),
          config(/(iot:\s?["'])(.*?)(["'])/, '$servers.iot'),
          config(/(reports:\s?["'])(.*?)(["'])/, '$servers.reports'),
          config(/(wallet:\s?["'])(.*?)(["'])/, '$servers.wallet'),
          config(/(["']operator-react["']:["'])(.*?)(["'])/, '$servers.operator-react'),
          config(/(dwh:\s?["'])(.*?)(["'])/, '$servers.dwh'),
          config(/(["']order-service["']:\s?["'])(.*?)(["'])/, '$servers.order-service'),
        ],
      },
      'dev-dev': {
        src: ['app/config.js'],
        overwrite: true,
        replacements: [
          config(/(env:\s?["'])(.*?)(["'])/, 'local'),
          config(/(gaTrackingId:\s?["'])(.*?)(["'])/, 'UA-106082721-3'),
          config(/(authlogin:\s?["'])(.*?)(["'])/, 'https://auth-dev.ninjavan.co'),
          config(/(dpms:\s?["'])(.*?)(["'])/, 'https://api-dev.ninjavan.co/$country/dpms'),
          config(/(engine:\s?["'])(.*?)(["'])/, 'https://api-dev.ninjavan.co/$country/engine'),
          config(/(core:\s?["'])(.*?)(["'])/, 'https://api-dev.ninjavan.co/$country/core'),
          config(/(["']core-kafka["']:\s?["'])(.*?)(["'])/, 'https://api-dev.ninjavan.co/$country/core-kafka'),
          config(/(route:\s?["'])(.*?)(["'])/, 'https://api-dev.ninjavan.co/$country/route'),
          config(/(driver:\s?["'])(.*?)(["'])/, 'https://api-dev.ninjavan.co/$country/driver'),
          config(/(["']script-engine["']:\s?["'])(.*?)(["'])/, 'https://api-dev.ninjavan.co/$country/script-engine'),
          config(/(shipper:\s?["'])(.*?)(["'])/, 'https://api-dev.ninjavan.co/$country/shipper'),
          config(/(["']shipper-search["']:\s?["'])(.*?)(["'])/, 'https://api-dev.ninjavan.co/$country/shipper-search'),
          config(/(image:\s?["'])(.*?)(["'])/, 'https://api-dev.ninjavan.co/$country/image'),
          config(/(ticketing:\s?["'])(.*?)(["'])/, 'https://api-dev.ninjavan.co/$country/ticketing'),
          config(/(addressing:\s?["'])(.*?)(["'])/, 'https://api-dev.ninjavan.co/$country/addressing'),
          config(/(hub:\s?["'])(.*?)(["'])/, 'https://api-dev.ninjavan.co/$country/hub'),
          config(/(vrp:\s?["'])(.*?)(["'])/, 'https://api-dev.ninjavan.co/$country/vrp'),
          config(/(["']shipper-dashboard["']:\s?["'])(.*?)(["'])/, 'https://api-dev.ninjavan.co/$country/shipper-dashboard'),
          config(/(auth:\s?["'])(.*?)(["'])/, 'https://api-dev.ninjavan.co/$country/auth'),
          config(/(reservation:\s?["'])(.*?)(["'])/, 'https://api-dev.ninjavan.co/$country/reservation'),
          config(/(lighthouse:\s?["'])(.*?)(["'])/, 'https://api-dev.ninjavan.co/$country/lighthouse'),
          config(/(overwatch:\s?["'])(.*?)(["'])/, 'https://api-dev.ninjavan.co/$country/overwatch'),
          config(/(events:\s?["'])(.*?)(["'])/, 'https://api-dev.ninjavan.co/$country/events'),
          config(/(checkout:\s?["'])(.*?)(["'])/, 'https://api-dev.ninjavan.co/$country/checkout'),
          config(/(["']url-shortener["']:\s?["'])(.*?)(["'])/, 'http://dev.nnj.vn'),
          config(/(["']order-create["']:\s?["'])(.*?)(["'])/, 'https://api-dev.ninjavan.co/$country/order-create'),
          config(/(["']order-search["']:\s?["'])(.*?)(["'])/, 'https://api-dev.ninjavan.co/$country/order-search'),
          config(/(vault:\s?["'])(.*?)(["'])/, 'https://api-dev.ninjavan.co/$country/vault'),
          config(/(shipperpanel:\s?["'])(.*?)(["'])/, 'https://api-dev.ninjavan.co/$country/shipperpanel'),
          config(/(dash:\s?["'])(.*?)(["'])/, 'https://api-dev.ninjavan.co/$country/dash'),
          config(/(dp:\s?["'])(.*?)(["'])/, 'https://api-dev.ninjavan.co/$country/dev'),
          config(/(iot:\s?["'])(.*?)(["'])/, 'https://iot-qa.ninjavan.co/'),
          config(/(reports:\s?["'])(.*?)(["'])/, 'https://api-dev.ninjavan.co/$country/reports'),
          config(/(wallet:\s?["'])(.*?)(["'])/, 'https://api-dev.ninjavan.co/$country/wallet'),
          config(/(["']operator-react["']:["'])(.*?)(["'])/, 'https://operator-react-dev.ninjavan.co'),
          config(/(dwh:\s?["'])(.*?)(["'])/, 'https://api-dev.ninjavan.co/$country/dwh'),
          config(/(["']order-service["']:\s?["'])(.*?)(["'])/, 'https://api-dev.ninjavan.co/$country/order'),
        ],
      },
      'dev-nginx-dev': {
        src: ['app/config.js'],
        overwrite: true,
        replacements: [
          config(/(env:\s?["'])(.*?)(["'])/, 'local'),
          config(/(gaTrackingId:\s?["'])(.*?)(["'])/, 'UA-106082721-3'),
          config(/(authlogin:\s?["'])(.*?)(["'])/, 'https://auth-dev.ninjavan.co'),
          config(/(dpms:\s?["'])(.*?)(["'])/, 'http://operatorv2-local.ninjavan.co/api-dev/$country/dpms'),
          config(/(engine:\s?["'])(.*?)(["'])/, 'http://operatorv2-local.ninjavan.co/api-dev/$country/engine'),
          config(/(core:\s?["'])(.*?)(["'])/, 'http://operatorv2-local.ninjavan.co/api-dev/$country/core'),
          config(/(["']core-kafka["']:\s?["'])(.*?)(["'])/, 'http://operatorv2-local.ninjavan.co/api-dev/$country/core-kafka'),
          config(/(route:\s?["'])(.*?)(["'])/, 'http://operatorv2-local.ninjavan.co/api-dev/$country/route'),
          config(/(driver:\s?["'])(.*?)(["'])/, 'http://operatorv2-local.ninjavan.co/api-dev/$country/driver'),
          config(/(["']script-engine["']:\s?["'])(.*?)(["'])/, 'http://operatorv2-local.ninjavan.co/api-dev/$country/script-engine'),
          config(/(shipper:\s?["'])(.*?)(["'])/, 'http://operatorv2-local.ninjavan.co/api-dev/$country/shipper'),
          config(/(["']shipper-search["']:\s?["'])(.*?)(["'])/, 'http://operatorv2-local.ninjavan.co/api-dev/$country/shipper-search'),
          config(/(image:\s?["'])(.*?)(["'])/, 'http://operatorv2-local.ninjavan.co/api-dev/$country/image'),
          config(/(ticketing:\s?["'])(.*?)(["'])/, 'http://operatorv2-local.ninjavan.co/api-dev/$country/ticketing'),
          config(/(addressing:\s?["'])(.*?)(["'])/, 'http://operatorv2-local.ninjavan.co/api-dev/$country/addressing'),
          config(/(hub:\s?["'])(.*?)(["'])/, 'http://operatorv2-local.ninjavan.co/api-dev/$country/hub'),
          config(/(vrp:\s?["'])(.*?)(["'])/, 'http://operatorv2-local.ninjavan.co/api-dev/$country/vrp'),
          config(/(["']shipper-dashboard["']:\s?["'])(.*?)(["'])/, 'http://operatorv2-local.ninjavan.co/api-dev/$country/shipper-dashboard'),
          config(/(auth:\s?["'])(.*?)(["'])/, 'http://operatorv2-local.ninjavan.co/api-dev/$country/auth'),
          config(/(reservation:\s?["'])(.*?)(["'])/, 'http://operatorv2-local.ninjavan.co/api-dev/$country/reservation'),
          config(/(lighthouse:\s?["'])(.*?)(["'])/, 'http://operatorv2-local.ninjavan.co/api-dev/$country/lighthouse'),
          config(/(overwatch:\s?["'])(.*?)(["'])/, 'http://operatorv2-local.ninjavan.co/api-dev/$country/overwatch'),
          config(/(events:\s?["'])(.*?)(["'])/, 'http://operatorv2-local.ninjavan.co/api-dev/$country/events'),
          config(/(checkout:\s?["'])(.*?)(["'])/, 'http://operatorv2-local.ninjavan.co/api-dev/$country/checkout'),
          config(/(["']url-shortener["']:\s?["'])(.*?)(["'])/, 'http://dev.nnj.vn'),
          config(/(["']order-create["']:\s?["'])(.*?)(["'])/, 'http://operatorv2-local.ninjavan.co/api-dev/$country/order-create'),
          config(/(["']order-search["']:\s?["'])(.*?)(["'])/, 'http://operatorv2-local.ninjavan.co/api-dev/$country/order-search'),
          config(/(vault:\s?["'])(.*?)(["'])/, 'http://operatorv2-local.ninjavan.co/api-dev/$country/vault'),
          config(/(shipperpanel:\s?["'])(.*?)(["'])/, 'http://operatorv2-local.ninjavan.co/api-dev/$country/shipperpanel'),
          config(/(dash:\s?["'])(.*?)(["'])/, 'http://operatorv2-local.ninjavan.co/api-dev/$country/dash'),
          config(/(dp:\s?["'])(.*?)(["'])/, 'http://operatorv2-local.ninjavan.co/api-dev/$country/dp'),
          config(/(iot:\s?["'])(.*?)(["'])/, 'https://iot-dev.ninjavan.co/'),
          config(/(reports:\s?["'])(.*?)(["'])/, 'http://operatorv2-local.ninjavan.co/api-dev/$country/reports'),
          config(/(wallet:\s?["'])(.*?)(["'])/, 'http://operatorv2-local.ninjavan.co/api-dev/$country/wallet'),
          config(/(["']operator-react["']:["'])(.*?)(["'])/, 'https://operator-react-dev.ninjavan.co'),
          config(/(dwh:\s?["'])(.*?)(["'])/, 'http://operatorv2-local.ninjavan.co/api-dev/$country/dwh'),
          config(/(["']order-service["']:\s?["'])(.*?)(["'])/, 'http://operatorv2-local.ninjavan.co/api-dev/$country/order'),
        ],
      },
      'dev-nginx-qa': {
        src: ['app/config.js'],
        overwrite: true,
        replacements: [
          config(/(env:\s?["'])(.*?)(["'])/, 'local'),
          config(/(gaTrackingId:\s?["'])(.*?)(["'])/, 'UA-106082721-3'),
          config(/(authlogin:\s?["'])(.*?)(["'])/, 'https://auth-qa.ninjavan.co'),
          config(/(dpms:\s?["'])(.*?)(["'])/, 'http://operatorv2-local.ninjavan.co/api-qa/$country/dpms'),
          config(/(engine:\s?["'])(.*?)(["'])/, 'http://operatorv2-local.ninjavan.co/api-qa/$country/engine'),
          config(/(core:\s?["'])(.*?)(["'])/, 'http://operatorv2-local.ninjavan.co/api-qa/$country/core'),
          config(/(["']core-kafka["']:\s?["'])(.*?)(["'])/, 'http://operatorv2-local.ninjavan.co/api-qa/$country/core-kafka'),
          config(/(route:\s?["'])(.*?)(["'])/, 'http://operatorv2-local.ninjavan.co/api-qa/$country/route'),
          config(/(driver:\s?["'])(.*?)(["'])/, 'http://operatorv2-local.ninjavan.co/api-qa/$country/driver'),
          config(/(["']script-engine["']:\s?["'])(.*?)(["'])/, 'http://operatorv2-local.ninjavan.co/api-qa/$country/script-engine'),
          config(/(shipper:\s?["'])(.*?)(["'])/, 'http://operatorv2-local.ninjavan.co/api-qa/$country/shipper'),
          config(/(["']shipper-search["']:\s?["'])(.*?)(["'])/, 'http://operatorv2-local.ninjavan.co/api-qa/$country/shipper-search'),
          config(/(image:\s?["'])(.*?)(["'])/, 'http://operatorv2-local.ninjavan.co/api-qa/$country/image'),
          config(/(ticketing:\s?["'])(.*?)(["'])/, 'http://operatorv2-local.ninjavan.co/api-qa/$country/ticketing'),
          config(/(addressing:\s?["'])(.*?)(["'])/, 'http://operatorv2-local.ninjavan.co/api-qa/$country/addressing'),
          config(/(hub:\s?["'])(.*?)(["'])/, 'http://operatorv2-local.ninjavan.co/api-qa/$country/hub'),
          config(/(vrp:\s?["'])(.*?)(["'])/, 'http://operatorv2-local.ninjavan.co/api-qa/$country/vrp'),
          config(/(["']shipper-dashboard["']:\s?["'])(.*?)(["'])/, 'http://operatorv2-local.ninjavan.co/api-qa/$country/shipper-dashboard'),
          config(/(auth:\s?["'])(.*?)(["'])/, 'http://operatorv2-local.ninjavan.co/api-qa/$country/auth'),
          config(/(reservation:\s?["'])(.*?)(["'])/, 'http://operatorv2-local.ninjavan.co/api-qa/$country/reservation'),
          config(/(overwatch:\s?["'])(.*?)(["'])/, 'http://operatorv2-local.ninjavan.co/api-qa/$country/overwatch'),
          config(/(events:\s?["'])(.*?)(["'])/, 'http://operatorv2-local.ninjavan.co/api-qa/$country/events'),
          config(/(checkout:\s?["'])(.*?)(["'])/, 'http://operatorv2-local.ninjavan.co/api-qa/$country/checkout'),
          config(/(["']url-shortener["']:\s?["'])(.*?)(["'])/, 'http://qa.nnj.vn'),
          config(/(["']order-create["']:\s?["'])(.*?)(["'])/, 'http://operatorv2-local.ninjavan.co/api-qa/$country/order-create'),
          config(/(["']order-search["']:\s?["'])(.*?)(["'])/, 'http://operatorv2-local.ninjavan.co/api-qa/$country/order-search'),
          config(/(vault:\s?["'])(.*?)(["'])/, 'http://operatorv2-local.ninjavan.co/api-qa/$country/vault'),
          config(/(shipperpanel:\s?["'])(.*?)(["'])/, 'http://operatorv2-local.ninjavan.co/api-qa/$country/shipperpanel'),
          config(/(dash:\s?["'])(.*?)(["'])/, 'http://operatorv2-local.ninjavan.co/api-qa/$country/dash'),
          config(/(dp:\s?["'])(.*?)(["'])/, 'http://operatorv2-local.ninjavan.co/api-qa/$country/dp'),
          config(/(iot:\s?["'])(.*?)(["'])/, 'https://iot-qa.ninjavan.co/'),
          config(/(reports:\s?["'])(.*?)(["'])/, 'http://operatorv2-local.ninjavan.co/api-qa/$country/reports'),
          config(/(wallet:\s?["'])(.*?)(["'])/, 'http://operatorv2-local.ninjavan.co/api-qa/$country/wallet'),
          config(/(["']operator-react["']:["'])(.*?)(["'])/, 'https://operator-react-qa.ninjavan.co'),
          config(/(dwh:\s?["'])(.*?)(["'])/, 'http://operatorv2-local.ninjavan.co/api-qa/$country/dwh'),
          config(/(["']order-service["']:\s?["'])(.*?)(["'])/, 'http://operatorv2-local.ninjavan.co/api-qa/$country/order'),
        ],
      },
      'dev-nginx-sandbox': {
        src: ['app/config.js'],
        overwrite: true,
        replacements: [
          config(/(env:\s?["'])(.*?)(["'])/, 'local'),
          config(/(gaTrackingId:\s?["'])(.*?)(["'])/, 'UA-106082721-3'),
          config(/(authlogin:\s?["'])(.*?)(["'])/, 'https://auth-sandbox.ninjavan.co'),
          config(/(dpms:\s?["'])(.*?)(["'])/, 'http://operatorv2-local.ninjavan.co/api-sandbox/$country/dpms'),
          config(/(engine:\s?["'])(.*?)(["'])/, 'http://operatorv2-local.ninjavan.co/api-sandbox/$country/engine'),
          config(/(core:\s?["'])(.*?)(["'])/, 'http://operatorv2-local.ninjavan.co/api-sandbox/$country/core'),
          config(/(["']core-kafka["']:\s?["'])(.*?)(["'])/, 'http://operatorv2-local.ninjavan.co/api-sandbox/$country/core-kafka'),
          config(/(route:\s?["'])(.*?)(["'])/, 'http://operatorv2-local.ninjavan.co/api-sandbox/$country/route'),
          config(/(driver:\s?["'])(.*?)(["'])/, 'http://operatorv2-local.ninjavan.co/api-sandbox/$country/driver'),
          config(/(["']script-engine["']:\s?["'])(.*?)(["'])/, 'http://operatorv2-local.ninjavan.co/api-sandbox/$country/script-engine'),
          config(/(shipper:\s?["'])(.*?)(["'])/, 'http://operatorv2-local.ninjavan.co/api-sandbox/$country/shipper'),
          config(/(["']shipper-search["']:\s?["'])(.*?)(["'])/, 'http://operatorv2-local.ninjavan.co/api-sandbox/$country/shipper-search'),
          config(/(image:\s?["'])(.*?)(["'])/, 'http://operatorv2-local.ninjavan.co/api-sandbox/$country/image'),
          config(/(ticketing:\s?["'])(.*?)(["'])/, 'http://operatorv2-local.ninjavan.co/api-sandbox/$country/ticketing'),
          config(/(addressing:\s?["'])(.*?)(["'])/, 'http://operatorv2-local.ninjavan.co/api-sandbox/$country/addressing'),
          config(/(hub:\s?["'])(.*?)(["'])/, 'http://operatorv2-local.ninjavan.co/api-sandbox/$country/hub'),
          config(/(vrp:\s?["'])(.*?)(["'])/, 'http://operatorv2-local.ninjavan.co/api-sandbox/$country/vrp'),
          config(/(["']shipper-dashboard["']:\s?["'])(.*?)(["'])/, 'http://operatorv2-local.ninjavan.co/api-sandbox/$country/shipper-dashboard'),
          config(/(auth:\s?["'])(.*?)(["'])/, 'http://operatorv2-local.ninjavan.co/api-sandbox/$country/auth'),
          config(/(reservation:\s?["'])(.*?)(["'])/, 'http://operatorv2-local.ninjavan.co/api-sandbox/$country/reservation'),
          config(/(overwatch:\s?["'])(.*?)(["'])/, 'http://operatorv2-local.ninjavan.co/api-sandbox/$country/overwatch'),
          config(/(events:\s?["'])(.*?)(["'])/, 'http://operatorv2-local.ninjavan.co/api-sandbox/$country/events'),
          config(/(checkout:\s?["'])(.*?)(["'])/, 'http://operatorv2-local.ninjavan.co/api-sandbox/$country/checkout'),
          config(/(["']url-shortener["']:\s?["'])(.*?)(["'])/, 'http://sandbox.nnj.vn'),
          config(/(["']order-create["']:\s?["'])(.*?)(["'])/, 'http://operatorv2-local.ninjavan.co/api-sandbox/$country/order-create'),
          config(/(["']order-search["']:\s?["'])(.*?)(["'])/, 'http://operatorv2-local.ninjavan.co/api-sandbox/$country/order-search'),
          config(/(vault:\s?["'])(.*?)(["'])/, 'http://operatorv2-local.ninjavan.co/api-sandbox/$country/vault'),
          config(/(shipperpanel:\s?["'])(.*?)(["'])/, 'http://operatorv2-local.ninjavan.co/api-sandbox/$country/shipperpanel'),
          config(/(dash:\s?["'])(.*?)(["'])/, 'http://operatorv2-local.ninjavan.co/api-sandbox/$country/dash'),
          config(/(dp:\s?["'])(.*?)(["'])/, 'http://operatorv2-local.ninjavan.co/api-sandbox/$country/dp'),
          config(/(iot:\s?["'])(.*?)(["'])/, 'https://iot-sandbox.ninjavan.co/'),
          config(/(reports:\s?["'])(.*?)(["'])/, 'http://operatorv2-local.ninjavan.co/api-sandbox/$country/reports'),
          config(/(wallet:\s?["'])(.*?)(["'])/, 'http://operatorv2-local.ninjavan.co/api-sandbox/$country/wallet'),
          config(/(["']operator-react["']:["'])(.*?)(["'])/, 'https://operator-react.ninjavan.co'),
          config(/(dwh:\s?["'])(.*?)(["'])/, 'http://operatorv2-local.ninjavan.co/api-sandbox/$country/dwh'),
          config(/(["']order-service["']:\s?["'])(.*?)(["'])/, 'http://operatorv2-local.ninjavan.co/api-sandbox/$country/order'),
        ],
      },
      'dev-nginx-production': {
        src: ['app/config.js'],
        overwrite: true,
        replacements: [
          config(/(env:\s?["'])(.*?)(["'])/, 'local'),
          config(/(gaTrackingId:\s?["'])(.*?)(["'])/, 'UA-106082721-3'),
          config(/(authlogin:\s?["'])(.*?)(["'])/, 'https://auth.ninjavan.co'),
          config(/(dpms:\s?["'])(.*?)(["'])/, 'http://operatorv2-local.ninjavan.co/api/$country/dpms'),
          config(/(engine:\s?["'])(.*?)(["'])/, 'http://operatorv2-local.ninjavan.co/api/$country/engine'),
          config(/(core:\s?["'])(.*?)(["'])/, 'http://operatorv2-local.ninjavan.co/api/$country/core'),
          config(/(["']core-kafka["']:\s?["'])(.*?)(["'])/, 'http://operatorv2-local.ninjavan.co/api/$country/core-kafka'),
          config(/(route:\s?["'])(.*?)(["'])/, 'http://operatorv2-local.ninjavan.co/api/$country/route'),
          config(/(driver:\s?["'])(.*?)(["'])/, 'http://operatorv2-local.ninjavan.co/api/$country/driver'),
          config(/(["']script-engine["']:\s?["'])(.*?)(["'])/, 'http://operatorv2-local.ninjavan.co/api/$country/script-engine'),
          config(/(shipper:\s?["'])(.*?)(["'])/, 'http://operatorv2-local.ninjavan.co/api/$country/shipper'),
          config(/(["']shipper-search["']:\s?["'])(.*?)(["'])/, 'http://operatorv2-local.ninjavan.co/api/$country/shipper-search'),
          config(/(image:\s?["'])(.*?)(["'])/, 'http://operatorv2-local.ninjavan.co/api/$country/image'),
          config(/(ticketing:\s?["'])(.*?)(["'])/, 'http://operatorv2-local.ninjavan.co/api/$country/ticketing'),
          config(/(addressing:\s?["'])(.*?)(["'])/, 'http://operatorv2-local.ninjavan.co/api/$country/addressing'),
          config(/(hub:\s?["'])(.*?)(["'])/, 'http://operatorv2-local.ninjavan.co/api/$country/hub'),
          config(/(vrp:\s?["'])(.*?)(["'])/, 'http://operatorv2-local.ninjavan.co/api/$country/vrp'),
          config(/(["']shipper-dashboard["']:\s?["'])(.*?)(["'])/, 'http://operatorv2-local.ninjavan.co/api/$country/shipper-dashboard'),
          config(/(auth:\s?["'])(.*?)(["'])/, 'http://operatorv2-local.ninjavan.co/api/$country/auth'),
          config(/(reservation:\s?["'])(.*?)(["'])/, 'http://operatorv2-local.ninjavan.co/api/$country/reservation'),
          config(/(lighthouse:\s?["'])(.*?)(["'])/, 'http://operatorv2-local.ninjavan.co/api/$country/lighthouse'),
          config(/(overwatch:\s?["'])(.*?)(["'])/, 'http://operatorv2-local.ninjavan.co/api/$country/overwatch'),
          config(/(events:\s?["'])(.*?)(["'])/, 'http://operatorv2-local.ninjavan.co/api/$country/events'),
          config(/(checkout:\s?["'])(.*?)(["'])/, 'http://operatorv2-local.ninjavan.co/api/$country/checkout'),
          config(/(["']url-shortener["']:\s?["'])(.*?)(["'])/, 'https://www.nnj.vn'),
          config(/(["']order-create["']:\s?["'])(.*?)(["'])/, 'http://operatorv2-local.ninjavan.co/api/$country/order-create'),
          config(/(["']order-search["']:\s?["'])(.*?)(["'])/, 'http://operatorv2-local.ninjavan.co/api/$country/order-search'),
          config(/(vault:\s?["'])(.*?)(["'])/, 'http://operatorv2-local.ninjavan.co/api/$country/vault'),
          config(/(shipperpanel:\s?["'])(.*?)(["'])/, 'http://operatorv2-local.ninjavan.co/api/$country/shipperpanel'),
          config(/(dash:\s?["'])(.*?)(["'])/, 'http://operatorv2-local.ninjavan.co/api/$country/dash'),
          config(/(dp:\s?["'])(.*?)(["'])/, 'http://operatorv2-local.ninjavan.co/api/$country/dp'),
          config(/(iot:\s?["'])(.*?)(["'])/, 'https://iot.ninjavan.co/'),
          config(/(reports:\s?["'])(.*?)(["'])/, 'http://operatorv2-local.ninjavan.co/api/$country/reports'),
          config(/(wallet:\s?["'])(.*?)(["'])/, 'http://operatorv2-local.ninjavan.co/api/$country/wallet'),
          config(/(["']operator-react["']:["'])(.*?)(["'])/, 'https://operator-react.ninjavan.co'),
          config(/(dwh:\s?["'])(.*?)(["'])/, 'http://operatorv2-local.ninjavan.co/api/$country/dwh'),
          config(/(["']order-service["']:\s?["'])(.*?)(["'])/, 'http://operatorv2-local.ninjavan.co/api/$country/order'),
        ],
      },
      'dev-nginx-ninjasaas-production': {
        src: ['app/config.js'],
        overwrite: true,
        replacements: [
          config(/(env:\s?["'])(.*?)(["'])/, 'local'),
          config(/(gaTrackingId:\s?["'])(.*?)(["'])/, 'UA-106082721-3'),
          config(/(authlogin:\s?["'])(.*?)(["'])/, 'https://auth.ninjasaas.com'),
          config(/(dpms:\s?["'])(.*?)(["'])/, 'http://operatorv2-local.ninjasaas.com/api/$country/dpms'),
          config(/(engine:\s?["'])(.*?)(["'])/, 'http://operatorv2-local.ninjasaas.com/api/$country/engine'),
          config(/(core:\s?["'])(.*?)(["'])/, 'http://operatorv2-local.ninjasaas.com/api/$country/core'),
          config(/(["']core-kafka["']:\s?["'])(.*?)(["'])/, 'http://operatorv2-local.ninjasaas.com/api/$country/core-kafka'),
          config(/(route:\s?["'])(.*?)(["'])/, 'http://operatorv2-local.ninjasaas.com/api/$country/route'),
          config(/(driver:\s?["'])(.*?)(["'])/, 'http://operatorv2-local.ninjasaas.com/api/$country/driver'),
          config(/(["']script-engine["']:\s?["'])(.*?)(["'])/, 'http://operatorv2-local.ninjasaas.com/api/$country/script-engine'),
          config(/(shipper:\s?["'])(.*?)(["'])/, 'http://operatorv2-local.ninjasaas.com/api/$country/shipper'),
          config(/(["']shipper-search["']:\s?["'])(.*?)(["'])/, 'http://operatorv2-local.ninjasaas.com/api/$country/shipper-search'),
          config(/(image:\s?["'])(.*?)(["'])/, 'http://operatorv2-local.ninjasaas.com/api/$country/image'),
          config(/(ticketing:\s?["'])(.*?)(["'])/, 'http://operatorv2-local.ninjasaas.com/api/$country/ticketing'),
          config(/(addressing:\s?["'])(.*?)(["'])/, 'http://operatorv2-local.ninjasaas.com/api/$country/addressing'),
          config(/(hub:\s?["'])(.*?)(["'])/, 'http://operatorv2-local.ninjasaas.com/api/$country/hub'),
          config(/(vrp:\s?["'])(.*?)(["'])/, 'http://operatorv2-local.ninjasaas.com/api/$country/vrp'),
          config(/(["']shipper-dashboard["']:\s?["'])(.*?)(["'])/, 'http://operatorv2-local.ninjasaas.com/api/$country/shipper-dashboard'),
          config(/(auth:\s?["'])(.*?)(["'])/, 'http://operatorv2-local.ninjasaas.com/api/$country/auth'),
          config(/(reservation:\s?["'])(.*?)(["'])/, 'http://operatorv2-local.ninjasaas.com/api/$country/reservation'),
          config(/(lighthouse:\s?["'])(.*?)(["'])/, 'http://operatorv2-local.ninjasaas.com/api/$country/lighthouse'),
          config(/(overwatch:\s?["'])(.*?)(["'])/, 'http://operatorv2-local.ninjasaas.com/api/$country/overwatch'),
          config(/(events:\s?["'])(.*?)(["'])/, 'http://operatorv2-local.ninjasaas.com/api/$country/events'),
          config(/(checkout:\s?["'])(.*?)(["'])/, 'http://operatorv2-local.ninjasaas.com/api/$country/checkout'),
          config(/(["']url-shortener["']:\s?["'])(.*?)(["'])/, 'https://www.nnj.vn'),
          config(/(["']order-create["']:\s?["'])(.*?)(["'])/, 'http://operatorv2-local.ninjasaas.com/api/$country/order-create'),
          config(/(["']order-search["']:\s?["'])(.*?)(["'])/, 'http://operatorv2-local.ninjasaas.com/api/$country/order-search'),
          config(/(vault:\s?["'])(.*?)(["'])/, 'http://operatorv2-local.ninjasaas.com/api/$country/vault'),
          config(/(shipperpanel:\s?["'])(.*?)(["'])/, 'http://operatorv2-local.ninjasaas.com/api/$country/shipperpanel'),
          config(/(dash:\s?["'])(.*?)(["'])/, 'http://operatorv2-local.ninjasaas.com/api/$country/dash'),
          config(/(dp:\s?["'])(.*?)(["'])/, 'http://operatorv2-local.ninjasaas.com/api/$country/dp'),
          config(/(iot:\s?["'])(.*?)(["'])/, 'https://iot.ninjasaas.com/'),
          config(/(reports:\s?["'])(.*?)(["'])/, 'http://operatorv2-local.ninjasaas.com/api/$country/reports'),
          config(/(wallet:\s?["'])(.*?)(["'])/, 'http://operatorv2-local.ninjasaas.com/api/$country/wallet'),
          config(/(["']operator-react["']:["'])(.*?)(["'])/, 'https://operator-react.ninjavan.co'),
          config(/(dwh:\s?["'])(.*?)(["'])/, 'http://operatorv2-local.ninjasaas.com/api/$country/dwh'),
          config(/(["']order-service["']:\s?["'])(.*?)(["'])/, 'http://operatorv2-local.ninjasaas.com/api/$country/order'),
        ],
      },
      'dev-dist-dev': {
        src: ['dist/config.*.js'],
        overwrite: true,
        replacements: [
          config('$env', 'local'),
          config('$gaTrackingId', 'UA-106082721-3'),
          config('$servers.authlogin', 'https://auth-dev.ninjavan.co'),
          config('$servers.dpms', 'https://api-dev.ninjavan.co/sg/dpms'),
          config('$servers.engine', 'https://api-dev.ninjavan.co/sg/engine'),
          config('$servers.core', 'https://api-dev.ninjavan.co/sg/core'),
          config('$servers.core-kafka', 'https://api-dev.ninjavan.co/sg/core-kafka'),
          config('$servers.route', 'https://api-dev.ninjavan.co/sg/route'),
          config('$servers.driver', 'https://api-dev.ninjavan.co/sg/driver'),
          config('$servers.script-engine', 'https://api-dev.ninjavan.co/sg/script-engine'),
          config('$servers.shipper', 'https://api-dev.ninjavan.co/sg/shipper'),
          config('$servers.shipper-search', 'https://api-dev.ninjavan.co/sg/shipper-search'),
          config('$servers.image', 'https://api-dev.ninjavan.co/sg/image'),
          config('$servers.ticketing', 'https://api-dev.ninjavan.co/sg/ticketing'),
          config('$servers.addressing', 'https://api-dev.ninjavan.co/sg/addressing'),
          config('$servers.hub', 'https://api-dev.ninjavan.co/sg/hub'),
          config('$servers.vrp', 'https://api-dev.ninjavan.co/sg/vrp'),
          config('$servers.shipper-dashboard', 'https://api-dev.ninjavan.co/sg/shipper-dashboard'),
          config('$servers.auth', 'https://api-dev.ninjavan.co/sg/auth'),
          config('$servers.reservation', 'https://api-dev.ninjavan.co/sg/reservation'),
          config('$servers.lighthouse', 'https://api-dev.ninjavan.co/sg/lighthouse'),
          config('$servers.overwatch', 'https://api-dev.ninjavan.co/sg/overwatch'),
          config('$servers.events', 'https://api-dev.ninjavan.co/sg/events'),
          config('$servers.checkout', 'https://api-dev.ninjavan.co/sg/checkout'),
          config('$servers.url-shortener', 'http://dev.nnj.vn'),
          config('$servers.order-create', 'https://api-dev.ninjavan.co/sg/order-create'),
          config('$servers.order-search', 'https://api-dev.ninjavan.co/sg/order-search'),
          config('$servers.vault', 'https://api-dev.ninjavan.co/sg/vault'),
          config('$servers.shipperpanel', 'https://api-dev.ninjavan.co/sg/shipperpanel'),
          config('$servers.dash', 'https://api-dev.ninjavan.co/sg/dash'),
          config('$servers.new-dp', 'https://api-dev.ninjavan.co/sg/dp'),
          config('$servers.iot', 'https://iot-dev.ninjavan.co/'),
          config('$servers.reports', 'https://api-dev.ninjavan.co/sg/reports'),
          config('$servers.wallet', 'https://api-dev.ninjavan.co/sg/wallet'),
          config('$servers.operator-react', 'https://operator-react-dev.ninjavan.co'),
          config('$servers.dwh', 'https://api-dev.ninjavan.co/sg/dwh'),
          config('$servers.order-service', 'https://api-dev.ninjavan.co/sg/order'),
        ],
      },
    },

    karma: {
      full: {
        configFile: 'tests/karma-conf/karma-full.conf.js',
      },
      app: {
        configFile: 'tests/karma-conf/karma-app.conf.js',
      },
      directive: {
        configFile: 'tests/karma-conf/karma-directive.conf.js',
      },
    },

    shell: {
      'complexity-report': {
        command: './node_modules/.bin/plato -r -d reports/complexity -t "Operator V2" -x node_modules app',
      },
      'npm-install': {
        command: 'npm install',
        options: {
          execOptions: {
            cwd: '.',
          },
        },
      },
    },

  });

  grunt.registerTask('default', [
    'serve',
  ]);

  grunt.registerTask('serve', [
    'shell:npm-install',
    'clean:server',
    'compass:server',
    'postcss:server',
    'connect:server',
    'watch',
  ]);

  grunt.registerTask('serve:dist', [
    'build',
    'replace:dev-dist-dev',
    'copy:reverse-bootstrap',
    'connect:dist',
    'watch:dist',
  ]);

  // this task rearranges the index.html file in preparation for the deployment process on Bamboo
  // it is used in conjunction with the clean:bootstrap task
  // we need this task so that the deployment process can be separated into two stages:
  //  stage 1: the entire dist folder is uploaded to S3
  //  stage 2: the dist/bootstrap/index.html file is uploaded to the dist/index.html folder in S3
  //           with the 'Cache-Control' header set to 'no-cache'
  // this is necessary so that the CloudFront edge servers will always get the latest build
  grunt.registerTask('bootstrap', [
    'copy:bootstrap',
    'clean:bootstrap',
  ]);

  grunt.registerTask('build', [
    'clean:dist',
    'compass:dist',
    'postcss:dist',
    'copy:dist',
    'babel:dist',
    'useminPrepare',
    'ngtemplates',
    'concat',
    'uglify',
    'cssmin',
    'filerev',
    'usemin',
    'htmlmin',
    'copy:dist-nv',
    'replace:dist-config',
    'bootstrap',
  ]);

  grunt.registerTask('test', [
    'clean:reports',
    'compass:server',
    'postcss:server',
    'karma:full',
  ]);

  grunt.registerTask('test:app', [
    'clean:reports',
    'karma:app',
  ]);

  grunt.registerTask('test:directive', [
    'clean:reports',
    'compass:server',
    'postcss:server',
    'karma:directive',
  ]);

  grunt.registerTask('report', [
    'clean:reports',
    'compass:server',
    'postcss:server',
    'karma:full',
    'shell:complexity-report',
  ]);

  /**
   * Used in the replace task to easily replace regex capture groups.
   * @param groupIdx - The index of the regex capture group to replace.
   * @param text - The replacement text. Can be a string or a function that returns a string.
   * @returns {Function} - The callback replacement function passed to the replace task.
   */
  function group(groupIdx, text) {
    return function callback(word, i, all, groups) {
      if (_.isFunction(text)) {
        text = text(groups[groupIdx]); // eslint-disable-line no-param-reassign
      }
      const copy = [].concat(groups);
      copy[groupIdx] = text;
      return copy.join('');
    };
  }

  function trim(substr, str) {
    return str.replace(substr, '');
  }

  function prefix(prefx, str) {
    return prefx.concat(str);
  }

  function config(regex, str) {
    return {
      from: regex,
      to: group(1, str),
    };
  }
};
