(function controller() {
  angular
    .module('nvOperatorApp.controllers')
    .controller('LoginController', LoginController);

  LoginController.$inject = ['$state', '$scope', 'nvAuth', 'nvToast', '$window', '$timeout', 'nvClientStore'];

  function LoginController($state, $scope, nvAuth, nvToast, $window, $timeout, nvClientStore) {
    const ctrl = this;
    ctrl.login = nvAuth.login;

    if (nvAuth.isLoggedIn()) {
      if (!_.isUndefined(nvClientStore.localStorage.get('previousUrl', { namespace: false }))) {
        const url = nvClientStore.localStorage.get('previousUrl', { namespace: false });
        $timeout(() => {
          $window.location.href = url;
        });
        nvClientStore.localStorage.remove('previousUrl', { namespace: false });
      } else {
        $state.go('container.order.list');
      }

      nvToast.success(`Hello <strong>${$scope.user.firstName}</strong>.`, 'Welcome to your operator dashboard.');
    }
  }
}());
