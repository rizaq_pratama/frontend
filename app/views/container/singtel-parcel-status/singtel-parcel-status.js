(function controller() {
  angular
    .module('nvOperatorApp.controllers')
    .controller('SingtelParcelStatusController', SingtelParcelStatusController);
  SingtelParcelStatusController.$inject = [
    'nvFileSelectDialog', 'nvTranslate', 'nvButtonFilePickerType', 'Order', 'Excel',
    'nvToast',
  ];

  function SingtelParcelStatusController(
    nvFileSelectDialog, nvTranslate, nvButtonFilePickerType, Order, Excel,
    nvToast
  ) {
    // variables
    const SAMPLE_CSV = [{
      tracking_id: 'NVSGQAPO3000026935',
      status: 'D3',
      serial_no: '123456789',
    }];

    const ctrl = this;

    ctrl.backdropLoading = false;
    ctrl.successTrackingIds = [];
    ctrl.errorTrackingIds = [];

    // functions
    ctrl.openUploadDialog = openUploadDialog;
    ctrl.downloadSampleCsv = downloadSampleCsv;

    // functions details
    function openUploadDialog($event) {
      nvFileSelectDialog.show($event, {
        title: nvTranslate.instant('commons.upload-csv'),
        fileSelect: {
          accept: nvButtonFilePickerType.EXCEL_AND_CSV,
          maxFileCount: 1,
        },
        buttonOkTitle: nvTranslate.instant('commons.submit'),
        onFinishSelect: onFinishSelect,
      });

      function onFinishSelect(files) {
        const payloads = [];

        Excel.read(files[0]).then(excelReadSuccess);

        function excelReadSuccess(response) {
          // process json data
          let hasError = false;
          _.forEach(response.data, (row, index) => {
            if (_.size(row) <= 0) {
              return _.noop();
            }

            const order = processOrderPayload(row);

            // validate
            if (!order.tracking_id) {
              nvToast.error(
                nvTranslate.instant('container.singtel-parcel-status.require-to-fill-in-tracking-id', {
                  index: index + 2,
                })
              );

              hasError = true;
              return false;
            } else if (!order.status) {
              nvToast.error(
                nvTranslate.instant('container.singtel-parcel-status.require-to-fill-in-status', {
                  index: index + 2,
                })
              );

              hasError = true;
              return false;
            }

            payloads.push(order);
            return _.noop();
          });

          if (hasError) {
            return;
          }

          // submit
          ctrl.backdropLoading = true;
          Order.uploadSingtelOrderStatus(payloads)
            .then(onSuccess, onFailure).finally(finallyFn);
        }

        function processOrderPayload(row) {
          const payload = {
            serial_no: '',
            status: '',
            tracking_id: '',
          };

          _.forEach(row, (value, key) => {
            const splitKeys = _.split(key, '.');

            _.reduce(splitKeys, (thePayload, theKey, theIndex) => {
              if ((_.size(splitKeys) - 1) === theIndex) {
                thePayload[theKey] = _.toString(value) || null;
              }

              if (_.isUndefined(thePayload[theKey])) {
                thePayload[theKey] = {};
              }

              return thePayload[theKey];
            }, payload);
          });

          return payload;
        }

        function onSuccess(response) {
          ctrl.successTrackingIds = _.get(response, 'tracking_ids') || [];
          ctrl.errorTrackingIds = [];
        }

        function onFailure(response) {
          if (_.isArray(response)) {
            ctrl.successTrackingIds = [];
            ctrl.errorTrackingIds = response;
          }
        }

        function finallyFn() {
          ctrl.backdropLoading = false;
        }
      }
    }

    function downloadSampleCsv() {
      const header = [];
      const content = [];
      _.forEach(SAMPLE_CSV, (row, index) => {
        _.forEach(row, (value, key) => {
          if (index === 0) {
            header.push(key);
          }

          content.push(value);
        });
      });

      Excel.write([header, content], 'sample_singtel.xlsx');
    }
  }
}());
