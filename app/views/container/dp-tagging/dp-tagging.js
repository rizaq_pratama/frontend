(function controller() {
  angular.module('nvOperatorApp.controllers')
    .controller('DPTaggingController', DPTaggingController);

  DPTaggingController.$inject = [
    'nvTranslate', 'DistributionPoint', 'nvToast', '$window', '$state',
    'nvDateTimeUtils', 'nvTimezone', 'nvExtendUtils', 'nvDialog', 'nvFileUtils',
    'nvCsvParser', 'nvTable', 'Transaction', '$timeout',
  ];
  function DPTaggingController(
    nvTranslate, DistributionPoint, nvToast, $window, $state,
    nvDateTimeUtils, nvTimezone, nvExtendUtils, nvDialog, nvFileUtils,
    nvCsvParser, nvTable, Transaction, $timeout
  ) {
    const ctrl = this;

    // const
    const STATE = {
      IDLE: 'idle',
      WAITING: 'waiting',
    };

    const SAMPLE_CSV = [
      { trackingId: 'NVSGPAOV4001002003004', dpId: 10 },
      { trackingId: 'NVSGPAOV4001002004005', dpId: 10 },
      { trackingId: 'NVSGPAOV4001002005006', dpId: 12 },
    ];

    // variables
    ctrl.uploadState = STATE.IDLE;
    ctrl.pickupResultsTableParams = null;
    ctrl.deliveryResultsTableParams = null;
    ctrl.updateState = STATE.IDLE;
    ctrl.untaggingState = STATE.IDLE;
    ctrl.capacityState = STATE.IDLE;
    ctrl.dps = [];
    ctrl.dpOptions = [];
    ctrl.dpDateOptionsCollection = [];

    // methods
    ctrl.onFileSelect = onFileSelect;
    ctrl.assignSingle = assignSingle;
    ctrl.assignMultiple = assignMultiple;
    ctrl.untagBulkToDp = untagBulkToDp;
    ctrl.editOrder = editOrder;
    ctrl.refreshOrderDate = refreshOrderDate;
    ctrl.downloadSampleCsv = downloadSampleCsv;
    ctrl.downloadCurrentViewToCsv = downloadCurrentViewToCsv;

    init();
    // methods definition

    function init() {
      refreshDpCapacity();
    }

    function onFileSelect(file) {
      ctrl.uploadState = STATE.WAITING;
      return DistributionPoint.resolveUpload(file)
        .then(success, error);


      function success(results) {
        if (results && results.length === 0) {
          nvToast.error(nvTranslate.instant('container.dp-tagging.no-order-data-to-process-please-check-the-file'));
        } else {
          nvToast.success(nvTranslate.instant('container.dp-tagging.file-successfully-uploaded'));
        }
        refreshOrder(results);
        ctrl.uploadState = STATE.IDLE;
      }

      function error() {
        ctrl.uploadState = STATE.IDLE;
      }
    }

    function editOrder(order) {
      $window.open(
        $state.href('container.order.edit', { orderId: order.orderId })
      );
    }

    // START >>> bulk dp tagging <<< START
    function assignMultiple($event) {
      if (_.size(ctrl.deliveryResultsTableParams.getSelection()) <= 0) {
        nvToast.error(nvTranslate.instant('container.dp-tagging.select-at-least-one'));
        return;
      }

      ctrl.tagErrorContainer = [];
      // todo: cautions! currently only support delivery transactions tagging (drop_off)
      const orderToTags = _(ctrl.deliveryResultsTableParams.getTableData())
        .filter((order) => {
          if (ctrl.deliveryResultsTableParams.isSelected(order)) {
            return true;
          }
          ctrl.tagErrorContainer.push(assignErrorMessage(order, 'not selected'));
          return false;
        })
        .filter((order) => {
          if (order.distributionPointId != null && order.dropOffDate != null) {
            return true;
          }
          ctrl.tagErrorContainer.push(assignErrorMessage(order, 'dp or date empty'));
          return false;
        })
        .map(order => _.assign(order, {
          payload: {
            order_id: order.orderId,
            dp_id: order.distributionPointId,
            drop_off_date: nvDateTimeUtils.displayDate(
              order.dropOffDate, nvTimezone.getOperatorTimezone()
            ),
          },
        }))
        .value();

      if (orderToTags.length > 1) {
        multiTagging(orderToTags);
      } else if (orderToTags.length === 1) {
        singleTagging(orderToTags);
      }

      function singleTagging(orders) {
        const requests = _.cloneDeep(orders);
        const request = _.head(requests);
        const payload = request.payload;

        setBulkTaggingUpdating(true);
        DistributionPoint
          .tagOrder(payload)
          .then(() => {
            setBulkTaggingUpdating(false);

            const order = _.find(
              ctrl.deliveryResultsTableParams.getTableData(), ['orderId', request.orderId]
            );
            ctrl.deliveryResultsTableParams.deselect(order);
            ctrl.deliveryResultsTableParams.remove(order, 'orderId');

            // show toast
            nvToast.success(
              nvTranslate.instant('container.dp-tagging.order-tagged-successfully', {
                count: 1,
              })
            );
          }, () => setBulkTaggingUpdating(false));
      }

      function multiTagging(orders) {
        const requests = _.cloneDeep(orders);

        ctrl.bulkActionProgressPayload = {
          totalCount: requests.length,
          currentIndex: 0,
          errors: [],
        };

        nvDialog.showSingle($event, {
          templateUrl: 'lib/ninja-commons/services/dialogs/bulk-action-progress/bulk-action-progress.dialog.html',
          cssClass: 'bulk-action-progress',
          controller: 'BulkActionProgressDialogController',
          controllerAs: 'ctrl',
          skipHide: true,
          clickOutsideToClose: false,
          locals: {
            payload: ctrl.bulkActionProgressPayload,
          },
        }).then(progressDialogClosed);

        startMultiSelectRescheduling(requests);
      }

      function startMultiSelectRescheduling(requests) {
        let request;
        if (requests.length > 0) {
          setBulkTaggingUpdating(true);
          ctrl.bulkActionProgressPayload.currentIndex += 1;
          request = _.head(requests.splice(0, 1));
          DistributionPoint
            .tagOrder(request.payload, null, true)
            .then(multiSelectSuccess, multiSelectError);
        } else {
          setBulkTaggingUpdating(false);
        }

        function multiSelectSuccess() {
          ctrl.bulkActionProgressPayload.successCount += 1;
          if (requests.length > 0) {
            startMultiSelectRescheduling(requests);
          } else {
            setBulkTaggingUpdating(false);
          }
        }

        function multiSelectError(error) {
          const message = error.message || error.title || _.get(error, 'error.title');
          ctrl.tagErrorContainer.push(assignErrorMessage(request, message));
          ctrl.bulkActionProgressPayload.errors.push(`Order ${request.trackingId}: ${message}`);
          if (requests.length > 0) {
            startMultiSelectRescheduling(requests);
          } else {
            setBulkTaggingUpdating(false);
          }
        }
      }

      function progressDialogClosed() {
        if (getSuccessCount() > 0) {
          nvToast.success(
            nvTranslate.instant('container.dp-tagging.dp-tagging-performed-successfully'),
            nvTranslate.instant('container.dp-tagging.success-to-tag-x-orders', { count: getSuccessCount() })
          );
        }
        if (_.size(ctrl.tagErrorContainer) > 0) {
          nvToast.error(nvTranslate.instant('container.dp-tagging.some-order-failed'));
        }
        refreshDpCapacity(refreshOrder, ctrl.tagErrorContainer);
      }

      function getSuccessCount() {
        return ctrl.bulkActionProgressPayload.totalCount -
          ctrl.bulkActionProgressPayload.errors.length;
      }

      function setBulkTaggingUpdating(b) {
        if (b) {
          ctrl.updateState = STATE.WAITING;
        } else {
          ctrl.updateState = STATE.IDLE;
        }
      }

      function assignErrorMessage(order, message) {
        return _.assign(order, { message: message, isError: true });
      }
    }
    // END >>> bulk dp tagging <<< END

    // START >>> bulk dp un-tagging <<< START
    function untagBulkToDp($event) {
      if (_.size(ctrl.deliveryResultsTableParams.getSelection()) <= 0) {
        nvToast.error(nvTranslate.instant('container.dp-tagging.select-at-least-one'));
        return;
      }

      ctrl.tagErrorContainer = [];
      // todo: cautions! currently only support delivery transactions tagging (drop_off)
      const orderToTags = _(ctrl.deliveryResultsTableParams.getTableData())
        .filter((order) => {
          if (ctrl.deliveryResultsTableParams.isSelected(order)) {
            return true;
          }
          ctrl.tagErrorContainer.push(assignErrorMessage(order, 'not selected'));
          return false;
        })
        .value();

      if (orderToTags.length > 1) {
        multiTagging(orderToTags);
      } else if (orderToTags.length === 1) {
        singleTagging(orderToTags);
      }

      function singleTagging(orders) {
        const requests = _.cloneDeep(orders);
        const request = _.head(requests);

        setBulkUnTaggingUpdating(true);
        DistributionPoint
          .unTagOrder(request.orderId)
          .then(() => {
            setBulkUnTaggingUpdating(false);

            const order = _.find(
              ctrl.deliveryResultsTableParams.getTableData(), ['orderId', request.orderId]
            );
            ctrl.deliveryResultsTableParams.deselect(order);
            ctrl.deliveryResultsTableParams.remove(order, 'orderId');
            // show toast
            nvToast.success(
              nvTranslate.instant('container.dp-tagging.order-untagged-successfully', {
                count: 1,
              })
            );
          }, () => setBulkUnTaggingUpdating(false));
      }

      function multiTagging(orders) {
        const requests = _.cloneDeep(orders);

        ctrl.bulkActionProgressPayload = {
          totalCount: requests.length,
          currentIndex: 0,
          errors: [],
        };

        nvDialog.showSingle($event, {
          templateUrl: 'lib/ninja-commons/services/dialogs/bulk-action-progress/bulk-action-progress.dialog.html',
          cssClass: 'bulk-action-progress',
          controller: 'BulkActionProgressDialogController',
          controllerAs: 'ctrl',
          skipHide: true,
          clickOutsideToClose: false,
          locals: {
            payload: ctrl.bulkActionProgressPayload,
          },
        }).then(progressDialogClosed);

        startMultiSelectRescheduling(requests);
      }

      function startMultiSelectRescheduling(requests) {
        let request;
        if (requests.length > 0) {
          setBulkUnTaggingUpdating(true);
          ctrl.bulkActionProgressPayload.currentIndex += 1;
          request = _.head(requests.splice(0, 1));
          DistributionPoint
            .unTagOrder(request.orderId, null, true)
            .then(multiSelectSuccess, multiSelectError);
        } else {
          setBulkUnTaggingUpdating(false);
        }

        function multiSelectSuccess() {
          ctrl.bulkActionProgressPayload.successCount += 1;
          if (requests.length > 0) {
            startMultiSelectRescheduling(requests);
          } else {
            setBulkUnTaggingUpdating(false);
          }
        }

        function multiSelectError(error) {
          const message = error.message || error.title || _.get(error, 'error.title');
          ctrl.tagErrorContainer.push(assignErrorMessage(request, message));
          ctrl.bulkActionProgressPayload.errors.push(`Order ${request.trackingId}: ${message}`);
          if (requests.length > 0) {
            startMultiSelectRescheduling(requests);
          } else {
            setBulkUnTaggingUpdating(false);
          }
        }
      }

      function progressDialogClosed() {
        if (getSuccessCount() > 0) {
          nvToast.success(
            nvTranslate.instant('container.dp-tagging.dp-untagging-performed-successfully'),
            nvTranslate.instant('container.dp-tagging.success-to-untag-x-orders', { count: getSuccessCount() })
          );
        }
        if (_.size(ctrl.tagErrorContainer) > 0) {
          nvToast.error(nvTranslate.instant('container.dp-tagging.some-order-failed-to-be-untag'));
        }
        refreshDpCapacity(refreshOrder, ctrl.tagErrorContainer);
      }

      function getSuccessCount() {
        return ctrl.bulkActionProgressPayload.totalCount -
          ctrl.bulkActionProgressPayload.errors.length;
      }

      function setBulkUnTaggingUpdating(b) {
        if (b) {
          ctrl.untaggingState = STATE.WAITING;
        } else {
          ctrl.untaggingState = STATE.IDLE;
        }
      }

      function assignErrorMessage(order, message) {
        return _.assign(order, { message: message, isError: true });
      }
    }
    // END >>> bulk dp un-tagging <<< END

    function assignSingle(order) {
      order.isLoading = true;
      const payload = {
        order_id: order.orderId,
        dp_id: order.distributionPointId,
        drop_off_date: nvDateTimeUtils.displayDate(
          order.dropOffDate, nvTimezone.getOperatorTimezone()
        ),
      };
      return DistributionPoint.tagOrder(payload)
        .then(success, error)
        .finally(() => (order.isLoading = false));

      function success() {
        nvToast.success(nvTranslate.instant('container.dp-tagging.success-to-assign-dp-for-order', { x: order.trackingId }));

        const theOrder = _.find(
          ctrl.deliveryResultsTableParams.getTableData(), ['orderId', order.orderId]
        );
        ctrl.deliveryResultsTableParams.deselect(theOrder);
        ctrl.deliveryResultsTableParams.remove(theOrder, 'orderId');
      }

      function error() {
        nvToast.error(nvTranslate.instant('container.dp-tagging.failed-to-assign-dp-for-order', { x: order.trackingId }));
      }
    }

    function refreshDpCapacity(callback, orders) {
      ctrl.capacityState = STATE.WAITING;
      DistributionPoint.getCapacity({ all: true })
        .then(success)
        .finally(() => {
          ctrl.capacityState = STATE.IDLE;
          if (callback) {
            callback(orders);
          }
        });

      function success(datas) {
        ctrl.dps = DistributionPoint.extendDps(datas.dps);

        // options
        ctrl.dpOptions = DistributionPoint.buildOptions(ctrl.dps);
        ctrl.dpDateOptionsCollection = DistributionPoint.buildDateOptions(ctrl.dps);
      }
    }

    function refreshOrderDate(order) {
      order.dateOptions = order.distributionPointId
        ? ctrl.dpDateOptionsCollection[order.distributionPointId].options
        : [];
      order.dropOffDate = _.size(order.dateOptions) > 0 ? order.dateOptions[0].value : null;
    }

    function tryAssignDpFromDpCapacity(dpId) {
      const selected = _.find(ctrl.dpOptions, opt => opt.legacyValue === dpId);
      if (selected) {
        return selected.value;
      }
      return null;
    }

    function refreshOrder(orders) {
      ctrl.deliveryResultsTableParams = null;
      ctrl.pickupResultsTableParams = null;

      $timeout(() => {
        ctrl.deliveryResultsTableParams = nvTable.createTable();
        ctrl.deliveryResultsTableParams.addColumn('trackingId', {
          displayName: 'container.dp-tagging.tracking-id',
        });
        ctrl.deliveryResultsTableParams.addColumn('orderType', {
          displayName: 'container.dp-tagging.order-type',
        });
        ctrl.deliveryResultsTableParams.addColumn('orderGranularStatus', {
          displayName: 'container.dp-tagging.granular-status',
        });
        ctrl.deliveryResultsTableParams.addColumn('address', {
          displayName: 'container.dp-tagging.delivery-address',
        });
        ctrl.deliveryResultsTableParams.addColumn('shipperName', {
          displayName: 'container.dp-tagging.shipper',
        });
        ctrl.deliveryResultsTableParams.addColumn('currentDistributionPointId', {
          displayName: 'container.dp-tagging.current-dp-id',
        });
        ctrl.deliveryResultsTableParams.addColumn('distributionPointId', {
          displayName: 'container.dp-tagging.dp-id',
        });
        ctrl.deliveryResultsTableParams.addColumn('dropOffDate', {
          displayName: 'commons.date',
        });

        ctrl.pickupResultsTableParams = nvTable.createTable();

        const uploadDeliveryResults = [];
        const uploadPickupResults = [];
        processAndExtendOrders(orders);
        if (_.size(uploadDeliveryResults) > 0) {
          ctrl.deliveryResultsTableParams.setData(uploadDeliveryResults);
        }

        if (_.size(uploadPickupResults) > 0) {
          ctrl.pickupResultsTableParams.setData(uploadPickupResults);
          nvToast.error(nvTranslate.instant('container.dp-tagging.pickup-dp-ignored'));
        }

        function processAndExtendOrders(theOrders) {
          return _.map(theOrders, order => (
            setCustomOrderData(order)
          ));

          function setCustomOrderData(order) {
            order.orderId = +order.orderId;
            order.distributionPointId = tryAssignDpFromDpCapacity(
              +order.distributionPointId
            );
            order.address = _.join(
              _.compact([order.address1, order.address2])
              , ' ');
            order.isLoading = false;

            refreshOrderDate(order);

            if (order.transactionType === Transaction.TYPE.DELIVERY) {
              uploadDeliveryResults.push(order);
            } else if (order.transactionType === Transaction.TYPE.PICKUP) {
              uploadPickupResults.push(order);
            }

            return order;
          }
        }
      });
    }

    function downloadCurrentViewToCsv() {
      const csvContent = _.map(ctrl.deliveryResultsTableParams.data, order => ({
        trackingId: order.trackingId,
        dpId: order.distributionPointId,
        message: order.message || '-',
      }));
      const fileName = `tag-order-to-dp-${nvDateTimeUtils.displayDateTime(moment(), nvTimezone.getOperatorTimezone())}.csv`;
      nvCsvParser
        .unparse(csvContent)
        .then(content => nvFileUtils.downloadCsv(content, fileName));
    }

    function downloadSampleCsv() {
      const sample = _.map(SAMPLE_CSV, row => (
        [row.trackingId, row.dpId]
      ));

      nvCsvParser
        .unparse(sample)
        .then(content => nvFileUtils.downloadCsv(content, 'sample-tag-order-to-dp.csv'));
    }
  }
}());
