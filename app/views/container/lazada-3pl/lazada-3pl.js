(function controller() {
  angular
          .module('nvOperatorApp.controllers')
          .controller('Lazada3plController', Lazada3plController);
  Lazada3plController.$inject = [
    'nvFileSelectDialog',
    'nvTranslate',
    'nvButtonFilePickerType',
    'Order',
    'Excel',
    '$q',
    '$filter',
  ];

  function Lazada3plController(nvFileSelectDialog,
    nvTranslate, nvButtonFilePickerType, Order, Excel, $q, $filter) {
    const ctrl = this;
    ctrl.upload = upload;
    ctrl.onSelectFile = onSelectFile;
    ctrl.uploadAnother = uploadAnother;
    ctrl.success = [];
    ctrl.errors = [];
    ctrl.STATE = {
      READY: 'ready',
      WAITING: 'waiting',
      DONE: 'done',
    };
    ctrl.SAMPLE_CSV = [{
      tracking_id: 'NVSG-009090',
      comment: 'test comment',
    },
    {
      tracking_id: 'NVSG-009091',
      comment: 'test comment 2',
    }];
    ctrl.file = null;
    ctrl.SAMPLE_CSV_HEADER = ['tracking_id', 'comment'];
    ctrl.ERROR_CSV_HEADER = ['tracking_id', 'comment', 'reason'];
    ctrl.viewState = ctrl.STATE.READY;
    ctrl.data = {
      toUpload: [],
      errors: [],
      success: [],
    };
    const STATUS = {
      SUCCESS: 'Success',
    };

    function onSelectFile(file) {
      ctrl.file = file;
    }

    function upload() {
      ctrl.viewState = ctrl.STATE.WAITING;
      return Excel.read(ctrl.file).then(onFinishRead);
    }

    function uploadAnother() {
      ctrl.file = null;
      ctrl.data.toUpload = [];
      ctrl.data.errors = [];
      ctrl.data.success = [];
      ctrl.viewState = ctrl.STATE.READY;
    }

    function onFinishRead(data) {
      // validate the column number
      if (data.cols.length < 2) {
        return $q.reject();
      }
      _.forEach(data.data, (row) => {
        if (_.isEmpty(row.comment)) {
          row.reason = 'Empty comment';
          ctrl.data.errors.push(row);
          return;
        }
        if (_.trim(row.comment).length === 0) {
          row.reason = 'Empty comment';
          ctrl.data.errors.push(row);
          return;
        }
        if (_.trim(row.comment).length > 255) {
          row.comment = $filter('nvLimitTo')(row.comment, 252);
        }
        ctrl.data.toUpload.push(row);
      });
      if (ctrl.data.toUpload.length === 0) {
        return $q.reject();
      }
      const chunks = _.chunk(ctrl.data.toUpload, 1000);
      const promises = _.map(chunks, chunk => Order.lazadaCancelledBy3pl(chunk));
      return $q.all(promises).then(onFinishUpload);
    }

    function onFinishUpload(results) {
      let result = [];
      _.forEach(results, (arr) => {
        result = _.union(result, arr);
      });
      _.forEach(result, (row) => {
        if (row.result === STATUS.SUCCESS) {
          ctrl.data.success.push(row);
        } else {
          const rowData = _.find(ctrl.data.toUpload, { tracking_id: row.tracking_id });
          if (rowData) {
            rowData.reason = row.result;
          }
          ctrl.data.errors.push(rowData);
        }
      });
      ctrl.viewState = ctrl.STATE.DONE;
    }
  }
}());
