(function controller() {
  angular
        .module('nvOperatorApp.controllers')
        .controller('ChangeDateDialogController', ChangeDateDialogController);

  ChangeDateDialogController.$inject = ['$mdDialog', 'Shipping', '$rootScope', 'nvDateTimeUtils', '$scope'];
  function ChangeDateDialogController($mdDialog, Shipping, $rootScope, nvDateTimeUtils, $scope) {
    const ctrl = this;
    ctrl.onChangeDate = onChangeDate;
    ctrl.dateChanged = false;
    ctrl.date = null;
    ctrl.onCancel = onCancel;
    function onChangeDate() {
      if (ctrl.date) {
        return $mdDialog.hide(ctrl.date);
      }
      return $mdDialog.cancel();
    }

    function onCancel() {
      return $mdDialog.cancel();
    }

    $scope.$watch('ctrl.date', () => {
      if (ctrl.date) {
        ctrl.dateChanged = true;
      }
    });
  }
}());
