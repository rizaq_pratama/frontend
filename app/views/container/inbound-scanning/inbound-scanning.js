(function InboundScanningCtrl() {
  angular.module('nvOperatorApp.controllers')
        .controller('InboundScanningController', controller);

  controller.$inject = ['$rootScope', 'Shipping', '$timeout', '$scope',
      'nvDialog', 'nvToast', 'nvDateTimeUtils', '$q', 'nvTranslate', 'nvTimezone'];
  function controller($rootScope, Shipping, $timeout,
                      $scope, nvDialog, nvToast, nvDateTimeUtils, $q, nvTranslate, nvTimezone) {
    const ctrl = this;
    ctrl.scanState = 'ready';
    ctrl.scanStateText = '';
    ctrl.scanStateCommand = 'AWAITING SCAN..';
    ctrl.shippingId = '';
    ctrl.availableHubs = [];
    ctrl.scanCardState = 'ready';
    ctrl.hideCardContent = hideCardContent;
    ctrl.hub = null;
    ctrl.inbounding = false;
    ctrl.startInbounding = startInbounding;
    ctrl.onChangeEndDate = onChangeEndDate;
    ctrl.inboundingType = {
      value: '01',
    };
    ctrl.showInstruction = {
      display: false,
    };
    ctrl.showHubSelector = {
      display: true,
    };

    ctrl.scans = [];

    ctrl.onLoad = onLoad;
    ctrl.scanShipment = scanShipment;


    function startInbounding() {
      ctrl.inbounding = true;
    }

    function hideCardContent(value) {
      const status = value.display;
      value.display = !status;
    }

    function onLoad() {
            // load the hubs for current country
      const country = $rootScope.domain;
      return Shipping.getHubs(country)
                .then(onSuccess);
      function onSuccess(result) {
        ctrl.availableHubs = _.sortBy(_.map(result.hubs, o => ({
          value: o,
          displayName: o.name,
        })), 'displayName', 'asc');
      }
    }

    function scanShipment(evt) {
      if (evt) {
        if (evt.charCode !== 13 && evt.charCode !== 9) {
          return $q.reject();
        }
      }
      if (!ctrl.shipmentId) {
        return $q.reject();
      }
      if (!ctrl.hub) {
        nvToast.error(nvTranslate.instant('container.inbound-scanning.please-select-hub-first'));
        return $q.reject();
      }

      const payload = {
        scan: {
          scan_value: ctrl.shipmentId,
          hub_country: ctrl.hub.country,
          hub_id: ctrl.hub.id,
        },
      };
      if (ctrl.inboundingType.value === '01') {
        return Shipping.hubInboundScan(payload)
                    .then(onSuccess, onFailure);
      }
      return Shipping.vanInboundScan(payload)
                    .then(onSuccess, onFailure);


      function onSuccess(result) {
        if (!result.scans) {
          return;
        }
        const scan = result.scans[0];
        const scanResult = scan.result;
        const shipmentIds = _.map(result.scans, s => (s.shipment_id));
        const timeStamp = moment().tz(nvTimezone.getOperatorTimezone());
        const hubName = scan.hub_name;

        if (scanResult) {
          if (scanResult === 'Transit') {
            ctrl.scanState = 'transit';
          } else if (scanResult === 'Completed') {
            ctrl.scanState = 'completed';
          } else if (scanResult === 'Cancelled') {
            ctrl.scanState = 'cancelled';
          } else {
            ctrl.scanState = 'error';
          }
          $timeout(() => {
            $scope.successSound.playPause(0);
          }, 1);
          ctrl.scanCardState = 'success';
          ctrl.scanMessage = nvTranslate.instant('container.inbound-scanning.last-scanned',
            {
              shipmentId: _.cloneDeep(ctrl.shipmentId),
            });
          buildScanStatusCardStatus(shipmentIds, ctrl.scanState);
          addToScanTable(shipmentIds, ctrl.scanState, timeStamp, hubName);
          $timeout(() => {
            resetScanState();
          }, 1);
        } else {
          errorOnScan(result);
        }
      }

      function onFailure(err) {
        errorOnScan(err);
      }

      function errorOnScan(result = {}) {
        ctrl.scanCardState = 'error';
        try {
          const { data: { data: { message } } } = result;
          ctrl.scanMessage = message || nvTranslate.instant('container.inbound-scanning.invalid-scanning-message',
            {
              shipmentId: _.clone(ctrl.shipmentId),
            });
        } catch (ex) {
          // empty
        }
        $timeout(() => {
          $scope.errorSound.playPause(0);
        }, 1);
        addToScanTable(ctrl.shipmentId, 'error', new Date());
        $timeout(() => {
          resetScanState(true);
        }, 1);
      }
    }

    function addToScanTable(shipmentIds, state, timestamp, hubName) {
      // default changeddate flag to false
      const toAdd = _.map(_.castArray(shipmentIds), (shipmentId) => ({
        sn: 1,
        timestamp: timestamp,
        shipmentId: shipmentId,
        result: state,
        hubName: hubName,
        changedDate: false,
      }));
      ctrl.scans = _.concat(ctrl.scans, toAdd);
      reorderSn(ctrl.scans);
    }

    function buildScanStatusCardStatus(shipmentIds, scanState) {
      if (scanState === 'ready') {
        ctrl.scanStateText = '';
        ctrl.scanStateCommand = nvTranslate.instant('container.inbound-scanning.state-command.awaiting-scan');
      } else if (scanState === 'cancelled') {
        ctrl.scanStateText = nvTranslate.instant('container.inbound-scanning.state-text.shipment-cancelled');
        ctrl.scanStateCommand = nvTranslate.instant('container.inbound-scanning.state-command.pull-shipment');
      } else if (scanState === 'transit') {
        ctrl.scanStateText = nvTranslate.instant('container.inbound-scanning.state-text.still-in-transit');
        ctrl.scanStateCommand = nvTranslate.instant('container.inbound-scanning.state-command.continue-shipment');
      } else if (scanState === 'completed') {
        ctrl.scanStateText = nvTranslate.instant('container.inbound-scanning.state-text.destination-reached');
        ctrl.scanStateCommand = nvTranslate.instant('container.inbound-scanning.state-command.open-shipment');
      }
      ctrl.scanStateShipmentIds = shipmentIds;
    }

        /**
         *
         * @param failed : true if a scan is failed, false if scan is success
         */
    function resetScanState(failed) {
      ctrl.shipmentId = '';
      if (failed) {
        ctrl.scanState = 'ready';
        buildScanStatusCardStatus(null, ctrl.scanState);
      }
    }

    function onChangeEndDate($event) {
      nvDialog.showSingle($event, {
        templateUrl: 'views/container/inbound-scanning/dialogs/change-date/' +
                'change-date.dialog.html',
        theme: 'nvGreen',
        cssClass: 'nv-container-shipment-change-date',
        controllerAs: 'ctrl',
        controller: 'ChangeDateDialogController',

      }).then(onSuccess);
      function onSuccess(newDate) {
        const dateString = nvDateTimeUtils.displayDate(newDate);
                // filter by changedDate true
        const shipmentsToChange = _.filter(ctrl.scans, (o) => {
          if (o.result !== 'error' && o.changedDate === false) {
            return o;
          }
          return null;
        });
        return Shipping.updateShipmentDates(shipmentsToChange, dateString)
                    .then(onSuccessUpdateDate);

        function onSuccessUpdateDate() {
          nvToast.info('Success update end date');
                    // change changedDate flag and end date  upon success
          _.forEach(shipmentsToChange, (o) => {
            o.end_date = dateString;
            o.changedDate = true;
          });
        }
      }
    }

    /*
     recalculate s/n value
     */
    function reorderSn(orders) {
      let sn = 0;
      return _.forEach(orders, (o) => {
        sn += 1;
        o.sn = sn;
        return o;
      });
    }

    ctrl.onLoad();
  }
}());
