(function controller() {
  angular.module('nvOperatorApp.controllers')
    .controller('RFPreBiddingManagementController', RFPreBiddingManagementController);

  RFPreBiddingManagementController.$inject = [
    '$q',
    '$scope',
    'nvTranslate',
    'nvToast',
    'Driver',
    'DriverType',
    'DriverTemplate',
    'nvButtonFilePickerType',
    'nvFileSelectDialog',
    'nvCsvParser',
  ];

  function RFPreBiddingManagementController(
    $q,
    $scope,
    nvTranslate,
    nvToast,
    Driver,
    DriverType,
    DriverTemplate,
    nvButtonFilePickerType,
    nvFileSelectDialog,
    nvCsvParser
  ) {
      // ////////////////////////////////////////////////
      // variables
      // ////////////////////////////////////////////////
    const ctrl = this;
    let driverTypes = [];
    let resetTemplate = {};
    let isStart = true;

    ctrl.sampleCsvHeader = ['Username', 'Driver Type ID', 'Zone ID 1', 'Zone ID 1 - Max Parcels', 'Zone ID 2', 'Zone ID 2 - Max Parcels', 'Zone ID 3', 'Zone ID 3 - Max Parcels', 'Comments'];
    ctrl.sampleCsvOrder = ['username', 'driverType', 'zoneId1', 'maxWaypoints1', 'zoneId2', 'maxWaypoints2', 'zoneId3', 'maxWaypoints3', 'comments'];
    ctrl.sampleCsv = [];
    ctrl.possibleOptions = [];
    ctrl.selectedOptions = [];
    ctrl.searchText = '';

    ctrl.getAll = getAll;
    ctrl.showSelectCsvDialog = showSelectCsvDialog;
    ctrl.updateFilter = updateFilter;

      // ////////////////////////////////////////////////
      // function details
      // ////////////////////////////////////////////////
    function getAll() {
      const readPromises = [
        DriverType.searchAll(),
        DriverTemplate.read(),
      ];

      $q.all(readPromises).then(success);

      function success(response) {
        driverTypes = response[0];
        resetTemplate = response[1][0];

        _.forEach(DriverType.toFilterOptions(driverTypes), (option) => {
          if (_.findIndex(resetTemplate.driverTypeIds, id => id === option.id) !== -1) {
            ctrl.selectedOptions.push(option);
          } else {
            ctrl.possibleOptions.push(option);
          }
        });

        $scope.$watch(() => ctrl.selectedOptions.length, updateFilter);
      }
    }

    function showSelectCsvDialog($event) {
      nvFileSelectDialog.show($event, {
        title: nvTranslate.instant('container.change-delivery-timings.upload-title'),
        fileSelect: {
          accept: nvButtonFilePickerType.CSV,
          maxFileCount: 1,
        },
        buttonOkTitle: nvTranslate.instant('container.change-delivery-timings.upload-csv'),
        onFinishSelect: parseAndUpdatePreferences,
        onDone: angular.noop,
      });
    }

    function updateFilter() {
      // the watcher will be triggered during initialization,
      // prevent from calling updating in this case
      if (isStart) {
        isStart = false;
        return;
      }

      resetTemplate.driverTypeIds = _.map(ctrl.selectedOptions, option => option.id);

      DriverTemplate.update(resetTemplate.id, { driverTypeIds: resetTemplate.driverTypeIds });
    }

    function parseAndUpdatePreferences(files) {
      const file = files[0];
      const templateId = resetTemplate.id;
      const config = {
        header: true,
        dynamicTyping: true,
      };

      nvCsvParser.parse(file, config).then(updatePreferences);

      function updatePreferences(result) {
        const fields = result.meta.fields;
        const data = result.data;

        if (!_.isEqual(fields, ctrl.sampleCsvHeader)) {
          nvToast.error(nvTranslate.instant('commons.incorrect-csv-header'));
          return;
        }

        const mappedData = _(data)
          .map(d => toBackendFormat(d))
          .filter(d => d.username && d.username !== '')
          .value();

        Driver
          .updatePreferences(templateId, { driverPreferences: mappedData })
          .then(() => nvToast.success(nvTranslate.instant('container.rf-prebidding-management.update-preferences-response')));
      }
    }

    function toBackendFormat(preference) {
      return {
        username: preference.Username,
        comments: preference.Comments,
        driverTypeId: preference['Driver Type ID'],
        zonePreferences: [
          {
            zoneId: preference['Zone ID 1'],
            maxWaypoints: preference['Zone ID 1 - Max Parcels'],
          },
          {
            zoneId: preference['Zone ID 2'],
            maxWaypoints: preference['Zone ID 2 - Max Parcels'],
          },
          {
            zoneId: preference['Zone ID 3'],
            maxWaypoints: preference['Zone ID 3 - Max Parcels'],
          },
        ],
      };
    }
  }
}());
