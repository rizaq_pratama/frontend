;(function() {
    'use strict';

    angular
        .module('nvOperatorApp.controllers')
        .controller('DriversController', DriversController);

    DriversController.$inject = ['$http', 'nvToast'];

    function DriversController($http, nvToast) {
        var ctrl = this;

        ctrl.getDriverTypes = getDriverTypes;

        //////////////////////////////////////////////////

        function getDriverTypes() {
            $http.get('api/drivers/types').then(success, failure);
        }

        function success(result) {
            console.log(result);
            nvToast.success('We successfully got the data from ' + result.config.url);
        }

        function failure(error) {
            console.log(error);
            nvToast.error('We failed to get the data from ' + error.config.url);
        }
    }

})();
