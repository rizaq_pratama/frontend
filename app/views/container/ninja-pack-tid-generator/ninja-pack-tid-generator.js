(function controller() {
  angular
    .module('nvOperatorApp.controllers')
    .controller('NinjaPackTidGeneratorController', NinjaPackTidGeneratorController);

  NinjaPackTidGeneratorController.$inject = ['Order', 'Excel', 'nvDateTimeUtils', 'nvDialog', '$mdDialog',
  '$scope', 'nvToast', 'nvTranslate'];

  const PARCEL_SIZE_OPTIONS = [
    { value: 'xs', displayName: 'container.ninja-pack-tid-generator.packet-size-extra-small' },
    { value: 'sm', displayName: 'container.ninja-pack-tid-generator.packet-size-small' },
    { value: 'md', displayName: 'container.ninja-pack-tid-generator.packet-size-medium' },
    { value: 'lg', displayName: 'container.ninja-pack-tid-generator.packet-size-large' },
    { value: 'xl', displayName: 'container.ninja-pack-tid-generator.packet-size-extra-large' },
  ];

  const SERVICE_SCOPE_OPTIONS = [
    { value: 'Intracity', displayName: 'container.ninja-pack-tid-generator.packet-service-scope-intracity' },
    { value: 'Nationwide', displayName: 'container.ninja-pack-tid-generator.packet-service-scope-nationwide' },
  ];

  const HEADER = [
    ['Tracking ID', 'Parcel Size', 'Service Scope', 'Expiry Date.'],
  ];


  function NinjaPackTidGeneratorController(Order, Excel, nvDateTimeUtils, nvDialog, $mdDialog,
    $scope, nvToast, nvTranslate) {
    const ctrl = this;

    ctrl.generate = {
      onClick: onGenerate,
      state: 'idle',
    };

    init();

    function init() {
      ctrl.parcelSize = {
        value: PARCEL_SIZE_OPTIONS[0].value,
        options: PARCEL_SIZE_OPTIONS,
      };
      ctrl.parcelServiceScope = {
        value: SERVICE_SCOPE_OPTIONS[0].value,
        options: SERVICE_SCOPE_OPTIONS,
      };
      ctrl.quantity = null;
    }

    function onGenerate($event) {
      const payload = {
        count: ctrl.quantity,
        parcel_size: ctrl.parcelSize.value,
        service_scope: ctrl.parcelServiceScope.value,
      };

      nvDialog.showSingle($event, {
        cssClass: 'ninja-pack-tid-generator-confirmation-dialog',
        templateUrl: 'views/container/ninja-pack-tid-generator/dialog/confirmation.dialog.html',
        scope: {
          onCancel: onCancel,
          onConfirm: onConfirm,
          payload: payload,
        },
      }).then(() => {
        ctrl.generate.state = 'waiting';
        Order.generatePackTrackingId(payload)
          .then((datas) => {
            const body = _.map(datas,
              data => [data.tracking_id, data.parcel_size, data.service_scope, data.expiry_date]
            );
            const datasWithHeader = _.concat(
              HEADER,
              body
            );
            nvToast.success(nvTranslate.instant('container.ninja-pack-tid-generator.tacking-id-generated-successfully'));
            Excel.write(datasWithHeader, `ninja_pack_tracking_id_${nvDateTimeUtils.displayDateTime(moment())}.xlsx`);
            init();
          })
          .finally(() => {
            ctrl.generate.state = 'idle';
            resetForm();
          });
      });

      function onCancel() {
        $mdDialog.cancel();
      }

      function onConfirm() {
        $mdDialog.hide(0);
      }
    }

    function resetForm() {
      $scope.simpleForm.quantity.$setPristine();
      $scope.simpleForm.quantity.$setUntouched();
    }
  }
}());
