(function controller() {
  angular
        .module('nvOperatorApp.controllers')
        .controller('TagManagementController', TagManagementController);

  TagManagementController.$inject = [
    '$q', 'Tag', 'nvToast',
    'nvDialog', 'nvEditModelDialog', 'nvUtils',
    'nvTable', 'nvTranslate',
  ];

  function TagManagementController(
    $q, Tag, nvToast,
    nvDialog, nvEditModelDialog, nvUtils,
    nvTable, nvTranslate
  ) {
    // ////////////////////////////////////////////////
    // variables
    // ////////////////////////////////////////////////
    const ctrl = this;

    ctrl.tableParam = nvTable.createTable(Tag.FIELDS);

    // ////////////////////////////////////////////////
    // functions
    // ////////////////////////////////////////////////
    ctrl.getTags = getTags;
    ctrl.addTag = addTag;
    ctrl.editTag = editTag;


    // ////////////////////////////////////////////////
    // functions details
    // ////////////////////////////////////////////////
    function getTags() {
      return Tag.all().then(success, $q.reject);

      function success(response) {
        ctrl.tableParam.setData(response.tags);
      }
    }

    function addTag($event) {
      const fields = Tag.getAddFields();

      nvDialog.showSingle($event, {
        templateUrl: 'views/container/tag-management/dialog/tag-management-add.dialog.html',
        cssClass: 'tag-management-add',
        theme: 'nvGreen',
        scope: _.assign(nvEditModelDialog.scopeTemplate({ onSave: create }), { fields: fields }),
      }).then(success);

      function create(data) {
        return Tag.create([data]);
      }

      function success(response) {
        const tag = response.tags[0];
        ctrl.tableParam.mergeIn(tag, 'id');
        nvToast.success(`${nvTranslate.instant('commons.id')}: ${tag.id}`, nvTranslate.instant('container.tag-management.num-tag-created', { num: 1 }));
      }
    }

    function editTag($event, id) {
      const model = _.find(ctrl.tableParam.getTableData(), ['id', id]);
      const fields = Tag.getEditFields(model);

      nvDialog.showSingle($event, {
        templateUrl: 'views/container/tag-management/dialog/tag-management-edit.dialog.html',
        cssClass: 'tag-management-edit',
        scope: _.assign(nvEditModelDialog.scopeTemplate({ onSave: update }), {
          fields: fields,
          tag: model,
        }),
      }).then(success);

      function update(data) {
        return Tag.updateOne(data);
      }

      function success(response) {
        const tag = response.tag;
        ctrl.tableParam.mergeIn(tag, 'id');
        nvToast.success(`${nvTranslate.instant('commons.id')}: ${tag.id}`, nvTranslate.instant('container.tag-management.num-tag-updated', { num: 1 }));
      }
    }
  }
}());
