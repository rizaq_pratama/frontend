(function controller() {
  angular
    .module('nvOperatorApp.controllers')
    .controller('ThirdPartyShipperController', ThirdPartyShipperController);

  ThirdPartyShipperController.$inject = ['ThirdPartyShipper', 'nvTable', 'nvDialog',
                                         'nvEditModelDialog', 'nvToast', 'nvTranslate', '$stateParams'];
  function ThirdPartyShipperController(ThirdPartyShipper, nvTable, nvDialog,
                                          nvEditModelDialog, nvToast, nvTranslate, $stateParams) {
    const ctrl = this;

    ctrl.tableParam = nvTable.createTable(ThirdPartyShipper.FIELDS);
    ctrl.tableParam.setDataPromise(ThirdPartyShipper.read());
    ctrl.tableParam.prefilter($stateParams);

    ctrl.updateOne = updateOne;
    ctrl.deleteOne = deleteOne;
    ctrl.createOne = createOne;

    function updateOne($event, shipper) {
      const model = _.find(ctrl.tableParam.getTableData(), ['id', shipper.id]);
      const fields = ThirdPartyShipper.getEditFields(model);

      nvDialog.showSingle($event, {
        templateUrl: 'views/container/third-party-shipper/dialog/third-party-shipper-edit.dialog.html',
        cssClass: 'third-party-shipper-edit',
        scope: _.assign(nvEditModelDialog.scopeTemplate({ onSave: update }), {
          fields: fields,
        }),
      }).then(success);

      function update(data) {
        const updateData = _.defaults(data, {
          code: '',
        });

        return ThirdPartyShipper.update(updateData);
      }

      function success(updatedShipper) {
        ctrl.tableParam.mergeIn(updatedShipper, 'id');
        nvToast.info(nvTranslate.instant('container.third-party-shipper.shipper-updated', updatedShipper));
      }
    }

    function deleteOne($event, shipper) {
      nvDialog.confirmDelete($event, {
        content: `Are you sure you want to permanently delete \'${shipper.name}\'?`,
      }).then(onDelete);

      function onDelete() {
        return ThirdPartyShipper.delete(shipper).then(success);
      }

      function success(deletedShipper) {
        ctrl.tableParam.remove(deletedShipper, 'id');
        nvToast.warning(nvTranslate.instant('container.third-party-shipper.shipper-deleted', deletedShipper));
      }
    }

    function createOne($event) {
      const fields = ThirdPartyShipper.getAddFields();

      nvDialog.showSingle($event, {
        templateUrl: 'views/container/third-party-shipper/dialog/third-party-shipper-add.dialog.html',
        cssClass: 'third-party-shipper-add',
        theme: 'nvGreen',
        scope: _.assign(nvEditModelDialog.scopeTemplate({ onSave: create }), {
          fields: fields,
        }),
      }).then(success);

      function create(data) {
        return ThirdPartyShipper.create(data);
      }

      function success(newShipper) {
        ctrl.tableParam.mergeIn(newShipper, 'id');
        nvToast.success(nvTranslate.instant('container.third-party-shipper.shipper-created', newShipper));
      }
    }
  }
}());
