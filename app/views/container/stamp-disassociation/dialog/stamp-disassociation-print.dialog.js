(function controller() {
  angular
    .module('nvOperatorApp.controllers')
    .controller('StampDisassociationPrintController', StampDisassociationPrintController);

  StampDisassociationPrintController.$inject = ['orderData', '$mdDialog', 'Printer', 'nvToast',
    'nvTranslate'];

  function StampDisassociationPrintController(orderData, $mdDialog, Printer, nvToast,
    nvTranslate) {
    const ctrl = this;

    ctrl.orders = _.map(angular.copy(orderData), order => _.assign(order, { isSelected: true }));
    ctrl.onCancel = onCancel;
    ctrl.onPrint = onPrint;
    ctrl.isAbleToPrint = isAbleToPrint;

    init();

    function init() {
      ctrl.printState = 'idle';
    }

    function onPrint() {
      ctrl.printState = 'loading';
      const orders = _.filter(ctrl.orders, order => order.isSelected === true);
      Printer.printByPost(convertToPrintModel(orders)).then(() => {
        ctrl.printState = 'idle';
        nvToast.success(nvTranslate.instant('container.order.edit.shipping-label-printed'));
        onCancel();
      }, () => {
        ctrl.printState = 'idle';
      });
    }

    function onCancel() {
      $mdDialog.hide(0);
    }

    function isAbleToPrint() {
      return _.filter(ctrl.orders, order => order.isSelected === true).length > 0;
    }

    function convertToPrintModel(orders) {
      return _.map(orders, order => ({
        recipient_name: order.toName,
        address1: order.toAddress1,
        address2: order.toAddress2,
        contact: order.toContact,
        country: order.toCountry,
        postcode: order.toPostcode,
        shipper_name: order.fromName,
        tracking_id: order.trackingId,
        barcode: order.trackingId,
        route_id: '',
        driver_name: '',
        template: 'hub_shipping_label_70X50',
      }));
    }
  }
}());
