(function controller() {
  angular
    .module('nvOperatorApp.controllers')
    .controller('StampDisassociationController', StampDisassociationController);

  StampDisassociationController.$inject = ['Order', '$timeout', 'Printer', 'nvToast',
    'nvTranslate', 'nvAuth', 'nvDialog'];

  const LOAD_STATE = {
    IDLE: 0,
    SUCCESS: 1,
    LOADING: 2,
    ERROR: -1,
  };

  const API_STATE = {
    IDLE: 'idle',
    WAITING: 'waiting',
  };

  function StampDisassociationController(Order, $timeout, Printer, nvToast,
    nvTranslate, nvAuth, nvDialog) {
    const ctrl = this;

    ctrl.state = {};
    ctrl.data = {};
    ctrl.input = {};
    ctrl.function = {};
    ctrl.output = {};

    ctrl.state.load = LOAD_STATE.IDLE;
    ctrl.state.disassociate = LOAD_STATE.IDLE;
    ctrl.state.disassociateButtonState = API_STATE.IDLE;
    ctrl.state.print = API_STATE.IDLE;
    ctrl.state.disassociateDisabled = false;
    ctrl.state.printDisabled = false;

    ctrl.function.selectAll = selectAll;
    ctrl.function.onScan = onScan;
    ctrl.function.disassociateStamp = disassociateStamp;
    ctrl.function.print = print;
    ctrl.function.isStampIdAvailable = isStampIdAvailable;

    init();

    function init() {
      ctrl.input.search = '';
      ctrl.data.orders = [];
      ctrl.output.errorMessage = '';
    }

    function reset() {
      ctrl.data.search = null;
      if (ctrl.data.timer) {
        $timeout.cancel(ctrl.data.timer);
      }
    }

    function onScan($event) {
      // trigger on enter
      if ($event.which === 13) {
        if (ctrl.input.search && (ctrl.input.search.length > 0)
          && (ctrl.state.load !== LOAD_STATE.LOADING)) {
          const search = ctrl.input.search;
          ctrl.state.load = LOAD_STATE.LOADING;

          Order.getTrackingIdListV2(search).then((orders) => {
            if (orders.length < 1) {
              onError();
              return;
            }
            ctrl.data.orders = extendData(orders);
            reset();

            ctrl.state.load = LOAD_STATE.SUCCESS;
            ctrl.state.loadTimer = LOAD_STATE.SUCCESS;
            ctrl.data.timer = $timeout(() => (ctrl.state.loadTimer = LOAD_STATE.IDLE), 5000);
          }, onError);
        }
      }

      function onError(err) {
        if (err && err.status === 401) {
          const TOAST_TITLE = 'Network Request Error';
          const errorLines = ['Unauthenticated'];
          nvToast.error(TOAST_TITLE, errorLines, { buttonTitle: 'Re-Authenticate', buttonCallback: nvAuth.login });
        }
        ctrl.output.errorMessage = 'not found';
        ctrl.state.load = LOAD_STATE.ERROR;
        ctrl.state.loadTimer = LOAD_STATE.ERROR;
      }
    }

    function disassociateStamp(order) {
      order.disassociateButtonState = API_STATE.WAITING;

      Order.updateStamp(order.id, { stamp_id: null }).then(() => {
        order.stampId = null;
        order.disassociateButtonState = API_STATE.IDLE;
      }, () => {
        order.disassociateButtonState = API_STATE.IDLE;
      });
    }

    function print($event) {
      nvDialog.showSingle($event, {
        templateUrl: 'views/container/stamp-disassociation/dialog/stamp-disassociation-print.dialog.html',
        theme: 'nvBlue',
        cssClass: 'stamp-disassociation-print-dialog',
        controller: 'StampDisassociationPrintController',
        controllerAs: 'ctrl',
        locals: {
          orderData: ctrl.data.orders,
        },
      });
    }

    function selectAll($event) {
      $event.target.select();
    }

    function extendData(orders) {
      return _.map(orders, order => _.assign(order, {
        from: `${order.fromName} - ${order.fromContact}`,
        fromAddress: `${order.fromAddress1} ${order.fromAddress2}, ${order.fromCity}, ${order.fromCountry} ${order.fromPostcode}`,
        to: `${order.toName} - ${order.toContact}`,
        toAddress: `${order.toAddress1} ${order.toAddress2}, ${order.toCity}, ${order.toCountry} ${order.toPostcode}`,
        disassociateButtonState: API_STATE.IDLE,
      }));
    }

    function isStampIdAvailable(order) {
      return order.stampId != null;
    }
  }
}());
