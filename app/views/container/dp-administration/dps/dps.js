(function controller() {
  angular
    .module('nvOperatorApp.controllers')
    .controller('DPsController', DPsController);

  DPsController.$inject = [
    '$timeout', '$state', '$stateParams', 'DistributionPoint',
    'nvExtendUtils', 'nvDialog', 'nvEditModelDialog', 'nvAddressLookupDialog',
    'nvAddress', 'nvGoogleMaps', 'Shippers', 'Hub', '$q', 'nvAutocomplete.Data',
    'nvDateTimeUtils', 'nvTimezone', 'nvTable', 'nvCsvParser', 'nvFileUtils',
    'nvToast', 'nvTranslate',
  ];

  function DPsController($timeout, $state, $stateParams, DistributionPoint,
    nvExtendUtils, nvDialog, nvEditModelDialog, nvAddressLookupDialog,
    nvAddress, nvGoogleMaps, Shippers, Hub, $q, nvAutocompleteData,
    nvDateTimeUtils, nvTimezone, nvTable, nvCsvParser, nvFileUtils,
    nvToast, nvTranslate) {
    const ctrl = this;

    const DP_PARTNER_OLD_ID = $stateParams.dpPartnerId;
    const DP_PARTNER_GLOBAL_ID = $stateParams.dpPartnerGlobalId;
    const DP_PARTNER_NAME = $stateParams.dpPartnerName;

    let hubOptions;
    let hubsMap = {};
    let oldAllDayVal = {};
    let oldAllDayValOperating = {};

    ctrl.getDPs = getDPs;
    ctrl.addDP = addDP;
    ctrl.editDP = editDP;
    ctrl.viewDPUsers = viewDPUsers;
    ctrl.goBackToDPPartners = goBackToDPPartners;
    ctrl.dps = [];
    ctrl.dpsTableParams = null;
    ctrl.dpPartnerName = decodeURIComponent($stateParams.dpPartnerName);
    ctrl.downloadCsv = downloadCsv;

    // ////////////////////////////////////////////////

    function getDPs() {
      // todo - server should throw 404 when dpPartnerId doesn't exist;
      // currently it returns an empty array
      return $q.all(read()).then(success, goBackToDPPartners);

      function read() {
        return [
          DistributionPoint.readAllV2(DP_PARTNER_GLOBAL_ID),
          Hub.read(),
        ];
      }

      function success([dps, hubs]) {
        hubOptions = Hub.toOptionsWithId(hubs);
        hubsMap = _.keyBy(hubOptions, 'value');

        DistributionPoint.setOptions('hub_id', hubOptions);
        const extendedDps = extend(dps.dps);
        initTable();
        ctrl.tableParam.setData(extendedDps);
      }
    }

    function initTable() {
      ctrl.tableParam = nvTable.createTable({
        id: { displayName: 'commons.id' },
        dpms_id: { displayName: 'commons.id' },
        name: { displayName: 'commons.name' },
        short_name: { displayName: 'container.dp-administration.dps.shortname' },
        hub: { displayName: 'commons.model.hub' },
        address: { displayName: 'commons.address' },
        directions: { displayName: 'container.dp-administration.dps.directions' },
        activity: { displayName: 'container.dp-administration.dps.activity' },
      });
    }

    function addDP($event) {
      oldAllDayVal = {};
      oldAllDayValOperating = {};
      const fields = DistributionPoint.getAddFields();
      const state = {
        isAddressFinder: false,
      };
      const days = ['allDay', 'monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday', 'sunday'];
      const startTimeDefault = DistributionPoint.defaultOpenHour();
      const endTimeDefault = DistributionPoint.defaultCloseHour();

      fields.opening_hours = _.reduce(days, (opening_hours, day) => {
        opening_hours[day] = [{
          start_time: angular.copy(startTimeDefault),
          end_time: angular.copy(endTimeDefault),
        }];
        return opening_hours;
      }, {});

      fields.operating_hours = _.reduce(days, (operating_hours, day) => {
        operating_hours[day] = [{
          start_time: angular.copy(startTimeDefault),
          end_time: angular.copy(endTimeDefault),
        }];
        return operating_hours;
      }, {});

      fields.selectedMode = '10';
      fields.selectedModeOperating = '10';
      setAllSelected(1);
      setAllSelected(2);
      setupAutocomplete(fields);

      nvDialog.showSingle($event, {
        templateUrl: 'views/container/dp-administration/dialog/distribution-point/distribution-point-add.dialog.html',
        cssClass: 'distribution-point-add',
        theme: 'nvGreen',
        scope: _.assign(nvEditModelDialog.scopeTemplate({ onSave: create }),
          {
            fields: fields,
            state: state,
            onClickFind: onClickFind,
            onClickback: onClickback,
            onAllDayChange: onAllDayChange,
            onAllDayChangeOperating: onAllDayChangeOperating,
            addMoreOpeningHour: addMoreOpeningHour,
            removeOpeningHour: removeOpeningHour,
            addMoreOperatingHour: addMoreOperatingHour,
            removeOperatingHour: removeOperatingHour,
            isUpdate: false,
          }),
      }).then(success);

      function create(payload) {
        payload.shipper_id = getSelectedShipperId(fields);
        payload.opening_hours = getOpeningHours(fields);
        payload.operating_hours = getOperatingHours(fields);
        payload.hub_id = _.get(fields, 'hub_id.value.value') || _.get(fields, 'hub_id.value');
        if (_.isEmpty(payload.operating_hours) || _.isEmpty(payload.opening_hours)) {
          nvToast.error(nvTranslate.instant("container.hub-list.please-fill-at-least-one-operating-hour"));
          return $q.reject();
        }
        return DistributionPoint.createV2(DP_PARTNER_GLOBAL_ID, payload);
      }

      function success(data) {
        ctrl.tableParam.mergeIn(extend(data), 'id');
      }

      function onClickFind() {
        state.isAddressFinder = true;
      }

      function onClickback(address) {
        state.isAddressFinder = false;
        if (address) {
          fields.address_1.value = address.address1;
          fields.address_2.value = address.address2;
          fields.city.value = address.city;
          fields.country.value = nvAddress.getCountryCode(address.country);
          fields.latitude.value = address.latitude;
          fields.longitude.value = address.longitude;
          fields.postal_code.value = address.postcode;
          setDirty(this);
        }
      }

      function onAllDayChange(time, startEnd) {
        if (!time) {
          return;
        }
        let isChanged = true;
        if (oldAllDayVal[startEnd]) {
          isChanged = !time.isSame(oldAllDayVal[startEnd]);
        }
        if (isChanged) {
          _.each(fields.opening_hours, (a, key, collection) => {
            if (key !== 'allDay') {
              collection[key] = angular.copy(fields.opening_hours.allDay);
            }
          });
          setAllSelected(1);
          oldAllDayVal[startEnd] = angular.copy(time);
        }
      }

      function onAllDayChangeOperating(time, startEnd) {
        if (!time) {
          return;
        }
        let isChanged = true;
        if (oldAllDayValOperating[startEnd]) {
          isChanged = !time.isSame(oldAllDayValOperating[startEnd]);
        }
        if (isChanged) {
          _.each(fields.operating_hours, (a, key, collection) => {
            if (key !== 'allDay') {
              collection[key] = angular.copy(fields.operating_hours.allDay);
            }
          });
          setAllSelected(2);
          oldAllDayValOperating[startEnd] = angular.copy(time);
        }
      }

      // 1 for opening hours
      // 2 for operating hours
      function setAllSelected(idx = 1) {
        if (idx === 1) {
          fields.selectedDate = {
            monday: true,
            tuesday: true,
            wednesday: true,
            thursday: true,
            friday: true,
            saturday: true,
            sunday: true,
          };
        } else if (idx === 2) {
          fields.selectedDateOperating = {
            monday: true,
            tuesday: true,
            wednesday: true,
            thursday: true,
            friday: true,
            saturday: true,
            sunday: true,
          };
        }
      }

      function addMoreOpeningHour(day) {
        fields.opening_hours[day].push({
          start_time: null,
          end_time: null,
        });
      }

      function removeOpeningHour(day, idx) {
        fields.opening_hours[day][idx] = null;
        fields.opening_hours[day] = _.compact(fields.opening_hours[day]);
      }

      function addMoreOperatingHour(day) {
        fields.operating_hours[day].push({
          start_time: null,
          end_time: null,
        });
      }

      function removeOperatingHour(day, idx) {
        fields.operating_hours[day][idx] = null;
        fields.operating_hours[day] = _.compact(fields.operating_hours[day]);
      }
    }

    function editDP($event, data) {
      const model = _.cloneDeep(data);
      oldAllDayVal = {};
      oldAllDayValOperating = {};
      const fields = DistributionPoint.getEditFields(model);
      const state = {
        isAddressFinder: false,
      };
      setupAutocomplete(fields);
      fields.opening_hours = mapToDate(model.opening_hours);
      fields.operating_hours = mapToDate(model.operating_hours);
      fields.selectedDate = checkSelectedDate(fields.opening_hours);
      fields.selectedDateOperating = checkSelectedDate(fields.operating_hours);
      fields.selectedMode = '01';
      fields.selectedModeOperating = '01';

      nvDialog.showSingle($event, {
        templateUrl: 'views/container/dp-administration/dialog/distribution-point/distribution-point-edit.dialog.html',
        cssClass: 'distribution-point-edit',
        scope: _.assign(nvEditModelDialog.scopeTemplate({ onSave: update }),
          {
            fields: fields,
            state: state,
            onClickFind: onClickFind,
            onClickback: onClickback,
            addMoreOpeningHour: addMoreOpeningHour,
            onAllDayChange: onAllDayChange,
            onAllDayChangeOperating: onAllDayChangeOperating,
            removeOpeningHour: removeOpeningHour,
            addMoreOperatingHour: addMoreOperatingHour,
            removeOperatingHour: removeOperatingHour,
            isUpdate: true,
          }),
      }).then(success);

      function update(payload) {
        payload.shipper_id = getSelectedShipperId(fields);
        payload.opening_hours = getOpeningHours(fields);
        payload.operating_hours = getOperatingHours(fields);
        payload.hub_id = _.get(fields, 'hub_id.value.value') || _.get(fields, 'hub_id.value');
        if (_.isEmpty(payload.operating_hours) || _.isEmpty(payload.opening_hours)) {
          nvToast.error(nvTranslate.instant("container.hub-list.please-fill-at-least-one-operating-hour"));
          return $q.reject();
        }
        return DistributionPoint.updateV2(DP_PARTNER_GLOBAL_ID, payload.id, payload);
      }

      function success(response) {
        ctrl.tableParam.mergeIn(extend(response), 'id');
      }

      function onAllDayChange(time, startEnd) {
        if (!time) {
          return;
        }
        let isChanged = true;
        if (oldAllDayVal[startEnd]) {
          isChanged = !time.isSame(oldAllDayVal[startEnd]);
        }
        if (isChanged) {
          if (fields.opening_hours.allDay && fields.opening_hours.allDay[0]
            && fields.opening_hours.allDay[0].start_time !== null
            && fields.opening_hours.allDay[0].end_time !== null) {
            _.each(fields.opening_hours, (a, key, collection) => {
              if (key !== 'allDay') {
                collection[key] = angular.copy(fields.opening_hours.allDay);
              }
            });
            setAllSelected(1);
          }
          oldAllDayVal[startEnd] = angular.copy(time);
        }
      }


      function onAllDayChangeOperating(time, startEnd) {
        if (!time) {
          return;
        }
        let isChanged = true;
        if (oldAllDayValOperating[startEnd]) {
          isChanged = !time.isSame(oldAllDayValOperating[startEnd]);
        }
        if (isChanged) {
          if (fields.operating_hours.allDay && fields.operating_hours.allDay[0]
            && fields.operating_hours.allDay[0].start_time !== null
            && fields.operating_hours.allDay[0].end_time !== null) {
            _.each(fields.operating_hours, (a, key, collection) => {
              if (key !== 'allDay') {
                collection[key] = angular.copy(fields.operating_hours.allDay);
              }
            });
            setAllSelected(2);
          }
          oldAllDayValOperating[startEnd] = angular.copy(time);
        }
      }

      function setAllSelected(idx = 1) {
        if (idx === 1) {
          fields.selectedDate = {
            monday: true,
            tuesday: true,
            wednesday: true,
            thursday: true,
            friday: true,
            saturday: true,
            sunday: true,
          };
        } else if (idx === 2) {
          fields.selectedDateOperating = {
            monday: true,
            tuesday: true,
            wednesday: true,
            thursday: true,
            friday: true,
            saturday: true,
            sunday: true,
          };
        }
      }

      function addMoreOpeningHour(day) {
        fields.opening_hours[day].push({
          start_time: null,
          end_time: null,
        });
      }

      function onClickFind() {
        state.isAddressFinder = true;
      }

      function onClickback(address) {
        state.isAddressFinder = false;
        if (address) {
          fields.address_1.value = address.address1;
          fields.address_2.value = address.address2;
          fields.city.value = address.city;
          fields.country.value = nvAddress.getCountryCode(address.country);
          fields.latitude.value = address.latitude;
          fields.longitude.value = address.longitude;
          fields.postal_code.value = address.postcode;
          setDirty(this);
        }
      }

      function removeOpeningHour(day, idx) {
        fields.opening_hours[day][idx] = null;
        fields.opening_hours[day] = _.compact(fields.opening_hours[day]);
      }


      function addMoreOperatingHour(day) {
        fields.operating_hours[day].push({
          start_time: null,
          end_time: null,
        });
      }

      function removeOperatingHour(day, idx) {
        fields.operating_hours[day][idx] = null;
        fields.operating_hours[day] = _.compact(fields.operating_hours[day]);
      }

      function checkSelectedDate(openingHours) {
        const selectedDates = {};
        _.each(openingHours, (d, key) => {
          if (d && d[0] && d[0].start_time) {
            selectedDates[key] = true;
          }
        });
        return selectedDates;
      }

      function mapToDate(openingHours) {
        const days = ['allDay', 'monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday', 'sunday'];
        const opnHours = _.reduce(days, (hours, day) => {
          hours[day] = [{
            start_time: null,
            end_time: null,
          }];
          return hours;
        }, {});

        _.each(openingHours, (day, key) => {
          opnHours[key] = _.map(day, datePair => (toMoment(datePair)));
        });

        function toMoment(datePair) {
          const obj = {
            start_time: nvDateTimeUtils
              .toMoment(datePair.start_time, nvTimezone.getOperatorTimezone()),
            end_time: nvDateTimeUtils
              .toMoment(datePair.end_time, nvTimezone.getOperatorTimezone()),
          };
          if (datePair.end_time === '23:59:59') {
            obj.end_time = nvDateTimeUtils
              .toMoment('00:00:00', nvTimezone.getOperatorTimezone());
          }
          return obj;
        }

        return opnHours;
      }
    }

    function viewDPUsers(dp) {
      $state.go('container.dp-administration.dp-users', {
        dpPartnerId: DP_PARTNER_OLD_ID,
        dpPartnerGlobalId: DP_PARTNER_GLOBAL_ID,
        dpPartnerName: DP_PARTNER_NAME,
        dpGlobalId: dp.id,
        dpId: dp.dpms_id,
        dpName: encodeURIComponent(dp.name)
          .replace(/-/g, '%2D'), // don't confuse dashes in the names with the dashes in the route
      });
    }

    function goBackToDPPartners() {
      $state.go('container.dp-administration.dp-partners');
    }

    function extend(distributionPoint) {
      const extended = _(distributionPoint)
        .castArray()
        .map((obj) => {
          const address = _.compact([obj.address_1, obj.address_2, obj.city, obj.postal_code, obj.country]).join(' ');
          return _.assign(obj, { _address: address });
        })
        .map(_.partial(nvExtendUtils.addValueForBoolean, _, 'is_active', { true: 'Active', false: 'Inactive' }))
        .map((dp) => {
          dp.hub = (dp.hub_id !== null) ? hubsMap[dp.hub_id] : null;
          return dp;
        })
        .value();
      return _.isArray(distributionPoint) ? extended : extended[0];
    }

    function setupAutocomplete(fields) {
      fields.shipper_id.delegateHandle = nvAutocompleteData.generateHandle();
      fields.shipper_id.dataService = nvAutocompleteData
        .getByHandle(fields.shipper_id.delegateHandle);

      fields.shipper_id.dataService.setPossibleOptions(getShippers);
      processSelectedShippers();

      fields.hub_id.delegateHandle = nvAutocompleteData.generateHandle();
      fields.hub_id.dataService = nvAutocompleteData.getByHandle(fields.hub_id.delegateHandle);
      fields.hub_id.dataService.setPossibleOptions(_.clone(hubOptions))
        .then(() => {
          if (fields.hub_id.value) {
            fields.hub_id.value = _.find(hubOptions, opt => opt.value === fields.hub_id.value);
          }
        });

      function getShippers(text) {
        return Shippers.filterSearch(text, this.getSelectedOptions());
      }

      function processSelectedShippers() {
        let displayName = fields.shipper_id.value;

        if (fields.shipper_id.value) {
          Shippers.elasticCompactSearch({
            legacy_ids: [fields.shipper_id.value],
            size: 1,
          }).then(readCompactSuccess).finally(finallyFn);
        }

        function readCompactSuccess(response) {
          const shippers = _.get(response, 'details');
          const theShipper = _.find(shippers, ['id', fields.shipper_id.value]);

          if (theShipper) {
            displayName = `${theShipper.id}-${theShipper.name}`;
          }
        }

        function finallyFn() {
          fields.shipper_id.dataService.removeAll();
          fields.shipper_id.dataService.select({
            id: fields.shipper_id.value,
            displayName: displayName,
          });
        }
      }
    }

    function getSelectedShipperId(fields) {
      // selected shipper can be null because it is optional
      const selectedShipper = fields.shipper_id.dataService.getSelectedOptions()[0];
      return selectedShipper && selectedShipper.id;
    }

    function getOpeningHours(fields) {
      const opening_hours = {};
      _.each(_.omit(fields.opening_hours, ['allDay']), (day, key) => {
        if (fields.selectedDate[key]) {
          opening_hours[key] = _.compact(_.map(day, datePair => (toTimeString(datePair))));
        }
      });
      return opening_hours;
    }

    function toTimeString(datePair) {
      if (datePair.start_time && datePair.end_time) {
        const obj = {
          start_time: nvDateTimeUtils
            .displayTime(datePair.start_time, nvTimezone.getOperatorTimezone()),
          end_time: nvDateTimeUtils
            .displayTime(datePair.end_time, nvTimezone.getOperatorTimezone()),
        };
        if (obj.end_time === '00:00:00') {
          obj.end_time = '23:59:59';
        }
        return obj;
      }
      return null;
    }

    function getOperatingHours(fields) {
      const operatingHours = {};
      _.each(_.omit(fields.operating_hours, ['allDay']), (day, key) => {
        if (fields.selectedDateOperating[key]) {
          operatingHours[key] = _.compact(_.map(day, datePair => (toTimeString(datePair))));
        }
      });
      return operatingHours;
    }

    function onReset() {
      this.address = {};
      this.placesSearchText = null;
      this.markers = nvGoogleMaps.clearMarkers(this.markers);
      this.map.setZoom(nvGoogleMaps.getDefaultZoomLevelForDomain());
      this.map.setCenter(nvGoogleMaps.getDefaultLatLngForDomain());
    }

    function onDone(address) {
      this.fields.address_1.value = nvAddress.address1(address);
      this.fields.address_2.value = nvAddress.address2(address);
      this.fields.city.value = nvAddress.city(address);
      this.fields.country.value = nvAddress.country(address);
      this.fields.postal_code.value = address.postcode;
      this.fields.latitude.value = address.latitude;
      this.fields.longitude.value = address.longitude;
      this.showDialog(0);
      setDirty(this);
    }

    function onCancel() {
      this.showDialog(0);
      if (this.isFormDirty) {
        setDirty(this);
      }
    }

    function setDirty(self) {
      $timeout(() => {
        self.modelForm.$setDirty();
      });
    }

    function downloadCsv() {
      const datas = ctrl.tableParam.getTableData();
      const csvData = _.map(datas, data => ({
        ID: data.id,
        Name: data.name || '-',
        'Short Name': data.short_name || '-',
        Hub: _.get(data, 'hub.displayName', '-'),
        Address: data._address || '-',
        Directions: data.directions || '-',
        Activity: data._is_active,
      }));
      nvCsvParser
        .unparse(csvData)
        .then(content => nvFileUtils.downloadCsv(content, `data-dps-${nvDateTimeUtils.displayDateTime(moment(), nvTimezone.getOperatorTimezone())}.csv`));
    }
  }
}());
