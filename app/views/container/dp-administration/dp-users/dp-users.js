(function dpUsersController() {
  angular
    .module('nvOperatorApp.controllers')
    .controller('DPUsersController', DPUsersController);

  DPUsersController.$inject = [
    '$state', '$stateParams', 'DistributionPointUser',
    'nvDialog', 'nvEditModelDialog', 'nvTable',
    'nvCsvParser', 'nvFileUtils', 'nvDateTimeUtils', 'nvTimezone',
  ];

  function DPUsersController(
    $state, $stateParams, DistributionPointUser,
    nvDialog, nvEditModelDialog, nvTable,
    nvCsvParser, nvFileUtils, nvDateTimeUtils, nvTimezone
  ) {
    const ctrl = this;

    const DP_PARTNER_OLD_ID = $stateParams.dpPartnerId;
    const DP_PARTNER_GLOBAL_ID = $stateParams.dpPartnerGlobalId;
    const DP_PARTNER_NAME = $stateParams.dpPartnerName;
    const DP_OLD_ID = $stateParams.dpId;
    const DP_GLOBAL_ID = $stateParams.dpGlobalId;

    ctrl.getDPUsers = getDPUsers;
    ctrl.addDPUser = addDPUser;
    ctrl.editDPUser = editDPUser;
    ctrl.goBackToDPPartners = goBackToDPPartners;
    ctrl.goBackToDPs = goBackToDPs;
    ctrl.dpUsers = [];
    ctrl.dpUsersTableParams = null;
    ctrl.dpPartnerName = decodeURIComponent($stateParams.dpPartnerName);
    ctrl.dpName = decodeURIComponent($stateParams.dpName);
    ctrl.downloadCsv = downloadCsv;

    // ////////////////////////////////////////////////

    function getDPUsers() {
      return search().then(success, goBackToDPs);

      function search() {
        return DistributionPointUser.search(DP_PARTNER_GLOBAL_ID, DP_GLOBAL_ID);
      }

      function success(dpUsers) {
        initTable();
        ctrl.tableParam.setData(dpUsers);
      }
    }

    function initTable() {
      ctrl.tableParam = nvTable.createTable({
        username:   { displayName: 'commons.username'   },
        first_name: { displayName: 'commons.first-name' },
        last_name:  { displayName: 'commons.last-name' },
        email:      { displayName: 'commons.email'      },
        contact_no: { displayName: 'commons.contact'    },
      });
    }

    function addDPUser($event) {
      const fields = DistributionPointUser.getAddFields();

      nvDialog.showSingle($event, {
        templateUrl: 'views/container/dp-administration/dialog/distribution-point-user/distribution-point-user-add.dialog.html',
        cssClass: 'distribution-point-user-add',
        theme: 'nvGreen',
        scope: _.assign(nvEditModelDialog.scopeTemplate({ onSave: create }), { fields: fields }),
      }).then(success);

      function create(data) {
        return DistributionPointUser.create(withIdFields(data));
      }

      function success(dpUser) {
        ctrl.tableParam.mergeIn(dpUser, 'username');
      }
    }

    function editDPUser($event, data) {
      const model = _.cloneDeep(data);
      const fields = DistributionPointUser.getEditFields(model);

      nvDialog.showSingle($event, {
        templateUrl: 'views/container/dp-administration/dialog/distribution-point-user/distribution-point-user-edit.dialog.html',
        cssClass: 'distribution-point-user-edit',
        scope: _.assign(nvEditModelDialog.scopeTemplate({ onSave: update }), { fields: fields }),
      }).then(success);

      function update(payload) {
        return DistributionPointUser.update(withIdFields(payload));
      }

      function success(dpUser) {
        ctrl.tableParam.mergeIn(dpUser, 'username');
      }
    }

    function goBackToDPPartners() {
      $state.go('container.dp-administration.dp-partners');
    }

    function goBackToDPs() {
      $state.go('container.dp-administration.dps', {
        dpPartnerId: DP_PARTNER_OLD_ID,
        dpPartnerGlobalId: DP_PARTNER_GLOBAL_ID,
        dpPartnerName: DP_PARTNER_NAME,
      });
    }

    function withIdFields(fields) {
      return _.assign(fields, { partnerId: DP_PARTNER_GLOBAL_ID, dpId: DP_GLOBAL_ID });
    }

    function downloadCsv() {
      const tableData = ctrl.tableParam.getTableData();
      const csvData = _.map(tableData, data => ({
        'Username'    : data.username,
        'First Name'  : data.first_name,
        'Last Name'   : data.last_name,
        'Email'       : data.email,
        'Contact No.' : data.contact_no,
      }));
      nvCsvParser
        .unparse(csvData)
        .then(content => nvFileUtils.downloadCsv(content, `data-dp-users-${nvDateTimeUtils.displayDateTime(moment(), nvTimezone.getOperatorTimezone())}.csv`));
    }
  }
}());
