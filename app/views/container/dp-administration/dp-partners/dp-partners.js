(function controller() {
  angular
    .module('nvOperatorApp.controllers')
    .controller('DPPartnersController', DPPartnersController);

  DPPartnersController.$inject = [
    '$q', '$state', 'DistributionPointPartner',
    'nvDialog', 'nvEditModelDialog', 'nvUtils', 'nvTableUtils',
    'nvTable', 'nvCsvParser', 'nvFileUtils', 'nvDateTimeUtils', 'nvTimezone',
  ];

  function DPPartnersController($q, $state, DistributionPointPartner,
    nvDialog, nvEditModelDialog, nvUtils, nvTableUtils,
    nvTable, nvCsvParser, nvFileUtils, nvDateTimeUtils, nvTimezone) {
    const ctrl = this;
    ctrl.getDPPartners = getDPPartners;
    ctrl.addDPPartner = addDPPartner;
    ctrl.editDPPartner = editDPPartner;
    ctrl.viewDPs = viewDPs;
    ctrl.downloadCsv = downloadCsv;
    ctrl.dpPartners = [];
    ctrl.dpPartnersTableParams = null;

    function getDPPartners() {
      return DistributionPointPartner.readAllV2().then(success, $q.reject);

      function success(dpPartners) {
        initTable();
        const datas = _.get(dpPartners, 'partners', []);
        ctrl.tableParam.setData(_.sortBy(datas, 'id'));
      }
    }

    function addDPPartner($event) {
      const fields = DistributionPointPartner.getAddFields();

      nvDialog.showSingle($event, {
        templateUrl: 'views/container/dp-administration/dialog/distribution-point-partner/distribution-point-partner-add.dialog.html',
        cssClass: 'distribution-point-partner-add',
        theme: 'nvGreen',
        scope: _.assign(nvEditModelDialog.scopeTemplate({ onSave: create }), { fields: fields }),
      }).then(success);

      function create(data) {
        return DistributionPointPartner.createV2(data);
      }

      function success(dpPartner) {
        ctrl.tableParam.mergeIn(dpPartner, 'id');
      }
    }

    function editDPPartner($event, data) {
      const model = _.cloneDeep(data);
      const fields = DistributionPointPartner.getEditFields(model);

      nvDialog.showSingle($event, {
        templateUrl: 'views/container/dp-administration/dialog/distribution-point-partner/distribution-point-partner-edit.dialog.html',
        cssClass: 'distribution-point-partner-edit',
        scope: _.assign(nvEditModelDialog.scopeTemplate({ onSave: update }), { fields: fields }),
      }).then(success);

      function update(payload) {
        return DistributionPointPartner.updateV2(model.id, payload);
      }

      function success(dpPartner) {
        ctrl.tableParam.mergeIn(dpPartner, 'id');
      }
    }

    function viewDPs(data) {
      $state.go('container.dp-administration.dps', {
        dpPartnerId: data.dpms_partner_id,
        dpPartnerGlobalId: data.id,
        dpPartnerName: encodeURIComponent(data.name)
          .replace(/-/g, '%2D'), // don't confuse dashes in the names with the dashes in the route
      });
    }

    function initTable() {
      ctrl.tableParam = nvTable.createTable({
        id: { displayName: 'commons.id' },
        name: { displayName: 'commons.name' },
        poc_name: { displayName: 'container.dp-administration.dp-partners.header-poc-name' },
        poc_tel: { displayName: 'container.dp-administration.dp-partners.header-poc-no' },
        poc_email: { displayName: 'container.dp-administration.dp-partners.header-poc-email' },
        restrictions: { displayName: 'container.dp-administration.dp-partners.header-restrictions' },
      });
    }

    function downloadCsv() {
      const datas = ctrl.tableParam.getTableData();
      const csvData = _.map(datas, data => ({
        ID: data.id,
        Name: data.name || '-',
        'POC Name': data.poc_name || '-',
        'POC No.': data.poc_tel || '-',
        'POC Email': data.poc_email || '-',
        Restrictions: data.restrictions || '-',
      }));
      nvCsvParser
        .unparse(csvData)
        .then(content => nvFileUtils.downloadCsv(content, `data-dp-users-${nvDateTimeUtils.displayDateTime(moment(), nvTimezone.getOperatorTimezone())}.csv`));
    }
  }
}());
