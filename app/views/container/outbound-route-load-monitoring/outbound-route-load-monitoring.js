(function controller() {
  angular
    .module('nvOperatorApp.controllers')
    .controller('OutboundRouteLoadController', OutboundRouteLoadController);

  OutboundRouteLoadController.$inject = ['$state'];

  function OutboundRouteLoadController($state) {
    // variables
    const ctrl = this;
    ctrl.loading = true;

    // shared params
    ctrl.contentLoading = false; // child controller content loading
    // functions
    ctrl.switchTab = switchTab;
    ctrl.isActive = isActive;

    function switchTab(stateName) {
      if ($state.current.name !== stateName) {
        $state.go(stateName);
      }
    }

    function isActive(stateName) {
      return $state.current.name === stateName;
    }
  }
}());
