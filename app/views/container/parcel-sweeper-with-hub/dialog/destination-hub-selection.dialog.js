(function controller() {
  angular.module('nvOperatorApp.controllers')
        .controller('DestinationHubSelectionDialogController', DestinationHubSelectionDialogController);

  DestinationHubSelectionDialogController.$inject = ['nvToast', 'hubs', 'hubGroups', '$scope', 'nvAutocomplete.Data', '$mdDialog', 'nvTranslate'];

  function DestinationHubSelectionDialogController(nvToast, hubs, hubGroups, $scope, nvAutocompleteData, $mdDialog, nvTranslate) {
    const ctrl = this;
    ctrl.onSync = onSync;
    ctrl.onCancel = onCancel;
    ctrl.selectedHubs = [];
    ctrl.selectedHubGroups = [];
    ctrl.selectAll = false;
    ctrl.hubs = _.clone(hubs);
    ctrl.hubGroups = _.clone(hubGroups);
    ctrl.hubSearch = nvAutocompleteData.getByHandle('hubs-auto');
    ctrl.state = 'idle';
    ctrl.onRemoveItem = onRemoveItem;

    init();

    $scope.$watch('ctrl.selectAll', () => {
      _.forEach(ctrl.hubGroups, (hg) => { hg.selected = ctrl.selectAll; });
    });

    function init() {
      ctrl.hubSearch.setPossibleOptions(_.cloneDeep(ctrl.hubs));
    }

    function onCancel() {
      return $mdDialog.cancel();
    }

    function onRemoveItem(hub) {
      ctrl.hubSearch.remove(hub);
    }

    function onSync() {
      let hubGroupsIds = [];
      _.filter(ctrl.hubGroups, hg => (hg.selected)).forEach((hgx) => {
        hubGroupsIds = _.union(hubGroupsIds, hgx.hub_ids);
      });

      const hubIds = _.map(ctrl.selectedHubs, (hub) => (hub.value.id));
      const combinedHubIds = _.union(hubGroupsIds, hubIds);
      if (_.size(combinedHubIds) <= 0) {
        nvToast.error(nvTranslate.instant('container.parcel-sweeper.please-select-hub-or-hub-group'));
      } else {
        return $mdDialog.hide(combinedHubIds);
      }
    }
  }
}());
