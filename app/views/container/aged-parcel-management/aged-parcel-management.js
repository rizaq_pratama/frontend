(function controller() {
  angular
    .module('nvOperatorApp.controllers')
    .controller('AgedParcelManagementController', AgedParcelManagementController);

  AgedParcelManagementController.$inject = [
    'nvBackendFilterUtils', 'nvTranslate', 'Shippers', 'Order', 'nvTable',
    'nvDateTimeUtils', '$state', 'nvTimezone', 'nvToast', '$window',
    'nvDialog', 'nvEditModelDialog', 'nvFileUtils', 'Transaction', '$mdDialog',
    '$q', 'nvEnv',
  ];

  function AgedParcelManagementController(
    nvBackendFilterUtils, nvTranslate, Shippers, Order, nvTable,
    nvDateTimeUtils, $state, nvTimezone, nvToast, $window,
    nvDialog, nvEditModelDialog, nvFileUtils, Transaction, $mdDialog,
    $q, nvEnv
  ) {
    // ////////////////////////////////////////////////
    // variables
    // ////////////////////////////////////////////////
    const ctrl = this;
    const PAGES = { FILTER: 'filter', RESULT: 'result' };

    ctrl.GRANULAR_STATUS = Order.GRANULAR_STATUS;
    ctrl.PAGES = PAGES;
    ctrl.currentPage = PAGES.FILTER;

    // Filter page
    ctrl.getAgedParcelsState = 'idle';

    ctrl.selectedFilters = [];
    ctrl.possibleFilters = [];

    // Result page
    ctrl.state = {
      isRescheduleUpdating: false,
    };
    ctrl.isApplyActionLoading = false;

    // ////////////////////////////////////////////////
    // functions
    // ////////////////////////////////////////////////
    ctrl.goToPage = goToPage;
    ctrl.isPage = isPage;
    ctrl.getAgedParcels = getAgedParcels;
    ctrl.getSelectedCount = getSelectedCount;
    ctrl.getSelected = getSelected;
    ctrl.returnSelectedToSender = returnSelectedToSender;
    ctrl.rescheduleSelected = rescheduleSelected;
    ctrl.downloadCsvSelected = downloadCsvSelected;
    ctrl.rescheduleToNextDay = rescheduleToNextDay;
    ctrl.returnToSender = returnToSender;
    ctrl.editOrder = editOrder;
    ctrl.isNinjavan = isNinjavan;

    // start
    init();

    // ////////////////////////////////////////////////
    // functions details
    // ////////////////////////////////////////////////
    function init() {
      const filters = initFilter();
      ctrl.possibleFilters = filters.generalFilters;
      ctrl.selectedFilters = _.remove(ctrl.possibleFilters,
          filter => filter.showOnInit === true
      );

      function initFilter() {
        const generalFilters = [
          {
            type: nvBackendFilterUtils.FILTER_AUTOCOMPLETE,
            showOnInit: true,
            mainTitle: nvTranslate.instant('commons.shipper'),
            selectedOptions: [],
            key: 'shipperIds',
            presetKey: 'shipperIds',
            backendKey: 'legacy_ids',
            callback: getShippers,
          },
          {
            type: nvBackendFilterUtils.FILTER_TEXT,
            showOnInit: true,
            mainTitle: nvTranslate.instant('container.aged-parcel-management.aged-days'),
            placeholder: nvTranslate.instant('container.aged-parcel-management.aged-days'),
            model: 1,
            key: 'ageDays',
            presetKey: 'ageDays',
          },
        ];

        return {
          generalFilters: generalFilters,
        };

        function getShippers(text) {
          return Shippers.filterSearch(
            text,
            _.isUndefined(this.getSelectedOptions) ? [] : this.getSelectedOptions()
          );
        }
      }
    }

    function goToPage(page) {
      if (page === PAGES.FILTER) {
        ctrl.page = 1;
        ctrl.agedParcelsTableParam = null;
      }
      ctrl.currentPage = page;
    }

    function isPage(page) {
      return ctrl.currentPage === page;
    }

    function getAgedParcels() {
      // prepare params
      const params = {
        tzOffset: moment.tz.zone(
          nvTimezone.getOperatorTimezone()
        ).offset(new Date().getTime()) / 60,
      };
      _.merge(params, nvBackendFilterUtils.constructParams(ctrl.selectedFilters));

      if (!_.isUndefined(params.agedDays) && isNaN(params.agedDays)) {
        nvToast.warning(
          nvTranslate.instant('container.aged-parcel-management.aged-days-error')
        );
        return;
      }

      if (_.size(params.shipperIds) > 0) {
        params.shipperIds = params.shipperIds.join(',');
      }

      // call backend
      ctrl.getAgedParcelsState = 'waiting';
      Order.getAgedParcelManagementData(params).then(success, failure);

      function success(response) {
        ctrl.getAgedParcelsState = 'idle';
        processAgedParcelsTableParam(response);
      }

      function failure() {
        ctrl.getAgedParcelsState = 'idle';
      }
    }

    function getSelectedCount() {
      return ctrl.agedParcelsTableParam.getSelection().length;
    }

    function returnSelectedToSender($event) {
      ctrl.isApplyActionLoading = true;
      getByOrderIdsWithTransactionsPromise(
        ctrl.agedParcelsTableParam.getSelection()
      ).then(success, () => {
        ctrl.isApplyActionLoading = false;
      });

      function success(response) {
        ctrl.isApplyActionLoading = false;

        nvDialog.showSingle($event, {
          templateUrl: 'views/container/order/dialog/edit-rts-details/edit-rts-details.dialog.html',
          cssClass: 'order-edit-rts-details',
          controller: 'OrderEditRtsDetailsDialogController',
          controllerAs: 'ctrl',
          locals: {
            ordersData: extendOrders(response.orders),
            settingsData: {
              isMultiSelect: true,
            },
          },
        }).then(onSet);

        function onSet(rtsResponse) {
          ctrl.agedParcelsTableParam.clearSelect();
          reloadProcessedParcels(rtsResponse.success);
        }
      }
    }

    function rescheduleSelected($event) {
      const fields = {
        date: new Date(),
      };

      nvDialog.showSingle($event, {
        type: 'nv-dialog',
        templateUrl: 'views/container/failed-delivery-management/dialog/reschedule-selected.dialog.html',
        cssClass: 'failed-delivery-management-reschedule-selected',
        scope: _.assign(nvEditModelDialog.scopeTemplate(), {
          onSubmit: onSubmit,
          fields: fields,
          selectedOrdersCount: getSelectedCount(),
          state: ctrl.state,
        }),
      });

      function onSubmit() {
        const date = nvDateTimeUtils.displayDate(fields.date);
        const agedParcels = getSelected();
        ctrl.rescheduledSuccessfully = [];
        if (agedParcels.length > 1) {
          multiSelectRescheduling(agedParcels, date);
        } else {
          singleRescheduling(agedParcels, date);
        }
      }

      function singleRescheduling(agedParcels, date) {
        const requests = _.map(agedParcels, agedParcel => generateRescheduleRequest(agedParcel, date));
        const request = _.head(requests);
        ctrl.state.isRescheduleUpdating = true;

        Order.reschedule(request.orderId, request.data).then((data) => {
          if (data.status.toLowerCase() === 'success') {
            nvToast.error(
              nvTranslate.instant('container.aged-parcel-management.rescheduling-success'),
              nvTranslate.instant('container.aged-parcel-management.num-orders-rescheduling-success', { num: 1 })
            );
            ctrl.rescheduledSuccessfully = _.concat(ctrl.rescheduledSuccessfully, requests);
            removeFromSelection(request);
          } else {
            nvToast.error(
              nvTranslate.instant('container.aged-parcel-management.rescheduling-failed')
            );
          }

          ctrl.state.isRescheduleUpdating = false;
          $mdDialog.hide();
        }, () => {
          ctrl.state.isRescheduleUpdating = false;
          $mdDialog.hide();
        });
      }

      function multiSelectRescheduling(agedParcels, date) {
        const toBeProcessedList = _.map(angular.copy(agedParcels), agedParcel =>
          generateRescheduleRequest(agedParcel, date));

        ctrl.bulkActionProgressPayload = {
          totalCount: toBeProcessedList.length,
          currentIndex: 0,
          errors: [],
        };

        // hide date dialog
        $mdDialog.hide();
        const successReschedules = [];

        nvDialog.showSingle($event, {
          templateUrl: 'lib/ninja-commons/services/dialogs/bulk-action-progress/bulk-action-progress.dialog.html',
          cssClass: 'bulk-action-progress',
          controller: 'BulkActionProgressDialogController',
          controllerAs: 'ctrl',
          skipHide: true,
          clickOutsideToClose: false,
          locals: {
            payload: ctrl.bulkActionProgressPayload,
          },
        }).then(progressDialogClosed);

        startMultiSelectRescheduling(toBeProcessedList);

        function startMultiSelectRescheduling(requests) {
          let request;
          if (requests.length > 0) {
            ctrl.state.isRescheduleUpdating = true;
            ctrl.bulkActionProgressPayload.currentIndex += 1;
            request = _.head(requests.splice(0, 1));

            Order.reschedule(request.orderId, request.data).then(multiSelectSuccess, multiSelectError);
          } else {
            // should never get there
            onMultiSelectProcessFinish();
          }

          function multiSelectSuccess(response) {
            // check status string
            if (response.status.toLowerCase() === 'success') {
              ctrl.bulkActionProgressPayload.successCount += 1;
              ctrl.rescheduledSuccessfully = _.concat(ctrl.rescheduledSuccessfully, request);

              successReschedules.push(response);
              removeFromSelection(request);

              if (requests.length > 0) {
                startMultiSelectRescheduling(requests);
              } else {
                onMultiSelectProcessFinish();
              }
            } else {
              multiSelectError({ message: response.message });
            }
          }

          function multiSelectError(error) {
            ctrl.bulkActionProgressPayload.errors.push(`${request.trackingId}: ${error.message}`);
            if (requests.length > 0) {
              startMultiSelectRescheduling(requests);
            } else {
              onMultiSelectProcessFinish();
            }
          }

          function onMultiSelectProcessFinish() {
            reloadProcessedParcels(successReschedules);
            ctrl.state.isRescheduleUpdating = false;
          }
        }
      }

      function progressDialogClosed() {
        if (getSuccessCount() > 0) {
          nvToast.success(
            nvTranslate.instant('container.aged-parcel-management.rescheduling-success'),
            nvTranslate.instant('container.aged-parcel-management.num-orders-rescheduling-success', { num: getSuccessCount() }));
        }
      }

      function getSuccessCount() {
        if (ctrl.bulkActionProgressPayload) {
          return ctrl.bulkActionProgressPayload.totalCount - ctrl.bulkActionProgressPayload.errors.length;
        }
        return 0;
      }

      function removeFromSelection(agedParcel) {
        const tableData = _.find(ctrl.agedParcelsTableParam.getSelection(), order =>
          order.tracking_id === agedParcel.trackingId);
        ctrl.agedParcelsTableParam.deselect(tableData);
      }
    }

    function downloadCsvSelected() {
      let csvContent = 'Order ID, ' +
        'Tracking ID, ' +
        'Shipper, ' +
        'Current Granular Status, ' +
        'Route Id, ' +
        'RTS, ' +
        'Hub Name, ' +
        'Inbound Datetime, ' +
        'Days since inbound, ' +
        'Last Attempt Datetime, ' +
        'Days since Last Attend, ' +
        'Total No. of Failures\n';

      _.forEach(ctrl.agedParcelsTableParam.getSelection(), (agedParcel) => {
        csvContent += (`${agedParcel.order_id},${
          agedParcel.tracking_id},${
          agedParcel.shipper},${
          agedParcel.granular_status},${
          agedParcel.route_id || ''},${
          agedParcel.custom_rts},${
          agedParcel.latest_hub_name},${
          agedParcel.custom_inbound_time},${
          agedParcel.custom_days_since_inbound},${
          agedParcel.custom_last_attempt_time},${
          agedParcel.custom_days_since_last_attempt},${
          agedParcel.total_failures}\n`);
      });

      nvFileUtils.downloadCsv(csvContent, 'aged-parcel-list.csv');
    }

    function rescheduleToNextDay(agedParcel) {
      if (agedParcel.granular_status !== Order.GRANULAR_STATUS.PENDING_RESCHEDULE) {
        return;
      }
      const request = generateRescheduleRequest(agedParcel, getTomorrowDate());

      agedParcel.custom_is_rescheduling = true;
      Order.reschedule(request.orderId, request.data).then(success, failure);

      function success(response) {
        agedParcel.custom_is_rescheduling = false;

        if (response && (response.status.toLowerCase() === 'success')) {
          nvToast.info(
            nvTranslate.instant('container.aged-parcel-management.num-order-rescheduled', {
              num: 1,
            }),
            nvTranslate.instant('container.aged-parcel-management.order-x-comma-x', {
              orders: agedParcel.tracking_id,
            })
          );

          reloadProcessedParcels(response);
        } else {
          nvToast.warning(
            nvTranslate.instant('container.aged-parcel-management.reschedule-next-day-error')
          );
        }
      }

      function failure() {
        agedParcel.custom_is_rescheduling = false;
      }

      function getTomorrowDate() {
        const newDate = new Date();
        newDate.setDate(newDate.getDate() + 1);

        return nvDateTimeUtils.displayDate(newDate);
      }
    }

    function returnToSender($event, agedParcel) {
      if (agedParcel.rts) {
        return;
      }

      agedParcel.custom_is_rts_ing = true;

      getByOrderIdsWithTransactionsPromise(agedParcel).then(success, failure);

      function success(response) {
        agedParcel.custom_is_rts_ing = false;

        nvDialog.showSingle($event, {
          templateUrl: 'views/container/order/dialog/edit-rts-details/edit-rts-details.dialog.html',
          cssClass: 'order-edit-rts-details expand',
          controller: 'OrderEditRtsDetailsDialogController',
          controllerAs: 'ctrl',
          locals: {
            ordersData: extendOrders(response.orders || []),
            settingsData: {},
          },
        }).then(onSet);

        function onSet(onSetResponse) {
          reloadProcessedParcels(onSetResponse.success);
        }
      }

      function failure() {
        agedParcel.custom_is_rts_ing = false;
      }
    }

    function editOrder(agedParcel) {
      $window.open(
        $state.href('container.order.edit', { orderId: agedParcel.order_id })
      );
    }

    function isNinjavan() {
      return nvEnv.isNinjavan(nv.config.env);
    }

    function processAgedParcelsTableParam(response) {
      _.forEach(ctrl.selectedFilters, (filter) => {
        filter.description = nvBackendFilterUtils.getDescription(filter);
      });

      if (!ctrl.agedParcelsTableParam) {
        ctrl.agedParcelsTableParam = nvTable.createTable();
        ctrl.agedParcelsTableParam.addColumn('tracking_id', { displayName: 'commons.tracking-id' });
        ctrl.agedParcelsTableParam.addColumn('shipper', { displayName: 'commons.shipper' });
        ctrl.agedParcelsTableParam.addColumn('granular_status', {
          displayName: 'container.aged-parcel-management.current-granular-status',
        });
        ctrl.agedParcelsTableParam.addColumn('route_id', { displayName: 'commons.model.route-id' });
        ctrl.agedParcelsTableParam.addColumn('custom_rts', { displayName: 'commons.rts' });
        ctrl.agedParcelsTableParam.addColumn('latest_hub_name', { displayName: 'commons.hub-name' });
        ctrl.agedParcelsTableParam.addColumn('custom_inbound_time', {
          displayName: 'container.aged-parcel-management.inbound-datetime',
        });
        ctrl.agedParcelsTableParam.addColumn('custom_days_since_inbound', {
          displayName: 'container.aged-parcel-management.days-since-inbound',
        });
        ctrl.agedParcelsTableParam.addColumn('custom_last_attempt_time', {
          displayName: 'container.aged-parcel-management.last-attempt-datetime',
        });
        ctrl.agedParcelsTableParam.addColumn('custom_days_since_last_attempt', {
          displayName: 'container.aged-parcel-management.days-since-last-attempt',
        });
        ctrl.agedParcelsTableParam.addColumn('total_failures', {
          displayName: 'container.aged-parcel-management.total-num-of-failures',
        });
      }

      ctrl.agedParcelsTableParam.setData(extendAgedParcels(response) || []);
      goToPage(PAGES.RESULT);
    }

    function extendAgedParcels(agedParcels) {
      return _.map(agedParcels, agedParcel => (
        setCustomAgedParcelData(agedParcel)
      ));
    }

    function setCustomAgedParcelData(agedParcel) {
      agedParcel.custom_is_rescheduling = false;
      agedParcel.custom_is_rts_ing = false;

      agedParcel.custom_rts = agedParcel.rts ?
        nvTranslate.instant('commons.yes') : nvTranslate.instant('commons.no');

      agedParcel.custom_days_since_inbound = '';
      agedParcel.custom_inbound_time = '';
      if (agedParcel.inbound_time) {
        agedParcel.custom_inbound_time = nvDateTimeUtils.displayDateTime(
          moment.utc(agedParcel.inbound_time)
        );

        agedParcel.custom_days_since_inbound = moment(new Date()).diff(
            moment(agedParcel.custom_inbound_time), 'days'
          ) || nvTranslate.instant('commons.today');
      }

      agedParcel.custom_days_since_last_attempt = '';
      agedParcel.custom_last_attempt_time = nvTranslate.instant('container.aged-parcel-management.no-attempt');
      if (agedParcel.last_attempt) {
        agedParcel.custom_last_attempt_time = nvDateTimeUtils.displayDateTime(
          moment.utc(agedParcel.last_attempt)
        );

        agedParcel.custom_days_since_last_attempt = moment(new Date()).diff(
            moment(agedParcel.custom_last_attempt_time), 'days'
          ) || nvTranslate.instant('commons.today');
      }

      return agedParcel;
    }

    function generateRescheduleRequest(agedParcel, date) {
      return {
        orderId: agedParcel.order_id,
        trackingId: agedParcel.tracking_id,
        shipperName: agedParcel.shipper_name,
        data: { date: date },
      }; // payload and parameter changes based on ORDER-296
    }

    function getByOrderIdsWithTransactionsPromise(agedParcels) {
      const orderIds = [];
      _.forEach(_.castArray(agedParcels), (agedParcel) => {
        orderIds.push(agedParcel.order_id);
      });

      const param = {
        noOfRecords: _.size(orderIds),
        page: 1,
        orderIds: orderIds,
        withTransactions: true,
        oneMonthSearch: false,
      };

      return Order.search(param);
    }

    function extendOrders(orders) {
      return _.map(orders, order => (
        setCustomOrderData(order)
      ));

      function setCustomOrderData(order) {
        order._granularStatus = Order.GRANULAR_STATUS[order.granularStatus] || order.granularStatus;
        order._lastPPNT = Transaction.getLastPPNT(order.transactions) || {};

        return order;
      }
    }

    function reloadProcessedParcels(agedParcels) {
      const deferred = $q.defer();

      getByOrderIdsWithTransactionsPromise(agedParcels).then(success, failure);

      return deferred.promise;

      function success(response) {
        _.forEach(response.orders, (order) => {
          const agedParcel = _.find(ctrl.agedParcelsTableParam.data, ['order_id', order.id]);
          if (agedParcel) {
            agedParcel.rts = order.rts;
            agedParcel.granular_status = Order.GRANULAR_STATUS[order.granularStatus];

            const ddnt = Transaction.getLastDDNT(order.transactions);
            if (ddnt) {
              agedParcel.route_id = ddnt.routeId;
            }

            ctrl.agedParcelsTableParam.mergeIn(setCustomAgedParcelData(agedParcel), 'order_id');
          }
        });

        deferred.resolve(response);
      }

      function failure(response) {
        deferred.reject(response);
      }
    }

    function getSelected() {
      return ctrl.agedParcelsTableParam.getSelection();
    }
  }
}());
