(function controller() {
  angular
    .module('nvOperatorApp.controllers')
    .controller('DriverStrengthDataController', DriverStrengthDataController);

  DriverStrengthDataController.$inject = ['Driver', '$stateParams', '$scope', 'nvTable', 'nvExtendUtils',
                                      'nvDialog', '$q'];

  function DriverStrengthDataController(Driver, $stateParams, $scope, nvTable, nvExtendUtils,
                                        nvDialog, $q) {
    const ctrl = this;
    ctrl.filterZones = toArray($stateParams.zone);
    ctrl.filterDriverTypes = toArray($stateParams.type);
    ctrl.filters = createFilterArrayForDescription();
    function createFilterArrayForDescription() {
      const array = [];
      if (ctrl.filterZones.length > 0) {
        array.push({ title: 'Zones',
               description: _(ctrl.filterZones)
                            .map(zone => _.find($scope.filterZones, ['id', zone]).displayName)
                            .join(),
                  });
      }
      if (ctrl.filterDriverTypes.length > 0) {
        array.push({ title: 'Driver Types',
               description: _(ctrl.filterDriverTypes)
                        .map(type => _.find($scope.filterDriverTypes, ['id', type]).displayName)
                        .join(),
                  });
      }
      return array;
    }

    ctrl.createOne = createOne;
    ctrl.updateOne = updateOne;
    ctrl.deleteOne = deleteOne;
    ctrl.refreshDriverCache = refreshDriverCache;
    ctrl.refreshDriverCacheState = 'idle';
    ctrl.toggleAvailability = toggleAvailability;
    ctrl.massUpdateAvailability = massUpdateAvailability;
    ctrl.showEditFilter = showEditFilter;
    ctrl.isNumber = _.isNumber;
    ctrl.getDataForCSV = getDataForCSV;
    ctrl.getDriverSize = getDriverSize;
    // trigger build
    const zoneOptions = Driver.getOptions('zonePreferences.zoneId');

    init();

    // funtion declaration ///////////////////////////

    function init() {
      ctrl.totalItems = Number.MAX_VALUE;
      initTable();
      ctrl.tableParam.setDataPromise(loadDrivers());
    }

    function toArray(data = '') {
      return _(data.split(',')).compact().map(_.toNumber).value();
    }

    function loadDrivers(refresh = false, reload = false) {
      if (!reload && ctrl.tableParam.getUnfilteredLength() >= ctrl.totalItems) {
        return $q.reject();
      }
      return Driver.searchAll(_.omitBy({
        driverTypeIds: ctrl.filterDriverTypes.join(','),
        zoneIds: ctrl.filterZones.join(','),
        refresh: refresh,
      }, value => _.isString(value) && _.isEmpty(value))).then(processResult).then(extend);

      function processResult(result) {
        ctrl.totalItems = result.count;
        return result.drivers;
      }
    }

    function extend(drivers) {
      return _.map(_.castArray(drivers), (driver) => {
        _.forEach(driver.vehicles, (vehicle) => {
          _.merge(vehicle, { _ownVehicle: vehicle.ownVehicle === '10' });
        });

        driver.vehicles = nvExtendUtils.addValueForBoolean(driver.vehicles, '_ownVehicle', { true: 'Yes', false: 'No' });
        driver.name = `${driver.firstName} ${driver.lastName}`;

        _.forEach(driver.zonePreferences, (zonePreference) => {
          const zone = _.find(zoneOptions, ['value', zonePreference.zoneId]);
          if (zone) {
            _.merge(zonePreference, { _zoneName: zone.displayName });
          }
        });
        return driver;
      });
    }

    function filterByHub(strFn) {
      return (driver) => {
        const lowercase = strFn().toLowerCase();
        return lowercase.length === 0 ||
          (driver.hub !== null && (`${driver.hub.value} - ${driver.hub.displayName.toLowerCase()}`).indexOf(lowercase) >= 0);
      };
    }

    function filterByVehicleType(strFn) {
      return (driver) => {
        const lowercase = strFn().toLowerCase();
        return lowercase.length === 0 || _.some(driver.vehicles, vehicle =>
          vehicle.vehicleType && vehicle.vehicleType.toLowerCase().indexOf(lowercase) >= 0
        );
      };
    }
    function filterByVehicleOwned(strFn) {
      return (driver) => {
        const lowercase = strFn().toLowerCase();
        return lowercase.length === 0 || _.some(driver.vehicles, vehicle =>
          vehicle.__ownVehicle && vehicle.__ownVehicle.toLowerCase().indexOf(lowercase) >= 0
        );
      };
    }
    function filterByZonePreference(strFn) {
      return (driver) => {
        const lowercase = strFn().toLowerCase();
        return lowercase.length === 0 || _.some(driver.zonePreferences, zonePreference =>
          zonePreference._zoneName && zonePreference._zoneName.toLowerCase().indexOf(lowercase) >= 0
        );
      };
    }
    function filterByZonePreferenceMinWaypoint(strFn) {
      return (driver) => {
        const lowercase = strFn().toLowerCase();
        return lowercase.length === 0 || _.some(driver.zonePreferences, zonePreference =>
            zonePreference.minWaypoints &&
            _.isNumber(zonePreference.minWaypoints) &&
            _.String(zonePreference.minWaypoints).indexOf(lowercase) >= 0
        );
      };
    }
    function filterByZonePreferenceMaxWaypoint(strFn) {
      return (driver) => {
        const lowercase = strFn().toLowerCase();
        return lowercase.length === 0 || _.some(driver.zonePreferences, zonePreference =>
            zonePreference.maxWaypoints &&
            _.isNumber(zonePreference.maxWaypoints) &&
            _.String(zonePreference.maxWaypoints).indexOf(lowercase) >= 0
        );
      };
    }

    function sortByVehicleType(drivers, order) {
      return _.orderBy(drivers, [drivs, 'id'], order);
      function drivs(driver) {
        return _(driver.vehicles)
          .map('vehicleType')
          .orderBy(_.identity)
          .value()
          .join(' ');
      }
    }
    function sortByVehicleOwned(drivers, order) {
      return _.orderBy(drivers, [drivs, 'id'], order);
      function drivs(driver) {
        return _(driver.vehicles)
          .map('__ownVehicle')
          .orderBy(_.identity)
          .value()
          .join(' ');
      }
    }
    function sortByZonePreference(drivers, order) {
      return _.orderBy(drivers, [sortZonePreference, 'id'], order);
      function sortZonePreference(driver) {
        return _(driver.zonePreferences)
          .map('_zoneName')
          .orderBy(_.identity)
          .value()
          .join(' ');
      }
    }
    function sortByZonePreferenceMinWaypoint(drivers, order) {
      return _.orderBy(drivers, [sortZonePreferenceMinWaypoint, 'id'], order);

      function sortZonePreferenceMinWaypoint(driver) {
        const minWaypoints = _(driver.zonePreferences)
          .map('minWaypoints')
          .orderBy(_.identity);
        return (order === 'asc' ? minWaypoints.min() : minWaypoints.max()) || 0;
      }
    }
    function sortByZonePreferenceMaxWaypoint(drivers, order) {
      return _.orderBy(drivers, [sortZonePreferenceMaxWaypoint, 'id'], order);

      function sortZonePreferenceMaxWaypoint(driver) {
        const maxWaypoints = _(driver.zonePreferences)
          .map('maxWaypoints')
          .orderBy(_.identity);
        return (order === 'asc' ? maxWaypoints.min() : maxWaypoints.max()) || 0;
      }
    }

    function createOne($event) {
      ctrl.tableParam.mergeInPromise($scope.createOne($event)
        .then(driver => extend([driver])[0]),
      'id');
    }
    function updateOne($event, uuid) {
      const model = _.find(ctrl.tableParam.getTableData(), ['uuid', uuid]);
      ctrl.tableParam.mergeInPromise($scope.updateOne($event, model).then(extend), 'id');
    }
    function deleteOne($event, driver) {
      nvDialog.confirmDelete($event, {
        content: `Are you sure you want to permanently delete \'${driver.name}\'?`,
      }).then(onDelete);

      function onDelete() {
        return Driver.deleteOne(driver.uuid).then(success);
      }

      function success(results) {
        ctrl.tableParam.remove(results, 'uuid');
      }
    }
    function refreshDriverCache() {
      ctrl.refreshDriverCacheState = 'waiting';
      initTable();
      ctrl.tableParam.setDataPromise(loadDrivers(true, true))
        .then(() => { ctrl.refreshDriverCacheState = 'idle'; });
    }
    function toggleAvailability($event, availability, uuid) {
      Driver.updateAvailability(uuid, availability).then(success);

      function success(result) {
        const updated = _.map(result.drivers, driver =>
          ({ availability: !!availability, uuid: driver.uuid }));
        ctrl.tableParam.mergeIn(updated, 'uuid');
      }
    }

    function massUpdateAvailability(availability) {
      const selection = ctrl.tableParam.getSelection();
      Driver.updateAvailability(_.map(selection, driver => driver.uuid), availability).then(success);

      function success(result) {
        const updated = _.map(result.drivers, driver =>
          ({ availability: !!availability, uuid: driver.uuid }));
        ctrl.tableParam.mergeIn(updated, 'uuid');
      }
    }

    function showEditFilter($event) {
      const zonePossibleOptions = angular.copy($scope.filterZones);
      const typePossibleOptions = angular.copy($scope.filterDriverTypes);
      const zoneSelectedOptions = _.remove(zonePossibleOptions, zone =>
        _.indexOf(ctrl.filterZones, zone.id) > -1
      );
      const typeSelectedOptions = _.remove(typePossibleOptions, type =>
        _.indexOf(ctrl.filterDriverTypes, type.id) > -1
      );

      nvDialog.showSingle($event, {
        templateUrl: 'views/container/driver-strength/dialog/driver-filter.dialog.html',
        cssClass: 'driver-edit-filter',
        theme: 'nvBlue',
        controller: 'DriverStrengthFilterDialogController',
        controllerAs: 'ctrl',
        locals: {
          filters: {
            zonePossibleOptions: zonePossibleOptions,
            typePossibleOptions: typePossibleOptions,
            zoneSelectedOptions: zoneSelectedOptions,
            typeSelectedOptions: typeSelectedOptions,
          },
          $scope: 'a',
        },
      });
    }

    function getDataForCSV() {
      const drivers = _.cloneDeep(ctrl.tableParam.getTableData());
      _.forEach(drivers, (driver) => {
        const zones = [];
        const minWaypoints = [];
        const maxWaypoints = [];
        const vehicleTypes = [];
        const ownVehicles = [];
        _.forEach(driver.zonePreferences, (zonePreference) => {
          zones.push(zonePreference._zoneName);
          minWaypoints.push(zonePreference.minWaypoints);
          maxWaypoints.push(zonePreference.maxWaypoints);
        });

        _.forEach(driver.vehicles, (vehicle) => {
          vehicleTypes.push(vehicle.vehicleType);
          ownVehicles.push(vehicle.__ownVehicle);
        });

        driver.vehicleTypes = vehicleTypes.join('\n');
        driver.ownVehicles = ownVehicles.join('\n');
        driver.zones = zones.join('\n');
        driver.minWaypoints = minWaypoints.join('\n');
        driver.maxWaypoints = maxWaypoints.join('\n');
        driver.attendance = driver.availability ? 'Yes' : 'No';
      });

      return drivers;
    }

    function initTable() {
      ctrl.tableParam = nvTable.createTable(Driver.FIELDS);
  
      ctrl.tableParam.addColumn('name', { displayName: 'commons.name' });
      ctrl.tableParam.addColumn('availability', { displayName: 'container.driver-strength.header-coming' });
  
      ctrl.tableParam.updateColumnFilter('hub', filterByHub);
      ctrl.tableParam.updateColumnFilter('vehicles.vehicleType', filterByVehicleType);
      ctrl.tableParam.updateColumnFilter('vehicles.ownVehicle', filterByVehicleOwned);
      ctrl.tableParam.updateColumnFilter('zonePreferences.zoneId', filterByZonePreference);
      ctrl.tableParam.updateColumnFilter('zonePreferences.minWaypoints', filterByZonePreferenceMinWaypoint);
      ctrl.tableParam.updateColumnFilter('zonePreferences.maxWaypoints', filterByZonePreferenceMaxWaypoint);
      ctrl.tableParam.setSortFunction('vehicles.vehicleType', sortByVehicleType);
      ctrl.tableParam.setSortFunction('vehicles.ownVehicle', sortByVehicleOwned);
      ctrl.tableParam.setSortFunction('zonePreferences.zoneId', sortByZonePreference);
      ctrl.tableParam.setSortFunction('zonePreferences.minWaypoints', sortByZonePreferenceMinWaypoint);
      ctrl.tableParam.setSortFunction('zonePreferences.maxWaypoints', sortByZonePreferenceMaxWaypoint);
    }

    function getDriverSize() {
      if (ctrl.tableParam) {
        return ctrl.tableParam.getUnfilteredLength();
      }
      return 0;
    }
  }
}());
