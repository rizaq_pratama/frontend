(function controller() {
  angular
    .module('nvOperatorApp.controllers')
    .controller('DriverStrengthController', DriverStrengthController);

  DriverStrengthController.$inject = [
    '$scope', '$rootScope', 'Driver', 'VehicleType', 'DriverContactType',
    'Zone', 'DriverType', 'Hub', 'nvDialog', 'nvEditModelDialog',
    'nvCurrency', 'nvToast', 'nvGoogleMaps', '$q',
  ];

  function DriverStrengthController(
    $scope, $rootScope, Driver, VehicleType, DriverContactType,
    Zone, DriverType, Hub, nvDialog, nvEditModelDialog,
    nvCurrency, nvToast, nvGoogleMaps, $q
  ) {

    const ctrl = this;
    const currency = nvCurrency.getSymbol($rootScope.countryId);

    $scope.createOne = createOne;
    $scope.updateOne = updateOne;
    $scope.filterZones = [];
    $scope.filterDriverTypes = [];

    ctrl.init = init;

    function createOne($event) {
      const defer = $q.defer();
      const fields = Driver.getAddFields();

      nvDialog.showSingle($event, {
        templateUrl: 'views/container/driver-strength/dialog/driver-add.dialog.html',
        cssClass: 'driver-add',
        theme: 'nvGreen',
        scope: _.assign(nvEditModelDialog.scopeTemplate({ onSave: create }), {
          fields: fields,
          currency: currency,
          checkAvailability: checkAvailability,
          onAddMore: onAddMore,
          onRemove: onRemove,
          onChangeActive: onChangeActive,
          onUpRank: onUpRank,
          onDownRank: onDownRank,
          showMaxBidLimit: showMaxBidLimit,
          isDisabled: isDisabled,
          isDisabledText: isDisabledText,
        }),
      }).then(defer.resolve, defer.reject);

      function create(data) {
        data.username = _.trim(data.username);
        return Driver.createOne(data);
      }
      return defer.promise;
    }

    function updateOne($event, model) {
      const defer = $q.defer();
      const fields = Driver.getEditFields(model);

      nvDialog.showSingle($event, {
        templateUrl: 'views/container/driver-strength/dialog/driver-edit.dialog.html',
        cssClass: 'driver-edit',
        scope: _.assign(nvEditModelDialog.scopeTemplate({ onSave: update }), {
          fields: fields,
          currency: currency,
          onAddMore: onAddMore,
          onRemove: onRemove,
          onChangeActive: onChangeActive,
          onUpRank: onUpRank,
          onDownRank: onDownRank,
          showMaxBidLimit: showMaxBidLimit,
          isDisabled: isDisabled,
          isDisabledText: isDisabledText,
        }),
      }).then(defer.resolve, defer.reject);

      function update(data) {
        data.username = _.trim(data.username);
        return Driver.updateOne(data.uuid, data);
      }
      return defer.promise;
    }

    function checkAvailability(username) {
      const trimmed = _.trim(username);
      return Driver.checkUsernameAvailability(trimmed).then(success);

      function success(result) {
        // todo show availability on dialog UI
        if (result.availability) {
          nvToast.success(`Username: ${trimmed} is available!`);
        } else {
          nvToast.warning(`Username: ${trimmed} is not available!`);
        }
        return result;
      }
    }

    function onAddMore(type, values) {
      if (type === 'zonePreferences') {
        const center = nvGoogleMaps.getDefaultLatLngForDomain();
        values.push({ latitude: center.lat, longitude: center.lng, rank: values.length + 1 });
      } else if (type === 'vehicles') {
        values.push({
          capacity: 100000,
          active: !_.some(values, isActive),
        });
      } else if (type === 'contacts') {
        values.push({
          // if there is already a 'Primary', then this new value should not be active = true
          active: !_.some(values, isActive),
        });
      }

      this.modelForm.$setDirty();

      function isActive(value) {
        return value.active;
      }
    }

    function onRemove(values, idx, type) {
      if (type === 'zonePreferences') {
        for (let i = idx + 1; i < values.length; ++i) {
          values[i].rank -= 1;
        }
      }

      values.splice(idx, 1);

      this.modelForm.$setDirty();
    }

    function onChangeActive(field, index) {
      const values = this.fields[field]._values;

      // toggle the state of the selected item
      values[index].active = !values[index].active;

      // if the selected item has become active, then un-toggle the other items
      if (values[index].active) {
        _.forEach(values, (value, idx) => {
          if (idx !== index) {
            value.active = false;
          }
        });
      }

      this.modelForm.$setDirty();
    }

    function onUpRank(values, index) {
      if (index !== 0) {
        // swap values
        let temp = values[index - 1];
        values[index - 1] = values[index];
        values[index] = temp;

        // swap ranks
        temp = values[index - 1].rank;
        values[index - 1].rank = values[index].rank;
        values[index].rank = temp;

        this.modelForm.$setDirty();
      }
    }

    function onDownRank(values, index) {
      if (index !== values.length - 1) {
        // swap values
        let temp = values[index + 1];
        values[index + 1] = values[index];
        values[index] = temp;

        // swap ranks
        temp = values[index + 1].rank;
        values[index + 1].rank = values[index].rank;
        values[index].rank = temp;

        this.modelForm.$setDirty();
      }
    }

    function showMaxBidLimit() {
      return this.fields.tags.RESUPPLY.value === '10';
    }

    function isDisabled() {
      return this.fields.vehicles._values.length === 0 ||
                this.fields.contacts._values.length === 0 ||
                this.fields.zonePreferences._values.length === 0;
    }

    function isDisabledText() {
      return []
        .concat(this.fields.vehicles._values.length === 0 ? ['vehicle'] : [])
        .concat(this.fields.contacts._values.length === 0 ? ['contact'] : [])
        .concat(this.fields.zonePreferences._values.length === 0 ? ['preferred zone'] : [])
        .join(', ');
    }

    function init() {
      return $q.all([VehicleType.searchAll(),
                      DriverContactType.searchAll(),
                      Zone.read(),
                      DriverType.searchAll(),
                      Hub.read({ archived: false })])
      .then(([vehicleType, driverContactType, zone, driverType, hubs]) => {
        Driver.setOptions('vehicles.vehicleType', VehicleType.toOptions(vehicleType));
        Driver.setOptions('contacts.type', DriverContactType.toOptions(driverContactType));
        Driver.setOptions('zonePreferences.zoneId', Zone.toOptions(zone));
        Driver.setOptions('driverType', DriverType.toOptions(driverType));
        Driver.setOptions('hub', Hub.toOptionsWithId(hubs));
        $scope.filterZones = Zone.toFilterOptions(zone);
        $scope.filterDriverTypes = DriverType.toFilterOptions(driverType);
      });
    }
  }
}());
