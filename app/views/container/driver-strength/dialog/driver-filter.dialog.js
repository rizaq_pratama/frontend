(function controller() {
  angular
    .module('nvOperatorApp.controllers')
    .controller('DriverStrengthFilterDialogController', DriverStrengthFilterDialogController);

  DriverStrengthFilterDialogController.$inject = ['Driver', '$state', '$scope', 'nvDialog', 'filters'];

  function DriverStrengthFilterDialogController(Driver, $state, $scope, nvDialog, filters) {
    const ctrl = this;
    ctrl.selectedFilters = [];
    ctrl.possibleFilters = [];
    let filter = (filters.zoneSelectedOptions.length > 0) ?
      ctrl.selectedFilters : ctrl.possibleFilters;
    filter.push({
      type: 2,
      mainTitle: 'Zones',
      possibleOptions: filters.zonePossibleOptions,
      selectedOptions: filters.zoneSelectedOptions,
      searchText: {},
    });
    filter = (filters.typeSelectedOptions.length > 0) ? ctrl.selectedFilters : ctrl.possibleFilters;
    filter.push({
      type: 2,
      mainTitle: 'Driver Types',
      possibleOptions: filters.typePossibleOptions,
      selectedOptions: filters.typeSelectedOptions,
      searchText: {},
    });

    ctrl.loadSelection = loadSelection;
    ctrl.isEmptyFilter = isEmptyFilter;
    ctrl.cancel = nvDialog.cancel;
    ctrl.isButtonDisabled = true;

    const zoneSelectedOptions = angular.copy(filters.zoneSelectedOptions);
    const typeSelectedOptions = angular.copy(filters.typeSelectedOptions);
    $scope.$watch(() => filters.zoneSelectedOptions.length, updateSubmitButtonState);
    $scope.$watch(() => filters.typeSelectedOptions.length, updateSubmitButtonState);

    function loadSelection($event) {
      const zones = _.map(filters.zoneSelectedOptions, zone => zone.id).join(',');
      const types = _.map(filters.typeSelectedOptions, type => type.id).join(',');
      $state.go('container.driver-strength.data', { type: types, zone: zones }, { reload: true });
      ctrl.cancel($event);
    }

    function isEmptyFilter() {
      return ctrl.selectedFilters.length === 0 ||
        (filters.zoneSelectedOptions.length === 0 && filters.typeSelectedOptions.length === 0);
    }

    function updateSubmitButtonState() {
      if (_.xorBy(zoneSelectedOptions, filters.zoneSelectedOptions, 'id').length > 0 ||
        _.xorBy(typeSelectedOptions, filters.typeSelectedOptions, 'id').length > 0) {
        ctrl.isButtonDisabled = false;
      } else {
        ctrl.isButtonDisabled = true;
      }
    }
  }
}());
