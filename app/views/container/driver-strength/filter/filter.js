(function controller() {
  angular
    .module('nvOperatorApp.controllers')
    .controller('DriverStrengthFilterController', DriverStrengthFilterController);

  DriverStrengthFilterController.$inject = ['Driver', '$state', '$scope'];

  function DriverStrengthFilterController(Driver, $state, $scope) {
    const ctrl = this;
    const selected = {
      zones: [],
      types: [],
    };
    ctrl.selectedFilters = [];
    ctrl.possibleFilters = [{
      type: 2,
      mainTitle: 'Zones',
      possibleOptions: angular.copy($scope.filterZones),
      selectedOptions: selected.zones,
      searchText: {},
    }, {
      type: 2,
      mainTitle: 'Driver Types',
      possibleOptions: angular.copy($scope.filterDriverTypes),
      selectedOptions: selected.types,
      searchText: {},
    }];

    ctrl.loadSelection = loadSelection;
    ctrl.createOne = $scope.createOne;
    ctrl.isEmptyFilter = isEmptyFilter;

    function loadSelection() {
      const zones = _.map(selected.zones, zone => zone.id).join(',');
      const types = _.map(selected.types, type => type.id).join(',');
      $state.go('container.driver-strength.data', { type: types, zone: zones });
    }

    function isEmptyFilter() {
      return ctrl.selectedFilters.length === 0 ||
        (selected.zones.length === 0 && selected.types.length === 0);
    }
  }
}());
