(function controller() {
  angular.module('nvOperatorApp.controllers')
        .controller('SmsController', smsController);

  smsController.$inject = [
    'nvTable',
    'nvTranslate',
    'Sms',
    'nvToast',
    'UrlShortener',
    '$q',
    'nvDialog',
    'nvButtonFilePickerType',
  ];

  function smsController(
        nvTable,
        nvTranslate,
        Sms,
        nvToast,
        UrlShortener,
        $q,
        nvDialog,
        nvButtonFilePickerType
    ) {
    const ctrl = this;

    ctrl.tableParam = {};
    ctrl.trackingId = '';
    ctrl.state = 'none';
    ctrl.showPage = 'none';
    ctrl.sendSmsState = 'idle';
    ctrl.fields = [];
    ctrl.records = [];
    ctrl.failedUploads = [];
    ctrl.fileName = '';
    ctrl.editorHelpText = '0/240';
    ctrl.partialSuccess = false;
    ctrl.messageTemplate = '';
    ctrl.nvButtonFilePickerType = nvButtonFilePickerType;

    // functions
    ctrl.onLoadSmsHistory = onLoadSmsHistory;
    ctrl.onFileSelect = onFileSelect;
    ctrl.onFileReject = onFileReject;
    ctrl.onChangeEditor = onChangeEditor;
    ctrl.updatePreview = updatePreview;
    ctrl.sendSms = sendSms;
    ctrl.onContinuePartialSuccess = onContinuePartialSuccess;
    ctrl.onAddVariableToEditor = onAddVariableToEditor;

    const urlPattern = /(http(s)?:\/\/.)?(www\.)?[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)?/gi;
    const urlRegex = new RegExp(urlPattern);

    init();


    function init() {
    }

    function updatePreview() {
      const msg = angular.copy(ctrl.messageTemplate);
      const sampleData = angular.copy(ctrl.records[0]);
      const shortenPromises = [];
      const fieldsToShort = [];
      let needShorten = false;
      _.each(ctrl.fields, (field) => {
        if (field.shorten) {
          shortenPromises.push(
              UrlShortener.bulkShort(
                  createShortenUrlPayload(field.name, sampleData)
                )
            );
          fieldsToShort.push(field.name);
          needShorten = true;
        }
      });


      if (needShorten) {
        return $q.all(shortenPromises)
                .then((results) => {
                  _.each(results, (result, index) => {
                    sampleData[fieldsToShort[index]] = result[0].id;
                  });
                  processTemplate(msg, sampleData);
                });
      }
      return processTemplate(msg, sampleData);


      function processTemplate(template, data) {
        let result = angular.copy(template);
        _.each(data, (val, key) => {
          result = replaceAll(result, `{{${key}}}`, val);
        });
        ctrl.messagePreview = result;
      }
    }


    function onChangeEditor(text) {
      const length = _.trim(text).length;
      ctrl.editorHelpText = `${length}/240`;
    }

    function onAddVariableToEditor(variable) {
      const cursorPos = $('#message').prop('selectionStart');
      if (cursorPos > 0) {
        const currentText = angular.copy(ctrl.messageTemplate);
        const textBefore = currentText.substring(0, cursorPos);
        const textAfter = currentText.substring(cursorPos, currentText.length);
        ctrl.messageTemplate = `${textBefore} {{${variable}}} ${textAfter}`;
      } else {
        ctrl.messageTemplate = `{{${variable}}}`;
      }
    }

    function onFileReject() {
      nvToast.error('CSV Rejected');
    }

    function onContinuePartialSuccess() {
      ctrl.showPage = 'editor';
      ctrl.state = 'none';
    }
    function onFileSelect(selectedFile) {
      ctrl.fileName = selectedFile.name;
      ctrl.state = 'uploading';
      ctrl.showPage = 'none';
      ctrl.partialSuccess = false;
      ctrl.records = [];
      ctrl.fields = [];
      ctrl.failedUploads = [];
      return Sms.uploadCsv(selectedFile)
                .then(success, failed);

      function success(response) {
                // get the column from the first result
                // check the status code
        if (response.status === 207) {
          ctrl.partialSuccess = true;
          processResult(response.data);
          return nvDialog.showSingle(null, {
            templateUrl: 'views/container/sms/dialog/partial-failed-upload-csv.html',
            controller: 'SmsPartialFailedDialogControler',
            controllerAs: 'ctrl',
            cssClass: 'nv-partial-failed-upload-csv',
            theme: 'nvBlue',
            locals: {
              failedUploads: ctrl.failedUploads,
              allFailed: ctrl.records.length === 0,
            },
          }).then(onContinuePartialSuccess, resetPage);
        }
        nvToast.success(nvTranslate.instant('container.sms.success-upload-csv'));
        processResult(response.data);
        ctrl.showPage = 'editor';
        ctrl.state = 'none';
        return 0;
      }

      function processResult(results) {
        if (results.length > 0) {
          const data = results[0].data;
          ctrl.fields = _.map(data, (val, key) => ({
            name: key,
            isUrl: val.match(urlRegex) !== null,
          }));
        }
        ctrl.records = _.compact(_.map(results, (val) => {
          if (val.error) {
            const errorUpload = angular.copy(val.data);
            errorUpload.errorMsg = val.error.message;
            ctrl.failedUploads.push(errorUpload);
            return false;
          }
          return val.data;
        }));
      }

      function failed() {
        resetPage();
      }
    }

    function sendSms() {
      ctrl.sendSmsState = 'waiting';
            // check for shortened url
      const shortenPromises = [];
      const fieldsToShort = [];
      let hasUrlToShort = false;
      _.each(ctrl.fields, (val) => {
        if (val.shorten) {
          fieldsToShort.push(val.name);
          shortenPromises.push(UrlShortener.bulkShort(createShortenUrlPayload(val.name)));
          hasUrlToShort = true;
        }
      });

      if (hasUrlToShort) {
        return $q.all(shortenPromises).then((results) => {
                    // replace with shorten url
          _.each(results, (res, index) => {
            _.each(ctrl.records, (record, index2) => {
              record[fieldsToShort[index]] = res[index2].id;
            });
          });
          return sendSmsPayload();
        });
      }

      return sendSmsPayload();


      function sendSmsPayload() {
        const payload = {
          template: ctrl.messageTemplate,
          order_details: ctrl.records,
        };
        return Sms.sendSms(payload)
                    .then(success, failed);
      }

      function success(result) {
        ctrl.sendSmsState = 'idle';
        let count = 0;
        _.each(result, (res) => {
          if (!res.error) {
            count += 1;
          }
        });
        if (count === 1) {
          nvToast.info(nvTranslate.instant('container.sms.successfully-sent-1-sms'));
        } else {
          nvToast.info(nvTranslate.instant('container.sms.successfully-sent-x-smses', { x: count }));          
        }
        resetPage();
      }

      function failed() {
        nvToast.error(nvTranslate.instant('container.sms.failed-to-send-sms'));
      }
    }

    function createShortenUrlPayload(key, record) {
      if (record) {
        return [{
          longUrl: record[key],
        }];
      }
      return _.map(ctrl.records, val => ({
        longUrl: val[key],
      }));
    }

    function onLoadSmsHistory(trackingId) {
      return Sms.history(trackingId)
                .then(success, error);
      function success(result) {
        ctrl.tableParam = nvTable.createTable(Sms.fields);
        ctrl.tableParam.setData(_.orderBy(result, ['sentTime'], ['desc']));
        ctrl.showPage = 'history';
      }
      function error(err) {
        return nvToast.error(err.message);
      }
    }

    function resetPage() {
      ctrl.state = 'none';
      ctrl.showPage = 'none';
      ctrl.sendSmsState = 'idle';
      ctrl.fields = [];
      ctrl.records = [];
      ctrl.failedUploads = [];
      ctrl.fileName = '';
      ctrl.messageTemplate = '';
    }

    function replaceAll(source, find, replace) {
      return source.replace(new RegExp(find, 'g'), replace);
    }
  }
}());
