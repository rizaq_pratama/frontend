(function controller() {
  angular
    .module('nvOperatorApp.controllers')
    .controller('SmsPartialFailedDialogControler', Controller);

  Controller.$inject = [
    'failedUploads',
    'allFailed',
    'nvTable',
    '$mdDialog',
    'nvTranslate',
  ];

  function Controller(
    failedUploads,
    allFailed,
    nvTable,
    $mdDialog,
    nvTranslate
  ) {
    const ctrl = this;

    ctrl.fields = {
      tracking_id: { displayName: nvTranslate.instant('commons.tracking-id') },
      errorMsg: { displayName: nvTranslate.instant('container.sms.error-msg') },
    };

    ctrl.tableParam = {};
    ctrl.failedUploads = angular.copy(failedUploads);
    ctrl.allFailed = allFailed;
    ctrl.onCancel = onCancel;
    ctrl.onContinue = onContinue;

    function onCancel() {
      return $mdDialog.cancel();
    }

    function onContinue() {
      if (ctrl.allFailed) {
        return $mdDialog.cancel();
      }
      return $mdDialog.hide('ok');
    }

    init();
    function init() {
      ctrl.tableParam = nvTable.createTable(ctrl.fields);
      ctrl.tableParam.setData(ctrl.failedUploads);
    }
  }
}());
