(function RoleDialogEditController() {
  angular
        .module('nvOperatorApp.controllers')
        .controller('RoleDialogEditController', controller);
  controller.$inject = [
    '$q',
    '$mdDialog',
    'OauthGroup',
    'fields',
    'roleObj',
    'nvAutocomplete.Data',
    '$translate',
  ];

  function controller(
        $q,
        $mdDialog,
        OauthGroup,
        fields,
        roleObj,
        nvAutocompleteData,
        $translate
    ) {
    const ctrl = this;
    ctrl.id = roleObj.id;
    ctrl.selectedRole = {};
    ctrl.scopesCount = 0;
    ctrl.userCount = 0;
    ctrl.scopes = [];
    ctrl.onCancel = onCancel;
    ctrl.updateOne = updateOne;
    ctrl.forceLogout = forceLogoutRole;
    ctrl.deleteRole = deleteRole;
    ctrl.scopesSearchText = '';
    ctrl.removeScopes = [];
    ctrl.addScopes = [];
    ctrl.allScopes = [];
    ctrl.userTableData = [];
    ctrl.fields = fields;
    ctrl.serviceIds = [];
    ctrl.selectedServiceIds = [];
    ctrl.table = {
      scope: $translate.instant('container.role-management.dialogs.scope'),
      username: $translate.instant('commons.email-username'),
      country: $translate.instant('commons.country'),
      serialNo: $translate.instant('commons.sn'),
    };

    ctrl.tableFilter = {
      serviceIdNotSelected : $translate.instant('container.role-management.dialogs.filter.notselected'),
      serviceIdSelected : $translate.instant('container.role-management.dialogs.filter.selected')
    };
    ctrl.onLoadPage = onLoadPage;
    ctrl.onScopesSearch = onScopesSearch;
    ctrl.onUsersSearch = onUsersSearch;
    ctrl.scopeTableParams = null;
    ctrl.userTableParams = null;
    ctrl.removeScope = removeScope;
    ctrl.scopeSearch = nvAutocompleteData.getByHandle('role-edit-scopes');
    ctrl.scopeSearch.onSelect(addItemToSelectedOptions);
    ctrl.onServiceIdSelect = onServiceIdSelect;

    function onLoadPage() {
      $q.all(
          [OauthGroup.readOneByIdFilterByServiceId(ctrl.id, '').then(readOneByIdOnLoadSuccess),
            OauthGroup.readAllScopes().then(readAllScopesSuccess),
          OauthGroup.readScopesByGroupId(ctrl.id).then(readScopesByGroupIdOnLoadSuccess),
            OauthGroup.getServiceIds().then(readServiceIdsSuccess)]
      );

      function readScopesByGroupIdOnLoadSuccess(result) {
        ctrl.scopes = result;
        ctrl.scopeTableData = ctrl.scopes;
        ctrl.scopesCount = ctrl.scopes.length;
      }

      function readAllScopesSuccess(result) {
        ctrl.allScopes = result;
                // ctrl.filters.scopes.possibleOptions = result;
        ctrl.scopeSearch.setPossibleOptions(ctrl.allScopes);
      }

      function readServiceIdsSuccess(result) {
        ctrl.serviceIds = result;
      }
    }

    function readOneByIdOnLoadSuccess(result) {
      ctrl.selectedRole = result.group;
      ctrl.oauthClientsCount = result.oauthClientsCount;
      ctrl.userCount = result.oauthClientsCount;
      ctrl.users = result.countryOauthClientBeans;
      ctrl.userTableData = ctrl.users;
    }

    function onCancel() {
      $mdDialog.cancel();
    }

    function deleteRole() {
      return OauthGroup.deleteRole(ctrl.id).then(success);
    }

    function forceLogoutRole() {
      const data = {};
      data.groupId = ctrl.id;
      return OauthGroup.forceLogoutRole(data).then(success);
    }

    function updateOne() {
      const addScopeIds = ctrl.scopes.map(o =>
         o.id
      );
      const data = {
        id: ctrl.selectedRole.id,
        name: ctrl.selectedRole.name,
        description: ctrl.selectedRole.description,
        add: addScopeIds,
        remove: ctrl.removeScopes,
      };
      return OauthGroup.updateOne(ctrl.id, data).then(success, failed);
    }

    function success(result) {
      $mdDialog.hide();
      ctrl.selectedUser = result;
    }

    function addItemToSelectedOptions(item) {
      if (item !== undefined) {
        ctrl.scopes.push(item);
      }
    }

    function failed(error) {
      if (error) {
        ctrl.errorMessage = error.data.errorMessage;
      } else {
        ctrl.errorMessage = 'unknown error on add role';
      }
    }

    function removeScope(scope) {
      const scopeToPutBack = _.filter(ctrl.scopeSearch.getSelectedOptions(), { 'name': scope.name });
      if (scopeToPutBack && scopeToPutBack.length > 0) {
        ctrl.scopeSearch.remove(scopeToPutBack[0]);
      }
      _.pull(ctrl.scopes, scope);
      ctrl.removeScopes.push(scope.id);
    }

    function onScopesSearch(scopesSearchText) {
      if (scopesSearchText !== undefined && scopesSearchText !== '') {
        ctrl.scopeTableData = _.filter(ctrl.scopes, o =>
          _.includes(o.name.toLowerCase(), scopesSearchText.trim().toLowerCase())
        );
      } else {
        ctrl.scopeTableData = ctrl.scopes;
      }
    }

    function onUsersSearch(userSearchText) {
      if (userSearchText !== undefined && userSearchText !== '') {
        ctrl.userTableData = _.filter(ctrl.users, o =>
        _.includes(o.displayName.toLowerCase(), userSearchText.trim().toLowerCase())
      );
      } else {
        ctrl.userTableData = ctrl.users;
      }
    }

    function onServiceIdSelect(serviceId) {
      // if((ctrl.selectedServiceIds[serviceId])
      if (_.indexOf(ctrl.selectedServiceIds, serviceId) >= 0) {
        _.pull(ctrl.selectedServiceIds, serviceId);
      } else {
        ctrl.selectedServiceIds.push(serviceId);
      }
      OauthGroup.readOneByIdFilterByServiceId(ctrl.id, ctrl.selectedServiceIds.join())
          .then(readOneByIdOnLoadSuccess);
    }
  }
}());
