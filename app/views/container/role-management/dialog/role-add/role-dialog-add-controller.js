(function RoleDialogAddController() {
  angular
        .module('nvOperatorApp.controllers')
        .controller('RoleDialogAddController', controller);
  controller.$inject = [
    '$mdDialog',
    'OauthGroup',
    'fields',
    'nvAutocomplete.Data',
    '$translate',
  ];

  function controller(
        $mdDialog,
        OauthGroup,
        fields,
        nvAutocompleteData,
        $translate
    ) {
    const ctrl = this;
    ctrl.scopes = [];
    ctrl.onCancel = onCancel;
    ctrl.createOne = createOne;
    ctrl.addScopes = [];
    ctrl.allScopes = [];
    ctrl.scopeTableData = [];
    ctrl.fields = fields;
    ctrl.newRole = {};
    ctrl.searchText = '';
    ctrl.table = {
      scope: $translate.instant('container.role-management.dialogs.scope'),
      serialNo: $translate.instant('commons.sn'),
    };
    ctrl.onLoadPage = onLoadPage;
    ctrl.scopeSearch = nvAutocompleteData.getByHandle('role-add-scopes');
    ctrl.scopeSearch.onSelect(addItemToSelectedOptions);
    ctrl.removeScope = removeScope;
    ctrl.onScopesSearch = onScopesSearch;

    function onLoadPage() {
      return OauthGroup.readAllScopes().then((result) => {
        ctrl.allScopes = result;
        ctrl.scopeSearch.setPossibleOptions(ctrl.allScopes);
      });
    }

    function onCancel() {
      $mdDialog.cancel();
    }

    function createOne() {
      const addScopeIds = ctrl.scopes.map(o =>
         o.id
      );
      ctrl.newRole.add = addScopeIds;
      return OauthGroup.createOne(ctrl.newRole).then(success, failed);
    }

    function success() {
      $mdDialog.hide();
    }

    function failed(error) {
      if (error) {
        ctrl.errorMessage = error.data.errorMessage;
      } else {
        ctrl.errorMessage = 'unknown error on add role';
      }
    }

    function addItemToSelectedOptions(item) {
      if (!_.isUndefined(item)) {
        ctrl.scopes.push(item);
      }
    }


    function removeScope(scope) {
      const scopeToPutBack = _.filter(ctrl.scopeSearch.getSelectedOptions(), { 'name': scope.name });
      if (scopeToPutBack && scopeToPutBack.length > 0) {
        ctrl.scopeSearch.remove(scopeToPutBack[0]);
      }
      _.pull(ctrl.scopes, scope);
    }

    function onScopesSearch(scopesSearchText) {
      if (scopesSearchText !== undefined && scopesSearchText !== '') {
        ctrl.scopeTableData = _.filter(ctrl.scopes, o =>
          _.includes(o.name.toLowerCase(), scopesSearchText.trim().toLowerCase())
      );
      } else {
        ctrl.scopeTableData = ctrl.scopes;
      }
    }
  }
}());
