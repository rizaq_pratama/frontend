(function RoleManagementController() {
  angular
        .module('nvOperatorApp.controllers')
        .controller('RoleManagementController', controller);

  controller.$inject = [
    'OauthGroup',
    '$mdDialog',
    'NgTableParams',
    'nvTranslate',
  ];

  function controller(
        OauthGroup,
        $mdDialog,
        NgTableParams,
        nvTranslate
    ) {
    const ctrl = this;
    ctrl.allUserGroups = [];
    ctrl.allRoleParams = null;
    ctrl.loadingSheet = true;
    ctrl.table = {};
        // functions
    ctrl.load = load;
    ctrl.createOne = createOne;
    ctrl.updateOne = updateOne;
    ctrl.onSearch = onSearch;

    function load() {
      ctrl.loadingSheet = true;
      OauthGroup.readAllWithScopesCountAndGroupsCount().then((body) => {
        ctrl.allUserGroups = _.sortBy(body, 'id');
        ctrl.allRoleParams = new NgTableParams(
                    { count: 100 },
                    { counts: [], dataset: ctrl.allUserGroups }
                );
        ctrl.table = {
          id: nvTranslate.instant('commons.id'),
          name: nvTranslate.instant('commons.name'),
          numberOfScopes: nvTranslate.instant('container.role-management.noOfScopes'),
          numberOfUsers: nvTranslate.instant('container.role-management.noOfUsers'),
          description: nvTranslate.instant('commons.description'),
          actions: nvTranslate.instant('commons.actions'),
          serialNo: nvTranslate.instant('commons.sn'),
        };
        ctrl.loadingSheet = false;
      });
    }

    function createOne($event) {
      const fields = OauthGroup.getAddFields();

      $mdDialog.show({
        templateUrl: 'views/container/role-management/dialog/role-add/role-add.dialog.html',
        locals: {
          fields: fields,
        },
        bindToController: true,
        controllerAs: 'ctrl',
        controller: 'RoleDialogAddController',
        targetEvent: $event,
      }).then(success);
    }

    function updateOne($event, obj) {
      const fields = OauthGroup.getAddFields();


      $mdDialog.show({
        templateUrl: 'views/container/role-management/dialog/role-edit/role-edit.dialog.html',
        locals: {
          fields: fields,
          roleObj: obj,
        },
        bindToController: true,
        controllerAs: 'ctrl',
        controller: 'RoleDialogEditController',
        targetEvent: $event,
      }).then(success);
    }

    function success() {
      ctrl.load();
    }
    function onSearch(searchText) {
      if (ctrl.allRoleParams) {
        ctrl.allRoleParams.filter({ $: searchText });
      }
    }
  }
}());
