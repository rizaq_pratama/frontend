(function controller() {
  angular
    .module('nvOperatorApp.controllers')
    .controller('AddressVerificationController', AddressVerificationController);

  function AddressVerificationController() {
    const ctrl = this;
    ctrl.url = `${nv.config.servers['operator-react']}/address-verification`;
  }
}());

