(function controller() {
  angular
    .module('nvOperatorApp.controllers')
    .controller('AddParcelToRouteController', AddParcelToRouteController);

  AddParcelToRouteController.$inject = [
    '$q',
    '$timeout',
    'nvToast',
    'nvTableUtils',
    'Route',
    'RouteGroup',
    'Tag',
    'nvDateTimeUtils',
  ];

  function AddParcelToRouteController(
    $q,
    $timeout,
    nvToast,
    nvTableUtils,
    Route,
    RouteGroup,
    Tag,
    nvDateTimeUtils
  ) {
    const ctrl = this;
    const ENABLED_BY_DEFAULT = { FLT: true };
    const SUBMIT_BUTTON_STATE = { IDLE: 'idle', WAITING: 'waiting' };

    ctrl.routeGroupOptions = [];
    ctrl.routeGroupSelections = [];
    ctrl.tags = [];
    ctrl.routeDate = new Date();

    ctrl.submitButtonState = SUBMIT_BUTTON_STATE.IDLE;
    ctrl.isSubmitButtonDisabled = true;
    ctrl.isSubmitSuccess = false;
    ctrl.submissionResults = [];
    ctrl.tableParams = null;

    ctrl.getData = getData;
    ctrl.onChange = onChange;
    ctrl.onSubmit = onSubmit;
    ctrl.onSearch = onSearch;

    function getData() {
      return $q
        .all([
          RouteGroup.read(),
          Tag.all(),
        ])
        .then(success, $q.reject);

      function success(response) {
        ctrl.routeGroupOptions = RouteGroup.toOptions(response[0].routeGroups);
        ctrl.tags = Tag.setValues(response[1], ENABLED_BY_DEFAULT);
      }
    }

    function onChange() {
      $timeout(() => {
        const isAnyRGSelected = (ctrl.routeGroupSelections || []).length > 0;
        const isAnyTGSelected = _.some(ctrl.tags, tag => tag._value);
        ctrl.isSubmitButtonDisabled = !isAnyRGSelected || !isAnyTGSelected;
      });
    }

    function onSubmit() {
      const data = {
        route_group_ids: ctrl.routeGroupSelections || [],
        tags: _(ctrl.tags).filter(tag => tag._value).map(tag => tag.id).value(),
        route_date: nvDateTimeUtils.displayDateTime(
          nvDateTimeUtils.toMoment(ctrl.routeDate).startOf('day'), 'UTC'
        ),
      };
      ctrl.submitButtonState = SUBMIT_BUTTON_STATE.WAITING;
      Route.addByRouteGroup(data).then(success, failure);

      function success(response) {
        ctrl.isSubmitSuccess = true;
        ctrl.submissionResults = _.map(response, mapSubmissionResults);
        ctrl.tableParams = nvTableUtils.generate(ctrl.submissionResults, {
          columns: ['driver_name', 'route_id', 'order_id', 'tracking_id'],
        });
        ctrl.submitButtonState = SUBMIT_BUTTON_STATE.IDLE;
        nvToast.success('Submitted successfully');
      }

      function failure() {
        ctrl.submitButtonState = SUBMIT_BUTTON_STATE.IDLE;
      }
    }

    function onSearch(searchText) {
      ctrl.tableParams.filter({ $: searchText });
    }

    function mapSubmissionResults(data) {
      const defaultValues = { driver_name: '-', order_id: '-', route_id: '-' };
      switch (data.type) {
        case 'reservation':
          return _.defaults(data, defaultValues, { tracking_id: 'Reservation' });
        case 'transaction':
        default:
          return _.defaults(data, defaultValues, { tracking_id: '-' });
      }
    }
  }
}());
