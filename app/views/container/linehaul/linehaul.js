(function Controller() {
  angular
        .module('nvOperatorApp.controllers')
        .controller('LinehaulController', controller)
    ;

  controller.$inject = [
    '$q', '$scope', '$timeout', 'Linehaul',
    'nvDialog', '$mdDialog', '$rootScope', 'nvToast', '$state', '$location',
  ];

  function controller($q, $scope, $timeout, Linehaul, nvDialog,
                        $mdDialog, $rootScope, nvToast, $state, $location) {
    const ctrl = this;
    ctrl.currentTab = 0;
    ctrl.gotoPage = gotoPage;
    ctrl.currentPage = 'container.linehaul.entries';
    ctrl.linehaulMenu = [{
      displayName: 'Linehaul Entries',
      page: 'container.linehaul.entries',
    }, {
      displayName: 'Linehaul Date',
      page: 'container.linehaul.date',
    }];
    load();

    $scope.$watch(() => {
      return $location.path();
    }, (value) => {
      if (value && value.endsWith('/linehaul')) {
        load();
      }
    });


    function gotoPage(page) {
      $state.go(page);
      ctrl.currentPage = page;
    }

    function load() {
      gotoPage(ctrl.currentPage);
    }
  }
}());
