(function Controller() {
  angular
        .module('nvOperatorApp.controllers')
        .controller('LinehaulEntriesController', controller)
    ;

  controller.$inject = [
    '$q', '$timeout', 'Linehaul', 'Shipping',
    'nvDialog', 'nvBackendFilterUtils', 'LinehaulFilterTemplate',
    'nvEditModelDialog', 'nvToast', 'nvTable',
    '$filter', '$rootScope',

  ];

  function controller($q, $timeout, Linehaul,
                        Shipping, nvDialog, nvBackendFilterUtils,
                        LinehaulFilterTemplate, nvEditModelDialog,
                        nvToast, nvTable, $filter,
                        $rootScope) {
    const ctrl = this;
    const SEARCH_STATE = {
      WAITING: 'waiting',
      IDLE: 'idle',
    };
    ctrl.showFilter = true;
    ctrl.linehauls = [];
    ctrl.linehaulsRef = [];
    ctrl.filterText = '';
    ctrl.selectSortBy = 'id';
    ctrl.page = 1;
    ctrl.pageSize = 10;
    ctrl.timezone = $rootScope.user.timezone;
    ctrl.totalItems = Number.MAX_VALUE;
    ctrl.searchState = SEARCH_STATE.IDLE;


    ctrl.onLoad = onLoad;
    ctrl.searchLinehaul = searchLinehaulPromise;
    ctrl.search = search;
    ctrl.onCreate = onCreate;
    ctrl.onUpdate = onUpdate;
    ctrl.showFilterPage = showFilterPage;
    ctrl.savePreset = savePreset;
    ctrl.deletePreset = deletePreset;
    ctrl.updatePreset = updatePreset;
    ctrl.loadMore = loadMore;
    ctrl.deleteOne = deleteOne;


        // ///////////////////////
        // FILTER
        // //////////////////////

    ctrl.selectedFilters = [];
    ctrl.possibleFilters = [];
    ctrl.filterPresets = [];
    ctrl.selectedPreset = null;

        // ///////////////////////
        // TABLE
        // /////////////////////
    ctrl.tableParam = {};
    ctrl.page = 1;
    ctrl.itemsPerPage = nvTable.DEFAULT_BUFFER_SIZE;
    ctrl.totalItems = Number.MAX_VALUE;


        // /////////////////
        // FUNCTION BODIES
        // /////////////////
    function onLoad() {
      return $q.all([
        Shipping.getHubs(),
        LinehaulFilterTemplate.getFilter(),
      ]).then(onSuccess);
      function onSuccess(result) {
        ctrl.hubs = _.map(result[0].hubs, h => (
          {
            id: h.id,
            displayName: h.name,
            country: h.country,
            lowercaseName: h.name.toLowerCase(),
            hubValue: h,
          }
                ));

        ctrl.filterPresets = result[1].templates;
        ctrl.possibleFilters = [
          {
            type: nvBackendFilterUtils.FILTER_OPTIONS,
            mainTitle: 'Frequency',
            possibleOptions: angular.copy(Linehaul.getFrequencyOptions()),
            selectedOptions: [],
            searchBy: 'displayName',
            searchText: {},
            key: 'frequency',
            presetKey: 'frequency',
            transformFn: Linehaul.mapToFrequencyRequest,
            comparator: comparatorFrequency,
          },
          {
            type: nvBackendFilterUtils.FILTER_OPTIONS,
            mainTitle: 'Day of Week',
            possibleOptions: angular.copy(Linehaul.getDayOptions()),
            selectedOptions: [],
            searchBy: 'displayName',
            searchText: {},
            key: 'day_of_week',
            presetKey: 'day_of_week',
            transformFn: Linehaul.mapToDayOfWeekRequest,
            comparator: comparatorDayOfWeek,
          },
          {
            type: nvBackendFilterUtils.FILTER_OPTIONS,
            mainTitle: 'Start Hub',
            possibleOptions: angular.copy(ctrl.hubs),
            selectedOptions: [],
            searchBy: 'displayName',
            searchText: {},
            key: 'start_hub',
            presetKey: 'start_hub',
            transformFn: Linehaul.mapToHubRequest,
            comparator: comparatorHub,
          },
          {
            type: nvBackendFilterUtils.FILTER_OPTIONS,
            mainTitle: 'Transit Hub',
            possibleOptions: angular.copy(ctrl.hubs),
            selectedOptions: [],
            searchBy: 'displayName',
            searchText: {},
            key: 'transit_hub',
            presetKey: 'transit_hub',
            transformFn: Linehaul.mapToHubRequest,
            comparator: comparatorHub,
          },
          {
            type: nvBackendFilterUtils.FILTER_OPTIONS,
            mainTitle: 'End Hub',
            possibleOptions: angular.copy(ctrl.hubs),
            selectedOptions: [],
            searchBy: 'displayName',
            searchText: {},
            key: 'end_hub',
            presetKey: 'end_hub',
            transformFn: Linehaul.mapToHubRequest,
            comparator: comparatorHub,
          },
        ];
      }
    }

    function isFilterNonEmpty() {
      return ctrl.selectedFilters.length > 0;
    }

    function showFilterPage() {
      ctrl.showFilter = true;
      ctrl.page = 1;
    }

    function searchLinehaulPromise() {
      if (isFilterNonEmpty()) {
        const params = nvBackendFilterUtils.constructParams(ctrl.selectedFilters);
        return Linehaul.searchPage(params, ctrl.page, ctrl.pageSize);
      }
      return Linehaul.searchPage({}, ctrl.page, ctrl.pageSize);
    }

    function loadMore() {
      if (ctrl.state !== 'loading') {
        ctrl.loadState = 'loading';
        return ctrl.tableParam.mergeInPromise(loadMoreLinehaul(), 'id');
      }
      return null;

      function loadMoreLinehaul(){
        if(ctrl.tableParam.getUnfilteredLength() < ctrl.totalItems){
          return searchLinehaulPromise().then(linehaulExtend);
        }
        return $q.reject();
      }

      function linehaulExtend(result){
        if(result.count > 0){
          const sortedLinehaul = _.orderBy(result.linehauls, ['id'], ['asc']);
          const linehauls = transformLinehaul(sortedLinehaul);
          ctrl.page += 1;
          return linehauls;
        }
        return $q.reject();
      }
    }



    function extend(result) {
      var linehauls =[];
      if (result.linehauls.length > 0) {
        const sortedLinehaul = _.orderBy(result.linehauls, ['id'], ['asc']);
        linehauls = transformLinehaul(sortedLinehaul);
        ctrl.page += 1;
        ctrl.totalItems = result.count;
      }
      return linehauls;
    }


    function search() {
      ctrl.searchState = SEARCH_STATE.WAITING;
      ctrl.tableParam = nvTable.createTable(Linehaul.TABLE_FIELDS);
      return searchLinehaulPromise().then(success);

      function success(result) {
        ctrl.searchState = SEARCH_STATE.IDLE;
        ctrl.tableParam.setData(extend(result));
        ctrl.showFilter = false;
      }
    }

    function transformHubObjToString(hubObj) {
      return `${hubObj.hub_country}-${hubObj.hub_name}`;
    }

    function transformLinehaul(linehauls) {
      return _.map(linehauls, (o) => {
        o.startHub = transformHubObjToString(_.head(o.linehaul_legs));
        o.endHub = transformHubObjToString(_.last(o.linehaul_legs));
        o.transitHub = _.chain(o.linehaul_legs)
                    .slice(1, o.linehaul_legs.length - 1)
                    .map(m => (`${m.hub_country}-${m.hub_name}`))
                    .join(', ')
                    .value();
        o.daysOfWeek = _.join(o.days_of_week, ', ');
        o.comments_f = o.comments ? $filter('limitTo')(o.comments, 50) : '-';
        return o;
      });
    }


    function onCreate($event) {
      nvDialog.showSingle($event, {
        templateUrl: 'views/container/linehaul/tabs/linehaul-entries/dialogs/linehaul-entries.dialog.html',
        locals: {
          mode: 'create',
          linehaul: null,
          hubs: ctrl.hubs,
        },
        theme: 'nvGreen',
        cssClass: 'linehaul-add',
        controllerAs: 'ctrl',
        controller: 'LinehaulEntriesDialogController',
      }).then(success);
      function success(result) {
        const linehaul = transformLinehaul(_.castArray(result));
        if (ctrl.tableParam && ctrl.tableParam.ready) {
          ctrl.tableParam.mergeIn(linehaul, 'id');
        }
      }
    }

    function onUpdate($event, linehaul) {
      nvDialog.showSingle($event, {
        templateUrl: 'views/container/linehaul/tabs/linehaul-entries/dialogs/linehaul-entries.dialog.html',
        locals: {
          mode: 'edit',
          linehaul: linehaul,
          hubs: ctrl.hubs,
        },
        theme: 'nvGreen',
        cssClass: 'linehaul-add',
        controllerAs: 'ctrl',
        controller: 'LinehaulEntriesDialogController',
      }).then(success);
      function success(result) {
        const linehaulUpdated = transformLinehaul(_.castArray(result));
        if (ctrl.tableParam && ctrl.tableParam.ready) {
          ctrl.tableParam.mergeIn(linehaulUpdated, 'id');
        }
      }
    }

        /**
         *
         * @param $event
         * @param linehaul
         */
    function deleteOne($event, linehaul) {
            // show delete confirmation
      const linehaulId = linehaul.id;
      nvDialog.confirmDelete($event, {
        content: `Are you sure you want to permanently delete Linehaul ID ${linehaulId}?`,
      }).then(onDelete);

      function onDelete() {
        return Linehaul.deleteLinehaul(linehaulId)
                    .then(success);
      }

      function success() {
                // add new item to table
        ctrl.searchLinehaul();
        nvToast.info(`Success delete Linehaul ID ${linehaulId}`);
        ctrl.tableParam.remove(linehaul, 'id');
      }
    }

    

        /**
         *
         * @param data
         * @returns {*}
         */
    function savePreset(data) {
      return LinehaulFilterTemplate.create(data).then(response => response.template);
    }

        /**
         * Show a dialog window to let user delete a filter
         * @param data
         */
    function deletePreset(id) {
      return LinehaulFilterTemplate.deleteFilter(id);
    }

        /**
         * Update current preset
         * @returns {*}
         */
    function updatePreset(id, body) {
      return LinehaulFilterTemplate.update(id, body).then(response => response.template);
    }


        /**
         * Comparator function for frequency
         * @param object
         * @param filterObj
         * @returns {boolean}
         */
    function comparatorFrequency(object, filterObj) {
      return object.value === filterObj;
    }

        /**
         * Comparator function for day of week
         * @param object
         * @param filterObj
         * @returns {boolean}
         */
    function comparatorDayOfWeek(object, filterObj) {
      return object.value === filterObj;
    }

        /**
         *  Comparator function for hub
         * @param object
         * @param filterObj
         * @returns {boolean}
         */
    function comparatorHub(object, filterObj) {
      return filterObj.country === object.country && filterObj.id === object.id;
    }
  }
}());
