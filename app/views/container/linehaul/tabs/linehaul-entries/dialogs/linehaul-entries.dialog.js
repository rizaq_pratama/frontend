(function controller() {
  angular
        .module('nvOperatorApp.controllers')
        .controller('LinehaulEntriesDialogController', LinehaulEntriesDialogController);
  LinehaulEntriesDialogController.$inject = [
    'Linehaul', '$mdDialog', 'mode', 'linehaul', 'hubs', 'nvToast', '$translate', '$q',
  ];
  function LinehaulEntriesDialogController(Linehaul, $mdDialog, mode,
                                           linehaul, hubs, nvToast, $translate, $q) {
    const ctrl = this;
    ctrl.selectedHubs = [{
      state: 'init',
    }, {
      state: 'init',
    }]; // default to two hubs
    ctrl.isLast = isLast;
    ctrl.isFirst = isFirst;
    ctrl.onCancel = onCancel;
    ctrl.moveUp = moveUp;
    ctrl.moveDown = moveDown;
    ctrl.removeItem = removeItem;
    ctrl.addHubLeg = addHubLeg;
    ctrl.onSave = onSave;
    ctrl.onUpdate = onUpdate;
    ctrl.onDelete = onDelete;
    ctrl.hubs = hubs;
    ctrl.mode = mode;
    ctrl.onLoad = onLoad;
    ctrl.frequencyOptions = Linehaul.getFrequencyOptions();
    ctrl.daysOptions = Linehaul.getDayOptions();
    ctrl.createState = 'idle';


    // functions for hub list
    function isLast(idx) {
      return idx === ctrl.selectedHubs.length - 1;
    }
    function isFirst(idx) {
      return idx === 0;
    }
    function moveUp(idx) {
            // get the element
      const movee = ctrl.selectedHubs[idx];
      const moved = ctrl.selectedHubs[idx - 1];
      ctrl.selectedHubs[idx - 1] = movee;
      ctrl.selectedHubs[idx] = moved;
    }

    function moveDown(idx) {
            // get the element
      const movee = ctrl.selectedHubs[idx];
      const moved = ctrl.selectedHubs[idx + 1];
      ctrl.selectedHubs[idx + 1] = movee;
      ctrl.selectedHubs[idx] = moved;
    }

    function removeItem(idx) {
      ctrl.selectedHubs.splice(idx, 1);
    }

    function addHubLeg() {
      ctrl.selectedHubs.push({ state: 'new' });
    }
    // end of function for hub list


    function onSave() {
      ctrl.createState = 'waiting';
      let createdLinehaul;
      if (ctrl.selectedHubs.length < 2) {
        nvToast.error($translate.instant('container.linehaul.entries.linehaul-2-legs-warning'));
        return $q.reject();
      }
      const payload = {
        linehaul: {
          name: ctrl.linehaul.name,
          days_of_week: (ctrl.linehaul.daysOfWeek instanceof Array) ?
                     ctrl.linehaul.daysOfWeek : [ctrl.linehaul.daysOfWeek],
          frequency: ctrl.linehaul.frequency,
          comments: ctrl.linehaul.comments,
        },
      };

      return Linehaul.createLinehaul(payload)
                .then(onCreateLinehaulResponse)
                .then(onCreateLinehaulLegResponse);

      function onCreateLinehaulResponse(result) {
        createdLinehaul = result.linehaul;
        const linehaulLeg = _.map(ctrl.selectedHubs, o => ({
          hub_country: o.hub.country,
          hub_id: o.hub.id,
          from_datetime_offset: '0d 00:00',
          to_datetime_offset: '0d 00:00',
        })
        );
        const legPayload = {
          legs: linehaulLeg,
        };
        return Linehaul.updateLinehaulLeg(createdLinehaul.id, legPayload);
      }

      function onCreateLinehaulLegResponse(result) {
        ctrl.createState = 'idle';
        nvToast.success(`Linehaul ${createdLinehaul.id} created`);
        createdLinehaul.linehaul_legs = result.legs; // append linehaul leg name
        $mdDialog.hide(createdLinehaul);
      }
    }

    function onUpdate() {
      ctrl.createState = 'waiting';
      let savedLinehaul;
      if (ctrl.selectedHubs.length < 2) {
        nvToast.error($translate.instant('container.linehaul.entries.linehaul-2-legs-warning'));
        return $q.reject();
      }

      const payload = {
        linehaul: {
          name: ctrl.linehaul.name,
          days_of_week: (ctrl.linehaul.daysOfWeek instanceof Array) ?
                        ctrl.linehaul.daysOfWeek : [ctrl.linehaul.daysOfWeek],
          frequency: ctrl.linehaul.frequency,
          comments: ctrl.linehaul.comments,
        },
      };

      return Linehaul.updateLinehaul(ctrl.linehaul.id, payload)
                .then(onCreateLinehaulResponse)
                .then(onCreateLinehaulLegResponse);

      function onCreateLinehaulResponse(result) {
        savedLinehaul = result.linehaul;
        const legs = _.map(ctrl.selectedHubs, o => ({
          hub_country: o.hub.country,
          hub_id: o.hub.id,
          from_datetime_offset: '0d 00:00',
          to_datetime_offset: '0d 00:00',
        }));
        const legPayload = {
          legs: legs,
        };
        return Linehaul.updateLinehaulLeg(savedLinehaul.id, legPayload);
      }

      function onCreateLinehaulLegResponse(result) {
        ctrl.createState = 'idle';
        nvToast.success(`Linehaul ${savedLinehaul.id} updated`);
        savedLinehaul.linehaul_legs = result.legs;
        $mdDialog.hide(savedLinehaul);
      }
    }

    function onDelete() {
      const deletedLinehaulId = ctrl.linehaul.id;
      const linehaulObj = ctrl.linehaul;
      return Linehaul.deleteLinehaul(ctrl.linehaul.id)
                .then(onSuccessDelete);
      function onSuccessDelete() {
        nvToast.success(`Linehaul ${deletedLinehaulId} deleted`);
        $mdDialog.hide(linehaulObj);
      }
    }

    function onCancel() {
      $mdDialog.cancel();
    }
    function onLoad() {
      // check mode
      if (ctrl.mode === 'edit') {
        // try to reconstruct
        ctrl.linehaul = linehaul;
        const linehaulLegs = ctrl.linehaul.linehaul_legs;
        ctrl.selectedHubs.length = 0;
        _.forEach(linehaulLegs, (o) => {
          const foundHub = _.find(ctrl.hubs, m => (
          o.hub_id === m.id && o.hub_country === m.country
          ));
          if (foundHub) {
            const sHub = {
              state: 'init',
              hub: foundHub.hubValue,
            };
            ctrl.selectedHubs.push(sHub);
          }
        });
        // special case for nv-select multiple
        if (ctrl.linehaul.days_of_week.length === 1) {
          ctrl.linehaul.daysOfWeek = ctrl.linehaul.days_of_week[0];
        } else {
          ctrl.linehaul.daysOfWeek = ctrl.linehaul.days_of_week;
        }
      }
    }

    onLoad();
  }
}());
