(function Controller() {
  angular
        .module('nvOperatorApp.controllers')
        .controller('LinehaulDateController', controller)
    ;

  controller.$inject = [
    '$q', 'Linehaul', 'Shipping',
    'nvDialog', 'nvCalendar.Calendar', 'nvCalendar.Data', 'nvDateTimeUtils', 'nvToast',
  ];

  function controller($q, Linehaul, Shipping, nvDialog,
                      nvCalendar, nvCalendarData, nvDateTimeUtils, nvToast) {
    const ctrl = this;
    ctrl.onLoad = onLoad;
    ctrl.onCreate = onCreate;
    ctrl.onDisable = onDisable;
    ctrl.onEdit = onEdit;

    ctrl.hubs = [];
    ctrl.selectedLinehaulDate = null;
    ctrl.linehaulsRequestPromise = null;
    ctrl.linehaulsDate = [];


    ctrl.calendar = nvCalendar.getByHandle('linehaul');
    ctrl.onChangeMonth = onChangeMonth;
    ctrl.onDayClicked = onDayClicked;

    ctrl.calendarData = nvCalendarData.getByHandle('linehaul');
    ctrl.calendarData.setDayContent(calendarGetContent);

    const DATE_FORMAT = 'YYYY-MM-DD';


    function onLoad() {
      $q.all([Linehaul.getAllLinehaul(), Shipping.getHubs()])
                .then(onSuccess);
      function onSuccess(result) {
        ctrl.linehauls = result[0].linehauls;
        ctrl.hubs = _.map(result[1].hubs, h => (
          {
            id: h.id,
            displayName: h.name,
            country: h.country,
            lowercaseName: h.name.toLowerCase(),
            hubValue: h,
          }
        ));
      }
    }

    function buildLinehaulSchedule(linehauls, linehaulsSchedule) {
      ctrl.linehaulsDate = [];
      _.forEach(linehaulsSchedule, (o) => {
        const linehaul = _.find(linehauls, { id: o.id });
        if (linehaul) {
          _.forEach(o.scheduled_dates, (sch) => {
            if (ctrl.linehaulsDate[sch]) {
              ctrl.linehaulsDate[sch] = _.concat(ctrl.linehaulsDate[sch], linehaul);
            } else {
              ctrl.linehaulsDate[sch] = [linehaul];
            }
          });
        }
      });
      ctrl.calendar = nvCalendar.getByHandle('linehaul');
      ctrl.onChangeMonth = onChangeMonth;
      ctrl.onDayClicked = onDayClicked;

      ctrl.calendarData = nvCalendarData.getByHandle('linehaul');
      ctrl.calendarData.setDayContent(calendarGetContent);
    }


    function onChangeMonth() {
      const startDate = ctrl.calendar.getFirstDayInCalendar().format(DATE_FORMAT);
      const endDate = ctrl.calendar.getLastDayInCalendar().format(DATE_FORMAT);
      ctrl.linehaulsRequestPromise = Linehaul.linehaulsSchedule(startDate, endDate)
          .then(onSuccess);
      return ctrl.linehaulsRequestPromise;
      function onSuccess(result) {
        buildLinehaulSchedule(ctrl.linehauls, result.linehauls);
        ctrl.linehaulsRequestPromise = null;
      }
    }
    function onDayClicked(date) {
      ctrl.selectedLinehaulDate = date.format('DD MMMM YYYY (dddd)');
      const dateStringKey = date.format(DATE_FORMAT);
      if (ctrl.linehaulsDate) {
        ctrl.linehaulsForDate = _.map(ctrl.linehaulsDate[dateStringKey], (o) => {
          o.linehaulLegsString = _.chain(o.linehaul_legs)
                          .map(m => (`${m.hub_country}-${m.hub_name}`))
                          .join(' to ')
                          .value();
          return o;
        });
      }
    }

    function calendarGetContent(date) {
      if (_.isNull(ctrl.linehaulsRequestPromise)) {
        return getContent(date);
      }
      return ctrl.linehaulsRequestPromise
          .then(_.partial(getContent, date));

      function getContent(dateCell) {
        let data = {};
        const dateStringKey = dateCell.format(DATE_FORMAT);

        const linehaulsForDate = ctrl.linehaulsDate[dateStringKey];
        let linehaulsList = [];
        if (linehaulsForDate && linehaulsForDate.length > 0) {
          linehaulsList = _.map(linehaulsForDate, d => (d.name));
          data = {
            date: dateCell,
            linehauls: linehaulsList,
          };
        }
        return data;
      }
    }

    function onDisable() {
      nvToast.info('Not implemented');
    }

    function onEdit(linehaul, $event) {
      nvDialog.showSingle($event, {
        templateUrl:
            'views/container/linehaul/tabs/linehaul-entries/dialogs/linehaul-entries.dialog.html',
        locals: {
          mode: 'edit',
          linehaul: linehaul,
          hubs: ctrl.hubs,
        },
        theme: 'nvGreen',
        cssClass: 'linehaul-add',
        controllerAs: 'ctrl',
        controller: 'LinehaulEntriesDialogController',
      });
    }
    function onCreate($event) {
      nvDialog.showSingle($event, {
        templateUrl:
                    'views/container/linehaul/tabs/linehaul-entries/dialogs/linehaul-entries.dialog.html',
        locals: {
          mode: 'create',
          linehaul: null,
          hubs: ctrl.hubs,
        },
        theme: 'nvGreen',
        cssClass: 'linehaul-add',
        controllerAs: 'ctrl',
        controller: 'LinehaulEntriesDialogController',
      });
    }
    onLoad();
  }
}());
