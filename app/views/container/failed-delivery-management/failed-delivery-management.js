(function controller() {
  angular
    .module('nvOperatorApp.controllers')
    .controller('FailedDeliveryManagementController', FailedDeliveryManagementController);

  FailedDeliveryManagementController.$inject = [
    '$q', 'Order', 'nvTable', 'nvDateTimeUtils', '$window',
    '$state', 'nvToast', 'nvTranslate', 'nvDialog', 'nvEditModelDialog',
    'nvFileUtils', '$mdDialog', 'Transaction', 'Shippers', 'Hub',
    'Driver',
  ];

  function FailedDeliveryManagementController(
    $q, Order, nvTable, nvDateTimeUtils, $window,
    $state, nvToast, nvTranslate, nvDialog, nvEditModelDialog,
    nvFileUtils, $mdDialog, Transaction, Shippers, Hub,
    Driver
  ) {
    // ////////////////////////////////////////////////
    // variables
    // ////////////////////////////////////////////////
    const ctrl = this;
    let SHIPPERS_MAP;
    let FAILURE_REASONS_MAP;
    let HUBS_MAP;

    ctrl.state = {
      isRescheduleUpdating: false,
    };
    ctrl.isApplyActionLoading = false;

    ctrl.tableParam = nvTable.createTable();
    ctrl.tableParam.addColumn('tracking_id', { displayName: 'commons.tracking-id' });
    ctrl.tableParam.addColumn('type', { displayName: 'commons.type' });
    ctrl.tableParam.addColumn('_shipperName', { displayName: 'commons.shipper' });
    ctrl.tableParam.addColumn('_lastAttemptTime', {
      displayName: 'container.failed-delivery-management.last-attempt-datetime',
    });
    ctrl.tableParam.addColumn('_failureReasonComments', {
      displayName: 'container.failed-delivery-management.failure-comments',
    });
    ctrl.tableParam.addColumn('attempt_count', {
      displayName: 'container.failed-delivery-management.total-number-of-failures',
    });
    ctrl.tableParam.addColumn('_invalidFailureCount', {
      displayName: 'container.failed-delivery-management.invalid-failure-count',
    });
    ctrl.tableParam.addColumn('_validFailureCount', {
      displayName: 'container.failed-delivery-management.valid-failure-count',
    });
    ctrl.tableParam.addColumn('_failureReasonCodeDescriptions', {
      displayName: 'container.failed-delivery-management.failure-reason',
    });
    ctrl.tableParam.addColumn('_daysSinceLastAttempt', {
      displayName: 'container.failed-delivery-management.days-after-last-attempt',
    });
    ctrl.tableParam.addColumn('_priorityLevel', {
      displayName: 'commons.model.priority-level',
    });
    ctrl.tableParam.addColumn('_lastScannedHubName', {
      displayName: 'container.failed-delivery-management.last-scanned-hub',
    });
    ctrl.tableParam.addColumn('_orderTags', {
      displayName: 'container.failed-delivery-management.order-tags',
    });

    ctrl.tableParam.updateColumnFilter('_orderTags', strFn => (fdm) => {
      const tagsValue = _.get(fdm, '_orderTags.value');
      const toLowerCase = _.toLower(strFn());
      const found = _.find(tagsValue, tag => tag.indexOf(toLowerCase) > -1);
      return !!found;
    });

    // ////////////////////////////////////////////////
    // functions
    // ////////////////////////////////////////////////
    ctrl.getAll = getAll;
    ctrl.getSelectedCount = getSelectedCount;
    ctrl.returnSelectedToSender = returnSelectedToSender;
    ctrl.rescheduleSelected = rescheduleSelected;
    ctrl.downloadCsvSelected = downloadCsvSelected;
    ctrl.rescheduleToNextDay = rescheduleToNextDay;
    ctrl.returnToSender = returnToSender;
    ctrl.editOrder = editOrder;

    // ////////////////////////////////////////////////
    // functions details
    // ////////////////////////////////////////////////
    function getAll() {
      return Order.getFailedDeliveryManagementData().then(success, $q.reject);

      function success(failedDeliveryManagementResponse) {
        return getRemainingDetails(failedDeliveryManagementResponse);
      }

      function getRemainingDetails(failedDeliveryManagementResponse) {
        return $q.all([
          Shippers.searchByIds(
            _.uniq(_.map(failedDeliveryManagementResponse, 'shipper_id'))
          ),
          Driver.searchFailureReasonsByIds(
            _.uniq(_.flatten(_.map(failedDeliveryManagementResponse, 'failure_reason_ids')))
          ),
          Hub.searchByIds(
            _.uniq(_.compact(_.map(failedDeliveryManagementResponse, 'last_scanned_hub_id')))
          ),
        ]).then(getRemainingDetailsSuccess, $q.reject);

        function getRemainingDetailsSuccess(response) {
          SHIPPERS_MAP = _.keyBy(response[0], 'id');
          FAILURE_REASONS_MAP = _.keyBy(response[1], 'id');
          HUBS_MAP = _.keyBy(response[2], 'id');

          ctrl.tableParam.setData(extend(failedDeliveryManagementResponse));
        }
      }
    }

    function getSelectedCount() {
      return ctrl.tableParam.getSelection().length;
    }

    function returnSelectedToSender($event) {
      ctrl.isApplyActionLoading = true;
      getByOrderIdsWithTransactionsPromise(
        ctrl.tableParam.getSelection()
      ).then(success, () => {
        ctrl.isApplyActionLoading = false;
      });

      function success(response) {
        ctrl.isApplyActionLoading = false;

        nvDialog.showSingle($event, {
          templateUrl: 'views/container/order/dialog/edit-rts-details/edit-rts-details.dialog.html',
          cssClass: 'order-edit-rts-details',
          controller: 'OrderEditRtsDetailsDialogController',
          controllerAs: 'ctrl',
          locals: {
            ordersData: extendOrders(response.orders),
            settingsData: {
              isMultiSelect: true,
            },
          },
        }).then(onSet);

        function onSet(rtsResponse) {
          const rtsedOrders = rtsResponse.success;
          ctrl.tableParam.remove(rtsedOrders, 'order_id');
          ctrl.tableParam.deselect();
        }
      }
    }

    function rescheduleSelected($event) {
      const fields = {
        date: new Date(),
      };

      nvDialog.showSingle($event, {
        type: 'nv-dialog',
        templateUrl: 'views/container/failed-delivery-management/dialog/reschedule-selected.dialog.html',
        cssClass: 'failed-delivery-management-reschedule-selected',
        scope: _.assign(nvEditModelDialog.scopeTemplate(), {
          onSubmit: onSubmit,
          fields: fields,
          selectedOrdersCount: getSelectedCount(),
          state: ctrl.state,
        }),
      });

      function downloadCsv() {
        const failedDeliveries = angular.copy(ctrl.rescheduledSuccessfully);
        if (failedDeliveries && failedDeliveries.length > 0) {
          let csvContent = 'Tracking ID, Order ID, Shipper Name\n';

          _.forEach(failedDeliveries, (failedDelivery) => {
            csvContent += (`${failedDelivery.trackingId},${
                          failedDelivery.orderId},${
                          failedDelivery.shipperName}\n`);
          });

          nvFileUtils.downloadCsv(csvContent, 'delivery-reschedule.csv');
        }
      }

      function onSubmit() {
        const date = nvDateTimeUtils.displayDate(fields.date);
        const failedDeliveries = ctrl.tableParam.getSelection();
        ctrl.rescheduledSuccessfully = [];
        if (failedDeliveries.length > 1) {
          multiSelectRescheduling(failedDeliveries, date);
        } else {
          singleRescheduling(failedDeliveries, date);
        }
      }

      function singleRescheduling(failedDeliveries, date) {
        const requests = _.map(failedDeliveries, failedDelivery => generateRescheduleRequest(failedDelivery, date));
        const request = _.head(requests);
        ctrl.state.isRescheduleUpdating = true;
        ctrl.rescheduledSuccessfully = [];

        Order.reschedule(request.orderId, request.data).then((data) => {
          if (data.status.toLowerCase() === 'success') {
            nvToast.success(
              nvTranslate.instant('container.failed-delivery-management.rescheduling-success'),
              nvTranslate.instant('container.failed-delivery-management.num-orders-rescheduling success', { num: 1 })
            );
            ctrl.rescheduledSuccessfully = _.concat(ctrl.rescheduledSuccessfully, requests);
            removeFromTable(request);
            downloadCsv();
          } else {
            nvToast.error(
              nvTranslate.instant('container.failed-delivery-management.rescheduling-failed')
            );
          }

          ctrl.state.isRescheduleUpdating = false;
          $mdDialog.hide();
        }, () => {
          ctrl.state.isRescheduleUpdating = false;
          $mdDialog.hide();
        });
      }

      function multiSelectRescheduling(failedDeliveries, date) {
        const requests = _.map(angular.copy(failedDeliveries), failedDelivery =>
          generateRescheduleRequest(failedDelivery, date));

        ctrl.bulkActionProgressPayload = {
          totalCount: requests.length,
          currentIndex: 0,
          errors: [],
        };
        // hide date dialog
        $mdDialog.hide();

        nvDialog.showSingle($event, {
          templateUrl: 'lib/ninja-commons/services/dialogs/bulk-action-progress/bulk-action-progress.dialog.html',
          cssClass: 'bulk-action-progress',
          controller: 'BulkActionProgressDialogController',
          controllerAs: 'ctrl',
          skipHide: true,
          clickOutsideToClose: false,
          locals: {
            payload: ctrl.bulkActionProgressPayload,
          },
        }).then(progressDialogClosed);

        startMultiSelectCancelling(requests);
      }

      function startMultiSelectCancelling(requests) {
        let request;
        if (requests.length > 0) {
          ctrl.state.isRescheduleUpdating = true;
          ctrl.bulkActionProgressPayload.currentIndex += 1;
          request = _.head(requests.splice(0, 1));

          Order.reschedule(request.orderId, request.data).then(multiSelectSuccess, multiSelectError);
        } else {
          // should never get here
          ctrl.state.isRescheduleUpdating = false;
        }

        function multiSelectSuccess(data) {
          // check status string
          if (data.status.toLowerCase() === 'success') {
            ctrl.bulkActionProgressPayload.successCount += 1;
            ctrl.rescheduledSuccessfully = _.concat(ctrl.rescheduledSuccessfully, request);
            removeFromTable(request);

            if (requests.length > 0) {
              startMultiSelectCancelling(requests);
            } else {
              downloadCsv();
              ctrl.state.isRescheduleUpdating = false;
            }
          } else {
            multiSelectError({ message: data.message });
          }
        }

        function multiSelectError(error) {
          ctrl.bulkActionProgressPayload.errors.push(`${request.trackingId}: ${error.message}`);
          if (requests.length > 0) {
            startMultiSelectCancelling(requests);
          } else {
            downloadCsv();
            ctrl.state.isRescheduleUpdating = false;
          }
        }
      }

      function progressDialogClosed() {
        if (getSuccessCount() > 0) {
          nvToast.success(
            nvTranslate.instant('container.failed-delivery-management.rescheduling-success'),
            nvTranslate.instant('container.failed-delivery-management.num-orders-rescheduling success', { num: getSuccessCount() }));
        }

        if (ctrl.bulkActionProgressPayload.errors.length === 0) {
          $mdDialog.hide();
        }
      }

      function getSuccessCount() {
        if (ctrl.bulkActionProgressPayload) {
          return ctrl.bulkActionProgressPayload.totalCount - ctrl.bulkActionProgressPayload.errors.length;
        }
        return 0;
      }

      function removeFromTable(failedDelivery) {
        const tableData = _.find(ctrl.tableParam.getSelection(), order =>
          order.tracking_id === failedDelivery.trackingId);
        ctrl.tableParam.deselect(tableData);
        ctrl.tableParam.remove(tableData, 'tracking_id');
      }
    }

    function downloadCsvSelected() {
      let csvContent = 'Order ID, ' +
        'Tracking ID, ' +
        'Type, ' +
        'Shipper, ' +
        'Last Attempt Datetime, ' +
        'Failure Comments, ' +
        'Total No. of Failures, ' +
        'Invalid Failure Count, ' +
        'Valid Failure Count, ' +
        'Failure Reason, ' +
        'Days After Last Attempt, ' +
        'Priority Level, ' +
        'Last Scanned Hub\n';

      _.forEach(ctrl.tableParam.getSelection(), (failedDelivery) => {
        csvContent += (`${failedDelivery.order_id},${
          failedDelivery.tracking_id},${
          failedDelivery.type},${
          failedDelivery._shipperName},${
          failedDelivery._lastAttemptTime},${
          failedDelivery._failureReasonComments || ''},${
          failedDelivery.attempt_count},${
          failedDelivery._invalidFailureCount},${
          failedDelivery._validFailureCount},${
          failedDelivery._failureReasonCodeDescriptions},${
          failedDelivery._daysSinceLastAttempt},${
          failedDelivery._priorityLevel},${
          failedDelivery._lastScannedHubName}\n`);
      });

      nvFileUtils.downloadCsv(csvContent, 'failed-delivery-list.csv');
    }

    // Next Day Reschedule
    function rescheduleToNextDay(failedDelivery) {
      const request = generateRescheduleRequest(failedDelivery, getTomorrowDate());

      failedDelivery._isRescheduling = true;
      Order.reschedule(request.orderId, request.data).then(success, failure);

      function success(data) {
        if (data.status.toLowerCase() === 'success') {
          nvToast.success(
            nvTranslate.instant('container.failed-delivery-management.rescheduling-success'),
            nvTranslate.instant('container.failed-delivery-management.num-orders-rescheduling success', { num: 1 })
          );
          ctrl.tableParam.remove(failedDelivery, 'order_id');
        } else {
          nvToast.error(
            nvTranslate.instant('container.failed-delivery-management.rescheduling-failed'),
            nvTranslate.instant('container.failed-delivery-management.order-x-comma-x', { orders: request.trackingId })
          );
        }
        failedDelivery._isRescheduling = false;
      }

      function failure() {
        failedDelivery._isRescheduling = false;
      }

      function getTomorrowDate() {
        const newDate = new Date();
        newDate.setDate(newDate.getDate() + 1);

        return nvDateTimeUtils.displayDate(newDate);
      }
    }

    function returnToSender($event, failedDelivery) {
      failedDelivery._isRTSing = true;

      getByOrderIdsWithTransactionsPromise(failedDelivery).then(success, failure);

      function success(response) {
        failedDelivery._isRTSing = false;

        nvDialog.showSingle($event, {
          templateUrl: 'views/container/order/dialog/edit-rts-details/edit-rts-details.dialog.html',
          cssClass: 'order-edit-rts-details expand',
          controller: 'OrderEditRtsDetailsDialogController',
          controllerAs: 'ctrl',
          locals: {
            ordersData: extendOrders(response.orders || []),
            settingsData: {},
          },
        }).then(onSet);

        function onSet() {
          ctrl.tableParam.remove(failedDelivery, 'order_id');
        }
      }

      function failure() {
        failedDelivery._isRTSing = false;
      }
    }

    function editOrder(failedDelivery) {
      $window.open(
        $state.href('container.order.edit', { orderId: failedDelivery.order_id })
      );
    }

    function extend(failedDeliveries) {
      return _.map(failedDeliveries, failedDelivery => (
        setCustomFailedDeliveryData(failedDelivery)
      ));
    }

    function setCustomFailedDeliveryData(failedDelivery) {
      failedDelivery._isRescheduling = false;
      failedDelivery._isRTSing = false;
      failedDelivery._shipperName = _.get(SHIPPERS_MAP[failedDelivery.shipper_id], 'name') || '';
      failedDelivery._lastScannedHubName = _.get(
        HUBS_MAP[failedDelivery.last_scanned_hub_id], 'name'
      ) || '';

      failedDelivery._daysSinceLastAttempt = '';
      failedDelivery._lastAttemptTime = '';
      if (failedDelivery.last_attempt_time) {
        failedDelivery._lastAttemptTime = nvDateTimeUtils.displayDateTime(
          moment.utc(failedDelivery.last_attempt_time)
        );

        failedDelivery._daysSinceLastAttempt = moment(new Date()).diff(
            moment(failedDelivery._lastAttemptTime), 'days'
          ) || nvTranslate.instant('commons.today');
      }

      failedDelivery._priorityLevel = failedDelivery.priority_level !== null ? failedDelivery.priority_level : '-';

      failedDelivery._invalidFailureCount = 0;
      failedDelivery._validFailureCount = 0;
      _.forEach(failedDelivery.failure_reason_ids, (failureReasonId) => {
        const failureReason = FAILURE_REASONS_MAP[failureReasonId] || null;
        if (failureReason) {
          switch (_.get(failureReason, 'failureReasonCode.liability')) {
            case Driver.FAILURE_REASON_CODE_LIABILITY.EXTERNAL:
              failedDelivery._validFailureCount += 1;
              break;
            case Driver.FAILURE_REASON_CODE_LIABILITY.NINJAVAN:
              failedDelivery._invalidFailureCount += 1;
              break;
            default:
          }
        }
      });

      failedDelivery._failureReasonCodeDescriptions = '';
      const lastFailureReasonId = _.last(failedDelivery.failure_reason_ids);
      const lastFailureReason = FAILURE_REASONS_MAP[lastFailureReasonId] || null;
      if (lastFailureReason) {
        failedDelivery._failureReasonComments = _.get(lastFailureReason, 'description');
        failedDelivery._failureReasonCodeDescriptions = _.get(lastFailureReason, 'failureReasonCode.description');
      }
      const { tags } = failedDelivery;
      if (_.size(tags) > 0) {
        failedDelivery._orderTags = {
          value: _.map(tags, tag => _.toLower(tag)),
          tags: _.map(tags, tag => _.toUpper(tag)),
        };
      } else {
        failedDelivery._orderTags = {
          value: ['no tags'],
          tags: [],
        };
      }
      

      return failedDelivery;
    }

    function generateRescheduleRequest(failedDelivery, date) {
      return {
        orderId: failedDelivery.order_id,
        trackingId: failedDelivery.tracking_id,
        shipperName: failedDelivery._shipperName,
        data: { date: date },
      }; // payload and parameter changes based on ORDER-296
    }

    function getByOrderIdsWithTransactionsPromise(failedDeliveries) {
      const orderIds = [];
      _.forEach(_.castArray(failedDeliveries), (failedDelivery) => {
        orderIds.push(failedDelivery.order_id);
      });

      const param = {
        noOfRecords: _.size(orderIds),
        page: 1,
        orderIds: orderIds,
        withTransactions: true,
        oneMonthSearch: false,
      };

      return Order.search(param);
    }

    function extendOrders(orders) {
      return _.map(orders, order => (
        setCustomOrderData(order)
      ));

      function setCustomOrderData(order) {
        order._granularStatus = Order.GRANULAR_STATUS[order.granularStatus] || order.granularStatus;
        order._lastPPNT = Transaction.getLastPPNT(order.transactions) || {};

        return order;
      }
    }
  }
}());

(function directive() {
  angular
    .module('nvOperatorApp.directives')
    .directive('nvFdmPills', nvFdmPills);

  function nvFdmPills() {
    return {
      restrict: 'E',
      scope: {
        tags: '=',
        trackingId: '=',
      },
      link: link,
      template: `
        <div ng-if="size > 0" class="pill" ng-repeat="tag in x">
          {{ tag }}
        </div>
        <div ng-if="size === 0">
          NO TAGS
        </div>
      `,
    };

    function link(scope) {
      scope.$watch('tags', () => {
        scope.x = _.cloneDeep(scope.tags);
        scope.size = _.size(scope.x);
      });
    }
  }
}());

