(function controller() {
  angular
    .module('nvOperatorApp.controllers')
    .controller('RouteLogsController', RouteLogsController);

  RouteLogsController.$inject = [
    '$q',
    '$timeout',
    'Driver',
    'Hub',
    'nvTable',
    'Route',
    'Zone',
    'Tag',
    'DriverType',
    '$rootScope',
    'nvDateTimeUtils',
    'nvDialog',
    'nvTranslate',
    'nvToast',
    'nvEditModelDialog',
    '$mdDialog',
    'nvURL',
    '$state',
    '$window',
    'nvBackendFilterUtils',
    'RouteFilterTemplate',
    'RouteFilter',
    'Vehicle',
    '$translate',
  ];

  function RouteLogsController(
    $q,
    $timeout,
    Driver,
    Hub,
    nvTable,
    Route,
    Zone,
    Tag,
    DriverType,
    $rootScope,
    nvDateTimeUtils,
    nvDialog,
    nvTranslate,
    nvToast,
    nvEditModelDialog,
    $mdDialog,
    nvURL,
    $state,
    $window,
    nvBackendFilterUtils,
    RouteFilterTemplate,
    RouteFilter,
    Vehicle,
    $translate
  ) {
    // ////////////////////////////////////////////////
    // variables
    // ////////////////////////////////////////////////
    const ROUTE_VALIDATION_ACTIONS = {
      UNARCHIVE: 'container.route-logs.unarchive',
      ARCHIVE: 'container.route-logs.archive',
    };

    const ctrl = this;
    const TIMEZONE = $rootScope.user.timezone;
    const PAGES = { FILTER: 'filter', RESULT: 'result' };
    let drivers = [];
    const driversMap = {};
    let hubs = [];
    const hubsMap = {};
    let zones = [];
    const zonesMap = {};
    let tags = [];
    const tagsMap = {};
    let driverTypes = [];
    const driverTypesMap = {};
    let vehicles = [];
    const vehiclesMap = {};

    ctrl.PAGES = PAGES;
    ctrl.currentPage = PAGES.FILTER;

    // Filter page
    ctrl.form = {};
    ctrl.selectedFilters = [];
    ctrl.possibleFilters = [];
    ctrl.filterPresets = [];
    ctrl.getRoutesState = 'idle';
    ctrl.selectedPreset = null;

    // Result page
    ctrl.editDetailsFields = null;
    ctrl.routesTableParam = null;
    ctrl.tagsSelectionOptions = [];


    // ////////////////////////////////////////////////
    // functions
    // ////////////////////////////////////////////////
    ctrl.goToPage = goToPage;
    ctrl.isPage = isPage;

    // Filter page
    ctrl.checkPresetName = checkPresetName;
    ctrl.deletePreset = deletePreset;
    ctrl.onLoadSelection = getRouteLogs;
    ctrl.savePreset = savePreset;
    ctrl.getAll = getAll;
    ctrl.reverifyAddress = reverifyAddress;

    // Result page
    ctrl.archive = archive;
    ctrl.unarchive = unarchive;
    ctrl.bulkEditDetails = bulkEditDetails;
    ctrl.createRoute = createRoute;
    ctrl.deleteSelected = deleteSelected;
    ctrl.editDetails = editDetails;
    ctrl.editDriverTypes = editDriverTypes;
    ctrl.editRoute = editRoute;
    ctrl.editRouteSelected = editRouteSelected;
    ctrl.getSelectedCount = getSelectedCount;
    ctrl.mergeTransactions = mergeTransactions;
    ctrl.optimizeRoute = optimizeRoute;
    ctrl.optimizeSelected = optimizeSelected;
    ctrl.printRoute = printRoute;
    ctrl.printPassword = printPassword;
    ctrl.updateTag = updateTag;

    // ////////////////////////////////////////////////
    // functions details
    // ////////////////////////////////////////////////
    function isPage(page) {
      return ctrl.currentPage === page;
    }

    function goToPage(page) {
      ctrl.currentPage = page;
    }

    function getAll() {
      return read().then(success, $q.reject);

      function read() {
        return $q.all([
          Driver.searchAll(),
          Hub.read({ active_only: false }),
          Zone.read(),
          Tag.all(),
          RouteFilterTemplate.read(),
          DriverType.searchAll(),
          Vehicle.read(),
        ]);
      }

      function success(response) {
        drivers = extendDrivers(response[0].drivers);
        hubs = response[1];
        zones = response[2];
        tags = response[3].tags;
        ctrl.filterPresets = response[4].routeFilterTemplates;
        driverTypes = response[5].driverTypes;
        vehicles = response[6].vehicles;

        ctrl.driversSelectionOptions = Driver.toOptions(response[0]);
        ctrl.hubsSelectionOptions = Hub.toOptions(filterActiveHubOnly(response[1]));
        ctrl.zonesSelectionOptions = Zone.toOptions(response[2]);
        ctrl.tagsSelectionOptions = Tag.toOptions(response[3]);
        ctrl.driverTypesSelectionOptions = DriverType.toSelectOptions(response[5]);
        ctrl.vehiclesSelectionOptions = Vehicle.toOptions(response[6]);

        _.forEach(drivers, (driver) => {
          driver.displayName = _.compact([driver.firstName, driver.lastName]).join(' ');
          driversMap[driver.id] = driver;
        });

        _.forEach(hubs, (hub) => {
          hub.displayName = hub.name;
          hubsMap[hub.id] = hub;
        });

        _.forEach(zones, (zone) => {
          zone.displayName = zone.name;
          zonesMap[zone.id] = zone;
        });

        _.forEach(tags, (tag) => {
          tagsMap[tag.id] = tag;
        });

        _.forEach(driverTypes, (driverType) => {
          driverTypesMap[driverType.id] = driverType;
        });

        _.forEach(vehicles, (vehicle) => {
          vehicle.displayName = [vehicle.vehicleType, vehicle.vehicleNo].join('; ');
          vehiclesMap[vehicle.id] = vehicle;
        });

        const filters = RouteFilter.init({
          hubs: hubs,
          driverTypes: driverTypes,
          drivers: drivers,
          tags: tags,
          zones: zones,
        });
        ctrl.selectedFilters = filters.default;
        ctrl.possibleFilters = filters.possible;
      }

      function filterActiveHubOnly(theHubs) {
        return _.filter(theHubs, hub => hub.active);
      }
    }

    function getRouteLogs() {
      const params = nvBackendFilterUtils.constructParams(ctrl.selectedFilters);
      if (_.size(params.driverTypeIds) > 0) {
        params.driverTypeIds = params.driverTypeIds.join(',');
      }

      if (_.size(params.driverIds) > 0) {
        params.driverIds = params.driverIds.join(',');
      }

      if (_.size(params.hubIds) === 0) {
        nvToast.error(nvTranslate.instant('container.route-logs.please-select-at-least-one-hub'));
        return -1;
      }

      if (_.size(params.hubIds) > 0) {
        params.hubIds = params.hubIds.join(',');
      }

      if (_.size(params.zoneIds) > 0) {
        params.zoneIds = params.zoneIds.join(',');
      }

      if (_.size(params.tagIds) > 0) {
        params.tagIds = params.tagIds.join(',');
      }

      ctrl.getRoutesState = 'waiting';
      return Route.read(params).then(success, () => {
        ctrl.getRoutesState = 'idle';
      });

      function success(response) {
        ctrl.getRoutesState = 'idle';
        ctrl.routesTableParam = createRouteTableParam();
        ctrl.routesTableParam.setDataPromise(extendAsync(response));

        _.forEach(ctrl.selectedFilters, (filter) => {
          filter.description = nvBackendFilterUtils.getDescription(filter);
        });

        // add custom filter
        ctrl.routesTableParam.updateColumnFilter('tags', strFn => (
          (route) => {
            // strfn is the function that will return you the search text
            const lowercase = strFn().toLowerCase();
            return lowercase.length === 0 || _.some(route.tags, tagId =>
                tagsMap[tagId] && tagsMap[tagId].name.toLowerCase().indexOf(lowercase) >= 0
              );
          }
        ));

        goToPage(PAGES.RESULT);
      }
    }

    function createRouteTableParam() {
      return nvTable.createTable(Route.FIELDS)
        .addColumn('tags', { displayName: 'container.route-logs.tags' })
        .addColumn('_date', { displayName: 'commons.model.date' })
        .addColumn('_driverName', { displayName: 'container.route-logs.driver-name' })
        .addColumn('_hubName', { displayName: 'container.route-logs.hub-name' })
        .addColumn('_zoneName', { displayName: 'container.route-logs.zone-name' })
        .addColumn('_driverTypeName', { displayName: 'container.route-logs.driver-type-name' });
    }

    function getSelectedCount() {
      return ctrl.routesTableParam.getSelection().length;
    }

    function savePreset(options) {
      return RouteFilterTemplate.create(options).then(response => response.routeFilterTemplate);
    }

    function deletePreset(id) {
      return RouteFilterTemplate.delete(id).then(response => response.routeFilterTemplate);
    }

    function checkPresetName(name) {
      return RouteFilterTemplate.checkNameAvailability(name)
        .then(response => response.isNameAvailable);
    }

    function editRouteSelected($event, routes) {
      nvDialog.showSingle($event, {
        type: 'nv-dialog',
        templateUrl: 'views/container/route-logs/dialog/route-logs-edit-route.dialog.html',
        cssClass: 'route-logs-edit-route',
        scope: _.assign(nvEditModelDialog.scopeTemplate(), {
          routes: routes,
          openEditRoutePage: openEditRoutePage,
        }),
      });
    }

    function archive($event, routes) {
      // validation
      const routesValidationErrorData = getRoutesValidationErrorData(
        routes, ROUTE_VALIDATION_ACTIONS.ARCHIVE
      );
      if (routesValidationErrorData.errors.length > 0) {
        showSelectionErrorDialog(routes, routesValidationErrorData, $event, archive);
        return;
      }

      const payload = [{
        routeIds: [],
      }];
      _.forEach(routes, (route) => {
        payload[0].routeIds.push(route.id);
      });

      nvDialog.showSingle($event, {
        type: 'nv-dialog',
        templateUrl: 'views/container/route-logs/dialog/route-logs-archive.dialog.html',
        cssClass: 'route-logs-archive',
        scope: _.assign(nvEditModelDialog.scopeTemplate({ onSave: update }), {
          selectedRoutesCount: routes.length,
        }),
      }).then(success);

      function update() {
        return Route.archiveRoutes(payload);
      }

      function success(response) {
        let notArchivedIds = [];
        if (_.size(response.unarchived_route_ids) > 0) {
          notArchivedIds = response.unarchived_route_ids;

          nvToast.error(
            $translate.instant('container.route-logs.routes-ids-have-pending-waypoints', {
              ids: _.join(notArchivedIds, ', '),
            })
          );
        }

        const archivedRouteIds = [];
        _.forEach(routes, (route) => {
          if (!_.includes(notArchivedIds, route.id)) {
            archivedRouteIds.push(route.id);

            route.status = Route.getStatusEnum(Route.STATUS.ARCHIVED);
            route.archived = true;
          }
        });

        if (_.size(archivedRouteIds) > 0) {
          nvToast.info(
            nvTranslate.instant('container.route-logs.num-route-archived', { num: archivedRouteIds.length }),
            nvTranslate.instant('container.route-logs.route-x-comma-x', { routes: archivedRouteIds.join(', ') })
          );
        }
      }
    }

    function unarchive($event, routes) {
      // validation
      const routesValidationErrorData = getRoutesValidationErrorData(
        routes, ROUTE_VALIDATION_ACTIONS.UNARCHIVE
      );
      if (routesValidationErrorData.errors.length > 0) {
        showSelectionErrorDialog(routes, routesValidationErrorData, $event, unarchive);
        return;
      }

      // start submitting
      const payload = [];
      _.forEach(routes, (route) => {
        payload.push({
          id: route.id,
          archived: false,
        });
      });

      nvDialog.showSingle($event, {
        type: 'nv-dialog',
        templateUrl: 'views/container/route-logs/dialog/route-logs-unarchive.dialog.html',
        cssClass: 'route-logs-unarchive',
        scope: _.assign(nvEditModelDialog.scopeTemplate({ onSave: update }), {
          selectedRoutesCount: routes.length,
        }),
      }).then(success);

      function update() {
        return Route.updateRouteDetails(payload);
      }

      function success(response) {
        const routeIds = [];

        _.forEach(response, (updatedRoute) => {
          const route = _.find(routes, ['id', updatedRoute.id]);

          updatedRoute.driverId = (updatedRoute.driver ? updatedRoute.driver.id : null) ||
            route.driverId;
          setCustomRouteData(updatedRoute);
          ctrl.routesTableParam.mergeIn(updatedRoute, 'id');

          routeIds.push(updatedRoute.id);
        });

        nvToast.info(
          nvTranslate.instant('container.route-logs.num-route-unarchived', { num: routeIds.length }),
          nvTranslate.instant('container.route-logs.route-x-comma-x', { routes: routeIds.join(', ') })
        );
      }
    }

    function getRoutesValidationErrorData(routes, action) {
      const routesValidationErrorData = {
        action: action,
        errors: [],
      };

      _.forEach(routes, (route) => {
        if (action === ROUTE_VALIDATION_ACTIONS.UNARCHIVE) {
          // only archived routes can be unarchived
          if (Route.STATUS[route.status] !== Route.STATUS.ARCHIVED) {
            routesValidationErrorData.errors.push(
              generateRouteValidationErrorData(route, 'container.route-logs.invalid-status-to-change')
            );
          }
        } else if (action === ROUTE_VALIDATION_ACTIONS.ARCHIVE) {
          // only non-archived routes can be archived
          if (Route.STATUS[route.status] === Route.STATUS.ARCHIVED) {
            routesValidationErrorData.errors.push(
              generateRouteValidationErrorData(route, 'container.route-logs.invalid-status-to-change')
            );
          }
        }
      });

      return routesValidationErrorData;

      function generateRouteValidationErrorData(route, errorMessage) {
        return {
          id: route.id,
          errorMessage: errorMessage,
        };
      }
    }

    function showSelectionErrorDialog(routes, routesValidationErrorData, $event, fn) {
      nvDialog.showSingle($event, {
        templateUrl: 'views/container/route-logs/dialog/selection-error/selection-error.dialog.html',
        theme: 'nvRed',
        cssClass: 'route-selection-error',
        controller: 'RouteSelectionErrorDialogController',
        controllerAs: 'ctrl',
        locals: {
          routesValidationErrorData: routesValidationErrorData,
        },
      }).then(success);

      function success() {
        const validRoutes = _.differenceBy(routes, routesValidationErrorData.errors, 'id');

        ctrl.routesTableParam.deselect();
        _.forEach(validRoutes, (route) => {
          const theRoute = _.find(ctrl.routesTableParam.data, ['id', route.id]);

          if (theRoute) {
            ctrl.routesTableParam.select(theRoute);
          }
        });

        if (validRoutes.length <= 0) {
          nvToast.error(
            nvTranslate.instant('commons.unable-to-apply-action'),
            nvTranslate.instant('commons.no-valid-selection')
          );
        } else {
          fn($event, validRoutes);
        }
      }
    }

    function optimizeSelected($event, routes) {
      ctrl.unOptimizedRoutes = [];
      ctrl.optimizedRoutes = [];

      ctrl.selectedRoutes = _.cloneDeep(routes);
      const selectedRoutesCount = ctrl.selectedRoutes.length;

      loopOptimizationProcess(ctrl.selectedRoutes);

      nvDialog.showSingle($event, {
        type: 'nv-dialog',
        templateUrl: 'views/container/route-logs/dialog/route-logs-optimize-selected.dialog.html',
        cssClass: 'route-logs-optimize-selected',
        scope: _.assign(nvEditModelDialog.scopeTemplate(), {
          optimizedRoutes: ctrl.optimizedRoutes,
          unOptimizedRoutes: ctrl.unOptimizedRoutes,
          selectedRoutesCount: selectedRoutesCount,
        }),
      });
    }

    function printRoute($event, routes) {
      return Route.downloadManifestPdf(_.map(routes, 'id'));
    }

    function printPassword($event, routes) {
      return Route.downloadPasswordPdf({ route_ids: _.map(routes, 'id') });
    }

    function bulkEditDetails($event, routes) {
      let payload = [];
      const fields = _.cloneDeep(Route.getAddFields());
      const bulkEditTable = createRouteTableParam();
      bulkEditTable.setDataPromise(extendAsync(routes));

      nvDialog.showSingle($event, {
        type: 'nv-dialog',
        templateUrl: 'views/container/route-logs/dialog/route-logs-bulk-edit-details.dialog.html',
        cssClass: 'route-logs-bulk-edit-details',
        scope: _.assign(nvEditModelDialog.scopeTemplate({ onSave: update }), {
          fields: fields,
          tableParam: bulkEditTable,
          selectedRoutesCount: routes.length,
          tagsSelectionOptions: _.cloneDeep(ctrl.tagsSelectionOptions),
          hubsSelectionOptions: _.cloneDeep(ctrl.hubsSelectionOptions),
          driversSelectionOptions: _.cloneDeep(ctrl.driversSelectionOptions),
          vehiclesSelectionOptions: _.cloneDeep(ctrl.vehiclesSelectionOptions),
        }),
      }).then(success);

      function update() {
        payload = _.map(routes, route => _.omitBy(({
          id: route.id,
          datetime: nvDateTimeUtils.displayDateTime(nvDateTimeUtils.toSOD(fields.date.value), 'utc'),
          date: nvDateTimeUtils.displayISO(nvDateTimeUtils.toSOD(fields.date.value), 'utc'),
          tags: fields.tags.value,
          hubId: fields.hubId.value && fields.hubId.value.value,
          driverId: fields.driverId.value && fields.driverId.value.value,
          vehicleId: fields.vehicleId.value && fields.vehicleId.value.value,
          comments: fields.comments.value,
        }), _.isNil));

        return Route
          .updateRouteDetails(payload);
      }

      function success(response) {
        // TODO: Backend doesn't return the created route details
        const updatedRoutes = _.map(response, (r, idx) => _.defaults(payload[idx], r));
        ctrl.routesTableParam.mergeInPromise(extendAsync(updatedRoutes), 'id');

        nvToast.info(
          nvTranslate.instant('container.route-logs.num-route-edited', { num: routes.length })
        );
      }
    }

    function editDriverTypes($event, routes) {
      let selectedDriverTypeNames;

      nvDialog.showSingle($event, {
        type: 'nv-dialog',
        templateUrl: 'views/container/route-logs/dialog/route-logs-driver-types.dialog.html',
        cssClass: 'route-logs-driver-types',
        scope: _.assign(nvEditModelDialog.scopeTemplate({ onSave: update }), {
          fields: Route.getAddFields(),
          selectedRoutesCount: routes.length,
          driverTypesSelectionOptions: ctrl.driverTypesSelectionOptions,
        }),
      }).then(success);

      function update(extractedFields, originalFields) {
        const data = _.map(routes,
          route => ({
            routeId: route.id,
            driverTypeIds: originalFields.selectedDriverTypes,
          })
        );
        selectedDriverTypeNames = _(driverTypesMap)
          .pick(originalFields.selectedDriverTypes)
          .map('name')
          .value();
        return Route.updateRouteDriverTypes(data);
      }

      function success() {
        nvToast.info(
          nvTranslate.instant('container.route-logs.num-route-edited', { num: routes.length }),
          nvTranslate.instant('container.route-logs.driver-types-updated-description', {
            routes: _(routes).map('id').join(', '),
            driverTypes: selectedDriverTypeNames.join(', '),
          })
        );
      }
    }

    function mergeTransactions($event, routes) {
      const routeIds = _.map(routes, 'id');

      nvDialog.confirmSave($event, {
        title: nvTranslate.instant('container.route-logs.merge-transactions-title'),
        content: nvTranslate.instant('container.route-logs.merge-transactions-description', { count: routes.length }),
        ok: nvTranslate.instant('container.route-logs.merge-transactions-confirm'),
      })
      .then(() => Route.mergeTransactions({ routeIds: routeIds }))
      .then(() => nvToast.info(
        nvTranslate.instant('container.route-logs.merge-transactions-success', { count: routes.length }),
        nvTranslate.instant('container.route-logs.route-x-comma-x', { routes: routeIds.join(', ') })
      ));
    }

    function updateTag(route) {
      if (_.isEqual(route.tags, route.previousTags)) { return; }

      const payload = [{
        routeId: route.id,
        tagIds: route.tags,
      }];
      const routeIds = [route.id];
      const tagNames = [];

      _.forEach(route.tags, (tagId) => {
        tagNames.push(tagsMap[tagId].name);
      });

      Route.updateTags(payload).then(success);

      function success() {
        route.previousTags = route.tags;
        nvToast.info(
          nvTranslate.instant('container.route-logs.num-route-tagged', { num: routeIds.length }),
          nvTranslate.instant('container.route-logs.num-route-tagged-description', { routes: routeIds.join(', '), tags: tagNames.join(', ') || '-' })
        );
      }
    }

    function editRoute($event, route) {
      editRouteSelected($event, [route]);
    }

    function editDetails($event, route) {
      let payload = [];
      const draftRoute = _.cloneDeep(route); // to prevent referencing
      ctrl.editDetailsFields = Route.getEditFields(draftRoute);

      draftRoute._hubSelection = _.find(ctrl.hubsSelectionOptions, [
        'value', _.get(draftRoute, '_hub.id'),
      ]) || null;
      draftRoute._driverSelection = _.find(ctrl.driversSelectionOptions, [
        'value', _.get(draftRoute, '_driver.id'),
      ]) || null;
      draftRoute._vehicleSelection = _.find(ctrl.vehiclesSelectionOptions, [
        'value', _.get(draftRoute, '_vehicle.id'),
      ]) || null;

      nvDialog.showSingle($event, {
        type: 'nv-dialog',
        templateUrl: 'views/container/route-logs/dialog/route-logs-edit-details.dialog.html',
        cssClass: 'route-logs-edit-details',
        scope: _.assign(nvEditModelDialog.scopeTemplate({ onSave: update }), {
          fields: ctrl.editDetailsFields,
          route: draftRoute,
          deleteSelected: deleteSelected,
          tagsSelectionOptions: _.cloneDeep(ctrl.tagsSelectionOptions),
          hubsSelectionOptions: _.cloneDeep(ctrl.hubsSelectionOptions),
          driversSelectionOptions: _.cloneDeep(ctrl.driversSelectionOptions),
          vehiclesSelectionOptions: _.cloneDeep(ctrl.vehiclesSelectionOptions),
        }),
      }).then(success);

      function update(data) {
        payload = [{
          id: draftRoute.id,
          datetime: nvDateTimeUtils.displayDateTime(nvDateTimeUtils.toSOD(data.date), 'utc'),
          date: nvDateTimeUtils.displayISO(nvDateTimeUtils.toSOD(data.date), 'utc'),
          tags: data.tags,
          hubId: draftRoute._hubSelection && draftRoute._hubSelection.value,
          driverId: draftRoute._driverSelection && draftRoute._driverSelection.value,
          vehicleId: draftRoute._vehicleSelection && draftRoute._vehicleSelection.value,
          comments: data.comments,
        }];
        return Route.updateRouteDetails(payload);
      }

      function success(response) {
        // TODO: Backend doesn't return the created route details
        const updatedRoute = _.defaults(payload[0], response[0]);
        ctrl.routesTableParam.mergeIn(setCustomRouteData(updatedRoute), 'id');

        nvToast.info(
          nvTranslate.instant('container.route-logs.num-route-edited', { num: 1 }),
          nvTranslate.instant('container.route-logs.route-x-comma-x', { routes: updatedRoute.id })
        );
      }
    }

    function optimizeRoute($event, route) {
      Route.optimize(route.id).then(success, $q.reject);

      function success(response) {
        routeOptimizationStep2(response, route, $event);
      }
    }

    function deleteSelected($event, routes) {
      $mdDialog.cancel(); // make sure cancel the prev dialog before open new one

      nvDialog.confirmDelete($event, {
        title: nvTranslate.instant('container.route-logs.delete-routes'),
        content: nvTranslate.instant('container.route-logs.delete-routes-description'),
      }).then(onDelete);

      function onDelete() {
        const payload = _.map(routes, r => ({ id: r.id }));
        return Route
          .delete(payload)
          .then(success, failure);
      }

      function success() {
        ctrl.routesTableParam.remove(routes, 'id');
        nvToast.warning(
          nvTranslate.instant('container.route-logs.num-route-deleted', { num: routes.length }),
          nvTranslate.instant('container.route-logs.route-x-comma-x', { routes: _(routes).map('id').join(', ') })
        );

        $mdDialog.cancel(); // make sure cancel the prev dialog before open new one
      }

      function failure(response) {
        nvToast.error(response.messages ? response.messages[0] : '-');
      }
    }

    function createRoute($event, fromFilterPage = false) {
      let payload = [];
      const fields = [_.cloneDeep(Route.getAddFields())];

      nvDialog.showSingle($event, {
        templateUrl: 'views/container/route-logs/dialog/route-logs-create-route.dialog.html',
        cssClass: 'route-logs-create-route',
        theme: 'nvGreen',
        scope: _.assign(nvEditModelDialog.scopeTemplate({ onSave: create }), {
          fields: fields,
          onDuplicateRoute: onDuplicateRoute,
          onDeleteDraft: onDeleteDraft,
          isDisabled: isDisabled,
          tagsSelectionOptions: _.cloneDeep(ctrl.tagsSelectionOptions),
          zonesSelectionOptions: _.cloneDeep(ctrl.zonesSelectionOptions),
          hubsSelectionOptions: _.cloneDeep(ctrl.hubsSelectionOptions),
          driversSelectionOptions: _.cloneDeep(ctrl.driversSelectionOptions),
          vehiclesSelectionOptions: _.cloneDeep(ctrl.vehiclesSelectionOptions),
        }),
      }).then(success);

      function onDeleteDraft(index) {
        fields.splice(index, 1);
        $timeout(() => this.modelForm.$setDirty());
      }

      function onDuplicateRoute() {
        fields.push(_.chain(fields)
          .last()
          .pick(_.keys(Route.getAddFields()))
          .cloneDeep()
          .value()
        );
        $timeout(() => this.modelForm.$setDirty());
      }

      function isDisabled() {
        return _.reduce(fields, (result, route) => (
          result || !(route.hubId.value && route.zoneId.value)
        ), false);
      }

      function create() {
        payload = _.map(fields, route => ({
          date: nvDateTimeUtils.displayDateTime(nvDateTimeUtils.toSOD(route.date.value), 'utc'),
          dateTime: nvDateTimeUtils.displayISO(nvDateTimeUtils.toSOD(route.date.value), 'utc'),
          tags: route.tags.value,
          zoneId: route.zoneId.value && route.zoneId.value.value,
          hubId: route.hubId.value && route.hubId.value.value,
          driverId: route.driverId.value && route.driverId.value.value,
          vehicleId: route.vehicleId.value && route.vehicleId.value.value,
          comments: route.comments.value,
        }));

        return Route
          .createRoutes(payload);
      }

      function success(response) {
        if (fromFilterPage) {
          nvToast.success(
            nvTranslate.instant('container.route-logs.x-routes-created', { x: response.length }),
            _(response)
              .map((route, idx) => `${idx + 1}. ${nvTranslate.instant('container.route-logs.route-x', { x: route.id })}`)
              .join('<br>')
          );
        } else {
          // TODO: Backend doesn't return the created route details
          const updatedRoutes = _.map(response, (r, idx) => {
            payload[idx].date = payload[idx].dateTime;
            return _.defaults(payload[idx], r);
          });
          nvToast.success(
            nvTranslate.instant('container.route-logs.x-routes-created', { x: response.length }),
            _(response)
              .map((route, idx) => `${idx + 1}. ${nvTranslate.instant('container.route-logs.route-x', { x: route.id })}`)
              .join('<br>')
          );
          ctrl.routesTableParam.mergeInPromise(extendAsync(updatedRoutes), 'id');
        }
      }
    }

    function openEditRoutePage(routes, fetchUnroutedWaypoints) {
      const selectedRoutes = _.castArray(routes);

      const selectedHubs = [];
      const params = {
        ids: _.join(_.map(selectedRoutes, 'id'), ','),
        unrouted: fetchUnroutedWaypoints,
        cluster: true,
      };

      if (fetchUnroutedWaypoints) {
        nvDialog.showSingle(null, {
          type: 'nv-dialog',
          templateUrl: 'views/container/route-logs/dialog/route-logs-edit-route-filter-hub.dialog.html',
          cssClass: 'route-logs-edit-route-filter-hub',
          scope: _.assign(nvEditModelDialog.scopeTemplate(), {
            onSave: onLoad,
            searchText: '',
            selectedOptions: selectedHubs,
            hubsSelectionOptions: _.cloneDeep(ctrl.hubsSelectionOptions),
          }),
        });
      } else {
        openPage();
      }

      function onLoad() {
        const selectedHubIds = _.map(selectedHubs, 'value');
        const selectedZones = _.filter(zones, zone =>
          _.includes(selectedHubIds, zone.hub_id)
        );

        params.zones = _.noop();
        if (_.size(selectedZones) > 0) {
          params.zones = _.join(_.map(selectedZones, 'id'), ',');
        }

        openPage();
      }

      function openPage() {
        $window.open(
          $state.href('container.zonal-routing-improved.edit', params)
        );
      }
    }

    // ////////////////////////////////////////////////
    // Helper Functions
    // ////////////////////////////////////////////////
    function extendAsync(routes) {
      return $q.all([
        fetchDataAsync(routes, 'driverId', Driver, driverProcessor),
        fetchDataAsync(routes, 'hubId', Hub, hubProcessor),
        fetchDataAsync(routes, 'zoneId', Zone, zoneProcessor),
      ])
             .then(() => routes.map(setCustomRouteData));
    }

    function fetchDataAsync(routes, keyInRoute, Model, processor) {
      const uniqueIds = _.compact(Array.from(new Set(routes.map(r => r[keyInRoute]))));

      return Model.searchByIds(uniqueIds).then((instances) => {
        const byId = _.keyBy(instances, 'id');
        routes.forEach(route => processor(route, byId[route[keyInRoute]]));
        return routes;
      });
    }

    function driverProcessor(route, driver) {
      if (driver) {
        route._driverName = [driver.firstName, driver.lastName].join(' ');
        route._driverTypeName = driver.driverType;
      } else {
        route._driverName = '-';
        route._driverTypeName = '-';
      }
      route._driver = driver;
    }

    function hubProcessor(route, hub) {
      route._hubName = hub ? hub.name : '-';
      route._hub = hub;
    }

    function zoneProcessor(route, zone) {
      route._zone = zone;
      route._zoneName = zone ? zone.name : '-';
    }

    function setCustomRouteData(route) {
      const vehicleData = getVehicleData(route.vehicleId);

      route._date = nvDateTimeUtils.displayDate(nvDateTimeUtils.toMoment(route.date, 'utc'), TIMEZONE);
      route._vehicle = vehicleData;

      route._vehicleName = vehicleData ? vehicleData.vehicleNo : '-';
      route._tagsName = route.tags ? _(route.tags)
        .map(t => (tagsMap[t] ? tagsMap[t].name : null))
        .join(', ') : '-';

      route.previousTags = route.tags;

      return route;
    }

    function extendDrivers(responsedrivers) {
      return _.map(responsedrivers, (driver) => {
        driver._name = `${driver.firstName} ${driver.lastName}`;
        return driver;
      });
    }

    function routeOptimizationStep2(coreResult, route, $event) {
      const payload = {
        currLat: coreResult.currLat,
        currLng: coreResult.currLng,
        waypoints: coreResult.waypoints,
      };

      Route.optimizeSingleRouteSequence(payload).then(success, $q.reject);

      function success(vrpResult) {
        const oldWpIds = [];
        const newWpIds = [];

        _.forEach(coreResult.waypoints, (wp) => {
          oldWpIds.push(wp.id);
        });

        _.forEach(vrpResult, (wp) => {
          newWpIds.push(parseInt(wp.id, 10));
        });

        const routetags = _.cloneDeep(route.tags) || [];
        if (routetags.indexOf(Tag.getTagId('RES', tags)) < 0) {
          routetags.push(Tag.getTagId('RES', tags));
        }

        const droppedWpIds = _.difference(oldWpIds, newWpIds);
        const routesToUpdate = [{
          id: parseInt(route.id, 10),
          driverId: parseInt(coreResult.driverId, 10),
          vehicleId: parseInt(coreResult.vehicleId, 10),
          waypoints: newWpIds,
          tags: routetags,
        }];

        nvDialog.showSingle($event, {
          type: 'nv-dialog',
          templateUrl: 'views/container/route-logs/dialog/route-logs-optimize.dialog.html',
          cssClass: 'route-logs-optimize',
          scope: _.assign(nvEditModelDialog.scopeTemplate({ onSave: update }), {
            droppedWpIds: droppedWpIds,
            coreResult: coreResult,
            vrpResult: vrpResult,
            joinWithComma: joinWithComma,
          }),
        }).then(updateRoutesSuccess);

        function update() {
          return Route.updateRoutes(routesToUpdate);
        }

        function updateRoutesSuccess(response) {
          const updatedRoute = response[0];
          updatedRoute.driverId = updatedRoute.driver ? updatedRoute.driver.id : null;
          setCustomRouteData(updatedRoute);
          ctrl.routesTableParam.mergeIn(updatedRoute, 'id');

          nvToast.info(
            nvTranslate.instant('container.route-logs.num-route-optimized', { num: 1 }),
            nvTranslate.instant('container.route-logs.route-x-comma-x', { routes: route.id })
          );
        }
      }
    }

    function loopOptimizationProcess(routes) {
      if (routes.length <= 0) {
        return;
      }

      const route = routes[0];

      // phase 1
      Route.optimize(route.id).then(optimizeSuccess, optimizeFailure);
      function optimizeSuccess(coreResult) {
        const originalWaypointCount = coreResult.waypoints.length;

        // phase 2
        const payload = {
          currLat: coreResult.currLat,
          currLng: coreResult.currLng,
          waypoints: coreResult.waypoints,
        };

        Route.optimizeSingleRouteSequence(payload)
          .then(optimizeSingleRouteSequenceSuccess, optimizeSingleRouteSequenceFailure);
        function optimizeSingleRouteSequenceSuccess(vrpResult) {
          routes.splice(0, 1);
          loopOptimizationProcess(routes);
          const newWaypointCount = vrpResult.length;

          // time to update the route
          if (newWaypointCount < originalWaypointCount) {
            ctrl.unOptimizedRoutes.push({
              id: route.id,
              originalCount: originalWaypointCount,
              newWaypointCount: newWaypointCount,
            });
          } else {
            const newWpIds = [];
            _.forEach(vrpResult, (wp) => {
              newWpIds.push(parseInt(wp.id, 10));
            });

            const routetags = _.cloneDeep(route.tags) || [];
            if (routetags.indexOf(Tag.getTagId('RES', tags)) < 0) {
              routetags.push(Tag.getTagId('RES', tags));
            }

            const routesToUpdate = [{
              id: parseInt(route.id, 10),
              driverId: parseInt(coreResult.driverId, 10),
              vehicleId: parseInt(coreResult.vehicleId, 10),
              waypoints: newWpIds,
              tags: routetags,
            }];

            // phase 3
            Route.updateRoutes(routesToUpdate).then((response) => {
              ctrl.optimizedRoutes.push(response[0]);
            }, () => {
              ctrl.unOptimizedRoutes.push({
                id: route.id,
                originalCount: originalWaypointCount,
                newWaypointCount: newWaypointCount,
              });
            });
          }
        }

        function optimizeSingleRouteSequenceFailure() {
          routes.splice(0, 1);
          loopOptimizationProcess(routes);

          ctrl.unOptimizedRoutes.push({
            id: route.id,
            originalCount: originalWaypointCount,
            newWaypointCount: '-',
          });
        }
      }

      function optimizeFailure() {
        routes.splice(0, 1);
        loopOptimizationProcess(routes);

        ctrl.unOptimizedRoutes.push({
          id: route.id,
          originalCount: '-',
          newWaypointCount: '-',
        });
      }
    }

    function getDriverData(id) {
      return driversMap[id] || null;
    }

    function getHubData(id) {
      return hubsMap[id] || null;
    }

    function getVehicleData(id) {
      return vehiclesMap[id] || null;
    }

    function getZoneData(id) {
      return zonesMap[id] || null;
    }

    function joinWithComma(array) {
      return array.join(', ');
    }

    function reverifyAddress($event, route) {
      const id = route.id;

      nvDialog.confirmSave($event, {
        title: $translate.instant('container.route-logs.dialog.verify-route-address.title'),
        content: $translate.instant('container.route-logs.dialog.verify-route-address.content'),
        ok: $translate.instant('container.route-logs.dialog.verify-routep-positive-button'),
        cancel: $translate.instant('commons.cancel'),
        skipHide: false,
      }).then(() => Route
        .reverifyAddress(id)
        .then(() => nvToast.success(nvTranslate.instant('container.route-logs.address-reverified-successfully')))
      );
    }
  }
}());
