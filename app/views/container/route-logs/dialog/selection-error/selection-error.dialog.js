(function controller() {
  angular
    .module('nvOperatorApp.controllers')
    .controller('RouteSelectionErrorDialogController', RouteSelectionErrorDialogController);

  RouteSelectionErrorDialogController.$inject = [
    '$mdDialog', 'routesValidationErrorData',
  ];

  function RouteSelectionErrorDialogController(
    $mdDialog, routesValidationErrorData
  ) {
    // variables
    const ctrl = this;
    ctrl.routesValidationErrorData = routesValidationErrorData;

    // functions
    ctrl.onCancel = onCancel;
    ctrl.onProceed = onProceed;

    // functions details
    function onCancel() {
      $mdDialog.cancel();
    }

    function onProceed() {
      $mdDialog.hide();
    }
  }
}());
