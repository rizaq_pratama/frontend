(function controller() {
  angular
    .module('nvOperatorApp.controllers')
    .controller('PrintersController', PrintersController);

  PrintersController.$inject = ['$q', 'Printer', 'nvToast',
    'nvDialog', 'nvEditModelDialog', 'nvUtils', 'nvTable',
  ];

  function PrintersController($q, Printer, nvToast,
    nvDialog, nvEditModelDialog, nvUtils, nvTable) {
    // ////////////////////////////////////////////////
    // variables
    // ////////////////////////////////////////////////
    const ctrl = this;
    const COLUMNS = {
      id: { displayName: 'container.printers.column-id' },
      name: { displayName: 'container.printers.column-name' },
      ip_address: { displayName: 'container.printers.column-ip-address' },
      version: { displayName: 'container.printers.column-version' },
      is_default: { displayName: 'container.printers.column-default' },
    };

    const highlightActive = {
      class: 'printer-default',
      highlighted: true,
    };
    const highlightDefult = {
      class: '',
      highlighted: false,
    };

    ctrl.defaultPrinter = {};
    ctrl.tableParam = nvTable.createTable(COLUMNS);
    // ////////////////////////////////////////////////
    // functions
    // ////////////////////////////////////////////////
    ctrl.init = init;
    ctrl.addPrinter = addPrinter;
    ctrl.editPrinter = editPrinter;
    ctrl.deletePrinter = deletePrinter;
    ctrl.getDefaultPrinter = getDefaultPrinter;
    ctrl.setAsSessionDefaultPrinter = setAsSessionDefaultPrinter;
    ctrl.getPrintersData = getPrintersData;

    // ////////////////////////////////////////////////
    // functions details
    // ////////////////////////////////////////////////
    function init() {
      if (localStorage.printer) {
        ctrl.defaultPrinter = JSON.parse(localStorage.printer);
      }

      ctrl.tableParam.setHighlightFn(highlightFn);
      return Printer.readAll().then(success, $q.reject);

      function success(datas) {
        const printers = extend(datas);
        ctrl.tableParam.setData(printers);
        if (!localStorage.printer) {
          getDefaultPrinter();
        }
      }

      function highlightFn(printer) {
        // use local storage if available
        if (ctrl.defaultPrinter) {
          return ctrl.defaultPrinter.id === printer.id ? highlightActive : highlightDefult;
        }
        return printer.is_default ? highlightActive : highlightDefult;
      }
    }

    function addPrinter($event) {
      const fields = Printer.getAddFields();

      nvDialog.showSingle($event, {
        templateUrl: 'views/container/printers/dialog/printer-add.dialog.html',
        cssClass: 'printer-add',
        theme: 'nvGreen',
        scope: _.assign(nvEditModelDialog.scopeTemplate({ onSave: create }), { fields: fields }),
      }).then(success);

      function create(data) {
        return Printer.create(data);
      }

      function success(printer) {
        if (printer.is_default) {
          setAsSessionDefaultPrinter(printer, false);
          setOtherPrintersIsDefaultToFalse(printer.id);
        }
        ctrl.tableParam.mergeIn(processPrinter(printer), 'id');
      }
    }

    function editPrinter($event, printer) {
      const model = _.cloneDeep(printer);
      const fields = Printer.getEditFields(model);

      nvDialog.showSingle($event, {
        templateUrl: 'views/container/printers/dialog/printer-edit.dialog.html',
        cssClass: 'printer-edit',
        scope: _.assign(nvEditModelDialog.scopeTemplate({ onSave: update }), { fields: fields }),
      }).then(success);

      function update(data) {
        return Printer.update(data.id, data);
      }

      function success(result) {
        if (result.is_default) {
          setAsSessionDefaultPrinter(result, false);
          setOtherPrintersIsDefaultToFalse(result.id);
        }
        ctrl.tableParam.mergeIn(processPrinter(result), 'id');
      }
    }

    function deletePrinter($event, printer) {
      nvDialog.confirmDelete($event, {
        content: `Are you sure you want to permanently delete'${printer.name}'?`,
      }).then(onDelete);

      function onDelete() {
        return Printer.delete(printer.id).then(success);
      }

      function success() {
        _.remove(ctrl.printers, { id: printer.id });
        ctrl.tableParam.remove(processPrinter(printer), 'id');
      }
    }

    function getDefaultPrinter() {
      Printer.getDefaultPrinter().then(success, $q.reject);

      function success(printer) {
        if (printer) { setAsSessionDefaultPrinter(printer, false); }
      }
    }

    function setAsSessionDefaultPrinter(printer, showToastMessage = true) {
      // cancel if same id selected
      if (ctrl.defaultPrinter.id === printer.id) {
        return;
      }
      let printers = _.castArray(_.cloneDeep(printer));
      if (ctrl.defaultPrinter.id) {
        const currentDefault = _.cloneDeep(ctrl.defaultPrinter);
        printers = _.concat(printers, _.castArray(currentDefault));
      }
      localStorage.printer = JSON.stringify(printer);
      ctrl.defaultPrinter = JSON.parse(localStorage.printer);

      ctrl.tableParam.mergeIn(printers, 'id');
      if (showToastMessage) { nvToast.success('Set successfully'); }
    }

    // ////////////////////////////////////////////////
    // Helper Functions
    // ////////////////////////////////////////////////
    function extend(printers) {
      return _.map(printers, (printer) => {
        processPrinter(printer);
        return printer;
      });
    }

    function processPrinter(printer) {
      printer.is_default = !!printer.is_default;
      return printer;
    }

    function setOtherPrintersIsDefaultToFalse(id) {
      _.forEach(ctrl.printers, (printer) => {
        if (printer.id !== id && printer.is_default === true) {
          printer.is_default = false;
          ctrl.tableParam.mergeIn(processPrinter(printer), 'id');
        }
      });
    }
    function getPrintersData() {
      if (ctrl.tableParam) {
        return ctrl.tableParam.getTableData();
      }
      return 0;
    }
  }
}());
