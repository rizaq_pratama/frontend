(function controller() {
  angular
    .module('nvOperatorApp.controllers')
    .controller('RouteManifestEditWaypointDialogController', RouteManifestEditWaypointDialogController);

  RouteManifestEditWaypointDialogController.$inject = [
    '$mdDialog', 'nvDialog', 'routeId', 'waypoint', 'failureReasons', 'otherParam',
    'Waypoint', 'nvTranslate', '$timeout',
  ];

  function RouteManifestEditWaypointDialogController(
    $mdDialog, nvDialog, routeId, waypoint, failureReasons, otherParam,
    Waypoint, nvTranslate, $timeout
  ) {
    // variables
    const ctrl = this;
    const WAYPOINT = _.cloneDeep(waypoint);

    ctrl.Waypoint = Waypoint;
    ctrl.isPickupOnly = otherParam.isPickupOnly;
    ctrl.isDeliveryOnly = otherParam.isDeliveryOnly;

    ctrl.formSubmitState = 'idle';
    ctrl.action = null;

    ctrl.chosenFailureReasons = [];

    // functions
    ctrl.chooseAction = chooseAction;
    ctrl.failureReasonOnChange = failureReasonOnChange;
    ctrl.isFormSubmitDisabled = isFormSubmitDisabled;
    ctrl.showConfirmationDialog = showConfirmationDialog;
    ctrl.onCancel = onCancel;

    // start
    init();

    // functions details
    function init() {
      addFailureReasonSelectionRowIfApplicable(null);
    }

    function chooseAction($event, statusAction) {
      ctrl.action = statusAction;

      if (ctrl.action === Waypoint.UPDATE_STATUS_ACTION.SUCCESS) {
        if (_.size(WAYPOINT.orderCods) > 0) {
          showCodCollectionDialog();
        } else {
          showConfirmationDialog($event);
        }
      }
    }

    function failureReasonOnChange(failureReason) {
      $timeout(() => {
        const failureReasonIndex = _.findIndex(ctrl.chosenFailureReasons, [
          'parentId', failureReason.parentId,
        ]);

        if (failureReasonIndex >= 0) {
          ctrl.chosenFailureReasons.splice(
            failureReasonIndex + 1,
            (_.size(ctrl.chosenFailureReasons) - (failureReasonIndex + 1))
          );
        }

        addFailureReasonSelectionRowIfApplicable(failureReason.selectedId);
      });
    }

    function showConfirmationDialog($event, codDatas) {
      nvDialog.confirmSave($event, {
        title: nvTranslate.instant('commons.are-you-sure'),
        content: getContent(),
        ok: nvTranslate.instant('commons.proceed'),
        skipHide: true,
      }).then(onConfirm);

      function getContent() {
        if (ctrl.action === Waypoint.UPDATE_STATUS_ACTION.SUCCESS) {
          return nvTranslate.instant('container.route-manifest.success-action-confirmation-message');
        }

        return nvTranslate.instant('container.route-manifest.fail-action-confirmation-message');
      }

      function onConfirm() {
        const payload = {
          action: ctrl.action,
        };

        if (ctrl.action === Waypoint.UPDATE_STATUS_ACTION.SUCCESS && _.size(codDatas) > 0) {
          const orderIds = _(codDatas)
            .filter(codData => codData.isCodCollected)
            .map(codData => codData.orderId)
            .value();
          _.assign(payload, { cod_collected_order_ids: orderIds });
        }

        if (ctrl.action === Waypoint.UPDATE_STATUS_ACTION.FAIL) {
          const lastChosenFailureReason = _.last(ctrl.chosenFailureReasons);
          payload.failure_reason_id = lastChosenFailureReason.selectedId;
        }

        ctrl.formSubmitState = 'waiting';
        Waypoint.updateStatus(routeId, WAYPOINT.id, payload).then(success, failure);


        function success() {
          ctrl.formSubmitState = 'idle';
          $mdDialog.hide();
        }

        function failure() {
          ctrl.formSubmitState = 'idle';
        }
      }
    }

    function showCodCollectionDialog($event) {
      const orderCods = WAYPOINT.orderCods;

      nvDialog.showSingle($event, {
        templateUrl: 'views/container/route-manifest/dialog/collect-cod/collect-cod.dialog.html',
        cssClass: 'route-manifest-collect-cod',
        theme: 'nvBlue',
        controller: 'RouteManifestCollectCodDialogController',
        controllerAs: 'ctrl',
        locals: {
          orderCods: orderCods,
        },
      }).then(data => showConfirmationDialog($event, data));
    }

    function isFormSubmitDisabled() {
      const lastChosenFailureReason = _.last(ctrl.chosenFailureReasons);
      return !lastChosenFailureReason.selectedId;
    }

    function onCancel() {
      $mdDialog.cancel();
    }

    function addFailureReasonSelectionRowIfApplicable(parentId) {
      const filteredFailureReasons = _.filter(failureReasons, ['parent', parentId]);
      const failureReasonsOptions = _.map(filteredFailureReasons, failureReason => ({
        displayName: `${failureReason.description} - ${failureReason.type}`,
        value: failureReason.id,
      }));

      if (_.size(failureReasonsOptions) > 0) {
        ctrl.chosenFailureReasons.push({
          parentId: parentId,
          options: failureReasonsOptions,
          selectedId: null,
        });
      }
    }
  }
}());
