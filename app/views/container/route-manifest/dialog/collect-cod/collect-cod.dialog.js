(function controller() {
  angular
    .module('nvOperatorApp.controllers')
    .controller('RouteManifestCollectCodDialogController', RouteManifestCollectCodDialogController);

  RouteManifestCollectCodDialogController.$inject = ['orderCods', '$mdDialog', 'nvCurrency', '$rootScope'];

  const API_STATE = {
    WAITING: 'waiting',
    IDLE: 'idle',
  };

  function RouteManifestCollectCodDialogController(orderCods, $mdDialog, nvCurrency, $rootScope) {
    const ctrl = this;

    ctrl.orderCods = _.cloneDeep(orderCods);
    ctrl.currencyCode = nvCurrency.getCode($rootScope.countryId);

    ctrl.buttonState = API_STATE.IDLE;
    ctrl.isCodLocked = isCodLocked;
    ctrl.onFinish = onFinish;

    function isCodLocked(orderCod) {
      return _.size(_.get(orderCod, 'cod.codCollections')) > 0;
    }

    function onFinish() {
      $mdDialog.hide(ctrl.orderCods);
    }
  }
}());
