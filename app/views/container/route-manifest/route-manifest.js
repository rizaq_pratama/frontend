(function controller() {
  angular
    .module('nvOperatorApp.controllers')
    .controller('RouteManifestController', RouteManifestController);

  RouteManifestController.$inject = [
    '$stateParams',
    '$mdDialog',
    '$q',
    'nvTable',
    'nvToast',
    'nvDateTimeUtils',
    'nvDistance',
    'nvDialog',
    'RouteManifest',
    'Order',
    'Shippers',
    'Waypoint',
    'nvFileUtils',
    'nvTimezone',
    'Driver',
  ];

  function RouteManifestController(
    $stateParams,
    $mdDialog,
    $q,
    nvTable,
    nvToast,
    nvDateTimeUtils,
    nvDistance,
    nvDialog,
    RouteManifest,
    Order,
    Shippers,
    Waypoint,
    nvFileUtils,
    nvTimezone,
    Driver,
  ) {
    const ctrl = this;
    ctrl.loading = false;

    ctrl.getRouteManifest = getRouteManifest;
    ctrl.viewWaypointDetails = viewWaypointDetails;
    ctrl.showEditWaypointDialog = showEditWaypointDialog;
    ctrl.routeId = null;
    ctrl.driverName = null;
    ctrl.routeDate = null;
    ctrl.waypointsById = {};

    ctrl.showNumberModel = '10';
    ctrl.showRouteSummary = true;
    ctrl.isSummaryAvailable = isSummaryAvailable;
    ctrl.showParcelCount = showParcelCount;
    ctrl.showWaypointType = showWaypointType;
    ctrl.showTimeslot = showTimeslot;
    ctrl.showCodCollection = showCodCollection;
    ctrl.showLocationAccuracy = showLocationAccuracy;

    ctrl.parcelCount = {};
    ctrl.waypointType = {};
    ctrl.timeslot = {};
    ctrl.codCollection = {};
    ctrl.locationAccuracy = {};

    const routeId = $stateParams.routeId;
    let shippersById = {};
    let failureReasons = [];

    if (!_.isInteger(+routeId)) {
      nvToast.error(`[${routeId}] is not a valid route id`);
      return;
    }

    ctrl.tableParam = nvTable.createTable(RouteManifest.fields);

    ctrl.tableParam.setDataPromise(getRouteManifest());

    function getRouteManifest() {
      ctrl.loading = true;

      return $q.all([
        RouteManifest.manifest(routeId),
        Driver.searchFailureReasons(),
      ]).then(success).finally(finallyFn);

      function success([
        manifestResponse,
        failureReasonResponse,
      ]) {
        if (_.get(failureReasonResponse, 'failureReasons')) {
          failureReasons = _.get(failureReasonResponse, 'failureReasons');
        }

        return processManifestResponse(manifestResponse);
      }

      function finallyFn() {
        ctrl.loading = false;
      }
    }

    function processManifestResponse(manifestResponse) {
      const deferred = $q.defer();
      let tableData = [];
      let waypoints = [];

      if (manifestResponse && manifestResponse.route) {
        ctrl.routeId = manifestResponse.route.id;
        ctrl.routeDate = moment(manifestResponse.route.date).format('YYYY-MM-DD');

        if (manifestResponse.route.driver) {
          ctrl.driverName = _.compact([
            manifestResponse.route.driver.firstName,
            manifestResponse.route.driver.lastName,
          ]).join(' ');
        }
      }

      if (manifestResponse && manifestResponse.route &&
        _.isArray(manifestResponse.route.waypoints)) {
        waypoints = manifestResponse.route.waypoints
          .slice(1, manifestResponse.route.waypoints.length - 1); // ignore dummy waypoints

        // check if need to call shipper search endpoint
        let shipperIds = [];
        _.forEach(waypoints, (waypoint) => {
          if (hasReservation(waypoint)) {
            shipperIds = _.concat(shipperIds, _.map(waypoint.reservations, 'shipperId'))
          }
        });
        shipperIds = _.compact(_.uniq(shipperIds));

        if (_.size(shipperIds) > 0) {
          Shippers.elasticCompactSearch({
            legacy_ids: shipperIds,
            size: _.size(shipperIds),
          }).then(readCompactSuccess).finally(readCompactFinallyFn);
        } else {
          processTableData(waypoints);
        }
      } else {
        deferred.resolve(tableData);
      }

      return deferred.promise;

      function readCompactSuccess(response) {
        // prepare shippers data for waypoint info
        shippersById = _.keyBy(_.get(response, 'details'), 'id');
      }

      function readCompactFinallyFn() {
        processTableData(waypoints);
      }

      function processTableData(theWaypoints) {
        tableData = _.map(theWaypoints, (waypoint, idx) => {
          ctrl.waypointsById[waypoint.id] = waypoint;
          const _codDetail = getCod(waypoint);
          return {
            sequence: idx,
            id: waypoint.id || '-',
            countD: getCountD(waypoint),
            countP: getCountP(waypoint),
            status: getStatus(waypoint),
            priority: getPriority(waypoint),
            address: getAddress(waypoint),
            trackingIds: getTrackingIds(waypoint),
            orderCods: getOrderCods(waypoint),
            timeWindow: getTimeWindow(waypoint),
            serviceEnd: getServiceEnd(waypoint),
            distance: getDistance(waypoint),
            reason: getReason(waypoint),
            addressee: getAddressee(waypoint),
            contact: getContact(waypoint),
            cod: getCodDisplay(_codDetail),
            comments: getComments(waypoint),
            _isNotInTimeWindow: isNotInTimeWindow(waypoint),
            _codDetail: _codDetail,
          };
        });
        setRouteSummary(tableData);
        deferred.resolve(tableData);
      }
    }

    function viewWaypointDetails($event, waypointId) {
      if (!ctrl.waypointsById[waypointId]) {
        nvToast.error(`Waypoint with id=${waypointId} not found`);
        return;
      }

      const waypoint = ctrl.waypointsById[waypointId];
      const fields = {
        waypointId: waypointId,
        waypointStatus: getStatus(waypoint),
        highestPriority: getPriority(waypoint),
        serviceType: getServiceType(waypoint),
        addressee: getAddressee(waypoint),
        contact: getContact(waypoint),
        recipient: getRecipient(waypoint),
        relationship: getRelationship(waypoint),
        reservations: getReservations(waypoint),
        deliveries: getDeliveries(waypoint),
        pickups: getPickups(waypoint),
        photos: [],
      };

      Waypoint.getPhotos(waypointId)
        .then(resp => (fields.photos = extendPhotos(resp)));

      const dialog0 = {
        templateUrl: 'views/container/route-manifest/dialog/waypoint-details.dialog.html',
        cssClass: 'waypoint-details',
        theme: 'nvBlue',
        scope: {
          fields: fields,
          onCancel: $mdDialog.cancel,
          onViewPODDelivery: onViewPODDelivery,
          isViewPODDeliveryDisabled: isViewPODDeliveryDisabled,
          onViewPODPickup: onViewPODPickup,
          isViewPODPickupDisabled: isViewPODPickupDisabled,
          onViewPODReservation: onViewPODReservation,
          isViewPODReservationDisabled: isViewPODReservationDisabled,
          downloadSelected: downloadSelected,
          downloadSignature: downloadSignature,
          isDownloadAvailable: isDownloadAvailable,
          podData: {},
        },
      };

      const dialog1 = {
        templateUrl: 'views/container/route-manifest/dialog/pod-delivery.dialog.html',
        cssClass: 'pod-delivery',
      };

      const dialog2 = {
        templateUrl: 'views/container/route-manifest/dialog/pod-pickup.dialog.html',
        cssClass: 'pod-pickup',
      };

      const dialog3 = {
        templateUrl: 'views/container/route-manifest/dialog/pod-reservation.dialog.html',
        cssClass: 'pod-reservation',
      };

      nvDialog.showMultiple($event, [dialog0, dialog1, dialog2, dialog3]);

      function extendPhotos(photos) {
        let idx = 0;
        return _.map(photos, (p) => {
          const id = idx += 1;
          const takenAt = nvDateTimeUtils.displayDateTime(
            nvDateTimeUtils.toMoment(p.taken_at, nvTimezone.getOperatorTimezone()),
            nvTimezone.getOperatorTimezone()
          );
          const filename = p.image_url.split('/').pop();
          return _.assign({}, {
            id: id - 1,
            isActive: false,
            url: p.image_url,
            title: `${filename} - ${takenAt}` });
        });
      }
    }

    function showEditWaypointDialog($event, waypoint) {
      nvDialog.showSingle($event, {
        templateUrl: 'views/container/route-manifest/dialog/edit-waypoint/edit-waypoint.dialog.html',
        cssClass: 'route-manifest-edit-waypoint',
        controller: 'RouteManifestEditWaypointDialogController',
        controllerAs: 'ctrl',
        locals: {
          routeId: ctrl.routeId,
          waypoint: waypoint,
          failureReasons: getFilteredFailureReasons(waypoint),
          otherParam: {
            isPickupOnly: isPickupOnly(waypoint),
            isDeliveryOnly: isDeliveryOnly(waypoint),
          },
        },
      }).then(onSet);

      function onSet() {
        RouteManifest.manifest(routeId).then(success);
      }

      function success(manifestResponse) {
        const tableData = processManifestResponse(manifestResponse);
        ctrl.tableParam.mergeIn(tableData, 'id');
      }

      function isDeliveryOnly(wp) {
        return wp.countD > 0 && wp.countP === 0;
      }

      function isPickupOnly(wp) {
        return wp.countD === 0 && wp.countP > 0;
      }

      function getFilteredFailureReasons(wp) {
        if (isDeliveryOnly(wp)) {
          return _.filter(failureReasons, reason => _.toLower(reason.mode) === 'delivery');
        } else if (isPickupOnly(wp)) {
          return _.filter(failureReasons, reason => _.toLower(reason.mode) === 'pickup');
        }
        return failureReasons;
      }
    }

    function onViewPODDelivery(data) {
      const self = this;
      self.podData.data = data;
      self.showDialog(1);
    }

    function onViewPODPickup(data) {
      const self = this;
      self.podData.data = data;
      self.showDialog(2);
    }

    function onViewPODReservation(data) {
      const self = this;
      self.podData.data = data;
      if (data && data.scannedParcels && data.scannedParcels.length > 0) {
        Order
          .getOrdersByTrackingIds(data.scannedParcels)
          .then((theData) => {
            self.podData.orders = theData;
            self.podData.isOrdersLoaded = true;
          });
      }
      self.showDialog(3);
    }

    function isViewPODDeliveryDisabled(data) {
      return !data || !data.hasBlobData;
    }

    function isViewPODPickupDisabled(data) {
      return !data || !data.hasBlobData;
    }

    function isViewPODReservationDisabled(data) {
      return !data || !data.hasBlobData;
    }

    function downloadSelected(photos) {
      _(photos)
        .filter(p => p.isActive)
        .forEach((p) => {
          const filename = p.url.split('/').pop();
          nvFileUtils.download(p.url, filename);
        });
    }

    function downloadSignature(url) {
      if (url) {
        const filename = url.split('/').pop();
        nvFileUtils.download(url, filename);
      }
    }

    function isDownloadAvailable(photos) {
      const selected = _.filter(photos, p => p.isActive);
      if (selected) {
        return selected.length > 0;
      }
      return false;
    }

    function setRouteSummary(tableData) {
      ctrl.parcelCount = getParcelCount(tableData);
      ctrl.waypointType = getWaypointType(tableData);
      ctrl.timeslot = getTimeslot(tableData);
      ctrl.codCollection = getCodCollection(tableData);
      ctrl.locationAccuracy = getLocationAccuracy(tableData);
    }

    function getParcelCount(tableData) {
      const parcelCount = _.reduce(tableData, (parcelCountInfo, rowData) => {
        const waypoint = ctrl.waypointsById[rowData.id];
        const transactions = waypoint.transactions || [];

        const ddTxns = _.filter(transactions, txn => txn.type === 'DD');
        const ppTxns = _.filter(transactions, txn => txn.type === 'PP');

        const ddDetailCounts = _(ddTxns)
          .countBy(txn => txn.status)
          .defaults({ All: ddTxns.length || 0 })
          .value();

        const ppDetailCounts = _(ppTxns)
          .countBy(txn => txn.status)
          .defaults({ All: ppTxns.length || 0 })
          .value();

        const totalDetailCounts = _(transactions)
          .countBy(txn => txn.status)
          .defaults({ All: transactions.length || 0 })
          .value();

        parcelCountInfo.Deliveries = aggregateRowSummary(
          parcelCountInfo.Deliveries, ddDetailCounts
        );
        parcelCountInfo['Pick Ups'] = aggregateRowSummary(parcelCountInfo['Pick Ups'], ppDetailCounts);
        parcelCountInfo.Total = aggregateRowSummary(parcelCountInfo.Total, totalDetailCounts);
        return parcelCountInfo;
      }, {});

      _.forEach(parcelCount, (parcelCountRow) => {
        const rowTotal = parcelCountRow.All.value;
        _.mapValues(parcelCountRow, (number) => {
          number.percent = `${(100 * (number.value / rowTotal)).toFixed(0)}%`;
        });
      });
      return parcelCount;
    }

    function getWaypointType(tableData) {
      const waypointType = _.reduce(tableData, (waypointTypeInfo, rowData) => {
        const waypoint = ctrl.waypointsById[rowData.id];
        const transactions = waypoint.transactions || [];
        const reservations = waypoint.reservations || [];

        // not distributionPoint
        const normalTxns = _.filter(transactions, txn => !txn.dpId);
        const normalRxns = _.filter(reservations, rxn => !rxn.dpId);
        const dpTxns = _.filter(transactions, txn => txn.type === 'DD' && txn.dpId);
        const dpRxns = _.filter(reservations, rxn => rxn.dpId);

        const txnDetailCounts = _(normalTxns)
          .countBy(txn => txn.status)
          .defaults({ All: normalTxns.length || 0 })
          .value();

        const rxnDetailCounts = _(normalRxns)
          .countBy(rxn => rxn.status)
          .defaults({ All: normalRxns.length || 0 })
          .value();

        const dpTxnDetailCounts = _(dpTxns)
          .countBy(txn => txn.status)
          .defaults({ All: dpTxns.length || 0 })
          .value();

        const dpRxnDetailCounts = _(dpRxns)
          .countBy(rxn => rxn.status)
          .defaults({ All: dpRxns.length || 0 })
          .value();

        waypointTypeInfo.Normal = aggregateRowSummary(waypointTypeInfo.Normal, txnDetailCounts);
        waypointTypeInfo.Reservation = aggregateRowSummary(
          waypointTypeInfo.Reservation, rxnDetailCounts
        );
        waypointTypeInfo['DP Drop Off'] = aggregateRowSummary(waypointTypeInfo['DP Drop Off'], dpTxnDetailCounts);
        waypointTypeInfo['DP Pick Up'] = aggregateRowSummary(waypointTypeInfo['DP Pick Up'], dpRxnDetailCounts);

        return waypointTypeInfo;
      }, {});

      _.forEach(waypointType, (waypointTypeRow) => {
        const rowTotal = waypointTypeRow.All.value;
        _.mapValues(waypointTypeRow, (number) => {
          number.percent = `${(100 * (number.value / rowTotal)).toFixed(0)}%`;
        });
      });

      return waypointType;
    }

    function getTimeslot(tableData) {
      const timeslot = _.reduce(tableData, (timeslotInfo, rowData) => {
        const waypoint = ctrl.waypointsById[rowData.id];
        const transactions = waypoint.transactions || [];

        const serviceEnds = getServiceEndMoments(transactions);
        const timeWindow = waypoint.timeWindow;
        const times = _.split(timeWindow, ' - ');

        if (hasTransaction(waypoint)) {
          const timeslotDetailCounts = _.reduce(serviceEnds, (timeslotDetail, serviceEnd) => {
            if (nvDateTimeUtils.isBeforeTimeslot(_.head(times), serviceEnd)) {
              timeslotDetail.early += 1;
            } else if (nvDateTimeUtils.isWithinTimeslot(_.head(times), _.last(times), serviceEnd)) {
              timeslotDetail.onTime += 1;
            } else if (nvDateTimeUtils.isAfterTimeslot(_.last(times), serviceEnd)) {
              timeslotDetail.late += 1;
            }
            return timeslotDetail;
          }, { early: 0, onTime: 0, late: 0 });

          timeslotDetailCounts.pending = transactions.length
            - timeslotDetailCounts.early
            - timeslotDetailCounts.onTime
            - timeslotDetailCounts.late;

          timeslotInfo[timeWindow] = aggregateRowSummary(timeslotInfo[timeWindow],
            timeslotDetailCounts,
            { pending: { value: 0 }, early: { value: 0 }, onTime: { value: 0 }, late: { value: 0 } }
          );
        }

        return timeslotInfo;
      }, {});

      _.forEach(timeslot, (timeslotRow) => {
        const rowTotal = _(timeslotRow).map(number => number.value).sum();
        _.mapValues(timeslotRow, (number) => {
          number.percent = `${(100 * (number.value / rowTotal)).toFixed(0)}%`;
        });
      });

      return timeslot;
    }

    function getCodCollection(tableData) {
      let total = 0;
      const codCollection = _.reduce(tableData, (sum, rowData) => {
        const cod = rowData._codDetail;
        sum.Collected.value += cod.collected;
        sum.Cancelled.value += cod.cancelled;
        sum.Pending.value += cod.aggregate - cod.collected - cod.cancelled;
        total += cod.aggregate;
        return sum;
      }, { Collected: { value: 0 }, Cancelled: { value: 0 }, Pending: { value: 0 } });

      _.forEach(codCollection, (count) => {
        count.percent = `${(100 * (count.value / total)).toFixed(0)}%`;
      });
      return codCollection;
    }

    function getLocationAccuracy(tableData) {
      let total = 0;
      const locationAccuracy = _.reduce(tableData, (count, rowData) => {
        const distance = rowData.distance;

        if (_.isNumber(distance)) {
          if (distance <= 100) {
            count.Accurate.value += 1;
          } else if (distance <= 1000) {
            count['Inaccurate < 1km'].value += 1;
          } else if (distance > 1000) {
            count['Inaccurate > 1km'].value += 1;
          }
          total += 1;
        }
        return count;
      }, { Accurate: { value: 0 }, 'Inaccurate < 1km': { value: 0 }, 'Inaccurate > 1km': { value: 0 } });

      _.forEach(locationAccuracy, (count) => {
        count.percent = `${(100 * (count.value / total)).toFixed(0)}%`;
      });

      return locationAccuracy;
    }

    function aggregateRowSummary(rowSummary, detailCounts, init) {
      const theInit = init || {
        Pending: { value: 0 },
        Success: { value: 0 },
        Fail: { value: 0 },
        All: { value: 0 },
      };
      return _(rowSummary)
        .defaults(theInit)
        .mapValues((aggregated, key) => {
          aggregated.value += detailCounts[key] || 0;
          return aggregated;
        }).value();
    }

    function isSummaryAvailable() {
      return showParcelCount()
        || showWaypointType()
        || showTimeslot()
        || showCodCollection()
        || showLocationAccuracy();
    }

    function showParcelCount() {
      return ctrl.parcelCount && getTotalSum(ctrl.parcelCount);
    }

    function showWaypointType() {
      return ctrl.waypointType && getTotalSum(ctrl.waypointType);
    }

    function showTimeslot() {
      return ctrl.timeslot && getTotalSum(ctrl.timeslot);
    }

    function showCodCollection() {
      return ctrl.codCollection && getTotalSum(ctrl.codCollection);
    }

    function showLocationAccuracy() {
      return ctrl.locationAccuracy && getTotalSum(ctrl.locationAccuracy);
    }

    function getTotalSum(summaryData) {
      // For a 2d summary, we need to go a step further
      return _(summaryData)
        .flatMap(row => row.value || _.map(row, x => x.value))
        .sum();
    }

    function getServiceType(waypoint) {
      if (hasReservation(waypoint)) {
        return 'Reservation';
      }
      if (isDelivery(waypoint)) {
        return 'Delivery';
      }
      if (isPickup(waypoint)) {
        return 'Pickup';
      }
      return '-';
    }

    function getCountD(waypoint) {
      return _.reduce(waypoint.transactions || [], (sum, txn) => (
        txn.type === 'DD' ? sum + 1 : sum
      ), 0);
    }

    function getCountP(waypoint) {
      return _.reduce(waypoint.transactions || [], (sum, txn) => (
        txn.type === 'PP' ? sum + 1 : sum
      ), 0);
    }

    function getStatus(waypoint) {
      let statuses = [];
      if (hasTransaction(waypoint)) {
        statuses = _(waypoint.transactions)
          .map(txn => txn.status)
          .uniq()
          .value();
      }
      if (hasReservation(waypoint)) {
        statuses = _(waypoint.reservations)
          .map(rxn => rxn.status)
          .uniq()
          .value();
      }
      return statuses.length > 1 ? 'Partial' : (_.head(statuses) || 'Unknown');
    }

    function getPriority(waypoint) {
      const priority = _(waypoint.transactions || [])
        .map(txn => txn.priority)
        .uniq()
        .max();
      return !priority ? 'NP' : `P ${priority}`;
    }

    function getAddress(waypoint) {
      return _.compact([
        waypoint.address1,
        waypoint.address2,
        waypoint.city,
        waypoint.country,
        waypoint.postcode,
      ]).join(' ') || '-';
    }

    function getTrackingIds(waypoint) {
      if (hasReservation(waypoint)) {
        return [
          'Reservation',
          _(waypoint.reservations)
            .map(rxn => (rxn.shipperId && shippersById[rxn.shipperId] && shippersById[rxn.shipperId].short_name) || '-')
            .join(', '),
          _(waypoint.reservations)
            .map(rxn => rxn.approxVolume || '-')
            .join(', '),
        ].join('\n');
      }

      return _(waypoint.transactions || [])
          .map(txn => txn.order && txn.order.trackingId)
          .compact()
          .value()
          .join(', ') || '-';
    }

    function getOrderCods(waypoint) {
      if (hasReservation(waypoint)) {
        return [];
      }
      return _(waypoint.transactions || [])
          .filter(txn => !!txn.order)
          .map(txn => txn.order)
          .filter(order => _.get(order, 'cod.goodsAmount') > 0)
          .map(order => ({
            orderId: order.id,
            cod: order.cod,
            trackingId: order.trackingId,
            isCodCollected: isCodCollected(order),
          }))
          .sortBy('trackingId')
          .value();

      function isCodCollected(order) {
        return _.size(_.get(order, 'cod.codCollections')) > 0;
      }
    }

    function getServiceEnd(waypoint) {
      const moments = _.concat(
        getServiceEndMoments(waypoint.transactions || []),
        getServiceEndMoments(waypoint.reservations || [])
      );

      const maxMoment = moment.max(moments);
      return moments.length > 0 && maxMoment.isValid()
        ? maxMoment.format('YYYY-MM-DD HH:mm:ss')
        : '-';
    }

    function getDistance(waypoint) {
      const txArrays = hasTransaction(waypoint) ?
        waypoint.transactions : waypoint.reservations;
      const distances = _(txArrays || [])
        .map((transaction) => {
          if (!_.get(transaction, 'blob.data.sign_coordinates')) {
            return null;
          }
          const signCoordinates = transaction.blob.data.sign_coordinates.split(',');
          const wLat = +waypoint.latitude;
          const wLng = +waypoint.longitude;
          const sLat = +_.last(signCoordinates);
          const sLng = +_.head(signCoordinates);
          if (signCoordinates.length !== 2 || _.isNaN(wLat) ||
            _.isNaN(wLng) || _.isNaN(sLat) || _.isNaN(sLng)) {
            return null;
          }
          return nvDistance.toKmFromLatLng(wLat, wLng, sLat, sLng) * 1000;
        }).compact().value();
      return distances.length === 0 ? '' : _.toInteger(_.sum(distances) / distances.length);
    }

    function getReason(waypoint) {
      return _(waypoint.transactions || [])
          .map(txn =>
            txn.blob && txn.blob.data && _.compact([
              txn.blob.data.name,
              txn.blob.data.relationship,
              txn.blob.data.notes,
            ]).join(', ')
          )
          .compact()
          .value()
          .join(', ') || '-';
    }

    function getRecipient(waypoint) {
      return _(waypoint.transactions || [])
          .map(txn => txn.blob && txn.blob.data && txn.blob.data.name)
          .compact()
          .value()
          .join(', ') || '-';
    }

    function getRelationship(waypoint) {
      return _(waypoint.transactions || [])
          .map(txn => txn.blob && txn.blob.data && txn.blob.data.relationship)
          .compact()
          .value()
          .join(', ') || '-';
    }

    function getAddressee(waypoint) {
      if (hasReservation(waypoint)) {
        return _(waypoint.reservations)
          .map(rxn => rxn.blob && rxn.blob.data && rxn.blob.data.name)
          .compact()
          .value()
          .join(', ') || '-';
      }

      return _(waypoint.transactions || [])
          .map(txn => _.trim(txn.name))
          .compact()
          .uniq()
          .value()
          .join(', ') || '-';
    }

    function getContact(waypoint) {
      return _(waypoint.transactions || [])
          .map(txn => _.trim(txn.contact))
          .compact()
          .uniq()
          .value()
          .join(', ') || '-';
    }

    function getCodDisplay(cod) {
      return cod.aggregate === 0 && cod.collected === 0
        ? 'No COD'
        : `${cod.collected.toFixed(2)} of ${cod.aggregate.toFixed(2)}`;
    }

    function getCod(waypoint) {
      const cod = _.reduce(waypoint.transactions || [], (sum, txn) => {
        if (txn.order && txn.order.cod) {
          sum.aggregate += getAggregateCOD(txn.order.cod);
          sum.collected += getCollectedCOD(txn.order.cod);
          if (txn.status === 'Cancelled') {
            sum.cancelled += getAggregateCOD(txn.order.cod);
          }
        }
        return sum;
      }, { aggregate: 0, collected: 0, cancelled: 0 });
      return cod;
    }

    function getComments(waypoint) {
      return _(waypoint.transactions || [])
          .map(txn => _.trim(txn.comments))
          .compact()
          .value()
          .join(', ') || '-';
    }

    function isNotInTimeWindow(waypoint) {
      const moments = getServiceEndMoments(waypoint.transactions || []);
      if (moments.length === 0) {
        return false;
      }
      const serviceEnd = moment.max(moments);
      const times = _.split(waypoint.timeWindow, ' - ');
      return !nvDateTimeUtils.isWithinTimeslot(_.head(times), _.last(times), serviceEnd);
    }

    function getTimeWindow(waypoint) {
      if (hasReservation(waypoint)) {
        const startDateTime = _.get(waypoint, 'timeWindowStart');
        const endDateTime = _.get(waypoint, 'timeWindowEnd');
        if (startDateTime && endDateTime) {
          const FORMAT = 'hA';
          const startTime = nvDateTimeUtils.displayFormat(moment(startDateTime), FORMAT);
          const endTime = nvDateTimeUtils.displayFormat(moment(endDateTime), FORMAT);
          return `${startTime} - ${endTime}`;
        }
        return '-';
      }
      return _.get(waypoint, 'timeWindow', '-');
    }

    function getServiceEndMoments(transactions) {
      return _(transactions).map((transaction) => {
        const serviceEndTime = moment(transaction.serviceEndTime);
        return !transaction.serviceEndTime || !serviceEndTime.isValid()
          ? null
          : serviceEndTime;
      }).compact().value();
    }

    function getReservations(waypoint) {
      return _(waypoint.reservations || [])
        .map(rxn => ({
          hasBlobData: !!(rxn.blob && rxn.blob.data),
          reservationId: rxn.id,
          shipper: _.get(rxn, 'blob.data.name') || '-',
          phone: _.get(rxn, 'blob.data.contact') || '-',
          status: rxn.status || 'Unknown',
          expectedNo: rxn.approxVolume || '-',
          collectedNo: getCollectedNo(rxn),
          scannedParcels: _.get(rxn, 'blob.data.scanned_parcels') || [],
          signature: _.get(rxn, 'rxn.blob.data.url') || null,
          failureReason: _.get(rxn, 'blob.data') ? _.compact([
            rxn.blob.data.notes,
            rxn.blob.data.comments,
          ]).join(', ') : null,
        }))
        .compact()
        .value();
    }

    function getDeliveries(waypoint) {
      return _(waypoint.transactions || [])
        .map((txn) => {
          if (txn.type === 'DD') {
            return {
              hasBlobData: !!(txn.blob && txn.blob.data),
              trackingId: _.get(txn, 'order.trackingId'),
              orderId: _.get(txn, 'order.id'),
              addressee: txn.name || '-',
              phone: txn.contact || '-',
              email: txn.email || '-',
              status: txn.status,
              deliveryQuantity: _.get(txn, 'blob.data.delivery_quantity') || null,
              receivedBy: _.get(txn, 'blob.data.name') || null,
              relationship: _.get(txn, 'blob.data.relationship') || '-',
              verificationCode: _.get(txn, 'blob.data.verification_code') || null,
              hiddenLocation: _.get(txn, 'blob.data.hidden_location') || '-',
              receiptDate: getServiceEndTime(txn) || '-',
              signature: _.get(txn, 'blob.data.url') || null,
              codCollected: getCODCollected(txn),
              failureReason: _.get(txn, 'blob.data') ? _.compact([
                txn.blob.data.failure_reason_code_id,
                txn.blob.data.comments,
              ]).join(', ') : null,
            };
          }
          return _.noop();
        })
        .compact()
        .value();
    }

    function getPickups(waypoint) {
      return _(waypoint.transactions || [])
        .map((txn) => {
          if (txn.type === 'PP') {
            return {
              hasBlobData: !!(txn.blob && txn.blob.data),
              trackingId: txn.order && txn.order.trackingId,
              orderId: txn.order && txn.order.id,
              addressee: txn.name || '-',
              phone: txn.contact || '-',
              email: txn.email || '-',
              status: txn.status,
              pickupQuantity: _.get(txn, 'blob.data.pickup_quantity') || null,
              receivedFrom: _.get(txn, 'blob.data.name') || null,
              relationship: _.get(txn, 'blob.data.relationship') || '-',
              receiptDate: getServiceEndTime(txn) || '-',
              signature: _.get(txn, 'blob.data.url'),
              failureReason: _.get(txn, 'blob.data') ? _.compact([
                txn.blob.data.failure_reason_code_id,
                txn.blob.data.comments,
              ]).join(', ') : null,
            };
          }
          return _.noop();
        })
        .compact()
        .value();
    }

    function getCollectedNo(rxn) {
      return (rxn.blob && rxn.blob.data && rxn.blob.data.received_parcels) || 0;
    }

    function getServiceEndTime(txn) {
      const m = moment(txn.serviceEndTime);
      return txn.serviceEndTime && m.isValid() && m.format('YYYY-MM-DD HH:mm:ss');
    }

    function getCODCollected(txn) {
      return !txn.order || !txn.order.cod
        ? 'No COD'
        : `${getCollectedCOD(txn.order.cod).toFixed(2)} of ${getAggregateCOD(txn.order.cod).toFixed(2)}`;
    }

    function getAggregateCOD(cod) {
      return (+cod.goodsAmount || 0) + (+cod.shippingAmount || 0);
    }

    function getCollectedCOD(cod) {
      return _.reduce(cod.codCollections, (sum, codCollection) => (
        sum + (+codCollection.collectedSum || 0)
      ), 0);
    }

    function hasReservation(waypoint) {
      return waypoint.reservations && waypoint.reservations.length > 0;
    }

    function hasTransaction(waypoint) {
      return waypoint.transactions && waypoint.transactions.length > 0;
    }

    function isDelivery(waypoint) {
      const lastTransaction = _.last(waypoint.transactions || []);
      return lastTransaction && lastTransaction.type === 'DD';
    }

    function isPickup(waypoint) {
      const lastTransaction = _.last(waypoint.transactions || []);
      return lastTransaction && lastTransaction.type === 'PP';
    }
  }
}());
