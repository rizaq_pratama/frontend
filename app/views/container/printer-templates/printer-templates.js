(function Controller() {
  angular
    .module('nvOperatorApp.controllers')
    .controller('PrinterTemplatesController', PrinterTemplatesController);

  PrinterTemplatesController.$inject = [
    '$q',
    '$timeout',
    'Printer',
    'LabelFont',
    'nvAutocomplete.Data',
    'nvToast',
    'nvTranslate',
  ];

  function PrinterTemplatesController(
    $q,
    $timeout,
    Printer,
    LabelFont,
    nvAutocompleteData,
    nvToast,
    nvTranslate
  ) {
    // ////////////////////////////////////////////////
    // variables
    // ////////////////////////////////////////////////
    const ctrl = this;

    let printTemplates = [];
    const printTemplatesMap = {};

    ctrl.variablesMap = {};
    ctrl.printTemplatesSelection = { value: null };
    ctrl.template = '';
    ctrl.submitState = 'idle';
    ctrl.previewState = 'idle';
    ctrl.labelPreview = null;
    ctrl.fontOptions = LabelFont.getOptions();

    ctrl.getAll = getAll;
    ctrl.setPrintTemplateChange = setPrintTemplateChange;
    ctrl.printStamp = printStamp;
    ctrl.togglePreview = togglePreview;
    ctrl.showPreview = false;

    // ////////////////////////////////////////////////
    // function details
    // ////////////////////////////////////////////////

    function getAll() {
      return Printer.searchPrinterTemplates().then(success, $q.reject);

      function success(response) {
        printTemplates = response;
        ctrl.printTemplatesSelection.options = _.map(printTemplates, (template) => {
          return {
            value: template.id,
            displayName: template.name,
          };
        });

        _.forEach(printTemplates, (printTemplate) => {
          printTemplatesMap[printTemplate.id] = printTemplate;
        });
      }
    }

    function setPrintTemplateChange() {
      ctrl.showPreview = false;
      $timeout(() => {
        chooseTemplate(printTemplatesMap[ctrl.printTemplatesSelection.value].template);
      });
    }

    function togglePreview() {
      if (ctrl.showPreview) {
        showLabel();
      } else {
        showPreview();
      }

      function showPreview() {
        const label = compileLabel(angular.copy(ctrl.template));
        ctrl.previewState = 'waiting';
        return Printer.getPreview(label).then((preview) => {
          ctrl.labelPreview = preview;
          ctrl.previewState = 'idle';
          ctrl.showPreview = true;
        });
      }

      function showLabel() {
        ctrl.showPreview = false;
      }
    }

    function printStamp() {
      ctrl.submitState = 'waiting';
      generateLabelsPromise().then((labels) => {
        printing(labels);
      });

      function printing(labels) {
        if (_.size(labels) > 0) {
          const labelToProcess = labels.splice(0, 1)[0];

          Printer.printLabel(labelToProcess).then(printingSuccess, printingFailure);
        } else {
          success();
        }

        function printingSuccess() {
          printing(labels);
        }

        function printingFailure() {
          printing(labels);
        }
      }

      function success() {
        ctrl.submitState = 'idle';
        nvToast.success(nvTranslate.instant('container.stamp-printing.stamp-printed'));
      }
    }

    // ////////////////////////////////////////////////
    // function helpers
    // ////////////////////////////////////////////////

    function chooseTemplate(template) {
      if (template == null) {
        ctrl.template = '';
        ctrl.variablesMap = {};
      }
      ctrl.template = template;
      // cleanup
      ctrl.variablesMap = {};

      _.forEach(ctrl.template.match(/{{[^{]+}}/g), (variable) => {
        const expression = variable.replace(/{{([^{]+)}}/, '$1');
        let displayName = expression.replace(/(.+)\[.*\]*/, '$1');
        let value = '';
        let operations = expression.match(/\[([^\]]*)\]/);

        if (operations !== null) {
          operations = operations[1].split(',');
        }

        switch (displayName) {
          case 'printer_x_offset': value = 130; break;
          case 'printer_y_offset': value = 10; break;
          case 'tracking_id': value = 1; displayName = 'quantity'; break;
          case 'font': value = 1; break;
          default: break;
        }
        /*
         * @id: expression to be replaced: e.g. {{font[18,10]}}
         * @displayName: field to be shown: e.g. font
         * @operations: operations to be done on the input-value: e.g. 18, 10, uppercase
         * @value: input-value
         **/
        const property = {
          id: variable,
          displayName: displayName,
          operations: operations,
        };

        // initialize
        if (ctrl.variablesMap[displayName] == null) {
          ctrl.variablesMap[displayName] = [];
          ctrl.variablesMap[displayName].value = value;
        }

        ctrl.variablesMap[displayName].push(property);
      });
    }

    function doOperations(value, operations) {
      let evaluatedVar = value;

      _.forEach(operations, (op) => {
        // uppercase
        if (op === 'uppercase') {
          evaluatedVar = evaluatedVar.toUpperCase();
        }

        // limit
        if (op.indexOf('limit') !== -1) {
          evaluatedVar = evaluatedVar.substring(0, op.split(':')[1]);
        }

        // convert_to_zpl_hex
        if (op === 'convert_to_zpl_hex') {
          evaluatedVar = convertToZplHex(evaluatedVar);
        }
      });

      return evaluatedVar;
    }

    function toUTF8Array(str) {
      const utf8 = [];
      for (let i = 0; i < str.length; i++) {
        let charcode = str.charCodeAt(i);
        if (charcode < 0x80) {
          utf8.push(charcode);
        } else if (charcode < 0x800) {
          utf8.push(0xc0 | (charcode >> 6),
            0x80 | (charcode & 0x3f));
        } else if (charcode < 0xd800 || charcode >= 0xe000) {
          utf8.push(0xe0 | (charcode >> 12),
            0x80 | ((charcode >> 6) & 0x3f),
            0x80 | (charcode & 0x3f));
        } else { // surrogate pair
          i += 1;
          // UTF-16 encodes 0x10000-0x10FFFF by
          // subtracting 0x10000 and splitting the
          // 20 bits of 0x0-0xFFFFF into two halves
          charcode = 0x10000 + (((charcode & 0x3ff) << 10)
            | (str.charCodexAt(i) & 0x3ff));
          utf8.push(0xf0 | (charcode >> 18),
            0x80 | ((charcode >> 12) & 0x3f),
            0x80 | ((charcode >> 6) & 0x3f),
            0x80 | (charcode & 0x3f));
        }
      }
      return utf8;
    }

    function convertToZplHex(str) {
      let hex = '';

      _.forEach(str, (char) => {
        if (char === ' ') {
          hex += ' ';
        } else {
          const bytes = toUTF8Array(char);
          _.forEach(bytes, (byte) => {
            hex += `_${byte.toString(16)}`;
          });
        }
      });

      return hex;
    }

    function generateLabelsPromise() {
      let quantity = 1;

      if (ctrl.variablesMap.quantity) {
        quantity = parseInt(ctrl.variablesMap.quantity.value) || 1;
      }
      const labels = [];

      return Printer.getTrackingIds(
        quantity, printTemplatesMap[ctrl.printTemplatesSelection.value].id
      ).then((trackingIds) => {
        _.forEach(trackingIds, (trackingId) => {
          labels.push(compileLabel(ctrl.template, trackingId));
        });
        return labels;
      });
    }

    function compileLabel(lab, trackingId = 'nv-test-123') {
      let label = angular.copy(lab);

      _.forEach(ctrl.variablesMap, (variableList, key) => {
        const value = variableList.value;

        _.forEach(variableList, (variable) => {
          let evaluatedVar;

          switch (key) {
            case 'font':
              evaluatedVar = LabelFont.getZpl(value, variable.operations);
              break;

            case 'quantity':
              evaluatedVar = doOperations(trackingId, variable.operations);
              break;

            default:
              evaluatedVar = doOperations(value, variable.operations);
              break;
          }

          label = label.replace(variable.id, evaluatedVar);
        });
      });

      return label;
    }
  }
}());
