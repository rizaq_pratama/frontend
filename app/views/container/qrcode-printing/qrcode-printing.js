(function controller() {
  angular
    .module('nvOperatorApp.controllers')
    .controller('QrcodePrintingController', QrcodePrintingController);

  QrcodePrintingController.$inject = ['nvPrint'];

  function QrcodePrintingController(nvPrint) {
    const ctrl = this;

    ctrl.value = '';

    ctrl.isValid = () => {
      return ctrl.value.trim() !== '';
    };

    ctrl.print = () => {
      nvPrint.printNode(document.getElementsByTagName('nv-qrcode')[0]);
    };
  }
}());
