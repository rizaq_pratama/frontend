(function BulkAddressVerification() {
  angular
        .module('nvOperatorApp.controllers')
        .controller('BulkAddressVerificationController', controller);
  controller.$inject = ['$scope', 'nvFileSelectDialog', 'nvTranslate', 'Address',
         'nvButtonFilePickerType', '$mdDialog', 'nvToast', 'nvTableUtils', 'nvCsvParser'];


  function controller($scope, nvFileSelectDialog, nvTranslate, Address
        , nvButtonFilePickerType, $mdDialog, nvToast, nvTableUtils, nvCsvParser) {
    const ctrl = this;
    const READY_STATE = 'ready';
    const UPLOADING_STATE = 'uploading';
    const UPLOADED_STATE = 'uploaded';
    const CHUNK_SIZE = 500;

    const SUCCESS_COLUMNS = [
      'sn',
      'id',
      'address',
      'rack_sector',
      'coordinate',
      'latitude',
      'longitude',
    ];

    const ERROR_COLUMNS = [
      'sn',
      'id',
      'error',
      'details',
    ];
    const STATE = {
      WAITING: 'waiting',
      IDLE: 'idle',
    };

    ctrl.uploadSuccessMatchesState = STATE.IDLE;
    ctrl.openUploadDialog = openUploadDialog;
    ctrl.pageState = READY_STATE;
    ctrl.uploadSuccessMatches = updateWaypoints;
    ctrl.onUploadNewFile = onUploadNewFile;
    ctrl.uploadedFileName = '';
    ctrl.errors = [];
    ctrl.success = [];
    ctrl.incorrects = [];
    ctrl.SAMPLE_CSV = [{
      waypoint: 5259518,
      latitude: 1.32323,
      longitude: 103.1212,
    }];
    ctrl.SAMPLE_CSV_HEADER = ['waypoint', 'latitude', 'longitude'];

    function openUploadDialog($event) {
      nvFileSelectDialog.show($event, {
        title: nvTranslate.instant('container.bulk-address-verification.upload-address-csv'),
        fileSelect: {
          accept: nvButtonFilePickerType.CSV,
          maxFileCount: 1,
        },
        buttonOkTitle: nvTranslate.instant('commons.submit'),
        onFinishSelect: onFinishSelect,
        onDone: onDone,
      });
    }

    async function onFinishSelect(files) {
      ctrl.pageState = UPLOADING_STATE;
      ctrl.uploadedFileName = buildFileNames(files);
      reset();
      // parse the file, then create several files from it
      // take care the first row as it is needed for the header
      const { data, errors } = await nvCsvParser.parse(files[0]);
      if (errors && errors.length > 0) {
        nvToast.error('Error parsing csv');
        return null;
      }
      const header = data.shift();
      const date = new Date();
      const csvFiles = _.chunk(data, CHUNK_SIZE)
        .map((d) => {
          if (d.length === 1 && d[0] === '') {
            return null;
          }
          d.unshift(header);
          const csvText = nvCsvParser.unparseInstant(d);
          return csvText;
        })
        .filter(d => d)
        .map((csvText, index) => {
          const blob = new Blob([csvText], { type: 'text/csv' });
          return new File([blob], `${ctrl.uploadedFileName}-${index}.csv`, {
            type: 'text/csv',
            lastModified: date.getTime(),
          });
        });
      const uploadPromises = csvFiles.map(file => Address.uploadCSV(file));
      return Promise.all(uploadPromises).then(onSuccess);
      function onSuccess(results) {
        ctrl.pageState = UPLOADED_STATE;
        let waypointErrorsData = [];
        let waypointUpdatesData = [];
        _.each(results, ({ waypoint_errors, waypoint_updates }) => {
          waypointErrorsData = _.concat(waypointErrorsData, waypoint_errors);
          waypointUpdatesData = _.concat(waypointUpdatesData, waypoint_updates);
        });

        ctrl.errors = _.map(waypointErrorsData, (o, i) => (
          {
            ...o,
            sn: i + 1,
            details: o.details || '-',
          }
        ));
        ctrl.success = _.map(waypointUpdatesData, (o, i) => (
          {
            ...o,
            sn: i + 1,
            coordinate: `${o.latitude},${o.longitude}`,
            address: o.waypoint.address1,
          }
        ));
        ctrl.errorTableParams = nvTableUtils.generate(ctrl.errors,
          { columns: ERROR_COLUMNS, count: 10 });
        ctrl.successTableParams = nvTableUtils.generate(ctrl.success,
          { columns: SUCCESS_COLUMNS, count: 10 });
      }
    }

    function onDone() {
      return angular.noop();
    }

    function buildFileNames(files) {
      return _.chain(files)
                .map(o => (o.name))
                .join(', ')
                .value();
    }

    function reset() {
      ctrl.errors.length = 0;
      ctrl.success.length = 0;
      ctrl.incorrects.length = 0;
    }

    function onUploadNewFile($event) {
      showDialog($event).then(onLeave);
      function onLeave(result) {
        if (result) {
          reset();
          ctrl.pageState = READY_STATE;
        }
      }

      function showDialog(event) {
        return $mdDialog.show(
                    $mdDialog.confirm()
                        .targetEvent(event)
                        .title(nvTranslate.instant('container.bulk-address-verification.unsaved-progress'))
                        .content(nvTranslate.instant('container.bulk-address-verification.your-current-csv-is-not'))
                        .ok(nvTranslate.instant('container.bulk-address-verification.upload-new-file'))
                        .cancel(nvTranslate.instant('commons.cancel'))
                );
      }
    }

    function updateWaypoints() {
      const approvedArray = _.map(ctrl.success, (o) => {
        // eslint-disable-next-line no-unused-vars
        const { waypoint, ...rest } = o;
        return rest;
      });
      ctrl.uploadSuccessMatchesState = STATE.WAITING;
      const chunks = _.chunk(approvedArray, CHUNK_SIZE);
      return Promise.all(_.map(chunks, chunk => (Address.updateWaypoints(chunk))))
        .then(onSuccessUpdate)
        .then(onFinishUpdate, onFinishUpdate);

      function onSuccessUpdate(results) {
        // check updated count
        let waypoints = [];
        _.each(results, r => (waypoints = _.concat(waypoints, r.waypoints)));
        const successUpdated = waypoints.filter(w => (w.status));
        nvToast.success(nvTranslate.instant('container.bulk-address-verification.x-waypoints-updated', { count: successUpdated.length }));
        reset();
        ctrl.pageState = READY_STATE;
      }

      function onFinishUpdate() {
        ctrl.uploadSuccessMatchesState = STATE.IDLE;
      }
    }
  }
}());
