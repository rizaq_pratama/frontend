(function BulkyBatchController() {
  angular.module('nvOperatorApp.controllers')
        .controller('BulkyBatchController', controller);
  controller.$inject = ['Order', 'nvTable'];


  function controller(Order, nvTable) {
    const ctrl = this;

        // functions
    ctrl.triggerUpdate = triggerUpdate;

        // properties
    ctrl.from = null;
    ctrl.to = null;
    ctrl.tableParam = null;
    ctrl.data = null;
    ctrl.state = 'idle';
    ctrl.fields = {
      id: { displayName: 'commons.id', fieldName: 'id' },
      name: { displayName: 'commons.route-groups', fieldName: 'name' },
    };


    (function init() {
      ctrl.tableParam = nvTable.createTable(ctrl.fields, { showEndTableDescription: false });
    }());

    function triggerUpdate() {
      ctrl.state = 'waiting';
      return Order.triggerBulkyUpdate(
                ctrl.from, ctrl.to
            ).then((result) => {
              ctrl.data = result;
              ctrl.tableParam.setData(ctrl.data);
              ctrl.state = 'idle';
            });
    }
  }
}());
