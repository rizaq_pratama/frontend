(function controller() {
  angular
    .module('nvOperatorApp.controllers')
    .controller('ShipmentInboundController', ShipmentInboundController);

  ShipmentInboundController.$inject = ['WarehouseSweep', 'Hub', '$q', '$interval',
    'nvDialog', '$scope', 'nvTranslate', '$timeout', 'nvSoundService',
    '$log', 'nvTimezone', 'Outbound', 'Shipping', 'nvDateTimeUtils', '$rootScope', 'nvToast'];

  const PAGE = {
    HUB_SELECTION: 0,
    SCAN: 1,
  };

  function ShipmentInboundController(WarehouseSweep, Hub, $q, $interval,
    nvDialog, $scope, nvTranslate, $timeout, nvSoundService,
    $log, nvTimezone, Outbound, Shipping, nvDateTimeUtils, $rootScope, nvToast) {
    const ctrl = this;
    const SCAN_STATUS = {
      NOT_FOUND: 'Not Found',
      NIL: 'NIL',
      ON_HOLD: 'ON HOLD',
      PARCEL_STATUS_ERROR: 'PARCEL STATUS ERROR',
      ALERT: 'ALERT',
      RECOVERY: 'RECOVERY',
      NOT_ROUTED: 'NOT ROUTED',
      SET_ASIDE: 'SET ASIDE',
    };
    const FILE_REGEX = /[`~!@#$%^&*()_|+\-=;:'",.<>\{\}\[\]\\\/]/gi;
    const LOAD_PER_REQUEST = 500;
    ctrl.data = {};
    ctrl.scanData = {};
    ctrl.state = {
      inboundSync: false,
      dialog: false,
    };
    ctrl.PAGE = PAGE;
    ctrl.pageState = PAGE.HUB_SELECTION;
    ctrl.onSelectHub = onSelectHub;
    ctrl.onSyncOrders = onSyncOrder;
    ctrl.onAddPrefix = onAddPrefix;
    ctrl.onEditSelectedHub = onEditSelectedHub;
    ctrl.onKeyPressTrackingId = onKeyPressTrackingId;
    ctrl.onSelectInboundHub = onSelectInboundHub;
    ctrl.isShipmentFilterComplete = isShipmentFilterComplete;
    ctrl.onMultiselectChange = onMultiselectChange;
    ctrl.loadingSheet = false;
    let queuePromise = null;

    ctrl.requestQueue = [];
    ctrl.data.selectedShipmentIds = [];
    ctrl.data.selectedShipmentIdsAsString = '';

    const intervalPromise = $interval(() => {
      checkConnection();
      focusTrap();
    }, 500);

    $scope.$on('destroy', () => {
      $interval.cancel(intervalPromise);
    });

    init();

    function focusTrap() {
      if (ctrl.pageState === PAGE.SCAN && !ctrl.state.dialog) {
        $timeout(() => { $('#scan').focus(); });
      }
    }
    /**
     * Callback function upon selection of hub
     */
    function onSelectHub() {
      ctrl.loadingSheet = true;
      ctrl.pageState = PAGE.SCAN;
      return grabAll();
    }

    function grabAll() {
      resetPlayList();
      resetScanUi();
      ctrl.data.selectedShipmentIds = _(ctrl.multiselect.selected)
        .map(item => _.toNumber(_.get(item, 'id')))
        .uniq()
        .compact()
        .value();
      const warehouseSweepPromise = _.map(
        ctrl.data.selectedShipmentIds, id => WarehouseSweep.fetchWithShipment(id));
      return $q.all(warehouseSweepPromise)
        .then((results) => {
          ctrl.data = {
            ...ctrl.data,
            selectedShipmentIdsAsString: _.join(ctrl.data.selectedShipmentIds, ', '),
            urgent: [],
            driverData: [],
            parcelRoutingData: {},
            lastSynced: moment.tz(new Date(), nvTimezone.getOperatorTimezone()),
          };

          _.forEach(results, ({ urgents, routes, parcelRoutingData }, i) => {
            _.forEach(routes, route =>
              (route.shipmentId = Number(ctrl.data.selectedShipmentIds[i])));
            const parcelRoutingDataMap = _.mapValues(parcelRoutingData, val => ({
              ...val,
              shipmentId: Number(ctrl.data.selectedShipmentIds[i]),
              orderTags: _.size(val.tags) > 0 ? _.join(val.tags, ', ') : '-',
            }));
            ctrl.data = {
              ...ctrl.data,
              driverData: _.concat(ctrl.data.driverData, routes),
              parcelRoutingData: _.merge(ctrl.data.parcelRoutingData, parcelRoutingDataMap),
            };
          });
          playSingleAudio(nvSoundService.getReadySoundFile());
          ctrl.loadingSheet = false;
          ctrl.state.inboundSync = true;
        }, () => {
          ctrl.pageState = PAGE.HUB_SELECTION;
          ctrl.loadingSheet = false;
          init();
        });
    }

    /**
     * Callback function when user click Sync Order button
     */
    function onSyncOrder() {
      ctrl.loadingSheet = true;
      ctrl.state.inboundSync = false;
      return grabAll();
    }


    function onKeyPressTrackingId(event) {
      resetPlayList();
      if (event.which === 13) {
        resetScanUi();
        let shipmentId = null;
        const prefix = ctrl.data.prefix || '';
        const scan = `${prefix}${ctrl.data.trackingId}`;
        const parcelData = _.get(ctrl.data.parcelRoutingData, scan);

        if (parcelData) {
          // parcel exist
          shipmentId = _.get(parcelData, 'shipmentId');
          ctrl.scanData.orderTags = parcelData.orderTags;
          ctrl.scanData.zone = buildZoneName(parcelData);
          if (parcelData.routeId != null) {
            // show the route id
            ctrl.scanData.routeId = _.toString(parcelData.routeId);
            const routeData = _.find(ctrl.data.driverData, { route_id: parcelData.routeId });
            if (routeData) {
              ctrl.scanData.driverName = _.get(routeData, 'driver_name', '');
              ctrl.scanData.hubId = _.get(routeData, 'hub_id');
              ctrl.scanData.destinationHub = _.get(routeData, 'hub_name');
            } else {
              // route has different date
              ctrl.state.hasDifferentDate = true;
              ctrl.scanData.destinationHub = parcelData.responsibleHubName;
              $log.debug('has different date');
            }
            playHubname(parcelData);
          } else if (parcelData.rackSector != null) {
            // show rack sector
            $log.debug(`rack sector ${parcelData.rackSector}`);
            ctrl.scanData.destination = parcelData.rackSector;
            ctrl.scanData.routeId = SCAN_STATUS.NOT_ROUTED;
            ctrl.scanData.driverName = SCAN_STATUS.NIL;
            ctrl.scanData.destinationHub = parcelData.responsibleHubName;
            playHubname(parcelData);
          }

          // add confirm scans
          if (ctrl.scanData.hubId && ctrl.scanData.hubId === ctrl.data.hub.id) {
            confirmScans(ctrl.scanData.routeId, scan, ctrl.scanData.hubId);
          }

          // check for set aside
          if (parcelData.setAside) {
            $log.debug('set aside');
            ctrl.scanData.routeId = SCAN_STATUS.SET_ASIDE;
            ctrl.scanData.driverName = parcelData.setAsideGroup;
            ctrl.scanData.scanError = true;
            ctrl.scanData.isSetAside = true;
          }

          // check for rts
          ctrl.data.isRtsed = _.get(parcelData, 'rtsed', false);
          $log.debug(`is rtsed ? : ${ctrl.data.isRtsed}`);
          // check for priorities

          // 0 (Non Priority) = no indication
          // 1 (Normal Priority) = Yellow background
          // 2-90 (Late priorities) = Orange Background
          // 91 and above (Urgent priorities) = Red background

          if (parcelData.priorityLevel != null && parcelData.priorityLevel >= 0) {
            ctrl.scanData.priorityLevel = _.toString(parcelData.priorityLevel);
            if (parcelData.priorityLevel === 0) {
              ctrl.scanData.priorityLevelColorCode = '';
            } else if (parcelData.priorityLevel === 1) {
              ctrl.scanData.priorityLevelColorCode = 'yellow';
            } else if (parcelData.priorityLevel > 1 && parcelData.priorityLevel < 91) {
              ctrl.scanData.priorityLevelColorCode = 'orange';
            } else {
              ctrl.scanData.priorityLevelColorCode = 'red';
            }
          }
        } else {
          // show not found
          $log.debug('not found');
          ctrl.scanData.destinationHub = SCAN_STATUS.NOT_FOUND;
          ctrl.scanData.scanError = true;
          ctrl.scanData.routeId = SCAN_STATUS.NOT_FOUND;
          ctrl.scanData.driverName = SCAN_STATUS.NIL;
          ctrl.scanData.zone = SCAN_STATUS.NIL;
          playSingleAudio(nvSoundService.getNotFoundSoundFile());
        }
        ctrl.data.lastScannedTrackingId = _.clone(scan);
        ctrl.data.trackingId = '';
        warehouseSweepCountForShipment(scan, ctrl.data.hub.id, shipmentId, ctrl.scanData);
      }
    }

    function warehouseSweepCountForShipment(trackingId, selectedHubId, shipmentId, data) {
      // eslint-disable-next-line prefer-rest-params
      const args = arguments;
      const alert = data.hasDifferentDate ? `ROUTE ${data.routeId} HAS DIFFERENT DATE` : '';
      const resultData = `${data.destinationHub || ''} ${alert} ${data.destination || ''} ${data.driverName || ''}`;
      return WarehouseSweep
        .warehouseSweepForShipment(trackingId, selectedHubId, shipmentId, resultData)
        .then(
          result => (successCallback(result)),
          result => (failureCallback(result, 'warehousesweep', warehouseSweepCountForShipment, args))
        );
    }

    function confirmScans(routeId, trackingId, hubId) {
      // eslint-disable-next-line prefer-rest-params
      const args = arguments;
      return Outbound.confirmScans(routeId, trackingId, hubId)
        .then(
          result => (successCallback(result)),
          result => (failureCallback(result, 'confirmscans', confirmScans, args))
        );
    }

    function buildZoneName(parcelData) {
      if (!parcelData) {
        return SCAN_STATUS.NIL;
      }
      return `${parcelData.rackSector} (${parcelData.zoneName})`;
    }

    function successCallback(result) {
      $log.debug(result);
    }

    function failureCallback(response, fnName, fn, args) {
      $log.debug(response);
      $log.debug(fn);
      $log.debug(args);

      if (response.status === 404 || response.status === -1) {
        if (!findInTheQueue(ctrl.requestQueue, fn, args[0])) {
          ctrl.requestQueue.push({ fn: fn, args: args });
          startInterval();
        } else {
          $log.debug(`${args[0]} already in offline queue`);
        }
      } else {
        $log.debug(`error posting to api ${fnName}. status ${response.status}`);
      }
    }

    function startInterval() {
      if (!queuePromise) {
        queuePromise = $interval(function recall() {
          const length = ctrl.requestQueue.length;
          if (length > 0) {
            for (let i = 0; i < length; ++i) {
              ctrl.requestQueue[i].fn.apply(this, ctrl.requestQueue[i].args);
            }
            ctrl.requestQueue.splice(0, length);
          } else {
            $interval.cancel(queuePromise);
            queuePromise = null;
          }
        }, 5000);
      }
    }

    /**
     * Callback function when user click Add Prefix button
     */
    function onAddPrefix($event) {
      ctrl.state.dialog = true;
      return nvDialog.showSingle($event, {
        templateUrl: 'views/container/global-inbound/dialogs/prefix/' +
                'prefix.html',
        theme: 'nvGreen',
        cssClass: 'nv-global-inbound-prefix',
        controllerAs: 'ctrl',
        controller: 'GlobalInboundPrefixDialogController',
        locals: {
          data: ctrl.data,
        },

      }).then((result) => {
        ctrl.data.prefix = result.prefix;
        if (ctrl.data.prefix) {
          ctrl.state.isPrefixSet = true;
        } else {
          ctrl.state.isPrefixSet = false;
        }
        ctrl.state.dialog = false;
      });
    }

    /**
     * Callback function when user click change selected hub button
     */
    function onEditSelectedHub() {
      return nvDialog.confirmDelete(null, {
        title: nvTranslate.instant('container.parcel-sweeper.confirm-change-hub'),
        content: nvTranslate.instant('container.parcel-sweeper.confirm-change-hub-text'),
        ok: nvTranslate.instant('container.parcel-sweeper.change-hub'),
      }).then(() => {
        ctrl.pageState = PAGE.HUB_SELECTION;
        ctrl.data.hub = null;
      });
    }

    /**
     * background checking for connection
     */
    function checkConnection() {
      if (navigator.onLine) {
        ctrl.state.isConnected = true;
      } else {
        ctrl.state.isConnected = false;
      }
    }

    // audio function

    function playHubname(parcelData) {
      resetPlayList();
      addToPlaylist(nvSoundService.getRackSoundFileWord(_.replace(parcelData.responsibleHubName, FILE_REGEX, '')));
      playAudio();
    }
    function playAudio() {
      $timeout(() => { $scope.mediaPlayer.play(0); });
    }

    function playSingleAudio(src) {
      $scope.audioPlaylist.push({
        src: src,
        type: 'audio/mp3',
      });
      $timeout(() => { $scope.mediaPlayer.play(0); });
    }

    function resetPlayList() {
      $scope.audioPlaylist = [];
    }

    function addToPlaylist(src) {
      $scope.audioPlaylist = _.concat($scope.audioPlaylist, _.map(_.castArray(src), s => ({
        src: s,
        type: 'audio/mp3',
      })));
    }

    function resetScanUi() {
      ctrl.state.hasDifferentDate = false;
      ctrl.scanData = {};
    }

    $scope.$on('$destroy', () => {      
      if (queuePromise || ctrl.requestQueue.length > 0) {
        // eslint-disable-next-line no-alert
        alert(nvTranslate.instant('container.shipment-inbound.still-processing-qeueu'));
      }
    });

    function init() {
      ctrl.loadingSheet = true;
      ctrl.shipmentFetched = false;
      ctrl.isValidExtra = false;
      return $q.all([
        Hub.read(),
      ]).then(onFinishFetch);

      function onFinishFetch(results) {
        ctrl.data.hubs = Hub.toOptionsWithObjectValue(results[0]);
        ctrl.loadingSheet = false;
      }
    }

    function findInTheQueue(queue, fn, trackingId) {
      const qu = _.find(queue, q => (q.fn === fn && q.args[0] === trackingId));
      return qu;
    }

    function onSelectInboundHub(hub) {
      ctrl.shipmentFetched = false;
      if (!hub) {
        return angular.noop();
      }

      const today = moment.tz(new Date(), nvTimezone.getOperatorTimezone());
      const thirtyDaysBefore = moment().subtract(30, 'days');
      const filter = {
        dest_hub: _.castArray({ system_id: _.upperCase($rootScope.domain), id: hub.id }),
        shipment_status: ['COMPLETED'],
        created: {
          from: nvDateTimeUtils.displayFormat(nvDateTimeUtils.toSOD(thirtyDaysBefore),
            nvDateTimeUtils.FORMAT_ISO_TZ, 'utc'),
          to: nvDateTimeUtils.displayFormat(nvDateTimeUtils.toEOD(today),
            nvDateTimeUtils.FORMAT_ISO_TZ, 'utc'),
        },
        shipment_type: [],
      };
      return Shipping.searchPagev1(filter, LOAD_PER_REQUEST, 1)
        .then((response) => {
          if (response.shipments) {
            initMultiSelect();
            _.assign(ctrl.multiselect, { options: Shipping.toOptions(_.orderBy(response.shipments, 'id', 'desc')) });
          } else {
            nvToast.info(nvTranslate.instant('container.shipment-inbound.no-shipment-for-the-selected-hub'));
          }
        });
    }

    function initMultiSelect() {
      ctrl.multiselect = {};

      ctrl.multiselect = {
        selected: [],
        extra: { addText: 'Add Shipment', itemType: 'Shipment ID' },
        options: [],
      };
      ctrl.shipmentFetched = true;
    }

    function isShipmentFilterComplete() {
      return !!ctrl.data.hub && _.size(_.get(ctrl.multiselect, 'selected')) > 0 && ctrl.isValidExtra;
    }

    function onMultiselectChange(views, items) {
      ctrl.isValidExtra = _.size(views) === _.size(items);
    }
  }
}());
