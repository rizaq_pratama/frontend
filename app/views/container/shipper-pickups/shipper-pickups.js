(function controller() {
  angular
    .module('nvOperatorApp.controllers')
    .controller('ShipperPickupsController', ShipperPickupsController);

  ShipperPickupsController.$inject = ['ShipperPickupsFilter', 'nvTable', 'nvBackendFilterUtils',
    'ShipperPickups', 'Route', 'nvDialog', 'nvDateTimeUtils', 'nvToast', 'nvTimezone', 'nvFileUtils',
    'nvCsvParser', 'nvEditModelDialog', 'Reservation', '$mdDialog', '$timeout', 'nvTranslate', 'Driver',
    'Shippers', '$q', 'nvRequestUtils'];

  const API_STATE = {
    IDLE: 'idle',
    WAITING: 'waiting',
  };

  const PAGE = {
    FILTER: 0,
    DATA: 1,
  };

  const EMPTY_DUMMY = [];
  let SHIPPERS_MAP = [];
  let ADDRESSES_MAP = [];
  let DRIVERS_MAP = [];
  let FAILURE_REASONS_MAP = [];

  const CSV_HEADER = ['Reservation ID', 'Name', 'Pickup Address', 'Route ID', 'Driver Name', 'Priority Level', 'Ready By', 'Last By', 'Type', 'Status', 'Created Date', 'Service Time', 'Approx Volume', 'Failure Reason', 'Comments'];
  const field = ['id', 'name', 'pickupAddress', 'routeId', 'driverName', 'priorityLevel', 'readyBy', 'lastBy', 'type', 'status', 'createdDate', 'serviceTime', 'approxVolume', 'failureReason', 'comments'];

  const MAX_DATA = 1000;

  function ShipperPickupsController(ShipperPickupsFilter, nvTable, nvBackendFilterUtils,
    ShipperPickups, Route, nvDialog, nvDateTimeUtils, nvToast, nvTimezone, nvFileUtils,
    nvCsvParser, nvEditModelDialog, Reservation, $mdDialog, $timeout, nvTranslate, Driver,
    Shippers, $q, nvRequestUtils) {
    const ctrl = this;
    const maxSelectable = 30;

    ctrl.state = {
      currentIndex: 0,
      lastIndex: undefined,
      currentParams: null,
    };
    ctrl.data = {};
    ctrl.form = {};

    ctrl.data.tableParam = null;

    ctrl.function = {
      init: init,
      loadFromFilter: loadFromFilter,
      showEditFilter: showEditFilter,
      downloadCsvDisabled: downloadCsvDisabled,
      downloadCsv: downloadCsv,
      createReservation: createReservation,
      bulkActionDisabled: bulkActionDisabled,
      getSelectedSize: getSelectedSize,
      editReservation: editReservation,
      showReservationDetails: showReservationDetails,
      getReservationClass: getReservationClass,
      editReservationDisabled: editReservationDisabled,
      finishReservationDisabled: finishReservationDisabled,
      refreshRoute: refreshRoute,
      openBulkRouteAssignmentDialog: openBulkRouteAssignmentDialog,
      openBulkPriorityUpdateDialog: openBulkPriorityUpdateDialog,
      openForceFinishReservationDialog: openForceFinishReservationDialog,
      isRoutesAvailable: isRoutesAvailable,
      removeRouteFromReservations: removeRouteFromReservations,
    };

    function init() {
      ctrl.state.page = PAGE.FILTER;
      ctrl.state.loadState = API_STATE.IDLE;
      ctrl.state.refreshRoute = API_STATE.IDLE;

      // filter
      const filters = ShipperPickupsFilter.init();
      ctrl.data.possibleFilters = filters;
      ctrl.data.selectedFilters = _.remove(ctrl.data.possibleFilters,
          filter => filter.showOnInit === true);

      return $q.all([
        Route.fetchRoute(getRouteDateParam()),
        Driver.searchFailureReasons(),
      ])
      .then(onSuccess);

      function onSuccess(datas) {
        // prefetch here because the call takes very long
        ctrl.data.routes = extendRoutes(datas[0]);
        ctrl.data.failureReasons = _.get(datas[1], 'failureReasons');
      }
    }

    function loadFromFilter() {
      ctrl.state.loadState = API_STATE.WAITING;
      initTable();

      _.forEach(ctrl.data.selectedFilters, (filter) => {
        filter.description = nvBackendFilterUtils.getDescription(filter);
      });

      getSellerIds(ctrl.data.selectedFilters)
        .then(load)
        .then(queryMoreInfo)
        .then(success)
        .finally(() => (ctrl.state.loadState = API_STATE.IDLE));

      function getSellerIds(filters) {
        const masterShippers = ShipperPickupsFilter.getSelectedOptions(filters, 'masterShippers');
        const masterShipperIds = _(masterShippers).map('global_id').compact().value();
        return masterShipperIds.length > 0
          ? Shippers.readSellersByIds(masterShipperIds)
          : $q.resolve([]);
      }

      async function load(subShippers) {
        const params = _.omit(nvBackendFilterUtils.constructParams(ctrl.data.selectedFilters), 'undefined');

        params.shipper_ids = params.shipper_ids && _(subShippers)
            .map('legacy_id')
            .concat(params.shipper_ids)
            .compact()
            .uniq()
            .value();

        _.assign(params, {
          last_id: 0,
          max_result: MAX_DATA,
        });

        let endState = false;
        let accumulator = [];
        while (!endState) {
          try {
            const result = await ShipperPickups.search(params);
            endState = _.size(result) === 0;
            _.assign(params, {
              last_id: _.get(_.last(result), 'id'),
            });
            accumulator = _.concat(accumulator, result);
          } catch (ex) {
            nvToast.error(nvTranslate.instant('container.shipper-pickups.some-request-failed'));
          }
        }
        return $q.when(accumulator);
      }

      function success(datas) {
        const data = datas[0];
        SHIPPERS_MAP = _.keyBy(datas[1], 'id');
        ADDRESSES_MAP = _.keyBy(datas[2], 'id');
        DRIVERS_MAP = _.keyBy(datas[3], 'id');
        FAILURE_REASONS_MAP = _.keyBy(datas[4], 'id');

        ctrl.data.tableParam.setData(extendDatas(data));
        ctrl.state.page = PAGE.DATA;
      }
    }

    function queryMoreInfo(datas) {
      const initialDataMap = {
        shippers: [],
        addresses: [],
        drivers: [],
        failureReasonIds: [],
      };
      const dirtyDataMap = _.reduce(datas, (result, data) => {
        result.shippers.push(data.shipper_id);
        result.addresses.push(data.address_id);
        result.drivers.push(data.driver_id);
        result.failureReasonIds.push(data.failure_reason_id);
        return result;
      }, initialDataMap);

      const dataMap = {};
      _.forEach(dirtyDataMap, (value, key) => {
        dataMap[key] = _(value).uniq().compact().value();
      });
      return $q.all([
        $q.when(datas),
        Shippers.searchByIds(dataMap.shippers, Shippers.SEARCH_BY.id),
        Shippers.searchAddressByIds(dataMap.addresses),
        Driver.searchByIds(dataMap.drivers),
        Driver.searchFailureReasonsByIds(dataMap.failureReasonIds),
      ]);
    }

    function showEditFilter() {
      resetFilterPage();
      ctrl.state.page = PAGE.FILTER;
    }

    function downloadCsvDisabled() {
      if (getSelectedSize() < 1) {
        return true;
      }
      return false;
    }

    function bulkActionDisabled() {
      if (getSelectedSize() < 1) {
        return true;
      }
      return false;
    }

    function getSelectedSize() {
      if (ctrl.data.tableParam) {
        return ctrl.data.tableParam.getSelection().length;
      }
      return 0;
    }

    function getSelected() {
      if (ctrl.data.tableParam) {
        return ctrl.data.tableParam.getSelection();
      }
      return EMPTY_DUMMY;
    }

    function refreshRoute() {
      ctrl.state.refreshRoute = API_STATE.WAITING;
      Route.fetchRoute(getRouteDateParam()).then(onSuccess, () => {
        ctrl.state.refreshRoute = API_STATE.IDLE;
      });

      function onSuccess(data) {
        ctrl.data.routes = extendRoutes(data);
        ctrl.state.refreshRoute = API_STATE.IDLE;
      }
    }

    function showReservationDetails($event, reservation) {
      const parameter = {
        reservation: reservation,
        routes: ctrl.data.routes,
      };

      nvDialog.showSingle($event, {
        templateUrl: 'views/container/shipper-pickups/dialogs/reservation-details/reservation-details.dialog.html',
        theme: 'nvBlue',
        cssClass: 'shipper-pickups-reservation-details-dialog',
        controller: 'ShipperPickupsReservationDetailsController',
        controllerAs: 'ctrl',
        escapeToClose: false,
        skipHide: true,
        locals: {
          parameter: parameter,
        },
      });
    }

    function editReservation($event, reservation) {
      const parameter = {
        reservation: reservation,
        routes: ctrl.data.routes,
      };

      nvDialog.showSingle($event, {
        templateUrl: 'views/container/shipper-pickups/dialogs/route-edit/route-edit.dialog.html',
        theme: 'nvBlue',
        cssClass: 'shipper-pickups-route-edit-dialog',
        controller: 'ShipperPickupsRouteEditController',
        controllerAs: 'ctrl',
        locals: {
          parameter: parameter,
        },
      }).then(({ reservationId, addressId }) => {
        // get updated address from shipper client
        Shippers.searchAddressByIds(addressId).then((addresses) => {
          ADDRESSES_MAP[addressId] = _.head(addresses);
          ShipperPickups.getPickupReservations(reservationId).then((data) => {
            ctrl.data.tableParam.mergeIn(extendDatas([data]), 'id');
          }, (err) => {
            nvToast.warning(['Error to update updated data, please refresh', err]);
          });
        });
      });
    }

    function openForceFinishReservationDialog($event, rsvn) {
      const parameter = {
        reservation: rsvn,
        failureReasons: ctrl.data.failureReasons,
      };

      nvDialog.showSingle($event, {
        templateUrl: 'views/container/shipper-pickups/dialogs/finish-reservation/finish-reservation.dialog.html',
        theme: 'nvBlue',
        cssClass: 'shipper-pickups-finish-reservation-dialog',
        controller: 'FinishReservationDialogController',
        controllerAs: 'ctrl',
        locals: {
          parameter: parameter,
        },
      }).then((id) => {
        ShipperPickups.getPickupReservations(id).then((data) => {
          ctrl.data.tableParam.mergeIn(extendDatas([data]), 'id');
        }, (err) => {
          nvToast.warning(['Error to update updated data, please refresh', err]);
        });
      });
    }

    function initTable() {
      ctrl.data.tableParam = nvTable.createTable({
        name: { displayName: 'container.shipper-pickups.table.col-1' },
        pickupAddress: { displayName: 'container.shipper-pickups.table.col-2' },
        routeId: { displayName: 'container.shipper-pickups.table.col-3' },
        driverName: { displayName: 'container.shipper-pickups.table.col-4' },
        readyBy: { displayName: 'container.shipper-pickups.table.col-5' },
        lastBy: { displayName: 'container.shipper-pickups.table.col-6' },
        type: { displayName: 'container.shipper-pickups.table.col-7' },
        status: { displayName: 'container.shipper-pickups.table.col-8' },
        createdDate: { displayName: 'container.shipper-pickups.table.col-9' },
        serviceTime: { displayName: 'container.shipper-pickups.table.col-10' },
        approxVolume: { displayName: 'container.shipper-pickups.table.col-11' },
        failureReason: { displayName: 'container.shipper-pickups.table.col-12' },
        comments: { displayName: 'container.shipper-pickups.table.col-14' },
        priorityLevel: { displayName: 'container.shipper-pickups.table.priority-level' },
      }, { isStripped: false });

      ctrl.data.tableParam.setHighlightFn(getReservationClass);
    }

    function extendDatas(datas) {
      const NOT_FOUND = 'not found';
      return _.map(datas, (data) => {
        let shipper = null;
        let address = ADDRESSES_MAP[data.address_id];
        let addresses = [NOT_FOUND];
        let driver = null;
        let failureReason = NOT_FOUND;

        if (SHIPPERS_MAP[data.shipper_id]) {
          shipper = SHIPPERS_MAP[data.shipper_id];
        }

        if (isHyperlocalOrOnDemand(data.type)) {
          address = {
            address1: data.address1,
            address2: data.address2,
            city: data.city,
            state: data.state,
            country: data.country,
            postcode: data.postcode,
            latitude: data.latitude,
            longitude: data.longitude,
          };
          addresses = [
            data.address1, data.address2, data.city,
            data.state, data.country, data.postcode,
          ];

          _.assign(shipper, {
            name: data.name,
            contact: data.contact,
          });
        } else {
          address = ADDRESSES_MAP[data.address_id];
          if (address) {
            addresses = [
              address.address1, address.address2, address.neighbourhood, address.locality,
              address.region, address.country, address.postcode];
          }
        }

        if (DRIVERS_MAP[data.driver_id]) {
          driver = DRIVERS_MAP[data.driver_id];
        }

        if (FAILURE_REASONS_MAP[data.failure_reason_id]) {
          failureReason = _.get(FAILURE_REASONS_MAP[data.failure_reason_id], 'description');
        }
        return {
          id: data.id,
          name: shipper ? `${_.get(shipper, 'name', NOT_FOUND)} - ${_.get(shipper, 'contact', 'N/A')}` : '-',
          pickupAddress: _(addresses).compact().join(', '),
          routeId: data.route_id ? data.route_id : '-',
          driverName: driver ? _.compact([_.trim(driver.firstName), _.trim(driver.lastName)]).join(' ') : '-',
          readyBy: data.ready_datetime ? getDateString(data.ready_datetime) : '-',
          lastBy: data.latest_datetime ? getDateString(data.latest_datetime) : '-',
          type: data.type,
          status: data.status,
          createdDate: data.created_at ? getDateString(data.created_at) : '-',
          serviceTime: data.service_time ? getDateString(data.service_time) : '-',
          approxVolume: data.approx_vol,
          failureReason: data.failure_reason_id ? failureReason : '-',
          comments: data.comments || '-',
          shipperName: _.get(shipper, 'name', NOT_FOUND),
          shipperContact: _.get(shipper, 'contact', NOT_FOUND),
          shipperId: data.shipper_id,
          waypointId: data.waypoint_id,
          addressId: data.address_id,
          timewindowId: data.timewindow_id,
          dpId: data.distribution_point_id,
          priorityLevel: data.priority_level,
          priorityLevelAsString: data.priority_level.toString(),
          _name: _.get(shipper, 'name', NOT_FOUND),
          _address: {
            address1: _.get(address, 'address1'),
            address2: _.get(address, 'address2'),
            neighbourhood: _.get(address, 'neighbourhood'),
            locality: _.get(address, 'locality'),
            region: _.get(address, 'region'),
            state: _.get(address, 'state'),
            postcode: _.get(address, 'postcode'),
            country: _.get(address, 'country'),
            latitude: _.get(address, 'latitude'),
            longitude: _.get(address, 'longitude'),
          },
          _readyDatetime: nvDateTimeUtils
            .toMoment(data.ready_datetime)
            .tz(nvTimezone.getOperatorTimezone()),
          _lastDatetime: nvDateTimeUtils
            .toMoment(data.latest_datetime)
            .tz(nvTimezone.getOperatorTimezone()),
        };
      });
      function getDateString(date) {
        const moment = nvDateTimeUtils.toMoment(date);
        return nvDateTimeUtils.displayDateTime(moment);
      }

      /**
       * @param {string} reservationTypeEnum reservation enum in upper case
       * @return {boolean}
       */
      function isHyperlocalOrOnDemand(reservationTypeEnum) {
        return Reservation.TYPE[reservationTypeEnum] === Reservation.TYPE.ON_DEMAND ||
          Reservation.TYPE[reservationTypeEnum] === Reservation.TYPE.HYPERLOCAL;
      }
    }

    function resetFilterPage() {
      ctrl.data.tableParam = null;
    }

    function getReservationClass(reservation) {
      const style = { class: 'row-default', highlighted: false };
      if (reservation.status === 'FAIL') {
        style.class = 'row-red';
      } else if (reservation.status === 'SUCCESS') {
        style.class = 'row-green';
      }
      return style;
    }

    function editReservationDisabled(status) {
      if (status === 'PENDING') {
        return false;
      }
      return true;
    }

    function finishReservationDisabled(rsvn) {
      const status = rsvn.status;
      const routeId = rsvn.routeId;

      if (routeId && routeId !== '-') {
        return editReservationDisabled(status);
      }
      return true;
    }

    function getRouteDateParam() {
      const currentTime = moment().toDate();
      return {
        from: nvDateTimeUtils.displayDateTime(
          nvDateTimeUtils.toSOD(currentTime, nvTimezone.getOperatorTimezone()), 'utc'),
        to: nvDateTimeUtils.displayDateTime(
          nvDateTimeUtils.toEOD(currentTime, nvTimezone.getOperatorTimezone()), 'utc'),
        include_waypoints: false,
        unarchived: true,
      };
    }

    function downloadCsv() {
      const selected = getSelected();

      nvCsvParser.unparse({
        fields: CSV_HEADER,
        data: _.map(selected, el => _.toArray(_.pick(el, field))),
      }).then((csv) => {
        nvFileUtils.downloadCsv(csv, 'shipper-pickups.csv');
      });
    }

    // START >>> reschedule reservations/create bulk reservations <<< START
    function createReservation($event) {
      const fields = {
        date: new Date(),
      };
      setBulkCreationUpdating(false);

      nvDialog.showSingle($event, {
        type: 'nv-dialog',
        templateUrl: 'views/container/shipper-pickups/dialogs/bulk-reservation-create/bulk-reservation-create.dialog.html',
        cssClass: 'reservation-reschedule-selected',
        scope: _.assign(nvEditModelDialog.scopeTemplate(), {
          onSubmit: onSubmit,
          fields: fields,
          selectedReservationsCount: getSelectedSize(),
          state: ctrl.state,
        }),
      });

      function onSubmit() {
        const date = nvDateTimeUtils.displayDate(fields.date);
        const reservations = getSelected();
        ctrl.data.rescheduledSuccessfully = [];

        if (reservations.length > 1) {
          multiSelectRescheduling(reservations, date);
        } else {
          singleRescheduling(reservations, date);
        }
      }

      function singleRescheduling(reservations, date) {
        const requests = _.map(reservations, rsv => generateRescheduleRequest(rsv, date));
        const request = _.head(requests);

        if (request.isValid) {
          setBulkCreationUpdating(true);
          Reservation.create(request.data)
            .then((data) => {
              setBulkCreationUpdating(false);
              deselectReservation(request.id);

              let hasFailedResponse = false;
              _.forEach(data, (response) => {
                if (response.state === nvRequestUtils.PROMISE_STATE.REJECTED) {
                  hasFailedResponse = true;
                }
              });

              if (!hasFailedResponse) {
                // show toast
                const createdReservation = _.get(data, '[0].value');
                nvToast.success(
                  nvTranslate.instant('container.shipper-pickups.n-reservation-created', {
                    count: 1,
                  }),
                  nvTranslate.instant('container.shipper-pickups.created-reservation-str', {
                    str: createdReservation
                      ? `${createdReservation.id} on ${readDate(createdReservation.ready_datetime)}`
                      : '-',
                  })
                );
              } else {
                nvToast.error(
                  nvTranslate.instant('container.shipper-pickups.n-reservation-created', {
                    count: 0,
                  }),
                  nvTranslate.instant('container.shipper-pickups.reservation-is-created-for-requested-date')
                );
              }
              $mdDialog.hide();
            }, () => {
              setBulkCreationUpdating(false);
              $mdDialog.hide();
            }
          );
        } else {
          nvToast.error(
            nvTranslate.instant('container.shipper-pickups.error-create-single-reservation'),
            `Shipper ${request.shipperName}: request.message`);
        }
      }

      function multiSelectRescheduling(reservations, date) {
        const requests = _.map(angular.copy(reservations), reservation =>
          generateRescheduleRequest(reservation, date));

        ctrl.bulkActionProgressPayload = {
          totalCount: requests.length,
          currentIndex: 0,
          errors: [],
        };
        $mdDialog.hide();

        nvDialog.showSingle($event, {
          templateUrl: 'lib/ninja-commons/services/dialogs/bulk-action-progress/bulk-action-progress.dialog.html',
          cssClass: 'bulk-action-progress',
          controller: 'BulkActionProgressDialogController',
          controllerAs: 'ctrl',
          skipHide: true,
          clickOutsideToClose: false,
          locals: {
            payload: ctrl.bulkActionProgressPayload,
          },
        }).then(progressDialogClosed);

        startMultiSelectRescheduling(requests);
      }

      function startMultiSelectRescheduling(requests) {
        let request;
        if (requests.length > 0) {
          setBulkCreationUpdating(true);
          ctrl.bulkActionProgressPayload.currentIndex += 1;
          request = _.head(requests.splice(0, 1));
          if (request.isValid) {
            Reservation.create(request.data)
              .then(multiSelectSuccess, multiSelectError);
          } else {
            $timeout(() => {
              multiSelectError({ message: request.message });
            }, 100);
          }
        } else {
          setBulkCreationUpdating(false);
        }

        function multiSelectSuccess(data) {
          let hasFailedResponse = false;
          _.forEach(data, (response) => {
            if (response.state === nvRequestUtils.PROMISE_STATE.REJECTED) {
              hasFailedResponse = true;
            }
          });

          if (!hasFailedResponse) {
            ctrl.bulkActionProgressPayload.successCount += 1;
            ctrl.data.rescheduledSuccessfully =
              _.concat(ctrl.data.rescheduledSuccessfully, _.get(data, '[0].value'));
            deselectReservation(request.id);
          } else {
            // mark as error
            multiSelectError({ message: nvTranslate.instant('container.shipper-pickups.reservation-is-created-for-requested-date') });
          }
          if (requests.length > 0) {
            startMultiSelectRescheduling(requests);
          } else {
            setBulkCreationUpdating(false);
          }
        }

        function multiSelectError(error) {
          ctrl.bulkActionProgressPayload.errors.push(`Shipper ${request.shipperName}: ${error.message}`);
          if (requests.length > 0) {
            startMultiSelectRescheduling(requests);
          } else {
            setBulkCreationUpdating(false);
          }
        }
      }

      function progressDialogClosed() {
        if (getSuccessCount() > 0) {
          nvToast.success('Reservation(s) created successfully', `Success to create ${getSuccessCount()} reservations`);
        }
        showSummary();
      }

      function getSuccessCount() {
        return ctrl.bulkActionProgressPayload.totalCount -
          ctrl.bulkActionProgressPayload.errors.length;
      }

      function setBulkCreationUpdating(b) {
        ctrl.state.isRescheduleUpdating = b;
      }

      function showSummary() {
        const datas = _.map(ctrl.data.rescheduledSuccessfully, d => ({
          id: d.id,
          name: d.name,
          rsvnDate: readDate(d.ready_datetime),
        }));

        nvDialog.showSingle($event, {
          templateUrl: 'views/container/shipper-pickups/dialogs/reservation-summary/reservation-summary.dialog.html',
          cssClass: 'shipper-pickups-reservation-summary',
          controller: 'ShiperPickupsReservationSummaryDialogController',
          controllerAs: 'ctrl',
          skipHide: true,
          clickOutsideToClose: false,
          locals: {
            payload: datas,
          },
        });
      }
    }
    // END >>> reschedule reservations/create bulk reservations <<< END

    function generateRescheduleRequest(reservation, date) {
      // get delta from readyDateTime
      const delta = getDeltaDay(reservation.readyBy, date);
      const ready = nvDateTimeUtils.toMoment(reservation.readyBy, nvTimezone.getOperatorTimezone());
      const last = nvDateTimeUtils.toMoment(reservation.lastBy, nvTimezone.getOperatorTimezone());

      ready.add(delta, 'd');
      last.add(delta, 'd');

      const validity = validateCreateReservation(reservation.type, delta);

      return {
        id: reservation.id,
        isValid: validity.value,
        message: validity.message,
        shipperName: reservation.shipperName,
        data: [{
          legacy_shipper_id: reservation.shipperId,
          pickup_start_time: Reservation
            .combineDateTimeToString(ready, ready),
          pickup_end_time: Reservation
            .combineDateTimeToString(last, last),
          pickup_approx_volume: reservation.approxVolume,
          pickup_instruction: reservation.comments,
          pickup_address_id: reservation.addressId,
          pickup_service_type: Reservation.PICKUP_SERVICE_TYPE.SCHEDULED,
          pickup_service_level: Reservation.PICKUP_SERVICE_LEVEL.STANDARD,
          is_on_demand: false,
          priority_level: reservation.priorityLevel,
          timewindow_id: reservation.timewindowId,
          disable_cutoff_validation: true,
          distribution_point_id: reservation.dpId,
        }],
      };
    }

    function getDeltaDay(before, after) {
      const momentBefore = nvDateTimeUtils.toMoment(before, nvTimezone.getOperatorTimezone()).startOf('day');
      const momentAfter = nvDateTimeUtils.toMoment(after, nvTimezone.getOperatorTimezone()).startOf('day');
      return momentAfter.diff(momentBefore, 'day');
    }

    function validateCreateReservation(type, delta) {
      const validity = {
        value: true,
        message: '',
      };
      if (type.toLowerCase() !== 'regular') {
        validity.value = false;
        validity.message = `Invalid reservation type: ${type}`;
        return validity;
      }
      // prevent recreation for date earlier than current
      if (delta < 0) {
        validity.value = false;
        validity.message = 'Please choose a date later than current reservation date';
        return validity;
      }
      return validity;
    }

    function deselectReservation(id) {
      if (getSelectedSize() > 0) {
        const toDeselect = _.find(getSelected(), item => item.id === id);
        ctrl.data.tableParam.deselect(toDeselect);
      }
    }

    function openBulkRouteAssignmentDialog($event) {
      const selections = _(getSelected())
        .map(rsvn => _.pick(rsvn, ['id', 'routeId', 'driverName', '_name', 'pickupAddress']))
        .filter(rsvn => rsvn.routeId === '-')
        .value();

      const toBeDeselectList = _.filter(getSelected(), rsvn => rsvn.routeId !== '-');
      _.forEach(toBeDeselectList, rsvn => deselectReservation(rsvn.id));

      if (!selections || selections.length === 0) {
        nvToast.error('No Valid Reservation Selected');
        return;
      }

      const parameter = {
        reservations: selections,
        routes: ctrl.data.routes,
      };

      nvDialog.showSingle($event, {
        templateUrl: 'views/container/shipper-pickups/dialogs/bulk-route-edit/bulk-route-edit.dialog.html',
        theme: 'nvBlue',
        cssClass: 'shipper-pickups-bulk-route-edit-dialog',
        controller: 'BulkRouteEditDialogController',
        controllerAs: 'ctrl',
        locals: {
          localParameter: parameter,
        },
      }).then(onSet);

      function onSet(response) {
        // array means the process is success
        if (_.isArray(response)) {
          _.forEach(response, (id) => {
            ShipperPickups.getPickupReservations(id).then((data) => {
              ctrl.data.tableParam.mergeIn(extendDatas([data]), 'id');
              deselectReservation(id);
            }, (err) => {
              nvToast.warning(['Error to update updated data, please refresh', err]);
            });
          });
        }
      }
    }

    function openBulkPriorityUpdateDialog($event) {
      const selections = _(getSelected())
        .map(rsvn => _.pick(rsvn, ['id', 'routeId', 'priorityLevel']))
        .value();

      const localParameter = {
        reservations: selections,
      };

      nvDialog.showSingle($event, {
        templateUrl: 'views/container/shipper-pickups/dialogs/bulk-priority-edit/bulk-priority-edit.dialog.html',
        theme: 'nvBlue',
        cssClass: 'shipper-pickups-bulk-priority-edit-dialog',
        controller: 'BulkPriorityEditDialogController',
        controllerAs: 'ctrl',
        locals: {
          localParameter: localParameter,
        },
      }).then(onSet);

      function onSet(response) {
        if (_.isArray(response)) {
          _.forEach(response, (id) => {
            ShipperPickups.getPickupReservations(id).then((data) => {
              ctrl.data.tableParam.mergeIn(extendDatas([data]), 'id');
              deselectReservation(id);
            }, (err) => {
              nvToast.warning(['Error to update updated data, please refresh', err]);
            });
          });
        }
      }
    }

    function isRoutesAvailable() {
      if (ctrl.data.routes) {
        return ctrl.data.routes.length > 0 && getSelected().length <= maxSelectable;
      }
      return false;
    }

    // START >>> route/driver removal from reservation <<< START
    function removeRouteFromReservations($event) {
      setBulkRouteRemovalUpdating(false);

      nvDialog.showSingle($event, {
        type: 'nv-dialog',
        templateUrl: 'views/container/shipper-pickups/dialogs/bulk-route-removal/bulk-route-removal.dialog.html',
        cssClass: 'bulk-route-removal-dialog',
        scope: _.assign(nvEditModelDialog.scopeTemplate(), {
          onSubmit: onSubmit,
          selectedReservationsCount: getSelectedSize(),
          state: ctrl.state,
        }),
      });

      function onSubmit() {
        const reservations = getSelected();
        ctrl.data.routeRemovedSuccessfully = [];

        if (reservations.length > 1) {
          multiSelectUnroute(reservations);
        } else {
          singleUnroute(reservations);
        }
      }

      function singleUnroute(reservations) {
        // ids should be int array of single element
        const currentId = _.head(_.map(reservations, rsv => rsv.id));

        setBulkRouteRemovalUpdating(true);
        Reservation
          .unrouteReservation(currentId)
          .then(onSuccess, onError)
          .finally(reload);

        function onSuccess(response) {
          setBulkRouteRemovalUpdating(false);

          const isUnrouted = isUnrouteSuccess(response);
          if (isUnrouted) {
            // show toast
            nvToast.success(
              nvTranslate.instant('container.shipper-pickups.n-route-removed', {
                count: 1,
              }),
              nvTranslate.instant('container.shipper-pickups.route-removal-str', {
                str: currentId,
              })
            );
          } else {
            nvToast.warning(
              nvTranslate.instant('container.shipper-pickups.n-route-removed', {
                count: 0,
              }),
              nvTranslate.instant('container.shipper-pickups.route-removal-failed', {
                str: _.get(response, 'message', '-'),
              })
            );
          }
          $mdDialog.hide();
        }

        function onError() {
          setBulkRouteRemovalUpdating(false);
          $mdDialog.hide();
        }

        function reload() {
          ShipperPickups.getPickupReservations(currentId).then((data) => {
            ctrl.data.tableParam.mergeIn(extendDatas([data]), 'id');
            deselectReservation(currentId);
          }, (err) => {
            nvToast.warning(['Error to update updated data, please refresh', err]);
          });
        }
      }

      function multiSelectUnroute(reservations) {
        const ids = _.map(reservations, rsv => rsv.id);

        ctrl.bulkActionProgressPayload = {
          totalCount: ids.length,
          currentIndex: 0,
          errors: [],
        };
        $mdDialog.hide();

        nvDialog.showSingle($event, {
          templateUrl: 'lib/ninja-commons/services/dialogs/bulk-action-progress/bulk-action-progress.dialog.html',
          cssClass: 'bulk-action-progress',
          controller: 'BulkActionProgressDialogController',
          controllerAs: 'ctrl',
          skipHide: true,
          clickOutsideToClose: false,
          locals: {
            payload: ctrl.bulkActionProgressPayload,
          },
        }).then(onRouteRemovalProgressDialogClosed);

        const clonedIds = _.cloneDeep(ids);
        startMultiSelectRouteRemoval(clonedIds);

        function onRouteRemovalProgressDialogClosed() {
          if (getSuccessCountRouteRemoval() > 0) {
            nvToast.success(nvTranslate.instant(
              'container.shipper-pickups.n-route-removed',
              { count: getSuccessCountRouteRemoval() }
            ));

            _.forEach(ctrl.data.routeRemovedSuccessfully, (id) => {
              ShipperPickups.getPickupReservations(id).then((data) => {
                ctrl.data.tableParam.mergeIn(extendDatas([data]), 'id');
                deselectReservation(id);
              }, (err) => {
                nvToast.warning(['Error to update updated data, please refresh', err]);
              });
            });
          }
        }
      }

      function startMultiSelectRouteRemoval(requests) {
        let currentId;
        if (requests.length > 0) {
          setBulkRouteRemovalUpdating(true);
          ctrl.bulkActionProgressPayload.currentIndex += 1;
          currentId = _.head(requests.splice(0, 1));
          Reservation
            .unrouteReservation(currentId)
            .then(multiSelectSuccess, multiSelectError);
        } else {
          setBulkRouteRemovalUpdating(false);
        }

        function multiSelectSuccess(response) {
          const isUnrouted = isUnrouteSuccess(response);
          if (isUnrouted) {
            ctrl.bulkActionProgressPayload.successCount += 1;
            ctrl.data.routeRemovedSuccessfully.push(currentId);
            deselectReservation(currentId);
          } else {
            // mark as error
            multiSelectError({ message: nvTranslate.instant(
              'container.shipper-pickups.route-removal-failed',
               { str: response ? response.message : 'unknown error' }) });
          }
          if (requests.length > 0) {
            startMultiSelectRouteRemoval(requests);
          } else {
            setBulkRouteRemovalUpdating(false);
          }
        }

        function multiSelectError(error) {
          const errMessage = _.get(error, 'message');
          ctrl.bulkActionProgressPayload.errors.push(`Rsvn ID: ${currentId} - ${errMessage}`);
          if (requests.length > 0) {
            startMultiSelectRouteRemoval(requests);
          } else {
            setBulkRouteRemovalUpdating(false);
          }
        }
      }

      function getSuccessCountRouteRemoval() {
        return ctrl.bulkActionProgressPayload.totalCount -
          ctrl.bulkActionProgressPayload.errors.length;
      }

      function setBulkRouteRemovalUpdating(b) {
        ctrl.state.isRescheduleUpdating = b;
      }

      function isUnrouteSuccess(response) {
        const status = _.get(response, 'status');
        return _.toLower(status) === 'pending';
      }
    }
    // END >>> route/driver removal from reservation <<< END

    function extendRoutes(routes) {
      return _(routes).filter(route => !!route.driver_id)
        .map(route => ({ value: route, displayName: `${route.route_id} - ${route.driver_name}` }))
        .value();
    }

    /**
     * @param  {string} str date and time in UTC, ex: 2018-09-14 10:00:00
     * @return {string} date in local timezone, ex: 2018-09-14
     */
    function readDate(str) {
      const time = moment.tz(str, 'UTC');
      return time.tz(nvTimezone.getOperatorTimezone()).format(nvDateTimeUtils.FORMAT_DATE);
    }
  }
}());
