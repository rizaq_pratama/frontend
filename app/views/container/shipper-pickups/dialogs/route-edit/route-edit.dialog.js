(function controller() {
  angular
    .module('nvOperatorApp.controllers')
    .controller('ShipperPickupsRouteEditController', ShipperPickupsRouteEditController);

  ShipperPickupsRouteEditController.$inject = ['$mdDialog', 'nvAutocomplete.Data', 'ShipperPickups',
    'parameter', 'nvDialog', '$translate', 'Reservation', '$q', 'nvToast', 'nvTranslate',
    'Timewindow', 'nvDateTimeUtils', 'nvTimezone', '$scope', 'Address', 'Shippers'];

  const API_STATE = {
    IDLE: 'idle',
    WAITING: 'waiting',
  };

  const STATES = {
    LOADING: 0,
    CONTENT: 1,
    ERROR: 2,
  };

  function ShipperPickupsRouteEditController($mdDialog, nvAutocompleteData, ShipperPickups,
    parameter, nvDialog, $translate, Reservation, $q, nvToast, nvTranslate,
    Timewindow, nvDateTimeUtils, nvTimezone, $scope, Address, Shippers) {
    const ctrl = this;
    const currentRsvn = _.cloneDeep(parameter.reservation);
    const NON_TIMESLOT_TIMING = ['ON_DEMAND', 'PREMIUM_SCHEDULED'];

    ctrl.state = {};
    ctrl.data = {};
    ctrl.service = {};
    ctrl.function = {};

    _.assign(ctrl.state, {
      masterView: STATES.LOADING,
      saveButton: API_STATE.IDLE,
      initiallyRouted: false,
      isNonTimeslotTiming: false,
      isPickupAddressChanged: false,
      endTimeMin: null,
      endTimeMax: moment().hour(22).minute(0).second(0),
      startTimeMin: moment().hour(9).minute(0).second(0),
      startTimeMax: null,
      pickupCountry: Shippers.SUPPORTED_PICKUP_COUNTRY,
      pickupCountryOptions: Shippers.pickupCountryToOptions(),
    });

    _.assign(ctrl.data, {
      // reservation come from table data
      reservation: _.assign(currentRsvn, { minPriorityLevel: currentRsvn.priorityLevel }),
      routes: _.cloneDeep(parameter.routes),
      reservationId: currentRsvn.id,
      textRoute: '',
      selectedRoute: null,
      selectedTimeslot: null,
    });

    ctrl.function.cancel = cancel;
    ctrl.function.save = save;
    ctrl.function.isNoRouteAvailable = isNoRouteAvailable;
    ctrl.function.changePickupAddress = changePickupAddress;

    init();

    function init() {
      const reservation = ctrl.data.reservation;

      reservation.fullAddress = reservation.pickupAddress;
      reservation.currentRoute = isInitiallyRouted()
        ? `${reservation.routeId} - ${reservation.driverName}`
        : 'Not Available';

      if (ctrl.data.routes && ctrl.data.routes.length > 0) {
        ctrl.service.selectDriver = nvAutocompleteData.getByHandle('shipper-pickups-select-route');
        ctrl.service.selectDriver.setPossibleOptions(ctrl.data.routes);
        ctrl.service.selectDriver.setMatchFn((item, fromItem) =>
          item.value.id === fromItem.value.id);

        if (isInitiallyRouted()) {
          ctrl.state.initiallyRouted = true;
          ctrl.data.selectedRoute = _.find(ctrl.data.routes, opt =>
            opt.value.route_id === reservation.routeId);
        }
      }
      initTimeslotOptions();
      initAddress();
      initHint();
      initListener();
      ctrl.state.masterView = STATES.CONTENT;

      function isInitiallyRouted() {
        return reservation.routeId && reservation.routeId !== '-';
      }

      function initTimeslotOptions() {
        ctrl.data.startTime = _.cloneDeep(reservation._readyDatetime);
        ctrl.data.endTime = _.cloneDeep(reservation._lastDatetime);
        // clone to initial state
        ctrl.state.initialStartTime = _.cloneDeep(reservation._readyDatetime);
        ctrl.state.initialEndTime = _.cloneDeep(reservation._lastDatetime);

        ctrl.state.endTimeMin = ctrl.data.startTime.clone();
        ctrl.state.startTimeMax = ctrl.data.endTime.clone();
        if (reservation.type === 'PREMIUM_SCHEDULED') {
          ctrl.state.endTimeMin = ctrl.data.startTime.clone().add(90, 'm');
          ctrl.state.startTimeMax = ctrl.data.endTime.clone().subtract(90, 'm');
        }
        
        ctrl.state.isNonTimeslotTiming = _.indexOf(NON_TIMESLOT_TIMING, reservation.type) > -1;
        
        if (!ctrl.state.isNonTimeslotTiming) {
          const timewindow = _.find(Timewindow.TIMEWINDOWS, (tw) => {
            const isFromTimeMatch = reservation._readyDatetime.hour() === tw.fromTime.hour();
            const isEndTimeMatch = reservation._lastDatetime.hour() === tw.toTime.hour();
            return isFromTimeMatch && isEndTimeMatch;
          });
          const timewindowId = _.get(timewindow, 'id');

          const options = Timewindow.toOptions();
          const current = _.find(options, opt => opt.value === timewindowId);
          ctrl.data.selectedTimeslot = current ? current.value : null;
          ctrl.timeslotOptions = options;
        }
      }

      function initAddress() {
        const internalAddress = {
          address1: _.get(ctrl.data.reservation, '_address.address1'),
          address2: _.get(ctrl.data.reservation, '_address.address2'),
          neighbourhood: _.get(ctrl.data.reservation, '_address.neighbourhood'),
          locality: _.get(ctrl.data.reservation, '_address.locality'),
          region: _.get(ctrl.data.reservation, '_address.region'),
          state: _.get(ctrl.data.reservation, '_address.state'),
          postcode: _.get(ctrl.data.reservation, '_address.postcode'),
          country: _.get(ctrl.data.reservation, '_address.country'),
          latitude: null,
          longitude: null,
        };
        ctrl.data.address = Shippers.mapToLocalizedAddress(internalAddress);
      }

      function initHint() {
        switch (reservation.type) {
          case 'PREMIUM_SCHEDULED':
            ctrl.state.hint = nvTranslate.instant('container.shipper-pickups.dialog.hint-premium-scheduled');
            break;
          case 'ON_DEMAND':
            ctrl.state.hint = nvTranslate.instant('container.shipper-pickups.dialog.hint-on-demand');
            break;
          default:
            ctrl.state.hint = null;
        }
      }

      function initListener() {
        if (reservation.type === 'PREMIUM_SCHEDULED') {
          // only add if it was premium scheduled
          $scope.$watch(() => (ctrl.data.startTime ? ctrl.data.startTime.toString() : ''), () => {
            const m = nvDateTimeUtils.toMoment(ctrl.data.startTime);
            if (moment.isMoment(m)) {
              ctrl.state.endTimeMin = m.clone().add(90, 'm');
            }
          });
          $scope.$watch(() => (ctrl.data.endTime ? ctrl.data.endTime.toString() : ''), () => {
            const m = nvDateTimeUtils.toMoment(ctrl.data.endTime);
            if (moment.isMoment(m)) {
              ctrl.state.startTimeMax = m.clone().subtract(90, 'm');
            }
          });
        }
      }
    }

    function cancel() {
      $mdDialog.cancel();
    }

    function save($event) {
      const reservation = ctrl.data.reservation;

      if (reservation.type === 'REGULAR' || reservation.type === 'PREMIUM_SCHEDULED') {
        doSave();
      } else {
        const options = {
          title: $translate.instant('dialog.confirm-save.title'),
          content: $translate.instant('container.shipper-pickups.dialog.reservation-confirmation', { reservation_type: reservation.type }),
          ok: $translate.instant('commons.ok'),
          cancel: $translate.instant('commons.cancel'),
          skipHide: true,
        };

        nvDialog.confirmSave($event, options).then(doSave);
      }
    }

    function doSave() {
      ctrl.state.saveButton = API_STATE.WAITING;
      // if (!ctrl.state.isNonTimeslotTiming) {
      //   // using timewindows selectors -- disabled for now
      //   const selectedTimeslot = _.find(Timewindow.TIMEWINDOWS, tw => tw.id === ctrl.data.selectedTimeslot);
      //   const from = _.get(selectedTimeslot, 'fromTime');
      //   const to = _.get(selectedTimeslot, 'toTime');
      //   ctrl.data.startTime = ctrl.data.startTime.hour(from.hour()).minute(from.minute());
      //   ctrl.data.endTime = ctrl.data.endTime.hour(to.hour()).minute(to.minute());
      // }

      const updateRsvnPayload = {
        pickup_address_id: ctrl.data.reservation.addressId,
        legacy_shipper_id: ctrl.data.reservation.shipperId,
        reservation_type_value: Reservation.getTypeId(ctrl.data.reservation.type),
        priority_level: ctrl.data.reservation.priorityLevel,
      };

      if (isTimewindowsChanged() && ctrl.data.reservation !== 'REGULAR') {
        _.assign(updateRsvnPayload, {
          pickup_start_time: nvDateTimeUtils.displayISO(ctrl.data.startTime, nvTimezone.getOperatorTimezone()),
          pickup_end_time: nvDateTimeUtils.displayISO(ctrl.data.endTime, nvTimezone.getOperatorTimezone()),
        });
      }

      if (ctrl.state.isPickupAddressChanged) {
        // update address data to shipper service->address first, then update reservation
        const address = Shippers.mapToInternalAddress(ctrl.data.address);
        if (address.latitude === 0) {
          address.latitude = null;
        }
        if (address.longitude === 0) {
          address.longitude = null;
        }

        const legacyShipperId = ctrl.data.reservation.shipperId;
        const addressId = ctrl.data.reservation.addressId;

        if (ctrl.data.reservation.type === 'ON_DEMAND') {
          // directly update waypoint
          _.omit(updateRsvnPayload, 'pickup_address_id');
          _.assign(updateRsvnPayload, {
            is_on_demand: true,
            address1: address.address1,
            address2: address.address2,
            city: address.city,
            state: address.state,
            country: address.country,
            postcode: address.postcode,
            latitude: address.latitude === 0 ? null : address.latitude,
            longitude: address.longitude === 0 ? null : address.longitude,
          });
          updateReservationData();
        } else {
          // update address id first
          Shippers
          .readShipperLegacy(legacyShipperId)
          .then(result => Shippers
            .updateAddress(result.id, addressId, address)
            .then(updateReservationData));
        }
      } else {
        // directly update reservation data
        updateReservationData();
      }

      function updateReservationData() {
        if (ctrl.data.selectedRoute != null) {
          // it has route and priority level filled
          const rerouteRsvnPayload = {
            new_route_id: ctrl.data.selectedRoute.value.route_id,
            route_index: -1,
            overwrite: true,
          };

          $q.all([
            Reservation.updateV2(ctrl.data.reservation.id, updateRsvnPayload),
            ShipperPickups.rerouteReservation(ctrl.data.reservationId, rerouteRsvnPayload),
          ]).then(hideDialog).finally(reinitButtonState);
        } else if (ctrl.state.initiallyRouted) {
          // it has a route before, but now route not selected
          $q.all([
            Reservation.updateV2(ctrl.data.reservation.id, updateRsvnPayload),
            Reservation.unrouteReservation(ctrl.data.reservationId).then(checkResult),
          ]).then(hideDialog).finally(reinitButtonState);
        } else {
          // rsvn never assigned to any route, so only priority level updated
          Reservation.updateV2(ctrl.data.reservation.id, updateRsvnPayload)
            .then(hideDialog).finally(reinitButtonState);
        }
      }

      function hideDialog() {
        $mdDialog.hide(
          {
            reservationId: ctrl.data.reservationId,
            addressId: ctrl.data.reservation.addressId,
          });
      }

      function reinitButtonState() {
        ctrl.state.saveButton = API_STATE.IDLE;
      }

      function checkResult(response) {
        const status = _.get(response, 'status');
        if (status && status.toLowerCase() === 'pending') {
          return $q.resolve();
        }
        nvToast.warning(nvTranslate.instant(
          'container.shipper-pickups.route-removal-failed',
           { str: response ? response.message : 'unknown error' }));
        return $q.reject();
      }

      function isTimewindowsChanged() {
        const isStartTimeEqual = ctrl.state.initialStartTime.isSame(ctrl.data.startTime);
        const isEndTimeEqual = ctrl.state.initialEndTime.isSame(ctrl.data.endTime);
        return !(isStartTimeEqual && isEndTimeEqual);
      }
    }

    function isNoRouteAvailable() {
      return (ctrl.data.routes.length === 0) && (ctrl.data.selectedRoute === null);
    }

    function changePickupAddress() {
      ctrl.state.isPickupAddressChanged = true;
    }
  }
}());
