(function controller() {
  angular
    .module('nvOperatorApp.controllers')
    .controller('BulkRouteEditDialogController', BulkRouteEditDialogController);

  BulkRouteEditDialogController.$inject = ['$mdDialog', 'localParameter', 'nvAutocomplete.Data',
    'ShipperPickups', 'nvDialog', 'Tag', 'nvDateTimeUtils', 'nvTimezone'];

  const VIEW = {
    LOADING: 0,
    CONTENT: 1,
    ERROR: 2,
  };

  const DEFAULT_EMPTY = [];

  function BulkRouteEditDialogController($mdDialog, localParameter, nvAutocompleteData,
    ShipperPickups, nvDialog, Tag, nvDateTimeUtils, nvTimezone) {
    const ctrl = this;
    const reservations = angular.copy(localParameter.reservations);
    const routes = angular.copy(localParameter.routes);

    ctrl.data = {};
    ctrl.state = {};
    ctrl.function = {};
    ctrl.view = {};

    ctrl.state.view = VIEW.LOADING;
    ctrl.state.saveButtonState = 'idle';
    ctrl.state.suggestButtonState = 'idle';

    ctrl.function.onCancel = onCancel;
    ctrl.function.onSave = onSave;
    ctrl.function.isManuallyAssigned = isManuallyAssigned;
    ctrl.function.isReservationEmpty = isReservationEmpty;
    ctrl.function.isAutomaticallyAssigned = isAutomaticallyAssigned;
    ctrl.function.isAnyFilled = isAnyFilled;
    ctrl.function.onSuggestRoute = onSuggestRoute;
    ctrl.function.isSuggestRouteDisabled = isSuggestRouteDisabled;
    ctrl.function.getReservationsLength = getReservationsLength;
    ctrl.function.getSelectedRouteTags = getSelectedRouteTags;
    ctrl.function.removeTag = removeTag;

    init();

    function init() {
      ctrl.data.routes = routes;
      ctrl.data.routeTags = [];

      let idx = 0;
      ctrl.data.reservations = _.map(reservations, (rsvn) => {
        const currIdx = idx;
        const autoCompleteService = nvAutocompleteData.getByHandle(`shipper-pickups-select-route-${currIdx}`);
        autoCompleteService.setPossibleOptions(_.cloneDeep(ctrl.data.routes));

        // manipulate idx
        idx += 1;
        return _.assign(rsvn, {
          idx: currIdx,
          routeIndex: -1,
          serviceSelectedItem: null,
          serviceSearchText: '',
          service: autoCompleteService,
        });
      });

      Tag.all().then((response) => {
        ctrl.view.tagSearchText = '';
        ctrl.view.tagSearchService = nvAutocompleteData.getByHandle('shipper-edit-route-tags');

        const options = _(response.tags)
          .map(tag => ({ displayName: tag.name, value: tag.name }))
          .sortBy('displayName')
          .value();
        ctrl.view.tagSearchService.setPossibleOptions(options);
        ctrl.state.view = VIEW.CONTENT;
      }, () => (ctrl.state.view = VIEW.ERROR));
    }

    function onCancel() {
      $mdDialog.hide(-1);
    }

    function onSave($event) {
      const filledReservations = _(ctrl.data.reservations)
        .filter(isFilledReservation)
        .map(rsvn => _.assign({}, {
          reservationId: rsvn.id,
          payload: {
            new_route_id: rsvn.serviceSelectedItem.value.route_id,
            route_index: rsvn.routeIndex,
            overwrite: true,
          },
        }))
        .value();

      if (filledReservations.length > 1) {
        multiSelectRouteAssignment($event, filledReservations);
      } else {
        singleSelectRouteAssignment($event, filledReservations);
      }

      function isFilledReservation(rsvn) {
        return rsvn.serviceSelectedItem != null && rsvn.routeIndex != null && rsvn.routeIndex !== '';
      }
    }

    function isManuallyAssigned(rsvn) {
      if (rsvn.serviceSelectedItem) {
        return !((rsvn.serviceSelectedItem.value.route_id === rsvn.suggestedRouteId) &&
          (rsvn.routeIndex === rsvn.suggestedIndex));
      }
      return false;
    }

    function isReservationEmpty(rsvn) {
      return rsvn.serviceSelectedItem == null;
    }

    function isAutomaticallyAssigned(rsvn) {
      if (rsvn.serviceSelectedItem) {
        return ((rsvn.serviceSelectedItem.value.route_id === rsvn.suggestedRouteId) &&
          (rsvn.routeIndex === rsvn.suggestedIndex));
      }
      return false;
    }

    function isAnyFilled() {
      return _.find(ctrl.data.reservations, rsvn => rsvn.serviceSelectedItem != null);
    }

    function singleSelectRouteAssignment($event, requests) {
      const request = _.head(requests);
      ctrl.state.saveButtonState = 'loading';

      ShipperPickups.rerouteReservation(request.reservationId, request.payload).then(() => {
        $mdDialog.hide(_.castArray(request.reservationId));
        ctrl.state.saveButtonState = 'idle';
      }, () => {
        ctrl.state.saveButtonState = 'idle';
      });
    }

    function multiSelectRouteAssignment($event, requests) {
      const requestsCopy = _.cloneDeep(requests);

      const successIds = [];

      ctrl.bulkActionProgressPayload = {
        totalCount: requests.length,
        currentIndex: 0,
        errors: [],
      };

      nvDialog.showSingle($event, {
        templateUrl: 'lib/ninja-commons/services/dialogs/bulk-action-progress/bulk-action-progress.dialog.html',
        cssClass: 'bulk-action-progress',
        controller: 'BulkActionProgressDialogController',
        controllerAs: 'ctrl',
        skipHide: true,
        clickOutsideToClose: false,
        locals: {
          payload: ctrl.bulkActionProgressPayload,
        },
      }).then(progressDialogClosed);

      ctrl.state.saveButtonState = 'waiting';
      startMultiSelectReroute(requestsCopy);

      function progressDialogClosed() {
        $mdDialog.hide(successIds);
      }

      function startMultiSelectReroute() {
        let request;

        if (requestsCopy.length > 0) {
          request = _.head(requestsCopy.splice(0, 1));
          ShipperPickups.rerouteReservation(request.reservationId, request.payload)
            .then(multiSelectSuccess, multiSelectError);
        } else {
          ctrl.state.saveButtonState = 'idle';
        }

        function multiSelectSuccess() {
          ctrl.bulkActionProgressPayload.successCount += 1;
          successIds.push(request.reservationId);

          if (requestsCopy.length > 0) {
            startMultiSelectReroute();
          } else {
            ctrl.state.saveButtonState = 'idle';
          }
        }

        function multiSelectError(error) {
          ctrl.bulkActionProgressPayload.errors.push(`Rsvn ${request.reservationId}: ${error.message}`);
          if (requestsCopy.length > 0) {
            startMultiSelectReroute();
          } else {
            ctrl.state.saveButtonState = 'idle';
          }
        }
      }
    }

    function onSuggestRoute() {
      ctrl.state.suggestButtonState = 'waiting';
      const date = nvDateTimeUtils.toMoment(new Date(), nvTimezone.getOperatorTimezone());
      const payload = _.map(ctrl.data.reservations, rsvn => _.assign({}, {
        reservation_id: rsvn.id,
        route_tags: _.map(getSelectedRouteTags(), tag => tag.value),
        route_date_filter: nvDateTimeUtils.displayDateTime(
          nvDateTimeUtils.toSOD(date, nvTimezone.getOperatorTimezone()), 'utc'),
      }));

      ShipperPickups.suggestRoute(payload)
        .then(
          onSuccess,
          () => (ctrl.state.suggestButtonState = 'idle')
      );

      function onSuccess(response) {
        _.forEach(response, assignOptimalRoute);
        ctrl.state.suggestButtonState = 'idle';

        function assignOptimalRoute(sgst) {
          if (sgst) {
            const reservation = _.find(ctrl.data.reservations, rsvn =>
              rsvn.id === sgst.reservation_id);
            const optimalIndex = sgst.suggested_route_seq_no;
            const optimalRouteId = sgst.suggested_route_id;

            _.assign(reservation, {
              suggestedIndex: optimalIndex,
              suggestedRouteId: optimalRouteId,
            });

            const options = reservation.service.getPossibleOptions();
            const toBeSelected = _.find(options, opt => opt.value.route_id === optimalRouteId);
            if (toBeSelected) {
              _.assign(reservation, {
                serviceSelectedItem: toBeSelected,
                routeIndex: optimalIndex,
              });
            }
          }
        }
      }
    }

    function getReservationsLength() {
      if (ctrl.data.reservations) {
        return ctrl.data.reservations.length;
      }
      return 0;
    }

    function isSuggestRouteDisabled() {
      return getSelectedRouteTags().length === 0;
    }

    function getSelectedRouteTags() {
      if (ctrl.view.tagSearchService) {
        return ctrl.view.tagSearchService.getSelectedOptions();
      }
      return DEFAULT_EMPTY;
    }

    function removeTag(item) {
      ctrl.view.tagSearchService.remove(item);
    }
  }
}());
