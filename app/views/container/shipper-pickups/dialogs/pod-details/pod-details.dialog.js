(function controller() {
  angular
    .module('nvOperatorApp.controllers')
    .controller('ShipperPickupsPodDetailsController', ShipperPickupsPodDetailsController);

  ShipperPickupsPodDetailsController.$inject = ['$mdDialog', 'parameter', 'nvFileUtils', 'nvDateTimeUtils',
    'nvTimezone'];

  function ShipperPickupsPodDetailsController($mdDialog, parameter, nvFileUtils, nvDateTimeUtils,
    nvTimezone) {
    const ctrl = this;

    ctrl.data = {};
    ctrl.function = {};

    ctrl.data.reservation = angular.copy(parameter.reservation);
    ctrl.data.pod = angular.copy(parameter.pod);
    ctrl.data.photos = constructPhotos(parameter.photos);

    ctrl.function.cancel = cancel;
    ctrl.function.isSuccess = isSuccess;
    ctrl.function.isFail = isFail;
    ctrl.function.isDownloadAvailable = isDownloadAvailable;
    ctrl.function.downloadSelected = downloadSelected;
    ctrl.function.downloadSignature = downloadSignature;

    function cancel() {
      $mdDialog.hide();
    }

    function isSuccess() {
      return ctrl.data.reservation.status === 'SUCCESS';
    }

    function isFail() {
      return ctrl.data.reservation.status === 'FAIL';
    }

    function constructPhotos(photos) {
      let idx = 0;
      return _.map(photos, (p) => {
        const id = idx += 1;
        const takenAt = nvDateTimeUtils.displayDateTime(
          nvDateTimeUtils.toMoment(p.taken_at, nvTimezone.getOperatorTimezone()),
          nvTimezone.getOperatorTimezone()
        );
        const filename = p.image_url.split('/').pop();
        return _.assign({}, {
          id: id - 1,
          isActive: false,
          url: p.image_url,
          title: `${filename} - ${takenAt}` });
      });
    }

    function downloadSelected() {
      _(ctrl.data.photos)
        .filter(p => p.isActive)
        .forEach((p) => {
          const filename = p.url.split('/').pop();
          nvFileUtils.download(p.url, filename);
        });
    }

    function downloadSignature() {
      const url = ctrl.data.pod.signature_url;
      if (ctrl.data.pod.signature_url) {
        const filename = url.split('/').pop();
        nvFileUtils.download(url, filename);
      }
    }

    function isDownloadAvailable() {
      const selected = _.filter(ctrl.data.photos, p => p.isActive);
      if (selected) {
        return selected.length > 0;
      }
      return false;
    }
  }
}());
