(function controller() {
  angular
    .module('nvOperatorApp.controllers')
    .controller('FinishReservationDialogController', FinishReservationDialogController);

  FinishReservationDialogController.$inject = ['nvDialog', '$mdDialog', 'parameter', 'Driver', '$scope',
    'nvTranslate', 'ShipperPickups', 'Waypoint', '$timeout'];

  function FinishReservationDialogController(nvDialog, $mdDialog, parameter, Driver, $scope,
      nvTranslate, ShipperPickups, Waypoint, $timeout) {
    const ctrl = this;
    const reservation = _.cloneDeep(parameter.reservation);
    const failureReasons = _.cloneDeep(parameter.failureReasons);

    ctrl.Waypoint = Waypoint;
    ctrl.formSubmitState = 'idle';
    ctrl.action = null;
    ctrl.chosenFailureReasons = [];

    ctrl.chooseAction = chooseAction;
    ctrl.failureReasonOnChange = failureReasonOnChange;
    ctrl.isFormSubmitDisabled = isFormSubmitDisabled;
    ctrl.showConfirmationDialog = showConfirmationDialog;
    ctrl.onCancel = onCancel;

    init();

    function init() {
      ctrl.failureReasons = _(failureReasons)
        .filter(fr => fr.active === 1)
        .filter(fr => _.toLower(fr.mode) === 'pickup')
        .filter(fr => _.toLower(fr.type) !== 'return')
        .value();

      addFailureReasonSelectionRowIfApplicable(null);
    }

    function chooseAction($event, statusAction) {
      ctrl.action = statusAction;

      if (ctrl.action === Waypoint.UPDATE_STATUS_ACTION.SUCCESS) {
        showConfirmationDialog($event);
      }
    }

    function failureReasonOnChange(failureReason) {
      $timeout(() => {
        const failureReasonIndex = _.findIndex(ctrl.chosenFailureReasons, [
          'parentId', failureReason.parentId,
        ]);

        if (failureReasonIndex >= 0) {
          ctrl.chosenFailureReasons.splice(
            failureReasonIndex + 1,
            (_.size(ctrl.chosenFailureReasons) - (failureReasonIndex + 1))
          );
        }

        addFailureReasonSelectionRowIfApplicable(failureReason.selected.id);
      });
    }

    function showConfirmationDialog($event) {
      const opt = { skipHide: true, ok: nvTranslate.instant('commons.proceed') };

      if (ctrl.action === Waypoint.UPDATE_STATUS_ACTION.SUCCESS) {
        _.defaults(opt, getSuccessOptions());
      } else {
        _.defaults(opt, getFailOptions());
      }

      nvDialog.confirmDelete($event, opt).then(onConfirm);

      function getSuccessOptions() {
        return {
          title: nvTranslate.instant('container.shipper-pickups.dialog.confirm-success-header'),
          content: `${nvTranslate.instant('container.shipper-pickups.dialog.confirm-success-body', {
            rsvnId: reservation.id,
          })}
            <br /><br />
            <h5>${nvTranslate.instant('container.shipper-pickups.dialog.confirm-can-not-be-undone')}</h5>
            <br />
            ${nvTranslate.instant('container.shipper-pickups.dialog.confirm-continue')}`,
        };
      }

      function getFailOptions() {
        const lastChosenFailureReason = _.last(ctrl.chosenFailureReasons);
        return {
          title: nvTranslate.instant('container.shipper-pickups.dialog.confirm-fail-header'),
          content: `${nvTranslate.instant('container.shipper-pickups.dialog.confirm-fail-body', {
            rsvnId: reservation.id,
            failReason: lastChosenFailureReason.selected.description,
          })}
            <br /><br />
            <h5>${nvTranslate.instant('container.shipper-pickups.dialog.confirm-can-not-be-undone')}</h5>
            <br />
            ${nvTranslate.instant('container.shipper-pickups.dialog.confirm-continue')}`,
        };
      }

      function onConfirm() {
        const payload = {};
        if (ctrl.action === Waypoint.UPDATE_STATUS_ACTION.SUCCESS) {
          _.assign(payload, { action: 'success' });
        } else {
          const lastChosenFailureReason = _.last(ctrl.chosenFailureReasons);
          _.assign(payload, { action: 'fail', failure_reason_id: lastChosenFailureReason.selected.id });
        }
        ctrl.formSubmitState = 'waiting';
        ShipperPickups.removeRoute(reservation.routeId, reservation.waypointId, payload)
          .then(onSuccess)
          .finally(() => {
            ctrl.formSubmitState = 'idle';
          });

        function onSuccess() {
          $mdDialog.hide(reservation.id);
        }
      }
    }

    function isFormSubmitDisabled() {
      const lastChosenFailureReason = _.last(ctrl.chosenFailureReasons);
      return !lastChosenFailureReason.selected;
    }

    function onCancel() {
      $mdDialog.cancel();
    }

    function addFailureReasonSelectionRowIfApplicable(parentId) {
      const filteredFailureReasons = _.filter(failureReasons, ['parent', parentId]);
      const failureReasonsOptions = _.map(filteredFailureReasons, failureReason => ({
        displayName: `${failureReason.description} - ${failureReason.type}`,
        value: { id: failureReason.id, description: failureReason.description },
      }));

      if (_.size(failureReasonsOptions) > 0) {
        ctrl.chosenFailureReasons.push({
          parentId: parentId,
          options: failureReasonsOptions,
          selected: null,
        });
      }
    }
  }
}());
