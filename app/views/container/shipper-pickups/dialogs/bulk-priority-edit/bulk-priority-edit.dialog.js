(function controller() {
  angular
    .module('nvOperatorApp.controllers')
    .controller('BulkPriorityEditDialogController', BulkPriorityEditDialogController);

  BulkPriorityEditDialogController.$inject = ['$mdDialog', 'localParameter',
    'Reservation', '$timeout', 'nvDialog'];

  const VIEW = {
    LOADING: 0,
    CONTENT: 1,
    ERROR: 2,
  };

  function BulkPriorityEditDialogController($mdDialog, localParameter,
    Reservation, $timeout, nvDialog) {
    const ctrl = this;
    const reservations = _.cloneDeep(localParameter.reservations);

    ctrl.data = {};
    ctrl.state = {};
    ctrl.function = {};

    ctrl.function.onCancel = onCancel;
    ctrl.function.onSave = onSave;
    ctrl.function.isAnyFieldsEmpty = isAnyFieldsEmpty;
    ctrl.function.priorityUpdateChanged = priorityUpdateChanged;

    init();

    function init() {
      ctrl.state.isSetToAll = false;
      ctrl.state.saveButtonState = 'idle';
      ctrl.state.view = VIEW.CONTENT;

      ctrl.data.reservations = _(reservations)
        .map(rsvn => _.assign(rsvn, { minPriorityLevel: rsvn.priorityLevel }))
        .orderBy('priorityLevel', 'desc')
        .value();
    }

    function onCancel() {
      $mdDialog.hide(-1);
    }

    function onSave() {
      const payloads = _.map(ctrl.data.reservations, rsvn => _.pick(rsvn, ['id', 'priorityLevel']));
      const RESULTS = [];
      ctrl.bulkActionProgressPayload = {
        totalCount: _.size(reservations),
        currentIndex: 0,
        errors: [],
      };

      nvDialog.showSingle(null, {
        templateUrl: 'lib/ninja-commons/services/dialogs/bulk-action-progress/bulk-action-progress.dialog.html',
        cssClass: 'bulk-action-progress',
        controller: 'BulkActionProgressDialogController',
        controllerAs: 'ctrl',
        skipHide: true,
        clickOutsideToClose: false,
        locals: {
          payload: ctrl.bulkActionProgressPayload,
        },
      }).then(onSet);

      startUpdatePriorityLevel(_.cloneDeep(payloads));

      function onSet() {
        $mdDialog.hide(RESULTS);
      }

      function startUpdatePriorityLevel(requests) {
        let request;
        if (requests.length > 0) {
          ctrl.bulkActionProgressPayload.currentIndex += 1;
          request = _.head(requests.splice(0, 1));
          Reservation
            .updatePartial(request)
            .then(multiSelectSuccess, multiSelectError);
        } else {
          // finish
        }

        function multiSelectSuccess() {
          ctrl.bulkActionProgressPayload.successCount += 1;
          RESULTS.push(request);
          if (requests.length > 0) {
            startUpdatePriorityLevel(requests);
          } else {
            // finish
          }
        }

        function multiSelectError(error) {
          const errMessage = _.get(error, 'message');
          ctrl.bulkActionProgressPayload.errors.push(`Rsvn ID: ${request.id} - ${errMessage}`);
          if (requests.length > 0) {
            startUpdatePriorityLevel(requests);
          } else {
            // finish
          }
        }
      }
    }

    function isAnyFieldsEmpty() {
      if (ctrl.data.reservations) {
        return !!_.find(ctrl.data.reservations, rsvn => _.isUndefined(rsvn.priorityLevel));
      }
      return true;
    }

    function priorityUpdateChanged(rsvn) {
      $timeout(() => {
        if (ctrl.state.isSetToAll && ctrl.data.reservations.length > 1) {
          _.forEach(ctrl.data.reservations, (theRsvn) => {
            if (theRsvn.minPriorityLevel <= rsvn.priorityLevel) {
              theRsvn.priorityLevel = rsvn.priorityLevel;
            }
          });
        }
      });
    }
  }
}());
