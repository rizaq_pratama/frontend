(function controller() {
  angular
    .module('nvOperatorApp.controllers')
    .controller('ShipperPickupsReservationDetailsController', ShipperPickupsReservationDetailsController);

  ShipperPickupsReservationDetailsController.$inject = ['$mdDialog', 'ShipperPickups', 'parameter',
    'nvCsvParser', 'nvToast', 'nvDialog', 'nvDateTimeUtils'];

  const STATES = {
    LOADING: 0,
    CONTENT: 1,
    ERROR: 2,
  };

  function ShipperPickupsReservationDetailsController($mdDialog, ShipperPickups, parameter,
    nvCsvParser, nvToast, nvDialog, nvDateTimeUtils) {
    const ctrl = this;

    ctrl.state = {};
    ctrl.data = {};
    ctrl.function = {};

    ctrl.state.masterView = STATES.LOADING;

    ctrl.data.reservation = angular.copy(parameter.reservation);
    ctrl.data.routes = angular.copy(parameter.routes);
    ctrl.data.reservationId = ctrl.data.reservation.id;

    ctrl.function.cancel = cancel;
    ctrl.function.selectPod = selectPod;
    ctrl.function.downloadPodCsvDisabled = downloadPodCsvDisabled;
    ctrl.function.downloadPodCsv = downloadPodCsv;
    ctrl.function.showPodDetails = showPodDetails;

    init();

    function init() {
      const reservation = ctrl.data.reservation;
      ctrl.data.reservation.full_address = `${reservation.address1} ${reservation.address2}, ${reservation.state} ${reservation.city} ${reservation.postcode}, ${reservation.country}`;

      ShipperPickups.getPod(ctrl.data.reservationId)
        .then(onSuccess, () => {
          ctrl.state.masterView = STATES.ERROR;
        });

      function onSuccess(data) {
        ctrl.data.shipperName = data.shipper_name;
        ctrl.data.shipperId = data.shipper_id;
        ctrl.data.pickupAddress = `${data.address1} ${data.address2}, ${data.state} ${data.city} ${data.postcode}, ${data.country}`;
        ctrl.data.waypointId = data.waypoint_id;
        ctrl.data.photos = data.photos;
        ctrl.data.pods = _.map(data.pods, pod =>
          _.defaults(pod, {
            class: '',
            submitted_scans_quantity: 0,
            actual_shipper_scans_quantity: pod.shipper_scans.length,
            actual_hub_scans_quantity: pod.hub_scans.length,
            timestamp_string: nvDateTimeUtils.displayDateTime(
              nvDateTimeUtils.toMoment(pod.timestamp)),
          }));

        if (ctrl.data.pods.length > 0) {
          selectPod(null, ctrl.data.pods[0].id);
        }
        ctrl.state.masterView = STATES.CONTENT;
      }
    }

    function cancel() {
      $mdDialog.cancel();
    }

    function showPodDetails($event, pod) {
      nvDialog.showSingle($event, {
        templateUrl: 'views/container/shipper-pickups/dialogs/pod-details/pod-details.dialog.html',
        theme: 'nvBlue',
        cssClass: 'shipper-pickups-pod-details-dialog',
        controller: 'ShipperPickupsPodDetailsController',
        controllerAs: 'ctrl',
        skipHide: true,
        escapeToClose: false,
        locals: {
          parameter: {
            reservation: ctrl.data.reservation,
            photos: ctrl.data.photos,
            pod: pod,
          },
        },
      });
    }

    function selectPod($event, id) {
      if (ctrl.data.selectedPod) {
        ctrl.data.selectedPod.class = '';
      }
      const selected = _.find(ctrl.data.pods, pod => pod.id === id);
      ctrl.data.selectedPod = selected;
      ctrl.data.selectedPod.class = 'selected';
    }

    function downloadPodCsv() {
      const scannedAtShippers = angular.copy(ctrl.data.selectedPod.shipper_scans);
      const inboundedAtHub = angular.copy(ctrl.data.selectedPod.hub_scans);
      // make both data has balanced length
      let delta = scannedAtShippers.length - inboundedAtHub.length;
      if (delta > 0) {
        fillArray(inboundedAtHub, delta);
      } else {
        delta = Math.abs(delta);
        fillArray(scannedAtShippers, delta);
      }
      
      const datas = [];
      for (let i = 0; i < scannedAtShippers.length; i++) {
        datas.push([scannedAtShippers[i], inboundedAtHub[i]]);
      }

      nvCsvParser.unparse({
        fields: ['Order Scanned at Shipper (POD)', 'Orders Inbounded at Warehouse (Inbounded)'],
        data: datas,
      }).then((result) => {
        const file = document.createElement('a');
        angular.element(file)
                .attr('href', `data:application/csv;charset=utf-8,${encodeURIComponent(result)}`)
                .attr('download', `pod-file-id-${ctrl.data.selectedPod.id}.csv`);
        document.body.appendChild(file);
        file.click();
      }, (err) => {
        nvToast.warning(['Error to create csv, please report the error', err]);
      });

      function fillArray(array, n, filler = '-') {
        for (let i = 0; i < n; i++) {
          array.push(filler);
        }
      }
    }

    function downloadPodCsvDisabled() {
      if (ctrl.data.selectedPod) {
        return false;
      }
      return true;
    }
  }
}());
