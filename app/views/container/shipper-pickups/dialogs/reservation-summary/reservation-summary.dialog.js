(function controller() {
  angular
    .module('nvOperatorApp.controllers')
    .controller('ShiperPickupsReservationSummaryDialogController', ShiperPickupsReservationSummaryDialogController);

  ShiperPickupsReservationSummaryDialogController.$inject = [
    '$mdDialog', 'nvTranslate', 'payload', 'nvTable', 'nvToast',
  ];

  function ShiperPickupsReservationSummaryDialogController($mdDialog, nvTranslate, payload, nvTable,
    nvToast) {
    const ctrl = this;
    ctrl.datas = _.cloneDeep(payload);
    ctrl.onCancel = onCancel;
    
    init();

    function init() {
      ctrl.tableParam = nvTable.createTable({
        id: { displayName: 'commons.id' },
        name: { displayName: 'commons.name' },
        rsvnDate: { displayName: 'commons.date' },
      });
      ctrl.tableParam.setData(ctrl.datas);
    }

    function onCancel() {
      $mdDialog.cancel();
    }
  }
}());
