(function controller() {
  angular
        .module('nvOperatorApp.controllers')
        .controller('CODReportController', CODReportController);

  const COD_HEADER = ['Tracking Id', 'Granular Status', 'Shipper Name', 'Cash On Delivery Amount', 'Collected Sum', 'Collected'];
  const DRIVER_COD_HEADER = ['Tracking Id', 'Granular Status', 'Shipper Name', 'Cash On Delivery Amount', 'Collected Sum', 'Collected', 'Driver Name', 'Route Id'];

  const COD_ORDER = ['tracking_id', 'granular_status', 'shipper_name', 'goods_amount', 'collected_sum', 'collected'];
  const DRIVER_COD_ORDER = ['tracking_id', 'granular_status', 'shipper_name', 'goods_amount', 'collected_sum', 'collected', 'driver_name', 'route_id'];

  CODReportController.$inject = ['$scope', 'CODReport', 'nvTableUtils'];
  function CODReportController($scope, CODReport, nvTableUtils) {
    const ctrl = this;

    ctrl.cod = [];
    ctrl.codTableParams = null;
    ctrl.codColumns = CODReport.COD_TABLE_COLUMNS;
    ctrl.driverCodColumns = CODReport.DRIVER_COD_TABLE_COLUMNS;
    ctrl.fields = CODReport.FIELDS;

    ctrl.reportMode = '10';
    ctrl.reportDriverMode = false;
    ctrl.date = moment().toDate();
    ctrl.fetchState = 'idle';
    ctrl.fetchCOD = fetchCOD;
    ctrl.onSearch = onSearch;

    // csv
    ctrl.COD_HEADER = JSON.stringify(COD_HEADER);
    ctrl.DRIVER_COD_HEADER = JSON.stringify(DRIVER_COD_HEADER);
    ctrl.COD_ORDER = JSON.stringify(COD_ORDER);
    ctrl.DRIVER_COD_ORDER = JSON.stringify(DRIVER_COD_ORDER);

    ctrl.fetchCOD();

    $scope.$watch('ctrl.reportMode', fetchCOD);

    // //////////////////

    function fetchCOD() {
      ctrl.fetchState = 'waiting';
      if (ctrl.reportMode === '10') {
        return CODReport.getCOD(ctrl.date)
                .then((data) => {
                  ctrl.cod = data;
                  ctrl.codTableParams = nvTableUtils.generate(ctrl.cod,
                    { columns: ctrl.codColumns });
                  ctrl.reportDriverMode = false;
                }).finally(() => {
                  ctrl.fetchState = 'idle';
                });
      } else if (ctrl.reportMode === '01') {
        return CODReport.getDriverCOD(ctrl.date)
                .then((data) => {
                  ctrl.cod = data;
                  ctrl.codTableParams = nvTableUtils.generate(ctrl.cod,
                    { columns: ctrl.driverCodColumns });
                  ctrl.reportDriverMode = true;
                }).finally(() => {
                  ctrl.fetchState = 'idle';
                });
      }
      return null;
    }

    function onSearch(searchText) {
      ctrl.codTableParams.filter({ $: searchText });
    }
  }
}());
