(function() {
    'use strict';

    angular
        .module('nvOperatorApp.controllers')
        .controller('RelabelStampController', RelabelStampController);

    RelabelStampController.$inject = [
        'Printer', 'nvToast', 'Order',
        'nvTranslate'
    ];
    function RelabelStampController(Printer, nvToast, Order, nvTranslate) {

        //////////////////////////////////////////////////
        // variables
        //////////////////////////////////////////////////
        var ctrl = this;
        ctrl.scan = "";


        //////////////////////////////////////////////////
        // functions
        //////////////////////////////////////////////////
        ctrl.relabelStamp = relabelStamp;


        //////////////////////////////////////////////////
        // start
        //////////////////////////////////////////////////



        //////////////////////////////////////////////////
        // functions details
        //////////////////////////////////////////////////
        function relabelStamp() {
            Order.getTrackingIdV2(ctrl.scan).then(success, failure);

            function success(order) {
                console.log(order);

                var payload = [{
                    recipient_name: order.toName,
                    address1: order.toAddress1,
                    address2: order.toAddress2,
                    contact: order.toContact,
                    country: order.toCountry,
                    postcode: order.toPostcode,
                    shipper_name: order.fromName,
                    tracking_id: order.trackingId,
                    barcode: order.trackingId,
                    route_id: "",
                    driver_name: "",
                    template: "hub_shipping_label_70X50"
                }];

                Printer.printByPost(payload)
                    .then(function(){
                        nvToast.success(nvTranslate.instant('container.relabel-stamp.stamp-printed'));
                    });
            }

            function failure() {
                nvToast.error(nvTranslate.instant('container.relabel-stamp.error-not-found'));
            }

        }


        //////////////////////////////////////////////////
        // Helper Functions
        //////////////////////////////////////////////////



    }
})();
