(function controller() {
  angular
    .module('nvOperatorApp.controllers')
    .controller('HubEditDialogController', HubEditDialogController);

  HubEditDialogController.$inject = ['hubData', '$mdDialog', '$timeout', 'Hub', 'nvExtendUtils'];

  function HubEditDialogController(hubData, $mdDialog, $timeout, Hub, nvExtendUtils) {
    const ctrl = this;
    ctrl.hub = _.cloneDeep(hubData);

    ctrl.hubFacilityOptions = _.cloneDeep(Hub.HUB_FACILITY_TYPES);
    ctrl.saveButtonState = 'clean';

    ctrl.onSave = onSave;
    ctrl.onCancel = onCancel;
    ctrl.onChangeCity = onChangeCity;
    ctrl.onChangeCountry = onChangeCountry;

    function onSave() {
      ctrl.saveButtonState = 'saving';
      // clean up some legacy fields
      const omittedFields = ['_shortName', 'short_name', '_latlng', '_facilityType', '_active'];
      const hubRequest = nvExtendUtils.convertKeysToSnakeCase(_.omit(ctrl.hub, omittedFields));
      Hub.update(hubRequest).then((result) => {
        $mdDialog.hide(result);
      }).finally(() => (ctrl.saveButtonState = 'clean'));
    }

    function onCancel() {
      $mdDialog.cancel();
    }

    function onChangeCity() {
      $timeout(() => {
        if (ctrl.hub.city) {
          ctrl.hub.city = ctrl.hub.city.toLowerCase()
            .replace(/\b\S/g, t => (t.toUpperCase()));
        }
      }, 0);
    }

    function onChangeCountry() {
      $timeout(() => {
        ctrl.hub.country = _.upperCase(ctrl.hub.country);
      });
    }
  }
}());
