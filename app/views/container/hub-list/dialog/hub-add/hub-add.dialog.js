(function controller() {
  angular
    .module('nvOperatorApp.controllers')
    .controller('HubAddDialogController', HubAddDialogController);

  HubAddDialogController.$inject = ['$mdDialog', '$timeout', 'Hub', 'nvExtendUtils'];

  function HubAddDialogController($mdDialog, $timeout, Hub, nvExtendUtils) {
    const ctrl = this;
    init();

    ctrl.hubFacilityOptions = _.cloneDeep(Hub.HUB_FACILITY_TYPES);
    ctrl.saveButtonState = 'clean';
    ctrl.onSave = onSave;
    ctrl.onCancel = onCancel;
    ctrl.onChangeCity = onChangeCity;
    ctrl.onChangeCountry = onChangeCountry;

    function init() {
      ctrl.hub = _.defaults({}, {
        name: '',
        city: '',
        country: '',
        longitude: null,
        latitude: null,
        shortName: null,
        facilityType: Hub.HUB_FACILITY_TYPES[4].value,
      });
    }
    function onSave() {
      ctrl.saveButtonState = 'saving';
      const hubRequest = nvExtendUtils.convertKeysToSnakeCase(ctrl.hub);
      Hub.create(hubRequest).then((result) => {
        $mdDialog.hide(result);
      }).finally(() => (ctrl.saveButtonState = 'clean'));
    }

    function onCancel() {
      $mdDialog.cancel();
    }

    function onChangeCity() {
      $timeout(() => {
        if (ctrl.hub.city) {
          ctrl.hub.city = ctrl.hub.city.toLowerCase()
            .replace(/\b\S/g, t => (t.toUpperCase()));
        }
      }, 0);
    }

    function onChangeCountry() {
      $timeout(() => {
        ctrl.hub.country = _.upperCase(ctrl.hub.country);
      });
    }
  }
}());
