(function controller() {
  angular
    .module('nvOperatorApp.controllers')
    .controller('HubListController', HubListController);

  const TABLE_FIELDS = {
    id: { displayName: 'commons.id' },
    name: { displayName: 'commons.name' },
    shortName: { displayName: 'container.hub-list.display-name' },
    city: { displayName: 'commons.city' },
    country: { displayName: 'commons.country' },
    _latlng: { displayName: 'container.hub-list.header-lat-long' },
    _active: { displayName: 'commons.status' },
    _facilityType: { displayName: 'container.hub-list.facility-type' },
  };

  HubListController.$inject = [
    '$q', 'Hub', 'nvDialog', 'nvEditModelDialog',
    'nvToast', 'nvTranslate', '$timeout', 'nvTable',
  ];

  function HubListController($q, Hub, nvDialog, nvEditModelDialog,
    nvToast, nvTranslate, $timeout, nvTable) {
    const ctrl = this;

    ctrl.getHubs = getHubs;
    ctrl.addHub = addHub;
    ctrl.editHub = editHub;
    ctrl.activateHub = activateHub;
    ctrl.deactivateHub = deactivateHub;
    ctrl.refreshHub = refreshHub;

    ctrl.getDatas = getDatas;
    ctrl.hubsTableParams = null;
    ctrl.tableParam = nvTable.createTable(TABLE_FIELDS);
    // //////////////////////////////////////////////////

    function getHubs() {
      return Hub.read({ active_only: false }).then(success, $q.reject);

      function success(hubs) {
        const sortedHubs = _.sortBy(hubs, 'id');
        ctrl.tableParam.setData(extendHubs(sortedHubs));
      }
    }

    function extendHubs(hubs) {
      const datas = _.castArray(hubs);
      return _.map(datas, (data) => {
        _.defaults(data, { active: data.deletedAt == null });
        data._active = data.active ? 'Active' : 'Disabled';
        data._facilityType = Hub.getFacilityTypeName(_.get(data, 'facilityType'));
        return data;
      });
    }

    function addHub($event) {
      nvDialog.showSingle($event, {
        templateUrl: 'views/container/hub-list/dialog/hub-add/hub-add.dialog.html',
        cssClass: 'hub-add',
        theme: 'nvGreen',
        controller: 'HubAddDialogController',
        controllerAs: 'ctrl',
      }).then(success);

      function success(hub) {
        ctrl.tableParam.mergeIn(extendHubs(hub), 'id');
      }
    }

    function editHub($event, hub) {
      // there's still disrepancy about short_name and shortName
      // pending backend changes
      hub.short_name = _.get(hub, 'shortName', hub.short_name);

      nvDialog.showSingle($event, {
        templateUrl: 'views/container/hub-list/dialog/hub-edit/hub-edit.dialog.html',
        cssClass: 'hub-edit',
        theme: 'nvBlue',
        controller: 'HubEditDialogController',
        controllerAs: 'ctrl',
        locals: {
          hubData: hub,
        },
      }).then(success);

      function success(response) {
        ctrl.tableParam.mergeIn(extendHubs(response), 'id');
      }
    }

    function refreshHub() {
      Hub.refresh()
      .then(getHubs)
      .then(() => {
        nvToast.success(nvTranslate.instant('container.hub-list.hub-cache-refreshed'));
      });
    }

    function activateHub($event, hub) {
      const options = {
        title: nvTranslate.instant('container.hub-list.confirm-activation'),
        content: nvTranslate.instant('container.hub-list.confirm-activation-content'),
        ok: nvTranslate.instant('commons.activate'),
      };
      nvDialog.confirmSave($event, options)
        .then(() => {
          const theHub = _.cloneDeep(hub);
          _.assign(theHub, { activate: true });
          Hub.update(theHub, theHub.id).then(response => ctrl.tableParam.mergeIn(extendHubs(response), 'id'));
        });
    }

    function deactivateHub($event, hub) {
      const options = {
        title: nvTranslate.instant('container.hub-list.confirm-deactivation'),
        content: nvTranslate.instant('container.hub-list.confirm-deactivation-content'),
        ok: nvTranslate.instant('commons.disable'),
      };
      nvDialog.confirmDelete($event, options)
        .then(() => {
          Hub.delete(hub).then((response) => {
            // need to handle in frontend since backend return incomplete detail whether hub is deleted or not
            _.assign(response, { deletedAt: response.createdAt });
            ctrl.tableParam.mergeIn(extendHubs(response), 'id');
          });
        });
    }

    function getDatas() {
      if (ctrl.tableParam) {
        return ctrl.tableParam.getTableData();
      }
      return 0;
    }
  }
}());
