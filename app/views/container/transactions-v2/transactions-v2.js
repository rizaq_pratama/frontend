(function controller() {
  angular
    .module('nvOperatorApp.controllers')
    .controller('RoutableTransactionsControllerV2', RoutableTransactionsControllerV2);

  RoutableTransactionsControllerV2.$inject = [
    '$q',
    '$mdDialog',
    'RouteGroup',
    'nvDialog',
    'nvEditModelDialog',
    'nvToast',
    'nvFileSelectDialog',
    'nvButtonFilePickerType',
    'nvCsvParser',
    'nvDateTimeUtils',
    'nvTimezone',
    'TransactionFilterTemplate',
    'nvTranslate',
    'nvTable',
    'nvBackendFilterUtils',
    'Transaction',
    'Reservation',
    'TransactionFilter',
    'Shippers',
    'DistributionPoint',
  ];

  function RoutableTransactionsControllerV2(
    $q,
    $mdDialog,
    RouteGroup,
    nvDialog,
    nvEditModelDialog,
    nvToast,
    nvFileSelectDialog,
    nvButtonFilePickerType,
    nvCsvParser,
    nvDateTimeUtils,
    nvTimezone,
    TransactionFilterTemplate,
    nvTranslate,
    nvTable,
    nvBackendFilterUtils,
    Transaction,
    Reservation,
    TransactionFilter,
    Shippers,
    DistributionPoint
  ) {
    // ////////////////////////////////////////////////
    // variables
    // ////////////////////////////////////////////////
    const ctrl = this;

    let routeGroups = [];
    let routeGroupsSelection = {};

    ctrl.nvDateTimeUtils = nvDateTimeUtils;
    ctrl.PAGES = { FILTER: 'filter', RESULT: 'result', LOADING: 'loading' };
    ctrl.currentPage = ctrl.PAGES.FILTER;
    ctrl.loadForever = $q.defer().promise;

    // Filter page
    ctrl.filterPresets = [];
    ctrl.loadSelectionState = 'idle';
    ctrl.selectedPreset = null;

    // General Filter
    ctrl.selectedGeneralFilters = [];
    ctrl.possibleGeneralFilters = [];

    // Txn Filter
    ctrl.includeTxnModel = '10';
    ctrl.selectedTxnFilters = [];
    ctrl.possibleTxnFilters = [];

    // Rxn Filter
    ctrl.includeRxn = '01';
    ctrl.selectedRxnFilters = [];
    ctrl.possibleRxnFilters = [];

    ctrl.showTxn = showTxn;
    ctrl.showRxn = showRxn;

    const distributionPointById = {};

    // Result page
    ctrl.tableParam = {};

    // ////////////////////////////////////////////////
    // functions
    // ////////////////////////////////////////////////
    ctrl.goToPage = goToPage;
    ctrl.isPage = isPage;

    // Filter page
    ctrl.getAll = getAll;
    ctrl.loadSelection = loadSelection;
    ctrl.savePreset = savePreset;
    ctrl.updatePreset = updatePreset;
    ctrl.loadPreset = loadPreset;
    ctrl.deletePreset = deletePreset;
    ctrl.checkPresetName = checkPresetName;
    ctrl.findTxnsByCSV = findTxnsByCSV;

    // Result page
    ctrl.addToRouteGroup = addToRouteGroup;

    // ////////////////////////////////////////////////
    // start
    // ////////////////////////////////////////////////


    // ////////////////////////////////////////////////
    // functions details
    // ////////////////////////////////////////////////
    function getAll() {
      return read().then(success, $q.reject);

      function read() {
        return $q.all([
          TransactionFilterTemplate.read(),
          RouteGroup.read(),
          DistributionPoint.listDPs({ is_active: true }),
        ]);
      }

      function success(results) {
        ctrl.filterPresets = results[0];
        routeGroups = results[1].routeGroups;

        _.forEach(results[2], (dp) => {
          distributionPointById[dp.id] = dp;
        });

        translateVars();

        routeGroupsSelection.options = RouteGroup.toOptions(routeGroups);

        // initialize filters
        const filters = TransactionFilter.init({
          routeGroups: routeGroups,
          distributionPoints: results[2],
        });
        ctrl.possibleGeneralFilters = filters.generalFilters;
        ctrl.selectedGeneralFilters = _.remove(ctrl.possibleGeneralFilters,
            filter => filter.showOnInit === true
        );
        ctrl.selectedTxnFilters = [];
        ctrl.possibleTxnFilters = filters.txnFilters;
        ctrl.selectedRxnFilters = [];
        ctrl.possibleRxnFilters = filters.rxnFilters;
        ctrl.selectedPreset = null;
      }
    }

    function loadSelection() {
      if (!showTxn() && !showRxn()) {
        nvToast.error(nvTranslate.instant('container.transactions-v2.select-txn-or-rxn'));
        return;
      }

      ctrl.loadSelectionState = 'waiting';

      refreshRouteGroups(ctrl.selectedGeneralFilters)
        .then(() => getSubShippers(ctrl.selectedGeneralFilters))
        .then(getTxnsAndRxns)
        .then(success, failure);

      function getTxnsAndRxns(subShippers) {
        const txnParams = constructTxnParams(subShippers);
        const rxnParams = constructRxnParams(subShippers);

        // don't fetch if ids = []
        const searchTxnPromise = showTxn() && (!txnParams.ids || !_.isEmpty(txnParams.ids))
          ? fetchTxn(txnParams)
          : $q.resolve([]);

        // don't fetch if reservationId = []
        const searchRxnPromise = showRxn() &&
        (!rxnParams.reservationId || !_.isEmpty(rxnParams.reservationId))
          ? fetchRxn(rxnParams)
          : $q.resolve([]);

        return $q.all([
          searchTxnPromise,
          searchRxnPromise,
        ]);
      }

      function success([
        transactions,
        reservations,
      ]) {
        ctrl.loadSelectionState = 'idle';

        const tableData = _([])
          .concat(getTxnData(transactions))
          .concat(getRxnData(reservations))
          .map((value, idx) => {
            value.sequence = idx + 1;
            return value;
          })
          .value();

        generateTable(tableData, getSelectedFilters());
      }

      function failure() {
        ctrl.loadSelectionState = 'idle';
      }
    }

    function refreshRouteGroups(filters) {
      const filter = _.find(filters, { presetKey: 'routeGroupIds' });

      if (!filter) {
        return $q.resolve();
      }

      const selectedRgIds = _.map(filter.selectedOptions, 'id');
      const selectedRgIdsSet = new Set(selectedRgIds);
      return TransactionFilter
        .getRouteGroups(routeGroups)
        .then((rgs) => {
          const rgMap = {};
          _.forEach(rgs, rg => (rgMap[rg.id] = rg));
          filter.selectedOptions = _.map(selectedRgIds, id => rgMap[id]);
          _.remove(rgs, rg => selectedRgIdsSet.has(rg.id));
          filter.possibleOptions = rgs;
          return rgMap;
        });
    }

    function getSubShippers(filters) {
      const masterShippers = TransactionFilter.getSelectedOptions(filters, 'masterShipperIds');
      const masterShipperIds = _(masterShippers).map('global_id').compact().value();
      return masterShipperIds.length > 0
        ? Shippers.readSellersByIds(masterShipperIds)
        : $q.resolve([]);
    }

    function constructTxnParams(subShippers) {
      const txnFilters = _.concat(ctrl.selectedGeneralFilters, ctrl.selectedTxnFilters);
      const txnParams = nvBackendFilterUtils.constructParams(txnFilters);

      txnParams.shipperIds = txnParams.shipperIds &&
        _(subShippers).map('legacy_id')
          .concat(txnParams.shipperIds)
          .compact()
          .uniq()
          .value();

      if (txnParams.ids) {
        txnParams.ids = _.flatMap(txnParams.ids, rg => rg.transactionIds);
      }

      return txnParams;
    }

    function constructRxnParams(subShippers) {
      const rxnFilters = _.concat(ctrl.selectedGeneralFilters, ctrl.selectedRxnFilters);
      const rxnParams = nvBackendFilterUtils.constructParams(rxnFilters);

      rxnParams.shipperIds = rxnParams.shipperIds &&
        _(subShippers)
          .map('legacy_id')
          .concat(rxnParams.shipperIds)
          .compact()
          .uniq()
          .value();

      if (rxnParams.ids) {
        rxnParams.ids = _.flatMap(rxnParams.ids, rg => rg.reservationIds);
      }

      // Transform general filters
      const txnToRxn = {
        ids: 'reservationId',
        startTimeFrom: 'startDate',
        startTimeTo: 'endDate',
        endTimeFrom: 'startDate',
        endTimeTo: 'endDate',
        shipperIds: 'shipperId',
        distributionPointIds: 'dpId',
        orderCreateTimeFrom: 'createdFrom',
        orderCreateTimeTo: 'createdTo',
        isRouted: 'routed',
      };

      return _.mapKeys(rxnParams, (value, txnKey) => {
        const rxnKey = txnToRxn[txnKey];
        return rxnKey || txnKey;
      });
    }

    function fetchTxn(txnParams) {
      // has not in RG
      let hasNotInRg = false;
      const selectedTxnIds = txnParams.ids;

      if (_.find(txnParams.ids, id => id === -1)) {
        txnParams = _.omit(txnParams, 'ids');
        hasNotInRg = true;
      }

      return Transaction
        .searchIds(txnParams)
        .then((res) => {
          let filteredTxnIds = res.transactionIds;

          if (hasNotInRg) {
            filteredTxnIds = filterTxnIdsByRg(filteredTxnIds, selectedTxnIds);
          }
          return _.isEmpty(filteredTxnIds) ?
            [] : Transaction.search({ ids: filteredTxnIds });
        });
    }

    function fetchRxn(rxnParams) {
      // has not in RG
      let dummy = null;
      let hasNotInRg = false;
      const selectedRxnIds = rxnParams.reservationId;

      if (_.find(rxnParams.reservationId, id => id === -1)) {
        rxnParams = _.omit(rxnParams, 'reservationId');
        hasNotInRg = true;
      }

      return Reservation
        .search(_.defaults(rxnParams, { includeRoute: true }))
        .then((filteredRxns) => {
          if (hasNotInRg) {
            filteredRxns = filterRxnsByRg(filteredRxns, selectedRxnIds);
          }
          return filteredRxns;
        });
    }

    function filterTxnIdsByRg(filteredTxnIds, selectedTxnIds) {
      return filterIdsByRg(filteredTxnIds, selectedTxnIds, 'transactionIds');
    }

    function filterRxnsByRg(filteredRxns, selectedRxnIds) {
      const filteredRxnById = {};
      _.forEach(filteredRxns, (rxn) => {
        filteredRxnById[rxn.id] = rxn;
      });

      const filteredRxnIds = filterIdsByRg(
        _.map(filteredRxns, 'id'),
        selectedRxnIds,
        'reservationIds'
      );

      return _.map(filteredRxnIds, id => filteredRxnById[id]);
    }

    /**
     * 1) Receive filteredIds from txn/rxn
     * 2) Filter everything by the selected RGs => ids1
     * 3) Remove everything from not selected RGs => ids2
     * 4) Merge ids1 and idds2
    */
    function filterIdsByRg(filteredIds, selectedIds, path) {
      const ids1 = _.intersection(filteredIds, selectedIds);

      const rgFilter = _.find(ctrl.selectedGeneralFilters, { key: 'ids' });
      const notselectedIds = _.flatMap(rgFilter.possibleOptions, path);

      const ids2 = _.without(filteredIds, ...notselectedIds);
      return _.concat(ids1, ids2);
    }

    function getSelectedFilters() {
      return _(ctrl.selectedGeneralFilters)
        .concat(showTxn() ? ctrl.selectedTxnFilters : [])
        .concat(showRxn() ? ctrl.selectedRxnFilters : [])
        .value();
    }

    function getTxnData(transactions) {
      return _.map(transactions, txn => ({
        id: txn.id,
        orderId: txn.orderId,
        trackingId: txn.trackingId,
        type: `${txn.type} Transaction`,
        shipper: txn.fromName,
        address: getTxnAddress(txn),
        routeId: txn.routeId,
        status: txn.granularStatus,
        startDateTime: getDateTime(txn.startTime),
        endDateTime: getDateTime(txn.endTime),
        dp: getDistributionPoint(txn.distributionPointId),
        comments: txn.comments,
        _type: 'txn',
      }));
    }

    function getRxnData(reservations) {
      return _.map(reservations, rxn => ({
        id: rxn.id,
        type: getRxnType(rxn),
        shipper: rxn.name || '',
        address: getRxnAddress(rxn),
        routeId: getRxnRouteId(rxn),
        status: rxn.status,
        startDateTime: getDateTime(rxn.readyDatetime),
        endDateTime: getDateTime(rxn.latestDatetime),
        dp: getDistributionPoint(rxn.distributionPointId),
        pickupSize: rxn.approxVolume,
        comments: rxn.comments,
        _type: 'rxn',
      }));
    }

    function isPage(page) {
      return ctrl.currentPage === page;
    }

    function goToPage(page) {
      ctrl.currentPage = page;
    }

    function loadPreset(preset) {
      nvBackendFilterUtils.convertPresetToFilter(
        ctrl.possibleGeneralFilters,
        ctrl.selectedGeneralFilters,
        preset);

      nvBackendFilterUtils.convertPresetToFilter(
        ctrl.possibleTxnFilters,
        ctrl.selectedTxnFilters,
        preset);

      nvBackendFilterUtils.convertPresetToFilter(
        ctrl.possibleRxnFilters,
        ctrl.selectedRxnFilters,
        preset);

      ctrl.includeTxnModel = preset.showTransaction ? '10' : '01';
      ctrl.includeRxnModel = preset.showReservation ? '10' : '01';
    }

    function savePreset(options) {
      const params = getPresetFields(options);
      return TransactionFilterTemplate.create(params);
    }

    function updatePreset(id, options) {
      const params = getPresetFields(options);
      return TransactionFilterTemplate.update(id, params);
    }

    function deletePreset(selected) {
      return TransactionFilterTemplate.delete(selected);
    }

    function checkPresetName(name) {
      return TransactionFilterTemplate
        .checkNameAvailability(name)
        .then(response => response.isNameAvailable);
    }

    function findTxnsByCSV($event) {
      nvFileSelectDialog.show($event, {
        title: nvTranslate.instant('container.transactions.find-txns-by-csv'),
        fileSelect: {
          accept: nvButtonFilePickerType.CSV,
          maxFileCount: 1,
        },
        buttonOkTitle: nvTranslate.instant('commons.submit'),
        onFinishSelect: files => nvCsvParser.parse(files[0], { delimiter: ',', skipEmptyLines: true }).then(onFinishSelect),
      });

      function onFinishSelect(result) {
        if (result && result.errors && result.errors.length > 0) {
          _.forEach(result.errors, err => nvToast.error(nvTranslate.instant('container.transactions.error-parsing-csv'), err.message));
          return;
        }

        goToPage(ctrl.PAGES.LOADING);

        const ids = _(result.data)
          .flatMap(txnStr => +txnStr)
          .filter(_.isInteger)
          .uniq()
          .value();

        Transaction.search({ ids: ids }).then(success, failure);

        function success(txns) {
          const idMap = _.keyBy(txns, 'id');
          const notExistIds = [];
          const cancelledIds = [];

          _.forEach(ids, (id) => {
            if (!idMap[id]) {
              notExistIds.push(id);
            } else if (idMap[id].granularStatus && idMap[id].granularStatus.trim().toLowerCase() === 'cancelled') {
              cancelledIds.push(id);
              delete idMap[id];
            }
          });

          if (notExistIds.length > 0 || cancelledIds.length > 0) {
            nvDialog.showSingle($event, {
              templateUrl: 'views/container/transactions-v2/dialog/transaction-id-errors.dialog.html',
              cssClass: 'transaction-id-errors',
              scope: {
                notExistIds: notExistIds,
                cancelledIds: cancelledIds,
                onCancel: $event => $mdDialog.cancel(),
              },
            });
          }

          // update transactions with omitted ids
          txns = _.values(idMap);

          const tableData = _([])
            .concat(getTxnData(txns))
            .map((value, idx) => {
              value.sequence = idx + 1;
              return value;
            })
            .value();

          generateTable(tableData, []);
        }

        function failure() {
          goToPage(ctrl.PAGES.FILTER);
        }
      }
    }

    // ////////////////////////////////////////////////
    // Helper Functions
    // ////////////////////////////////////////////////

    function generateTable(tableData, filters) {
      ctrl.selectedFilters = filters;

      _.forEach(ctrl.selectedFilters, (filter) => {
        filter.description = nvBackendFilterUtils.getDescription(filter);
      });

      ctrl.tableParam = nvTable.createTable({
        sequence: { displayName: 'container.transactions-v2.sn' },
        id: { displayName: 'container.transactions-v2.trvn-id' },
        orderId: { displayName: 'container.transactions-v2.order-id' },
        trackingId: { displayName: 'container.transactions-v2.tracking-id' },
        type: { displayName: 'container.transactions-v2.type' },
        shipper: { displayName: 'container.transactions-v2.shipper' },
        address: { displayName: 'container.transactions-v2.address' },
        routeId: { displayName: 'container.transactions-v2.route-id' },
        status: { displayName: 'container.transactions-v2.status' },
        startDateTime: { displayName: 'container.transactions-v2.start-time' },
        endDateTime: { displayName: 'container.transactions-v2.end-time' },
        dp: { displayName: 'container.transactions-v2.dp' },
        pickupSize: { displayName: 'container.transactions-v2.pick-up-size' },
        comments: { displayName: 'container.transactions-v2.comments' },
      });

      ctrl.tableParam.setData(tableData);

      goToPage(ctrl.PAGES.RESULT);
    }

    function getPresetFields(options) {
      const params = nvBackendFilterUtils.getAddPresetFields(
        options.name,
        getSelectedFilters()
      );

      params.routeGroupIds = _.map(params.routeGroupIds, 'id');
      params.showTransaction = ctrl.includeTxnModel === '10';
      params.showReservation = ctrl.includeRxnModel === '10';

      return {
        name: params.name,
        filter: _.omit(params, 'name'),
      };
    }

    function translateVars() {
      routeGroupsSelection = {
        displayName: nvTranslate.instant('container.transactions.route-group'),
        value: null,
        options: [],
      };
    }

    function showTxn() {
      return ctrl.includeTxnModel === '10';
    }

    function showRxn() {
      return ctrl.includeRxnModel === '10';
    }

    function getTxnAddress(txn) {
      return _.compact([
        txn.address1,
        txn.address2,
        txn.postcode,
        txn.city,
      ]).join(' ');
    }

    function getRxnAddress(rxn) {
      const waypoint = rxn.activeWaypoint;
      return _.compact([
        waypoint.address1,
        waypoint.address2,
        waypoint.postcode,
        waypoint.city,
      ]).join(' ');
    }

    function getRxnRouteId(rxn) {
      const route = rxn.activeWaypoint.activeRoute;
      return route ? route.id : '-';
    }

    function getDateTime(time) {
      return nvDateTimeUtils.displayDateTime(moment(time), nvTimezone.getOperatorTimezone());
    }

    function getDistributionPoint(id) {
      return id && distributionPointById[id] ? `${id} - ${distributionPointById[id].name}` : '-';
    }

    function getRxnType(rxn) {
      return rxn.onDemand ? 'On-Demand Reservation' : 'Reservation';
    }

    function addToRouteGroup($event) {
      const fields = {
        addIntoExistingOrNew: '10',
        addFields: RouteGroup.getAddFields(),
        routeGroupsSelection: routeGroupsSelection,
      };

      const selectedTableParam = nvTable.createTable({
        sequence: { displayName: 'container.transactions-v2.sn' },
        id: { displayName: 'container.transactions-v2.trvn-id' },
        orderId: { displayName: 'container.transactions-v2.order-id' },
        trackingId: { displayName: 'container.transactions-v2.tracking-id' },
        type: { displayName: 'container.transactions-v2.type' },
        shipper: { displayName: 'container.transactions-v2.shipper' },
        address: { displayName: 'container.transactions-v2.address' },
        routeId: { displayName: 'container.transactions-v2.route-id' },
        status: { displayName: 'container.transactions-v2.status' },
        startDateTime: { displayName: 'container.transactions-v2.start-time' },
        endDateTime: { displayName: 'container.transactions-v2.end-time' },
        dp: { displayName: 'container.transactions-v2.dp' },
        pickupSize: { displayName: 'container.transactions-v2.pick-up-size' },
        comments: { displayName: 'container.transactions-v2.comments' },
      });

      selectedTableParam.setData(ctrl.tableParam.getSelection());

      nvDialog.showSingle($event, {
        templateUrl: 'views/container/transactions-v2/dialog/transaction.dialog.html',
        cssClass: 'transaction',
        scope: _.assign(nvEditModelDialog.scopeTemplate({ onSave: onSave }), {
          fields: fields,
          tableParam: selectedTableParam,
        }),
      }).then(success);

      function onSave(extractedData, data) {
        const toBeGrouped = _(selectedTableParam.data)
          .groupBy(trvn => trvn._type)
          .mapValues(trvn => _.map(trvn, o => o.id))
          .value();

        const appendReferencesBody = {
          transactionIds: toBeGrouped.txn || [],
          reservationIds: toBeGrouped.rxn || [],
        };

        const addFields = {
          name: data.addFields.name.value,
          description: '',
        };

        const findOrCreatePromise = data.addIntoExistingOrNew === '10' ?
          // existing route group
          $q.when(data.routeGroupsSelection.value) :
          // new route group
          RouteGroup
            .create(addFields.name, addFields.description)
            .then(d => d.routeGroup.id);

        return findOrCreatePromise
          .then(rgId => RouteGroup.appendReferences(rgId, appendReferencesBody));
      }

      function success() {
        getAll();
        goToPage(ctrl.PAGES.FILTER);
        nvToast.success(nvTranslate.instant('container.transactions.success-add'));
      }
    }
  }
}());
