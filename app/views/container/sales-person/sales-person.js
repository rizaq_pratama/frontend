(function controller() {
  angular
    .module('nvOperatorApp.controllers')
    .controller('SalesPersonController', SalesPersonController);

  SalesPersonController.$inject = ['$q', 'nvTable', 'nvDialog', 'SalesPerson'];

  const COLUMNS = {
    id: { displayName: 'commons.id' },
    name: { displayName: 'commons.name' },
    code: { displayName: 'commons.code' },
  };

  function SalesPersonController($q, nvTable, nvDialog, SalesPerson) {
    const ctrl = this;

    ctrl.init = init;
    ctrl.editSalesPerson = editSalesPerson;
    ctrl.openCsvUploadDialog = openCsvUploadDialog;

    function init() {
      ctrl.tableParam = nvTable.createTable(COLUMNS);
      return SalesPerson.readAll().then(onSuccess);

      function onSuccess(data) {
        ctrl.tableParam.setData(data);
      }
    }

    function editSalesPerson($event, salesPerson) {
      nvDialog.showSingle($event, {
        templateUrl: 'views/container/sales-person/dialog/edit-sales-person/edit-sales-person.dialog.html',
        controller: 'EditSalesPersonDialogController',
        controllerAs: 'ctrl',
        cssClass: 'sales-person-edit-dialog',
        locals: {
          salesPersonData: salesPerson,
        },
      }).then(onClose);

      function onClose(data) {
        if (data !== -1) {
          ctrl.tableParam.mergeIn(_.castArray(data), 'id');
        }
      }
    }

    function openCsvUploadDialog($event) {
      nvDialog.showSingle($event, {
        templateUrl: 'views/container/sales-person/dialog/upload-sales-person/upload-sales-person.dialog.html',
        controller: 'UploadSalesPersonDialogController',
        controllerAs: 'ctrl',
        skipHide: true,
        clickOutsideToClose: false,
        cssClass: 'nv-dialog-upload-sales-person',
      }).then(onClose);

      function onClose(code) {
        if (code === 0) {
          init();
        }
      }
    }
  }
}());
