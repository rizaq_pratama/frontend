(function controller() {
  angular
    .module('nvOperatorApp.controllers')
    .controller('UploadSalesPersonDialogController', UploadSalesPersonDialogController);

  UploadSalesPersonDialogController.$inject = ['$q', '$mdDialog', 'SalesPerson', 'nvCsvParser', 'nvFileUtils',
    'nvDateTimeUtils', 'Excel', 'nvButtonFilePickerType', 'nvDialog', 'nvToast', 'nvTranslate'];

  const SAMPLE_DATA = JSON.stringify([
    { a: 'NAME-A', b: 'NA' },
    { a: 'NAME-B', b: 'NB' },
    { a: 'NAME-C', b: 'NC' },
  ]);

  const EMPTY_ARRY = [];

  function UploadSalesPersonDialogController($q, $mdDialog, SalesPerson, nvCsvParser, nvFileUtils,
    nvDateTimeUtils, Excel, nvButtonFilePickerType, nvDialog, nvToast, nvTranslate) {
    const ctrl = this;

    ctrl.filePickerType = nvButtonFilePickerType.CSV;
    ctrl.formSubmitState = 'idle';
    ctrl.fileSelected = [];
    ctrl.salesPersonDatas = [];

    ctrl.onCancel = onCancel;
    ctrl.isFormSubmitDisabled = isFormSubmitDisabled;
    ctrl.onSave = onSave;
    ctrl.getSampleCsv = getSampleCsv;
    ctrl.onFileSelect = onFileSelect;
    ctrl.resetSelectedFile = resetSelectedFile;
    ctrl.getFileName = getFileName;
    ctrl.getDatas = getDatas;

    function onCancel() {
      $mdDialog.cancel();
    }

    function isFormSubmitDisabled() {
      return _.size(getDatas()) === 0;
    }

    function onSave($event) {
      const requests = _.cloneDeep(getDatas());

      ctrl.bulkActionProgressPayload = {
        totalCount: _.size(requests),
        successCount: 0,
        currentIndex: 0,
        errors: [],
        errorObjects: [],
      };

      nvDialog.showSingle($event, {
        templateUrl: 'lib/ninja-commons/services/dialogs/bulk-action-progress/bulk-action-progress.dialog.html',
        cssClass: 'bulk-action-progress',
        controller: 'BulkActionProgressDialogController',
        controllerAs: 'ctrl',
        skipHide: true,
        clickOutsideToClose: false,
        locals: {
          payload: ctrl.bulkActionProgressPayload,
        },
      }).then(progressDialogClosed);

      startMultiSelectCreation(requests);

      function startMultiSelectCreation(datas) {
        let request;
        if (datas.length > 0) {
          ctrl.bulkActionProgressPayload.currentIndex += 1;
          request = _.head(requests.splice(0, 1));
          SalesPerson.create(request)
              .then(multiSelectSuccess, multiSelectError);
        }

        function multiSelectSuccess() {
          ctrl.bulkActionProgressPayload.successCount += 1;

          if (datas.length > 0) {
            startMultiSelectCreation(datas);
          }
        }

        function multiSelectError(error) {
          ctrl.bulkActionProgressPayload.errors.push(`Sales ${request.name}: ${_.get(error, 'message', 'Unknown Error')}`);
          ctrl.bulkActionProgressPayload.errorObjects.push({
            name: request.name,
            code: request.code,
          });
          if (datas.length > 0) {
            startMultiSelectCreation(datas);
          }
        }
      }

      function progressDialogClosed() {
        if (getSuccessCount() > 0) {
          nvToast.success(nvTranslate.instant('container.sales-person.n-of-m-sales-person-created-successfully', { n: getSuccessCount(), m: getTotalCount() }));
        }

        if (getErrorCount() > 0) {
          nvToast.error(nvTranslate.instant('container.sales-person.some-sales-person-fail-to-be-created'));
          const csvData = _.map(ctrl.bulkActionProgressPayload.errorObjects, data => ([
            data.name, data.code,
          ]));
          downloadUploadError(csvData);
        }

        $mdDialog.hide(0);

        function getSuccessCount() {
          return _.get(ctrl.bulkActionProgressPayload, 'successCount');
        }

        function getErrorCount() {
          return _.size(ctrl.bulkActionProgressPayload.errors);
        }

        function getTotalCount() {
          return ctrl.bulkActionProgressPayload.totalCount;
        }
      }
    }

    function getSampleCsv() {
      return SAMPLE_DATA;
    }

    function onFileSelect(file) {
      ctrl.fileSelected.push(file);
      ctrl.fileName = file.name;
      Excel.read(file, false).then(readSuccess);

      function readSuccess(readResult) {
        ctrl.salesPersonDatas = _(readResult.data)
          .filter(data => data['A'] != null && data['B'] != null)
          .map(data => ({
            name: _.trim(data['A']),
            code: _.trim(data['B']),
          }))
          .uniq()
          .value();
      }
    }

    function resetSelectedFile() {
      ctrl.fileSelected = [];
    }

    function getFileName() {
      const file = ctrl.fileSelected[0];
      return file.name || '';
    }

    function getDatas() {
      if (_.size(ctrl.salesPersonDatas) > 0) {
        return ctrl.salesPersonDatas;
      }
      return EMPTY_ARRY;
    }

    function downloadUploadError(datas) {
      const fileName = `error-sales-person-upload-${nvDateTimeUtils.displayISO(moment())}.csv`;
      nvCsvParser.unparse(datas).then((csv) => {
        nvFileUtils.downloadCsv(csv, fileName);
      });
    }
  }
}());
