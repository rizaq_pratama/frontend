(function controller() {
  angular
    .module('nvOperatorApp.controllers')
    .controller('EditSalesPersonDialogController', EditSalesPersonDialogController);

  EditSalesPersonDialogController.$inject = ['$mdDialog', 'salesPersonData', 'SalesPerson'];

  function EditSalesPersonDialogController($mdDialog, salesPersonData, SalesPerson) {
    const ctrl = this;

    ctrl.salesPerson = _.cloneDeep(salesPersonData);

    ctrl.saveState = 'idle';

    ctrl.onCancel = onCancel;
    ctrl.onSave = onSave;

    function onSave() {
      SalesPerson.update(_.get(ctrl.salesPerson, 'id'), ctrl.salesPerson)
        .then((response) => {
          response.id = _.get(ctrl.salesPerson, 'id');
          $mdDialog.hide(response);
        });
    }
    function onCancel() {
      $mdDialog.cancel();
    }
  }
}());
