(function controller() {
  angular
        .module('nvOperatorApp.controllers')
        .controller('ContactTypeManagementController', ContactTypeManagementController);

  ContactTypeManagementController.$inject = ['$q', 'DriverContactType', 'nvDialog',
      'nvEditModelDialog', 'nvUtils', 'nvTableUtils'];

  function ContactTypeManagementController($q, DriverContactType, nvDialog,
                                           nvEditModelDialog, nvUtils, nvTableUtils) {
    // ////////////////////////////////////////////////
    // variables
    // ////////////////////////////////////////////////
    const ctrl = this;
    const columns = ['id', 'name'];
    ctrl.contactTypes = [];
    ctrl.contactTypesTableParams = null;

    // ////////////////////////////////////////////////
    // functions
    // ////////////////////////////////////////////////
    ctrl.getContactTypes = getContactTypes;
    ctrl.onAdd = onAdd;
    ctrl.onRemove = onRemove;
    ctrl.onEdit = onEdit;
    ctrl.onSearch = onSearch;

    // ////////////////////////////////////////////////
    // functions details
    // ////////////////////////////////////////////////
    function getContactTypes() {
      DriverContactType.searchAll().then(success, $q.reject);

      function success(response) {
        ctrl.contactTypes = _.sortBy(ctrl.contactTypes.concat(response.contactTypes), 'id');
        ctrl.contactTypesTableParams = nvTableUtils.generate(ctrl.contactTypes,
          { columns: columns });
      }
    }

    function onAdd($event) {
      const fields = DriverContactType.getAddFields();

      nvDialog.showSingle($event, {
        templateUrl: 'views/container/contact-type-management/dialog/contact-type-add.html',
        cssClass: 'contact-type-add',
        theme: 'nvGreen',
        scope: _.assign(nvEditModelDialog.scopeTemplate({ onSave: create }), { fields: fields }),
      }).then(success);

      function create(data) {
        const contactType = { contactType: data };
        return DriverContactType.createOne(contactType);
      }

      function success(response) {
        ctrl.contactTypes = _.sortBy(ctrl.contactTypes.concat(response.contactType), 'id');
        nvTableUtils.addTo(ctrl.contactTypesTableParams,
          response.contactType, { columns: columns });
      }
    }

    function onEdit($event, model) {
      const fields = DriverContactType.getEditFields(model);

      nvDialog.showSingle($event, {
        templateUrl: 'views/container/contact-type-management/dialog/contact-type-edit.html',
        cssClass: 'contact-type-edit',
        scope: _.assign(nvEditModelDialog.scopeTemplate({ onSave: edit }), { fields: fields }),
      }).then(success);

      function edit(data) {
        const contactType = { contactType: data };
        return DriverContactType.updateOne(contactType);
      }

      function success(response) {
        ctrl.contactTypes = nvUtils.mergeIn(ctrl.contactTypes, response.contactType, 'id', response.contactType.id);
        nvTableUtils.mergeIn(ctrl.contactTypesTableParams,
          response.contactType, { columns: columns });
      }
    }

    function onRemove($event, model) {
      nvDialog.confirmDelete($event, {
        content: `Are you sure you want to permanently delete \'${model.name}\'?`,
      }).then(remove);

      function remove() {
        DriverContactType.deleteOne(model.id).then(success, $q.reject);
      }

      function success(response) {
        _.remove(ctrl.contactTypes, { id: response.contactType.id });
        nvTableUtils.remove(ctrl.contactTypesTableParams, 'id', response.contactType.id);
      }
    }

    function onSearch(searchText) {
      if (ctrl.contactTypesTableParams) {
        ctrl.contactTypesTableParams.filter({ $: searchText });
      }
    }
    // ////////////////////////////////////////////////
    // Helper Functions
    // ////////////////////////////////////////////////
  }
}());
