(function controller() {
  angular
    .module('nvOperatorApp.controllers')
    .controller('ReservationRejectionController', ReservationRejectionController);

  ReservationRejectionController.$inject = [
    '$q', 'Driver', 'Hub', 'Reservation', 'nvTable',
    'nvDateTimeUtils', '$interval', 'nvDialog', 'Shippers', 'nvTimezone',
    '$scope',
  ];

  function ReservationRejectionController(
    $q, Driver, Hub, Reservation, nvTable,
    nvDateTimeUtils, $interval, nvDialog, Shippers, nvTimezone,
    $scope
  ) {
    // variables
    const REJECTION_FORM_ACTION = {
      REASSIGN: 'reassign',
      FAIL_PICKUP: 'fail-pickup',
    };
    let SHIPPERS_MAP = {};
    const REFRESH_INTERVAL = 900000;

    let DRIVERS_MAP;
    let HUBS_MAP;

    let routesResult = null;
    let failureReasonsResult = null;

    const ctrl = this;
    ctrl.REJECTION_FORM_ACTION = REJECTION_FORM_ACTION;
    ctrl.driversResult = null;
    ctrl.hubsResult = null;
    ctrl.rejectedReservationsTableParam = null;
    ctrl.interval = null;
    ctrl.lastUpdated = null;

    // functions
    ctrl.getAll = getAll;
    ctrl.showRejectionFormDialog = showRejectionFormDialog;
    ctrl.showVerifyAddressDialog = showVerifyAddressDialog;

    // start
    ctrl.interval = $interval(refreshRejectedReservationsPromise, REFRESH_INTERVAL);
    $scope.$on('$destroy', () => {
      cancelInterval();
    });

    // functions details
    function getAll() {
      return $q.all([
        Driver.searchAll(),
        Hub.read(),
      ]).then(success);

      function success(response) {
        ctrl.driversResult = response[0];
        ctrl.hubsResult = response[1];

        DRIVERS_MAP = _.keyBy(_.get(ctrl.driversResult, 'drivers'), 'id');
        HUBS_MAP = _.keyBy(ctrl.hubsResult, 'id');

        return refreshRejectedReservationsPromise();
      }
    }

    function showRejectionFormDialog($event, rejectedReservation, currentAction) {
      cancelInterval();

      const resultData = {
        routes: routesResult,
        failureReasons: failureReasonsResult,
      };

      nvDialog.showSingle($event, {
        templateUrl: 'views/container/reservation-rejection/dialog/rejection-form/rejection-form.dialog.html',
        cssClass: 'reservation-rejection-form',
        controller: 'ReservationRejectionFormDialogController',
        controllerAs: 'ctrl',
        locals: {
          rejectedReservationData: rejectedReservation,
          currentActionData: currentAction,
          resultData: resultData,
          REJECTION_FORM_ACTION: REJECTION_FORM_ACTION,
        },
      }).then(onSet, onCancel).finally(finallyFn);

      function onSet(response) {
        ctrl.rejectedReservationsTableParam.remove(rejectedReservation, 'id');
        handleCachedResult(response.cachedResult);
      }

      function onCancel(response) {
        handleCachedResult(response);
      }

      function finallyFn() {
        setInterval();
      }

      function handleCachedResult(result) {
        if (_.get(result, 'routesResult')) {
          routesResult = _.get(result, 'routesResult');
        }

        if (_.get(result, 'failureReasonsResult')) {
          failureReasonsResult = _.get(result, 'failureReasonsResult');
        }
      }
    }

    function showVerifyAddressDialog($event, rejectedReservation) {
      cancelInterval();

      nvDialog.showSingle($event, {
        templateUrl: 'views/container/reservation-rejection/dialog/verify-pickup-address/verify-pickup-address.dialog.html',
        cssClass: 'reservation-rejection-verify-pickup-address',
        controller: 'ReservationRejectionVerifyPickupAddressDialogController',
        controllerAs: 'ctrl',
        locals: {
          rejectedReservationData: rejectedReservation,
        },
      }).then(onSet).finally(finallyFn);

      function onSet(response) {
        rejectedReservation.from_latitude = response.latitude;
        rejectedReservation.from_longitude = response.longitude;

        ctrl.rejectedReservationsTableParam.mergeIn(rejectedReservation, 'id');
      }

      function finallyFn() {
        setInterval();
      }
    }

    function refreshRejectedReservationsPromise() {
      const deferred = $q.defer();
      let rejectedReservations = null;

      Reservation.getRejectedReservations({
        status: Reservation.BOOKING_STATUS.REJECTED,
      })
        .then(getRejectedReservationsSuccess, getRejectedReservationsFailure);

      return deferred.promise;

      function getRejectedReservationsSuccess(response) {
        ctrl.lastUpdated = {
          date: nvDateTimeUtils.displayDate(new Date()),
          time: nvDateTimeUtils.display12HourTime(new Date()),
        };

        rejectedReservations = _.get(response, 'bookings') || [];

        Shippers.searchByIds(
          _.uniq(_.map(rejectedReservations, 'shipper_id'))
        ).then(getShippersSuccess, getShippersFailure);
      }

      function getRejectedReservationsFailure(response) {
        deferred.reject(response);
      }

      function getShippersSuccess(shippers) {
        SHIPPERS_MAP = _.keyBy(shippers, 'id');

        const extendedRejectedReservations = extendRejectedReservations(rejectedReservations);

        if (ctrl.rejectedReservationsTableParam == null) {
          ctrl.rejectedReservationsTableParam = nvTable.createTable(Reservation.REJECTED_FIELDS);
          ctrl.rejectedReservationsTableParam.addColumn('c_timeRejected', {
            displayName: 'commons.model.time-rejected',
          });
          ctrl.rejectedReservationsTableParam.addColumn('c_pickup', {
            displayName: 'container.reservation-rejection.pickup-info',
          });
          ctrl.rejectedReservationsTableParam.addColumn('c_timeslot', {
            displayName: 'commons.timeslot',
          });
          ctrl.rejectedReservationsTableParam.addColumn('c_driver', {
            displayName: 'container.reservation-rejection.driver-info',
          });
          ctrl.rejectedReservationsTableParam.addColumn('c_hubName', {
            displayName: 'commons.hub',
          });

          ctrl.rejectedReservationsTableParam.updateColumnFilter('c_timeRejected', strFn => (
            (rejectedReservation) => {
              // strfn is the function that will return you the search text
              const lowercase = strFn().toLowerCase();
              return lowercase.length === 0 ||
                _.toLower(_.get(rejectedReservation, 'c_dateRejected')).indexOf(lowercase) >= 0 ||
                _.toLower(_.get(rejectedReservation, 'c_timeRejected')).indexOf(lowercase) >= 0;
            }
          ));

          ctrl.rejectedReservationsTableParam.updateColumnFilter('c_timeslot', strFn => (
            (rejectedReservation) => {
              // strfn is the function that will return you the search text
              const lowercase = strFn().toLowerCase();
              return lowercase.length === 0 ||
                _.toLower(_.get(rejectedReservation, 'c_dateslot')).indexOf(lowercase) >= 0 ||
                _.toLower(_.get(rejectedReservation, 'c_timeslot')).indexOf(lowercase) >= 0;
            }
          ));

          ctrl.rejectedReservationsTableParam.updateColumnFilter('c_pickup', strFn => (
            (rejectedReservation) => {
              // strfn is the function that will return you the search text
              const lowercase = strFn().toLowerCase();
              return lowercase.length === 0 ||
                _.toLower(_.get(rejectedReservation, 'c_pickup.name')).indexOf(lowercase) >= 0 ||
                _.toLower(_.get(rejectedReservation, 'c_pickup.address')).indexOf(lowercase) >= 0;
            }
          ));

          ctrl.rejectedReservationsTableParam.updateColumnFilter('c_driver', strFn => (
            (rejectedReservation) => {
              // strfn is the function that will return you the search text
              const lowercase = strFn().toLowerCase();
              return lowercase.length === 0 ||
                _.toLower(_.get(rejectedReservation, 'c_driver.name')).indexOf(lowercase) >= 0 ||
                _.some(_.get(rejectedReservation, 'c_driver.contacts'), contact =>
                  contact.details && _.toLower(contact.details).indexOf(lowercase) >= 0
              );
            }
          ));

          ctrl.rejectedReservationsTableParam.setSortFunction('c_timeslot', (data, orderBy) =>
            _.orderBy(data, ['c_slotMilis', 'id'], [orderBy, 'asc']));

          ctrl.rejectedReservationsTableParam.setSortFunction('c_timeRejected', (data, orderBy) =>
            _.orderBy(data, ['c_rejectedMilis', 'id'], [orderBy, 'asc']));

          ctrl.rejectedReservationsTableParam.setData(extendedRejectedReservations);
        } else {
          ctrl.rejectedReservationsTableParam.mergeIn(extendedRejectedReservations, 'id');
        }

        deferred.resolve(extendedRejectedReservations);
      }

      function getShippersFailure(response) {
        deferred.reject(response);
      }
    }

    function extendRejectedReservations(rejectedReservations) {
      return _.map(rejectedReservations, rejectedReservation => (
        setCustomRejectedReservationData(rejectedReservation)
      ));

      function setCustomRejectedReservationData(rejectedReservation) {
        rejectedReservation.c_timeRejected = '-';
        if (rejectedReservation.rejected_at) {
          rejectedReservation.c_dateRejected = nvDateTimeUtils.displayDate(
            nvDateTimeUtils.toDate(rejectedReservation.rejected_at, 'utc'),
            nvTimezone.getOperatorTimezone()
          );
          rejectedReservation.c_timeRejected = nvDateTimeUtils.display12HourTime(
            nvDateTimeUtils.toDate(rejectedReservation.rejected_at, 'utc'),
            nvTimezone.getOperatorTimezone()
          );
        }

        const rejectedDate = nvDateTimeUtils.toDate(rejectedReservation.rejected_at, 'utc');
        rejectedReservation.c_rejectedMilis = rejectedDate ? rejectedDate.getTime() : 0;

        rejectedReservation.c_dateslot = getDateslot(rejectedReservation);
        rejectedReservation.c_timeslot = getTimeslot(rejectedReservation);
        const slotDate = nvDateTimeUtils.toDate(rejectedReservation.start, 'utc');
        rejectedReservation.c_slotMilis = slotDate ? slotDate.getTime() : 0;

        rejectedReservation.c_pickup = _.cloneDeep(getShipper(rejectedReservation.shipper_id));
        if (rejectedReservation.c_pickup) {
          rejectedReservation.c_pickup.address = _.join(_.compact([
            rejectedReservation.from_address_1,
            rejectedReservation.from_address_2,
            rejectedReservation.from_district,
            rejectedReservation.from_province,
            rejectedReservation.from_city,
            rejectedReservation.from_region,
            rejectedReservation.from_state,
            rejectedReservation.from_country,
            rejectedReservation.from_postal_code,
          ]), ' ');
        }

        rejectedReservation.c_driver = _.cloneDeep(getDriver(rejectedReservation.driver_id));
        if (rejectedReservation.c_driver) {
          rejectedReservation.c_driver.name = _.join(_.compact([
            rejectedReservation.c_driver.firstName,
            rejectedReservation.c_driver.lastName,
          ]), ' ');
          rejectedReservation.c_driver.contacts = _.filter(rejectedReservation.c_driver.contacts, ['active', true]);
        }

        rejectedReservation.c_hubName = _.get(getHub(rejectedReservation.hub_id), 'name') || null;

        return rejectedReservation;

        function getDateslot(theRejectedReservation) {
          return nvDateTimeUtils.displayDate(
            nvDateTimeUtils.toDate(theRejectedReservation.start, 'utc'),
            nvTimezone.getOperatorTimezone()
          );
        }

        function getTimeslot(theRejectedReservation) {
          const start = nvDateTimeUtils.display12HourTime(
            nvDateTimeUtils.toDate(theRejectedReservation.start, 'utc'),
            nvTimezone.getOperatorTimezone()
          );

          const end = nvDateTimeUtils.display12HourTime(
            nvDateTimeUtils.toDate(theRejectedReservation.end, 'utc'),
            nvTimezone.getOperatorTimezone()
          );

          return `${start} - ${end}`;
        }
      }
    }

    function getShipper(shipperId) {
      return SHIPPERS_MAP[shipperId] || null;
    }

    function getHub(hubId) {
      return HUBS_MAP[hubId] || null;
    }

    function getDriver(driverId) {
      return DRIVERS_MAP[driverId] || null;
    }

    function setInterval() {
      ctrl.interval = $interval(refreshRejectedReservationsPromise, REFRESH_INTERVAL);
    }

    function cancelInterval() {
      $interval.cancel(ctrl.interval);
    }
  }
}());
