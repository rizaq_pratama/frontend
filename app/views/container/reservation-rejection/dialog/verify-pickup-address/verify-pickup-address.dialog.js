(function controller() {
  angular
    .module('nvOperatorApp.controllers')
    .controller('ReservationRejectionVerifyPickupAddressDialogController', ReservationRejectionVerifyPickupAddressDialogController);

  ReservationRejectionVerifyPickupAddressDialogController.$inject = [
    '$scope', 'rejectedReservationData', '$mdDialog', '$timeout', 'nvMaps',
    'nvTranslate', '$q', 'Reservation', 'Shippers',
  ];

  function ReservationRejectionVerifyPickupAddressDialogController(
    $scope, rejectedReservationData, $mdDialog, $timeout, nvMaps,
    nvTranslate, $q, Reservation, Shippers
  ) {
    // variables
    const ctrl = this;
    let updatedMarker = null;
    let addressId = null;
    let reservationTypeValue = null;

    ctrl.formSubmitState = 'idle';
    ctrl.rejectedReservation = _.cloneDeep(rejectedReservationData);
    ctrl.currentAddress = null;
    ctrl.formData = {
      inputLatLng: null,
      latitude: null,
      longitude: null,
    };
    ctrl.map = null;

    // functions
    ctrl.getAll = getAll;
    ctrl.onSave = onSave;
    ctrl.isAbleToSubmit = isAbleToSubmit;
    ctrl.onCancel = onCancel;
    ctrl.isValidCoordinates = isValidCoordinates;
    ctrl.updateCoordinates = updateCoordinates;

    // functions details
    function getAll() {
      return $q.all([
        Reservation.read({
          reservation_id: ctrl.rejectedReservation.reservation_id,
        }),
      ]).then(success);

      function success(response) {
        const reservation = _.get(response[0], '[0]') || null;
        addressId = _.get(reservation, 'addressId');
        reservationTypeValue = Reservation.getTypeId(_.get(reservation, 'reservationType'));

        initialize();
      }
    }

    function initialize() {
      ctrl.currentAddress = _.join([
        ctrl.rejectedReservation.c_pickup.address,
        `[${_.join([ctrl.rejectedReservation.from_latitude, ctrl.rejectedReservation.from_longitude], ', ')}]`,
      ], '\n');

      $timeout(() => {
        ctrl.map = nvMaps.initialize('verify-address-map', {
          zoomLevel: 16,
        });

        let options = {
          latitude: ctrl.rejectedReservation.from_latitude,
          longitude: ctrl.rejectedReservation.from_longitude,
          draggable: false,
          label: nvTranslate.instant('container.reservation-rejection.current-location'),
        };
        const iconConfig = {
          fillColor: 'b81233',
        };

        options = _.merge(options, nvMaps.getIconData(iconConfig));
        nvMaps.addMarker(ctrl.map, options);
        nvMaps.flyToLocation(ctrl.map, {
          latitude: options.latitude,
          longitude: options.longitude,
        });
      });
    }

    function onSave() {
      ctrl.formSubmitState = 'waiting';

      Shippers.updateAddress(
        _.get(ctrl.rejectedReservation, 'c_pickup.global_id'),
        addressId,
        {
          latitude: ctrl.formData.latitude,
          longitude: ctrl.formData.longitude,
        }
      ).then(updateAddressSuccess, updateAddressFailure);

      function updateAddressSuccess() {
        Reservation.updateV2(ctrl.rejectedReservation.reservation_id, {
          pickup_address_id: addressId,
          legacy_shipper_id: ctrl.rejectedReservation.shipper_id,
          reservation_type_value: reservationTypeValue,
        }).then(updateReservationSuccess).finally(finallyFn);
      }

      function updateAddressFailure() {
        finallyFn();
      }

      function updateReservationSuccess() {
        $mdDialog.hide({
          latitude: ctrl.formData.latitude,
          longitude: ctrl.formData.longitude,
        });
      }

      function finallyFn() {
        ctrl.formSubmitState = 'idle';
      }
    }

    function isAbleToSubmit() {
      return $scope.modelForm && $scope.modelForm.$valid &&
        ctrl.formData.latitude !== null && ctrl.formData.longitude !== null;
    }

    function onCancel() {
      $mdDialog.cancel();
    }

    function isValidCoordinates() {
      const latLngArray = getLatLngFromCoordinates();

      if (_.size(latLngArray) !== 2) {
        return false;
      }

      if (latLngArray[0] < -90 || latLngArray[0] > 90) {
        return false;
      }

      if (latLngArray[1] < -180 || latLngArray[1] > 180) {
        return false;
      }

      return true;
    }

    function updateCoordinates() {
      if (updatedMarker) {
        nvMaps.removeMarker(ctrl.map, updatedMarker);
      }

      const latLngArray = getLatLngFromCoordinates();
      ctrl.formData.latitude = latLngArray[0];
      ctrl.formData.longitude = latLngArray[1];

      let options = {
        latitude: ctrl.formData.latitude,
        longitude: ctrl.formData.longitude,
        draggable: false,
        label: nvTranslate.instant('container.reservation-rejection.updated-location'),
      };
      const iconConfig = {
        type: 'd_map_xpin_icon',
        fillColor: '25282a',
        icon: 'home',
      };

      options = _.merge(options, nvMaps.getIconData(iconConfig));
      updatedMarker = nvMaps.addMarker(ctrl.map, options);
      nvMaps.flyToLocation(ctrl.map, {
        latitude: options.latitude,
        longitude: options.longitude,
      });
    }

    function getLatLngFromCoordinates() {
      return _.map(_.split(_.trim(ctrl.formData.inputLatLng, ','), ','), _.toFinite);
    }
  }
}());
