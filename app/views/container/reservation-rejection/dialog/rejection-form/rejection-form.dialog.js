(function controller() {
  angular
    .module('nvOperatorApp.controllers')
    .controller('ReservationRejectionFormDialogController', ReservationRejectionFormDialogController);

  ReservationRejectionFormDialogController.$inject = [
    '$scope', 'rejectedReservationData', '$mdDialog', 'nvDateTimeUtils', 'nvTranslate',
    'REJECTION_FORM_ACTION', 'currentActionData', 'resultData', 'Route', '$q',
    'Driver', '$timeout', 'Reservation', 'nvToast',
  ];

  function ReservationRejectionFormDialogController(
    $scope, rejectedReservationData, $mdDialog, nvDateTimeUtils, nvTranslate,
    REJECTION_FORM_ACTION, currentActionData, resultData, Route, $q,
    Driver, $timeout, Reservation, nvToast
  ) {
    // variables
    let routesResult = null;
    let failureReasonsResult = null;

    const ctrl = this;
    ctrl.REJECTION_FORM_ACTION = REJECTION_FORM_ACTION;

    ctrl.theme = 'nvBlue';
    ctrl.title = null;
    ctrl.formSubmitState = 'idle';

    ctrl.rejectedReservation = _.cloneDeep(rejectedReservationData);
    ctrl.currentAction = currentActionData;
    ctrl.routeSelectionOptions = null;

    ctrl.chosenRoute = null;
    ctrl.chosenFailureReasons = [];

    // functions
    ctrl.getAll = getAll;
    ctrl.failureReasonOnChange = failureReasonOnChange;
    ctrl.onSave = onSave;
    ctrl.isAbleToSubmit = isAbleToSubmit;
    ctrl.onCancel = onCancel;

    // start
    initialize();

    // functions details
    function initialize() {
      switch (ctrl.currentAction) {
        case REJECTION_FORM_ACTION.REASSIGN:
          ctrl.title = nvTranslate.instant('container.reservation-rejection.reassign-reservation');
          ctrl.submitButtonTitle = nvTranslate.instant('container.reservation-rejection.reassign');
          break;
        case REJECTION_FORM_ACTION.FAIL_PICKUP:
          ctrl.title = nvTranslate.instant('container.reservation-rejection.fail-pickup');
          ctrl.submitButtonTitle = nvTranslate.instant('container.reservation-rejection.fail-pickup');
          break;
        default:
      }
    }

    function getAll() {
      const params = {
        from: nvDateTimeUtils.displayDateTime(nvDateTimeUtils.toSOD(new Date()), 'utc'),
        to: nvDateTimeUtils.displayDateTime(nvDateTimeUtils.toEOD(new Date()), 'utc'),
      };

      switch (ctrl.currentAction) {
        case REJECTION_FORM_ACTION.REASSIGN:
          if (resultData.routes) {
            return $q.when([
              resultData.routes,
            ]).then(success);
          }

          return $q.all([
            Route.fetchRoute(params),
          ]).then(success);
        case REJECTION_FORM_ACTION.FAIL_PICKUP:
          if (resultData.failureReasons) {
            return $q.when([
              resultData.failureReasons,
            ]).then(success);
          }

          return $q.all([
            Driver.searchFailureReasons(),
          ]).then(success);
        default:
          return $q.when(null);
      }

      function success(response) {
        switch (ctrl.currentAction) {
          case REJECTION_FORM_ACTION.REASSIGN:
            routesResult = response[0];
            ctrl.routeSelectionOptions = _(routesResult).filter(route => !!route.driver_id)
              .map(route => ({
                value: route.route_id,
                driverId: route.driver_id,
                displayName: `${route.route_id} - ${route.driver_name}`,
              }))
              .value();
            break;
          case REJECTION_FORM_ACTION.FAIL_PICKUP:
            failureReasonsResult = response[0];
            addFailureReasonSelectionRowIfApplicable(null);
            break;
          default:
        }
      }
    }

    function failureReasonOnChange(failureReason) {
      $timeout(() => {
        const failureReasonIndex = _.findIndex(ctrl.chosenFailureReasons, [
          'parentId', failureReason.parentId,
        ]);

        if (failureReasonIndex >= 0) {
          ctrl.chosenFailureReasons.splice(
            failureReasonIndex + 1,
            (_.size(ctrl.chosenFailureReasons) - (failureReasonIndex + 1))
          );
        }

        addFailureReasonSelectionRowIfApplicable(failureReason.selectedId);
      });
    }

    function onSave() {
      ctrl.formSubmitState = 'waiting';

      const payload = {};
      let lastChosenFailureReason;
      switch (ctrl.currentAction) {
        case REJECTION_FORM_ACTION.REASSIGN:
          payload.hub_id = ctrl.rejectedReservation.hub_id;
          payload.route_id = ctrl.chosenRoute.value;

          Reservation.assignReservation(
            ctrl.rejectedReservation.reservation_id,
            ctrl.chosenRoute.driverId,
            payload
          ).then(success).finally(finallyFn);
          break;
        case REJECTION_FORM_ACTION.FAIL_PICKUP:
          lastChosenFailureReason = _.last(ctrl.chosenFailureReasons);

          payload.failure_reason_id = lastChosenFailureReason.selectedId;
          payload.failure_reason = _.find(failureReasonsResult.failureReasons, ['id', payload.failure_reason_id]).description;

          Reservation.failReservation(ctrl.rejectedReservation.id, payload)
            .then(success).finally(finallyFn);
          break;
        default:
      }

      function success(response) {
        switch (ctrl.currentAction) {
          case REJECTION_FORM_ACTION.REASSIGN:
            nvToast.success(
              nvTranslate.instant('container.reservation-rejection.reassigned-successfully')
            );
            break;
          case REJECTION_FORM_ACTION.FAIL_PICKUP:
            nvToast.success(
              nvTranslate.instant('container.reservation-rejection.pickup-has-been-failed')
            );
            break;
          default:
        }

        $mdDialog.hide({
          response: response,
          cachedResult: {
            routesResult: routesResult,
            failureReasonsResult: failureReasonsResult,
          },
        });
      }

      function finallyFn() {
        ctrl.formSubmitState = 'idle';
      }
    }

    function isAbleToSubmit() {
      return $scope.modelForm && $scope.modelForm.$valid;
    }

    function onCancel() {
      $mdDialog.cancel({
        routesResult: routesResult,
        failureReasonsResult: failureReasonsResult,
      });
    }

    function addFailureReasonSelectionRowIfApplicable(parentId) {
      const filteredFailureReasons = _.filter(failureReasonsResult.failureReasons, failureReason =>
        failureReason.parent === parentId && failureReason.mode === 'Pickup'
      );
      const failureReasonsOptions = _.map(filteredFailureReasons, failureReason => ({
        displayName: `${failureReason.description} - ${failureReason.type}`,
        value: failureReason.id,
      }));

      if (_.size(failureReasonsOptions) > 0) {
        ctrl.chosenFailureReasons.push({
          parentId: parentId,
          options: failureReasonsOptions,
          selectedId: null,
        });
      }
    }
  }
}());
