(function controller() {
  angular
    .module('nvOperatorApp.controllers')
    .controller('PriorityLevelController', PriorityLevelController);

  PriorityLevelController.$inject = ['nvCsvParser', 'nvFileUtils', 'nvToast', 'nvDateTimeUtils',
    'nvTimezone', 'PriorityLevel', 'nvDialog', '$translate', 'nvTable', 'nvFileSelectDialog',
    'nvTranslate', 'Excel'];

  const API_STATE = {
    IDLE: 'idle',
    WAITING: 'waiting',
  };

  const SAMPLE_CSV = [
    { 'Transaction ID': 1106977, 'Priority Level': 1 },
    { 'Transaction ID': 1109697, 'Priority Level': 1 },
    { 'Transaction ID': 1106193, 'Priority Level': 11 },
    { 'Transaction ID': 1109693, 'Priority Level': 20 },
  ];

  const SAMPLE_CSV_RSVN = [
    { 'Reservation ID': 1106977, 'Priority Level': 1 },
    { 'Reservation ID': 1109697, 'Priority Level': 1 },
    { 'Reservation ID': 1106193, 'Priority Level': 11 },
    { 'Reservation ID': 1109693, 'Priority Level': 20 },
  ];

  const COLUMNS = {
    transactionId: { displayName: 'container.update-priority-level.table.transaction-id' },
    oldPriorityLevel: { displayName: 'container.update-priority-level.table.old-priority-level' },
    priorityLevel: { displayName: 'container.update-priority-level.table.new-priority-level' },
  };

  const CSV_TYPE = {
    ORDER: 0,
    RSVN: 1,
  };

  function PriorityLevelController(nvCsvParser, nvFileUtils, nvToast, nvDateTimeUtils,
    nvTimezone, PriorityLevel, nvDialog, $translate, nvTable, nvFileSelectDialog,
    nvTranslate, Excel) {
    const ctrl = this;

    ctrl.processAll = processAll;
    ctrl.csvType = CSV_TYPE;
    ctrl.downloadSampleCsv = downloadSampleCsv;
    ctrl.isDataReady = isDataReady;
    ctrl.editPriorityLevel = editPriorityLevel;
    ctrl.isTableUpdateDisabled = isTableUpdateDisabled;
    ctrl.bulkUpdate = bulkUpdate;
    ctrl.selectCsv = selectCsv;
    ctrl.cancelCsv = cancelCsv;

    init();

    function init() {
      ctrl.processAllState = API_STATE.IDLE;
      ctrl.tableUpdateState = API_STATE.IDLE;
      setBackdrop(false);

      ctrl.date = moment().toDate();
      ctrl.preProcessData = [];
      ctrl.currentFileName = 'None';
      ctrl.isTableReady = false;
    }

    function processAll($event) {
      const date = nvDateTimeUtils.toSOD(ctrl.date, nvTimezone.getOperatorTimezone());
      showProcessAllPrompt();
      startProcessAll();

      function showProcessAllPrompt() {
        nvDialog.confirmDelete($event, {
          title: 'Confirmation',
          content: `this action will execute priority level update on ${nvDateTimeUtils.displayDate(date)}, continue?`,
          ok: $translate.instant('commons.continue'),
          cancel: $translate.instant('commons.cancel'),
          skipHide: false,
        }).then(startProcessAll);
      }

      function startProcessAll(val) {
        // dont continue if cancel selected
        if (!val) {
          return;
        }

        ctrl.processAllState = API_STATE.WAITING;
        const payload = {
          route_date: nvDateTimeUtils.displayDateTime(date, 'utc'),
        };

        setBackdrop(true);
        PriorityLevel.update(payload).then(onSuccess, onError);

        function onSuccess(data) {
          if (data && data.result) {
            nvToast.success(
              nvTranslate.instant('container.update-priority-level.update-by-route-date-success'),
              `processed date: ${ctrl.date}`
            );
            ctrl.processAllState = API_STATE.IDLE;
            setBackdrop(false);
          } else {
            onError({ message: 'update failed' });
          }
        }

        function onError() {
          nvToast.warning(
            nvTranslate.instant('container.update-priority-level.update-by-route-date-failed'),
          );
          ctrl.processAllState = API_STATE.IDLE;
          setBackdrop(false);
        }
      }
    }

    function selectCsv($event, mode) {
      ctrl.preProcessData = [];
      const TITLE = mode === CSV_TYPE.ORDER ?
        nvTranslate.instant('container.order.create.upload-order-csv') :
        nvTranslate.instant('container.update-priority-level.upload-reservations-csv');
      nvFileSelectDialog.show($event, {
        title: TITLE,
        fileSelect: {
          accept: '.csv,.xlsx,.xls',
          maxFileCount: 1,
        },
        buttonOkTitle: nvTranslate.instant('commons.submit'),
        onFinishSelect: onFinishSelect,
      });

      function onFinishSelect(files) {
        Excel.read(files[0]).then(excelReadSuccess);

        function excelReadSuccess(response) {
          setBackdrop(true);
          processCsvData(response);
        }

        function processCsvData(result) {
          // make sure csv contianing transaction_id column
          ctrl.preProcessData = [];
          if (mode === CSV_TYPE.ORDER) {
            ctrl.preProcessData = _(result.data)
            .filter(data => data['Transaction ID'] != null && data['Priority Level'] != null)
            .map(data => ({
              transactionId: Number.parseInt(data['Transaction ID'], 0),
              priorityLevel: Number.parseInt(data['Priority Level'], 0),
            }))
            .value();
          } else if (mode === CSV_TYPE.RSVN) {
            ctrl.preProcessData = _(result.data)
              .filter(data => data['Reservation ID'] != null && data['Priority Level'] != null)
              .map(data => ({
                id: Number.parseInt(data['Reservation ID'], 0),
                priorityLevel: Number.parseInt(data['Priority Level'], 0),
              }))
              .value();
          }

          if (_.size(ctrl.preProcessData) === 0) {
            nvToast.warning(
              nvTranslate.instant('container.update-priority-level.failed-to-read-csv'),
              nvTranslate.instant('container.update-priority-level.please-check-your-file')
            );
            setBackdrop(false);
            return;
          }

          if (mode === CSV_TYPE.ORDER) {
            searchPirority();
          } else if (mode === CSV_TYPE.RSVN) {
            updateReservationPriority();
          }
        }

        function searchPirority() {
          ctrl.isTableReady = false;
          ctrl.tableParam = nvTable.createTable(COLUMNS);

          const payload = {
            transaction_ids: _.map(ctrl.preProcessData, data => data.transactionId),
          };
          PriorityLevel.search(payload).then(onSuccess, onError);

          function onSuccess(responses) {
            const data = extendResponse(responses);

            if (data && data.length === 0) {
              nvToast.error('No Result Found, Please check CSV');
            }

            ctrl.tableParam.setData(data);
            ctrl.isTableReady = true;
            setBackdrop(false);
          }

          function onError() {
            setBackdrop(false);
          }
        }

        function updateReservationPriority() {
          const selectedData = ctrl.preProcessData;
          const selections = _(selectedData)
            .map(row => _.pick(row, ['id', 'priorityLevel']))
            .value();

          const localParameter = {
            reservations: selections,
          };

          nvDialog.showSingle($event, {
            templateUrl: 'views/container/shipper-pickups/dialogs/bulk-priority-edit/bulk-priority-edit.dialog.html',
            theme: 'nvBlue',
            cssClass: 'shipper-pickups-bulk-priority-edit-dialog',
            controller: 'BulkPriorityEditDialogController',
            controllerAs: 'ctrl',
            locals: {
              localParameter: localParameter,
            },
          }).then(onSet);

          function onSet(data) {
            if (data !== -1) {
              nvToast.success(
                nvTranslate.instant('container.update-priority-level.n-reservations-of-n-updated-successfully', { success: _.size(data), total: _.size(selections) }));  
            }
            setBackdrop(false);
          }
        }
      }
    }

    function downloadSampleCsv(type) {
      if (type === CSV_TYPE.ORDER) {
        nvCsvParser.unparse(SAMPLE_CSV).then(
          data => (nvFileUtils.downloadCsv(data, 'priority_sample_orders.csv')
        ));
      } else if (type === CSV_TYPE.RSVN) {
        nvCsvParser.unparse(SAMPLE_CSV_RSVN).then(
          data => (nvFileUtils.downloadCsv(data, 'priority_sample_reservations.csv')
        ));
      }
    }

    function isDataReady() {
      if (ctrl.preProcessData) {
        return ctrl.preProcessData.length > 0;
      }
      return false;
    }

    function extendResponse(responses) {
      const offlineData = _.keyBy(ctrl.preProcessData, 'transactionId');
      return _.map(responses, response =>
        _.assign(response, {
          transactionId: response.transaction_id,
          oldPriorityLevel: response.current_priority_level,
          priorityLevel: offlineData[response.transaction_id].priorityLevel,
        }
      ));
    }

    function editPriorityLevel($event, data) {
      nvDialog.showSingle($event, {
        templateUrl: 'views/container/priority-levels/dialog/priority-level-edit.dialog.html',
        cssClass: 'priority-level-edit-dialog',
        controller: 'PriorityLevelEditDialogController',
        controllerAs: 'ctrl',
        locals: {
          priorityLevelLocal: data.priorityLevel,
        },
      }).then((result => (data.priorityLevel = result)));
    }

    function isTableUpdateDisabled() {
      return !(ctrl.isTableReady && ctrl.tableParam && (ctrl.tableParam.getLength() > 0));
    }

    function bulkUpdate() {
      ctrl.processAllState = API_STATE.WAITING;
      const payload = _.map(ctrl.tableParam.getTableData(), data => ({
        transaction_id: data.transactionId,
        priority_level: data.priorityLevel,
      }));
      PriorityLevel.bulkUpdate(payload).then(onSuccess, onError);

      function onSuccess() {
        nvToast.success(nvTranslate.instant('container.update-priority-level.update-by-csv-success'));
        ctrl.processAllState = API_STATE.IDLE;
      }

      function onError() {
        ctrl.processAllState = API_STATE.IDLE;
      }
    }

    function setBackdrop(b) {
      ctrl.stateBackdrop = b;
    }

    function cancelCsv() {
      ctrl.isTableReady = false;
    }
  }
}());
