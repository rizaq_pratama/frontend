(function controller() {
  angular
    .module('nvOperatorApp.controllers')
    .controller('PriorityLevelEditDialogController', PriorityLevelEditDialogController);

  PriorityLevelEditDialogController.$inject = ['priorityLevelLocal', '$mdDialog'];

  function PriorityLevelEditDialogController(priorityLevelLocal, $mdDialog) {
    const ctrl = this;

    ctrl.priorityLevel = _.clone(priorityLevelLocal);

    ctrl.onSave = onSave;
    ctrl.onCancel = onCancel;

    function onSave() {
      $mdDialog.hide(ctrl.priorityLevel);
    }

    function onCancel() {
      $mdDialog.cancel();
    }
  }
}());
