(function UserDialogEditController() {
  angular
        .module('nvOperatorApp.controllers')
        .controller('UserDialogEditController', controller);
  controller.$inject = [
    '$q',
    'OauthClient',
    '$mdDialog',
    'fields',
    'userObj',
    'OauthGroup',
    'nvAutocomplete.Data',
    'serviceIds',
    '$translate',
  ];

  function controller(
        $q,
        OauthClient,
        $mdDialog,
        fields,
        userObj,
        OauthGroup,
        nvAutocompleteData,
        serviceIds,
        $translate
    ) {
    const ctrl = this;
    ctrl.id = userObj.id;
    ctrl.serviceIds = serviceIds;
    ctrl.selectedUser = {};
    ctrl.countryRoles = [];
    ctrl.ownerInfo = [];
    ctrl.onCancel = onCancel;
    ctrl.updateOne = updateOne;
    ctrl.forceLogout = forceLogout;
    ctrl.changeStatus = changeStatus;
    ctrl.onChangeCountry = onChangeCountry;
    ctrl.removeGroups = [];
    ctrl.addGroups = [];
    ctrl.roleTableData = [];
    ctrl.countryRoleMap = {};
    ctrl.allRoles = [];
    ctrl.fields = fields;
    ctrl.roleContainerLabel = 'Role(s)';
    ctrl.table = {
      role: $translate.instant('container.user-management.dialogs.role'),
      country: $translate.instant('commons.country'),
      appId: $translate.instant('container.user-management.dialogs.appId'),
      appCode: $translate.instant('container.user-management.dialogs.appCode'),
      serialNo: $translate.instant('commons.sn'),
    };
    ctrl.onLoadPage = onLoadPage;
    ctrl.countryGroupsTableParams = null;
    ctrl.removeGroup = removeGroup;
    ctrl.onRolesSearch = onRolesSearch;
    ctrl.selectedCountry = ctrl.fields.country.options[0];
    ctrl.roleSearch = nvAutocompleteData.getByHandle('user-edit-roles');
    ctrl.roleSearch.onSelect(addRole);

    function onLoadPage() {
      $q.all(
          [OauthClient.readOneById(ctrl.id),
          OauthGroup.readAll()]).then(onSuccess);
      function onSuccess(results) {
        ctrl.selectedUser = results[0].oauthClient;
        ctrl.countryRoles = results[0].countryGroups;
        ctrl.ownerInfo = results[0].oauthClient.ownerInfo;
        ctrl.roleTableData = ctrl.countryRoles;
        _.each(ctrl.serviceIds, (country) => {
          const filtered = _.filter(ctrl.countryRoles, (role) => {
            return role.country === country;
          });
          ctrl.countryRoleMap[country] = filtered;
        });

        ctrl.allRoles = _.sortBy(results[1], 'id');
        // trigger role search refresh on initial
        onChangeCountry(_.first(ctrl.serviceIds));
      }
    }

    function onCancel() {
      $mdDialog.cancel();
    }

    function changeStatus() {
      const data = {};
      data.oauthClientId = ctrl.id;
      if (ctrl.selectedUser.active === undefined || ctrl.selectedUser.active === false) {
        data.action = 'activate';
        return OauthClient.activateUser(data).then(successNotCloseDialog, failed);
      } else if (ctrl.selectedUser.active === true) {
        data.action = 'de-activate';
        return OauthClient.deactivateUser(data).then(successNotCloseDialog, failed);
      }
      data.action = 'activate';
      return OauthClient.activateUser(data).then(successNotCloseDialog, failed);
    }

    function forceLogout() {
      const data = {};
      data.oauthClientId = ctrl.id;
      return OauthClient.forceLogout(data).then(successNotCloseDialog, failed);
    }

    function onChangeCountry(country) {
      const roleSelectedForThatCountry = ctrl.countryRoleMap[country];
      const selectedOptions = convertToOptions(roleSelectedForThatCountry);
      ctrl.roleSearch.setSelectedOptions(selectedOptions);
      ctrl.roleSearch.setPossibleOptions(_.difference(ctrl.allRoles, selectedOptions));
    }

    function updateOne() {
      const data = {};
      data.id = ctrl.id;
      data.firstName = ctrl.selectedUser.firstName;
      data.lastName = ctrl.selectedUser.lastName;
      data.addGroups = ctrl.addGroups;
      data.removeGroups = ctrl.removeGroups;
      return OauthClient.updateOne(data).then(success, failed);
    }

    function convertToOptions(selectedRoles){
      const options = _.filter(ctrl.allRoles, r => (_.find(selectedRoles, { groupName: r.name })));
      return options;
    }

    function success(result) {
      $mdDialog.hide();
      ctrl.selectedUser = result;
    }

    function successNotCloseDialog(result) {
      ctrl.selectedUser = result;
      ctrl.errorMessage = result.errorMessage;
    }

    function failed(error) {
      if (error) {
        ctrl.errorMessage = error.data.errorMessage;
      } else {
        ctrl.errorMessage = 'Unknown error on Edit User';
      }
    }

    function addGroup(countryGroup) {
      ctrl.addGroups.push(countryGroup);
    }

    function removeGroup(role) {
      const roleToPutBack = _.filter(ctrl.roleSearch.getSelectedOptions(), { 'name': role.groupName });
      if (roleToPutBack && roleToPutBack.length > 0) {
        ctrl.roleSearch.remove(roleToPutBack[0]);
      }
      //remove from map
      _.pull(ctrl.countryRoleMap[role.country], role);
      _.pull(ctrl.countryRoles, role);
      ctrl.removeGroups.push(role);
    }

    function addRole(item) {
      if (!_.isUndefined(item)) {
        let country;
        if (ctrl.selectedCountry !== undefined) {
          country = ctrl.selectedCountry;
        }
        const countryGroup = {};
        countryGroup.country = country;
        countryGroup.groupName = item.name;
        ctrl.roleTableData.push(countryGroup);
        addGroup(countryGroup);
        ctrl.countryRoleMap[country].push(countryGroup);
        ctrl.roleContainerLabel = ctrl.roleTableData.length.toString().concat('Roles(s)');
      }
    }

    function onRolesSearch(roleSearchText) {
      if (roleSearchText !== undefined && roleSearchText !== '') {
        ctrl.roleTableData = _.filter(ctrl.countryRoles, o =>
              _.includes(o.groupName.toLowerCase(), roleSearchText.trim().toLowerCase())
      );
      } else {
        ctrl.roleTableData = ctrl.countryRoles;
      }
    }
  }
}());
