(function UserDialogAddController() {
  angular
        .module('nvOperatorApp.controllers')
        .controller('UserDialogAddController', controller);

  controller.$inject = [
    'OauthClient',
    'OauthGroup',
    '$mdDialog',
    'fields',
    'nvAutocomplete.Data',
    'serviceIds',
    '$translate',
  ];

  function controller(
        OauthClient,
        OauthGroup,
        $mdDialog,
        fields,
        nvAutocompleteData,
        serviceIds,
        $translate
    ) {
    const ctrl = this;
    ctrl.fields = fields;
    ctrl.serviceIds = serviceIds;
    ctrl.errorMessage = '';
    ctrl.allUserGroups = [];
    ctrl.newUser = {};
    ctrl.searchText = '';
    ctrl.keyword = '';
    ctrl.country = '';
    ctrl.table = {
      role: $translate.instant('container.user-management.dialogs.role'),
      country: $translate.instant('commons.country'),
      appId: $translate.instant('container.user-management.dialogs.appId'),
      appCode: $translate.instant('container.user-management.dialogs.appCode'),
      serialNo: $translate.instant('commons.sn'),
    };
    ctrl.selectedCountry = ctrl.fields.country.options[0];
    ctrl.roleTableData = [];
        // functions
    ctrl.createOne = createOne;
    ctrl.onLoadPage = onLoadPage;
    ctrl.onCancel = onCancel;
    ctrl.removeRole = removeRole;
    ctrl.roleSearch = nvAutocompleteData.getByHandle('user-add-roles');
    ctrl.roleSearch.onSelect(addRoles);
    ctrl.onRolesSearch = onRolesSearch;
    ctrl.roles = [];

    function onLoadPage() {
      return OauthGroup.readAll().then(oauthGroupReadAllSuccess);

      function oauthGroupReadAllSuccess(body) {
        ctrl.allUserGroups = _.sortBy(body, 'id');
        ctrl.roleSearch.setPossibleOptions(ctrl.allUserGroups);
      }
    }

    function createOne() {
      const data = {};
      data.firstName = ctrl.newUser.firstName;
      data.lastName = ctrl.newUser.lastName;
      data.email = ctrl.newUser.email;
      data.grantType = ctrl.newUser.grantType;
      data.oauthClientGroups = ctrl.roles;
      return OauthClient.createOne(data).then(success, failed);
    }

    function success(result) {
      $mdDialog.hide();
      ctrl.selectedUser = result;
    }

    function failed(error) {
      if (error) {
        ctrl.errorMessage = error.errorMessage;
      } else {
        ctrl.errorMessage = 'Unknown error on Add User';
      }
    }

    function addRoles(item) {
      if (!_.isUndefined(item)) {
        let country;
        if (ctrl.selectedCountry !== undefined) {
          country = ctrl.selectedCountry;
        }
        const countryGroup = {};
        countryGroup.country = country;
        countryGroup.groupName = item.name;
        ctrl.roles.push(countryGroup);
      }
    }

    function removeRole(role) {
      const roleToPutBack = _.filter(ctrl.roleSearch.getSelectedOptions(), { 'name': role.groupName });
      if (roleToPutBack && roleToPutBack.length > 0) {
        ctrl.roleSearch.remove(roleToPutBack[0]);
      }
      _.pull(ctrl.roles, role);
    }

    function onCancel() {
      $mdDialog.cancel();
    }

    function onRolesSearch(roleSearchText) {
      if (roleSearchText !== undefined && roleSearchText !== '') {
        ctrl.roleTableData = _.filter(ctrl.roles, o =>
           _.includes(o.groupName.toLowerCase(), roleSearchText.trim().toLowerCase())
        );
      } else {
        ctrl.roleTableData = ctrl.roles;
      }
    }
  }
}());
