(function UserManagementController() {
  angular
    .module('nvOperatorApp.controllers')
    .controller('UserManagementController', controller);

  controller.$inject = ['OauthClient', 'OauthGroup', '$mdDialog', 'NgTableParams', 'ngTableDefaults',
    'nvToast', 'nvTranslate'];

  function controller(OauthClient, OauthGroup, $mdDialog, NgTableParams, ngTableDefaults,
    nvToast, nvTranslate) {
    const ctrl = this;
    ngTableDefaults.settings.counts = [];

        // fields
    ctrl.allOauthClients = [];
    ctrl.allUserGroups = [];
    ctrl.allOauthClientParams = null;
    ctrl.grantTypeFilter = [];
    ctrl.searchText = '';
    ctrl.keyword = '';
    ctrl.country = '';
        // function
    ctrl.load = load;
    ctrl.getAllOauthClients = getAllOauthClients;
    ctrl.getOauthClientsByfilter = getOauthClientsByfilter;
    ctrl.updateOne = updateOne;
    ctrl.createOne = createOne;
    ctrl.resendInvitation = resendInvitation;
    ctrl.getAllUserGroups = getAllUserGroups;
    ctrl.userGroupParams = [];
    ctrl.serviceIds = [];
    ctrl.filters = {
      grantTypes: {
        searchText: '',
        possibleOptions: [
                    { id: 1, displayName: 'Google', value: 'GOOGLE_SSO', lowercaseName: 'google' },
                    { id: 2, displayName: 'Password', value: 'PASSWORD', lowercaseName: 'password' },
                    { id: 3, displayName: 'Client Credentials', value: 'CLIENT_CREDENTIALS', lowercaseName: 'client credentials' },
        ],
        selectedOptions: [],
        onChangeFn: onFilterGrantTypeChangeFn,
        filterFn: grantTypeFilterFn,
      },
      statuses: {
        searchText: '',
        possibleOptions: [],
        selectedOptions: [],
        onChangeFn: onFilterGrantTypeChangeFn,
        filterFn: grantTypeFilterFn,
      },
    };

    ctrl.grantTypeOptions = [
      { id: 1, displayName: 'Google', value: 'GOOGLE_SSO', lowercaseName: 'google' },
      { id: 2, displayName: 'Password', value: 'PASSWORD', lowercaseName: 'password' },
      { id: 3, displayName: 'Client Credentials', value: 'CLIENT_CREDENTIALS', lowercaseName: 'client credentials' },
    ];

    ctrl.statusOptions = [
      { id: 1, displayName: 'Provisioned', value: 'PROVISIONED', lowercaseName: 'provisioned' },
      { id: 2, displayName: 'Linked', value: 'LINKED', lowercaseName: 'linked' },
      { id: 3, displayName: 'Registered', value: 'REGISTERED', lowercaseName: 'registered' },
      { id: 4, displayName: 'Invited', value: 'INVITED', lowercaseName: 'invited' },
      { id: 5, displayName: 'Revoked', value: 'REVOKED', lowercaseName: 'revoked' },
    ];

    function getOauthClientsByfilter() {
      const grantTypes = _.map(ctrl.filters.grantTypes.selectedOptions, 'value');
      const statuses = _.map(ctrl.filters.statuses.selectedOptions, 'value');
      const keyword = ctrl.keyword;

      ctrl.allOauthClientParams = new NgTableParams(
        { page: 1, count: 20 },
        { getData: getData }
      );

      function getData(params) {
        const page = params.page();
        const count = params.count();

        return OauthClient
          .readAllByFilter(page, count, keyword, grantTypes, statuses)
          .then((body) => {
            ctrl.allOauthClientParams.total(body.count);
            return body.oauthClients;
          }
        );
      }
    }

    function load() {
      ctrl.filters.grantTypes.possibleOptions = ctrl.grantTypeOptions;
      ctrl.filters.statuses.possibleOptions = ctrl.statusOptions;
      return OauthGroup.getServiceIds().then((result) => {
        ctrl.serviceIds = _.map(result, country => (_.toLower(country)));
      });
    }

    function getAllOauthClients() {
      return OauthClient.readAll().then((body) => {
        ctrl.allOauthClients = _.sortBy(body, 'id');
        ctrl.allOauthClientParams = new NgTableParams(
                    { count: 20 },
                    { counts: [],
                        dataset: ctrl.allOauthClients,
                    });
        ctrl.allOauthClientParams.reload();
      });
    }

    function createOne($event) {
      const fields = OauthClient.getAddFields();

      $mdDialog.show({
        templateUrl: 'views/container/user-management/dialog/user-add/user-add.dialog.html',
        locals: {
          fields: fields,
          serviceIds: ctrl.serviceIds,
        },
        bindToController: true,
        controllerAs: 'ctrl',
        controller: 'UserDialogAddController',
        targetEvent: $event,
      });
    }

    function updateOne($event, obj) {
      const fields = OauthClient.getAddFields();

      $mdDialog.show({
        templateUrl: 'views/container/user-management/dialog/user-edit/user-edit.dialog.html',
        locals: {
          fields: fields,
          userObj: obj,
          serviceIds: ctrl.serviceIds,
        },
        bindToController: true,
        controllerAs: 'ctrl',
        controller: 'UserDialogEditController',
        targetEvent: $event,
      });
    }

    function getAllUserGroups() {
      return OauthGroup.readAll().then(success);

      function success(body) {
        ctrl.allUserGroups = _.sortBy(body, 'id');
      }
    }

    function grantTypeFilterFn(selectedOptions, grantType) {
      return _.some(selectedOptions, _.partial((value, selectedOption) =>
         value === selectedOption.value
      , grantType.value));
    }

    function onFilterGrantTypeChangeFn(searchText) {
      if (searchText) {
        ctrl.grantTypeOptions = _.filter(ctrl.grantTypeOptions, o =>
        _.includes(o.displayName, searchText.trim().toLowerCase())
        );
      }
      return ctrl.grantTypeOptions;
    }

    function resendInvitation(user) {
      const { id, emailId } = user;
      const payload = { oauthClientId: id };

      OauthClient.resendInvitations(payload)
        .then((response) => {
          if (isInvitationSent(response)) {
            const { inviteCode: { email } } = response;
            nvToast.success(nvTranslate.instant('container.user-management.resend-invitation-success', { email: email }));
          } else {
            nvToast.error(nvTranslate.instant('container.user-management.resend-invitation-already-registered', { email: emailId }));
          }
        });

      function isInvitationSent(theResponse) {
        const { inviteCode } = theResponse;
        return inviteCode != null;
      }
    }
  }
}());
