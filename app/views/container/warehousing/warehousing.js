(function controller() {
  angular
    .module('nvOperatorApp.controllers')
    .controller('WarehousingController', WarehousingController);

  WarehousingController.$inject = [
    'Warehousing',
    'nvTable',
    'nvToast',
    'nvTranslate',
    '$q',
  ];
  function WarehousingController(Warehousing, nvTable,
    nvToast, nvTranslate, $q) {
    const ctrl = this;

    ctrl.uploadInboundData = uploadInboundData;
    ctrl.uploadParcelSortData = uploadParcelSortData;
    ctrl.loadParcelSortMappingData = loadParcelSortMappingData;
    ctrl.loadConfig = loadConfig;
    ctrl.setActive = setActive;
    ctrl.publishMapping = publishMapping;
    ctrl.selectZoneChunk = selectZoneChunk;

    ctrl.ipAddress = '';
    ctrl.tableParam = null;
    ctrl.tableParam2 = null;
    ctrl.loadConfigState = 'idle';
    ctrl.loadMappingDataState = 'idle';
    ctrl.uploadInboundState = 'idle';
    ctrl.uploadParcelState = 'idle';
    ctrl.lastSelectedRow = -1;

    ctrl.parcelSortData = null;
    ctrl.zones = [];
    ctrl.zone = null;
    ctrl.armSize = 16;

    ctrl.belt = 'NV_S_1';
    ctrl.belts = Warehousing.BELT_OPTIONS;

    function uploadInboundData() {
      if (!checkIpAddress()) {
        return;
      }
      ctrl.uploadInboundState = 'waiting';
      Warehousing.getInboundData().then(data =>
        Warehousing.uploadToGreyOrange(ctrl.ipAddress, data)
      ).then(() => {
        ctrl.uploadInboundState = 'idle';
        nvToast.success(nvTranslate.instant('container.warehousing.upload-inbound-data-success'));
      }, () => {
        ctrl.uploadInboundState = 'idle';
      });
    }

    function uploadParcelSortData() {
      if (!checkIpAddress()) {
        return;
      }
      ctrl.uploadParcelState = 'waiting';
      Warehousing.getParcelSortData().then(data =>
        Warehousing.uploadToGreyOrange(ctrl.ipAddress, data)
      ).then(() => {
        ctrl.uploadParcelState = 'idle';
        nvToast.success(nvTranslate.instant('container.warehousing.upload-parcel-data-success'));
      }, () => {
        ctrl.uploadParcelState = 'idle';
      });
    }

    function loadParcelSortMappingData() {
      ctrl.tableParam = null;
      ctrl.tableParam2 = null;
      ctrl.parcelSortData = null;
      ctrl.loadMappingDataState = 'waiting';
      return Warehousing.getParcelSortData().then((data) => {
        ctrl.loadMappingDataState = 'idle';
        ctrl.parcelSortData = data;
        ctrl.zones = _(data)
          .groupBy('zone')
          .map((d, zone) => ({ value: zone, displayName: `${zone} (${_(d).groupBy('route_id').size()})` }))
          .sortBy('zone')
          .value();
      }, () => {
        ctrl.loadMappingDataState = 'idle';
      });
    }

    function selectZoneChunk() {
      const { zone, armSize, parcelSortData: data } = ctrl;
      const oldRouteAwbs = _.filter(data, d => d.route_id === 'OLDROUTE');
      const unroutedAwbs = _.groupBy(_.filter(data, d => d.route_id === 'UNROUTED'), awb => awb.zone);
      const awbs = _.filter(data, d => d.route_id && d.route_id !== 'OLDROUTE' && d.route_id !== 'UNROUTED' && _.find(zone, z => z === d.zone));
      const routes = _.sortBy(_.map(_.groupBy(awbs, awb => awb.route_id),
        (awb, routeId) => ({
          routeId: routeId,
          size: awb.length,
          zone: _(awb).map('zone').uniq().value(),
        })), route => route.size * -1);

      const chunkSize = armSize - 2 - zone.length;
      const chunks = _.chunk(routes, chunkSize);

      const d = _.map(chunks, (chunk, idx) => {
        const routeChunks = _.map(chunk, c => [{
          routeId: c.routeId,
          size: c.size,
          zone: c.zone,
        }]);

        const partRoutes = _.map(routes.slice((idx + 1) * chunkSize, routes.length), c => ({
          routeId: c.routeId,
          size: c.size,
          zone: c.zone,
        }));
        if (!_.isEmpty(partRoutes)) {
          routeChunks.push(partRoutes);
        }

        const arms = _.map(routeChunks, (routeChunk, i) => ({
          arm: i + 2,
          routes: routeChunk,
        }));

        const rejectArm = 1;
        arms.push({
          arm: rejectArm,
          description: 'REJECT',
          routes: [{
            routeId: 'OLDROUTE',
            size: oldRouteAwbs.length,
            zone: zone,
          }],
        });

        const returnArms = _.range(arms.length + 1, arms.length + zone.length + 1);

        _.each(returnArms, (returnArm, rejectArmIdx) => {
          const zoneId = zone[rejectArmIdx];
          arms.push({
            arm: returnArm,
            description: 'RETURN',
            routes: [{
              routeId: 'UNROUTED',
              size: unroutedAwbs[zoneId].length,
              zone: [zoneId],
            }],
          });
        });

        const request = _.flatten(_.filter(_.map(routeChunks, (routeChunk, i) =>
          _.map(routeChunk, route => ({
            title: route.routeId,
            destination: route.routeId,
            factors: _.map(route.zone, z => ({
              zone: z,
              route_id: route.routeId,
            })),
            devices: [
              (i + 2).toString(),
            ],
            client_data: null,
          })
          )
        ), r => !_.isEmpty(r)));

        _.forEach(zone, (z) => {
          request.push({
            client_data: null,
            destination: `Zone ${z}`,
            devices: [
              rejectArm,
            ],
            factors: [
              {
                route_id: 'OLDROUTE',
                zone: z,
              },
            ],
            title: `Zone ${z}`,
          });
        });

        _.forEach(returnArms, (returnArm, returnArmIdx) => {
          const zoneId = zone[returnArmIdx];
          request.push({
            client_data: null,
            destination: `Zone ${zoneId}`,
            devices: [
              returnArm,
            ],
            factors: [
              {
                route_id: 'UNROUTED',
                zone: zoneId,
              },
            ],
            title: `Zone ${zoneId}`,
          });
        });

        const displayRoutes = _(arms)
          .map(x => x.routes || [{ routeId: '-', size: '-', zone: ['-'] }])
          .value();

        return {
          request: request,
          arms: _(arms).map('arm').value(),
          routes: _(displayRoutes).map(x => _(x).map('routeId').join(', ')).value(),
          zones: _(displayRoutes).map(x => _(x).flatMap('zone').uniq().join(', ')).value(),
          sizes: _(displayRoutes).map(x => _(x).map('size').sum()).value(),
          descriptions: _(arms).map(x => x.description || '-').value(),
        };
      });

      ctrl.tableParam = null;
      ctrl.tableParam2 = nvTable.createTable(Warehousing.ZONE_CHUNK_FIELDS);
      ctrl.tableParam2.setData(d);
      ctrl.lastSelectedRow = -1;
    }

    function loadConfig() {
      if (!checkIpAddress()) {
        return null;
      }
      ctrl.tableParam = null;
      ctrl.loadConfigState = 'waiting';
      return Warehousing.getConfig(ctrl.ipAddress, ctrl.belt).then((data) => {
        ctrl.tableParam2 = null;
        ctrl.tableParam = nvTable.createTable(Warehousing.FIELDS);
        ctrl.tableParam.addColumn('set-active', { displayName: 'container.warehousing.set-active' });
        ctrl.tableParam.setData(data);
        ctrl.loadConfigState = 'idle';
      }, () => {
        ctrl.loadConfigState = 'idle';
      });
    }

    function setActive(id) {
      if (!checkIpAddress()) {
        return;
      }
      Warehousing.updateConfig(ctrl.ipAddress, ctrl.belt, id, { active: true })
      .then(() => {
        nvToast.success(nvTranslate.instant('container.warehousing.set-active-message', { id: id }));
      })
      .then(loadConfig);
    }

    function publishMapping(requestBody, row) {
      if (!checkIpAddress()) {
        return;
      }
      Warehousing.getConfig(ctrl.ipAddress, ctrl.belt)
      .then((data) => {
        const conf = _.find(data, config => config.title === 'route');
        if (!conf) {
          return $q.reject(nvTranslate.instant('container.warehousing.no-config-named-route'));
        }
        return Warehousing.updateRouteMapping(ctrl.ipAddress, ctrl.belt, conf.id, requestBody);
      })
      .then(() => {
        nvToast.success(nvTranslate.instant('container.warehousing.update-mapping-success'));
        ctrl.lastSelectedRow = row;
      })
      .catch((error) => {
        nvToast.error(error);
      });
    }

    function checkIpAddress() {
      if (!ctrl.ipAddress) {
        nvToast.error(nvTranslate.instant('container.warehousing.no-ip-address'));
        return false;
      }
      return true;
    }
  }
}());
