(function controller() {
  angular
          .module('nvOperatorApp.controllers')
          .controller('AmazonParcelStatusController', AmazonParcelStatusController);
  AmazonParcelStatusController.$inject = [
    'nvFileSelectDialog',
    'nvTranslate',
    'nvButtonFilePickerType',
    'Order',
  ];

  function AmazonParcelStatusController(nvFileSelectDialog,
    nvTranslate, nvButtonFilePickerType, Order) {
    const ctrl = this;
    ctrl.openUploadDialog = openUploadDialog;
    ctrl.success = [];
    ctrl.errors = [];


    function openUploadDialog($event) {
      nvFileSelectDialog.show($event, {
        title: nvTranslate.instant('container.bulk-address-verification.upload-address-csv'),
        fileSelect: {
          accept: nvButtonFilePickerType.EXCEL,
          maxFileCount: 1,
        },
        buttonOkTitle: nvTranslate.instant('commons.submit'),
        onFinishSelect: onFinishSelect,
        onDone: angular.noop,
      });
    }

    function onFinishSelect(files) {
      ctrl.pageState = 'waiting';
      ctrl.success.length = 0;
      ctrl.errors.length = 0;
      return Order.uploadAmazonOrderStatus(files[0])
                  .then(onSuccess);

      function onSuccess(result) {
        ctrl.pageState = 'idle';
        ctrl.errors = _.filter(result, r => (!r.success));
        ctrl.success = _.filter(result, r => (r.success));
      }
    }
  }
}());
