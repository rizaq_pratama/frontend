(function AddressingDialog() {
  angular.module('nvOperatorApp.controllers')
        .controller('AddressingDialogController', AddressingDialogController);

  AddressingDialogController.$inject = ['Addressing', '$q', '$mdDialog', '$timeout',
        'nvToast', 'nvTranslate', 'address', 'mode', 'country', '$scope'];

  function AddressingDialogController(Addressing, $q, $mdDialog, $timeout,
        nvToast, nvTranslate, address, mode, country, $scope) {
    const ctrl = this;
    ctrl.address = {};
    ctrl.state = 'idle';
    ctrl.deleteState = 'idle';
    ctrl.countryFields = {};

    ctrl.addressSource = [
      {
        value: 'MANUAL',
        displayName: nvTranslate.instant('container.addressing.manual'),
      },
    ];
    ctrl.addressSourceEnabled = true;
    if (address) {
      ctrl.address = _.omit(_.cloneDeep(address), ['customFields']);
      ctrl.addressSource = [
        {
          value: 'MANUAL',
          displayName: nvTranslate.instant('container.addressing.manual'),
        },
        {
          value: 'SINGPOST',
          displayName: nvTranslate.instant('container.addressing.singpost'),
        },
        {
          value: 'GOOGLE',
          displayName: nvTranslate.instant('container.addressing.google'),
        },
        {
          value: 'ONEMAP',
          displayName: nvTranslate.instant('container.addressing.onemap'),
        },
      ];
      ctrl.addressSourceEnabled = false;
    }
    ctrl.mode = mode;
    if (country) {
      ctrl.country = country.value;
    }
    ctrl.countries = angular.copy(Addressing.getCountries());
    ctrl.addressTypes = [
      {
        value: 'A',
        displayName: nvTranslate.instant('container.addressing.apartment'),
      },
      {
        value: 'C',
        displayName: nvTranslate.instant('container.addressing.condominium'),
      },
      {
        value: 'H',
        displayName: nvTranslate.instant('container.addressing.hdb'),
      },
      {
        value: 'K',
        displayName: nvTranslate.instant('container.addressing.block'),
      },
      {
        value: 'S',
        displayName: nvTranslate.instant('container.addressing.standard'),
      },
      {
        value: 'U',
        displayName: nvTranslate.instant('container.addressing.unknown'),
      },
    ];

        // ////////////
        // Functions
        // ////////////
    ctrl.onSave = onSave;
    ctrl.onUpdate = onUpdate;
    ctrl.onDelete = onDelete;
    ctrl.onCancel = onCancel;
    ctrl.onSuccessCopy = onSuccessCopy;
    ctrl.showField = showField;


        // /////////////
        // watcher
        // ////////////

    $scope.$watch('ctrl.country', (newVal) => {
      onChangeCountry(newVal);
      // set city to singapore if country == sg
      if (newVal === 'sg') {
        ctrl.address.city = 'Singapore';
      } else {
        ctrl.address.city = null;
      }
    });


        // /////////////
        // Functions Body
        // ////////////

    function onCancel() {
      return $mdDialog.cancel();
    }

    function onSuccessCopy() {
      ctrl.copied = true;
      return $timeout(() => {
        ctrl.copied = false;
      }, 2000);
    }
    function onSave() {
            // append the country
      ctrl.state = 'waiting';
      ctrl.address.country = ctrl.country;
      return Addressing.createAddress(ctrl.address)
                .then(onSuccessCreate);
      function onSuccessCreate() {
        ctrl.state = 'idle';
        nvToast.success(nvTranslate.instant('container.addressing.success-create-address'));
        $mdDialog.hide(ctrl.address);
      }
    }

    function onChangeCountry(newCountry) {
      ctrl.countryFields = {};
      _.each(Addressing.getAddressFieldNamebyCountry(newCountry), (val) => {
        ctrl.countryFields[val] = true;
      });
    }

    function showField(fieldName) {
      return ctrl.countryFields[fieldName];
    }

    function onUpdate() {
      ctrl.state = 'waiting';
      return Addressing.updateAddress(ctrl.address, ctrl.address.id)
                .then(onSuccessUpdate);
      function onSuccessUpdate(result) {
        ctrl.state = 'idle';
        nvToast.success(nvTranslate.instant('container.addressing.success-update-address'));
        if (result) {
          $mdDialog.hide(ctrl.address);
        }
      }
    }

    function onDelete() {
      ctrl.deleteState = 'waiting';
      return Addressing.deleteAddress(ctrl.country, ctrl.address.id)
                .then(successDelete);

      function successDelete(result) {
        ctrl.deleteState = 'idle';
        nvToast.success(nvTranslate.instant('container.addressing.success-delete-address'));
        if (result) {
          $mdDialog.hide(ctrl.address);
        }
      }
    }
  }
}());
