(function controller() {
  angular.module('nvOperatorApp.controllers')
        .controller('AddressingController', AddressingController);

  AddressingController.$inject = ['Addressing', '$q', 'nvDialog',
     'nvToast', '$timeout', 'nvMaps', 'nvDomain', 'nvUtils'];
  function AddressingController(Addressing, $q, nvDialog,
    nvToast, $timeout, nvMaps, nvDomain, nvUtils) {
    const ctrl = this;
    ctrl.selectedAddress = null;
    ctrl.onLoad = onLoad;
    ctrl.addrresses = [];

    ctrl.country = null;
    ctrl.map = null;
    ctrl.marker = null;
    ctrl.searchQuery = '';

        // ///////////////////
        // FUNCTIONS
        // //////////////////
    ctrl.searchAddress = searchAddress;
    ctrl.showDetail = showDetail;
    ctrl.deleteAddress = deleteAddress;
    ctrl.onSuccessCopy = onSuccessCopy;
    ctrl.updateAddress = updateAddress;
    ctrl.createAddress = createAddress;

    onLoad();
    function showDetail(address) {
      if (address.source !== 'DEFAULT') {
        ctrl.selectedAddress = address;
        $timeout(() => (addLocation(address.latitude, address.longitude)), 500);
      } else {
        ctrl.selectedAddress = null;
      }
    }

    function addLocation(lat, lng) {
      if (!ctrl.map) {
        ctrl.map = nvMaps.initialize('address-map');
      }
      if (ctrl.marker) {
        nvMaps.removeMarker(ctrl.map, ctrl.marker);
      }
      let options = {
        latitude: lat,
        longitude: lng,
        draggable: true,
      };
      const colorConfig = {
        markerColorCode: 'c67b1e',
        size: 'R',
      };

      options = _.merge(options, nvMaps.getIconData(colorConfig));
      ctrl.marker = nvMaps.addMarker(ctrl.map, options);
      nvMaps.flyToLocation(ctrl.map, {
        latitude: lat,
        longitude: lng,
      });
    }

    function onLoad() {
            // get domain
      const domains = nvDomain.getDomain();

      ctrl.countries = _.map(Addressing.getCountries(), (a) => {
        a.value = a.key;
        a.displayName = a.name;
        return a;
      });
      ctrl.country = _.find(ctrl.countries, c => (c.value === domains.current));
    }

    function onSuccessCopy() {
      ctrl.copied = true;
      return $timeout(() => {
        ctrl.copied = false;
      }, 2000);
    }

    function searchAddress() {
      $timeout(() => {
        const query = ctrl.searchQuery;
        ctrl.map = null;
        ctrl.selectedAddress = null;
        if (query) {
          return Addressing.filterSearch(ctrl.country.key, query)
                    .then(success);
        }
        ctrl.addrresses = [];
        return $q.reject();
      }, 0);


      function success(result) {
        ctrl.addrresses = _.map(result, (address) => {
          address.formatted_address = address.formatted_address || createFormattedAddress(address);
          address.customFields = _.omit(address, ['id', 'score', 'source',
                        'jaro_winkler_score', 'latitude', 'longitude', 'formatted_address',
                        'address_type']);
          if (angular.isUndefined(address.raw_address)) {
            return address;
          }
          return null;
        });
      }
    }

        // format <building_no>, <building>, <street>, <city>, <postcode>
    function createFormattedAddress(a) {
      const formatterArray = _.compact([a.building_no, a.building, a.street, a.city, a.postcode]);
      return _.join(formatterArray, ', ');
    }

    function deleteAddress(evt, address) {
      return nvDialog.confirmDelete(evt, {})
                .then(onDelete).then(onSuccessDelete);

      function onDelete() {
        const countryCode = ctrl.country.key;
        return Addressing.deleteAddress(countryCode, address.id);
      }

      function onSuccessDelete() {
        nvToast.success(`Success delete address ${address.id}`);
        ctrl.selectedAddress = null;
        _.remove(ctrl.addrresses, address);
      }
    }

    function updateAddress($event, address) {
      return nvDialog.showSingle($event, {
        templateUrl: 'views/container/addressing/dialogs/edit-address/edit-address.dialog.html',
        locals: {
          address: address,
          mode: 'edit',
          country: ctrl.country,
        },
        theme: 'nvBlue',
        cssClass: 'nv-edit-address-container',
        controllerAs: 'ctrl',
        controller: 'AddressingDialogController',
      }).then(success);
      function success(result) {
        result.formatted_address = createFormattedAddress(result);
        result.customFields = _.omit(result, ['id', 'score', 'source',
                        'jaro_winkler_score', 'latitude', 'longitude', 'formatted_address',
                        'address_type']);
        nvUtils.mergeAllIn(ctrl.addrresses, _.castArray(result), 'id');
      }
    }

    function createAddress($event) {
      return nvDialog.showSingle($event, {
        templateUrl: 'views/container/addressing/dialogs/edit-address/edit-address.dialog.html',
        locals: {
          address: null,
          mode: 'create',
          country: ctrl.country,
        },
        theme: 'nvGreen',
        cssClass: 'nv-edit-address-container',
        controllerAs: 'ctrl',
        controller: 'AddressingDialogController',
      }).then(success);

      function success() {
        searchAddress();
      }
    }
  }
}());
