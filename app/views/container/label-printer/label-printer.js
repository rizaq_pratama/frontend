(function controller() {
  angular.module('nvOperatorApp.controllers')
    .controller('LabelPrinterController', LabelPrinterController);

  LabelPrinterController.$inject = ['Printer', 'nvToast', 'nvTranslate'];
  function LabelPrinterController(Printer, nvToast, nvTranslate) {
    const ctrl = this;
    let printer = null;
    ctrl.trackingId = null;
    ctrl.prefix = null;
    const INVALID_OBJECT = {
      color: 'yellow',
      id: '-',
      shipper_id: '-',
      to_address1: '-',
      to_address2: '-',
      to_contact: '-',
      to_name: '-',
    };
    ctrl.checkTrackingId = checkTrackingId;
    ctrl.scans = [];
    init();

    function init() {
      Printer.searchPrinter().then((result) => {
        printer = result;
      });
    }

    function checkTrackingId(prefix = null, trackingId) {
      ctrl.trackingId = null;
      let nTrackingId;
      if (prefix) {
        nTrackingId = `${prefix}${trackingId}`;
      } else {
        nTrackingId = trackingId;
      }
      // check for duplicate order
      const duplicateIndex = _.findIndex(ctrl.scans, { scan: nTrackingId });
      ctrl.scans.push({
        scan: nTrackingId,
        color: duplicateIndex >= 0 ? '#b378D3' : 'none',
        id: nvTranslate.instant('commons.loading'),
        shipper_id: nvTranslate.instant('commons.loading'),
        to_address1: nvTranslate.instant('commons.loading'),
        to_address2: nvTranslate.instant('commons.loading'),
        to_contact: nvTranslate.instant('commons.loading'),
        to_name: nvTranslate.instant('commons.loading'),
      });
      if (duplicateIndex >= 0) {
        nvToast.warning(nvTranslate.instant('container.label-printer.duplicate-order'));        
      }

      const payload = {
        scan: nTrackingId,
      };
      return Printer.printScannedOrder(payload).then(success, error);

      function success(result) {
        if (result.order) {
          const order = result.order;
          printerByConnectedPrinter(order);
        } else {
          setOrderNotFound(ctrl.scans.length - 1);
        }
      }

      function setOrderNotFound(orderIndex) {
        _.assign(ctrl.scans[orderIndex], INVALID_OBJECT);
      }

      function printerByConnectedPrinter(order) {
        const orderIndex = ctrl.scans.length - 1;
        if (printer.version &&
          _.indexOf([4], parseInt(printer.version, 10)) >= 0) {
          Printer.printByZpl({
            recipient_name: order.to_name,
            address1: order.to_address1,
            address2: order.to_address2,
            contact: order.to_contact,
            country: order.to_country,
            postcode: order.to_postcode,
            shipper_name: order.shipper_name,
            tracking_id: order.tracking_id,
            barcode: order.tracking_id,
            route_id: '',
            driver_name: '',
          }).then(() => {
            _.assign(ctrl.scans[orderIndex], order);
          }, () => {
            setOrderNotFound(orderIndex);
          });
        } else if (printer.version &&
          _.indexOf([2, 3], parseInt(printer.version, 10)) >= 0) {
          const printData = [{
            recipient_name: order.to_name,
            address1: order.to_address1,
            address2: order.to_address2,
            contact: order.to_contact,
            country: order.to_country,
            postcode: order.to_postcode,
            shipper_name: order.shipper_name,
            tracking_id: order.tracking_id,
            barcode: order.tracking_id,
            route_id: '',
            driver_name: '',
            template: 'hub_shipping_label_70X50',
          }];

          Printer.printByPost(printData).then(() => {
            _.assign(ctrl.scans[orderIndex], order);
          }, () => {
            setOrderNotFound(orderIndex);
          });
        } else {
          Printer.printByQueryParam(order);
        }
      }

      function error() {
        _.assign(ctrl.scans[ctrl.scans.length - 1], INVALID_OBJECT);
        return nvToast.error('Unknown tracking id');
      }
    }
  }
}());
