(function controller() {
  angular
    .module('nvOperatorApp.controllers')
    .controller('ShipperBillingDialogController', ShipperBillingDialogController);

  ShipperBillingDialogController.$inject = [
    '$mdDialog', '$scope', 'selectedShipper', 'nvDateTimeUtils',
    'nvURL', '$rootScope', '$window',
  ];

  function ShipperBillingDialogController(
    $mdDialog, $scope, selectedShipper, nvDateTimeUtils,
    nvURL, $rootScope, $window
  ) {
    // variables
    const ctrl = this;
    ctrl.selectedShipper = selectedShipper;
    ctrl.fromDate = new Date();
    ctrl.toDate = new Date();

    // functions
    ctrl.onCancel = onCancel;
    ctrl.isViewBillingButtonDisabled = isViewBillingButtonDisabled;
    ctrl.viewBilling = viewBilling;

    // functions details
    function onCancel() {
      $mdDialog.cancel();
    }

    function isViewBillingButtonDisabled() {
      return ($scope.modelForm && _.size($scope.modelForm.$error)) ||
        ctrl.fromDate > ctrl.toDate;
    }

    function viewBilling() {
      const fromDate = nvDateTimeUtils.displayDateTime(
        nvDateTimeUtils.toMoment(ctrl.fromDate).startOf('day'), 'UTC'
      );

      const toDate = nvDateTimeUtils.displayDateTime(
        nvDateTimeUtils.toMoment(ctrl.toDate).endOf('day'), 'UTC'
      );

      $window.open(
        `${nvURL.buildV1Url(nv.config.env, $rootScope.domain)}` +
        `/billing/orders/${selectedShipper.id}/${encodeURIComponent(fromDate)}/` +
        `${encodeURIComponent(toDate)}`
      );
    }
  }
}());
