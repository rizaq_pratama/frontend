(function controller() {
  angular
    .module('nvOperatorApp.controllers')
    .controller('ShipperErrorsDialogController', ShipperErrorsDialogController);

  ShipperErrorsDialogController.$inject = [
    '$mdDialog', 'payload',
  ];

  function ShipperErrorsDialogController(
    $mdDialog, payload
  ) {
    // variables
    const ctrl = this;
    ctrl.payload = _.defaults(payload, {
      errors: [], // push error message string here if item fail to process
      messages: {
        description: 'container.shippers.failed-to-update-x-items',
      },
    });

    // functions
    ctrl.onCancel = onCancel;

    // functions details
    function onCancel() {
      $mdDialog.hide();
    }
  }
}());
