(function controller() {
  angular
    .module('nvOperatorApp.controllers')
    .controller('ShippersController', ShippersController);

  ShippersController.$inject = [
    '$scope', 'Shippers', 'nvModelUtils', '$timeout', 'Reservation', 'Timewindow',
    'nvTranslate',
  ];

  function ShippersController(
    $scope, Shippers, nvModelUtils, $timeout, Reservation, Timewindow,
    nvTranslate
  ) {
    const RESERVATIONS_DAY = [
      { id: 0, value: 1, displayName: 'commons.sunday' },
      { id: 1, value: 2, displayName: 'commons.monday' },
      { id: 2, value: 3, displayName: 'commons.tuesday' },
      { id: 3, value: 4, displayName: 'commons.wednesday' },
      { id: 4, value: 5, displayName: 'commons.thursday' },
      { id: 5, value: 6, displayName: 'commons.friday' },
      { id: 6, value: 7, displayName: 'commons.saturday' },
    ];

    const ADDRESS_FIELDS = {
      contact: { displayName: 'Contact' },
      address: { displayName: 'Address' },
      actions: { displayName: 'Actions' },
    };

    const SHIPPER_CATEGORY = {
      NORMAL: 0,
      MARKETPLACE: 1,
      MARKETPLACE_SELLER: 2,
    };

    const SHIPPER_TYPE = [
      { id: SHIPPER_CATEGORY.NORMAL, value: { is_marketplace: false, is_marketplace_seller: false, id: SHIPPER_CATEGORY.NORMAL }, displayName: 'Normal' },
      { id: SHIPPER_CATEGORY.MARKETPLACE, value: { is_marketplace: true, is_marketplace_seller: false, id: SHIPPER_CATEGORY.MARKETPLACE }, displayName: 'Marketplace' },
      { id: SHIPPER_CATEGORY.MARKETPLACE_SELLER, value: { is_marketplace: true, is_marketplace_seller: true, id: SHIPPER_CATEGORY.MARKETPLACE_SELLER }, displayName: 'Marketplace Seller' },
    ];

    const FORM_VALIDATION = {
      shipperContact: [{
        key: 'numericOnly',
        msg: 'container.shippers.numeric-only',
      }],
      shipperPrefix: [{
        key: 'available',
        msg: 'container.shippers.prefix-already-used',
      }, {
        key: 'exactly5',
        msg: 'container.shippers.must-enter-exactly-5-chars',
      }, {
        key: '3to5',
        msg: 'container.shippers.must-enter-3-to-5-chars',
      }, {
        key: 'duplicate',
        msg: 'container.shippers.duplicate-prefix',
      }],
    };

    const INPUT_FIELD_TO_TAB_NAME = {
      // input field
      'shipper-prefix': {
        name: 'container.shippers.shipper-prefix',
        tab: 'container.shippers.tab-basic-settings',
      },
      'shipper-name': {
        name: 'commons.model.shipper-name',
        tab: 'container.shippers.tab-basic-settings',
      },
      'shipper-phone-number': {
        name: 'container.shippers.shipper-phone-number',
        tab: 'container.shippers.tab-basic-settings',
      },
      'liaison-address': {
        name: 'container.shippers.liaison-address',
        tab: 'container.shippers.tab-basic-settings',
      },
      'shipper-email': {
        name: 'container.shippers.shipper-email',
        tab: 'container.shippers.tab-basic-settings',
      },
      'shipper-classification': {
        name: 'container.shippers.shipper-classification',
        tab: 'container.shippers.tab-basic-settings',
      },
      'shipper-dashboard-password': {
        name: 'container.shippers.shipper-dashboard-password',
        tab: 'container.shippers.tab-basic-settings',
      },
      'pricing-script': {
        name: 'commons.pricing-script',
        tab: 'container.shippers.tab-basic-settings',
      },
      'premium-pickup-daily-limit': {
        name: 'container.shippers.premium-pickup-daily-limit',
        tab: 'container.shippers.tab-marketplace-settings',
      },
      industry: {
        name: 'container.shippers.oc-industry',
        tab: 'container.shippers.tab-basic-settings',
      },
      'account-type': {
        name: 'container.shippers.shipper-account-type',
        tab: 'container.shippers.tab-basic-settings',
      },

      // incremental input field
      'more-settings-service-type': {
        name: 'container.shippers.service-type',
        tab: 'container.shippers.tab-more-settings',
      },
      'more-settings-service-level': {
        name: 'container.shippers.service-level',
        tab: 'container.shippers.tab-more-settings',
      },
      'marketplace-service-type': {
        name: 'container.shippers.service-type',
        tab: 'container.shippers.tab-marketplace-settings',
      },
      'marketplace-service-level': {
        name: 'container.shippers.service-level',
        tab: 'container.shippers.tab-marketplace-settings',
      },

      // shipper update endpoint
      'shipper-details': {
        name: 'container.shippers.shipper-details',
        tab: 'container.shippers.tab-basic-settings',
      },

      // shipper endpoint namespace
      order_create: {
        name: 'container.shippers.order-create',
        tab: 'container.shippers.tab-basic-settings',
      },
      reservation: {
        name: 'container.shippers.more-reservation',
        tab: 'container.shippers.tab-more-settings',
      },
      shopify: {
        name: 'container.shippers.shopify',
        tab: 'container.shippers.tab-integration-settings',
      },
      returns: {
        name: 'container.shippers.more-returns',
        tab: 'container.shippers.tab-more-settings',
      },
      label_printer: {
        name: 'container.shippers.label-printer',
        tab: 'container.shippers.tab-basic-settings',
      },
      qoo10: {
        name: 'container.shippers.qoo10',
        tab: 'container.shippers.tab-integration-settings',
      },
      magento: {
        name: 'container.shippers.magento',
        tab: 'container.shippers.tab-integration-settings',
      },
      distribution_points: {
        name: 'container.shippers.more-distribution-points',
        tab: 'container.shippers.tab-more-settings',
      },
      marketplace_billing: {
        name: 'container.shippers.marketplace-billing',
        tab: 'container.shippers.tab-marketplace-settings',
      },
      marketplace_default: {
        name: 'container.shippers.marketplace-default',
        tab: 'container.shippers.tab-marketplace-settings',
      },
      notification: {
        name: 'container.shippers.more-notification',
        tab: 'container.shippers.tab-more-settings',
      },
      pickup: {
        name: 'commons.model.pickup',
        tab: 'container.shippers.tab-more-settings',
      },

      // others
      'create-sd-account': {
        name: 'container.shippers.create-sd-account',
        tab: 'container.shippers.tab-basic-settings',
      },
      'create-shipper-address': {
        name: 'container.shippers.create-shipper-address',
        tab: 'container.shippers.tab-more-settings',
      },
    };

    const TIMESLOTS = Timewindow.toOptions([
      Timewindow.TYPE.NIGHTSLOT,
      Timewindow.TYPE.DAYSLOT,
      Timewindow.TYPE.DAYNIGHTSLOT,
      Timewindow.TYPE.TIMESLOT,
    ]);

    const DEFAULT_OTP_LENGTH = 6;
    const DEFAULT_OTP_ATTEMPTS = 3;
    const DELIVERY_OTP_LIMIT_OPTIONS = [3, 4, 5, 6, 7, 8, 9, 10];
    const NINJA_PACK_ENUM = getNinjaPackEnum();

    $scope.FORM_VALIDATION = FORM_VALIDATION;

    $scope.RESERVATIONS_DAY = RESERVATIONS_DAY;
    $scope.ADDRESS_FIELDS = ADDRESS_FIELDS;
    $scope.SHIPPER_TYPE = SHIPPER_TYPE;
    $scope.TIMESLOTS = TIMESLOTS;
    $scope.SHIPPER_CATEGORY = SHIPPER_CATEGORY;
    $scope.DEFAULT_OTP_LENGTH = DEFAULT_OTP_LENGTH;
    $scope.DEFAULT_OTP_ATTEMPTS = DEFAULT_OTP_ATTEMPTS;
    $scope.DELIVERY_OTP_LIMIT_OPTIONS = DELIVERY_OTP_LIMIT_OPTIONS;
    $scope.NINJA_PACK_ENUM = NINJA_PACK_ENUM;
    $scope.NINJA_PACK_PREFIX = 'Ninja';

    // functions
    $scope.initView = initView;

    $scope.isTrackingTypeDisabled = isTrackingTypeDisabled;
    $scope.isMarketplaceTrackingTypeDisabled = isMarketplaceTrackingTypeDisabled;
    $scope.isReservationEnabled = isReservationEnabled;
    $scope.isAutoReservationEnable = isAutoReservationEnable;
    $scope.isMarketplaceReservationEnabled = isMarketplaceReservationEnabled;
    $scope.isMarketplace = isMarketplace;
    $scope.isMarketplaceSeller = isMarketplaceSeller;
    $scope.isV2 = isV2;
    $scope.isV3 = isV3;
    $scope.isV4 = isV4;
    $scope.isReturnsInitiallyEnabled = isReturnsInitiallyEnabled;
    $scope.isReturnsEnabled = isReturnsEnabled;
    $scope.isOrderCreateLocked = isOrderCreateLocked;
    $scope.isOcVersionDisabled = isOcVersionDisabled;
    $scope.isServiceTypeSupported = isServiceTypeSupported;
    $scope.isNinjaPackTrackingType = isNinjaPackTrackingType;

    $scope.constructShipper = constructShipper;
    $scope.constructSettings = constructSettings;
    $scope.constructMilkrunSettings = constructMilkrunSettings;
    $scope.findTimewindowIdByStartTime = findTimewindowIdByStartTime;

    $scope.evaluateOcVersion = evaluateOcVersion;
    $scope.evaluateTrackingType = evaluateTrackingType;
    $scope.evaluateShipperType = evaluateShipperType;
    $scope.evaluateMarketplaceOcVersion = evaluateMarketplaceOcVersion;

    $scope.checkShipperContactValidity = checkShipperContactValidity;
    $scope.checkPrefixAvailability = checkPrefixAvailability;

    $scope.addNewPrefix = addNewPrefix;
    $scope.deletePrefix = deletePrefix;
    $scope.setPrefixAsDefault = setPrefixAsDefault;
    $scope.isDeletePrefixDisabled = isDeletePrefixDisabled;
    $scope.isPrefixesEnabled = isPrefixesEnabled;
    $scope.generatePrefixesInputField = generatePrefixesInputField;

    $scope.isAbleToAddNewPickupService = isAbleToAddNewPickupService;
    $scope.addNewPickupService = addNewPickupService;
    $scope.deletePickupService = deletePickupService;
    $scope.isAbleToAddNewMarketplacePickupService = isAbleToAddNewMarketplacePickupService;
    $scope.addNewMarketplacePickupService = addNewMarketplacePickupService;
    $scope.deleteMarketplacePickupService = deleteMarketplacePickupService;
    $scope.isServiceTypeRequired = isServiceTypeRequired;

    $scope.getErrorTitle = getErrorTitle;
    $scope.getErrorDescription = getErrorDescription;

    // functions details
    function initView(view) {
      view.shipperTypeCategory = SHIPPER_CATEGORY.NORMAL;

      // mostly used in basic and marketplace settings
      view.shipperTypeOptions = _.filter(SHIPPER_TYPE, option => option.id !== 2);
      view.shipperClassification = _.sortBy(Shippers.SHIPPER_CLASSIFICATION, 'displayName');
      view.shipperAccountType = _.sortBy(Shippers.SHIPPER_ACCOUNT_TYPE, 'displayName');
      view.ocVersionOptions = Shippers.orderCreateTypeToOptions();
      view.trackingTypeOptions = Shippers.trackingTypeToOptions();
      view.availableOcServiceOptions = Shippers.OC_SERVICES;

      // mostly used in more settings
      view.approxVolumeOptions = Reservation.volumeOptions;
      view.reservationDayOptions = RESERVATIONS_DAY;
      view.hourTimeSlotOptions = generateHourOptions();
      view.minuteTimeSlotOptions = generateMinuteOptions();
      view.pickupServiceTypeOptions = Shippers.pickupServiceTypeToOptions();
      view.pickupServiceLevelOptions = Shippers.pickupServiceLevelToOptions();
      view.reservationTypeOptions = _.filter(Reservation.typeOptions, option =>
        option.id !== 3
      );

      function generateHourOptions() {
        const hours = [];
        for (let i = 0; i < 24; i++) {
          let hour = _.toString(i);
          if (hour.length < 2) {
            hour = `0${hour}`;
          }
          hours.push(hour);
        }
        return hours;
      }

      function generateMinuteOptions() {
        const minutes = [];
        for (let i = 0; i < 60; i++) {
          let minute = _.toString(i);
          if (minute.length < 2) {
            minute = `0${minute}`;
          }
          minutes.push(minute);
        }
        return minutes;
      }
    }

    function isTrackingTypeDisabled(data, initialTrackingType, trackingType,
      isPackDisabled = false) {
      // v2 - all will be `LegacyDynamic`
      // v3 - can be `Prefixless` or `Fixed`
      // v4 - marketplace or non-marketplace can be `Dynamic` / `MultiDynamic` /
      //      `Fixed` / `MultiFixed`
      // marketplace seller is always `Prefixless`
      // if initially it was prefixless, then we can keep using prefixless
      // if it was not prefixless before, can not change to prefixless

      if (_.includes([
        initialTrackingType,
        trackingType,
      ], Shippers.TRACKING_TYPE.PREFIXLESS.id)) {
        return true;
      } else if (isV2(data)) {
        return trackingType !== Shippers.TRACKING_TYPE.LEGACY_DYNAMIC.id;
      } else if (isV3(data)) {
        return trackingType !== Shippers.TRACKING_TYPE.FIXED.id;
      } else if (isV4(data) && isMarketplaceSeller(data)) {
        return trackingType !== Shippers.TRACKING_TYPE.PREFIXLESS.id;
      } else if (isV4(data)) {
        // marketplace also support ninja pack
        const trackingTypesEnabled = [
          Shippers.TRACKING_TYPE.DYNAMIC.id,
          Shippers.TRACKING_TYPE.FIXED.id,
          Shippers.TRACKING_TYPE.MULTI_FIXED.id,
          Shippers.TRACKING_TYPE.MULTI_DYNAMIC.id,
        ];
        if (!isPackDisabled && !isMarketplace(data)) {
          trackingTypesEnabled.push(Shippers.TRACKING_TYPE.NINJA_FIXED.id);
        }

        return !(_.includes(trackingTypesEnabled, trackingType));
      }
      return false;
    }

    function isMarketplaceTrackingTypeDisabled(data, trackingType) {
      if (data.marketplace.ocVersion === Shippers.ORDER_CREATE_TYPE.V2.id) {
        return trackingType !== Shippers.TRACKING_TYPE.LEGACY_DYNAMIC.id;
      } else if (data.marketplace.ocVersion === Shippers.ORDER_CREATE_TYPE.V3.id) {
        return trackingType !== Shippers.TRACKING_TYPE.FIXED.id;
      } else if (data.marketplace.ocVersion === Shippers.ORDER_CREATE_TYPE.V4.id) {
        return trackingType !== Shippers.TRACKING_TYPE.PREFIXLESS.id;
      }

      return false;
    }

    function isReservationEnabled(data) {
      return data.more.isAutoReservation === '10';
    }

    function isMarketplaceReservationEnabled(data) {
      return data.marketplace.isAutoReservation === '10';
    }

    function isMarketplace(data) {
      return _.get(data, 'basic.shipperType.id') === SHIPPER_CATEGORY.MARKETPLACE;
    }

    function isMarketplaceSeller(data) {
      return _.get(data, 'basic.shipperType.id') === SHIPPER_CATEGORY.MARKETPLACE_SELLER;
    }

    function isV2(data) {
      return data.basic.ocVersion === Shippers.ORDER_CREATE_TYPE.V2.id;
    }

    function isV3(data) {
      return data.basic.ocVersion === Shippers.ORDER_CREATE_TYPE.V3.id;
    }

    function isV4(data) {
      return data.basic.ocVersion === Shippers.ORDER_CREATE_TYPE.V4.id;
    }
    /**
     * Only OCv2 and OCv3 that able to use auto rsvn
     * @param {*} data
     */
    function isAutoReservationEnable(data) {
      return _.includes([
        Shippers.ORDER_CREATE_TYPE.V2.id,
        Shippers.ORDER_CREATE_TYPE.V3.id,
      ], data.basic.ocVersion);
    }

    function isReturnsInitiallyEnabled(data) {
      return data.more.initialReturnState;
    }

    function isReturnsEnabled(data) {
      return nvModelUtils.fromYesNoBitStringToBool(data.more.isReturnsEnabled);
    }

    function isOrderCreateLocked(data) {
      return _.get(data, 'basic.shipperType.id') !== SHIPPER_CATEGORY.NORMAL;
    }

    function isOcVersionDisabled(data, version) {
      if (isMarketplace(data)) {
        return version !== Shippers.ORDER_CREATE_TYPE.V4.id;
      }
      return false;
    }

    function isServiceTypeSupported(serviceTypes, serviceType) {
      return _.find(serviceTypes, type => type === serviceType) || false;
    }

    function getNinjaPackEnum() {
      let theEnum = null;
      _.forEach(Shippers.SERVICE_TYPE, (serviceType, key) => {
        if (serviceType.id === Shippers.SERVICE_TYPE.NINJA_PACK.id) {
          theEnum = key;
          return false;
        }

        return _.noop();
      });

      return theEnum;
    }

    function constructShipper(globalShipperId, view, data, originalShipper = {}) {
      // basic
      let shipper = angular.copy(originalShipper);

      const salesCode = view.salesPerson.selectedSales ?
        view.salesPerson.selectedSales.code : null;

      _.assign(shipper, {
        active: nvModelUtils.fromYesNoBitStringToBool(data.basic.status),
        billing_address: data.basic.billingAddress,
        billing_contact: data.basic.billingContact,
        billing_name: data.basic.billingName,
        billing_postcode: data.basic.billingPostcode,
        industry_id: data.basic.industry,
        liaison_address: data.basic.liaisonAddress,
        liaison_contact: data.basic.liaisonContact,
        liaison_email: data.basic.liaisonEmail,
        liaison_name: data.basic.liaisonName,
        liaison_postcode: data.basic.liaisonPostcode,
        name: data.basic.name,
        short_name: data.basic.shortName,
        sales_person: salesCode,
        contact: data.basic.contact,
        email: data.basic.email,
        distribution_channel_id: data.basic.shipperClassification,
        account_type_id: data.basic.accountType,
        dash_password: data.basic.password,
      });
      // move back to original shipper model
      shipper = _.assign(shipper, { id: globalShipperId });
      shipper = _.omit(shipper, ['global_id']);
      // return to help determine settings
      return shipper;
    }

    function constructSettings(data, originalSettings = {}) {
      const settings = angular.copy(originalSettings);
      constructOcSettings(settings, data);
      constructReservationSettings(settings, data);
      constructShopifySettings(settings, data);
      constructReturnsSettings(settings, data);
      constructLabelPrinterSettings(settings, data);
      constructQoo10Settings(settings, data);
      constructMagentoSettings(settings, data);
      constructDpSettings(settings, data);
      constructPickupSettings(settings, data);
      constructDeliverySettings(settings, data);
      constructNotificationSettings(settings, data);
      constructMarketplaceBillingSettings(settings, data);
      constructMarketplaceDefault(settings, data);
      return settings;
    }

    function constructOcSettings(settings, data) {
      settings.order_create = settings.order_create || {};
      const prefix = data.basic.trackingType === Shippers.TRACKING_TYPE.PREFIXLESS.id ?
        undefined : data.basic.prefix;

      settings.order_create = _.assign(settings.order_create, {
        version: data.basic.ocVersion,
        prefix: prefix,
        services_available: getServicesAvailable(data.basic.serviceLevels),
        available_service_types: getServicesTypes(data.basic.serviceType),
        available_service_levels: data.basic.serviceLevels,
        is_pre_paid: nvModelUtils.fromYesNoBitStringToBool(data.basic.isPrePaid),
        tracking_type: data.basic.trackingType,
        allow_cod_service: nvModelUtils.fromYesNoBitStringToBool(data.basic.allowCod),
        allow_cp_service: nvModelUtils.fromYesNoBitStringToBool(data.basic.allowCp),
        allow_staged_orders: nvModelUtils.fromYesNoBitStringToBool(data.basic.allowStaging),
        is_multi_parcel_shipper: nvModelUtils.fromYesNoBitStringToBool(
          data.basic.isMultiParcel
        ),
        is_marketplace: _.get(data, 'basic.shipperType.id') === SHIPPER_CATEGORY.MARKETPLACE,
        is_marketplace_seller: _.get(data, 'basic.shipperType.id') === SHIPPER_CATEGORY.MARKETPLACE_SELLER,
        delivery_otp_length: _.get(data, 'basic.deliveryOtpLength'),
      });
      settings.order_create = _.omit(settings.order_create,
        [
          'bulky_type',
          'same_day_pickup_cutoff_time',
          'pickup_cutoff_time',
        ]);

      if (isPrefixesEnabled(data.basic.trackingType)) {
        settings.order_create.prefixes = data.basic.prefixes;
        settings.order_create.prefix = settings.order_create.prefixes[0];
      } else {
        settings.order_create = _.omit(settings.order_create, 'prefixes');
      }

      if (isNinjaPackTrackingType(data.basic.trackingType)) {
        settings.order_create = _.omit(settings.order_create, 'prefix');
      }

      function getServicesAvailable(serviceLevels) {
        const servicesAvailable = [];
        _.forEach(serviceLevels, (serviceLevel) => {
          const ocService = _.find(
            Shippers.OC_SERVICES, ['serviceLevelId', serviceLevel]
          );

          if (ocService) {
            servicesAvailable.push(ocService.value);
          }
        });

        if (data.basic.hasFreight) {
          servicesAvailable.push('FREIGHT');
        }

        return servicesAvailable;
      }

      function getServicesTypes(serviceTypeToBoolMap) {
        const serviceTypes = [];
        _.forEach(serviceTypeToBoolMap, (bitString, serviceTypeKey) => {
          if (nvModelUtils.fromYesNoBitStringToBool(bitString)) {
            serviceTypes.push(Shippers.SERVICE_TYPE[serviceTypeKey].id);
          }
        });

        return serviceTypes;
      }
    }

    function constructReservationSettings(settings, data) {
      settings.reservation = settings.reservation || {};
      const readyDateTime = `${data.more.readyTime.hour}:${data.more.readyTime.minute}:00`;
      const latestDateTime = `${data.more.latestTime.hour}:${data.more.latestTime.minute}:00`;
      const cutoffDateTime = `${data.more.cutoffTime.hour}:${data.more.cutoffTime.minute}:00`;
      _.assign(settings.reservation, {
        days: data.more.reservationDays,
        allowed_types: data.more.allowedTypes,
        allow_shipper_customer_reservation: nvModelUtils.fromYesNoBitStringToBool(
          data.more.customerReservation
        ),
        auto_reservation_enabled: nvModelUtils.fromYesNoBitStringToBool(
          data.more.isAutoReservation
        ),
        auto_reservation_ready_time: readyDateTime,
        auto_reservation_latest_time: latestDateTime,
        auto_reservation_cutoff_time: cutoffDateTime,
        auto_reservation_address_id: data.more.defaultAddressId,
        auto_reservation_approx_volume: data.more.approxVolume,
        enforce_parcel_pickup_tracking: nvModelUtils.fromYesNoBitStringToBool(
          data.basic.enforceParcelPickupTracking
        ),
      });
    }

    function constructShopifySettings(settings, data) {
      settings.shopify = settings.shopify || {};
      _.assign(settings.shopify, {
        max_delivery_days: data.integrations.shopifyMaxDelivery,
        dd_offset: data.integrations.shopifyDdOffset,
        dd_timewindow_id: data.integrations.shopifyTimewindowId,
        base_uri: data.integrations.shopifyBaseUrl,
        api_key: data.integrations.shopifyApiKey,
        password: data.integrations.shopifyPassword,
        is_shipping_code_filter_enabled: nvModelUtils.fromYesNoBitStringToBool(
          data.integrations.shopifyCodeFilter
        ),
        shipping_codes: commaToArray(data.integrations.shopifyCode),
      });

      function commaToArray(str) {
        if (!str) {
          return [];
        }
        const result = _.split(str, ',');
        return _.map(result, el => el.trim());
      }
    }

    function constructReturnsSettings(settings, data) {
      settings.returns = settings.returns || {};
      const enabled = nvModelUtils.fromYesNoBitStringToBool(data.more.isReturnsEnabled);
      if (!enabled) {
        _.assign(settings.returns, {
          address_1: '',
          address_2: '',
          city: '',
          contact: '00000',
          email: '',
          last_return_number: '',
          name: '',
          postcode: '',
          delete: true,
        });
      } else {
        _.assign(settings.returns, {
          address_1: data.more.returnsAddress1 || '',
          address_2: data.more.returnsAddress2 || '',
          city: data.more.returnsCity || '',
          contact: data.more.returnsContact || '00000',
          email: data.more.returnsEmail || '',
          last_return_number: data.more.lastReturnsNumber || '',
          name: data.more.returnsName || '',
          postcode: data.more.returnsPostcode || '',
          delete: false,
        });
      }
    }

    function constructLabelPrinterSettings(settings, data) {
      settings.label_printer = settings.label_printer || {};
      _.assign(settings.label_printer, {
        show_shipper_details: nvModelUtils.fromYesNoBitStringToBool(
          data.basic.isPrinterAvailable
        ),
        printer_ip: data.basic.printerIp || null,
      });
    }

    function constructQoo10Settings(settings, data) {
      settings.qoo10 = settings.qoo10 || {};
      _.assign(settings.qoo10, {
        username: data.integrations.qooUsername || '',
        password: data.integrations.qooPassword || '',
      });
    }

    function constructMagentoSettings(settings, data) {
      settings.magento = settings.magento || {};
      _.assign(settings.magento, {
        username: data.integrations.magentoUsername || '',
        password: data.integrations.magentoPassword || '',
        soap_api_url: data.integrations.magentoApiUrl || '',
      });
    }

    function constructDpSettings(settings, data) {
      settings.distribution_points = settings.distribution_points || {};
      _.assign(settings.distribution_points, {
        allow_returns_on_dpms: nvModelUtils.fromYesNoBitStringToBool(
          data.more.isReturnsOnDpms
        ),
        dmps_logo_url: data.more.dpmsLogoUrl,
        allow_returns_on_vault: nvModelUtils.fromYesNoBitStringToBool(
          data.more.isReturnsOnVault
        ),
        // vault_company_id: '',
        vault_is_integrated: nvModelUtils.fromYesNoBitStringToBool(
          data.more.isIntegratedVault
        ),
        vault_collect_customer_nric_code: nvModelUtils.fromYesNoBitStringToBool(
          data.more.isCollectCustomerNricCode
        ),
        vault_logo_url: data.more.vaultLogoUrl,
        allow_returns_on_shipper_lite: nvModelUtils.fromYesNoBitStringToBool(
          data.more.isReturnsOnShipperLite
        ),
        allow_driver_reschedule: nvModelUtils.fromYesNoBitStringToBool(
          data.basic.disableReschedule
        ),
        shipper_lite_logo_url: data.more.shipperLiteLogoUrl,
      });
    }

    function constructPickupSettings(settings, data) {
      settings.pickup = settings.pickup || {};
      // get the window data
      const selectedTimewindow = _.find(Timewindow.TIMEWINDOWS, {
        id: data.more.defaultPickupTime,
      });
      const defaultStart = selectedTimewindow ? selectedTimewindow.fromTime.format('HH:mm') : null;
      const defaultEnd = selectedTimewindow ? selectedTimewindow.toTime.format('HH:mm') : null;
      _.assign(settings.pickup, {
        allow_premium_pickup_on_sunday: nvModelUtils.fromYesNoBitStringToBool(
          data.more.allowPremiumPickupOnSunday
        ),
        premium_pickup_daily_limit: data.more.premiumPickupDailyLimit,
        service_type_level: data.more.pickupServiceTypeLevel,
        default_start_time: defaultStart,
        default_end_time: defaultEnd,
      });

      _.forEach(settings.pickup.service_type_level, (service) => {
        if (!isServiceTypeRequired(service.type)) {
          service.level = null;
        }
      });
    }

    function constructDeliverySettings(settings, data) {
      settings.delivery = settings.delivery || {};
      _.assign(settings.delivery, { delivery_otp_validation_limit: _.get(data, 'basic.deliveryOtpValidationLimit') });
    }

    function constructNotificationSettings(settings, data) {
      settings.notification = settings.notification || {};
      _.assign(settings.notification, {
        transit_shipper: nvModelUtils.fromYesNoBitStringToBool(data.more.transitShipper),
        transit_customer: nvModelUtils.fromYesNoBitStringToBool(data.more.transitCustomer),
        completed_shipper: nvModelUtils.fromYesNoBitStringToBool(data.more.completedShipper),
        completed_customer: nvModelUtils.fromYesNoBitStringToBool(data.more.completedCustomer),
        pickup_fail_shipper: nvModelUtils.fromYesNoBitStringToBool(
          data.more.pickupFailShipper
        ),
        pickup_fail_customer: nvModelUtils.fromYesNoBitStringToBool(
          data.more.pickupFailCustomer
        ),
        delivery_fail_shipper: nvModelUtils.fromYesNoBitStringToBool(
          data.more.deliveryFailShipper
        ),
        delivery_fail_customer: nvModelUtils.fromYesNoBitStringToBool(
          data.more.deliveryFailCustomer
        ),
      });
    }

    function constructMarketplaceBillingSettings(settings, data) {
      if (!isMarketplace(data)) {
        settings.marketplace_billing = null;
      } else {
        _.assign(settings.marketplace_billing, {
          billing_name: data.marketplace.billingName || '',
          billing_contact: data.marketplace.billingContact || '',
          billing_address: data.marketplace.billingAddress || '',
          billing_postcode: data.marketplace.billingPostcode || '',
        });
      }
    }

    function constructMarketplaceDefault(settings, data) {
      if (!isMarketplace(data)) {
        settings.marketplace_default = null;
      } else {
        // get the timewindow
        const selectedTimewindow = _.find(Timewindow.TIMEWINDOWS, {
          id: data.marketplace.defaultPickupTime,
        });
        const defaultStart = selectedTimewindow ? selectedTimewindow.fromTime.format('HH:mm') : null;
        const defaultEnd = selectedTimewindow ? selectedTimewindow.toTime.format('HH:mm') : null;

        _.assign(settings.marketplace_default, {
          reservation_days: data.marketplace.reservationDays,
          auto_reservation_enabled: nvModelUtils.fromYesNoBitStringToBool(
            data.marketplace.isAutoReservation
          ),
          auto_reservation_ready_time: `${data.marketplace.readyTime.hour}:${data.marketplace.readyTime.minute}:00`,
          auto_reservation_latest_time: `${data.marketplace.latestTime.hour}:${data.marketplace.latestTime.minute}:00`,
          auto_reservation_cutoff_time: `${data.marketplace.cutoffTime.hour}:${data.marketplace.cutoffTime.minute}:00`,
          auto_reservation_address_id: data.marketplace.defaultAddressId,
          auto_reservation_approx_volume: data.marketplace.approxVolume,
          reservation_allowed_types: data.marketplace.allowedTypes,
          order_create_version: data.marketplace.ocVersion,
          order_create_services_available: data.marketplace.selectedOcServices,
          order_create_is_pre_paid: nvModelUtils.fromYesNoBitStringToBool(
            data.marketplace.isPrePaid
          ),
          order_create_tracking_type: data.marketplace.trackingType,
          order_create_allow_cod_service: nvModelUtils.fromYesNoBitStringToBool(
            data.marketplace.allowCod
          ),
          order_create_allow_cp_service: nvModelUtils.fromYesNoBitStringToBool(
            data.marketplace.allowCp
          ),
          order_create_allow_staged_orders: nvModelUtils.fromYesNoBitStringToBool(
            data.marketplace.allowStaging
          ),
          order_create_is_multi_parcel_shipper: nvModelUtils.fromYesNoBitStringToBool(
            data.marketplace.isMultiParcel
          ),
          reservation_enforce_parcel_pickup_tracking: nvModelUtils.fromYesNoBitStringToBool(
            data.marketplace.reservationEnforceParcelPickupTracking
          ),
          pickup_default_start_time: defaultStart,
          pickup_default_end_time: defaultEnd,
          pickup_allow_premium_pickup_on_sunday: nvModelUtils.fromYesNoBitStringToBool(
            data.marketplace.allowPremiumPickupOnSunday
          ),
          pickup_premium_pickup_daily_limit: data.marketplace.premiumPickupDailyLimit,
          pickup_services: data.marketplace.pickupServiceTypeLevel,
        });
      }
    }

    function constructMilkrunSettings(data) {
      const result = {
        milkrun_settings: [],
      };

      _.forEach(data.milkrunSettings, (milkrunSetting) => {
        const timewindow = _.find(Timewindow.TIMEWINDOWS, ['id', milkrunSetting.timewindowId]);

        result.milkrun_settings.push({
          start_time: timewindow.fromTime.format('HH:mm'),
          end_time: timewindow.toTime.format('HH:mm'),
          days: milkrunSetting.days || [],
          no_of_reservation: 1,
        });
      });

      result.is_milk_run = _.size(result.milkrun_settings) > 0;

      return result;
    }

    function findTimewindowIdByStartTime(startTime, endTime) {
      if (!startTime || !endTime) {
        return null;
      }

      const startTimeMoment = moment(startTime, 'HH:mm');
      const endTimeMoment = moment(endTime, 'HH:mm');
      const timewindow = _.find(Timewindow.TIMEWINDOWS, theTimewindow =>
        theTimewindow.fromTime.valueOf() === startTimeMoment.valueOf() &&
        theTimewindow.toTime.valueOf() === endTimeMoment.valueOf() &&
        theTimewindow.type !== Timewindow.TYPE.NIGHTSLOT
      );

      return _.get(timewindow, 'id');
    }

    function evaluateOcVersion(val, oldVal, data, initialTrackingType) {
      if (val !== oldVal) {
        if (isV2(data)) {
          data.basic.trackingType = Shippers.TRACKING_TYPE.LEGACY_DYNAMIC.id;
        } else if (isV3(data)) {
          if (isTrackingTypeDisabled(
              data, initialTrackingType, data.basic.trackingType
            )) {
            data.basic.trackingType = Shippers.TRACKING_TYPE.FIXED.id;
          }
        } else if (isV4(data) && isMarketplace(data)) {
          data.basic.trackingType = Shippers.TRACKING_TYPE.DYNAMIC.id;
        } else if (isV4(data) && !isMarketplace(data)) {
          if (isTrackingTypeDisabled(
              data, initialTrackingType, data.basic.trackingType
            )) {
            data.basic.trackingType = Shippers.TRACKING_TYPE.FIXED.id;
          }
        }
      }
    }

    function evaluateTrackingType(val, oldVal, data, legacyShipperId, form) {
      if (val !== oldVal) {
        if (isPrefixesEnabled(data.basic.trackingType)) {
          if (_.size(data.basic.prefixes) <= 0) {
            data.basic.prefixes.push(data.basic.prefix);
          }

          revalidatePrefixes(data, legacyShipperId, form);
        }
      }
    }

    function evaluateShipperType(val, oldVal, data) {
      if (val !== oldVal) {
        let isNinjaPackEnabled = true;

        if (isMarketplace(data)) {
          isNinjaPackEnabled = false;

          data.basic.ocVersion = Shippers.ORDER_CREATE_TYPE.V4.id;
          data.basic.trackingType = Shippers.TRACKING_TYPE.DYNAMIC.id;
        }

        data.basic.serviceType[
          NINJA_PACK_ENUM
        ] = nvModelUtils.fromBoolToYesNoBitString(isNinjaPackEnabled);
      }
    }

    function evaluateMarketplaceOcVersion(val, oldVal, data) {
      if (val !== oldVal) {
        if (data.marketplace.ocVersion === Shippers.ORDER_CREATE_TYPE.V2.id) {
          data.marketplace.trackingType = Shippers.TRACKING_TYPE.LEGACY_DYNAMIC.id;
        } else if (data.marketplace.ocVersion === Shippers.ORDER_CREATE_TYPE.V3.id) {
          data.marketplace.trackingType = Shippers.TRACKING_TYPE.FIXED.id;
        } else if (data.marketplace.ocVersion === Shippers.ORDER_CREATE_TYPE.V4.id) {
          data.marketplace.trackingType = Shippers.TRACKING_TYPE.PREFIXLESS.id;
        }
      }
    }

    function checkShipperContactValidity(text, inputField) {
      if (!_.isUndefined(text) && text.match(/^\d+$/) !== null) {
        inputField.$setValidity('numericOnly', true);
      } else {
        inputField.$setValidity('numericOnly', false);
      }
    }

    function checkPrefixAvailability(
      prefix, inputField, data, legacyShipperId = null, form = null
    ) {
      // `LegacyDynamic` - not validate prefix pattern, only check duplicated
      // `Fixed` - Prefix is 5 chars - allows characters and digits only
      // `Dynamic` - Prefix is 3 to 5 chars, allows characters, digits and hyphen only

      $timeout(() => {
        inputField.$setTouched();
        inputField.$validate();

        inputField.$setValidity('exactly5', true);
        inputField.$setValidity('3to5', true);
        inputField.$setValidity('available', true);
        inputField.$setValidity('duplicate', true);

        if (_.includes([
          Shippers.TRACKING_TYPE.FIXED.id,
          Shippers.TRACKING_TYPE.MULTI_FIXED.id,
        ], data.basic.trackingType)) {
          if (_.size(prefix) !== 5) {
            inputField.$setValidity('exactly5', false);
            return;
          }
        } else if (_.includes([
          Shippers.TRACKING_TYPE.DYNAMIC.id,
          Shippers.TRACKING_TYPE.MULTI_DYNAMIC.id,
        ], data.basic.trackingType)) {
          if (_.size(prefix) < 3 || _.size(prefix) > 5) {
            inputField.$setValidity('3to5', false);
            return;
          }
        }

        // check duplicate
        if (_.includes([
          Shippers.TRACKING_TYPE.MULTI_DYNAMIC.id,
          Shippers.TRACKING_TYPE.MULTI_FIXED.id,
        ], data.basic.trackingType)) {
          const duplicatePrefixes = _.filter(data.basic.prefixes,
            (thePrefix, index, iteratee) =>
              _.includes(iteratee, thePrefix, index + 1)
            );

          _.forEach(data.basic.prefixes, (thePrefix, index) => {
            const otherPrefixInputField = generatePrefixesInputField(form, index);
            otherPrefixInputField.$setValidity('duplicate', true);

            if (_.includes(duplicatePrefixes, thePrefix)) {
              otherPrefixInputField.$setTouched();
              otherPrefixInputField.$setValidity('duplicate', false);
            }
          });
        }

        Shippers.read({
          prefix: prefix,
        }).then((result) => {
          if (_.size(_.get(result, 'data')) > 0 &&
            (legacyShipperId === null || _.get(result, 'data[0].legacy_id') !== legacyShipperId)) {
            inputField.$setValidity('available', false);
          }
        });
      });
    }

    function addNewPrefix(data) {
      data.basic.prefixes.push('');
    }

    function deletePrefix(data, index) {
      data.basic.prefixes.splice(index, 1);
    }

    function setPrefixAsDefault(data, index, legacyShipperId, form) {
      const defaultPrefix = data.basic.prefixes[index];
      deletePrefix(data, index);
      data.basic.prefixes.unshift(defaultPrefix);

      revalidatePrefixes(data, legacyShipperId, form);
    }

    function isDeletePrefixDisabled(data) {
      return _.size(data.basic.prefixes) <= 1;
    }

    function isPrefixesEnabled(trackingType) {
      return _.includes([
        Shippers.TRACKING_TYPE.MULTI_FIXED.id,
        Shippers.TRACKING_TYPE.MULTI_DYNAMIC.id,
      ], trackingType);
    }

    function isNinjaPackTrackingType(trackingType) {
      return trackingType === Shippers.TRACKING_TYPE.NINJA_FIXED.id;
    }

    function revalidatePrefixes(data, legacyShipperId, form) {
      _.forEach(data.basic.prefixes, (prefix, index) => {
        $timeout(() => {
          checkPrefixAvailability(
            prefix,
            generatePrefixesInputField(form, index),
            data,
            legacyShipperId,
            form
          );
        }, 100);
      });
    }

    function generatePrefixesInputField(form, index) {
      return form[`shipper-prefix-${index}`];
    }

    function isAbleToAddNewPickupService(data) {
      return _.size(data.more.pickupServiceTypeLevel) < 3;
    }

    function addNewPickupService(data) {
      data.more.pickupServiceTypeLevel.push({
        type: null,
        level: null,
      });
    }

    function deletePickupService(data, index) {
      data.more.pickupServiceTypeLevel.splice(index, 1);
    }

    function isAbleToAddNewMarketplacePickupService(data) {
      return _.size(data.marketplace.pickupServiceTypeLevel) < 3;
    }

    function addNewMarketplacePickupService(data) {
      data.marketplace.pickupServiceTypeLevel.push({
        type: null,
        level: null,
      });
    }

    function deleteMarketplacePickupService(data, index) {
      data.marketplace.pickupServiceTypeLevel.splice(index, 1);
    }

    function isServiceTypeRequired(serviceType) {
      return serviceType === Shippers.PICKUP_SERVICE_TYPE.SCHEDULED.id;
    }

    function getErrorTitle(inputFieldId) {
      const processedInputFieldId = inputFieldId.replace(/[0-9]/g, '');

      if (!_.isUndefined(INPUT_FIELD_TO_TAB_NAME[processedInputFieldId])) {
        return `${nvTranslate.instant(INPUT_FIELD_TO_TAB_NAME[processedInputFieldId].name)} (${
          nvTranslate.instant(INPUT_FIELD_TO_TAB_NAME[processedInputFieldId].tab)})`;
      }

      return _.upperFirst(_.lowerCase(processedInputFieldId));
    }

    function getErrorDescription(theResponse) {
      const descriptions = [];
      _.forEach(_.get(theResponse, 'details'), (detail) => {
        descriptions.push(detail.message);
      });

      if (_.size(descriptions) <= 0 && !_.isUndefined(_.get(theResponse, 'message'))) {
        descriptions.push(_.get(theResponse, 'message'));
      }

      return _.join(descriptions, ', ');
    }
  }
}());
