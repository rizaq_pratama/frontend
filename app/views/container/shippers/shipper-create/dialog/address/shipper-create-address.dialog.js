(function controller() {
  angular
    .module('nvOperatorApp.controllers')
    .controller('ShipperCreateAddressDialogController', ShipperCreateAddressDialogController);

  ShipperCreateAddressDialogController.$inject = [
    'localData', '$mdDialog', 'nvTranslate', 'Shippers', 'Timewindow',
    '$timeout',
  ];

  const API_STATE = {
    IDLE: 'idle',
    WAITING: 'waiting',
  };
  const TAB = {
    ADDRESS: 0,
    MILKRUN_RESERVATION: 1,
  };

  function ShipperCreateAddressDialogController(
    localData, $mdDialog, nvTranslate, Shippers, Timewindow,
    $timeout
  ) {
    const ctrl = this;
    ctrl.mode = localData.mode;
    ctrl.theme = localData.theme;
    ctrl.RESERVATIONS_DAY = localData.RESERVATIONS_DAY;

    ctrl.TAB = TAB;
    ctrl.state = {};
    ctrl.title = '';
    ctrl.data = Shippers.mapToLocalizedAddress(localData.addressData);
    ctrl.function = {};

    ctrl.state.isAddressFinder = false;

    ctrl.function.onCancel = onCancel;
    ctrl.function.onDelete = onDelete;
    ctrl.function.onSubmit = onSubmit;
    ctrl.function.isAbleToSubmit = isAbleToSubmit;
    ctrl.function.addNewMilkrunSetting = addNewMilkrunSetting;
    ctrl.function.isRemoveMilkrunSettingDisabled = isRemoveMilkrunSettingDisabled;
    ctrl.function.removeMilkrunSetting = removeMilkrunSetting;
    ctrl.function.onAddressFinderCallback = onAddressFinderCallback;

    init();
    function init() {
      ctrl.title = ctrl.mode === 'edit' ? nvTranslate.instant('container.addressing.edit-address') : nvTranslate.instant('container.addressing.add-address');

      ctrl.data.isMilkrun = _.get(localData.addressData, 'is_milk_run') || false;
      ctrl.data.milkrunSettings = _.map(
        _.cloneDeep(_.get(localData.addressData, 'milkrun_settings')), generateMilkrunSettingPayload
      ) || [];

      ctrl.state = {
        apiDelete: API_STATE.IDLE,
        apiSubmit: API_STATE.IDLE,
        pickupCountry: Shippers.SUPPORTED_PICKUP_COUNTRY,
        pickupCountryOptions: Shippers.pickupCountryToOptions(),
        selectedTab: null,
        timeslotOptions: Timewindow.toOptions([
          Timewindow.TYPE.DAYSLOT,
          Timewindow.TYPE.DAYNIGHTSLOT,
          Timewindow.TYPE.TIMESLOT,
        ]),
        focusMilkrunReservationsTab: false,
      };

      if (localData.focusMilkrunReservationsTab) {
        $timeout(() => {
          ctrl.state.focusMilkrunReservationsTab = localData.focusMilkrunReservationsTab;
        }, 1000);
      }
    }

    function onCancel() {
      $mdDialog.hide(0);
    }

    function onDelete() {
      ctrl.state.apiDelete = API_STATE.WAITING;

      if (ctrl.data.id > 0) {
        Shippers.deleteAddress(localData.shipperId, ctrl.data.id)
          .then(onSuccess)
          .finally(() => (ctrl.state.apiDelete = API_STATE.IDLE));
      } else {
        ctrl.state.apiDelete = API_STATE.IDLE;
        onSuccess();
      }

      function onSuccess() {
        actionSuccess({ action: 'delete', data: ctrl.data });
      }
    }

    function onSubmit() {
      // create fake id for table purpose (negative value)
      const internalAddressData = Shippers.mapToInternalAddress(ctrl.data);
      if (ctrl.mode === 'create' && !_.get(ctrl.data, 'id')) {
        internalAddressData.id = -Math.floor(Math.random() * 1000);
      }

      _.assign(internalAddressData, localData.constructMilkrunSettings(ctrl.data));

      actionSuccess({ action: ctrl.mode, data: internalAddressData });
    }

    function isAbleToSubmit() {
      return !ctrl.state.isAddressFinder;
    }

    function addNewMilkrunSetting() {
      ctrl.data.milkrunSettings.push(generateMilkrunSettingPayload());
    }

    function isRemoveMilkrunSettingDisabled() {
      return ctrl.mode === 'edit' &&
        ctrl.data.isMilkrun && _.size(ctrl.data.milkrunSettings) <= 1;
    }

    function removeMilkrunSetting(index) {
      ctrl.data.milkrunSettings.splice(index, 1);
    }

    function generateMilkrunSettingPayload(milkrunSetting = {}) {
      return {
        timewindowId: localData.findTimewindowIdByStartTime(
          milkrunSetting.start_time, milkrunSetting.end_time
        ),
        days: milkrunSetting.days || [],
        noOfReservations: 1,
      };
    }

    function actionSuccess(data) {
      $mdDialog.hide(data);
    }

    function onAddressFinderCallback(address) {
      ctrl.state.isAddressFinder = false;
      if (address) {
        _.assign(ctrl.data, _.omit(address, 'country'));
        // API accept country code as country value
        // address finder return country name
        const selectedCountry = _.find(ctrl.state.pickupCountryOptions, country =>
          _.lowerCase(country.displayName) === _.lowerCase(address.country)
        );
        ctrl.data.country = selectedCountry.value;
      }
    }
  }
}());
