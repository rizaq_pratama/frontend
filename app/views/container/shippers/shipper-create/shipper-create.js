(function controller() {
  angular
      .module('nvOperatorApp.controllers')
      .controller('ShipperCreateController', ShipperCreateController);

  ShipperCreateController.$inject = [
    '$q', '$scope', '$state', '$timeout',
    'Shippers', 'Industry', 'PricingScriptsV2', 'Address',
    'appSidenav', 'nvNavGuard', 'nvDialog', 'nvTranslate',
    'nvAutocomplete.Data', 'nvTable', 'nvToast', '$stateParams', 'nvModelUtils',
    'nvRequestUtils', 'nvUtils', 'ShipperService',
  ];

  const TAB_ITEMS = [
      { id: 0, displayName: 'container.shippers.tab-basic-settings' },
      { id: 1, displayName: 'container.shippers.tab-more-settings' },
      { id: 2, displayName: 'container.shippers.tab-integration-settings' },
      { id: 3, displayName: 'container.shippers.tab-marketplace-settings' },
  ];

  function ShipperCreateController(
    $q, $scope, $state, $timeout,
    Shippers, Industry, PricingScriptsV2, Address,
    appSidenav, nvNavGuard, nvDialog, nvTranslate,
    nvAutocompleteData, nvTable, nvToast, $stateParams, nvModelUtils,
    nvRequestUtils, nvUtils, ShipperService
  ) {
    const ctrl = this;

    const globalShipperId = 0;
    const filter = $stateParams.filter;
    let isPageSaved = false;
    ctrl.initialTrackingType = Shippers.TRACKING_TYPE.PREFIXLESS.id;
    ctrl.isFailedSubmit = false;
    ctrl.createdShipperCache = {
      shipperCreated: false,
      sdShipperCreated: false,
      shipperAddressCreated: false,
      shipperId: null,
      legacyShipperId: null,
    };

    // global variable
    ctrl.Shippers = Shippers;
    ctrl.FORM_VALIDATION = $scope.FORM_VALIDATION;
    ctrl.marketplaceShippers = null;
    ctrl.marketplaceId = null;
    ctrl.ninjaPrefix = $scope.NINJA_PACK_PREFIX;

    ctrl.state = {};

    ctrl.data = {
      basic: {},
      more: {},
      integrations: {},
      marketplace: {},
    };

    ctrl.view = {
      tabs: TAB_ITEMS,
      timeslots: _.cloneDeep($scope.TIMESLOTS),
      serviceLevelOptions: Shippers.serviceLevelToOptions(),
    };

    ctrl.function = {
      init: init,
      discardChanges: discardChanges,
      saveChanges: saveChanges,
      onTabChanged: onTabChanged,
      addNewPrefix: $scope.addNewPrefix,
      deletePrefix: $scope.deletePrefix,
      setPrefixAsDefault: $scope.setPrefixAsDefault,
      isDeletePrefixDisabled: $scope.isDeletePrefixDisabled,
      isPrefixesEnabled: $scope.isPrefixesEnabled,
      generatePrefixesInputField: $scope.generatePrefixesInputField,
      isAbleToAddNewPickupService: $scope.isAbleToAddNewPickupService,
      addNewPickupService: $scope.addNewPickupService,
      deletePickupService: $scope.deletePickupService,
      isAbleToAddNewMarketplacePickupService: $scope.isAbleToAddNewMarketplacePickupService,
      addNewMarketplacePickupService: $scope.addNewMarketplacePickupService,
      deleteMarketplacePickupService: $scope.deleteMarketplacePickupService,
      isServiceTypeRequired: $scope.isServiceTypeRequired,
      addReservationAddress: addReservationAddress,
      editReservationAddress: editReservationAddress,
      isReturnsEnabled: $scope.isReturnsEnabled,
      setAddressAsDefault: setAddressAsDefault,
      setAddressAsMilkrun: setAddressAsMilkrun,
      addressButtonClass: addressButtonClass,
      replaceWithLiaisonDetails: replaceWithLiaisonDetails,
      isReservationEnabled: $scope.isReservationEnabled,
      isAutoReservationEnable: $scope.isAutoReservationEnable,
      isMarketplaceReservationEnabled: $scope.isMarketplaceReservationEnabled,
      isMarketplace: $scope.isMarketplace,
      isTrackingTypeDisabled: $scope.isTrackingTypeDisabled,
      isMarketplaceTrackingTypeDisabled: $scope.isMarketplaceTrackingTypeDisabled,
      isOcVersionDisabled: $scope.isOcVersionDisabled,
      isOrderCreateLocked: $scope.isOrderCreateLocked,
      checkShipperContactValidity: $scope.checkShipperContactValidity,
      checkSDAccountAvailability: checkSDAccountAvailability,
      checkPrefixAvailability: $scope.checkPrefixAvailability,
      nvModelUtils: nvModelUtils,
    };

      // start
    init();
    $scope.$on('$destroy', () => {
      appSidenav.setConfigLockedOpen(true);
    });

    function init() {
      setPageLoading(true);
      onTabChanged(0, true);
      nvNavGuard.lock();

      $timeout(() => {
        appSidenav.setConfigLockedOpen(false);
      });

      $q.all([
        Industry.read(),
        PricingScriptsV2.readAllActive(),
        Shippers.readSalesPerson(),
      ]).then((responses) => {
        const industries = responses[0];
        const scripts = responses[1];
        const salesPerson = responses[2];
        $scope.initView(ctrl.view);
        readSalesPerson(salesPerson);
        readPricingScript(scripts);
        readIndustry(industries);
        readAddresses();

        readBasicSettings();
        readMoreSettings();
        readIntegrations();
        readMarketplaceSettings();

        setupListener();
        setPageLoading(false);
      }, () => {
          // on failed bunch of call
        setPageLoading(false);
        gotoShipperListPage();
      });
    }

    function checkSDAccountAvailability(email) {
      return Shippers.checkSdAccount(email)
        .then(() => {
          ctrl.basicForm['shipper-email'].$setValidity('available', true);
        },
        () => {
          ctrl.basicForm['shipper-email'].$setValidity('available', false);
        });
    }

    function saveChanges($event) {
      const failedResponses = [];

      showLoadingSheet(true);

      const shipper = $scope.constructShipper(globalShipperId, ctrl.view, ctrl.data);
      let settings = $scope.constructSettings(ctrl.data);
      const pricingScriptId = ctrl.view.pricingScripts.selectedScript
          ? ctrl.view.pricingScripts.selectedScript.id
          : ctrl.originalPricingScriptId;

      if (!$scope.isMarketplace(ctrl.data)) {
        settings = _.omit(settings, ['marketplace_default', 'marketplace_billing']);
      }
      if (!$scope.isReturnsEnabled(ctrl.data) && $scope.isReturnsInitiallyEnabled(ctrl.data)) {
        settings = _.omit(settings, ['returns']);
      }

      const isValid = validate();
      if (isValid) {
        ctrl.createdShipperCache.shipperAddressCreated = false;

        if (!ctrl.createdShipperCache.shipperCreated) {
          Shippers.create(shipper).then(createShipperSuccess, createOrUpdateShipperFailure);
        } else {
          // update shipper
          Shippers.updateShipperV2(ctrl.createdShipperCache.shipperId, shipper).then(
            updateShipperSuccess, createOrUpdateShipperFailure
          );
        }
      } else {
        showLoadingSheet(false);
      }

      function createShipperSuccess(result) {
        ctrl.createdShipperCache.shipperCreated = true;
        ctrl.createdShipperCache.shipperId = result.id;
        ctrl.createdShipperCache.legacyShipperId = result.legacy_id;

        proceedToNextStep();
      }

      function createOrUpdateShipperFailure(response) {
        nvToast.remove();

        failedResponses.push({
          title: $scope.getErrorTitle('shipper-details'),
          description: $scope.getErrorDescription(response),
        });
        showErrorsDialog();

        nvToast.warning(nvTranslate.instant('container.shippers.some-changes-not-saved-successfully'));
        showLoadingSheet(false);
      }

      function updateShipperSuccess() {
        proceedToNextStep();
      }

      function createSdAccount() {
        Shippers.createSdAccount({
          name: shipper.name,
          email: shipper.email,
          password: ctrl.data.basic.password,
          shipper_id: ctrl.createdShipperCache.legacyShipperId,
          language_code: 'en',
        }).then(createSdAccountSuccess, createSdAccountFailure);

        function createSdAccountSuccess() {
          ctrl.createdShipperCache.sdShipperCreated = true;
          proceedToNextStep();
        }

        function createSdAccountFailure(response) {
          failedResponses.push({
            title: $scope.getErrorTitle('create-sd-account'),
            description: response.data,
          });
          showErrorsDialog();

          nvToast.warning(nvTranslate.instant('container.shippers.some-changes-not-saved-successfully'));
          showLoadingSheet(false);
        }
      }

      function createShipperAddress() {
        // save the address
        if (_.size(ctrl.view.addressTableParam.getUnfilteredTableData()) > 0) {
          const addressesCall = _.map(ctrl.view.addressTableParam.getUnfilteredTableData(),
              param => (Shippers.createAddress(
                ctrl.createdShipperCache.shipperId,
                _.omit(param.value, ['id']),
                { allow_update: true }
              )));
          $q.allSettled(addressesCall)
            .then(createShipperAddressSuccess, createShipperAddressFailure);
        } else {
          ctrl.createdShipperCache.shipperAddressCreated = true;
          proceedToNextStep();
        }

        function createShipperAddressSuccess(responses) {
          nvToast.remove();

          const tableData = _.cloneDeep(ctrl.view.addressTableParam.getUnfilteredTableData());
          _.forEach(responses, (response, index) => {
            if (response.state === nvRequestUtils.PROMISE_STATE.FULFILLED) {
              // replace to real id
              tableData[index].id = response.value.id;
              tableData[index].value.id = response.value.id;
            } else {
              failedResponses.push({
                title: $scope.getErrorTitle('create-shipper-address'),
                description: $scope.getErrorDescription(response.reason),
              });
            }
          });

          ctrl.view.addressTableParam.setData(tableData);
          ctrl.view.addressTableParam.refreshData();

          if (_.size(failedResponses) <= 0) {
            ctrl.createdShipperCache.shipperAddressCreated = true;
            proceedToNextStep();
          } else {
            showErrorsDialog();

            nvToast.warning(nvTranslate.instant('container.shippers.some-changes-not-saved-successfully'));
            showLoadingSheet(false);
          }
        }

        function createShipperAddressFailure(response) {
          nvToast.remove();

          failedResponses.push({
            title: $scope.getErrorTitle('create-shipper-address'),
            description: $scope.getErrorDescription(response),
          });
          showErrorsDialog();

          nvToast.warning(nvTranslate.instant('container.shippers.some-changes-not-saved-successfully'));
          showLoadingSheet(false);
        }
      }

      function saveShipperSettings() {
        const endpointsMap = [
          $scope.getErrorTitle('pricing-script'),
        ];

        const calls = [
          PricingScriptsV2.linkShippers(
            pricingScriptId, _.castArray(ctrl.createdShipperCache.shipperId), true
          ),
        ];

        _.forIn(settings, (val, key) => {
          calls.push(Shippers.updateSpecificSettingV2(
            ctrl.createdShipperCache.shipperId, val, key)
          );
          endpointsMap.push($scope.getErrorTitle(key));
        });

        $q.allSettled(calls).then(afterSaveShipperSettings);

        function afterSaveShipperSettings(responses) {
          nvToast.clear();

          _.forEach(responses, (response, index) => {
            if (response.state === nvRequestUtils.PROMISE_STATE.REJECTED) {
              failedResponses.push({
                title: getErrorTitle(index),
                description: $scope.getErrorDescription(response.reason),
              });
            }
          });

          if (_.size(failedResponses) > 0) {
            nvToast.warning(nvTranslate.instant('container.shippers.some-changes-not-saved-successfully'));
            showLoadingSheet(false);

            showErrorsDialog();
          } else {
            allSuccessHandling();
          }
        }

        function getErrorTitle(index) {
          return endpointsMap[index];
        }
      }

      function showErrorsDialog(isSubmitted = true) {
        const payload = {
          errors: failedResponses,
        };

        if (!isSubmitted) {
          payload.messages = {
            description: 'container.shippers.x-validation-errors',
          };
        }

        nvDialog.showSingle($event, {
          templateUrl: 'views/container/shippers/dialog/errors/errors.dialog.html',
          cssClass: 'nv-container-shipper-errors-dialog',
          controller: 'ShipperErrorsDialogController',
          controllerAs: 'ctrl',
          locals: {
            payload: payload,
          },
        });
      }

      function proceedToNextStep() {
        if (!ctrl.createdShipperCache.sdShipperCreated) {
          createSdAccount();
        } else if (!ctrl.createdShipperCache.shipperAddressCreated) {
          createShipperAddress();
        } else {
          saveShipperSettings();
        }
      }

      function allSuccessHandling() {
        // emit backgroudn task to disable shipper panel account if dash shipper created properly
        const shipper = {
          id: ctrl.createdShipperCache.shipperId,
          legacyId: ctrl.createdShipperCache.legacyShipperId,
        };
        ShipperService.verifyDashAccountAndDisableSdAccount(shipper);
        isPageSaved = true;
        nvToast.success(nvTranslate.instant('container.shippers.all-changes-saved-successfully'));
        showLoadingSheet(false);

        // redirect to edit shipper page
        nvNavGuard.unlock();
        $state.go('container.shippers.edit', {
          shipperId: ctrl.createdShipperCache.legacyShipperId,
        });
      }

      function setFormTouched(form) {
        _.each(form, (field) => {
          if (_.isObject(field) && !_.isUndefined(field.$validate)) {
            field.$setTouched();
            field.$validate();
          }
        });
      }

      function validate() {
        setFormTouched(ctrl.basicForm);
        setFormTouched(ctrl.integrationForm);
        setFormTouched(ctrl.marketplaceForm);
        setFormTouched(ctrl.moreForm);
        if (!(ctrl.basicForm.$valid && ctrl.integrationForm.$valid
          && ctrl.marketplaceForm.$valid && ctrl.moreForm.$valid)) {
          let errorFields = [];
          _.forEach([
            ctrl.basicForm,
            ctrl.integrationForm,
            ctrl.marketplaceForm,
            ctrl.moreForm,
          ], (form) => {
            _.forEach(form.$error, (error) => {
              errorFields = _.concat(errorFields, _.map(error, '$name'));
            });
          });

          errorFields = _.uniq(errorFields);
          _.forEach(errorFields, (errorField) => {
            failedResponses.push({
              title: $scope.getErrorTitle(errorField),
              description: null,
            });
          });
        }

        if (pricingScriptId == null) {
          failedResponses.push({
            title: $scope.getErrorTitle('pricing-script'),
            description: null,
          });
        }

        if (_.size(failedResponses) > 0) {
          showErrorsDialog(false);
          return false;
        }

        return true;
      }
    }

    function discardChanges($event) {
      if (isPageSaved) {
        gotoShipperListPage();
      } else {
        nvDialog.confirmDelete($event, {
          title: nvTranslate.instant('container.shippers.back-to-shipper-list'),
          content: nvTranslate.instant('container.zonal-routing.discard-changes-description'),
          ok: nvTranslate.instant('commons.leave'),
        }).then(gotoShipperListPage);
      }
    }

    function onTabChanged(mode, force) {
      if (!ctrl.state.loading || force) {
        ctrl.state.tab = mode;
      }
    }

    function gotoShipperListPage() {
      nvNavGuard.unlock();
      $state.go('container.shippers.list', { filter: filter });
    }

    function readPricingScript(scripts, basicSettings = true) {
      if (basicSettings === true) {
          // basic page
        ctrl.view.pricingScripts = {};
        ctrl.view.pricingScripts.selectedScript = null;
        ctrl.view.pricingScripts.options = extendScripts(scripts);
      } else {
          // marketplace page
        ctrl.view.marketplacePricingScripts = {};
        ctrl.view.marketplacePricingScripts.selectedScript = null;
        ctrl.view.marketplacePricingScripts.options = extendScripts(scripts);
      }

      function extendScripts(datas) {
        const assArr = _.castArray(datas);
        return _.map(assArr, script => ({
          value: script,
          displayName: `${script.id} - ${script.name}`,
        }));
      }
    }

    function readIndustry(industries) {
      ctrl.view.industry = extendIndustry(industries);

      function extendIndustry(datas) {
        const asArray = _.castArray(datas);
        return _.map(asArray, el => ({
          id: el.id,
          displayName: el.name,
        }));
      }
    }

    function readBasicSettings() {
      ctrl.data.basic.status = nvModelUtils.fromBoolToYesNoBitString(false);
      ctrl.data.basic.shipperType = $scope.SHIPPER_TYPE[0];
      ctrl.data.basic.ocVersion = Shippers.ORDER_CREATE_TYPE.V4.id;
      ctrl.data.basic.trackingType = Shippers.TRACKING_TYPE.DYNAMIC.id;
      ctrl.data.basic.allowCod = nvModelUtils.fromBoolToYesNoBitString(false);
      ctrl.data.basic.allowCp = nvModelUtils.fromBoolToYesNoBitString(false);
      ctrl.data.basic.isPrePaid = nvModelUtils.fromBoolToYesNoBitString(false);
      ctrl.data.basic.allowStaging = nvModelUtils.fromBoolToYesNoBitString(false);
      ctrl.data.basic.isMultiParcel = nvModelUtils.fromBoolToYesNoBitString(false);
      ctrl.data.basic.disableReschedule = nvModelUtils.fromBoolToYesNoBitString(false);
      ctrl.data.basic.enforceParcelPickupTracking = nvModelUtils.fromBoolToYesNoBitString(false);
      ctrl.data.basic.isPrinterAvailable = nvModelUtils.fromBoolToYesNoBitString(false);
      ctrl.data.basic.deliveryOtpLength = $scope.DEFAULT_OTP_LENGTH;
      ctrl.data.basic.deliveryOtpValidationLimit = $scope.DEFAULT_OTP_ATTEMPTS;

      ctrl.data.basic.prefix = '';
      ctrl.data.basic.prefixes = [];

      ctrl.data.basic.serviceType = _.reduce(Shippers.SERVICE_TYPE, (result, row, key) => {
        result[key] = nvModelUtils.fromBoolToYesNoBitString(true);
        return result;
      }, {});

      ctrl.data.basic.serviceLevels = [];
      ctrl.data.basic.hasFreight = false;

      // used for extended prefixType validation
      ctrl.initialTrackingType = ctrl.data.basic.trackingType;
    }

    function readSalesPerson(salesPerson) {
      ctrl.view.salesPerson = {};
      ctrl.view.salesPerson.selectedSales = null;
      ctrl.view.salesPerson.options = extendSalesPerson(salesPerson);

      function extendSalesPerson(datas) {
        const asArray = _.castArray(datas);
        return _.map(asArray, el => ({
          value: el,
          displayName: `${el.code}-${el.name}`,
        }));
      }
    }

    function readMoreSettings() {
      ctrl.data.more.reservationDays = [];
      ctrl.data.more.isAutoReservation = nvModelUtils.fromBoolToYesNoBitString(false);
        // reservation ready time etc
      ctrl.data.more.readyTime = { hour: '00', minute: '00' };
      ctrl.data.more.latestTime = { hour: '00', minute: '00' };
      ctrl.data.more.cutoffTime = { hour: '00', minute: '00' };

      ctrl.data.more.customerReservation = nvModelUtils.fromBoolToYesNoBitString(false);

      ctrl.data.more.allowPremiumPickupOnSunday = nvModelUtils.fromBoolToYesNoBitString(false);
      ctrl.data.more.premiumPickupDailyLimit = 1;
      ctrl.data.more.pickupServiceTypeLevel = [{
        type: Shippers.PICKUP_SERVICE_TYPE.SCHEDULED.id,
        level: Shippers.PICKUP_SERVICE_LEVEL.STANDARD.id,
      }];

      ctrl.data.more.initialReturnState = true;
      ctrl.data.more.isReturnsEnabled = nvModelUtils.fromBoolToYesNoBitString(false);
      ctrl.data.more.returnsName = '';
      ctrl.data.more.returnsContact = '';
      ctrl.data.more.returnsEmail = '';
      ctrl.data.more.returnsAddress1 = '';
      ctrl.data.more.returnsAddress2 = '';
      ctrl.data.more.returnsCity = '';
      ctrl.data.more.returnsPostcode = '';
      ctrl.data.more.lastReturnsNumber = '';

      ctrl.data.more.isIntegratedVault = nvModelUtils.fromBoolToYesNoBitString(false);
      ctrl.data.more.isCollectCustomerNricCode = nvModelUtils.fromBoolToYesNoBitString(false);
      ctrl.data.more.isReturnsOnDpms = nvModelUtils.fromBoolToYesNoBitString(false);
      ctrl.data.more.isReturnsOnVault = nvModelUtils.fromBoolToYesNoBitString(false);
      ctrl.data.more.isReturnsOnShipperLite = nvModelUtils.fromBoolToYesNoBitString(false);

      ctrl.data.more.transitShipper = nvModelUtils.fromBoolToYesNoBitString(false);
      ctrl.data.more.transitCustomer = nvModelUtils.fromBoolToYesNoBitString(false);
      ctrl.data.more.completedShipper = nvModelUtils.fromBoolToYesNoBitString(false);
      ctrl.data.more.completedCustomer = nvModelUtils.fromBoolToYesNoBitString(false);
      ctrl.data.more.pickupFailShipper = nvModelUtils.fromBoolToYesNoBitString(false);
      ctrl.data.more.pickupFailCustomer = nvModelUtils.fromBoolToYesNoBitString(false);
      ctrl.data.more.deliveryFailShipper = nvModelUtils.fromBoolToYesNoBitString(false);
      ctrl.data.more.deliveryFailCustomer = nvModelUtils.fromBoolToYesNoBitString(false);
    }

    function readIntegrations() {
      ctrl.data.integrations.shopifyCodeFilter = nvModelUtils.fromBoolToYesNoBitString(false);
    }

    function readMarketplaceSettings() {
        // services section
      ctrl.data.marketplace.allowCod =
          nvModelUtils.fromBoolToYesNoBitString(false);
      ctrl.data.marketplace.allowCp =
          nvModelUtils.fromBoolToYesNoBitString(false);
      ctrl.data.marketplace.isPrePaid = nvModelUtils.fromBoolToYesNoBitString(false);
      ctrl.data.marketplace.allowStaging =
          nvModelUtils.fromBoolToYesNoBitString(false);
      ctrl.data.marketplace.isMultiParcel =
          nvModelUtils.fromBoolToYesNoBitString(false);
      ctrl.data.marketplace.reservationEnforceParcelPickupTracking =
          nvModelUtils.fromBoolToYesNoBitString(false);

      ctrl.data.marketplace.ocVersion = Shippers.ORDER_CREATE_TYPE.V4.id;
      ctrl.data.marketplace.trackingType = Shippers.TRACKING_TYPE.PREFIXLESS.id;

      // billing section
      ctrl.data.marketplace.readyTime = { hour: '00', minute: '00' };
      ctrl.data.marketplace.latestTime = { hour: '00', minute: '00' };
      ctrl.data.marketplace.cutoffTime = { hour: '00', minute: '00' };
      ctrl.data.marketplace.isAutoReservation = nvModelUtils.fromBoolToYesNoBitString(false);
      ctrl.data.marketplace.customerReservation = nvModelUtils.fromBoolToYesNoBitString(false);

      // default value for pickup type and level
      ctrl.data.marketplace.pickupServiceTypeLevel = [{
        type: Shippers.PICKUP_SERVICE_TYPE.SCHEDULED.id,
        level: Shippers.PICKUP_SERVICE_LEVEL.STANDARD.id,
      }];
    }

    function setPageLoading(b) {
      ctrl.state.loading = b;
    }

    function showLoadingSheet(b) {
      ctrl.state.laodingSheet = b;
    }

    function addReservationAddress($event) {
      showAddressDialog($event, 'create');
    }

    function editReservationAddress($event, addressData, focusMilkrunReservationsTab = false) {
      showAddressDialog($event, 'edit', addressData, focusMilkrunReservationsTab);
    }

    function showAddressDialog($event, mode, addressData, focusMilkrunReservationsTab = false) {
      const theme = mode === 'edit' ? 'nvBlue' : 'nvGreen';
      const address = addressData ? addressData.value : null;

      nvDialog.showSingle($event, {
        templateUrl: 'views/container/shippers/shipper-create/dialog/address/shipper-create-address.dialog.html',
        theme: theme,
        cssClass: 'nv-container-shipper-create-dialog',
        controller: 'ShipperCreateAddressDialogController',
        controllerAs: 'ctrl',
        locals: {
          localData: {
            theme: theme,
            mode: mode,
            shipperId: ctrl.createdShipperCache.shipperId,
            addressData: address,
            RESERVATIONS_DAY: $scope.RESERVATIONS_DAY,
            constructMilkrunSettings: $scope.constructMilkrunSettings,
            findTimewindowIdByStartTime: $scope.findTimewindowIdByStartTime,
            focusMilkrunReservationsTab: focusMilkrunReservationsTab,
          },
        },
      }).then(onSet);

      function onSet(result) {
        const action = result.action;
        const data = result.data;

        if (action === 'edit') {
          updateAddress(data);
        } else if (action === 'create') {
          addAddress(data);
        } else {
            // delete
          removeAddress(data);
        }
      }
    }

    function readAddresses() {
      const tableContent = [];
      ctrl.view.addressTableParam = nvTable.createTable($scope.ADDRESS_FIELDS);
      ctrl.view.addressTableParam.setData(tableContent);
      ctrl.view.addressTableParam.setHighlightFn(isHighlighted);

      function isHighlighted(address) {
        return {
          class: '',
          highlighted: address.isDefault,
        };
      }
    }

    function buildAddressRow(address, defaultId) {
      const isDefault = address.id === defaultId;
      const addresses = [
        address.address1,
        address.address2,
        address.neighbourhood,
        address.locality,
        address.region,
        address.country,
        address.postcode,
      ];
      return {
        id: address.id,
        contact: address.contact,
        address: _(addresses).compact().join(', '),
        value: address,
        isDefault: isDefault,
        action: isDefault === true ? 'Default' : 'Set as Default',
      };
    }

    function setAddressAsDefault(address) {
      if (address.isDefault === true) {
        return;
      }
      const addresses = ctrl.view.addressTableParam.getTableData();
      const oldDefault = _.find(addresses, addr => addr.isDefault === true);
      if (oldDefault) {
        _.assign(oldDefault, { isDefault: false, action: 'Set as Default' });
      }
      _.assign(address, { isDefault: true, action: 'Default' });
      ctrl.data.more.defaultAddressId = address.id;
    }

    function setAddressAsMilkrun($event, address) {
      if (address.value.is_milk_run) {
        // set to false - show confirm delete warning
        nvDialog.confirmDelete($event, {
          title: nvTranslate.instant('container.shippers.remove-milkrun'),
          content: nvTranslate.instant('container.shippers.remove-milkrun-description'),
        }).then(milkrunOnDeleting);
      } else {
        // set to true - open edit address dialog
        editReservationAddress($event, address, true);
      }

      function milkrunOnDeleting() {
        address.value.is_milk_run = false;
        address.value.milkrun_settings = [];
      }
    }

    function addressButtonClass(bool, defaultClass) {
      return nvUtils.toThemeClass(bool ? 'nvBlue' : defaultClass);
    }

    function addAddress(address) {
      const newAddress = buildAddressRow(address, null);
      ctrl.view.addressTableParam.mergeIn(newAddress, 'id');
    }

    function updateAddress(address) {
      const newAddress = buildAddressRow(address, ctrl.data.more.defaultAddressId);
      ctrl.view.addressTableParam.mergeIn(newAddress, 'id');
    }

    function removeAddress(address) {
      ctrl.view.addressTableParam.remove(address, 'id');
    }

    function replaceWithLiaisonDetails() {
      ctrl.data.more.returnsName = ctrl.data.basic.liaisonName;
      ctrl.data.more.returnsContact = ctrl.data.basic.liaisonContact;
      ctrl.data.more.returnsEmail = ctrl.data.basic.liaisonEmail;
      ctrl.data.more.returnsAddress1 = ctrl.data.basic.liaisonAddress;
      ctrl.data.more.returnsAddress2 = null;
      ctrl.data.more.returnsCity = null;
      ctrl.data.more.returnsPostcode = ctrl.data.basic.liaisonPostcode;
    }

    function setupListener() {
      $scope.$watch('ctrl.data.basic.shipperType', (val, oldVal) =>
          $scope.evaluateShipperType(val, oldVal, ctrl.data)
      );
      $scope.$watch('ctrl.data.basic.ocVersion', (val, oldVal) =>
          $scope.evaluateOcVersion(val, oldVal, ctrl.data, ctrl.initialTrackingType)
      );
      $scope.$watch('ctrl.data.basic.trackingType', (val, oldVal) =>
          $scope.evaluateTrackingType(
            val,
            oldVal,
            ctrl.data,
            null,
            ctrl.basicForm
          )
      );
      $scope.$watch('ctrl.data.marketplace.ocVersion', (val, oldVal) =>
          $scope.evaluateMarketplaceOcVersion(val, oldVal, ctrl.data)
      );
    }
  }
}());
