(function controller() {
  angular
    .module('nvOperatorApp.controllers')
    .controller('ShippersListController', ShippersListController);

  ShippersListController.$inject = [
    '$q', 'Shippers', 'Dash', 'Industry', 'nvTable',
    'nvDialog', 'nvTranslate', '$state',
    '$stateParams',
  ];

  function ShippersListController(
    $q, Shippers, Dash, Industry, nvTable,
    nvDialog, nvTranslate, $state,
    $stateParams
  ) {
    // ////////////////////////////////////////////////
    // variables
    // ////////////////////////////////////////////////
    let industriesMap = {};
    let shipperWithNinjaDash = [];

    const ctrl = this;

    ctrl.tableParam = nvTable.createTable(Shippers.FIELDS);
    ctrl.tableParam.addColumn('_industry', { displayName: 'container.shippers.industry' });
    ctrl.tableParam.addColumn('_status', { displayName: 'commons.status' });
    ctrl.tableFilter = {};
    ctrl.isSaasEnv = _.includes(['saas', 'demo'], nv.config.env);

    // ////////////////////////////////////////////////
    // functions
    // ////////////////////////////////////////////////
    ctrl.getAll = getAll;
    ctrl.loginShipper = loginShipper;
    ctrl.loginNinjaDash = loginNinjaDash;
    ctrl.showBillingDialog = showBillingDialog;
    ctrl.editShipper = editShipper;
    ctrl.openCreateShipper = openCreateShipper;

    // ////////////////////////////////////////////////
    // functions details
    // ////////////////////////////////////////////////
    function getAll() {
      _.forEach(ctrl.tableParam.columns, (val, key) => {
        ctrl.tableParam.updateColumnFilter(key, strFn => (
          (row) => {
            const lowercase = (strFn() || '').toLowerCase();
            const data = _.get(row, key);
            ctrl.tableFilter[key] = lowercase;
            return lowercase.length === 0 ||
              _.toString(data).toLowerCase().indexOf(lowercase) > -1;
          }
        ));
      });

      // apply prefilter
      if ($stateParams.filter) {
        ctrl.tableParam.prefilter($stateParams.filter);
      }

      const promises = [Shippers.elasticReadAll(), Industry.read()];
      if (ctrl.isSaasEnv) {
        promises.push($q.resolve([])); // simulate as empty array for saas instance
      } else {
        promises.push(Dash.getNinjaDashShippers());
      }
      return $q.all(promises).then(success, $q.reject);

      function success(response) {
        industriesMap = _.keyBy(response[1], 'id');
        shipperWithNinjaDash = _.map(response[2], r => (r.shipper_id));
        ctrl.tableParam.totalItems = response[0].length;
        ctrl.tableParam.setData(extend(response[0]));
      }
    }

    function loginNinjaDash(shipper) {
      return Dash.createNinjaDashToken(shipper.global_id).then(onSuccessCreateToken);

      function onSuccessCreateToken(token) {
        Dash.loginDash(token.access_token, shipper.global_id);
      }
    }

    function loginShipper(shipper) {
      Shippers.loginToSd(shipper.id, shipper.email, window.location.host);
    }

    function showBillingDialog($event, shipper) {
      nvDialog.showSingle($event, {
        templateUrl: 'views/container/shippers/dialog/shipper-billing/shipper-billing.dialog.html',
        cssClass: 'shippers-list-shipper-billing',
        controller: 'ShipperBillingDialogController',
        controllerAs: 'ctrl',
        locals: {
          selectedShipper: shipper,
          $scope: 'a',
        },
      });
    }

    function openCreateShipper() {
      $state.go('container.shippers.create', { filter: getFilter() });
      function getFilter() {
        return ctrl.tableFilter;
      }
    }

    function editShipper($event, shipper) {
      $state.go('container.shippers.edit', { shipperId: shipper.id, filter: getFilter() });

      function getFilter() {
        return ctrl.tableFilter;
      }
    }

    function extend(shippers) {
      return _.map(shippers, shipper => (
        setCustomShipperData(shipper)
      ));
    }

    function setCustomShipperData(shipper) {
      shipper._industry = getIndustryName(shipper.industry_id);
      shipper._hasNinjaDashAccount = _.includes(shipperWithNinjaDash, shipper.global_id);
      shipper._status = nvTranslate.instant('container.shippers.active');
      if (!shipper.active) {
        shipper._status = nvTranslate.instant('container.shippers.inactive');
      }

      return shipper;
    }

    function getIndustryName(id) {
      if (industriesMap[id]) {
        return industriesMap[id].name;
      }

      return null;
    }
  }
}());
