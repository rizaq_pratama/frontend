(function controller() {
  angular
    .module('nvOperatorApp.controllers')
    .controller('ShipperEditController', ShipperEditController);

  ShipperEditController.$inject = [
    '$q', '$stateParams', '$scope', '$state', '$timeout',
    'Shippers', 'Industry', 'PricingScriptsV2', 'nvTable', 'Address',
    'appSidenav', 'nvNavGuard', 'nvDialog', 'nvTranslate', 'nvAutocomplete.Data',
    'nvToast', 'nvModelUtils', 'Timewindow', 'nvRequestUtils', 'nvUtils',
  ];

  const TAB_ITEMS = [
    { id: 0, displayName: 'container.shippers.tab-basic-settings' },
    { id: 1, displayName: 'container.shippers.tab-more-settings' },
    { id: 2, displayName: 'container.shippers.tab-integration-settings' },
    { id: 3, displayName: 'container.shippers.tab-marketplace-settings' },
    { id: 4, displayName: 'container.shippers.tab-marketplace-seller-subshipper' },
  ];

  const MARKETPLACE_SELLER_FIELDS = {
    id: { displayName: 'container.shippers.marketplace-sellers.table-shipper-id' },
    shortName: { displayName: 'container.shippers.marketplace-sellers.table-short-name' },
    name: { displayName: 'container.shippers.marketplace-sellers.table-shipper-name' },
    externalRef: { displayName: 'container.shippers.marketplace-sellers.table-external-ref' },
    isActive: { displayName: 'container.shippers.marketplace-sellers.table-active' },
  };

  function ShipperEditController(
    $q, $stateParams, $scope, $state, $timeout,
    Shippers, Industry, PricingScriptsV2, nvTable, Address,
    appSidenav, nvNavGuard, nvDialog, nvTranslate, nvAutocompleteData,
    nvToast, nvModelUtils, Timewindow, nvRequestUtils, nvUtils
  ) {
    const ctrl = this;
    // this one is using legacy
    const legacyShipperId = $stateParams.shipperId;
    const filter = $stateParams.filter;

    let globalShipperId = 0;
    let isPageSaved = false;

    const REQUIRED_NAMESPACES = [
      Shippers.namespaces.orderCreate,
      Shippers.namespaces.distributionPoints,
      Shippers.namespaces.pickup,
      Shippers.namespaces.notification,
      Shippers.namespaces.returns,
      Shippers.namespaces.reservation,
      Shippers.namespaces.labelPrinter,
      Shippers.namespaces.marketplaceBilling,
      Shippers.namespaces.marketplaceDefault,
      Shippers.namespaces.shopify,
      Shippers.namespaces.qoo10,
      Shippers.namespaces.magento,
      Shippers.namespaces.delivery,
    ];

    // global variable
    ctrl.Shippers = Shippers;
    ctrl.FORM_VALIDATION = $scope.FORM_VALIDATION;
    ctrl.initialTrackingType = Shippers.TRACKING_TYPE.PREFIXLESS.id;
    ctrl.marketplaceShippers = null;
    ctrl.marketplaceId = null;
    ctrl.originalShipper = null;
    ctrl.originalSettings = null;
    ctrl.originalPricingScriptId = null;
    ctrl.legacyShipperId = legacyShipperId;
    ctrl.ninjaPrefix = $scope.NINJA_PACK_PREFIX;

    ctrl.state = {};

    ctrl.data = {
      basic: {},
      more: {},
      integrations: {},
      marketplace: {},
    };

    ctrl.view = {
      tabs: TAB_ITEMS,
      timeslots: _.cloneDeep($scope.TIMESLOTS),
      serviceLevelOptions: Shippers.serviceLevelToOptions(),
    };

    ctrl.function = {
      init: init,
      discardChanges: discardChanges,
      saveChanges: saveChanges,
      onTabChanged: onTabChanged,
      addNewPrefix: $scope.addNewPrefix,
      deletePrefix: $scope.deletePrefix,
      setPrefixAsDefault: $scope.setPrefixAsDefault,
      isDeletePrefixDisabled: $scope.isDeletePrefixDisabled,
      isPrefixesEnabled: $scope.isPrefixesEnabled,
      generatePrefixesInputField: $scope.generatePrefixesInputField,
      isAbleToAddNewPickupService: $scope.isAbleToAddNewPickupService,
      addNewPickupService: $scope.addNewPickupService,
      deletePickupService: $scope.deletePickupService,
      isAbleToAddNewMarketplacePickupService: $scope.isAbleToAddNewMarketplacePickupService,
      addNewMarketplacePickupService: $scope.addNewMarketplacePickupService,
      deleteMarketplacePickupService: $scope.deleteMarketplacePickupService,
      isServiceTypeRequired: $scope.isServiceTypeRequired,
      addReservationAddress: addReservationAddress,
      editReservationAddress: editReservationAddress,
      isReturnsEnabled: $scope.isReturnsEnabled,
      setAddressAsDefault: setAddressAsDefault,
      setAddressAsMilkrun: setAddressAsMilkrun,
      addressButtonClass: addressButtonClass,
      replaceWithLiaisonDetails: replaceWithLiaisonDetails,
      isReservationEnabled: $scope.isReservationEnabled,
      isAutoReservationEnable: $scope.isAutoReservationEnable,
      isMarketplaceReservationEnabled: $scope.isMarketplaceReservationEnabled,
      isMarketplace: $scope.isMarketplace,
      isTrackingTypeDisabled: $scope.isTrackingTypeDisabled,
      isMarketplaceTrackingTypeDisabled: $scope.isMarketplaceTrackingTypeDisabled,
      isOcVersionDisabled: $scope.isOcVersionDisabled,
      isOrderCreateLocked: $scope.isOrderCreateLocked,
      checkShipperContactValidity: $scope.checkShipperContactValidity,
      checkPrefixAvailability: $scope.checkPrefixAvailability,
      nvModelUtils: nvModelUtils,
    };

    // start
    init();
    $scope.$on('$destroy', () => {
      appSidenav.setConfigLockedOpen(true);
    });

    function init() {
      setPageLoading(true);
      onTabChanged(0, true);
      nvNavGuard.lock();

      $timeout(() => {
        appSidenav.setConfigLockedOpen(false);
      });

      Shippers.readShipperLegacy(legacyShipperId).then((data) => {
        globalShipperId = data.id;
        $q.all([
          Shippers.readShipper(globalShipperId),
          Shippers.readAll({ is_marketplace: true }),
          Industry.read(),
          PricingScriptsV2.readAllActive(),
          Shippers.readSalesPerson(),
          Shippers.readMultipleSettingsV2(
            globalShipperId,
            _.map(REQUIRED_NAMESPACES, e => e.namespace)
          ),
          Shippers.readAddresses(globalShipperId),
          PricingScriptsV2.getScript(globalShipperId)]
        ).then((responses) => {
          const shipper = responses[0];
          const marketplaces = responses[1];
          const industries = responses[2];
          const scripts = responses[3];
          const salesPerson = responses[4];
          const settings = responses[5];
          const addresses = responses[6];
          const pricingScript = responses[7];

          ctrl.marketplaceShippers = marketplaces;
          ctrl.originalShipper = shipper;
          ctrl.originalSettings = _.omit(settings, 'pricing');
          ctrl.originalPricingScriptId = pricingScript && pricingScript.id;

          $scope.initView(ctrl.view);
          readSettings(settings);
          readSalesPerson(shipper, salesPerson);
          readPricingScript(scripts, ctrl.originalPricingScriptId);
          readIndustry(industries);
          readAddresses(addresses, settings);

          readBasicSettings(shipper, settings);
          readMoreSettings(shipper, settings);
          readIntegrations(shipper, settings);
          readMarketplaceSettings(shipper, settings);
          readMarketplaceConfig(shipper, marketplaces);

          setupListener();
          setPageLoading(false);

          initMarketplaceSellerPage();
        }, () => {
          // on failed bunch of call
          setPageLoading(false);
          gotoShipperListPage();
        });
      }, () => {
        // on failed calling legacy shipper
        setPageLoading(false);
        gotoShipperListPage();
      });
    }

    function saveChanges($event) {
      const failedResponses = [];

      showLoadingSheet(true);

      const shipper = $scope.constructShipper(
        globalShipperId, ctrl.view, ctrl.data, ctrl.originalShipper
      );
      let settings = $scope.constructSettings(ctrl.data, ctrl.originalSettings);
      const pricingScriptId = ctrl.view.pricingScripts.selectedScript
        ? ctrl.view.pricingScripts.selectedScript.id
        : ctrl.originalPricingScriptId;

      if (!$scope.isMarketplace(ctrl.data)) {
        settings = _.omit(settings, ['marketplace_default', 'marketplace_billing']);
      }
      if (!$scope.isReturnsEnabled(ctrl.data) && $scope.isReturnsInitiallyEnabled(ctrl.data)) {
        settings = _.omit(settings, ['returns']);
      }

      const isValid = validate();

      const endpointsMap = [
        $scope.getErrorTitle('shipper-details'),
        $scope.getErrorTitle('pricing-script'),
      ];
      if (isValid) {
        const calls = [
          Shippers.updateShipperV2(globalShipperId, shipper),
          PricingScriptsV2.linkShippers(pricingScriptId, _.castArray(globalShipperId), true),
        ];
        _.forIn(settings, (val, key) => {
          calls.push(Shippers.updateSpecificSettingV2(globalShipperId, val, key));
          endpointsMap.push($scope.getErrorTitle(key));
        });

        $q.allSettled(calls).then((responses) => {
          nvToast.clear();

          _.forEach(responses, (response, index) => {
            if (response.state === nvRequestUtils.PROMISE_STATE.REJECTED) {
              failedResponses.push({
                title: getErrorTitle(index),
                description: $scope.getErrorDescription(response.reason),
              });
            }
          });

          if (_.size(failedResponses) > 0) {
            nvToast.warning(nvTranslate.instant('container.shippers.some-changes-not-saved-successfully'));
            showErrorsDialog();
          } else {
            isPageSaved = true;
            nvToast.success(nvTranslate.instant('container.shippers.all-changes-saved-successfully'));
          }
        }).finally(() => {
          showLoadingSheet(false);
        });
      } else {
        showLoadingSheet(false);
      }

      function getErrorTitle(index) {
        return endpointsMap[index];
      }

      function showErrorsDialog(isSubmitted = true) {
        const payload = {
          errors: failedResponses,
        };

        if (!isSubmitted) {
          payload.messages = {
            description: 'container.shippers.x-validation-errors',
          };
        }

        nvDialog.showSingle($event, {
          templateUrl: 'views/container/shippers/dialog/errors/errors.dialog.html',
          cssClass: 'nv-container-shipper-errors-dialog',
          controller: 'ShipperErrorsDialogController',
          controllerAs: 'ctrl',
          locals: {
            payload: payload,
          },
        });
      }

      function setFormTouched(form) {
        _.each(form, (field) => {
          if (_.isObject(field) && !_.isUndefined(field.$validate)) {
            field.$setTouched();
            field.$validate();
          }
        });
      }

      function validate() {
        setFormTouched(ctrl.basicForm);
        setFormTouched(ctrl.integrationForm);
        setFormTouched(ctrl.marketplaceForm);
        setFormTouched(ctrl.moreForm);
        if (!(ctrl.basicForm.$valid && ctrl.integrationForm.$valid
          && ctrl.marketplaceForm.$valid && ctrl.moreForm.$valid)) {
          let errorFields = [];
          _.forEach([
            ctrl.basicForm,
            ctrl.integrationForm,
            ctrl.marketplaceForm,
            ctrl.moreForm,
          ], (form) => {
            _.forEach(form.$error, (error) => {
              errorFields = _.concat(errorFields, _.map(error, '$name'));
            });
          });

          errorFields = _.uniq(errorFields);
          _.forEach(errorFields, (errorField) => {
            failedResponses.push({
              title: $scope.getErrorTitle(errorField),
              description: null,
            });
          });
        }

        if (pricingScriptId == null) {
          failedResponses.push({
            title: $scope.getErrorTitle('pricing-script'),
            description: null,
          });
        }

        if (_.size(failedResponses) > 0) {
          showErrorsDialog(false);
          return false;
        }

        return true;
      }
    }

    function discardChanges($event) {
      if (isPageSaved) {
        gotoShipperListPage();
      } else {
        nvDialog.confirmDelete($event, {
          title: nvTranslate.instant('container.shippers.back-to-shipper-list'),
          content: nvTranslate.instant('container.zonal-routing.discard-changes-description'),
          ok: nvTranslate.instant('commons.leave'),
        }).then(gotoShipperListPage);
      }
    }

    function onTabChanged(mode, force) {
      if (!ctrl.state.loading || force) {
        ctrl.state.tab = mode;
      }
    }

    function gotoShipperListPage() {
      nvNavGuard.unlock();
      $state.go('container.shippers.list', { filter: filter });
    }

    function readMarketplaceConfig(shipper, marketplaces) {
      ctrl.view.marketplace = {};
      ctrl.view.marketplace.searchText = '';
      ctrl.view.marketplace.autocomplete = nvAutocompleteData.getByHandle('shipper-edit-marketplace');
      ctrl.view.marketplace.autocomplete.setPossibleOptions(extendMarketplace(marketplaces));
      ctrl.data.marketplace.selectedMarketplace = null;
      if (ctrl.view.shipperTypeCategory === $scope.SHIPPER_CATEGORY.MARKETPLACE_SELLER) {
        Shippers.readMarketplaces(shipper.global_id).then((parent) => {
          const selected = _.find(marketplaces, mp => mp.global_id === parent.id);
          if (selected) {
            ctrl.data.marketplace.selectedMarketplace = extendMarketplace(selected)[0];
          }
        });
      }

      function extendMarketplace(datas) {
        const asArray = _.castArray(datas);
        return _.map(asArray, el => ({
          value: el,
          displayName: `${el.id}-${el.short_name || el.name}`,
        }));
      }
    }

    function readPricingScript(scripts, scriptId, basicSettings = true) {
      if (basicSettings === true) {
        // basic page
        ctrl.view.pricingScripts = {};
        ctrl.view.pricingScripts.selectedScript = null;

        if (scriptId) {
          const ownedScript = _.find(scripts, script => script.id === scriptId);
          if (ownedScript) {
            ctrl.view.pricingScripts.selectedScript = _.get(extendScripts(ownedScript), '[0].value');
          }
        }
        ctrl.view.pricingScripts.options = extendScripts(scripts);
      } else {
        // marketplace page
        ctrl.view.marketplacePricingScripts = {};
        ctrl.view.marketplacePricingScripts.searchText = '';
        ctrl.view.marketplacePricingScripts.autocomplete = nvAutocompleteData.getByHandle('shipper-edit-scripts-marketplace');
        ctrl.view.marketplacePricingScripts.selectedScript = null;

        if (scriptId) {
          const ownedScript = _.find(scripts, script => script.id === scriptId);
          if (ownedScript) {
            ctrl.view.marketplacePricingScripts.selectedScript = extendScripts(ownedScript)[0];
          }
        }
        ctrl.view.marketplacePricingScripts.autocomplete.setPossibleOptions(extendScripts(scripts));
      }

      function extendScripts(datas) {
        const assArr = _.castArray(datas);
        return _.map(assArr, script => ({
          value: script,
          displayName: `${script.id} - ${script.name}`,
        }));
      }
    }

    function readIndustry(industries) {
      ctrl.view.industry = extendIndustry(industries);

      function extendIndustry(datas) {
        const asArray = _.castArray(datas);
        return _.map(asArray, el => ({
          id: el.id,
          displayName: el.name,
        }));
      }
    }

    function readBasicSettings(shipper, settings) {
      const ocSettings = settings.order_create;
      const dpSettings = settings.distribution_points;
      const reservationSettings = settings.reservation;
      const printerSettings = settings.label_printer;
      const deliverySettings = settings.delivery;

      ctrl.data.basic.status = nvModelUtils.fromBoolToYesNoBitString(shipper.active);
      ctrl.data.basic.name = shipper.name;
      ctrl.data.basic.shortName = shipper.short_name;
      ctrl.data.basic.contact = shipper.contact;
      ctrl.data.basic.email = shipper.email;

      ctrl.data.basic.liaisonName = shipper.liaison_name;
      ctrl.data.basic.liaisonContact = shipper.liaison_contact;
      ctrl.data.basic.liaisonEmail = shipper.liaison_email;
      ctrl.data.basic.liaisonAddress = shipper.liaison_address;
      ctrl.data.basic.liaisonPostcode = shipper.liaison_postcode;

      ctrl.data.basic.billingName = shipper.billing_name;
      ctrl.data.basic.billingContact = shipper.billing_contact;
      ctrl.data.basic.billingAddress = shipper.billing_address;
      ctrl.data.basic.billingPostcode = shipper.billing_postcode;

      ctrl.data.basic.shipperType = $scope.SHIPPER_TYPE[0];
      ctrl.view.shipperTypeCategory = $scope.SHIPPER_CATEGORY.NORMAL;
      if (ocSettings.is_marketplace === false && ocSettings.is_marketplace_seller === true) {
        // seller
        ctrl.view.shipperTypeCategory = $scope.SHIPPER_CATEGORY.MARKETPLACE_SELLER;
        ctrl.data.basic.shipperType = $scope.SHIPPER_TYPE[2];
      } else if (ocSettings.is_marketplace === true && ocSettings.is_marketplace_seller === false) {
        // marketplace
        ctrl.view.shipperTypeCategory = $scope.SHIPPER_CATEGORY.MARKETPLACE;
        ctrl.data.basic.shipperType = $scope.SHIPPER_TYPE[1];
      }
      ctrl.data.basic.shipperClassification = shipper.distribution_channel_id;
      ctrl.data.basic.industry = shipper.industry_id;
      ctrl.data.basic.accountType = shipper.account_type_id;

      // services section
      ctrl.data.basic.allowCod = nvModelUtils.fromBoolToYesNoBitString(
        ocSettings.allow_cod_service
      );
      ctrl.data.basic.allowCp = nvModelUtils.fromBoolToYesNoBitString(
        ocSettings.allow_cp_service
      );
      ctrl.data.basic.isPrePaid = nvModelUtils.fromBoolToYesNoBitString(
        ocSettings.is_pre_paid
      );
      ctrl.data.basic.allowStaging = nvModelUtils.fromBoolToYesNoBitString(
        ocSettings.allow_staged_orders
      );
      ctrl.data.basic.isMultiParcel = nvModelUtils.fromBoolToYesNoBitString(
        ocSettings.is_multi_parcel_shipper
      );
      ctrl.data.basic.disableReschedule = nvModelUtils.fromBoolToYesNoBitString(
        dpSettings.allow_driver_reschedule
      );
      ctrl.data.basic.enforceParcelPickupTracking = nvModelUtils.fromBoolToYesNoBitString(
        reservationSettings.enforce_parcel_pickup_tracking
      );

      ctrl.data.basic.ocVersion = ocSettings.version || Shippers.ORDER_CREATE_TYPE.V4.id;
      // remove v2 and v3 options and retain v2/v3 if it is the current oc version
      if (ocSettings.version === 'v3') {
        _.pullAllBy(ctrl.view.ocVersionOptions, [{ value: 'v2' }], 'value');
      } else if (ocSettings.version === 'v2') {
        _.pullAllBy(ctrl.view.ocVersionOptions, [{ value: 'v3' }], 'value');
      } else {
        _.pullAllBy(ctrl.view.ocVersionOptions, [{ value: 'v3' }, { value: 'v2' }], 'value');
      }

      ctrl.data.basic.trackingType = ocSettings.tracking_type || Shippers.TRACKING_TYPE.DYNAMIC.id;
      ctrl.data.basic.prefix = ocSettings.prefix;
      ctrl.data.basic.prefixes = ocSettings.prefixes || [];

      // determine printer settings
      ctrl.data.basic.isPrinterAvailable = nvModelUtils.fromBoolToYesNoBitString(
        printerSettings.show_shipper_details
      );
      ctrl.data.basic.printerIp = printerSettings.printer_ip;

      ctrl.data.basic.deliveryOtpLength = ocSettings.delivery_otp_length
        || $scope.DEFAULT_OTP_LENGTH;
      ctrl.data.basic.deliveryOtpValidationLimit = deliverySettings.delivery_otp_validation_limit
        || $scope.DEFAULT_OTP_ATTEMPTS;

      const serviceTypes = _.get(ocSettings, 'available_service_types');
      ctrl.data.basic.serviceType = _.reduce(Shippers.SERVICE_TYPE, (result, row, key) => {
        result[key] = nvModelUtils.fromBoolToYesNoBitString(
          $scope.isServiceTypeSupported(serviceTypes, row.id)
        );
        return result;
      }, {});

      ctrl.data.basic.serviceLevels = _.get(ocSettings, 'available_service_levels') || [];
      ctrl.data.basic.hasFreight = _.includes(
        _.get(ocSettings, 'services_available'),
        'FREIGHT',
      );

      // used for extended prefixType validation
      ctrl.initialTrackingType = ctrl.data.basic.trackingType;
    }

    function readSettings(settings) {
      settings.reservation = _.defaults(settings.reservation, {});
      settings.order_create = _.defaults(settings.order_create, {});
      settings.distribution_points = _.defaults(settings.distribution_points, {});
      settings.notification = _.defaults(settings.notification, {});
      settings.pricing = _.defaults(settings.pricing, {});
      settings.label_printer = _.defaults(settings.label_printer, {});
      settings.marketplace_billing = _.defaults(settings.marketplace_billing, {});
      settings.marketplace_default = _.defaults(settings.marketplace_default, {});
      settings.pickup = _.defaults(settings.pickup, {});

      // 3rd party configurations
      settings.qoo10 = _.defaults(settings.qoo10, {});
      settings.shopify = _.defaults(settings.shopify, {});
      settings.magento = _.defaults(settings.magento, {});
    }

    function readSalesPerson(shipper, salesPerson) {
      ctrl.view.salesPerson = {};
      ctrl.view.salesPerson.selectedSales = null;
      if (shipper.sales_person) {
        const salePerson = _.find(salesPerson, el => el.code === shipper.sales_person);
        if (salePerson) {
          ctrl.view.salesPerson.selectedSales = _.get(extendSalesPerson(salePerson), '[0].value');
        }
      }
      ctrl.view.salesPerson.options = extendSalesPerson(salesPerson);

      function extendSalesPerson(datas) {
        const asArray = _.castArray(datas);
        return _.map(asArray, el => ({
          value: el,
          displayName: `${el.code}-${el.name}`,
        }));
      }
    }

    function readMoreSettings(shipper, settings) {
      const reservationSettings = settings.reservation;
      const dpSettings = settings.distribution_points;
      const notificationSettings = settings.notification;
      const returnsSettings = settings.returns;
      const pickupSettings = settings.pickup;

      ctrl.data.more.reservationDays = reservationSettings.days || [];
      ctrl.data.more.isAutoReservation =
        nvModelUtils.fromBoolToYesNoBitString(reservationSettings.auto_reservation_enabled);
      // reservation ready time etc
      ctrl.data.more.readyTime = _.defaults(ctrl.data.more.readyTime, { hour: '00', minute: '00' });
      ctrl.data.more.latestTime = _.defaults(ctrl.data.more.latestTime, { hour: '00', minute: '00' });
      ctrl.data.more.cutoffTime = _.defaults(ctrl.data.more.cutoffTime, { hour: '00', minute: '00' });
      if (reservationSettings.auto_reservation_enabled === true) {
        const readyTime = _.split(reservationSettings.auto_reservation_ready_time, ':');
        const latestTime = _.split(reservationSettings.auto_reservation_latest_time, ':');
        const cutOffTime = _.split(reservationSettings.auto_reservation_cutoff_time, ':');

        ctrl.data.more.readyTime.hour = readyTime[0];
        ctrl.data.more.readyTime.minute = readyTime[1];
        ctrl.data.more.latestTime.hour = latestTime[0];
        ctrl.data.more.latestTime.minute = latestTime[1];
        ctrl.data.more.cutoffTime.hour = cutOffTime[0];
        ctrl.data.more.cutoffTime.minute = cutOffTime[1];
      }

      ctrl.data.more.defaultAddressId = reservationSettings.auto_reservation_address_id;
      ctrl.data.more.approxVolume = reservationSettings.auto_reservation_approx_volume;
      ctrl.data.more.allowedTypes = reservationSettings.allowed_types;
      ctrl.data.more.customerReservation = nvModelUtils.fromBoolToYesNoBitString(
        reservationSettings.allow_shipper_customer_reservation
      );

      ctrl.data.more.allowPremiumPickupOnSunday = nvModelUtils.fromBoolToYesNoBitString(
        pickupSettings.allow_premium_pickup_on_sunday
      );
      ctrl.data.more.premiumPickupDailyLimit = pickupSettings.premium_pickup_daily_limit || 1;
      ctrl.data.more.pickupServiceTypeLevel =
        pickupSettings.service_type_level && pickupSettings.service_type_level.length > 0 ?
        pickupSettings.service_type_level :
        [{
          type: Shippers.PICKUP_SERVICE_TYPE.SCHEDULED.id,
          level: Shippers.PICKUP_SERVICE_LEVEL.STANDARD.id,
        }];

      const selectedTimeslot = _.find(Timewindow.TIMEWINDOWS, tw =>
        tw.fromTime.format('HH:mm') === _.get(pickupSettings, 'default_start_time')
          && tw.toTime.format('HH:mm') === _.get(pickupSettings, 'default_end_time')
      );
      if (selectedTimeslot) {
        ctrl.data.more.defaultPickupTime = selectedTimeslot.id;
      }

      ctrl.data.more.initialReturnState = returnsSettings == null;
      ctrl.data.more.isReturnsEnabled = returnsSettings ?
        nvModelUtils.fromBoolToYesNoBitString(true) : nvModelUtils.fromBoolToYesNoBitString(false);
      ctrl.data.more.returnsName = returnsSettings ? returnsSettings.name : '';
      ctrl.data.more.returnsContact = returnsSettings ? returnsSettings.contact : '';
      ctrl.data.more.returnsEmail = returnsSettings ? returnsSettings.email : '';
      ctrl.data.more.returnsAddress1 = returnsSettings ? returnsSettings.address_1 : '';
      ctrl.data.more.returnsAddress2 = returnsSettings ? returnsSettings.address_2 : '';
      ctrl.data.more.returnsCity = returnsSettings ? returnsSettings.city : '';
      ctrl.data.more.returnsPostcode = returnsSettings ? returnsSettings.postcode : '';
      ctrl.data.more.lastReturnsNumber = returnsSettings ?
        returnsSettings.last_return_number : '';

      ctrl.data.more.isIntegratedVault = nvModelUtils.fromBoolToYesNoBitString(
        dpSettings.vault_is_integrated
      );
      ctrl.data.more.isCollectCustomerNricCode = nvModelUtils.fromBoolToYesNoBitString(
        dpSettings.vault_collect_customer_nric_code);
      ctrl.data.more.isReturnsOnDpms = nvModelUtils.fromBoolToYesNoBitString(
        dpSettings.allow_returns_on_dpms
      );
      ctrl.data.more.isReturnsOnVault = nvModelUtils.fromBoolToYesNoBitString(
        dpSettings.allow_returns_on_vault
      );
      ctrl.data.more.isReturnsOnShipperLite = nvModelUtils.fromBoolToYesNoBitString(
        dpSettings.allow_returns_on_shipper_lite
      );
      ctrl.data.more.dpmsLogoUrl = dpSettings.dmps_logo_url;
      ctrl.data.more.vaultLogoUrl = dpSettings.vault_logo_url;
      ctrl.data.more.shipperLiteLogoUrl = dpSettings.shipper_lite_logo_url;

      ctrl.data.more.transitShipper = nvModelUtils.fromBoolToYesNoBitString(
        notificationSettings.transit_shipper
      );
      ctrl.data.more.transitCustomer = nvModelUtils.fromBoolToYesNoBitString(
        notificationSettings.transit_customer
      );
      ctrl.data.more.completedShipper = nvModelUtils.fromBoolToYesNoBitString(
        notificationSettings.completed_shipper
      );
      ctrl.data.more.completedCustomer = nvModelUtils.fromBoolToYesNoBitString(
        notificationSettings.completed_customer
      );
      ctrl.data.more.pickupFailShipper = nvModelUtils.fromBoolToYesNoBitString(
        notificationSettings.pickup_fail_shipper
      );
      ctrl.data.more.pickupFailCustomer = nvModelUtils.fromBoolToYesNoBitString(
        notificationSettings.pickup_fail_customer
      );
      ctrl.data.more.deliveryFailShipper = nvModelUtils.fromBoolToYesNoBitString(
        notificationSettings.delivery_fail_shipper
      );
      ctrl.data.more.deliveryFailCustomer = nvModelUtils.fromBoolToYesNoBitString(
        notificationSettings.delivery_fail_customer
      );
    }

    function readIntegrations(shipper, settings) {
      const qooSettings = settings.qoo10;
      const shopifySettings = settings.shopify;
      const magentoSettings = settings.magento;

      ctrl.data.integrations.qooUsername = qooSettings.username;
      ctrl.data.integrations.qooPassword = qooSettings.password;

      ctrl.data.integrations.shopifyMaxDelivery = shopifySettings.max_delivery_days;
      ctrl.data.integrations.shopifyDdOffset = shopifySettings.dd_offset;
      ctrl.data.integrations.shopifyTimewindowId = shopifySettings.dd_timewindow_id;
      ctrl.data.integrations.shopifyBaseUrl = shopifySettings.base_uri;
      ctrl.data.integrations.shopifyApiKey = shopifySettings.api_key;
      ctrl.data.integrations.shopifyPassword = shopifySettings.password;
      ctrl.data.integrations.shopifyCode = arrayToComma(shopifySettings.shipping_codes);
      ctrl.data.integrations.shopifyCodeFilter =
        nvModelUtils.fromBoolToYesNoBitString(shopifySettings.is_shipping_code_filter_enabled);

      ctrl.data.integrations.magentoUsername = magentoSettings.username;
      ctrl.data.integrations.magentoPassword = magentoSettings.password;
      ctrl.data.integrations.magentoApiUrl = magentoSettings.soap_api_url;
    }

    function readMarketplaceSettings(shipper, settings) {
      const billingSettings = _.defaults(settings.marketplace_billing, {});
      const defaultSettings = _.defaults(settings.marketplace_default, {});

      // services section
      ctrl.data.marketplace.allowCod =
        nvModelUtils.fromBoolToYesNoBitString(defaultSettings.order_create_allow_cod_service);
      ctrl.data.marketplace.allowCp =
        nvModelUtils.fromBoolToYesNoBitString(defaultSettings.order_create_allow_cp_service);
      ctrl.data.marketplace.isPrePaid = nvModelUtils.fromBoolToYesNoBitString(
        defaultSettings.order_create_is_pre_paid
      );
      ctrl.data.marketplace.allowStaging =
        nvModelUtils.fromBoolToYesNoBitString(defaultSettings.order_create_allow_staged_orders);
      ctrl.data.marketplace.isMultiParcel =
        nvModelUtils.fromBoolToYesNoBitString(defaultSettings.order_create_is_multi_parcel_shipper);
      ctrl.data.marketplace.reservationEnforceParcelPickupTracking =
        nvModelUtils.fromBoolToYesNoBitString(
          defaultSettings.reservation_enforce_parcel_pickup_tracking
        );

      ctrl.data.marketplace.ocVersion =
        defaultSettings.order_create_version || Shippers.ORDER_CREATE_TYPE.V4.id;
      ctrl.data.marketplace.trackingType =
        defaultSettings.order_create_tracking_type || Shippers.TRACKING_TYPE.DYNAMIC.id;
      if (ctrl.data.marketplace.ocVersion === Shippers.ORDER_CREATE_TYPE.V4.id) {
        ctrl.data.marketplace.trackingType = Shippers.TRACKING_TYPE.PREFIXLESS.id;
      }

      ctrl.data.marketplace.selectedOcServices = defaultSettings.order_create_services_available;

      // billing section
      ctrl.data.marketplace.billingName = billingSettings.billing_name;
      ctrl.data.marketplace.billingContact = billingSettings.billing_contact;
      ctrl.data.marketplace.billingAddress = billingSettings.billing_address;
      ctrl.data.marketplace.billingPostcode = billingSettings.billing_postcode;

      ctrl.data.marketplace.readyTime = _.defaults(ctrl.data.marketplace.readyTime, { hour: '00', minute: '00' });
      ctrl.data.marketplace.latestTime = _.defaults(ctrl.data.marketplace.latestTime, { hour: '00', minute: '00' });
      ctrl.data.marketplace.cutoffTime = _.defaults(ctrl.data.marketplace.cutoffTime, { hour: '00', minute: '00' });

      ctrl.data.marketplace.reservationDays = defaultSettings.reservation_days;
      ctrl.data.marketplace.isAutoReservation =
        nvModelUtils.fromBoolToYesNoBitString(defaultSettings.auto_reservation_enabled);
      if (defaultSettings.auto_reservation_enabled === true) {
        const readyTime = _.split(defaultSettings.auto_reservation_ready_time, ':');
        const latestTime = _.split(defaultSettings.auto_reservation_latest_time, ':');
        const cutOffTime = _.split(defaultSettings.auto_reservation_cutoff_time, ':');

        ctrl.data.marketplace.readyTime.hour = readyTime[0];
        ctrl.data.marketplace.readyTime.minute = readyTime[1];
        ctrl.data.marketplace.latestTime.hour = latestTime[0];
        ctrl.data.marketplace.latestTime.minute = latestTime[1];
        ctrl.data.marketplace.cutoffTime.hour = cutOffTime[0];
        ctrl.data.marketplace.cutoffTime.minute = cutOffTime[1];
      }

      ctrl.data.marketplace.defaultAddressId = defaultSettings.auto_reservation_address_id;
      ctrl.data.marketplace.approxVolume = defaultSettings.auto_reservation_approx_volume;
      ctrl.data.marketplace.allowedTypes = defaultSettings.reservation_allowed_types;
      // todo ask
      ctrl.data.marketplace.customerReservation =
        nvModelUtils.fromBoolToYesNoBitString(defaultSettings.allow_shipper_customer_reservation);

      ctrl.data.marketplace.allowPremiumPickupOnSunday = nvModelUtils.fromBoolToYesNoBitString(
        defaultSettings.pickup_allow_premium_pickup_on_sunday
      );
      ctrl.data.marketplace.premiumPickupDailyLimit =
        defaultSettings.pickup_premium_pickup_daily_limit || 1;
      ctrl.data.marketplace.pickupServiceTypeLevel =
        (defaultSettings.pickup_services && defaultSettings.pickup_services.length > 0) ?
        defaultSettings.pickup_services :
        [{
          type: Shippers.PICKUP_SERVICE_TYPE.SCHEDULED.id,
          level: Shippers.PICKUP_SERVICE_LEVEL.STANDARD.id,
        }];

      const selectedTimeslot = _.find(Timewindow.TIMEWINDOWS, tw =>
        tw.fromTime.format('HH:mm') === _.get(defaultSettings, 'pickup_default_start_time')
          && tw.toTime.format('HH:mm') === _.get(defaultSettings, 'pickup_default_end_time')
      );
      if (selectedTimeslot) {
        ctrl.data.marketplace.defaultPickupTime = selectedTimeslot.id;
      }
    }

    function setPageLoading(b) {
      ctrl.state.loading = b;
    }

    function showLoadingSheet(b) {
      ctrl.state.laodingSheet = b;
    }

    function addReservationAddress($event) {
      showAddressDialog($event, 'create');
    }

    function editReservationAddress($event, addressData, focusMilkrunReservationsTab = false) {
      showAddressDialog($event, 'edit', addressData, focusMilkrunReservationsTab);
    }

    function showAddressDialog($event, mode, addressData, focusMilkrunReservationsTab = false) {
      const theme = mode === 'edit' ? 'nvBlue' : 'nvGreen';
      const address = addressData ? addressData.value : null;

      nvDialog.showSingle($event, {
        templateUrl: 'views/container/shippers/shipper-edit/dialog/address/shipper-edit-address.dialog.html',
        theme: theme,
        cssClass: 'nv-container-shipper-edit-dialog',
        controller: 'ShipperEditAddressDialogController',
        controllerAs: 'ctrl',
        locals: {
          localData: {
            shipperId: globalShipperId,
            theme: theme,
            mode: mode,
            addressData: address,
            RESERVATIONS_DAY: $scope.RESERVATIONS_DAY,
            constructMilkrunSettings: $scope.constructMilkrunSettings,
            findTimewindowIdByStartTime: $scope.findTimewindowIdByStartTime,
            focusMilkrunReservationsTab: focusMilkrunReservationsTab,
          },
        },
      }).then(onSet);

      function onSet(result) {
        const action = result.action;
        const data = result.data;

        if (action === 'edit') {
          updateAddress(data);
        } else if (action === 'create') {
          addAddress(data);
        } else {
          // delete
          removeAddress(data);
        }
      }
    }

    function readAddresses(addresses, settings) {
      const reservationSettings = settings.reservation;
      const defaultAddressId = reservationSettings.auto_reservation_address_id || null;
      const tableContent = _.map(addresses, el => buildAddressRow(el, defaultAddressId));

      ctrl.view.addressTableParam = nvTable.createTable($scope.ADDRESS_FIELDS);
      ctrl.view.addressTableParam.setData(tableContent);
      ctrl.view.addressTableParam.setHighlightFn(isHighlighted);

      function isHighlighted(address) {
        return {
          class: '',
          highlighted: address.isDefault,
        };
      }
    }

    function buildAddressRow(address, defaultId) {
      const isDefault = address.id === defaultId;
      const addresses = [
        address.address1,
        address.address2,
        address.neighbourhood,
        address.locality,
        address.region,
        address.country,
        address.postcode,
      ];
      return {
        id: address.id,
        contact: address.contact,
        address: _(addresses).compact().join(', '),
        value: address,
        isDefault: isDefault,
        action: isDefault === true ? 'Default' : 'Set as Default',
      };
    }

    function setAddressAsDefault(address) {
      if (address.isDefault === true) {
        return;
      }
      const addresses = ctrl.view.addressTableParam.getTableData();
      const oldDefault = _.find(addresses, addr => addr.isDefault === true);
      if (oldDefault) {
        _.assign(oldDefault, { isDefault: false, action: 'Set as Default' });
      }
      _.assign(address, { isDefault: true, action: 'Default' });
      ctrl.data.more.defaultAddressId = address.id;
    }

    function setAddressAsMilkrun($event, address) {
      if (address.value.is_milk_run) {
        // set to false - show confirm delete warning
        nvDialog.confirmDelete($event, {
          title: nvTranslate.instant('container.shippers.remove-milkrun'),
          content: nvTranslate.instant('container.shippers.remove-milkrun-description'),
        }).then(milkrunOnDeleting);
      } else {
        // set to true - open edit address dialog
        editReservationAddress($event, address, true);
      }

      function milkrunOnDeleting() {
        Shippers.updateAddress(globalShipperId, address.value.id, {
          is_milk_run: false,
        }).then(onSuccess);

        function onSuccess(response) {
          updateAddress(response);
        }
      }
    }

    function addressButtonClass(bool, defaultClass) {
      return nvUtils.toThemeClass(bool ? 'nvBlue' : defaultClass);
    }

    function addAddress(address) {
      const newAddress = buildAddressRow(address, null);
      ctrl.view.addressTableParam.mergeIn(newAddress, 'id');
    }

    function updateAddress(address) {
      const newAddress = buildAddressRow(address, ctrl.data.more.defaultAddressId);
      ctrl.view.addressTableParam.mergeIn(newAddress, 'id');
    }

    function removeAddress(address) {
      ctrl.view.addressTableParam.remove(address, 'id');
    }

    function replaceWithLiaisonDetails() {
      ctrl.data.more.returnsName = ctrl.data.basic.liaisonName;
      ctrl.data.more.returnsContact = ctrl.data.basic.liaisonContact;
      ctrl.data.more.returnsEmail = ctrl.data.basic.liaisonEmail;
      ctrl.data.more.returnsAddress1 = ctrl.data.basic.liaisonAddress;
      ctrl.data.more.returnsAddress2 = null;
      ctrl.data.more.returnsCity = null;
      ctrl.data.more.returnsPostcode = ctrl.data.basic.liaisonPostcode;
    }

    function setupListener() {
      $scope.$watch('ctrl.data.basic.shipperType', (val, oldVal) =>
        $scope.evaluateShipperType(val, oldVal, ctrl.data)
      );
      $scope.$watch('ctrl.data.basic.ocVersion', (val, oldVal) =>
        $scope.evaluateOcVersion(val, oldVal, ctrl.data, ctrl.initialTrackingType)
      );
      $scope.$watch('ctrl.data.basic.trackingType', (val, oldVal) =>
        $scope.evaluateTrackingType(
          val,
          oldVal,
          ctrl.data,
          legacyShipperId,
          ctrl.basicForm
        )
      );
      $scope.$watch('ctrl.data.marketplace.ocVersion', (val, oldVal) =>
          $scope.evaluateMarketplaceOcVersion(val, oldVal, ctrl.data)
      );
    }

    function arrayToComma(arr) {
      if (!arr) {
        return '';
      }
      return _.join(arr, ', ');
    }

    function initMarketplaceSellerPage() {
      if ($scope.isMarketplace(ctrl.data)) {
        ctrl.state.marketplaceSellerLoading = true;

        Shippers.elasticReadSellers(globalShipperId)
          .then(success)
          .finally(() => (ctrl.state.marketplaceSellerLoading = false));
      }

      function success(sellers) {
        initTable(sellers);
      }

      function initTable(sellers) {
        ctrl.data.marketplaceSellerTableParam = nvTable.createTable(MARKETPLACE_SELLER_FIELDS);
        ctrl.data.marketplaceSellerTableParam.setData(extendSellerData(sellers));
      }

      function extendSellerData(datas) {
        return _.map(datas, seller => _.assign({}, {
          id: seller.legacy_id,
          shortName: seller.short_name,
          name: seller.name,
          externalRef: seller.external_ref,
          isActive: seller.active ? 'Active' : 'Inactive',
        }));
      }
    }
  }
}());
