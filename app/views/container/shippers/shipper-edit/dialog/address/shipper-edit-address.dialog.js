(function controller() {
  angular
    .module('nvOperatorApp.controllers')
    .controller('ShipperEditAddressDialogController', ShipperEditAddressDialogController);

  ShipperEditAddressDialogController.$inject = [
    'localData', '$mdDialog', 'Shippers', 'nvTranslate', 'Timewindow',
    '$timeout',
  ];

  const API_STATE = {
    IDLE: 'idle',
    WAITING: 'waiting',
  };
  const TAB = {
    ADDRESS: 0,
    MILKRUN_RESERVATION: 1,
  };

  function ShipperEditAddressDialogController(
    localData, $mdDialog, Shippers, nvTranslate, Timewindow,
    $timeout
  ) {
    const ctrl = this;
    ctrl.shipperId = localData.shipperId;
    ctrl.mode = localData.mode;
    ctrl.theme = localData.theme;
    ctrl.RESERVATIONS_DAY = localData.RESERVATIONS_DAY;

    ctrl.TAB = TAB;
    ctrl.state = {};
    ctrl.title = '';
    ctrl.data = Shippers.mapToLocalizedAddress(localData.addressData);
    ctrl.function = {};

    ctrl.state.isAddressFinder = false;

    ctrl.function.onCancel = onCancel;
    ctrl.function.onDelete = onDelete;
    ctrl.function.onSubmit = onSubmit;
    ctrl.function.isAbleToSubmit = isAbleToSubmit;
    ctrl.function.addNewMilkrunSetting = addNewMilkrunSetting;
    ctrl.function.isRemoveMilkrunSettingDisabled = isRemoveMilkrunSettingDisabled;
    ctrl.function.removeMilkrunSetting = removeMilkrunSetting;
    ctrl.function.onAddressFinderCallback = onAddressFinderCallback;

    init();
    function init() {
      ctrl.title = ctrl.mode === 'edit' ? nvTranslate.instant('container.addressing.edit-address') : nvTranslate.instant('container.addressing.add-address');

      ctrl.data.isMilkrun = _.get(localData.addressData, 'is_milk_run') || false;
      if (ctrl.mode === 'edit' && _.includes([
        null,
        _.noop(),
      ], _.get(localData.addressData, 'milkrun_settings'))) {
        ctrl.contentLoading = true;
        Shippers.readMilkrunSettings(
          ctrl.shipperId, ctrl.data.id, { is_grouped: true }
        )
          .then(readMilkrunSuccess);
      } else {
        initMilkrunData(_.get(localData.addressData, 'milkrun_settings'));
      }

      ctrl.state = {
        apiDelete: API_STATE.IDLE,
        apiSubmit: API_STATE.IDLE,
        pickupCountry: Shippers.SUPPORTED_PICKUP_COUNTRY,
        pickupCountryOptions: Shippers.pickupCountryToOptions(),
        selectedTab: null,
        timeslotOptions: Timewindow.toOptions([
          Timewindow.TYPE.DAYSLOT,
          Timewindow.TYPE.DAYNIGHTSLOT,
          Timewindow.TYPE.TIMESLOT,
        ]),
        focusMilkrunReservationsTab: false,
      };

      function readMilkrunSuccess(response) {
        ctrl.contentLoading = false;
        initMilkrunData(response);
      }

      function initMilkrunData(milkrunData) {
        ctrl.data.milkrunSettings = _.map(
          _.cloneDeep(milkrunData), generateMilkrunSettingPayload
        ) || [];

        if (localData.focusMilkrunReservationsTab) {
          $timeout(() => {
            ctrl.state.focusMilkrunReservationsTab = localData.focusMilkrunReservationsTab;
          }, 1000);
        }
      }
    }

    function onCancel() {
      $mdDialog.hide(0);
    }

    function onDelete() {
      ctrl.state.apiDelete = API_STATE.WAITING;

      Shippers.deleteAddress(ctrl.shipperId, ctrl.data.id)
        .then(onSuccess)
        .finally(() => (ctrl.state.apiDelete = API_STATE.IDLE));

      function onSuccess() {
        actionSuccess({ action: 'delete', data: ctrl.data });
      }
    }

    function onSubmit() {
      ctrl.state.apiSubmit = API_STATE.WAITING;
      const internalAddressData = Shippers.mapToInternalAddress(ctrl.data);

      _.assign(internalAddressData, localData.constructMilkrunSettings(ctrl.data));

      if (ctrl.mode === 'edit') {
        Shippers.updateAddress(ctrl.shipperId, internalAddressData.id, internalAddressData)
          .then(onSuccess, onFailure);
      } else {
        // call create
        Shippers.createAddress(ctrl.shipperId, internalAddressData)
          .then(onSuccess, onFailure);
      }

      function onSuccess(address) {
        ctrl.state.apiSubmit = API_STATE.IDLE;
        actionSuccess({ action: ctrl.mode, data: address });
      }

      function onFailure() {
        ctrl.state.apiSubmit = API_STATE.IDLE;
      }
    }

    function isAbleToSubmit() {
      return !ctrl.state.isAddressFinder;
    }

    function addNewMilkrunSetting() {
      ctrl.data.milkrunSettings.push(generateMilkrunSettingPayload());
    }

    function isRemoveMilkrunSettingDisabled() {
      return ctrl.mode === 'edit' &&
        ctrl.data.isMilkrun && _.size(ctrl.data.milkrunSettings) <= 1;
    }

    function removeMilkrunSetting(index) {
      ctrl.data.milkrunSettings.splice(index, 1);
    }

    function generateMilkrunSettingPayload(milkrunSetting = {}) {
      return {
        timewindowId: localData.findTimewindowIdByStartTime(
          milkrunSetting.start_time, milkrunSetting.end_time
        ),
        days: milkrunSetting.days || [],
        noOfReservations: 1,
      };
    }

    function actionSuccess(data) {
      $mdDialog.hide(data);
    }

    function onAddressFinderCallback(address) {
      ctrl.state.isAddressFinder = false;
      if (address) {        
        _.assign(ctrl.data, _.omit(address, 'country'));
        // API accept country code as country value
        // address finder return country name
        const selectedCountry = _.find(ctrl.state.pickupCountryOptions, (country) => {
          return _.lowerCase(country.displayName) === _.lowerCase(address.country);
        });
        ctrl.data.country = selectedCountry.value;
      }
    }
  }
}());
