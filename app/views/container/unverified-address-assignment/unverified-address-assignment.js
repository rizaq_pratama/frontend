(function UnverifiedAddressAssignment() {
  angular.module('nvOperatorApp.controllers')
    .controller('UnverifiedAddressAssignmentController', UnverifiedAddressAssignmentCtrl);

  UnverifiedAddressAssignmentCtrl.$inject = [
    '$q',
    'Zone',
    'nvTranslate',
    'nvTable',
    'nvToast',
    'nvBackendFilterUtils',
    'UnverifiedAddress',
  ];


  function UnverifiedAddressAssignmentCtrl(
    $q, Zone, nvTranslate, nvTable, nvToast,
    nvBackendFilterUtils, UnverifiedAddress
  ) {
    const ctrl = this;
    ctrl.fields = {
      score: {
        displayName: nvTranslate.instant('container.unverified-address-assignment.score'),
      },
      granularStatus: {
        displayName: nvTranslate.instant('container.unverified-address-assignment.granular-status'),
      },
      shipperName: {
        displayName: nvTranslate.instant('container.unverified-address-assignment.shipper'),
      },
      address: {
        displayName: nvTranslate.instant('container.unverified-address-assignment.address'),
      },

    };

    // table
    ctrl.tableParam = {};
    ctrl.showPage = 'filter';
    ctrl.state = 'idle';
    ctrl.totalSize = 0;
    ctrl.zones = [];
    ctrl.zonesOptions = [];
    ctrl.grouped = [];
    ctrl.selectedZone = null;
    ctrl.failedData = [];
    // filters
    ctrl.selectedFilters = [];
    ctrl.possibleFilters = [];

    // function definitions
    ctrl.onLoad = onLoad;
    ctrl.loadData = loadData;
    ctrl.openFilterPage = openFilterPage;
    ctrl.isAbleToChange = false;
    ctrl.assign = assign;
    ctrl.downloadErrorAssignmentCsv = downloadErrorAssignmentCsv;


    ctrl.csvFileName = 'error-address-verification.csv';
    ctrl.csvHeader = 'Score, Granular Status, Shipper, Address';


    function openFilterPage() {
      ctrl.showPage = 'filter';
    }

    function onSelectItem() {
      const selection = ctrl.tableParam.getSelection();
      ctrl.isAbleToChange = selection ? selection.length > 0 : false;
    }

    function onDeselectItem() {
      const selection = ctrl.tableParam.getSelection();
      ctrl.isAbleToChange = selection ? selection.length > 0 : false;
    }


    function onLoad() {

      return $q.all([
        Zone.read(),
      ]).then(success);
      function success(results) {
        ctrl.zones = _.sortBy(results[0], zone => (zone.name));
        ctrl.zonesOptions = Zone.toOptions(ctrl.zones);

        ctrl.possibleFilters = UnverifiedAddress.getUnverifiedAddressFilter({
          zones: ctrl.zones,
        });
      }
    }


    function loadData() {
      ctrl.tableParam = nvTable.createTable(ctrl.fields, {
        onSelectItem: onSelectItem,
        onDeselectItem: onDeselectItem,
      });
      ctrl.state = 'waiting';
      return UnverifiedAddress.search(constructParams())
        .then(success, error);

      function success(result) {
        ctrl.state = 'idle';
        const tableData = _.map(result.results, data => ({
          score: data.score,
          waypointId: data.waypoint.id,
          address: concatAddress(data.waypoint),
          zone: data.zone,
          shipperName: data.shipper.name || '-',
          granularStatus: data.granular_status,
        }));
        ctrl.grouped = _.groupBy(tableData, 'zone.id');
        ctrl.totalSize = result.count;
        ctrl.tableParam.setData(tableData);
        ctrl.showPage = 'result';
      }

      function error() {
        ctrl.state = 'idle';
      }
    }

    function assign() {
      ctrl.state = 'waiting';
      const addresses = ctrl.tableParam.getSelection();
      const wps = _.map(addresses, addr => (addr.waypointId));
      return UnverifiedAddress.preAddressVerification(ctrl.selectedZone, wps)
      .then((result) => {
        ctrl.state = 'idle';
        ctrl.failedData = [];
        // check for result.failed
        if (result.failed && result.failed.length > 0) {
          if (wps.length === result.failed.length) {
            // complete error
            return nvToast.warning(nvTranslate.instant('container.unverified-address-assignment.no-address-were-assigned'));
          }
            // partial error
          ctrl.failedData = result.failed;
          nvToast.error(
              nvTranslate.instant('commons.error'),
              nvTranslate.instant('container.unverified-address-assignment.some-address-were-not-assigned'),
            {
              buttonTitle: 'Download CSV',
              buttonCallback: ctrl.downloadErrorAssignmentCsv,
              timeout: 13000,
            });
          return clearSelection();
        }
        nvToast.success(nvTranslate.instant('container.unverified-address-assignment.x-addresses-assigned', { x: result.success.length }));
        return clearSelection();


        function clearSelection() {
          const updated = [];
          _.each(result.success, (wpId) => {
            const addr = _.find(ctrl.tableParam.getSelection(), { waypointId: wpId });
            updated.push(addr);
          });
          ctrl.tableParam.remove(updated, 'waypointId');
          ctrl.grouped = _.groupBy(ctrl.tableParam.getUnfilteredTableData(), 'zone.id');
          ctrl.tableParam.clearSelect();
        }
      }, () => {
        ctrl.state = 'idle';
        nvToast.error(nvTranslate.instant('container.unverified-address-assignment.unable-to-assign'));
      });
    }

    function constructParams() {
      const txnParams = nvBackendFilterUtils.constructParams(ctrl.selectedFilters);
      if (!_.isUndefined(txnParams.jaroScoreFrom)) {
        txnParams.jaroScoreFrom = txnParams.jaroScoreFrom.toString();
      }
      if (!_.isUndefined(txnParams.jaroScoreTo)) {
        txnParams.jaroScoreTo = txnParams.jaroScoreTo.toString();
      }
      return txnParams;
    }

    function concatAddress(waypoint) {
      const addresses = _.compact([waypoint.address1, waypoint.address2, waypoint.city,
                        waypoint.country, waypoint.postcode]);
      return _.join(addresses, ', ');
    }

    function downloadErrorAssignmentCsv() {
      const csvRows = [];
      csvRows.push(ctrl.csvHeader);
      _.each(ctrl.failedData, (data) => {
        const address = _.find(ctrl.tableParam.getUnfilteredTableData(), { waypointId: data });
        csvRows.push(_.chain(address)
            .pick(['score', 'granularStatus', 'shipperName', 'address'])
            .values()
            .value());
      });
      const report = document.createElement('a');
      angular.element(report)
                .attr('href', `data:application/csv;charset=utf-8,${encodeURIComponent(_.join(csvRows, '\n'))}`)
                .attr('download', ctrl.csvFileName);
      document.body.appendChild(report);
      report.click();
    }
  }
}());
