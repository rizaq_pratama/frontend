(function controller() {
  angular
        .module('nvOperatorApp.controllers')
        .controller('VehicleTypeManagementController', VehicleTypeManagementController);

  VehicleTypeManagementController.$inject = ['$q', 'VehicleType', 'nvDialog',
      'nvEditModelDialog', 'nvUtils', 'nvTableUtils'];

  function VehicleTypeManagementController($q, VehicleType, nvDialog,
                                           nvEditModelDialog, nvUtils, nvTableUtils) {
    // ////////////////////////////////////////////////
    // variables
    // ////////////////////////////////////////////////
    const ctrl = this;
    const columns = ['id', 'name'];
    ctrl.vehicleTypes = [];
    ctrl.vehicleTypesTableParams = null;

    // ////////////////////////////////////////////////
    // functions
    // ////////////////////////////////////////////////
    ctrl.getVehicleTypes = getVehicleTypes;
    ctrl.onAdd = onAdd;
    ctrl.onRemove = onRemove;
    ctrl.onEdit = onEdit;
    ctrl.onSearch = onSearch;

    // ////////////////////////////////////////////////
    // functions details
    // ////////////////////////////////////////////////
    function getVehicleTypes() {
      VehicleType.searchAll().then(success, $q.reject);

      function success(response) {
        ctrl.vehicleTypes = _.sortBy(ctrl.vehicleTypes.concat(response.vehicleTypes), 'id');
        ctrl.vehicleTypesTableParams = nvTableUtils.generate(ctrl.vehicleTypes,
          { columns: columns });
      }
    }

    function onAdd($event) {
      const fields = VehicleType.getAddFields();

      nvDialog.showSingle($event, {
        templateUrl: 'views/container/vehicle-type-management/dialog/vehicle-type-add.html',
        cssClass: 'vehicle-type-add',
        theme: 'nvGreen',
        scope: _.assign(nvEditModelDialog.scopeTemplate({ onSave: create }), { fields: fields }),
      }).then(success);

      function create(data) {
        const vehicleType = { vehicleType: data };
        return VehicleType.createOne(vehicleType);
      }

      function success(response) {
        ctrl.vehicleTypes = _.sortBy(ctrl.vehicleTypes.concat(response.vehicleType), 'id');
        nvTableUtils.addTo(ctrl.vehicleTypesTableParams,
          response.vehicleType, { columns: columns });
      }
    }

    function onEdit($event, model) {
      const fields = VehicleType.getEditFields(model);

      nvDialog.showSingle($event, {
        templateUrl: 'views/container/vehicle-type-management/dialog/vehicle-type-edit.html',
        cssClass: 'vehicle-type-edit',
        scope: _.assign(nvEditModelDialog.scopeTemplate({ onSave: edit }), { fields: fields }),
      }).then(success);

      function edit(data) {
        const vehicleType = { vehicleType: data };
        return VehicleType.updateOne(vehicleType);
      }

      function success(response) {
        ctrl.vehicleTypes = nvUtils.mergeIn(ctrl.vehicleTypes, response.vehicleType, 'id', response.vehicleType.id);
        nvTableUtils.mergeIn(ctrl.vehicleTypesTableParams,
          response.vehicleType, { columns: columns });
      }
    }

    function onRemove($event, model) {
      nvDialog.confirmDelete($event, {
        content: `Are you sure you want to permanently delete \'${model.name}\'?`,
      }).then(remove);

      function remove() {
        VehicleType.deleteOne(model.id).then(success, $q.reject);
      }

      function success(response) {
        _.remove(ctrl.vehicleTypes, { id: response.vehicleType.id });
        nvTableUtils.remove(ctrl.vehicleTypesTableParams, 'id', response.vehicleType.id);
      }
    }

    function onSearch(searchText) {
      if (ctrl.vehicleTypesTableParams) {
        ctrl.vehicleTypesTableParams.filter({ $: searchText });
      }
    }
  }
}());
