(function controller() {
  angular
    .module('nvOperatorApp.controllers')
    .controller('BatchOrderRollbackController', BatchOrderRollbackController);

  BatchOrderRollbackController.$inject = [
    'Order', 'localData', '$mdDialog', 'nvToast', 'nvTranslate',
  ];

  function BatchOrderRollbackController(
    Order, localData, $mdDialog, nvToast, nvTranslate
  ) {
    const ctrl = this;
    const batchId = localData.id;

    const PASSWORD = '1234567890';
    const API_STATE = {
      IDLE: 'idle',
      WAITING: 'waiting',
    };

    ctrl.submitState = API_STATE.IDLE;

    ctrl.formPassword = '';

    ctrl.onCancel = onCancel;
    ctrl.onSubmit = onSubmit;

    function onCancel() {
      $mdDialog.hide(-1);
    }

    function onSubmit() {
      if (isValidPassword()) {
        rollback();
      } else {
        nvToast.error(nvTranslate.instant('container.order.edit.invalid-password'));
      }

      function isValidPassword() {
        return ctrl.formPassword === PASSWORD;
      }

      function rollback() {
        ctrl.submitState = API_STATE.WAITING;
        Order.deleteBatchOrders(batchId).then(() => {
          nvToast.success(nvTranslate.instant('container.batch-order.dialog-rollback-success'));

          ctrl.submitState = API_STATE.IDLE;
          $mdDialog.hide(0);
        }, () => {
          ctrl.submitState = API_STATE.IDLE;
        });
      }
    }
  }
}());
