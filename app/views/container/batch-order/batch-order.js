(function controller() {
  angular
    .module('nvOperatorApp.controllers')
    .controller('BatchOrderController', BatchOrderController);

  BatchOrderController.$inject = [
    'nvTable', 'Order', '$stateParams', 'nvDateTimeUtils', 'nvDialog',
    'nvTranslate', 'Printer', '$q', 'nvToast',
  ];

  function BatchOrderController(
    nvTable, Order, $stateParams, nvDateTimeUtils, nvDialog,
    nvTranslate, Printer, $q, nvToast
  ) {
    const ctrl = this;
    const TABLE_FIELD = {
      id: { displayName: 'commons.id' },
      tracking_id: { displayName: 'commons.tracking-id' },
      type: { displayName: 'commons.type' },
      c_from: { displayName: 'commons.from' },
      c_to: { displayName: 'commons.to' },
      status: { displayName: 'commons.status' },
      granular_status: { displayName: 'commons.model.granular-status' },
      c_createdAt: { displayName: 'commons.created-at' },
    };
    const API_STATE = {
      IDLE: 'idle',
      WAITING: 'waiting',
    };

    ctrl.formBatchId = $stateParams.id || null;
    ctrl.currentBatchId = null;
    ctrl.printState = API_STATE.IDLE;
    ctrl.searchState = API_STATE.IDLE;
    ctrl.tableParam = null;

    ctrl.isAbleToShowResult = isAbleToShowResult;
    ctrl.onSearch = onSearch;
    ctrl.rollback = rollback;
    ctrl.print = print;

    init();

    function init() {
      if (ctrl.formBatchId) {
        onSearch(ctrl.formBatchId);
      }
    }

    function isAbleToShowResult() {
      return ctrl.tableParam &&
        !ctrl.tableParam.empty &&
        ctrl.searchState === API_STATE.IDLE;
    }

    function onSearch() {
      ctrl.searchState = API_STATE.WAITING;

      Order.getBatchOrderDetails(ctrl.formBatchId)
        .then(success, failure)
        .finally(finallyFn);

      function success(response) {
        ctrl.currentBatchId = ctrl.formBatchId;

        ctrl.tableParam = nvTable.createTable(TABLE_FIELD);
        ctrl.tableParam.updateColumnFilter('c_from', strFn => (
          (order) => {
            // strfn is the function that will return you the search text
            const lowercase = strFn().toLowerCase();
            return lowercase.length === 0 ||
              _.toLower(order.from_name).indexOf(lowercase) >= 0 ||
              _.toLower(order.c_fromAddress).indexOf(lowercase) >= 0;
          }
        ));
        ctrl.tableParam.updateColumnFilter('c_to', strFn => (
          (order) => {
            // strfn is the function that will return you the search text
            const lowercase = strFn().toLowerCase();
            return lowercase.length === 0 ||
              _.toLower(order.to_name).indexOf(lowercase) >= 0 ||
              _.toLower(order.c_toAddress).indexOf(lowercase) >= 0;
          }
        ));
        ctrl.tableParam.updateColumnFilter('status', strFn => (
          (order) => {
            // strfn is the function that will return you the search text
            const lowercase = strFn().toLowerCase();
            return lowercase.length === 0 ||
              _.toLower(order.status).indexOf(lowercase) >= 0 ||
              _.toLower(order.granular_status).indexOf(lowercase) >= 0;
          }
        ));

        ctrl.tableParam.setData(extendData(response.orders));
      }

      function failure() {
        if (ctrl.tableParam) {
          ctrl.tableParam.setData([]);
        }
      }

      function finallyFn() {
        ctrl.searchState = API_STATE.IDLE;
      }

      function extendData(orders) {
        return _.map(orders, order =>
          setCustomOrderData(order)
        );

        function setCustomOrderData(order) {
          order.c_fromAddress = _.join(_.compact([
            order.from_address1,
            order.from_address2,
            order.from_city,
            order.from_state,
            order.from_country,
            order.from_postcode,
          ]), ' ');

          order.c_toAddress = _.join(_.compact([
            order.to_address1,
            order.to_address2,
            order.to_city,
            order.to_state,
            order.to_country,
            order.to_postcode,
          ]), ' ');

          order.c_createdAt = nvDateTimeUtils.displayDateTime(
            moment(order.created_at)
          );

          return order;
        }
      }
    }

    function rollback($event) {
      nvDialog.showSingle($event, {
        templateUrl: 'views/container/batch-order/dialog/rollback/rollback.dialog.html',
        theme: 'nvRed',
        cssClass: 'nv-container-batch-order-rollback',
        controller: 'BatchOrderRollbackController',
        controllerAs: 'ctrl',
        locals: {
          localData: { id: ctrl.currentBatchId },
        },
      }).then(onRollback);

      function onRollback(arg) {
        if (arg === 0) {
          ctrl.tableParam.setData([]);
        }
      }
    }

    function print($event) {
      const orders = ctrl.tableParam.getUnfilteredTableData();

      nvDialog.confirmDelete($event, {
        title: nvTranslate.instant('container.batch-order.dialog-print-title'),
        content: nvTranslate.instant('container.batch-order.dialog-print-body'),
        ok: nvTranslate.instant('commons.print'),
        cancel: nvTranslate.instant('commons.cancel'),
      }).then(onPrint);

      function onPrint() {
        ctrl.printState = 'waiting';
        const chunkedOrdersCollection = _.chunk(orders, 100);
        const results = [];

        _.forEach(chunkedOrdersCollection, (smallOrders) => {
          results.push(Printer.printByPost(convertToPrintModel(smallOrders)));
        });
        $q.all(results).then(() => {
          nvToast.success(nvTranslate.instant('container.order.edit.shipping-label-printed'));
        }).finally(() => (ctrl.printState = 'idle'));
      }

      function convertToPrintModel(datas) {
        return _.map(datas, data => ({
          recipient_name: data.to_name,
          address1: data.to_address1,
          address2: data.to_address2,
          contact: data.to_contact,
          country: data.to_country,
          postcode: data.to_postcode,
          shipper_name: data.from_name,
          tracking_id: data.tracking_id,
          barcode: data.tracking_id,
          route_id: '',
          driver_name: '',
          template: 'hub_shipping_label_70X50',
        }));
      }
    }
  }
}());
