(function ShipmentScanningController() {
  angular
        .module('nvOperatorApp.controllers')
        .controller('ShipmentScanningController', Controller);
  Controller.$inject = [
    '$q', 'Shipping', '$mdDialog', 'nvDialog', 'Order', 'Ticketing', '$timeout', '$scope', '$rootScope', 'nvToast',
    'nvTranslate', 'nvDateTimeUtils', 'nvSoundService', 'nvTable', 'nvClientStore',
    'hotkeys',
  ];


  function Controller($q, Shipping, $mdDialog,
                      nvDialog, Order, Ticketing, $timeout, $scope, $rootScope,
                      nvToast, nvTranslate, nvDateTimeUtils, nvSoundService,
                      nvTable, nvClientStore, hotkeys) {
    const ctrl = this;
    const DUPLICATE = 'duplicate';
    const NORMAL = 'normal';
    const ERROR = 'error';
    const SUCCESS = 'success';
    const ALL = 'all';
    const DIFFHUB = 'diffhub';
    const SETTING_KEY = 'shipmentScanningSetting';

    // variables
    ctrl.showPickShipment = true;
    ctrl.shipment = null;
    ctrl.shipments = [];
    ctrl.isEditing = false;
    ctrl.hubs = [];
    ctrl.hubId = null;
    ctrl.trackingId = null;
    ctrl.trackingIdDisabled = false;
    ctrl.orders = [];
    ctrl.scanState = 'normal'; // can be 'success','error','duplicate','normal'
    ctrl.shipmentTypes = [];
    ctrl.shipmentType = null;
    ctrl.unfilteredShipment = [];
    // functions
    ctrl.onChangeHub = onChangeHub;
    ctrl.onSelectShipment = onSelectShipment;
    ctrl.onChangeShipment = onChangeShipment;
    ctrl.fetchShipment = fetchShipment;
    ctrl.searchTrackingId = searchTrackingId;
    ctrl.removeTrackingId = removeTrackingId;
    ctrl.onCompleteShipment = onCompleteShipment;
    ctrl.onLoad = onLoad;
    ctrl.onDelete = onDelete;
    ctrl.openSettingsDialog = openSettingsDialog;
    ctrl.removeAll = removeAll;
    ctrl.queryTrackingId = queryTrackingId;
    ctrl.scannedTrackingId = null;
    ctrl.rackInfo = null;
    ctrl.scanMessage = '';
    ctrl.shipmentsExists = true;
    ctrl.showErrorBorder = showErrorBorder;
    ctrl.showSuccessBorder = showSuccessBorder;
    ctrl.onCloseCheatsheet = onCloseCheatsheet;
    ctrl.onOpenCheatsheet = onOpenCheatsheet;
    ctrl.lang = $rootScope.user.language || 'en';
    ctrl.loadShipmentState = 'idle';
    ctrl.sound = {
      success: {
        src: nvSoundService.getSuccessSoundFile(),
        type: 'audio/wav',
      },
      error: {
        src: nvSoundService.getErrorSoundFile(),
        type: 'audio/wav',
      },
      duplicate: {
        src: nvSoundService.getDuplicateSoundFile(),
        type: 'audio/wav',
      },
    };
    ctrl.tableParam = {};
    ctrl.settings = {
      hubAlert: false,
    };


        // functions declaration

    function queryTrackingId() {
      return [];
    }

    function onLoad() {
      // hotkeys binding
      hotkeys.bindTo($scope)
            .add({
              combo: '.+s',
              description: 'Focus to Scan',
              callback: () => {
                $timeout(() => { $('#scan_barcode_input').focus(); });
              },
            })
            .add({
              combo: '.+r',
              description: 'Focus to Remove',
              callback: () => {
                $timeout(() => { $('#scan_barcode_input_remove').focus(); });
              },
            });
          // load hub and
      ctrl.settings = _.assign(ctrl.settings, JSON.parse(
        nvClientStore.localStorage.get(SETTING_KEY) || '{}'
      ));
      return $q.all([
        Shipping.getHubs(),
        Shipping.getShipmentTypes(),
      ]).then(onSuccess);
      function onSuccess(values) {
            // first value is hubs
        if (values[0]) {
          ctrl.hubs = _.map(values[0].hubs, o => ({
            value: o.id,
            displayName: o.name,
          }));
        }
        ctrl.shipmentTypes = values[1];

        // add null value as all filter
        ctrl.shipmentTypes.push({
          value: ALL,
          displayName: nvTranslate.instant('commons.all'),
        });
      }
    }


        /**
         * pass true if want to hide the select shipment
         * pass false if want to show the select shipment
         * @param isHide
         */
    function hideOrShowSelectShipment(isHide) {
      if (isHide) {
        ctrl.showPickShipment = false;
        $timeout(() => { $('#scan_barcode_input').focus(); }, 500);
      } else {
                // clean the the tracking id
        ctrl.trackingId = null;
        ctrl.showPickShipment = true;
      }
    }

    function removeTrackingId(evt) { 
      // if user press ., need to outfocus this input
      if (event.which === 46) {
        // move the focus somewhere to trigger the hotkeys
        const target = event.target;
        target.blur();
      }
      if (evt) {
        if (evt.charCode !== 13 && evt.charCode !== 9) {
          return $q.reject;
        }
      }
      const trackingId = ctrl.trackingIdRemove;
      if (trackingId === '') {
        return $q.reject;
      }
      const trackingInShipment = _.find(ctrl.orders, o => (o.trackingId === trackingId));
      if (trackingInShipment) {
        const payload = {
          orders: [
            {
              order_country: $rootScope.domain,
              tracking_id: trackingId,
              action_type: 'DELETE',
            },
          ],
        };
        return Shipping.updateOrder(payload, ctrl.shipment.id).then((results) => {
          const result = _.get(results, 'orders');
          if (result[0].status) {
            nvToast.success(nvTranslate.instant('container.shipment-scanning.tracking-id-x-is-successfully-removed',
            { x: trackingId }
          ));
            ctrl.tableParam.remove({ trackingId: trackingId }, 'trackingId');
            ctrl.tableParam.refreshData();
          } else {
            nvToast.error(nvTranslate.instant('container.shipment-scanning.failed-delete-order', { orderId: trackingId }));
          }
          ctrl.trackingIdRemove = '';
        });
      }
      nvToast.error(nvTranslate.instant('container.shipment-scanning.tracking-id-x-is-not-exist-in-this-shipment',
      { x: trackingId }
    ));
    }

    function searchTrackingId(evt) {
      let payload = {};
      // if user press ., need to outfocus this input
      if (event.which === 46) {
        // move the focus somewhere to trigger the hotkeys
        const target = event.target;
        target.blur();
      }
      if (evt) {
        if (evt.charCode !== 13 && evt.charCode !== 9) {
          return $q.reject;
        }
      }
      const trackingId = ctrl.trackingId;
      if (trackingId === '') {
        return $q.reject;
      }
      const trackingInShipment = _.find(ctrl.orders, o => (o.trackingId === trackingId));
      if (trackingInShipment) {
        showDuplicate();
        return $q.reject;
      }

      if (trackingId) {
        ctrl.trackingIdDisabled = true; // disabled first to prevent multiple call
        // NOTE : we assume order_country is operator country
        payload = {
          orders: [
            {
              order_country: $rootScope.domain,
              tracking_id: trackingId,
              action_type: 'ADD',
            },
          ],
        };
        return Shipping.updateOrder(payload, ctrl.shipment.id)
            .then(onSuccess, onError);
      }
      return $q.reject;

       /**
      * callback for success search order by trackingId
      * @param result
      * @returns {*}
      */
      function onSuccess(results) {
      // retrieve the reponse on orders field
        if (results.orders && results.orders[0]) {
          const scanObj = results.orders[0];
          if (scanObj.status) {
            const order = scanObj.order;
            const obj = {
              id: order.order_id,
              trackingId: order.tracking_id,
              destination: order.to_address,
              addressee: order.to_name,
              deliverBy: displayTime(order.delivery_date),
              fromCountry: order.order_country,
              rackSector: order.rack_sector,
              validHub: isValidDestHub(order),
              _isNew: true,
            };
            ctrl.rackInfo = order.rack_sector;
            obj.sn = ctrl.orders.length + 1;
            ctrl.orders.unshift(obj);
            ctrl.tableParam.mergeIn(obj, 'trackingId');
            ctrl.trackingIdDisabled = false;
            return showSuccess(obj);
          }
          const message = scanObj.message;
          if (message === 'NOT_FOUND') {
            return showError();
          }
          return showDuplicate();
        }
        return showError();
      }

      function onError() {
        return showError();
      }

      function showError() {
        ctrl.scanState = ERROR;
        ctrl.rackInfo = nvTranslate.instant('container.shipment-scanning.rack-info-invalid');
        ctrl.trackingIdDisabled = false;
        ctrl.scannedTrackingId = angular.copy(ctrl.trackingId);
        ctrl.trackingId = null;
        ctrl.scanMessage = nvTranslate.instant('container.shipment-scanning.scan-message-invalid',
          {
            scannedTrackingId: ctrl.scannedTrackingId,
          });
        playErrorSound();
        return $timeout(() => {
          ctrl.scanState = NORMAL;
        }, 3000);
      }

      function showDuplicate() {
        ctrl.scanState = DUPLICATE;
        ctrl.rackInfo = nvTranslate.instant('container.shipment-scanning.rack-info-duplicate');
        ctrl.scannedTrackingId = angular.copy(ctrl.trackingId);
        ctrl.trackingId = null;
        ctrl.scanMessage = nvTranslate.instant('container.shipment-scanning.scan-message-duplicate',
          {
            scannedTrackingId: ctrl.scannedTrackingId,
          });
        playDuplicateSound();
        return $timeout(() => {
          ctrl.scanState = NORMAL;
        }, 3000);
      }

      function showSuccess(orderObj) {
        ctrl.scanState = SUCCESS;
        ctrl.scannedTrackingId = _.clone(ctrl.trackingId);        
        ctrl.scanMessage = nvTranslate.instant('container.shipment-scanning.scan-message-added',
          {
            scannedTrackingId: ctrl.scannedTrackingId,
          });
        if (!orderObj.isValidDestHub) {
          ctrl.scanMessage = nvTranslate.instant('container.shipment-scanning.this-parcel-is-going-to-the-same-hub-as-the-shipment');
        }
        ctrl.trackingId = null;
        playRackSound(orderObj.validHub);
        if (!orderObj.validHub) {
          ctrl.scanState = DIFFHUB;
        }
        return $timeout(() => {
          ctrl.scanState = NORMAL;
        }, 3000);
      }
    }

    /*
    check destination hub validity
     */
    function isValidDestHub(orderObj) {
      let validity = false;
      const hub = orderObj.destinationHub || orderObj.dest_hub;
      if (hub) {
        validity = hub.id === ctrl.shipment.dest_hub_id
            && hub.country === ctrl.shipment.dest_hub_country;
      }
      return validity;
    }

    function fetchShipment() {
      if (!ctrl.hub) {
        nvToast.info('Please select hub');
        return $q.reject();
      }

      if (!ctrl.shipmentType) {
        nvToast.info('Please select shipment type');
        return $q.reject();
      }
      let type = ctrl.shipmentType;
      if (ctrl.shipmentType === 'all') {
        type = '';
      }
      return Shipping.quickSearch(_.castArray(type),
          _.castArray({ system_id: _.upperCase($rootScope.domain), id: ctrl.hub }))
        .then(onSuccess);

      function onSuccess(result) {
        if (result.shipments && result.shipments.length > 0) {
          ctrl.shipmentsExists = true;
          ctrl.shipments = _.map(result.shipments, (shipment) => ({
            displayName: `${shipment.id} - ${shipment.dest_hub_name}`,
            value: shipment,
            id: shipment.id,
          }));
        } else {
          ctrl.shipmentsExists = false;
          ctrl.shipments = [];
        }
      }
    }

    // getShipmentBasedOnHub
    function onChangeHub(hub) {
      if (!hub) {
        return $q.reject();
      }
      ctrl.hubId = hub;
      ctrl.shipment = null;
      return Shipping.getShipmentByHub(hub).then(onSuccess);

      function onSuccess(result) {
        if (result.shipments && result.shipments.length > 0) {
          ctrl.shipmentsExists = true;
        } else {
          ctrl.shipmentsExists = false;
        }
        ctrl.unfilteredShipment = _.chain(result.shipments)
          .map(o => ({
            value: o,
            displayName: o.id,
            shipmentType: o.shipment_type,
          }))
          .orderBy('displayName', 'desc')
          .value();
      }
    }

    function playRackSound(success) {
      const charArray = ctrl.rackInfo.split('');
      let playlist = _.map(charArray, o => ({
        src: nvSoundService.getRackSoundFile(o, ctrl.lang),
        type: 'audio/mp3',
      }));
      if (success) {
        playlist = _.concat(playlist, $scope.playlist1);
      } else {
        if (ctrl.settings.hubAlert) {
          playlist = $scope.diffHubPlaylist;
        } else {
          playlist = _.concat(playlist, $scope.playlist2);
        }
      }
      $scope.rackSound.$playlist = playlist;
      $scope.playlistRack = playlist;
      return $timeout(() => {
        $scope.rackSound.play(0);
      }, 1);
    }

    function onSelectShipment() {
          // get shipment detail from server
      ctrl.loadShipmentState = 'waiting';
      return Shipping.get(ctrl.shipment.id)
          .then(onSuccess);

      function onSuccess(result) {
        ctrl.tableParam = nvTable.createTable(Shipping.getScanningFields
        , {
          newItemOnTop: true,
          isStripped: false,
        });
        ctrl.tableParam.setHighlightFn(highlightFn);
        if (result.shipment) {
          ctrl.shipment = result.shipment;
          const trackingIds = _.map(ctrl.shipment.orders, o => (o.order_id));
          return Order.getTrackingIds(trackingIds)
                      .then(onSuccessTrackingId);
        }
        return $q.reject;
        /**
        * callback for orders detail api call
        * @param resultTrackingId
        */
        function onSuccessTrackingId(resultTrackingId) {
          // try to map shipments into our table structure
          // reverse the order
          _.reverse(resultTrackingId);
          ctrl.orders = _.map(resultTrackingId, o => ({
            id: o.id,
            trackingId: o.trackingId,
            destination: o.toAddress1,
            addressee: o.toName,
            deliverBy: displayTime(o.deliveryDate),
            fromCountry: $rootScope.domain,
            rackSector: o.rackSector,
            validHub: isValidDestHub(o),
          }));
          ctrl.tableParam.setData(ctrl.orders);
          ctrl.loadShipmentState = 'idle';
          hideOrShowSelectShipment(true);
        }
      }
    }

    function openSettingsDialog(event) {
      return nvDialog.showSingle(event, {
        templateUrl: 'views/container/shipment-scanning/dialogs/settings.dialog.html',
        theme: 'nvBlue',
        cssClass: 'nv-shipment-scanning-settings',
        controllerAs: 'ctrl',
        controller: 'ShipmentScanningSettingsDialogController',
        locals: {
          settings: ctrl.settings,
        },
      }).then((result) => {
        nvToast.info(nvTranslate.instant('container.shipment-scanning.settings-updated'));
        ctrl.settings = result;
        nvClientStore.localStorage.set(SETTING_KEY, JSON.stringify(result));
      });
    }

    function onCompleteShipment() {
      ctrl.isEditing = false;
      const savedShipment = ctrl.shipment.id;
      nvToast.success(nvTranslate.instant('container.shipment-scanning.shipment-updated', {
        shipmentId: savedShipment,
      }));
      ctrl.shipment = null;
      ctrl.hub = null;
      hideOrShowSelectShipment(false);
    }

    function onChangeShipment($event) {
      // show the dialog
      showDialog($event).then(onLeave);

      function onLeave(result) {
        if (result) {
                // clear the flag
          ctrl.isEditing = false;
                //
          ctrl.shipment = null;
          ctrl.hub = null;
                // change state to select shipment
          hideOrShowSelectShipment(false);
        }
      }


      function showDialog(event) {
        return $mdDialog.show(
                    $mdDialog.confirm()
                        .targetEvent(event)
                        .title(nvTranslate.instant('commons.leaving-page'))
                        .content(nvTranslate.instant('commons.leave-content'))
                        .ok(nvTranslate.instant('commons.leave'))
                        .cancel(nvTranslate.instant('commons.stay'))
                );
      }
    }


    /**
     * callback for remove all button
     * @param $event
     */
    function removeAll($event) {
      nvDialog.confirmDelete($event, {
        title: nvTranslate.instant('container.shipment-scanning.remove-all-parcell'),
        content: nvTranslate.instant('container.shipment-scanning.remove-all-parcell-dialog'),
        ok: nvTranslate.instant('commons.remove'),
        cancel: nvTranslate.instant('commons.cancel'),
      }).then(doDelete);

      function doDelete() {
        // this will call update shipment order will action_type "DELETE"
        const orders = [];
        // NOTE : we assume order_country is operator country
        _.forEach(ctrl.orders, (o) => {
          orders.push(
            {
              order_country: $rootScope.domain,
              order_id: o.id,
              action_type: 'DELETE',
            }
          );
        });

        const payload = {
          orders: orders,
        };
        return Shipping.updateOrder(payload, ctrl.shipment.id)
                    .then(onSuccess);
        function onSuccess() {
          ctrl.orders.length = 0;
          ctrl.tableParam.setData(ctrl.orders);
          ctrl.tableParam.refreshData();
        }
      }
    }

    /**
     on delete call
     */
    function onDelete($event, order) {
      nvDialog.confirmDelete($event, {
        content: nvTranslate.instant('container.shipment-scanning.remove-all-shipment-confirm', {
          trackingId: order.trackingId,
        }),
      }).then(doDelete);

      function doDelete() {
        // this will call update shipment order will action_type "DELETE"
        // NOTE : we assume order_country is operator country
        const payload = {
          orders: [{
            order_country: $rootScope.domain,
            order_id: order.id,
            action_type: 'DELETE',
          }],
        };
        return Shipping.updateOrder(payload, ctrl.shipment.id)
            .then(onSuccess);
        function onSuccess(result) {
          // delete if status return true
          if (result.orders[0] && result.orders[0].status) {
            ctrl.tableParam.remove(order, 'id');
            // _.remove(ctrl.orders, o => (o.id === order.id));
            nvToast.success(nvTranslate.instant(
                'container.shipment-scanning.success-delete-order',
              {
                orderId: order.trackingId,
              }
            ));
          } else {
            nvToast.error(nvTranslate.instant(
                'container.shipment-scanning.failed-delete-order',
              {
                orderId: order.trackingId,
              }
            ));
          }
        }
      }
    }

    function playErrorSound() {
      return $timeout(() => {
        $scope.errorSound.playPause(0);
      }, 1);
    }

    function playDuplicateSound() {
      return $timeout(() => {
        $scope.duplicateSound.playPause(0);
      }, 1);
    }

    function displayTime(time) {
      return nvDateTimeUtils.displayDateTime(moment.utc(time), $rootScope.user.timezone);
    }

    function showErrorBorder() {
      return ctrl.rackInfo === 'INVALID' || ctrl.rackInfo === 'DUPLICATE' || ctrl.scanState === DIFFHUB;
    }

    function showSuccessBorder() {
      return (ctrl.rackInfo !== '' && ctrl.rackInfo !== null
      && ctrl.rackInfo !== 'INVALID' && ctrl.rackInfo !== 'DUPLICATE' && ctrl.scanState !== DIFFHUB);
    }

    function highlightFn(scans) {
      return {
        class: '',
        highlighted: scans !== 'end' && !scans.validHub,
      };
    }


    /**
     * Cheatsheet functions
     */

    function onCloseCheatsheet() {
      ctrl.showCheatsheet = false;
    }

    function onOpenCheatsheet() {
      ctrl.showCheatsheet = true;
    }
  }
}());
