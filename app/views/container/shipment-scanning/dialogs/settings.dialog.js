(function controller() {
  angular
        .module('nvOperatorApp.controllers')
        .controller('ShipmentScanningSettingsDialogController', ShipmentScanningSettingsDialogController);

  ShipmentScanningSettingsDialogController.$inject = ['$mdDialog', 'settings'];
  function ShipmentScanningSettingsDialogController($mdDialog, settings) {
    const ctrl = this;
    ctrl.onCancel = onCancel;
    ctrl.onSave = onSave;
    ctrl.settings = _.cloneDeep(settings);

    function onCancel() {
      return $mdDialog.cancel();
    }

    function onSave() {
      return $mdDialog.hide(ctrl.settings);
    }

  }
}());
