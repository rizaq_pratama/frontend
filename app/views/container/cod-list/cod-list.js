(function controller() {
  angular
    .module('nvOperatorApp.controllers')
    .controller('CODListController', CODListController);

  const COLUMNS = ['id', 'routeId', 'amountCollected', 'receiptNo', 'inboundedBy'];

  CODListController.$inject = [
    'COD', 'nvTableUtils', 'nvEditModelDialog', 'nvCurrency', 'nvDialog',
    'nvUtils', '$rootScope',
  ];
  function CODListController(
    COD, nvTableUtils, nvEditModelDialog, nvCurrency, nvDialog,
    nvUtils, $rootScope
  ) {
    const ctrl = this;
    
    ctrl.maxDateRange = 7;
    ctrl.getCods = getCods;
    ctrl.addCod = addCod;
    ctrl.editCod = editCod;
    ctrl.deleteCod = deleteCod;
    ctrl.onSearch = onSearch;
    ctrl.cods = [];
    ctrl.codsTableParams = null;
    ctrl.fromDate = moment().toDate();
    ctrl.toDate = moment().toDate();
    ctrl.codGetState = 'idle';
    ctrl.dirty = false;
    ctrl.onDateChanged = onDateChanged;

    // to trigger the limiter
    onDateChanged();

    // ////////////////////////////////////////////

    function getCods() {
      ctrl.codGetState = 'waiting';
      return COD.getAll(ctrl.fromDate, ctrl.toDate).then(success, fail);

      function success(cods) {
        ctrl.codGetState = 'idle';
        ctrl.cods = _.sortBy(cods, 'id');
        ctrl.codsTableParams = nvTableUtils.generate(ctrl.cods, { columns: COLUMNS });
        ctrl.dirty = true;
      }
      function fail() {
        ctrl.codGetState = 'idle';
      }
    }

    function onDateChanged() {
      setMaxDateModel();
      validateDateRange();

      function validateDateRange() {
        if (ctrl.maxDateRange > 0) {
          const bool = moment(ctrl.toDate).diff(ctrl.fromDate, 'days') < ctrl.maxDateRange;

          if (_.size(ctrl.form) > 0) {
            if (!bool) {
              ctrl.form.toDateField.$setValidity('maxdate', false);
            } else {
              ctrl.form.toDateField.$setValidity('maxdate', true);
            }
          }

          return bool;
        }

        return true;
      }

      function setMaxDateModel() {
        if (ctrl.maxDateRange > 0) {
          ctrl.toMaxDateModel = moment(ctrl.fromDate).add(ctrl.maxDateRange - 1, 'day').toDate();
        }
      }
    }

    function addCod($event) {
      const fields = COD.getAddFields();

      nvDialog.showSingle($event, {
        templateUrl: 'views/container/cod-list/dialog/cod-add.dialog.html',
        cssClass: 'cod-add',
        theme: 'nvGreen',
        scope: _.assign(nvEditModelDialog.scopeTemplate({ onSave: create }),
                 { fields: fields, currency: nvCurrency.getSymbol($rootScope.countryId) }),
      }).then(success);

      function create(data) {
        return COD.create(data);
      }

      function success(cod) {
        ctrl.cods = _.sortBy(ctrl.cods.concat([cod]), 'id');
        nvTableUtils.addTo(ctrl.codsTableParams, cod, { columns: COLUMNS });
      }
    }

    function editCod($event, id) {
      const model = _.find(ctrl.cods, ['id', id]);
      const fields = COD.getEditFields(model);

      nvDialog.showSingle($event, {
        templateUrl: 'views/container/cod-list/dialog/cod-edit.dialog.html',
        cssClass: 'cod-edit',
        scope: _.assign(nvEditModelDialog.scopeTemplate({ onSave: update }),
                  { fields: fields, currency: nvCurrency.getSymbol($rootScope.countryId) }),
      }).then(success);

      function update(data) {
        return COD.update(data);
      }

      function success(cod) {
        ctrl.cods = nvUtils.mergeIn(ctrl.cods, cod, 'id', cod.id);
        nvTableUtils.mergeIn(ctrl.codsTableParams, cod, { columns: COLUMNS });
      }
    }

    function deleteCod($event, id) {
      const model = _.find(ctrl.cods, ['id', id]);

      nvDialog.confirmDelete($event).then(deleteCodBackend).then(success);

      function deleteCodBackend() {
        return COD.delete(model);
      }

      function success(cod) {
        _.remove(ctrl.cods, { id: cod.id });
        nvTableUtils.remove(ctrl.codsTableParams, 'id', cod.id);
      }
    }

    function onSearch(searchText) {
      ctrl.codsTableParams.filter({ $: searchText });
    }
  }
}());
