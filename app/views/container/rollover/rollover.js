(function controller() {
  angular
    .module('nvOperatorApp.controllers')
    .controller('RolloverController', RolloverController);

  RolloverController.$inject = ['$scope', 'Rollover', 'nvTable', 'nvDateTimeUtils',
                              'nvDialog', 'nvTranslate', '$interval', 'nvTimezone', 'nvEnv'];
  function RolloverController($scope, Rollover, nvTable, nvDateTimeUtils,
                              nvDialog, nvTranslate, $interval, nvTimezone, nvEnv) {
    const ctrl = this;

    // Deprecated - all except saas env
    ctrl.deprecated = !nvEnv.isSaas(nv.config.env);

    if (!ctrl.deprecated) {
      ctrl.tableParam = nvTable.createTable(Rollover.FIELDS);
      ctrl.tableParam.setDataPromise(Rollover.pollAll());

      ctrl.date = moment().toDate();
      ctrl.dryRun = dryRun;
      ctrl.rollover = rollover;
      ctrl.refresh = refresh;
      ctrl.formatDate = nvDateTimeUtils.displayDateTime;

      ctrl.interval = $interval(polling, 30000);
      $scope.$on('$destroy', () => {
        // Make sure that the interval is destroyed too
        $interval.cancel(ctrl.interval);
      });
    }

    function dryRun() {
      ctrl.showingTable = false;
      const date = nvDateTimeUtils.displayDateTime(nvDateTimeUtils.toSOD(ctrl.date, nvTimezone.getOperatorTimezone()), 'utc');
      Rollover.dryRun(date).then((data) => {
        ctrl.tableParam.mergeIn(data, 'uuid');
      });
    }

    function rollover($event) {
      const date = nvDateTimeUtils.displayDateTime(nvDateTimeUtils.toSOD(ctrl.date, nvTimezone.getOperatorTimezone()), 'utc');
      nvDialog.confirmDelete($event, {
        title: nvTranslate.instant('container.rollover.rollover'),
        content: nvTranslate.instant('container.rollover.dialog-content', { date: nvDateTimeUtils.displayDate(ctrl.date) }),
        ok: nvTranslate.instant('container.rollover.rollover'),
      }).then(() =>
        Rollover.rollover(date)
      ).then((data) => {
        ctrl.tableParam.mergeIn(data, 'uuid');
      });
    }

    function refresh(uuid) {
      ctrl.tableParam.mergeIn({ uuid: uuid, status: 'Loading' }, 'uuid');
      ctrl.tableParam.mergeInPromise(Rollover.poll(uuid), 'uuid');
    }

    function polling() {
      _(ctrl.tableParam.getTableData())
      .filter(data => data.status === 0)
      .forEach(data => refresh(data.uuid));
    }
  }
}());
