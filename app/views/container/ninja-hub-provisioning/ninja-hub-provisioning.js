(function controller() {
  angular
    .module('nvOperatorApp.controllers')
    .controller('NinjaHubProvisioningController', NinjaHubProvisioningController);

  NinjaHubProvisioningController.$inject = ['nvDomain'];

  function NinjaHubProvisioningController(nvDomain) {
    const ctrl = this;
    ctrl.value = `${nvDomain.replaceDomain(nv.config.servers.lighthouse)}/1.0/mobile-applications`;
  }
}());
