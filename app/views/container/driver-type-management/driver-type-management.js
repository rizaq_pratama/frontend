(function() {
    'use strict';

    angular
        .module('nvOperatorApp.controllers')
        .controller('DriverTypeManagementController', controller);

    controller.$inject = [
        '$q', '$scope', '$timeout',
        'DriverType', 'DriverTypeRule',
        'nvUtils', 'nvDialog', 'nvEditModelDialog'
    ];

    function controller(
        $q, $scope, $timeout,
        DriverType, DriverTypeRule,
        nvUtils, nvDialog, nvEditModelDialog
    ) {
        /* jshint -W040 */
        var ctrl = this;
        /* jshint +W040 */

        ctrl.searchAll = searchAll;
        ctrl.createOne = createOne;
        ctrl.updateOne = updateOne;
        ctrl.deleteOne = deleteOne;
        ctrl.filterAll = filterAll;
        ctrl.isSortActive = isSortActive;
        ctrl.showSortIcon = showSortIcon;
        ctrl.sort = sort;

        ctrl.driverTypeProps = [];
        ctrl.tableData = [];
        ctrl.searchText = '';
        ctrl.isHidden = false;
        ctrl.filters = {
            conditions: {
                0: '000',
                1: '00',
                2: '000000',
                3: '0000',
                4: '000000'
            }
        };

        //////////////////////////////////////////////////

        var filtersForSearchBox = [filterById, filterByName, filterByDeliveryType, filterByPriorityLevel, filterByReservationSize, filterByParcelSize, filterByTimeslot];

        var currSortIdx = 0,
            currSortOpt = null,
            sortByTypes = [undefined, true, false], // no-sorting, ascending, descending
            sortOptions = {
                byId: sortById,
                byName: sortByName,
                byDeliveryType: sortByDeliveryType,
                byPriorityLevel: sortByPriorityLevel,
                byReservationSize: sortByReservationSize,
                byParcelSize: sortByParcelSize,
                byTimeslot: sortByTimeslot
            };

        $scope.$watch('ctrl.filters.conditions', function() {
            filterAll(ctrl.searchText);
        }, true);

        //////////////////////////////////////////////////

        function searchAll() {
            return read().then(success, $q.reject);

            function read() {
                return $q.all([DriverType.searchAll(), DriverTypeRule.read()]);
            }

            function success(results) {
                var driverTypeProps = toProps(results[0].driverTypes, results[1]);
                ctrl.driverTypeProps = ctrl.driverTypeProps.concat(driverTypeProps);
                filterAll(ctrl.searchText);

                // todo refactor this!
                var head = false, body = false;
                var t = null;
                $timeout(function() {
                    $(".table-head").on("scroll", function() {
                        if (body) {
                            return;
                        }
                        $(".table-body").scrollLeft($(this).scrollLeft());
                        $timeout.cancel(t);
                        t = $timeout(stop, 250);
                        head = true;
                    });
                    $(".table-body").on("scroll", function() {
                        if (head) {
                            return;
                        }
                        $(".table-head").scrollLeft($(this).scrollLeft());
                        $timeout.cancel(t);
                        t = $timeout(stop, 250);
                        body = true;
                    });
                    function stop() {
                        head = false;
                        body = false;
                    }
                }, 500);
            }
        }

        function createOne($event) {
            var fields = _.assign(DriverTypeRule.getAddFields(), DriverType.getAddFields());

            nvDialog.showSingle($event, {
                templateUrl: 'views/container/driver-type-management/dialog/driver-type-add.dialog.html',
                cssClass: 'driver-type-add',
                theme: 'nvGreen',
                scope: _.assign(nvEditModelDialog.scopeTemplate({onSave: create}), {fields: fields})
            }).then(success);

            function create(data) {
                return DriverType.createOne(data).then(function(driverType) {
                    return DriverTypeRule.create(driverType).then(function(driverTypeRule) {
                        return $q.when([driverType, driverTypeRule]);
                    });
                });
            }

            function success(results) {
                var driverTypeProps = toProps([results[0]], [results[1]]);
                ctrl.driverTypeProps = ctrl.driverTypeProps.concat(driverTypeProps);
                filterAll(ctrl.searchText);
            }
        }

        function updateOne($event, id) {
            var model = _.find(ctrl.driverTypeProps, ['id', id]);
            var fields = _.assign(DriverTypeRule.getEditFields(model), DriverType.getEditFields(model));

            nvDialog.showSingle($event, {
                templateUrl: 'views/container/driver-type-management/dialog/driver-type-edit.dialog.html',
                cssClass: 'driver-type-edit',
                scope: _.assign(nvEditModelDialog.scopeTemplate({onSave: update}), {fields: fields})
            }).then(success);

            function update(data) {
                return $q.all([DriverType.updateOne(data.id, data), DriverTypeRule.update(data)]);
            }

            function success(results) {
                var prop = toProps([results[0]], [results[1]])[0];
                ctrl.driverTypeProps = nvUtils.mergeIn(ctrl.driverTypeProps, prop, 'id', prop.id);
                filterAll(ctrl.searchText);
            }
        }

        function deleteOne($event, driverTypeProp) {
            nvDialog.confirmDelete($event, {
                content: 'Are you sure you want to permanently delete \'' + driverTypeProp.name + '\'?'
            }).then(onDelete);

            function onDelete() {
                var model = _.find(ctrl.driverTypeProps, ['id', driverTypeProp.id]);
                return $q.all([DriverType.deleteOne(model.id), DriverTypeRule.delete(model.id)]).then(success);
            }

            function success(results) {
                _.remove(ctrl.driverTypeProps, { 'id': results[0].id });
                filterAll(ctrl.searchText);
            }
        }

        function filterAll(searchText) {
            ctrl.searchText = searchText.trim().toLowerCase();

            var filteredData = _.reduce(ctrl.filters.conditions, reduceFn, ctrl.driverTypeProps);

            if (!ctrl.searchText) {
                ctrl.tableData = sortTableData(filteredData);
                return;
            }

            ctrl.tableData = _.filter(filteredData, function(driverTypeProp) {
                return _.some(filtersForSearchBox, function(filter) {
                    return filter.call(null, ctrl.searchText, driverTypeProp);
                });
            });

            ctrl.tableData = sortTableData(ctrl.tableData);

            function reduceFn(acc, val, key) {
                return +val ? _.filter(acc, filterFn(parseInt(val, 2), key)) : acc;
            }

            function filterFn(val, key) {
                return function(prop) {
                    if (_.isUndefined(prop.conditions)) {
                        return false;
                    }

                    if (_.isUndefined(prop.conditions[key])) {
                        return false;
                    }

                    /* jshint bitwise: false */
                    return val & parseInt(prop.conditions[key], 2);
                };
            }
        }

        function toProps(driverTypes, driverTypeRules) {
            // merge the driverTypeRules list into the driverTypes list,
            // ignoring driverTypeRules that don't exist in the driverTypes list,
            // and ensuring the driverTypes properties take precedence over the driverTypeRules properties
            var props = nvUtils.mergeAllIntoFirst(driverTypes, driverTypeRules, 'id', {overrideFirst: false});
            return DriverTypeRule.addConditionStrings(props);
        }

        function filterById(searchText, driverTypeProp) {
            return driverTypeProp.id.toString().indexOf(searchText) >= 0;
        }

        function filterByName(searchText, driverTypeProp) {
            return driverTypeProp.name.toLowerCase().indexOf(searchText) >= 0;
        }

        function filterByDeliveryType(searchText, driverTypeProp) {
            return driverTypeProp._conditions[0] && driverTypeProp._conditions[0].toLowerCase().indexOf(searchText) >= 0;
        }

        function filterByPriorityLevel(searchText, driverTypeProp) {
            return driverTypeProp._conditions[1] && driverTypeProp._conditions[1].toLowerCase().indexOf(searchText) >= 0;
        }

        function filterByReservationSize(searchText, driverTypeProp) {
            return driverTypeProp._conditions[2] && driverTypeProp._conditions[2].toLowerCase().indexOf(searchText) >= 0;
        }

        function filterByParcelSize(searchText, driverTypeProp) {
            return driverTypeProp._conditions[3] && driverTypeProp._conditions[3].toLowerCase().indexOf(searchText) >= 0;
        }

        function filterByTimeslot(searchText, driverTypeProp) {
            return driverTypeProp._conditions[4] && driverTypeProp._conditions[4].toLowerCase().indexOf(searchText) >= 0;
        }

        function isSortActive(option) {
            return currSortOpt === option && currSortIdx > 0;
        }

        function showSortIcon(option) {
            return currSortOpt === option ? (currSortIdx === 1 ? 'fa-sort-asc' : 'fa-sort-desc') : 'fa-sort';
        }

        function sort(option) {
            currSortIdx = currSortOpt === option ? (currSortIdx + 1) % 3 : 1;
            currSortOpt = currSortIdx === 0 ? null : option;
            ctrl.tableData = sortTableData(ctrl.tableData);
        }

        function sortTableData(tableData) {
            /* jshint -W014 */
            return !currSortOpt
                ? _.orderBy(tableData, 'id')
                : sortOptions[currSortOpt](tableData, sortByTypes[currSortIdx]);
            /* jshint +W014 */
        }

        function sortById(tableData, orderBy) {
            return _.orderBy(tableData, 'id', orderBy);
        }

        function sortByName(tableData, orderBy) {
            return _.orderBy(tableData, ['name', 'id'], [orderBy, 'asc']);
        }

        function sortByDeliveryType(tableData, orderBy) {
            return _.orderBy(tableData, ['_conditions.0', 'id'], [orderBy, 'asc']);
        }

        function sortByPriorityLevel(tableData, orderBy) {
            return _.orderBy(tableData, ['_conditions.1', 'id'], [orderBy, 'asc']);
        }

        function sortByReservationSize(tableData, orderBy) {
            return _.orderBy(tableData, ['_conditions.2', 'id'], [orderBy, 'asc']);
        }

        function sortByParcelSize(tableData, orderBy) {
            return _.orderBy(tableData, ['_conditions.3', 'id'], [orderBy, 'asc']);
        }

        function sortByTimeslot(tableData, orderBy) {
            return _.orderBy(tableData, ['_conditions.4', 'id'], [orderBy, 'asc']);
        }
    }

})();
