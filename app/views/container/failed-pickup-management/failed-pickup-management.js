(function controller() {
  angular
    .module('nvOperatorApp.controllers')
    .controller('FailedPickupManagementController', FailedPickupManagementController);

  FailedPickupManagementController.$inject = [
    '$q', 'Order', 'nvTable', 'nvDateTimeUtils', '$window',
    '$state', 'nvToast', 'nvTranslate', 'nvDialog', 'nvEditModelDialog',
    'nvFileUtils', '$mdDialog', 'Shippers', 'Driver',
  ];

  function FailedPickupManagementController(
    $q, Order, nvTable, nvDateTimeUtils, $window,
    $state, nvToast, nvTranslate, nvDialog, nvEditModelDialog,
    nvFileUtils, $mdDialog, Shippers, Driver
  ) {
    // ////////////////////////////////////////////////
    // variables
    // ////////////////////////////////////////////////
    const ctrl = this;
    let SHIPPERS_MAP;
    let FAILURE_REASONS_MAP;

    ctrl.state = {
      isRescheduleUpdating: false,
    };
    ctrl.tableParam = nvTable.createTable();
    ctrl.tableParam.addColumn('tracking_id', { displayName: 'commons.tracking-id' });
    ctrl.tableParam.addColumn('_shipperName', { displayName: 'commons.shipper' });
    ctrl.tableParam.addColumn('_lastAttemptTime', {
      displayName: 'container.failed-pickup-management.last-attempt-datetime',
    });
    ctrl.tableParam.addColumn('_failureReasonComments', {
      displayName: 'container.failed-pickup-management.failure-comments',
    });
    ctrl.tableParam.addColumn('attempt_count', {
      displayName: 'container.failed-pickup-management.total-number-of-failures',
    });
    ctrl.tableParam.addColumn('_invalidFailureCount', {
      displayName: 'container.failed-pickup-management.invalid-failure-count',
    });
    ctrl.tableParam.addColumn('_validFailureCount', {
      displayName: 'container.failed-pickup-management.valid-failure-count',
    });
    ctrl.tableParam.addColumn('_failureReasonCodeDescriptions', {
      displayName: 'container.failed-pickup-management.failure-reason',
    });
    ctrl.tableParam.addColumn('_daysSinceLastAttempt', {
      displayName: 'container.failed-pickup-management.days-after-last-attempt',
    });
    ctrl.tableParam.addColumn('_priorityLevel', {
      displayName: 'commons.model.priority-level',
    });


    // ////////////////////////////////////////////////
    // functions
    // ////////////////////////////////////////////////
    ctrl.getAll = getAll;
    ctrl.getSelectedCount = getSelectedCount;
    ctrl.cancelSelected = cancelSelected;
    ctrl.rescheduleSelected = rescheduleSelected;
    ctrl.downloadCsvSelected = downloadCsvSelected;
    ctrl.rescheduleToNextDay = rescheduleToNextDay;
    ctrl.editOrder = editOrder;

    // ////////////////////////////////////////////////
    // functions details
    // ////////////////////////////////////////////////
    function getAll() {
      return Order.getFailedPickupManagementData().then(success, $q.reject);

      function success(failedPickupManagementResponse) {
        return getRemainingDetails(failedPickupManagementResponse);
      }

      function getRemainingDetails(failedPickupManagementResponse) {
        return $q.all([
          Shippers.searchByIds(
            _.uniq(_.map(failedPickupManagementResponse, 'shipper_id'))
          ),
          Driver.searchFailureReasonsByIds(
            _.uniq(_.flatten(_.map(failedPickupManagementResponse, 'failure_reason_ids')))
          ),
        ]).then(getRemainingDetailsSuccess, $q.reject);

        function getRemainingDetailsSuccess(response) {
          SHIPPERS_MAP = _.keyBy(response[0], 'id');
          FAILURE_REASONS_MAP = _.keyBy(response[1], 'id');

          ctrl.tableParam.setData(extend(failedPickupManagementResponse));
        }
      }
    }

    function getSelectedCount() {
      return ctrl.tableParam.getSelection().length;
    }

    function cancelSelected($event) {
      const failedPickups = _.map(_.cloneDeep(ctrl.tableParam.getSelection()), (order) => {
        // extend order details for cancel-order dialog to use
        order.id = order.order_id;
        order.trackingId = order.tracking_id;
        order._granularStatus = Order.GRANULAR_STATUS.PICKUP_FAIL;
        return order;
      });

      nvDialog.showSingle($event, {
        templateUrl: 'views/container/order/dialog/cancel-order/cancel-order.dialog.html',
        theme: 'nvRed',
        cssClass: 'order-edit-cancel-order',
        controller: 'OrderEditCancelOrderDialogController',
        controllerAs: 'ctrl',
        locals: {
          ordersData: failedPickups,
          settingsData: {
            isMultiSelect: true,
          },
        },
      }).then(success);

      function success(response) {
        ctrl.tableParam.remove(response.success, 'order_id');
        ctrl.tableParam.deselect();
      }
    }

    function rescheduleSelected($event) {
      const fields = {
        date: new Date(),
      };

      nvDialog.showSingle($event, {
        type: 'nv-dialog',
        templateUrl: 'views/container/failed-pickup-management/dialog/reschedule-selected.dialog.html',
        cssClass: 'failed-pickup-management-reschedule-selected',
        scope: _.assign(nvEditModelDialog.scopeTemplate(), {
          onSubmit: onSubmit,
          fields: fields,
          selectedOrdersCount: getSelectedCount(),
          state: ctrl.state,
        }),
      });

      function downloadCsv() {
        const failedPickups = angular.copy(ctrl.rescheduledSuccessfully);
        if (failedPickups && failedPickups.length > 0) {
          let csvContent = 'Tracking ID, Order ID, Shipper Name\n';

          _.forEach(failedPickups, (failedPickup) => {
            csvContent += (`${failedPickup.trackingId},${
                          failedPickup.orderId},${
                          failedPickup.shipperName}\n`);
          });

          nvFileUtils.downloadCsv(csvContent, 'pickup-reschedule.csv');
        }
      }

      function onSubmit() {
        const date = nvDateTimeUtils.displayDate(fields.date);
        const failedPickups = ctrl.tableParam.getSelection();
        ctrl.rescheduledSuccessfully = [];
        if (failedPickups.length > 1) {
          multiSelectRescheduling(failedPickups, date);
        } else {
          singleRescheduling(failedPickups, date);
        }
      }

      function singleRescheduling(failedPickups, date) {
        const requests = _.map(failedPickups, failedPickup =>
          generateRescheduleRequest(failedPickup, date)
        );
        const request = _.head(requests);
        ctrl.state.isRescheduleUpdating = true;
        ctrl.rescheduledSuccessfully = [];

        Order.reschedule(request.orderId, request.data).then((data) => {
          if (data.status.toLowerCase() === 'success') {
            nvToast.success(
              nvTranslate.instant('container.failed-pickup-management.rescheduling-success'),
              nvTranslate.instant('container.failed-pickup-management.num-orders-rescheduling-success', { num: 1 })
            );
            ctrl.rescheduledSuccessfully = _.concat(ctrl.rescheduledSuccessfully, requests);
            removeFromTable(request);
            downloadCsv();
          } else {
            nvToast.error(
              nvTranslate.instant('container.failed-pickup-management.rescheduling-failed')
            );
          }

          ctrl.state.isRescheduleUpdating = false;
          $mdDialog.hide();
        }, () => {
          ctrl.state.isRescheduleUpdating = false;
          $mdDialog.hide();
        });
      }

      function multiSelectRescheduling(failedPickups, date) {
        const requests = _.map(angular.copy(failedPickups), failedPickup =>
          generateRescheduleRequest(failedPickup, date));

        ctrl.bulkActionProgressPayload = {
          totalCount: requests.length,
          currentIndex: 0,
          errors: [],
        };

        // hide date dialog
        $mdDialog.hide();

        nvDialog.showSingle($event, {
          templateUrl: 'lib/ninja-commons/services/dialogs/bulk-action-progress/bulk-action-progress.dialog.html',
          cssClass: 'bulk-action-progress',
          controller: 'BulkActionProgressDialogController',
          controllerAs: 'ctrl',
          skipHide: true,
          clickOutsideToClose: false,
          locals: {
            payload: ctrl.bulkActionProgressPayload,
          },
        }).then(progressDialogClosed);

        startMultiSelectCancelling(requests);
      }

      function startMultiSelectCancelling(requests) {
        let request;
        if (requests.length > 0) {
          ctrl.state.isRescheduleUpdating = true;
          ctrl.bulkActionProgressPayload.currentIndex += 1;
          request = _.head(requests.splice(0, 1));

          Order.reschedule(request.orderId, request.data)
            .then(multiSelectSuccess, multiSelectError);
        } else {
          // should never get here
          ctrl.state.isRescheduleUpdating = false;
        }

        function multiSelectSuccess(data) {
          // check status string
          if (data.status.toLowerCase() === 'success') {
            ctrl.bulkActionProgressPayload.successCount += 1;
            ctrl.rescheduledSuccessfully = _.concat(ctrl.rescheduledSuccessfully, request);
            removeFromTable(request);
            if (requests.length > 0) {
              startMultiSelectCancelling(requests);
            } else {
              downloadCsv();
              ctrl.state.isRescheduleUpdating = false;
            }
          } else {
            multiSelectError({ message: data.message });
          }
        }

        function multiSelectError(error) {
          ctrl.bulkActionProgressPayload.errors.push(`${request.trackingId}: ${error.message}`);
          if (requests.length > 0) {
            startMultiSelectCancelling(requests);
          } else {
            downloadCsv();
            ctrl.state.isRescheduleUpdating = false;
          }
        }
      }

      function progressDialogClosed() {
        if (getSuccessCount() > 0) {
          nvToast.success(
            nvTranslate.instant('container.failed-pickup-management.rescheduling-success'),
            nvTranslate.instant('container.failed-pickup-management.num-orders-rescheduling-success', { num: getSuccessCount() }));
        }
      }

      function getSuccessCount() {
        if (ctrl.bulkActionProgressPayload) {
          return ctrl.bulkActionProgressPayload.totalCount -
            ctrl.bulkActionProgressPayload.errors.length;
        }
        return 0;
      }

      function removeFromTable(failedPickup) {
        const tableData = _.find(ctrl.tableParam.getSelection(), order =>
          order.tracking_id === failedPickup.trackingId);
        ctrl.tableParam.deselect(tableData);
        ctrl.tableParam.remove(tableData, 'tracking_id');
      }
    }

    function downloadCsvSelected() {
      let csvContent = 'Order ID, ' +
        'Tracking ID, ' +
        'Shipper, ' +
        'Last Attempt Datetime, ' +
        'Failure Comments, ' +
        'Total No. of Failures, ' +
        'Invalid Failure Count, ' +
        'Valid Failure Count, ' +
        'Failure Reason, ' +
        'Days After Last Attempt, ' +
        'Priority Level\n';

      _.forEach(ctrl.tableParam.getSelection(), (failedPickup) => {
        csvContent += (`${failedPickup.order_id},${
          failedPickup.tracking_id},${
          failedPickup._shipperName},${
          failedPickup._lastAttemptTime},${
          failedPickup._failureReasonComments || ''},${
          failedPickup.attempt_count},${
          failedPickup._invalidFailureCount},${
          failedPickup._validFailureCount},${
          failedPickup._failureReasonCodeDescriptions},${
          failedPickup._daysSinceLastAttempt},${
          failedPickup._priorityLevel}\n`);
      });

      nvFileUtils.downloadCsv(csvContent, 'failed-pickup-list.csv');
    }

    // Next Day Reschedule
    function rescheduleToNextDay(failedPickup) {
      const request = generateRescheduleRequest(failedPickup, getTomorrowDate());

      failedPickup._isUpdating = true;
      Order.reschedule(request.orderId, request.data).then(success, failure);

      function success(data) {
        if (data.status.toLowerCase() === 'success') {
          nvToast.success(
            nvTranslate.instant('container.failed-pickup-management.rescheduling-success'),
            nvTranslate.instant('container.failed-pickup-management.num-orders-rescheduling-success', { num: 1 })
          );
          ctrl.tableParam.remove(failedPickup, 'order_id');
        } else {
          nvToast.error(
            nvTranslate.instant('container.failed-pickup-management.rescheduling-failed'),
            nvTranslate.instant('container.failed-pickup-management.order-x-comma-x', { orders: request.trackingId })
          );
        }

        failedPickup._isUpdating = false;
      }

      function failure() {
        failedPickup._isUpdating = false;
      }

      function getTomorrowDate() {
        const newDate = new Date();
        newDate.setDate(newDate.getDate() + 1);

        return nvDateTimeUtils.displayDate(newDate);
      }
    }

    function editOrder(failedPickup) {
      $window.open(
        $state.href('container.order.edit', { orderId: failedPickup.order_id })
      );
    }

    function extend(failedPickups) {
      return _.map(failedPickups, failedPickup => (
        setCustomFailedPickupData(failedPickup)
      ));
    }

    function setCustomFailedPickupData(failedPickup) {
      failedPickup._shipperName = _.get(SHIPPERS_MAP[failedPickup.shipper_id], 'name') || '';
      failedPickup._isUpdating = false;
      failedPickup._daysSinceLastAttempt = '';
      failedPickup._lastAttemptTime = '';
      if (failedPickup.last_attempt_time) {
        failedPickup._lastAttemptTime = nvDateTimeUtils.displayDateTime(
          moment.utc(failedPickup.last_attempt_time)
        );

        failedPickup._daysSinceLastAttempt = moment(new Date()).diff(
          moment(failedPickup._lastAttemptTime), 'days'
        ) || nvTranslate.instant('commons.today');
      }

      failedPickup._priorityLevel = failedPickup.priority_level !== null ? failedPickup.priority_level : '-';

      failedPickup._invalidFailureCount = 0;
      failedPickup._validFailureCount = 0;
      _.forEach(failedPickup.failure_reason_ids, (failureReasonId) => {
        const failureReason = FAILURE_REASONS_MAP[failureReasonId] || null;
        if (failureReason) {
          switch (_.get(failureReason, 'failureReasonCode.liability')) {
            case Driver.FAILURE_REASON_CODE_LIABILITY.EXTERNAL:
              failedPickup._validFailureCount += 1;
              break;
            case Driver.FAILURE_REASON_CODE_LIABILITY.NINJAVAN:
              failedPickup._invalidFailureCount += 1;
              break;
            default:
          }
        }
      });

      failedPickup._failureReasonCodeDescriptions = '';
      const lastFailureReasonId = _.last(failedPickup.failure_reason_ids);
      const lastFailureReason = FAILURE_REASONS_MAP[lastFailureReasonId] || null;
      if (lastFailureReason) {
        failedPickup._failureReasonComments = _.get(lastFailureReason, 'description');
        failedPickup._failureReasonCodeDescriptions = _.get(lastFailureReason, 'failureReasonCode.description');
      }

      return failedPickup;
    }

    function generateRescheduleRequest(failedPickup, date) {
      return {
        orderId: failedPickup.order_id,
        trackingId: failedPickup.tracking_id,
        shipperName: failedPickup._shipperName,
        data: { date: date },
      }; // payload and parameter changes based on ORDER-296
    }
  }
}());
