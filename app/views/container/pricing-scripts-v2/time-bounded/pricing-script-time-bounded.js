(function controller() {
  angular
    .module('nvOperatorApp.controllers')
    .controller('PricingScriptTimeBoundedController', PricingScriptTimeBoundedController);

  PricingScriptTimeBoundedController.$inject = ['$scope', 'PricingScriptsV2', 'appSidenav',
    '$state', 'nvTable', '$q', '$stateParams', 'nvDateTimeUtils',
    'nvTimezone'];

  function PricingScriptTimeBoundedController($scope, PricingScriptsV2, appSidenav,
    $state, nvTable, $q, $stateParams, nvDateTimeUtils,
    nvTimezone) {
    const ctrl = this;
    ctrl.function = {};
    ctrl.state = {};
    ctrl.view = {};
    ctrl.data = {};

    const FIELDS = {
      name: { displayName: 'Name' },
      status: { displayName: 'Status' },
      last_edit: { displayName: 'Last Edit' },
      duration: { displayName: 'Duration' },
      actions2: { displayName: '' },
    };

    ctrl.tableParam = nvTable.createTable(FIELDS);
    ctrl.data.parentId = $stateParams.parentId;

    ctrl.getChildScripts = getChildScripts;
    ctrl.addChild = addChild;
    ctrl.editScript = editScript;
    ctrl.onClickCancel = onClickCancel;

    init();
    $scope.$on('$destroy', () => {
      appSidenav.setConfigLockedOpen(true);
    });

    function init() {
      PricingScriptsV2.read(ctrl.data.parentId)
        .then((result) => {
          ctrl.data.script = result;
        });
    }

    function getChildScripts() {
      return PricingScriptsV2.readAllChildren(ctrl.data.parentId).then(success, $q.reject);

      function success(response) {
        const currentDate = moment().startOf('day');

        _.each(response, (script) => {
          if (script.start_date && script.end_date) {
            const startDate = moment(script.start_date);
            const startDateFormatted = nvDateTimeUtils.displayDate(startDate, nvTimezone.getOperatorTimezone());
            const endDate = moment(script.end_date);
            const endDateFormatted = nvDateTimeUtils.displayDate(endDate, nvTimezone.getOperatorTimezone());
            script.duration = `${startDateFormatted} - ${endDateFormatted}`;

            if (script.status === 'Draft') {
              script.time_bounded_status = 'Draft';
              script.sort = 2;
            } else if (currentDate >= startDate && currentDate <= endDate) {
              script.time_bounded_status = 'Active';
              script.sort = 0;
            } else if (currentDate < startDate) {
              script.time_bounded_status = 'On Hold';
              script.sort = 1;
            } else if (currentDate > endDate) {
              script.time_bounded_status = 'Expired';
              script.sort = 3;
            }
          } else {
            script.time_bounded_status = 'Draft';
            script.sort = 2;
          }
          script.lastModified = getLastModified(script);
        });

        const sorted = _.orderBy(response, ['sort', 'start_date'], ['asc', 'desc']);

        ctrl.tableParam.setData(sorted);
        ctrl.tableParam.totalItems = sorted.length;
      }
    }

    function getLastModified(script) {
      if (script.updated_at) {
        return nvDateTimeUtils.displayDateTime(moment(script.updated_at), nvTimezone.getOperatorTimezone());
      }
      return '-';
    }

    function addChild() {
      $state.go('container.v2-pricing-scripts-create-time-bounded', { parentId: ctrl.data.parentId, type: PricingScriptsV2.SCRIPT_TYPE.TIME_INBOUNDED });
    }

    function editScript(scriptId) {
      $state.go('container.v2-pricing-scripts-edit', { scriptId: scriptId, type: PricingScriptsV2.SCRIPT_TYPE.TIME_INBOUNDED });
    }

    function onClickCancel() {
      $state.go('container.v2-pricing-scripts-active');
    }
  }
}());
