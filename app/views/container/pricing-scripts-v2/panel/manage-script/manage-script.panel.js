(function controller() {
  angular
    .module('nvOperatorApp.controllers')
    .controller('ManageScriptToolsMenuController', ManageScriptToolsMenuController);
  ManageScriptToolsMenuController.$inject = ['mdPanelRef', 'onClose', 'manageScriptActions'];

  function ManageScriptToolsMenuController(
    mdPanelRef
  ) {
    // variables
    const ctrl = this;

    // functions
    ctrl.chooseAction = chooseAction;

    // functions details
    function chooseAction(action) {
      mdPanelRef.close().then(() => {
        action();
      });
    }
  }
}());
