(function controller() {
  angular
    .module('nvOperatorApp.controllers')
    .controller('PricingScriptVerifyController', PricingScriptVerifyController);

  PricingScriptVerifyController.$inject = ['$scope', 'PricingScriptsV2', 'appSidenav', '$state',
      'nvToast', 'nvTranslate', 'nvCurrency', 'nvDomain', '$rootScope'];

  function PricingScriptVerifyController($scope, PricingScriptsV2, appSidenav, $state, nvToast,
                                         nvTranslate, nvCurrency, nvDomain, $rootScope) {
    const ctrl = this;
    ctrl.function = {};
    ctrl.state = {};
    ctrl.view = {};
    ctrl.data = {};
    ctrl.data.parameters = [];
    ctrl.data.parameterOptions = {};

    ctrl.country = nvDomain.getDomain() ? nvDomain.getDomain().current.toUpperCase() : 'SG';
    ctrl.state.canRelease = false;
    ctrl.state.hasError = false;

    ctrl.getParameterOptions = getParametersOptions;
    ctrl.getFloatParameters = getFloatParameters;
    ctrl.canRelease = canRelease;
    ctrl.onClickCancel = onClickCancel;
    ctrl.onVerify = onVerify;
    ctrl.onRelease = onRelease;

    const currency = nvCurrency.getCode($rootScope.countryId);

    init();
    function init() {
      if ($state.params.activeParameters) {
        $state.params.activeParameters.forEach((parameter) => {
          switch (parameter.variable_name) {
            case 'weight':
              parameter.unit = 'kg';
              parameter.increment = 1;
              break;
            case 'insured_value':
              parameter.unit = currency;
              parameter.increment = getPriceIncrement();
              break;
            case 'cod_value':
              parameter.unit = currency;
              parameter.increment = getPriceIncrement();
              break;
            default:
          }

          ctrl.data.parameterOptions[parameter.variable_name] = parameter;
          ctrl.data.parameters.push(parameter);
        });
      }
    }

    function getPriceIncrement() {
      switch (ctrl.country) {
        case 'SG':
          return 1;
        case 'VN':
          return 10000;
        case 'ID':
          return 1000;
        case 'TH':
          return 25;
        case 'PH':
          return 50;
        case 'MY':
          return 1;
        case 'MM':
          return 1000;
        default:
          return 1;
      }
    }

    function getParametersOptions(key) {
      return ctrl.data.parameterOptions[key];
    }

    function getFloatParameters() {
      return _.filter(ctrl.data.parameters, param => param.type === 'Float');
    }

    function canRelease() {
      return this.state.canRelease;
    }

    function onClickCancel() {
      window.history.back();
    }

    function onVerify() {
      const params = _.map(ctrl.data.parameters, param => (
        {
          parameter_id: param.id,
          increment: getIncrement(param),
          start: param.start,
          end: param.end,
        })
      );
      const payload = {
        parameters: params,
        source: $state.params.script.source,
      };

      PricingScriptsV2.verify(payload).then(success, error);

      function getIncrement(param) {
        if (param.type === 'Float') {
          return param.increment || 1;
        }

        return param.increment;
      }

      function success(data) {
        ctrl.state.generate = 'idle';
        const report = document.createElement('a');
        angular.element(report)
          .attr('href', `data:application/csv;charset=utf-8,${encodeURIComponent(data)}`)
          .attr('download', 'bulk_test_report.csv');
        document.body.appendChild(report);

        if (data.includes('error')) {
          ctrl.state.hasError = true;
        }
        ctrl.state.canRelease = true;
        report.click();
      }

      function error() {
        ctrl.state.generate = 'idle';
      }
    }

    function onRelease() {
      const isActive = $state.params.script.status === 'Active';
      const success = () => {
        $state.go('container.v2-pricing-scripts-active');
        nvToast.info(
          nvTranslate.instant('container.pricing-scripts.message.release-success')
        );
      };

      $state.params.script.parameter_ids = $state.params.activeParameters.map(param => param.id);
      const updatePromise = PricingScriptsV2.update($state.params.script.id, $state.params.script);
      if (isActive) {
        return updatePromise.then(success);
      }
      return updatePromise.then(script => PricingScriptsV2.release(script.id))
        .then(success);
    }
  }
}());
