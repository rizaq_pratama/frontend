(function controller() {
  angular
    .module('nvOperatorApp.controllers')
    .controller('PricingScriptManageController', PricingScriptManageController);

  PricingScriptManageController.$inject = ['$q', '$stateParams', '$scope', '$state', '$timeout',
    'PricingScriptsV2', 'Reservation', 'Address',
    'appSidenav', 'nvCurrency', 'nvTranslate', 'nvToast', 'nvCsvParser', '$filter', 'nvButtonFilePickerType',
    'nvAutocomplete.Data', 'nvPanel', 'nvDialog', 'nvDateTimeUtils', 'nvTimezone', '$rootScope'];

  const TAB_ITEMS = [
    { id: 0, displayName: 'container.pricing-scripts.tab-script-info' },
    { id: 1, displayName: 'container.pricing-scripts.tab-write-script' },
    { id: 2, displayName: 'container.pricing-scripts.tab-check-script' },
  ];

  const EDIT_TAB_ITEMS = [
    { id: 0, displayName: 'container.pricing-scripts.tab-write-script' },
    { id: 1, displayName: 'container.pricing-scripts.tab-check-script' },
    { id: 2, displayName: 'container.pricing-scripts.tab-script-info' },
  ];

  const TIMEBOUNDED_TAB_ITEMS = [
    { id: 0, displayName: 'container.pricing-scripts.tab-time-bounded-script-info' },
    { id: 1, displayName: 'container.pricing-scripts.tab-write-time-bounded-script' },
    { id: 2, displayName: 'container.pricing-scripts.tab-check-script' },
  ];

  const TIMEBOUNDED_EDIT_TAB_ITEMS = [
    { id: 0, displayName: 'container.pricing-scripts.tab-write-time-bounded-script' },
    { id: 1, displayName: 'container.pricing-scripts.tab-check-time-bounded-script' },
    { id: 2, displayName: 'container.pricing-scripts.tab-time-bounded-script-info' },
  ];

  function PricingScriptManageController($q, $stateParams, $scope, $state, $timeout,
                                         PricingScriptsV2, Reservation, Address,
                                         appSidenav, nvCurrency, nvTranslate, nvToast, nvCsvParser,
                                         $filter, nvButtonFilePickerType, nvAutocompleteData,
                                         nvPanel, nvDialog, nvDateTimeUtils, nvTimezone, $rootScope) {
    const ctrl = this;

    const scriptId = $stateParams.scriptId;
    const scriptType = $stateParams.type;
    ctrl.mode = scriptId ? 'EDIT' : 'CREATE';

      // global variable
    ctrl.marketplaceShippers = null;
    ctrl.marketplaceId = null;
    ctrl.originalShipper = null;
    ctrl.originalSettings = null;

    ctrl.mediaType = nvButtonFilePickerType.CSV;

    ctrl.state = {};
    ctrl.data = {};
    ctrl.service = {};
    ctrl.view = {};
    ctrl.function = {};

    ctrl.data.basic = {};
    ctrl.data.more = {};
    ctrl.data.integrations = {};
    ctrl.data.marketplace = {};
    ctrl.data.parameters = [];

    ctrl.state.tab = 0;
    ctrl.state.loading = false;
    ctrl.state.canSubmitDraft = false;
    ctrl.state.canVerifyDraft = false;
    ctrl.state.canRunCheck = false;
    ctrl.state.syntax = 'pending';
    ctrl.state.sourceMessage = 'Run a syntax check before saving or verifying the draft.';

    ctrl.state.runCheck = 'idle';
    ctrl.state.loadScriptTemplate = 'idle';
    ctrl.state.checkSyntax = 'idle';
    ctrl.state.saveDraft = 'idle';

    ctrl.service.fromZoneService = null;
    ctrl.service.toZoneService = null;
    ctrl.service.templateService = null;

    ctrl.aceEditor = null;

    // auto complete stuff
    ctrl.view.textTemplate = null;
    ctrl.view.textFromZone = null;
    ctrl.view.textToZone = null;
    ctrl.view.template = null;
    ctrl.view.templates = null;
    ctrl.view.fromZone = null;
    ctrl.view.toZone = null;

    ctrl.view.result = {};

    ctrl.view.deliveryTypeOptions = PricingScriptsV2.DELIVERY_TYPE_OPTIONS;
    ctrl.view.orderTypeOptions = PricingScriptsV2.ORDER_TYPE_OPTIONS;
    ctrl.view.timeslotTypeOptions = PricingScriptsV2.TIMESLOT_TYPE_OPTIONS;
    ctrl.view.sizeOptions = PricingScriptsV2.SIZE_OPTIONS;
    ctrl.view.currency = nvCurrency.getCode($rootScope.countryId);
    ctrl.view.decimal = { decimal: 2, minValue: 0.00, maxValue: 99999999.99 };
    ctrl.view.legends = PricingScriptsV2.LEGENDS;
    ctrl.view.weightUnit = 'kg';

    ctrl.function.init = init;

    $scope.$on('$destroy', () => {
      appSidenav.setConfigLockedOpen(true);
    });

    ctrl.isTimeBoundedScript = isTimeBoundedScript;
    ctrl.getTitle = getTitle;
    ctrl.getTabs = getTabs;
    ctrl.onTabChanged = onTabChanged;
    ctrl.onSourceChange = onSourceChange;
    ctrl.onParameterChange = onParameterChange;
    ctrl.onCheckSyntax = onCheckSyntax;
    ctrl.onSubmitDraft = onSubmitDraft;
    ctrl.onUpdateDraft = onUpdateDraft;
    ctrl.onDeleteDraft = onDeleteDraft;
    ctrl.onVerifyDraft = onVerifyDraft;
    ctrl.onVerifyActiveScript = onVerifyActiveScript;
    ctrl.retrieveScript = retrieveScript;
    ctrl.getAllParameters = getAllParameters;
    ctrl.onClickCancel = onClickCancel;
    ctrl.canSubmitDraft = canSubmitDraft;
    ctrl.canVerifyDraft = canVerifyDraft;
    ctrl.canRunCheck = canRunCheck;
    ctrl.canSubmit = canSubmit;
    ctrl.canLoadScriptTemplate = canLoadScriptTemplate;
    ctrl.isCreating = isCreating;
    ctrl.isEditing = isEditing;
    ctrl.isActiveScript = isActiveScript;
    ctrl.getInactiveParameters = getInactiveParameters;
    ctrl.getActiveParameters = getActiveParameters;
    ctrl.loadAce = loadAce;
    ctrl.onRunCheck = onRunCheck;
    ctrl.onReset = onReset;
    ctrl.onImport = onImport;
    ctrl.onImportCancel = onImportCancel;
    ctrl.onLoadScriptTemplate = onLoadScriptTemplate;
    ctrl.onOpenToolsActionMenu = onOpenToolsActionMenu;

    function isTimeBoundedScript() {
      return (typeof $stateParams.parentId !== 'undefined' && $stateParams.parentId !== null);
    }

    function getTitle() {
      const action = isCreating() ? 'Create' : 'Edit';
      let entity = isActiveScript() ? 'Script' : 'Draft';
      entity = isTimeBoundedScript() ? 'Time-Bounded Script' : entity;
      return `${action} ${entity}`;
    }

    function getTabs() {
      if (!ctrl.isTimeBoundedScript()) {
        return ctrl.isCreating() ? TAB_ITEMS : EDIT_TAB_ITEMS;
      }
      return ctrl.isCreating() ? TIMEBOUNDED_TAB_ITEMS : TIMEBOUNDED_EDIT_TAB_ITEMS;
    }

    function getValidActions() {
      const items = [
        {
          key: 'SAVE',
          name: 'Save',
          visible: ctrl.isEditing() && !ctrl.isActiveScript(),
          action: ctrl.onUpdateDraft,
        },
        {
          key: 'SAVE_AND_EXIT',
          name: 'Save and Exit',
          visible: ctrl.isEditing() && !ctrl.isActiveScript(),
          action: () => { ctrl.onUpdateDraft(true); },
        },
        {
          key: 'DELETE',
          name: 'Delete',
          visible: ctrl.isEditing(),
          action: ctrl.onDeleteDraft,
        },
      ];

      return _.filter(items, item => item.visible);
    }

    // listeners
    function onTabChanged(index) {
      this.state.tab = index;
    }

    function onSourceChange() {
      ctrl.state.canSubmitDraft = false;
      showMessage('Run a syntax check before saving or verifying the draft.');
    }

    function onParameterChange() {
      ctrl.state.canSubmitDraft = false;
      showMessage('Run a syntax check before saving or verifying the draft.');
    }

    function onCheckSyntax() {
      ctrl.state.checkSyntax = 'waiting';
      ctrl.state.canSubmitDraft = false;
      ctrl.state.canRunCheck = false;
      const payload = _.pick(ctrl.data.script, ['name', 'description', 'source', 'parent_id', 'start_date', 'end_date']);
      payload.type = 'Shipper';
      payload.parameter_ids = ctrl.getActiveParameters().map(param => param.id);
      if (_.isDate(payload.start_date)) {
        payload.start_date = nvDateTimeUtils.displayFormat(
          transformDate(payload.start_date, 'from'), nvDateTimeUtils.FORMAT_LONG_TZ, 'UTC'
        );
      }

      if (_.isDate(payload.end_date)) {
        payload.end_date = nvDateTimeUtils.displayFormat(
          transformDate(payload.end_date), nvDateTimeUtils.FORMAT_LONG_TZ, 'UTC'
        );
      }

      PricingScriptsV2.checkSyntax(payload)
        .then((result) => {
          ctrl.state.sourceMessage = null;
          if (!result.valid) {
            showMessage(result.errors[0], 'error');
            ctrl.state.canSubmitDraft = false;
            ctrl.state.canRunCheck = false;
          } else {
            ctrl.state.canSubmitDraft = true;
            ctrl.state.canRunCheck = true;
            showMessage('No errors found. You may proceed to verify or save the draft.', 'success');
          }
        }, () => (ctrl.state.syntax = 'error'))
        .finally(() => (ctrl.state.checkSyntax = 'idle'));
    }

    function submitDraft() {
      const payload = _.pick(ctrl.data.script, ['name', 'description', 'source', 'parent_id']);

      if (scriptType === PricingScriptsV2.SCRIPT_TYPE.TIME_INBOUNDED) {
        // start date and end data only for time inbounded
        const extras = _.pick(ctrl.data.script, ['start_date', 'end_date']);
        payload.start_date = transformDate(extras.start_date, 'from');
        payload.end_date = transformDate(extras.end_date);
      }
      payload.type = 'Shipper';
      payload.parameter_ids = ctrl.getActiveParameters().map(param => param.id);
      ctrl.state.saveDraft = 'pending';
      ctrl.state.canSubmitDraft = false;
      return PricingScriptsV2.create(payload).then((result) => {
        if (!result.id) {
          ctrl.state.canSubmitDraft = true;
        } else {
          ctrl.state.canSubmitDraft = false;
          ctrl.data.script = transformScript(result);
          ctrl.mode = 'EDIT';
          nvToast.info(nvTranslate.instant('container.pricing-scripts.message.create-success'));
        }
        return result;
      }, (err) => {
        ctrl.state.canSubmitDraft = true;
        return $q.reject(err);
      })
      .finally(() => {
        ctrl.state.saveDraft = 'idle';
      });
    }

    function onSubmitDraft() {
      submitDraft().then(() => {
        goToListPage();
      });
    }

    function onUpdateDraft(exitFlag) {
      const payload = _.pick(ctrl.data.script, ['name', 'description', 'source', 'parent_id']);
      if (scriptType === PricingScriptsV2.SCRIPT_TYPE.TIME_INBOUNDED) {
        // start date and end data only for time inbounded
        const extras = _.pick(ctrl.data.script, ['start_date', 'end_date']);
        payload.start_date = transformDate(extras.start_date, 'from');
        payload.end_date = transformDate(extras.end_date);
      }
      payload.type = 'Shipper';
      payload.parameter_ids = ctrl.getActiveParameters().map(param => param.id);

      PricingScriptsV2.update(ctrl.data.script.id, payload).then((result) => {
        if (!result.id) {
          ctrl.state.canSubmitDraft = true;
        } else {
          ctrl.state.canSubmitDraft = false;
          ctrl.data.script = transformScript(result);
          if (exitFlag) {
            goToListPage();
          }
          nvToast.info(nvTranslate.instant('container.pricing-scripts.message.update-success'));
        }
      }, () => (ctrl.state.canSubmitDraft = false));
    }

    function onDeleteDraft($event) {
      if (isTimeBoundedScript()) {
        nvDialog.confirmDelete($event, {
          title: nvTranslate.instant('container.pricing-scripts.delete-time-bounded-script-title'),
          content: nvTranslate.instant('container.pricing-scripts.delete-time-bounded-script-content'),
          ok: nvTranslate.instant('commons.delete'),
        }).then(done);
      } else {
        nvDialog.confirmDelete($event, {
          title: nvTranslate.instant('container.pricing-scripts.delete-script-title'),
          content: nvTranslate.instant('container.pricing-scripts.delete-script-content'),
          ok: nvTranslate.instant('commons.delete'),
        }).then(done);
      }

      function done() {
        PricingScriptsV2.delete(ctrl.data.script.id)
          .then(() => {
            const deleteMessage = nvTranslate.instant('container.pricing-scripts.message.delete-success');
            nvToast.info(`${ctrl.data.script.name} ${deleteMessage}`);
            goToListPage();
          });
      }
    }

    function onVerifyDraft() {
      // called on create script
      submitDraft().then((result) => {
        $state.go('container.v2-pricing-scripts-verify', { script: result, activeParameters: ctrl.getActiveParameters(), saveBeforeRelease: false });
      });
    }

    function onVerifyActiveScript() {
      // called on edit script
      const script = ctrl.data.script;
      if (_.isDate(script.start_date)) {
        script.start_date = nvDateTimeUtils.displayFormat(
          transformDate(script.start_date, 'from'), nvDateTimeUtils.FORMAT_LONG_TZ, 'UTC'
        );
      }

      if (_.isDate(script.end_date)) {
        script.end_date = nvDateTimeUtils.displayFormat(
          transformDate(script.end_date), nvDateTimeUtils.FORMAT_LONG_TZ, 'UTC'
        );
      }
      $state.go('container.v2-pricing-scripts-verify', { script: script, activeParameters: ctrl.getActiveParameters(), saveBeforeRelease: true });
    }

    function retrieveScript(id) {
      return PricingScriptsV2.read(id)
        .then((result) => {
          ctrl.data.script = transformScript(result);
          $stateParams.parentId = ctrl.data.script.parent_id;
          if (isTimeBoundedScript()) {
            loadParentDetails();
          }
        });
    }

    function transformScript(script) {
      // convert start_date and end_date (string) to date object
      let tobeStartDate = script.start_date;
      let tobeEndDate = script.end_date;
      if (ctrl.isEditing() && _.isString(script.start_date) && _.isString(script.end_date)) {
        // date will be displayed in date picker, remove time to make date correct
        const startMoment = nvDateTimeUtils.toMoment(script.start_date);
        const endMoment = nvDateTimeUtils.toMoment(script.end_date);

        const startDateString =
          nvDateTimeUtils.displayDate(startMoment, nvTimezone.getOperatorTimezone());
        const endDateString =
          nvDateTimeUtils.displayDate(endMoment, nvTimezone.getOperatorTimezone());

        tobeStartDate = startDateString;
        tobeEndDate = endDateString;
      }

      if (script.start_date) {
        script.start_date = nvDateTimeUtils.toDate(tobeStartDate);
      }

      if (script.end_date) {
        script.end_date = nvDateTimeUtils.toDate(tobeEndDate);
      }
      return script;
    }

    function getAllParameters() {
      return PricingScriptsV2.getAllParameters()
        .then((result) => {
          ctrl.data.parameters = result;
        });
    }

    function onClickCancel() {
      goToListPage();
    }

    function canSubmitDraft() {
      return ctrl.state.canSubmitDraft;
    }

    function canVerifyDraft() {
      return ctrl.state.canSubmitDraft;
    }

    function canRunCheck() {
      return ctrl.state.canRunCheck;
    }

    function canSubmit() {
      return ctrl.state.canSubmit;
    }

    function canLoadScriptTemplate() {
      return ctrl.view.template !== null;
    }

    function onLoadScriptTemplate() {
      // ctrl.state.load = 'waiting';
      const selectedTemplate = ctrl.view.template;
      PricingScriptsV2.read(selectedTemplate.value.id).then(success, error);

      function success(result) {
        ctrl.state.load = 'idle';

        ctrl.view.template = null;
        ctrl.view.textTemplate = null;
        ctrl.service.templateService.removeAll();

        ctrl.data.script.source = result.source;
        showMessage(`Success load template ${selectedTemplate.value.name}`);
      }

      function error() {
        ctrl.state.load = 'idle';
        showMessage(`Fail to load template ${selectedTemplate.value.name}`, 'error');
      }
    }

    function isCreating() {
      return ctrl.mode === 'CREATE';
    }

    function isEditing() {
      return ctrl.mode === 'EDIT';
    }

    function isActiveScript() {
      return ctrl.data.script !== null && ctrl.data.script.status === 'Active';
    }

    function getInactiveParameters() {
      return ctrl.data.parameters.filter(parameter => !parameter.selected);
    }

    function getActiveParameters() {
      return ctrl.data.parameters.filter(parameter => parameter.selected);
    }

    function goToListPage() {
      if (isTimeBoundedScript()) {
        $state.go('container.v2-pricing-scripts-list-time-bounded', { parentId: $stateParams.parentId });
      } else if (ctrl.data.script.status === 'Active') {
        $state.go('container.v2-pricing-scripts-active');
      } else {
        $state.go('container.v2-pricing-scripts-drafts');
      }
    }

    function loadAce(editor) {
      const path = 'assets/ace-ui';
      const config = window.ace.require('ace/config');
      config.set('modePath', path);
      config.set('themePath', path);

      const session = editor.getSession();
      const renderer = editor.renderer;

      session.setUndoManager(new ace.UndoManager());
      editor.setTheme('ace/theme/twilight');
      editor.getSession().setMode('ace/mode/javascript');
      editor.$blockScrolling = Infinity;

      renderer.setShowGutter(true);
      ctrl.aceEditor = editor;
    }

    // CHECK SCRIPT TAB
    function onRunCheck() {
      ctrl.state.runCheck = 'waiting';
      const payload = {
        script: {
          source: ctrl.aceEditor.getValue(),
        },
        params: {
          delivery_type: ctrl.view.deliveryType,
          timeslot_type: ctrl.view.timeslotType,
          order_type: ctrl.view.orderType,
          weight: ctrl.view.weight,
          size: ctrl.view.size,
          insured_value: ctrl.view.insuredValue,
          cod_value: ctrl.view.codValue,
          from_billing_zone: ctrl.view.fromZone ? ctrl.view.fromZone.value : undefined,
          to_billing_zone: ctrl.view.toZone ? ctrl.view.toZone.value : undefined,
        },
      };

      PricingScriptsV2.test(payload).then(success, error);

      function success(response) {
        ctrl.state.runCheck = 'idle';
        ctrl.state.updated = true;

        const pricingResult = response.result;
        ctrl.view.result.total_with_tax = prefixCurrency(pricingResult.total_with_tax);
        ctrl.view.result.total_tax = prefixCurrency(pricingResult.total_tax);
        ctrl.view.result.deliveryFee = prefixCurrency(pricingResult.delivery_fee.amount);
        ctrl.view.result.codFee = prefixCurrency(pricingResult.cod_fee.amount);
        ctrl.view.result.insuranceFee = prefixCurrency(pricingResult.insurance_fee.amount);
        ctrl.view.result.handlingFee = prefixCurrency(pricingResult.handling_fee.amount);

        ctrl.view.result.comments = 'OK';

        $timeout(() => (ctrl.state.updated = false), 4000);
      }

      function error(result) {
        ctrl.state.runCheck = 'idle';

        ctrl.view.result.total_with_tax = prefixCurrency('-');
        ctrl.view.result.total_tax = prefixCurrency('-');
        ctrl.view.result.deliveryFee = prefixCurrency('-');
        ctrl.view.result.codFee = prefixCurrency('-');
        ctrl.view.result.insuranceFee = prefixCurrency('-');
        ctrl.view.result.handlingFee = prefixCurrency('-');

        if (result.error) {
          ctrl.view.result.comments = result.error.message;
        } else {
          ctrl.view.result.comments = 'unknown error';
        }
      }
    }

    function onReset() {
      ctrl.view.deliveryType = ctrl.view.deliveryTypeOptions[0].value;
      ctrl.view.timeslotType = ctrl.view.timeslotTypeOptions[0].value;
      ctrl.view.orderType = ctrl.view.orderTypeOptions[0].value;
      ctrl.view.weight = null;
      ctrl.view.size = ctrl.view.sizeOptions[0].value;
      ctrl.view.insuredValue = 0;
      ctrl.view.codValue = 0;
      ctrl.view.textFromZone = null;
      ctrl.view.textToZone = null;
      ctrl.view.fromZone = null;
      ctrl.view.toZone = null;
      ctrl.view.result.comments = null;
      ctrl.view.result.total_with_tax = null;
      ctrl.view.result.total_tax = null;
      ctrl.view.result.deliveryFee = null;
      ctrl.view.result.codFee = null;
      ctrl.view.result.insuranceFee = null;
      ctrl.view.result.handlingFee = null;
      ctrl.service.fromZoneService.removeAll();
      ctrl.service.toZoneService.removeAll();
    }

    function onImport(selectedFile) {
      let file = null;
      let reader = null;
      if (window.File && window.FileReader && window.FileList && window.Blob) {
        reader = new FileReader();
        file = selectedFile;

        if ((file != null) && (file.size < 2 * 1024 * 1024)) {
          reader.onload = onFileLoaded;
          reader.readAsText(file);
        } else if (file != null && (file.size > 2 * 1024 * 1024)) {
          showMessage('file size need to less than 2MB', 'error');
        } else {
          // file is null
          showMessage('file not available', 'error');
        }
      } else {
        showMessage('error due to browser API limitation', 'error');
      }

      function onFileLoaded() {
        const preProcessedText = reader.result;
        // let lineEncoding = 'LF';
        let stopCharacter = '\n';
        let skipChar = 1;

        if (preProcessedText.indexOf('\r\n') > 0) {
          // lineEncoding = 'CRLF';
          stopCharacter = '\r\n';
          skipChar = 2;
        }

        const header = preProcessedText.substr(0, preProcessedText.indexOf(stopCharacter));
        let newHeader = '';
        const body = preProcessedText.substr(preProcessedText.indexOf(stopCharacter)
          + skipChar, preProcessedText.length - 1);

        if (validateHeader(header, stopCharacter)) {
          const filteredCsv = newHeader + body;
          const cfg = {
            dynamicTyping: true,
            header: true,
          };
          nvCsvParser.parse(filteredCsv, cfg).then(onComplete, onError);
        } else {
          showMessage('CSV Header contain invalid character, accept ([A-Z],[a-z],space)', 'error');
        }

        function validateHeader(csvHeader, sc) {
          const headerElements = csvHeader.split(',');
          let errCode = 0;
          for (let i = 0; i < headerElements.length; i++) {
            if ((/[^a-zA-Z\d\s:]/).test(headerElements[i])) {
              nvToast.warning('Warn', `invalid header : ${headerElements[i]}, Col ${i}`);
              errCode = 1;
              break;
            }
            headerElements[i] = headerElements[i].replace(/\d/g, '').replace(/(.)([A-Z])/g, '$1 $2');
            newHeader += $filter('nvNaturalCase2CamelCase')(headerElements[i]);
            if (i === headerElements.length - 1) {
              newHeader += sc;
            } else {
              newHeader += ',';
            }
          }
          if (errCode === 0) {
            return true;
          }
          return false;
        }
      }

      function onComplete(result) {
        const data = result.data;
        const rawJson = JSON.stringify(data);

        const processedJson = rawJson
          .replace(new RegExp('},', 'g'), '},\n\t')
          .replace('[{', '[\n\t{')
          .replace('}]', '}\n]');

        const startString = '\n\n// Please rename "zonalRates" to something more descriptive - e.g. "nextDayBaseRates". \n// Note that if you import multiple CSVs into a script, each variable must have a unique name.\n';
        const resultString = `${startString}var zonalRates = ${processedJson};`;

        ctrl.data.script.source += resultString;

        showMessage(`Success load CSV ${file.name}`);
      }

      function onError() {
        showMessage(`Error load CSV ${file.name}`, 'error');
      }
    }

    function onImportCancel() {
      showMessage('Error on open file', 'error');
    }

    function onOpenToolsActionMenu($event) {
      const actions = getValidActions();
      nvPanel.show($event, {
        controller: 'ManageScriptToolsMenuController',
        templateUrl: 'views/container/pricing-scripts-v2/panel/manage-script/manage-script.panel.html',
        panelClass: 'polygon-tools-menu',
        darkTheme: true,
        xPosition: 'ALIGN_END',
        locals: {
          onClose: null,
          manageScriptActions: actions,
        },
      });
    }

    function showMessage(message, type) {
      if (type === 'error') {
        ctrl.state.syntax = 'error';
      } else if (type === 'success') {
        ctrl.state.syntax = 'success';
      } else {
        ctrl.state.syntax = 'pending';
      }
      ctrl.state.sourceMessage = message;
    }

    function prefixCurrency(string) {
      return `${nvCurrency.getCode($rootScope.countryId)} ${string}`;
    }

    init();
    function init() {
      ctrl.data.script = {
        source: 'function calculatePricing(params) {\n' +
        '    var price = 0.0;\n' +
        '    var result = {};\n' +
        '    result.delivery_fee = price;\n' +
        '    result.cod_fee = 0.0;\n' +
        '    result.insurance_fee = 0.0;\n' +
        '    result.handling_fee = 0.0;\n' +
        '    return result;\n' +
        '}',
      };

      loadParentDetails();

      if (ctrl.mode === 'EDIT') {
        ctrl.retrieveScript($stateParams.scriptId)
          .then(() => ctrl.getAllParameters())
          .then(() => {
            const activeParams = {};
            ctrl.data.script.parameter_ids.forEach((activeParameterId) => {
              activeParams[activeParameterId] = true;
            });
            ctrl.data.parameters.forEach((parameter) => {
              if (activeParams[parameter.id]) {
                parameter.selected = true;
              }
            });
            ctrl.onCheckSyntax();
          });
      } else {
        ctrl.getAllParameters();
      }

      initAutocomplete();
    }

    function loadParentDetails() {
      if (isTimeBoundedScript()) {
        ctrl.data.script.parent_id = $stateParams.parentId;

        PricingScriptsV2.read(ctrl.data.script.parent_id)
          .then((result) => {
            ctrl.data.parent_script = result;
          });
      }
    }

    function initAutocomplete() {
      $q.all([
        PricingScriptsV2.getBillingZones(),
        PricingScriptsV2.readAllActive(),
      ]).then(success, error);

      function success(results) {
        const zones = results[0];
        const scripts = results[1];
        ctrl.view.zones = zones;
        ctrl.view.templates = scripts;

        ctrl.service.templateService = nvAutocompleteData.getByHandle('script-template');
        ctrl.service.fromZoneService = nvAutocompleteData.getByHandle('script-from-zone');
        ctrl.service.toZoneService = nvAutocompleteData.getByHandle('script-to-zone');

        ctrl.service.templateService.setPossibleOptions(_.map(ctrl.view.templates, template => ({
          value: template,
          displayName: template.name,
        })));
        ctrl.service.fromZoneService.setPossibleOptions(_.map(ctrl.view.zones, zone => ({
          value: zone,
          displayName: zone,
        })));
        ctrl.service.toZoneService.setPossibleOptions(_.map(ctrl.view.zones, zone => ({
          value: zone,
          displayName: zone,
        })));
      }

      function error() {
        // ctrl.state.page = STATES.ERROR;
      }
    }

    function transformDate(date, type) {
      if (date === null) {
        return null;
      }

      if (type === 'from') {
        return nvDateTimeUtils.toSOD(date, nvTimezone.getOperatorTimezone());
      }
      return nvDateTimeUtils.toEOD(date, nvTimezone.getOperatorTimezone());
    }
  }
}());
