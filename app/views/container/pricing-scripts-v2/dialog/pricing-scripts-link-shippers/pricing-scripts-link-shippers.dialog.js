(function controller() {
  angular
    .module('nvOperatorApp.controllers')
    .controller('PricingScriptsV2LinkShippersDialogController', PricingScriptsV2LinkShippersDialogController);

  PricingScriptsV2LinkShippersDialogController.$inject = ['$mdDialog', 'originData',
  'PricingScriptsV2', '$q', 'nvAutocomplete.Data', 'nvTableUtils', 'NgTableParams', 'Shippers'];

  const STATES = {
    LOADING: 0,
    CONTENT: 1,
    ERROR: 2,
  };

  function PricingScriptsV2LinkShippersDialogController($mdDialog, originData,
  PricingScriptsV2, $q, nvAutocompleteData, nvTableUtils, NgTableParams, Shippers) {
    const ctrl = this;
    const SHIPPER_STATE = {
      DEFAULT: 'default',
      ADD: 'add',
    };

    let linkedShippers = [];

    ctrl.SHIPPER_STATE = SHIPPER_STATE;
    ctrl.scriptId = originData;

    ctrl.state = {};
    ctrl.view = {};
    ctrl.service = {};
    ctrl.func = {};

    ctrl.state.save = 'idle';
    ctrl.state.page = STATES.LOADING;

    ctrl.view.name = null;
    ctrl.view.shipperTableParam = null;

    // autocomplete stuff
    ctrl.view.textShipper = null;

    // autocomplete service
    ctrl.service.shippersService = null;

    ctrl.func.onCancel = onCancel;
    ctrl.func.onSave = onSave;
    ctrl.func.isAbleToSave = isAbleToSave;
    ctrl.func.onUndo = onUndo;
    ctrl.func.onAdd = onAdd;
    ctrl.func.getSelectedShippersLength = getSelectedShippersLength;
    ctrl.func.onNothing = angular.noop;

    init();

    function init() {
      $q.all([
        PricingScriptsV2.read(ctrl.scriptId),
        PricingScriptsV2.getLinkedShippers(ctrl.scriptId),
      ]).then(success, error);

      function success(results) {
        const script = results[0];
        const linkedShipperIds = results[1];

        linkedShippers = _.map(linkedShipperIds, globalShipperId => ({
          // pricing script service returns global id
          // so we have to set id to global id instead of legacy id
          id: globalShipperId,

          // for Shippers.filterSearch to use
          global_id: globalShipperId,

          // custom variables
          displayName: globalShipperId, // name will get refresh in processSelectedShippers() fn
          state: SHIPPER_STATE.DEFAULT,
          changed: false,
        }));
        processSelectedShippers().then(continueFn);

        function continueFn() {
          ctrl.view.name = script.name;

          ctrl.service.shippersService = nvAutocompleteData.getByHandle('linked-shippers');
          ctrl.service.shippersService.setPossibleOptions(getShippers);
          ctrl.service.shippersService.setSelectedOptions(linkedShippers);

          ctrl.view.shipperTableParam = new NgTableParams(
            { sorting: { state: 'desc' }, count: 1000 },
            { counts: [], dataset: ctrl.service.shippersService.getSelectedOptions() }
          );

          ctrl.state.page = STATES.CONTENT;
        }

        function getShippers(text) {
          return Shippers.filterSearch(text, this.getSelectedOptions(), 'global_id');
        }

        function processSelectedShippers() {
          return Shippers.searchByIds(
            _.map(linkedShippers, 'id'),
            Shippers.SEARCH_BY.global_id
          ).then(searchSuccess);

          function searchSuccess(shippers) {
            const inactiveShipperIds = [];

            _.forEach(linkedShippers, (shipper) => {
              const theShipper = _.find(shippers, ['global_id', shipper.id]);

              if (theShipper) {
                shipper.displayName = `${theShipper.id}-${theShipper.name}`;
              } else {
                inactiveShipperIds.push(shipper.id);
              }
            });

            linkedShippers = _.filter(linkedShippers, shipper =>
              !_.includes(inactiveShipperIds, shipper.id)
            );
          }
        }
      }

      function error() {
        ctrl.state.page = STATES.ERROR;
      }
    }

    function onCancel() {
      $mdDialog.cancel();
    }

    function onSave() {
      ctrl.state.save = 'waiting';

      const selection = ctrl.service.shippersService.getSelectedOptions();
      const changedShippers = _(selection)
        .filter(shipper => shipper.state === SHIPPER_STATE.ADD)
        .map(shipper => ({ id: shipper.global_id, script_id: ctrl.scriptId })).value();

      if (changedShippers.length > 0) {
        const shipperIds = _.map(changedShippers, shipper => shipper.id);
        PricingScriptsV2.linkShippers(ctrl.scriptId, shipperIds, true)
          .then(success);
      } else {
        success();
      }

      function success() {
        ctrl.state.save = 'idle';
        $mdDialog.hide(ctrl.scriptId);
      }
    }

    function isAbleToSave() {
      if (ctrl.service.shippersService) {
        const selection = ctrl.service.shippersService.getSelectedOptions();
        return selection.length > 0;
      }
      return false;
    }

    function onUndo(shipper) {
      _.remove(linkedShippers, ['id', shipper.id]);
      ctrl.view.shipperTableParam.reload();
    }

    function onAdd(item) {
      const linkedShipper = _.find(linkedShippers, ['global_id', item.global_id]);
      if (linkedShipper) {
        linkedShipper.id = linkedShipper.global_id;
        linkedShipper.state = SHIPPER_STATE.ADD;
        linkedShipper.changed = true;
        linkedShipper.displayName = `${linkedShipper.legacy_id}-${linkedShipper.name}`;
      }

      ctrl.view.shipperTableParam.reload();
    }

    function getSelectedShippersLength() {
      return getSelectedShippers().length;
    }

    function getSelectedShippers() {
      return ctrl.service.shippersService.getSelectedOptions();
    }
  }
}());

