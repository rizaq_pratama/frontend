(function controller() {
  angular
    .module('nvOperatorApp.controllers')
    .controller('PricingScriptsV2UpdatePastDialogController', PricingScriptsV2UpdatePastDialogController);

  PricingScriptsV2UpdatePastDialogController.$inject = ['$mdDialog', 'originData', 'Shippers',
  '$q', 'PricingScriptsV2', 'nvAutocomplete.Data', 'nvDateTimeUtils', 'Order', 'nvToast', '$scope'];

  const STATES = {
    LOADING: 0,
    CONTENT: 1,
    ERROR: 2,
  };

  function PricingScriptsV2UpdatePastDialogController($mdDialog, originData, Shippers,
  $q, PricingScriptsV2, nvAutocompleteData, nvDateTimeUtils, Order, nvToast, $scope) {
    const ctrl = this;

    ctrl.scriptId = originData;

    ctrl.state = {};
    ctrl.view = {};
    ctrl.service = {};
    ctrl.func = {};

    ctrl.state.apply = 'idle';
    ctrl.state.page = STATES.LOADING;

    ctrl.view.id = null;
    ctrl.view.name = null;
    ctrl.view.lastModified = null;
    ctrl.view.selectedShippers = null;
    ctrl.view.startDate = new Date();
    ctrl.view.endDate = new Date();
    ctrl.view.hint = 'This applies the current script to orders that occurred before the script was updated. Prices for orders palced in the date range below will be placed';
    ctrl.view.shipperMode = '10';
    ctrl.view.shipperLabel = 'container.pricing-scripts.selected-shippers';
    ctrl.view.totalShippers = 0;

    // autocomplete stuff
    ctrl.view.textShipper = null;

    // autocomplete service
    ctrl.service.shippersService = null;

    ctrl.func.onCancel = onCancel;
    ctrl.func.onApply = onApply;
    ctrl.func.isAbleToApply = isAbleToApply;
    ctrl.func.onAdd = onAdd;
    ctrl.func.removeShipper = removeShipper;

    init();

    function init() {
      $scope.$watch('ctrl.view.shipperMode', (val) => {
        if (val === '10') {
          ctrl.view.shipperLabel = 'container.pricing-scripts.selected-shippers';
        } else {
          ctrl.view.shipperLabel = 'container.pricing-scripts.excluded-shippers';
        }
      });

      $q.all([
        PricingScriptsV2.read(ctrl.scriptId),
        PricingScriptsV2.getLinkedShippers(ctrl.scriptId),
      ]).then(success, error);

      function success(results) {
        const script = results[0];
        const linkedShipperIds = results[1];

        ctrl.view.id = script.id;
        ctrl.view.name = script.name;
        ctrl.view.lastModified = getLastModified(script);

        const linkedShippers = _.map(linkedShipperIds, globalId => ({
          id: globalId,
          legacy_id: globalId, // legacy_id will get refresh in processSelectedShippers() fn
          displayName: globalId, // name will get refresh in processSelectedShippers() fn
        }));
        processSelectedShippers().then(continueFn);

        function continueFn() {
          ctrl.view.totalShippers = _.size(linkedShippers);

          ctrl.service.shippersService = nvAutocompleteData.getByHandle('linked-shippers');
          ctrl.service.shippersService.setPossibleOptions(linkedShippers);

          ctrl.view.selectedShippers = ctrl.service.shippersService.getSelectedOptions();
          ctrl.state.page = STATES.CONTENT;
        }

        function processSelectedShippers() {
          return Shippers.elasticCompactSearch({
            ids: _.map(linkedShippers, 'id'),
            size: _.size(linkedShippers),
          }).then(readCompactSuccess);

          function readCompactSuccess(response) {
            const shippers = _.get(response, 'details');

            _.forEach(linkedShippers, (shipper) => {
              const theShipper = _.find(shippers, ['global_id', shipper.id]);

              if (theShipper) {
                shipper.legacy_id = theShipper.id;
                shipper.displayName = `${theShipper.id}-${theShipper.name}`;
              } else {
                shipper.toRemove = true;
              }
            });

            _.remove(linkedShippers, ['toRemove', true]);
          }
        }
      }

      function error() {
        ctrl.state.page = STATES.ERROR;
      }
    }

    function onCancel() {
      $mdDialog.cancel();
    }

    function onApply() {
      let shippers = null;
      if (ctrl.view.shipperMode === '10') {
        shippers = ctrl.service.shippersService.getSelectedOptions();
      } else {
        shippers = ctrl.service.shippersService.getPossibleOptions();
      }

      if (shippers.length === 0) {
        nvToast.warning('Please select minimum 1 shipper');
        ctrl.state.apply = 'idle';
        return;
      }

      const startDate = nvDateTimeUtils.toSOD(ctrl.view.startDate);
      const endDate = nvDateTimeUtils.toEOD(ctrl.view.endDate);

      const payload = {
        script_id: ctrl.view.id,
        shipper_ids: _.map(shippers, shipper => shipper.legacy_id),
        from: nvDateTimeUtils.displayDateTime(startDate),
        to: nvDateTimeUtils.displayDateTime(endDate),
      };

      Order.scheduleRecalculate(payload).then(success, error);

      function success() {
        nvToast.success('Price update for requested shipper is scheduled');
      }

      function error() {
        nvToast.info('Price update for requested error, please try again');
      }
    }

    function isAbleToApply() {
      if (!ctrl.service.shippersService) {
        return false;
      }

      let shippers = null;
      if (ctrl.view.shipperMode === '10') {
        shippers = ctrl.service.shippersService.getSelectedOptions();
      } else {
        shippers = ctrl.service.shippersService.getPossibleOptions();
      }

      if (shippers.length > 0) {
        return true;
      }
      return false;
    }

    function getLastModified(script) {
      if (script.updated_at) {
        return nvDateTimeUtils.displayDateTime(moment(Number(script.updated_at)));
      }
      return nvDateTimeUtils.displayDateTime(moment(Number(script.created_at)));
    }

    function onAdd() {
      angular.noop();
    }

    function removeShipper(shipper) {
      ctrl.service.shippersService.remove(shipper);
    }
  }
}());

