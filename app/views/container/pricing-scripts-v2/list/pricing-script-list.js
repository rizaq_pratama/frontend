(function controller() {
  angular
    .module('nvOperatorApp.controllers')
    .controller('PricingV2Controller', PricingV2Controller);

  PricingV2Controller.$inject = ['$scope', 'PricingScriptsV2', 'appSidenav', '$state', 'nvTable', 'nvDateTimeUtils', 'nvDialog', '$stateParams'];

  const TAB_ITEMS = [
    { id: 0, displayName: 'container.pricing-scripts.active-scripts' },
    { id: 1, displayName: 'container.pricing-scripts.drafts' },
  ];

  function PricingV2Controller($scope, PricingScriptsV2, appSidenav, $state, nvTable,
                               nvDateTimeUtils, nvDialog, $stateParams) {
    const ctrl = this;
    ctrl.function = {};
    ctrl.state = {};
    ctrl.view = {};
    ctrl.data = {};

    const status = $stateParams.status;

    ctrl.view.tabs = TAB_ITEMS;

    ctrl.state.tab = status === 'Active' ? 0 : 1;
    ctrl.state.loading = false;


    ctrl.data.activeScripts = [];
    ctrl.data.draftScripts = [];

    ctrl.function.init = init;

    ctrl.activeScriptsTableParam = nvTable.createTable(PricingScriptsV2.FIELDS);
    ctrl.activeScriptsTableParam.totalItems = 0;

    ctrl.draftScriptsTableParam = nvTable.createTable(PricingScriptsV2.FIELDS);
    ctrl.draftScriptsTableParam.totalItems = 0;

    ctrl.onTabChanged = onTabChanged;
    ctrl.onClickCreateDraft = onClickCreateDraft;
    ctrl.onClickEditScript = onClickEditScript;
    ctrl.loadDraftScripts = loadDraftScripts;
    ctrl.loadActiveScripts = loadActiveScripts;
    ctrl.showLinkShippersDialog = showLinkShippersDialog;
    ctrl.showUpdatePastDialog = showUpdatePastDialog;
    ctrl.onClickManageTimeBounded = onClickManageTimeBounded;

    init();

    function onTabChanged(index) {
      if (index === 0) {
        $state.go('container.v2-pricing-scripts-active');
      } else if (index === 1) {
        $state.go('container.v2-pricing-scripts-drafts');
      }
    }

    function onClickCreateDraft() {
      $state.go('container.v2-pricing-scripts-create', { type: PricingScriptsV2.SCRIPT_TYPE.NORMAL });
    }

    function onClickEditScript(scriptId) {
      $state.go('container.v2-pricing-scripts-edit', { scriptId: scriptId, type: PricingScriptsV2.SCRIPT_TYPE.NORMAL });
    }

    function loadDraftScripts() {
      ctrl.state.loading = true;
      ctrl.draftScriptsTableParam = nvTable.createTable(PricingScriptsV2.FIELDS);
      PricingScriptsV2.readAllDrafts()
        .then((result) => {
          ctrl.data.draftScripts = result;
          ctrl.draftScriptsTableParam.setData(formatScriptList(ctrl.data.draftScripts));
        })
        .finally(() => (ctrl.state.loading = false));
    }

    function loadActiveScripts() {
      ctrl.state.loading = true;
      ctrl.activeScriptsTableParam = nvTable.createTable(PricingScriptsV2.FIELDS);
      PricingScriptsV2.readAllActive()
        .then((result) => {
          ctrl.data.activeScripts = result;
          ctrl.activeScriptsTableParam.setData(formatScriptList(ctrl.data.activeScripts));
        })
        .finally(() => (ctrl.state.loading = false));
    }

    function showLinkShippersDialog($event, scriptId) {
      nvDialog.showSingle($event, {
        templateUrl: 'views/container/pricing-scripts-v2/dialog/pricing-scripts-link-shippers/pricing-scripts-link-shippers.dialog.html',
        cssClass: 'pricing-script-v2-linked-shippers-dialog',
        controller: 'PricingScriptsV2LinkShippersDialogController',
        controllerAs: 'ctrl',
        locals: {
          originData: scriptId,
        },
      }).then(onSet);

      function onSet() {
        loadActiveScripts();
      }
    }

    function showUpdatePastDialog($event, scriptId) {
      nvDialog.showSingle($event, {
        templateUrl: 'views/container/pricing-scripts-v2/dialog/pricing-scripts-update-past/pricing-scripts-update-past.dialog.html',
        cssClass: 'pricing-script-v2-update-past-dialog',
        controller: 'PricingScriptsV2UpdatePastDialogController',
        controllerAs: 'ctrl',
        locals: {
          originData: scriptId,
        },
      });
    }

    function onClickManageTimeBounded(scriptId) {
      $state.go('container.v2-pricing-scripts-list-time-bounded', { parentId: scriptId });
    }

    function setCustomScriptData(script) {
      const result = _.clone(script);
      result.lastModified = getLastModified(result);
      return result;
    }

    function formatScriptList(list) {
      return _.map(list, script => setCustomScriptData(script));
    }

    function init() {
      if (ctrl.state.tab === 0) {
        ctrl.loadActiveScripts();
      } else {
        ctrl.loadDraftScripts();
      }

      $scope.$on('$destroy', () => {
        appSidenav.setConfigLockedOpen(true);
      });
    }

    function getLastModified(script) {
      if (script.updated_at) {
        return nvDateTimeUtils.displayDateTime(moment(script.updated_at));
      }
      return '-';
    }
  }
}());
