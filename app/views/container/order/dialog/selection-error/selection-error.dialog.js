(function controller() {
  angular
    .module('nvOperatorApp.controllers')
    .controller('OrderSelectionErrorDialogController', OrderSelectionErrorDialogController);

  OrderSelectionErrorDialogController.$inject = [
    '$mdDialog', 'ordersValidationErrorData',
  ];

  function OrderSelectionErrorDialogController(
    $mdDialog, ordersValidationErrorData
  ) {
    // variables
    const ctrl = this;
    ctrl.ordersValidationErrorData = ordersValidationErrorData;

    // functions
    ctrl.onCancel = onCancel;
    ctrl.onProceed = onProceed;

    // functions details
    function onCancel() {
      $mdDialog.cancel();
    }

    function onProceed() {
      $mdDialog.hide();
    }
  }
}());
