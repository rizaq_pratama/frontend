(function controller() {
  angular
    .module('nvOperatorApp.controllers')
    .controller('PrintAirwayBillsDialogController', PrintAirwayBillsDialogController);

  PrintAirwayBillsDialogController.$inject = [
    '$mdDialog', 'ordersData', 'nvTable', 'Order', 'nvDialog',
    'nvToast', 'nvTranslate',
  ];

  function PrintAirwayBillsDialogController(
    $mdDialog, ordersData, nvTable, Order, nvDialog,
    nvToast, nvTranslate
  ) {
    // variables
    let bulkActionProgressPayload;

    const ctrl = this;
    const BULK_PRINTING_SIZE = 40; // max orders in one AWB

    ctrl.printingSizeSelectionOptions = [
      { displayName: 'A4', value: 'A4' },
      { displayName: 'A5', value: 'A5' },
      { displayName: 'A6', value: 'A6' },
    ];
    ctrl.printingSize = ctrl.printingSizeSelectionOptions[0].value;

    // functions
    ctrl.onCancel = onCancel;
    ctrl.getSelectedCount = getSelectedCount;
    ctrl.printAirwayBills = printAirwayBills;
    ctrl.bulkPrintAirwayBills = bulkPrintAirwayBills;
    ctrl.getFileName = getFileName;

    // start
    initialize();

    // functions details
    function initialize() {
      ctrl.ordersToChunksTableParams = nvTable.createTable({
        id: { displayName: 'container.order.list.filename' },
        total: { displayName: 'container.order.list.total-orders' },
      });

      const ordersToChunks = _.chunk(ordersData, BULK_PRINTING_SIZE);
      const extendedOrdersToChunks = [];
      _.forEach(ordersToChunks, (orderChunk, index) => {
        extendedOrdersToChunks.push(extendCustomOrderChunkData(index, orderChunk));
      });
      ctrl.ordersToChunksTableParams.setData(extendedOrdersToChunks);
    }

    function onCancel() {
      $mdDialog.cancel();
    }

    function getSelectedCount() {
      return ctrl.ordersToChunksTableParams.getSelection().length;
    }

    function printAirwayBills(ordersToChunk) {
      ordersToChunk.isDownloading = true;

      Order.downloadAWB(
        getRequestPayload(ordersToChunk),
        getFileName(ordersToChunk.id)
      ).finally(finallyFn);

      function finallyFn() {
        ordersToChunk.isDownloading = false;
      }
    }

    function bulkPrintAirwayBills($event) {
      const ordersToChunksToProcess = _.cloneDeep(ctrl.ordersToChunksTableParams.getSelection());
      bulkActionProgressPayload = {
        totalCount: _.size(ordersToChunksToProcess),
        currentIndex: 0,
        errors: [],
        messages: {
          updating: 'container.order.list.downloading-x-of-y-files',
          updated: 'container.order.list.downloaded-x-of-y-files',
          error: 'container.order.list.failed-to-download-x-files',
        },
      };

      nvDialog.showSingle($event, {
        templateUrl: 'lib/ninja-commons/services/dialogs/bulk-action-progress/bulk-action-progress.dialog.html',
        cssClass: 'bulk-action-progress',
        controller: 'BulkActionProgressDialogController',
        controllerAs: 'ctrl',
        skipHide: true,
        clickOutsideToClose: false,
        locals: {
          payload: bulkActionProgressPayload,
        },
      }).then(progressDialogClosed);

      startBulkPrinting(ordersToChunksToProcess);

      function progressDialogClosed() {
        if (getSuccessCount() > 0) {
          nvToast.info(
            nvTranslate.instant('container.order.list.num-files-downloaded', { num: getSuccessCount() })
          );
        }
      }

      function getSuccessCount() {
        return bulkActionProgressPayload.totalCount - bulkActionProgressPayload.errors.length;
      }

      function startBulkPrinting(ordersToChunks) {
        let ordersToChunkToProcess;
        if (ordersToChunks.length > 0) {
          bulkActionProgressPayload.currentIndex += 1;
          ordersToChunkToProcess = ordersToChunks.splice(0, 1)[0];

          Order.downloadAWB(
            getRequestPayload(ordersToChunkToProcess),
            getFileName(ordersToChunkToProcess.id)
          ).then(success, failure);

          nvToast.remove();
        }

        function success() {
          nvToast.remove();
          bulkActionProgressPayload.successCount += 1;

          if (_.size(ordersToChunks) > 0) {
            startBulkPrinting(ordersToChunks); // recursive
          }
        }

        function failure() {
          nvToast.remove();
          bulkActionProgressPayload.errors.push(
            getFileName(ordersToChunkToProcess.id)
          );

          if (_.size(ordersToChunks) > 0) {
            startBulkPrinting(ordersToChunks); // recursive
          }
        }
      }
    }

    function getRequestPayload(ordersToChunk) {
      return {
        tids: _.toString(ordersToChunk.trackingIds),
        h: 0,
        s: ctrl.printingSize,
      };
    }

    function getFileName(id) {
      return `awb_${_.toLower(ctrl.printingSize)}_page_${id}.pdf`;
    }

    function extendCustomOrderChunkData(index, ordersToChunk) {
      const id = index + 1;

      return {
        id: id,
        total: _.size(ordersToChunk),
        trackingIds: _.map(ordersToChunk, 'trackingId'),
        isDownloading: false,
      };
    }
  }
}());
