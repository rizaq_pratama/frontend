(function controller() {
  angular
    .module('nvOperatorApp.controllers')
    .controller('OrderEditDpManagementDialogController', OrderEditDpManagementDialogController);

  OrderEditDpManagementDialogController.$inject = ['$mdDialog', 'orderData', 'DistributionPoint',
    'nvAutocomplete.Data', 'Order', 'Transaction', 'nvToast', 'nvTranslate', 'nvExtendUtils', 'nvTimezone',
    '$timeout', 'nvDateTimeUtils'];

  const LCE_VIEW = {
    LOADING: 'loading',
    ERROR: 'error',
    IDLE: 'idle',
  };

  const API_STATE = {
    WAITING: 'waiting',
    IDLE: 'idle',
  };

  const EMPTY_ARRAY = [];

  function OrderEditDpManagementDialogController($mdDialog, orderData, DistributionPoint,
    nvAutocompleteData, Order, Transaction, nvToast, nvTranslate, nvExtendUtils, nvTimezone,
    $timeout, nvDateTimeUtils) {
    const ctrl = this;
    const orderId = orderData.id;
    const transactions = orderData.transactions;

    ctrl.lceView = LCE_VIEW;

    ctrl.onCancel = onCancel;
    ctrl.onSave = onSave;
    ctrl.isDpEmpty = isDpEmpty;
    ctrl.onDpChanged = onDpChanged;
    init();

    function init() {
      ctrl.mainState = LCE_VIEW.LOADING;
      ctrl.formSubmitState = API_STATE.IDLE;
      const lastDdnt = Transaction.getLastDDNT(transactions);
      ctrl.initialDdDpState = !!(lastDdnt && lastDdnt.distributionPointId);
      ctrl.initialDdDpValue = _.get(lastDdnt, 'distributionPointId');

      ctrl.dpsDataService = nvAutocompleteData.getByHandle('order-edit-dps');
      ctrl.dpSearchText = '';
      ctrl.selectedDp = null;
      ctrl.dpDateOptionsCollection = [];
      ctrl.dateOptions = [];
      ctrl.selectedDropOffDate = null;

      DistributionPoint
        .getCapacity({ all: true })
        .then(onSuccess, () => (ctrl.formSubmitState = LCE_VIEW.ERROR));

      function onSuccess(dps) {
        ctrl.mainState = LCE_VIEW.IDLE;
        const extendedDps = DistributionPoint.extendDps(dps.dps);

        // options
        const options = DistributionPoint.buildOptions(extendedDps);
        ctrl.dpDateOptionsCollection = DistributionPoint.buildDateOptions(extendedDps);

        if (ctrl.initialDdDpState) {
          const selected = _.find(options, opt => opt.legacyValue === ctrl.initialDdDpValue);

          if (selected) {
            ctrl.selectedDp = selected;
            onDpChanged();
          }
        }
        ctrl.dpsDataService.setPossibleOptions(options);
      }
    }

    function onCancel() {
      $mdDialog.cancel();
    }

    function onSave() {
      ctrl.formSubmitState = API_STATE.WAITING;
      if (!isDpRemoved()) {
        const payload = {
          order_id: orderId,
          dp_id: ctrl.selectedDp.value,
          drop_off_date: nvDateTimeUtils.displayDate(
            ctrl.selectedDropOffDate,
            nvTimezone.getOperatorTimezone()
          ),
        };
        DistributionPoint
          .tagOrder(payload)
          .then(onSuccess)
          .finally(() => (ctrl.formSubmitState = API_STATE.IDLE));
      } else {
        DistributionPoint
          .unTagOrder(orderId)
          .then(onSuccess)
          .finally(() => (ctrl.formSubmitState = API_STATE.IDLE));
      }

      function onSuccess() {
        nvToast.success(nvTranslate.instant('container.order.edit.tagging-to-dp-success'));
        $mdDialog.hide(0);
      }
    }

    function isDpRemoved() {
      return ctrl.initialDdDpState && ctrl.selectedDp == null;
    }

    function isDpEmpty() {
      return !ctrl.initialDdDpState && ctrl.selectedDp == null;
    }

    function onDpChanged() {
      $timeout(() => {
        ctrl.dateOptions = ctrl.selectedDp
          ? ctrl.dpDateOptionsCollection[ctrl.selectedDp.value].options
          : EMPTY_ARRAY;
        ctrl.selectedDropOffDate = _.size(ctrl.dateOptions) > 0 ? ctrl.dateOptions[0].value : null;
      }, 0);
    }
  }
}());
