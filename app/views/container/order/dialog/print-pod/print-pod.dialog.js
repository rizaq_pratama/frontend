(function controller() {
  angular
    .module('nvOperatorApp.controllers')
    .controller('OrderPrintPodDialogController', OrderPrintPodDialogController);

  OrderPrintPodDialogController.$inject = [
    '$mdDialog', 'orderData', 'Order', 'PDF', 'nvDateTimeUtils',
  ];

  const API_STATE = {
    IDLE: 'idle',
    WAITING: 'waiting',
  };

  function OrderPrintPodDialogController($mdDialog, orderData, Order, PDF, nvDateTimeUtils) {
    const ctrl = this;

    ctrl.orders = _.cloneDeep(orderData);
    ctrl.downloadState = API_STATE.IDLE;

    ctrl.onCancel = onCancel;
    ctrl.onDownload = onDownload;

    function onCancel() {
      $mdDialog.cancel();
    }

    function onDownload() {
      const trackingIds = _.map(ctrl.orders, order => order.trackingId);
      ctrl.downloadState = API_STATE.WAITING;

      const fileName = `pod-${nvDateTimeUtils.displayDateTime(new Date())}`;
      PDF.downloadWithPost('core/orders/pods/generation',
        { tracking_ids: trackingIds },
        fileName)
      .then(() => $mdDialog.hide(0))
      .finally(() => (ctrl.downloadState = API_STATE.IDLE));
    }
  }
}());
