(function controller() {
  angular
    .module('nvOperatorApp.controllers')
    .controller('OrderEditPullFromRouteDialogController', OrderEditPullFromRouteDialogController);

  OrderEditPullFromRouteDialogController.$inject = [
    '$mdDialog', 'settingsData', 'transactionsData', 'Transaction', 'nvToast',
    'nvTranslate', 'nvDialog', 'Route', 'Order', '$scope',
  ];

  function OrderEditPullFromRouteDialogController(
    $mdDialog, settingsData, transactionsData, Transaction, nvToast,
    nvTranslate, nvDialog, Route, Order, $scope
  ) {
    // variables
    const orderToTransactionsMap = {};
    let bulkActionProgressPayload;
    $scope.Transaction = Transaction;
    const ctrl = this;
    ctrl.formSubmitState = 'idle';
    ctrl.processedTransactionsData = [];
    ctrl.settings = _.defaults(settingsData, {
      isMultiSelect: false,
      showTrackingId: false,
      pullTxn: null,
    });
    ctrl.submitButtonName = 'container.order.edit.pull-from-route';

    // initialize
    init();

    // functions
    ctrl.isAbleToSubmit = isAbleToSubmit;
    ctrl.printTransactionType = printTransactionType;
    ctrl.onCancel = onCancel;
    ctrl.onSave = onSave;

    // functions details
    function init() {
      // set orderToTransactionsMap data
      _.forEach(transactionsData, (txnData) => {
        if (_.isUndefined(orderToTransactionsMap[txnData.orderId])) {
          orderToTransactionsMap[txnData.orderId] = [];
        }
        if (ctrl.settings.pullTxn) {
          if (txnData.type === ctrl.settings.pullTxn) {
            orderToTransactionsMap[txnData.orderId].push(txnData);
          }
        } else {
          orderToTransactionsMap[txnData.orderId].push(txnData);
        }
      });

      // set button name
      if (_.size(orderToTransactionsMap) > 1) {
        ctrl.submitButtonName = 'container.order.edit.pull-orders-from-routes';
      }

      // set ctrl.processedTransactionsData
      // ctrl.processedTransactionsData is txns group by route and order
      _.forEach(orderToTransactionsMap, (txns) => {
        const routeToTransactionsMap = {};
        _.forEach(txns, (txn) => {
          if (_.isUndefined(routeToTransactionsMap[txn.routeId])) {
            routeToTransactionsMap[txn.routeId] = [];
          }

          routeToTransactionsMap[txn.routeId].push(txn);
        });

        _.forEach(routeToTransactionsMap, (theTxns, routeId) => {
          ctrl.processedTransactionsData.push(
            generateProcessedTransactionData(theTxns, routeId)
          );
        });
      });

      function generateProcessedTransactionData(transactions, routeId) {
        const firstTxn = transactions[0];

        const processedTransactionData = {
          isSelected: true, // default to true
          routeId: +routeId,
          trackingId: firstTxn.trackingId,
          orderId: firstTxn.orderId,
          type: firstTxn.type,
          transactions: compactTransactions(transactions),
        };

        if (firstTxn.status === 'SUCCESS') {
          processedTransactionData.isSelected = false;
          processedTransactionData.disabled = true;
        }

        return processedTransactionData;

        function compactTransactions(txns) {
          return _.map(txns, txn => ({
            id: txn.id,
            type: txn.type,
          }));
        }
      }
    }

    function printTransactionType(transactions) {
      const types = [];

      _.forEach(transactions, (txn) => {
        if (Transaction.isPickup(txn)) {
          types.push(nvTranslate.instant('commons.model.pickup'));
        } else if (Transaction.isDelivery(txn)) {
          types.push(nvTranslate.instant('commons.delivery'));
        }
      });

      return _.intersection(types).join(', ');
    }

    function isAbleToSubmit() {
      return _.size(getSelectedTransactions()) > 0;
    }

    function getSelectedTransactions() {
      return _.filter(ctrl.processedTransactionsData, processedTransactionData =>
        processedTransactionData.isSelected
      );
    }

    function onSave($event) {
      if (ctrl.settings.isMultiSelect) {
        multiSelectSaving();
      } else {
        nonMultiSelectSaving();
      }

      function multiSelectSaving() {
        const processedTxnsToProcess = _.cloneDeep(getSelectedTransactions());
        bulkActionProgressPayload = {
          totalCount: getSelectedTransactions().length,
          currentIndex: 0,
          errors: [],
        };

        nvDialog.showSingle($event, {
          templateUrl: 'lib/ninja-commons/services/dialogs/bulk-action-progress/bulk-action-progress.dialog.html',
          cssClass: 'bulk-action-progress',
          controller: 'BulkActionProgressDialogController',
          controllerAs: 'ctrl',
          skipHide: true,
          clickOutsideToClose: false,
          locals: {
            payload: bulkActionProgressPayload,
          },
        }).then(progressDialogClosed);

        startMultiSelectSaving(processedTxnsToProcess);

        function progressDialogClosed() {
          if (getSuccessCount() > 0) {
            nvToast.info(
              nvTranslate.instant('container.order.edit.num-order-updated', { num: getSuccessCount() }),
              nvTranslate.instant('container.order.edit.pull-from-route')
            );
          }

          $mdDialog.hide();
        }

        function getSuccessCount() {
          return bulkActionProgressPayload.totalCount - bulkActionProgressPayload.errors.length;
        }
      }

      function nonMultiSelectSaving() {
        const rowToProcess = ctrl.processedTransactionsData[0];

        ctrl.formSubmitState = 'waiting';
        const payload = generatePullFromRouteData(rowToProcess);
        Order.removeFromRouteV2(rowToProcess.orderId, payload, true)
          .then(nonMultiSelectSuccess)
          .finally(() => {
            ctrl.formSubmitState = 'idle';
          });

        function nonMultiSelectSuccess() {
          nvToast.info(
            nvTranslate.instant('container.order.edit.pull-from-route-success', {
              trackingId: rowToProcess.trackingId,
              routeId: rowToProcess.routeId,
            })
          );
          $mdDialog.hide();
        }
      }
    }

    function onCancel() {
      $mdDialog.cancel();
    }

    function startMultiSelectSaving(processedTransactions) {
      let rowToProcess;
      if (processedTransactions.length > 0) {
        bulkActionProgressPayload.currentIndex += 1;
        rowToProcess = processedTransactions.splice(0, 1)[0];

        ctrl.formSubmitState = 'waiting';
        const payload = generatePullFromRouteData(rowToProcess);
        Order.removeFromRouteV2(rowToProcess.orderId, payload, true)
          .then(multiSelectSuccess, multiSelectFailure);
      } else {
        ctrl.formSubmitState = 'idle';
      }

      function multiSelectSuccess(response) {
        if (response && response.status === 'SUCCESS') {
          bulkActionProgressPayload.successCount += 1;
          if (processedTransactions.length > 0) {
            startMultiSelectSaving(processedTransactions); // recursive
          } else {
            ctrl.formSubmitState = 'idle';
          }
        } else {
          multiSelectFailure({ message: _.get(response, 'message', 'unknown error') });
        }
      }

      function multiSelectFailure(error) {
        let errorMessage = _.get(error, 'message') || _.get(error, 'messages') || 'unknown error';
        if (_.isArray(errorMessage)) {
          errorMessage = _.join(errorMessage, ', ');
        }

        bulkActionProgressPayload.errors.push(`${rowToProcess.trackingId} | ${errorMessage}`);

        if (processedTransactions.length > 0) {
          startMultiSelectSaving(processedTransactions); // recursive
        } else {
          ctrl.formSubmitState = 'idle';
        }
      }
    }

    function generatePullFromRouteData(processedTransactionData) {
      const firstTxn = _.head(processedTransactionData.transactions);
      return {
        type: firstTxn.type,
      };
    }
  }
}());
