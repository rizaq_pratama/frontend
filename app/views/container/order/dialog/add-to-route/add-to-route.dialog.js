(function controller() {
  angular
    .module('nvOperatorApp.controllers')
    .controller('OrderAddToRouteDialogController', OrderAddToRouteDialogController);

  OrderAddToRouteDialogController.$inject = [
    '$mdDialog', 'ordersData', 'settingsData', 'Tag', 'Transaction',
    'Route', 'nvToast', 'nvTranslate', 'nvDialog', '$timeout', 'Order',
  ];

  function OrderAddToRouteDialogController(
    $mdDialog, ordersData, settingsData, Tag, Transaction,
    Route, nvToast, nvTranslate, nvDialog, $timeout, Order
  ) {
    // variables
    const DEFAULT_WAYPOINT_INDEX = -1;

    const ctrl = this;
    ctrl.formSubmitState = 'idle';
    ctrl.isFormSubmitDisabled = isFormSubmitDisabled;
    ctrl.formData = {
      isSetToAll: false,
      routeFinder: {
        isViewActive: false, // for multiselect version support only
        submitState: 'idle',
        isSubmitDisabled: isRouteFinderSubmitDisabled,
        suggestRoute: suggestRoute,
      },
      orders: [],
    };
    ctrl.settings = _.defaults(settingsData, {
      isMultiSelect: false,
      txLeg: Transaction.TYPE.DELIVERY,
    });
    ctrl.submitButtonName = 'container.order.edit.add-to-route';
    ctrl.tagSelectionOptions = [];
    ctrl.transactionTypeSelectionOptions = Transaction.getTransactionTypeOptions();
    let tagsMap;
    // initialize
    init();

    // functions
    ctrl.getAll = getAll;
    ctrl.onSave = onSave;
    ctrl.onCancel = onCancel;
    ctrl.resetWaypointIndex = resetWaypointIndex;
    ctrl.getTxnTypeName = getTxnTypeName;
    ctrl.getTagsName = getTagsName;
    ctrl.transactionTypeChanged = transactionTypeChanged;
    ctrl.routeIdChanged = routeIdChanged;
    ctrl.routeTagsChanged = routeTagsChanged;

    // functions details
    function init() {
      if (ordersData.length > 1) {
        ctrl.submitButtonName = 'container.order.edit.add-selected-to-routes';
      }

      ctrl.formData.orders = _.map(ordersData, o => (
        {
          trackingId: o.trackingId.toUpperCase(),
          orderId: o.id,
          routeId: null,
          waypointIndex: DEFAULT_WAYPOINT_INDEX,
          transactionType: ctrl.settings.txLeg,
          tags: [],
        }
      ));
    }

    function getAll() {
      return Tag.all().then(success);

      function success(response) {
        ctrl.tagSelectionOptions = Tag.toOptions(response);
        tagsMap = _.keyBy(ctrl.tagSelectionOptions, 'value');
        routeFinderSetDefaultTag(response.tags);
      }

      function routeFinderSetDefaultTag(tags) {
        if (ctrl.settings.isMultiSelect) {
          const fleetTagId = Tag.getTagId('FLT', tags);

          if (fleetTagId) {
            _.forEach(ctrl.formData.orders, (order) => {
              order.tags.push(fleetTagId);
            });
          }
        }
      }
    }

    function onSave($event) {
      if (ctrl.settings.isMultiSelect) {
        multiSelectSaving();
      } else {
        nonMultiSelectSaving();
      }

      function multiSelectSaving() {
        const ordersToProcess = generateBulkRequestPayload(ctrl.formData.orders);
        ctrl.bulkActionProgressPayload = {
          totalCount: ctrl.formData.orders.length,
          currentIndex: 0,
          errors: [],
        };

        nvDialog.showSingle($event, {
          templateUrl: 'lib/ninja-commons/services/dialogs/bulk-action-progress/bulk-action-progress.dialog.html',
          cssClass: 'bulk-action-progress',
          controller: 'BulkActionProgressDialogController',
          controllerAs: 'ctrl',
          skipHide: true,
          clickOutsideToClose: false,
          locals: {
            payload: ctrl.bulkActionProgressPayload,
          },
        }).then(progressDialogClosed);

        startMultiSelectBulkSaving(ordersToProcess);

        function progressDialogClosed() {
          const successCount = getSuccessCount();

          if (successCount > 0) {
            nvToast.info(
              nvTranslate.instant('container.order.edit.num-order-updated', { num: successCount }),
              nvTranslate.instant('container.order.edit.add-to-route')
            );
          }

          if (successCount < ctrl.bulkActionProgressPayload.totalCount) {
            const errors = ctrl.bulkActionProgressPayload.errors;
            const errStr = _(errors)
              .map(err => err.key && (err.key + (err.message ? ` (${err.message})` : '')))
              .compact()
              .join('\n');
            nvToast.warning(
              `${nvTranslate.instant('container.order.edit.num-order-update-failed', { num: errors.length })}\n${errStr}`,
              nvTranslate.instant('container.order.edit.add-to-route')
            );
          }

          $mdDialog.hide();
        }

        function getSuccessCount() {
          return ctrl.bulkActionProgressPayload.totalCount - ctrl.bulkActionProgressPayload.errors.length;
        }
      }

      function nonMultiSelectSaving() {
        const data = generateSingleRequestPayload(ctrl.formData.orders[0]);

        ctrl.formSubmitState = 'waiting';
        Order.addToRouteV2(+ctrl.formData.orders[0].orderId, data)
          .then(nonMultiSelectSuccess)
          .finally(() => (ctrl.formSubmitState = 'idle'));

        function nonMultiSelectSuccess(resp) {
          if (resp && resp.status === 'SUCCESS') {
            nvToast.info(
              nvTranslate.instant('container.order.edit.add-to-route-success', {
                trackingId: ctrl.formData.orders[0].trackingId,
                routeId: ctrl.formData.orders[0].routeId,
              })
            );
            $mdDialog.hide();
          } else {
            nvToast.warning('ERROR');
          }
        }
      }

      function startMultiSelectBulkSaving(requests) {
        let request;
        if (requests.length > 0) {
          setBulkActionUpdating(true);
          ctrl.bulkActionProgressPayload.currentIndex += 1;
          request = _.head(requests.splice(0, 1));
          Order
            .addToRouteV2(request.orderId, request.payload, true)
            .then(multiSelectSuccess, multiSelectError);
        } else {
          setBulkActionUpdating(false);
        }

        function multiSelectSuccess(data) {
          if (data && data.status === 'SUCCESS') {
            ctrl.bulkActionProgressPayload.successCount += 1;
          } else {
            // mark as error
            multiSelectError({ message: _.get(data, 'message', 'unknown error') });
          }
          if (requests.length > 0) {
            startMultiSelectBulkSaving(requests);
          } else {
            setBulkActionUpdating(false);
          }
        }

        function multiSelectError(error) {
          let errorMessage = _.get(error, 'message') || _.get(error, 'messages') || 'unknown error';
          if (_.isArray(errorMessage)) {
            errorMessage = _.join(errorMessage, ', ');
          }

          ctrl.bulkActionProgressPayload.errors.push(`${request.trackingId} | ${errorMessage}`);
          if (requests.length > 0) {
            startMultiSelectBulkSaving(requests);
          } else {
            setBulkActionUpdating(false);
          }
        }
      }
    }

    function onCancel() {
      ctrl.formData.orders = []; // cleanup any reference
      $mdDialog.cancel();
    }

    function setBulkActionUpdating(b) {
      if (b) {
        ctrl.formSubmitState = 'waiting';
      } else {
        ctrl.formSubmitState = 'idle';
      }
    }

    function generateBulkRequestPayload(orders) {
      return _.map(orders, order => ({
        orderId: order.orderId,
        trackingId: order.trackingId,
        payload: generateSingleRequestPayload(order),
      }));
    }

    function generateSingleRequestPayload(order) {
      const data = {
        route_id: +order.routeId,
        type: order.transactionType === 'DD' ? 'DELIVERY' : 'PICKUP',
      };

      if (order.waypointIndex !== DEFAULT_WAYPOINT_INDEX) {
        data.index = order.waypointIndex;
      }

      return data;
    }

    function resetWaypointIndex(order) {
      order.waypointIndex = DEFAULT_WAYPOINT_INDEX;
    }

    function getTxnTypeName(value) {
      const obj = _.find(ctrl.transactionTypeSelectionOptions, { value: value });
      return obj.displayName;
    }

    function getTagsName(ids) {
      const names = _.map(ids, id => (tagsMap[id].displayName));
      return names.join(', ');
    }

    function transactionTypeChanged(order) {
      $timeout(() => {
        if (ctrl.formData.isSetToAll) {
          _.forEach(ctrl.formData.orders, (theOrder) => {
            theOrder.transactionType = order.transactionType;

            resetWaypointIndex(theOrder);
          });
        } else {
          resetWaypointIndex(order);
        }
      });
    }

    function routeIdChanged(order) {
      $timeout(() => {
        if (ctrl.formData.isSetToAll) {
          _.forEach(ctrl.formData.orders, (theOrder) => {
            theOrder.routeId = order.routeId;

            resetWaypointIndex(theOrder);
          });
        } else {
          resetWaypointIndex(order);
        }
      });
    }

    function routeTagsChanged(order) {
      $timeout(() => {
        if (ctrl.formData.isSetToAll) {
          _.forEach(ctrl.formData.orders, (theOrder) => {
            theOrder.tags = order.tags;
          });
        }
      });
    }

    function isFormSubmitDisabled() {
      return ctrl.formData.routeFinder.isViewActive || _.size(
        // if route id no fill in for any of the orders, then cannot submit
        _.filter(ctrl.formData.orders, order =>
          !order.routeId || isNaN(order.routeId) || order.routeId <= 0
        )
      );
    }

    function isRouteFinderSubmitDisabled() {
      return _.size(
        // if no tag selected for any of the orders, then cannot submit
        _.filter(ctrl.formData.orders, order =>
          order.tags.length <= 0
        )
      );
    }

    function suggestRoute() {
      ctrl.formData.routeFinder.submitState = 'waiting';
      Route.suggestRoute(generateSuggestRoutePayload()).then(success, failure);

      function success(response) {
        ctrl.formData.routeFinder.submitState = 'idle';
        ctrl.formData.routeFinder.isViewActive = false;

        const suggestRouteOrders = response.success || [];
        const errorSuggestRouteOrders = response.error || [];
        _.forEach(suggestRouteOrders, (suggestRouteOrder) => {
          const order = _.find(ctrl.formData.orders, ['trackingId', suggestRouteOrder.trackingId]);

          order.routeId = suggestRouteOrder.optimalInsertionRoute.id;
          order.transactionType = Transaction.TYPE[suggestRouteOrder.transactionTypeToRoute];
          order.waypointIndex = suggestRouteOrder.optimalInsertionIndex;
        });

        const reasons = [];
        _.forEach(errorSuggestRouteOrders, (error) => {
          const order = _.find(ctrl.formData.orders, ['trackingId', error.trackingId]);
          order.routeId = null;

          reasons.push(error.reason);
        });

        if (_.size(suggestRouteOrders) <= 0) {
          nvToast.error(
            nvTranslate.instant('container.order.edit.suggest-route-failed'),
            _.intersection(reasons)
          );
        }
      }

      function failure() {
        ctrl.formData.routeFinder.submitState = 'idle';
        ctrl.formData.routeFinder.isViewActive = false;
      }

      function generateSuggestRoutePayload() {
        const payload = [];

        _.forEach(ctrl.formData.orders, (order) => {
          payload.push({
            tracking_id: order.trackingId,
            tag_ids: order.tags,
            type: order.transactionType,
          });
        });

        return payload;
      }
    }
  }
}());
