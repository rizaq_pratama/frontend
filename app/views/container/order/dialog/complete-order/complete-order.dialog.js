(function controller() {
  angular
    .module('nvOperatorApp.controllers')
    .controller('OrderEditCompleteOrderDialogController', OrderEditCompleteOrderDialogController);

  OrderEditCompleteOrderDialogController.$inject = [
    '$mdDialog', 'ordersData', 'Order', 'nvToast', 'nvTranslate',
    'settingsData', 'nvDialog',
  ];

  function OrderEditCompleteOrderDialogController(
    $mdDialog, ordersData, Order, nvToast, nvTranslate,
    settingsData, nvDialog
  ) {
    // variables
    let bulkActionProgressPayload;

    const ctrl = this;
    ctrl.formComments = null;
    ctrl.formSubmitState = 'idle';

    ctrl.orders = _.cloneDeep(ordersData);
    _.forEach(ctrl.orders, o => _.assign(o, { isCodCollected: false }));
    ctrl.ordersWithCod = _.filter(ctrl.orders, o => !!o.cod || !!o.codId);

    ctrl.settings = _.defaults(settingsData, {
      isMultiSelect: false,
    });

    // functions
    ctrl.onCancel = onCancel;
    ctrl.onSave = onSave;
    ctrl.markAll = markAll;
    ctrl.unmarkAll = unmarkAll;
    ctrl.codCollectedOnChange = codCollectedOnChange;

    // functions details
    function onSave($event) {
      if (ctrl.settings.isMultiSelect) {
        multiSelectSaving();
      } else {
        nonMultiSelectSaving();
      }

      function multiSelectSaving() {
        const ordersToProcess = _.cloneDeep(ctrl.orders);
        bulkActionProgressPayload = {
          totalCount: ctrl.orders.length,
          currentIndex: 0,
          errors: [],
        };

        nvDialog.showSingle($event, {
          templateUrl: 'lib/ninja-commons/services/dialogs/bulk-action-progress/bulk-action-progress.dialog.html',
          cssClass: 'bulk-action-progress',
          controller: 'BulkActionProgressDialogController',
          controllerAs: 'ctrl',
          skipHide: true,
          clickOutsideToClose: false,
          locals: {
            payload: bulkActionProgressPayload,
          },
        }).then(progressDialogClosed);

        startMultiSelectSaving(ordersToProcess);

        function progressDialogClosed() {
          if (getSuccessCount() > 0) {
            nvToast.info(
              nvTranslate.instant('container.order.edit.num-order-updated', { num: getSuccessCount() }),
              nvTranslate.instant('container.order.edit.complete-order')
            );
          }

          $mdDialog.hide();
        }

        function getSuccessCount() {
          return bulkActionProgressPayload.totalCount - bulkActionProgressPayload.errors.length;
        }
      }

      function nonMultiSelectSaving() {
        // non multi select suppose only has one order in ordersData payload
        const theOrder = ctrl.orders[0];

        Order.forceSuccess(theOrder.id, { codcollected: theOrder.isCodCollected })
          .then(success, () => {
            ctrl.formSubmitState = 'idle';
            nvToast.error(nvTranslate.instant('container.order.edit.complete-order-failed'));
          });

        function success() {
          ctrl.formSubmitState = 'idle';
          nvToast.info(nvTranslate.instant('container.order.edit.order-completed'));
          $mdDialog.hide();
        }
      }
    }

    function onCancel() {
      $mdDialog.cancel();
    }

    function startMultiSelectSaving(orders) {
      let orderToProcess;
      if (orders.length > 0) {
        bulkActionProgressPayload.currentIndex += 1;
        orderToProcess = orders.splice(0, 1)[0];

        ctrl.formSubmitState = 'waiting';
        Order.forceSuccess(orderToProcess.id, { codcollected: orderToProcess.isCodCollected })
          .then(multiSelectSuccess, multiSelectFailure);
      } else {
        ctrl.formSubmitState = 'idle';
      }

      function multiSelectSuccess() {
        bulkActionProgressPayload.successCount += 1;

        if (orders.length > 0) {
          startMultiSelectSaving(orders); // recursive
        } else {
          ctrl.formSubmitState = 'idle';
        }
      }

      function multiSelectFailure(error) {
        let errorMessage = _.get(error, 'message') || _.get(error, 'messages') || 'unknown error';
        if (_.isArray(errorMessage)) {
          errorMessage = _.join(errorMessage, ', ');
        }

        bulkActionProgressPayload.errors.push(`${orderToProcess.trackingId} | ${errorMessage}`);

        if (orders.length > 0) {
          startMultiSelectSaving(orders); // recursive
        } else {
          ctrl.formSubmitState = 'idle';
        }
      }
    }

    function markAll() {
      _.forEach(ctrl.ordersWithCod, (o) => {
        if (!o.rts) {
          o.isCodCollected = true;
        }
      });
    }

    function unmarkAll() {
      _.forEach(ctrl.ordersWithCod, (o) => {
        if (!o.rts) {
          o.isCodCollected = false;
        }
      });
    }

    function codCollectedOnChange(order) {
      if (order.rts) {
        // not allowed to check rts order
        order.isCodCollected = false;
      }
    }
  }
}());
