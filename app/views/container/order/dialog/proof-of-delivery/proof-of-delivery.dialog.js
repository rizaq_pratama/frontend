(function controller() {
  angular
    .module('nvOperatorApp.controllers')
    .controller('OrderProofOfDeliveryDialogController', OrderProofOfDeliveryDialogController);

  OrderProofOfDeliveryDialogController.$inject = [
    '$mdDialog', 'orderData', 'Order', 'nvTable', 'Waypoint', 'Transaction',
    'nvDateTimeUtils', '$window', '$state', 'nvMaps', 'nvTranslate',
    '$timeout', '$scope', '$q', 'nvTimezone', 'nvFileUtils',
  ];

  function OrderProofOfDeliveryDialogController(
    $mdDialog, orderData, Order, nvTable, Waypoint, Transaction,
    nvDateTimeUtils, $window, $state, nvMaps, nvTranslate,
    $timeout, $scope, $q, nvTimezone, nvFileUtils
  ) {
    // variables
    let tableFullHeight;
    let podDetailsFullHeight;
    let tableHeight;
    let podDetailsHeight;

    const ctrl = this;
    const ORDER = _.cloneDeep(orderData);

    ctrl.podsTableParam = null;
    ctrl.selectedPod = null;
    ctrl.csv = {
      header: '[]',
      columnOrder: '[]',
      filename: 'file.csv',
      data: [],
    };
    ctrl.trackingId = ORDER.trackingId;

    // functions
    ctrl.onCancel = onCancel;
    ctrl.navigateToRouteLogsPage = navigateToRouteLogsPage;

    ctrl.windowSizeChanged = windowSizeChanged;
    ctrl.tableContainerDivStyle = tableContainerDivStyle;
    ctrl.selectedPodDivStyle = selectedPodDivStyle;

    ctrl.getAll = getAll;
    ctrl.viewPOD = viewPOD;
    ctrl.isDownloadAvailable = isDownloadAvailable;
    ctrl.downloadSelected = downloadSelected;
    ctrl.downloadSignature = downloadSignature;

    // functions details
    function getAll() {
      const wpIds = _(orderData.transactions).map(txn => txn.waypointId).compact().value();

      return $q.all(_.concat(Order.getPODs(orderData.id), constructPhotosRequest(wpIds)))
        .then(success);

      function success(responses) {
        const podResponse = responses[0];
        // photos response is at index 1 to end
        const photosResponses = _.slice(responses, 1);
        const pods = extendPods(podResponse);

        ctrl.podsTableParam = nvTable.createTable();
        ctrl.podsTableParam.addColumn('id', { displayName: 'commons.pod-id' });
        ctrl.podsTableParam.addColumn('_rsvnOrTxn', { displayName: 'container.order.edit.rsvn-or-txn' });
        ctrl.podsTableParam.addColumn('_type', { displayName: 'commons.type' });
        ctrl.podsTableParam.addColumn('_status', { displayName: 'commons.status' });
        ctrl.podsTableParam.addColumn('_distance', { displayName: 'commons.distance' });
        ctrl.podsTableParam.addColumn('_podTime', { displayName: 'container.order.edit.pod-time' });
        ctrl.podsTableParam.addColumn('routeDriver', { displayName: 'commons.driver' });
        ctrl.podsTableParam.addColumn('_recipient', { displayName: 'commons.recipient' });
        ctrl.podsTableParam.addColumn('_address', { displayName: 'commons.address' });
        ctrl.podsTableParam.addColumn('_verificationMethod', { displayName: 'container.order.edit.pod-verification-method' });
        ctrl.podsTableParam.setData(pods);

        ctrl.podsTableParam.sort('_podTime');
        processRowHeight();

        ctrl.photos = {};
        _.forEach(photosResponses, (photosResponse, idx) => {
          ctrl.photos[wpIds[idx]] = extendPhotos(photosResponse);
        });
      }

      function constructPhotosRequest(ids) {
        return _.map(ids, id => Waypoint.getPhotos(id));
      }
    }

    function viewPOD(pod) {
      ctrl.selectedPod = pod;
      ctrl.selectedPhotos = ctrl.photos[pod.waypointId] || [];
      processRowHeight();

      const csvData = _.cloneDeep(ctrl.selectedPod);
      const header = [
        nvTranslate.instant('commons.status'),
        nvTranslate.instant('container.order.edit.pod-time'),
        nvTranslate.instant('container.order.edit.start-datetime'),
        nvTranslate.instant('container.order.edit.end-datetime'),
        nvTranslate.instant('commons.waypoint-id'),
        nvTranslate.instant('commons.model.route-id'),
        nvTranslate.instant('commons.driver'),
        nvTranslate.instant('container.order.edit.distance-from-point'),
        nvTranslate.instant('commons.model.priority-level'),
        nvTranslate.instant('commons.location'),
        nvTranslate.instant('commons.comments'),
        nvTranslate.instant(csvData._isTransaction ? 'commons.recipient-name' : 'commons.sender-name'),
        nvTranslate.instant('commons.contact'),
        nvTranslate.instant('container.order.edit.pod-verification-method'),
      ];
      const columnOrder = [
        '_status',
        '_podTime',
        '_startTime',
        '_endTime',
        'waypointId',
        'routeId',
        'routeDriver',
        '_distance',
        '_priorityLevel',
        '_address',
        '_comments',
        'pod.name',
        'pod.contact',
        '_verificationMethod',
      ];

      if (csvData.pod && csvData.pod.ic_number) {
        header.push(nvTranslate.instant('container.order.edit.ic-number'));
        columnOrder.push('pod.ic_number');
      }

      if (csvData._isTransaction) {
        header.push(nvTranslate.instant('container.order.edit.substitute-relationship'));
        columnOrder.push('pod.relationship');

        header.push(nvTranslate.instant('container.order.edit.verification-code'));
        columnOrder.push('pod.verification_code');

        header.push(nvTranslate.instant('container.order.edit.placed-at-location'));
        columnOrder.push('pod.hidden_location');
      }

      if (csvData._isReservation) {
        header.push(nvTranslate.instant('container.order.edit.total-parcels-picked-up'));
        columnOrder.push('pod.received_parcels');

        if (csvData.pod && _.size(csvData.pod.scanned_parcels) > 0) {
          csvData._scanned_parcels = csvData.pod.scanned_parcels.join(', ');
          header.push(nvTranslate.instant('commons.parcels'));
          columnOrder.push('_scanned_parcels');
        }
      }

      ctrl.csv = {
        header: JSON.stringify(header),
        columnOrder: JSON.stringify(columnOrder),
        filename: `${csvData.id} - ${csvData._rsvnOrTxn}.csv`,
        data: [csvData],
      };
    }

    function onCancel() {
      $mdDialog.cancel();
    }

    function navigateToRouteLogsPage() {
      $window.open(
        $state.href('container.route-logs')
      );
    }

    function windowSizeChanged() {
      processRowHeight();
    }

    function extendPods(pods) {
      return _.map(pods, pod => (
        setCustomPodData(pod)
      ));

      function setCustomPodData(pod) {
        pod._podTime = pod.createdAt && moment(pod.createdAt).isValid() ?
          nvDateTimeUtils.displayDateTime(moment.utc(pod.createdAt)) : '';

        pod._rsvnOrTxn = pod.waypointType;
        pod._isReservation = false;
        pod._isTransaction = false;
        if (pod.waypointType === Waypoint.TYPE.RESERVATION) {
          pod._isReservation = true;
          pod._rsvnOrTxn += ` (${pod.reservationId})`;
          pod._type = 'N.A.';
          pod._status = pod.reservationStatus;
          pod._isFailed = pod._status && pod._status.toLowerCase() === 'fail';
          pod._isSuccess = pod._status && pod._status.toLowerCase() === 'success';
          pod._recipient = 'N.A.';

          pod._comments = pod.reservationComments;

          const txn = _.find(orderData.transactions, ['waypointId', pod.waypointId]);
          const reservation = txn ? txn._reservation : null;
          pod._startTime = '';
          pod._endTime = '';
          pod._priorityLevel = 0;
          pod._distance = '-';
          if (reservation) {
            pod._startTime = reservation._readyDatetime;
            pod._endTime = reservation._latestDatetime;
            pod._priorityLevel = txn.priorityLevel;
            pod._distance = getDistance(txn, pod);
          }

          pod._isLate = false;
          pod._isOnTime = false;
          if (pod._podTime && pod._endTime && pod._startTime) {
            pod._isLate = moment.utc(pod._podTime).diff(moment.utc(pod._endTime)) > 0;
            pod._isOnTime = moment.utc(pod._startTime).diff(moment.utc(pod._podTime)) <= 0 &&
              moment.utc(pod._podTime).diff(moment.utc(pod._endTime)) <= 0;
          }
        } else { // Waypoint.TYPE.TRANSACTION
          pod._isTransaction = true;
          pod._rsvnOrTxn += ` (${pod.transactionId})`;
          pod._type = pod.transactionType;
          pod._status = pod.transactionStatus;
          pod._isFailed = pod._status && pod._status.toLowerCase() === 'fail';
          pod._isSuccess = pod._status && pod._status.toLowerCase() === 'success';
          pod._recipient = pod.transactionName;

          pod._comments = pod.transactionComments;

          const txn = _.find(orderData.transactions, ['id', pod.transactionId]);
          pod._startTime = '';
          pod._endTime = '';
          pod._priorityLevel = 0;
          pod._distance = '-';
          if (txn) {
            pod._startTime = txn._startTime;
            pod._endTime = txn._endTime;
            pod._priorityLevel = txn.priorityLevel;
            pod._distance = getDistance(txn, pod);
          }

          pod._isLate = false;
          pod._isOnTime = false;
          if (pod._podTime && pod._endTime && pod._startTime) {
            pod._isLate = moment.utc(pod._podTime).diff(moment.utc(pod._endTime)) > 0;
            pod._isOnTime = moment.utc(pod._startTime).diff(moment.utc(pod._podTime)) <= 0 &&
                            moment.utc(pod._podTime).diff(moment.utc(pod._endTime)) <= 0;
          }
        }

        pod._address = [pod.address1, pod.address2, pod.postcode].join(' ');
        pod._verificationMethod = _.get(pod, 'pod.verification_method') || nvTranslate.instant('container.order.edit.pod-verification-not-applicable');
        return pod;

        function getDistance(txn, thePod) {
          if (txn.latitude && txn.longitude && thePod.pod && thePod.pod.sign_coordinates) {
            const signCoordinates = _.split(thePod.pod.sign_coordinates, ',');

            if (_.size(signCoordinates) === 2 && +signCoordinates[0] !== -1) {
              const km = nvMaps.getDistanceFromLatLonInKm(
                +txn.latitude, +txn.longitude,
                +signCoordinates[1], +signCoordinates[0]
              );

              return `${Math.round(km * 1000)} m`;
            }
          }

          return '-';
        }
      }
    }

    function tableContainerDivStyle() {
      if (!tableHeight) {
        return { height: 'auto' };
      }

      return { height: `${tableHeight}px` };
    }

    function selectedPodDivStyle() {
      if (!podDetailsHeight) {
        return { height: 'auto' };
      }

      return { height: `${podDetailsHeight}px` };
    }

    function processRowHeight() {
      $timeout(() => {
        const toolbarHeight = $('.order-proof-of-delivery > md-toolbar').height();
        const maxDialogContentHeight = (($scope.$windowHeight * 90) / 100) - toolbarHeight;
        tableFullHeight = $('.order-proof-of-delivery .table-container .table-with-locks-container').outerHeight(true);
        podDetailsFullHeight = $('.order-proof-of-delivery .selected-pod-holder').outerHeight(true);

        if (maxDialogContentHeight < (tableFullHeight + podDetailsFullHeight)) {
          const tableRatioHeight = (maxDialogContentHeight * 40) / 100;

          if (tableFullHeight < tableRatioHeight) {
            tableHeight = tableFullHeight;
          } else {
            tableHeight = tableRatioHeight;
          }
          podDetailsHeight = maxDialogContentHeight - tableHeight;
        } else {
          tableHeight = 0;
          podDetailsHeight = 0;
        }
      });
    }

    function extendPhotos(photos) {
      let idx = 0;
      return _.map(photos, (p) => {
        const id = idx += 1;
        const takenAt = nvDateTimeUtils.displayDateTime(
          nvDateTimeUtils.toMoment(p.taken_at, nvTimezone.getOperatorTimezone()),
          nvTimezone.getOperatorTimezone()
        );
        const filename = p.image_url.split('/').pop();
        return _.assign({}, {
          id: id - 1,
          isActive: false,
          url: p.image_url,
          title: `${filename} - ${takenAt}` });
      });
    }

    function downloadSelected() {
      _(ctrl.selectedPhotos)
        .filter(p => p.isActive)
        .forEach((p) => {
          const filename = p.url.split('/').pop();
          nvFileUtils.download(p.url, filename);
        });
    }

    function downloadSignature() {
      const url = ctrl.selectedPod.pod.url;
      if (url) {
        const filename = url.split('/').pop();
        nvFileUtils.download(url, filename);
      }
    }

    function isDownloadAvailable() {
      const selected = _.filter(ctrl.selectedPhotos, p => p.isActive);
      if (selected) {
        return selected.length > 0;
      }
      return false;
    }
  }
}());
