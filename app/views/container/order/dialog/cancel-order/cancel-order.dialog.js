(function controller() {
  angular
    .module('nvOperatorApp.controllers')
    .controller('OrderEditCancelOrderDialogController', OrderEditCancelOrderDialogController);

  OrderEditCancelOrderDialogController.$inject = [
    '$mdDialog', 'ordersData', 'Order', 'nvToast', 'nvTranslate',
    'settingsData', 'nvDialog',
  ];

  function OrderEditCancelOrderDialogController(
    $mdDialog, ordersData, Order, nvToast, nvTranslate,
    settingsData, nvDialog
  ) {
    // variables
    let bulkActionProgressPayload;
    let cancelledOrders = [];
    let unCancelledOrders = [];

    const ctrl = this;
    ctrl.formComments = null;
    ctrl.formSubmitState = 'idle';
    ctrl.orders = ordersData;
    ctrl.settings = _.defaults(settingsData, {
      isMultiSelect: false,
    });
    ctrl.submitButtonName = 'container.order.edit.cancel-order';

    // initialize
    init();

    // functions
    ctrl.onCancel = onCancel;
    ctrl.onSave = onSave;

    // functions details
    function init() {
      if (ordersData.length > 1) {
        ctrl.submitButtonName = 'container.order.edit.cancel-orders';
      }
    }

    function onSave($event) {
      if (ctrl.settings.isMultiSelect) {
        multiSelectSaving();
      } else {
        nonMultiSelectSaving();
      }

      function multiSelectSaving() {
        const ordersToProcess = _.cloneDeep(ordersData);
        bulkActionProgressPayload = {
          totalCount: ordersData.length,
          currentIndex: 0,
          errors: [],
        };

        nvDialog.showSingle($event, {
          templateUrl: 'lib/ninja-commons/services/dialogs/bulk-action-progress/bulk-action-progress.dialog.html',
          cssClass: 'bulk-action-progress',
          controller: 'BulkActionProgressDialogController',
          controllerAs: 'ctrl',
          skipHide: true,
          clickOutsideToClose: false,
          locals: {
            payload: bulkActionProgressPayload,
          },
        }).then(progressDialogClosed);

        startMultiSelectSaving(ordersToProcess);

        function progressDialogClosed() {
          if (getSuccessCount() > 0) {
            nvToast.info(
              nvTranslate.instant('container.order.edit.num-order-updated', { num: getSuccessCount() }),
              nvTranslate.instant('container.order.edit.cancel-order')
            );
          }

          $mdDialog.hide({ success: cancelledOrders, failed: unCancelledOrders });
        }

        function getSuccessCount() {
          return bulkActionProgressPayload.totalCount - bulkActionProgressPayload.errors.length;
        }
      }

      function nonMultiSelectSaving() {
        // non multi select suppose only has one order in ordersData payload
        const theOrder = ordersData[0];

        ctrl.formSubmitState = 'waiting';
        Order.cancel(_.get(theOrder, 'id'), generateSingleRequestPayload())
          .then(nonMultiSelectSuccess, () => {
            ctrl.formSubmitState = 'idle';
            nvToast.error(
              nvTranslate.instant('container.order.edit.cancel-num-orders-failed', { num: ordersData.length }),
              nvTranslate.instant('container.order.edit.order-x-comma-x', { orders: getTrackingIds(ordersData).join(', ') })
            );
          });

        function nonMultiSelectSuccess() {
          ctrl.formSubmitState = 'idle';
          nvToast.info(
            nvTranslate.instant('container.order.edit.num-order-cancelled', { num: ordersData.length }),
            nvTranslate.instant('container.order.edit.order-x-comma-x', { orders: getTrackingIds(ordersData).join(', ') })
          );

          $mdDialog.hide({ success: ordersData, failed: [] });
        }

        function getTrackingIds(orders) {
          const trackingIds = [];

          _.forEach(orders, (order) => {
            trackingIds.push(order.trackingId);
          });

          return trackingIds;
        }
      }
    }

    function onCancel() {
      $mdDialog.cancel();
    }

    function startMultiSelectSaving(orders) {
      let orderToProcess;
      if (orders.length > 0) {
        bulkActionProgressPayload.currentIndex += 1;
        orderToProcess = orders.splice(0, 1)[0];

        ctrl.formSubmitState = 'waiting';
        const data = generateSingleRequestPayload();
        Order.cancel(orderToProcess.id, data)
          .then(multiSelectSuccess, multiSelectFailure);
      } else {
        ctrl.formSubmitState = 'idle';
      }

      function multiSelectSuccess() {
        cancelledOrders = _.concat(cancelledOrders, orderToProcess);
        bulkActionProgressPayload.successCount += 1;

        if (orders.length > 0) {
          startMultiSelectSaving(orders); // recursive
        } else {
          ctrl.formSubmitState = 'idle';
        }
      }

      function multiSelectFailure() {
        unCancelledOrders = _.concat(unCancelledOrders, orderToProcess);
        bulkActionProgressPayload.errors.push(orderToProcess.trackingId);

        if (orders.length > 0) {
          startMultiSelectSaving(orders); // recursive
        } else {
          ctrl.formSubmitState = 'idle';
        }
      }
    }

    function generateSingleRequestPayload() {
      return {
        comments: ctrl.formComments || '',
      };
    }
  }
}());
