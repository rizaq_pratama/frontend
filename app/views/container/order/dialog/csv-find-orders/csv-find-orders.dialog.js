(function controller() {
  angular
    .module('nvOperatorApp.controllers')
    .controller('OrderFindByCsvDialogController', OrderFindByCsvDialogController);

  OrderFindByCsvDialogController.$inject = [
    '$mdDialog', 'nvButtonFilePickerType', 'Order', 'nvDialog', 'nvToast',
    'nvTranslate', '$timeout', '$q', '$log', 'nvCsvParser', 'extras'
  ];

  const DIALOG_TYPE = {
    DEFAULT: 'default',
    WITH_WEIGHT: 'with_weight',
  };

  function OrderFindByCsvDialogController(
    $mdDialog, nvButtonFilePickerType, Order, nvDialog, nvToast,
    nvTranslate, $timeout, $q, $log, nvCsvParser, extras
  ) {
    // variables
    const MAX_CALL_IN_CHUNK = 1000;
    const bulkActionProgressPayload = {
      totalCount: 1,
      currentIndex: 1,
      errors: [],
      messages: {
        updated: 'container.order.list.upload-complete',
        error: 'container.order.list.error.failed-to-find-following-rows',
      },
    };
    const validOrders = [];
    const { dialogType = DIALOG_TYPE.DEFAULT } = extras;
    const ctrl = this;
    ctrl.formSubmitState = 'idle';
    ctrl.isUploadFailed = false;
    ctrl.filePickerType = nvButtonFilePickerType.CSV;
    ctrl.fileSelected = [];
    let trackingIds = [];
    let originalDatas = [];

    // functions
    ctrl.getCsvSampleData = getCsvSampleData;
    ctrl.onFileSelect = onFileSelect;
    ctrl.resetSelectedFile = resetSelectedFile;
    ctrl.getFileName = getFileName;
    ctrl.onCancel = onCancel;
    ctrl.onSave = onSave;
    ctrl.isFormSubmitDisabled = isFormSubmitDisabled;

    // functions details
    function getCsvSampleData() {
      switch (dialogType) {
        case DIALOG_TYPE.WITH_WEIGHT:
          return JSON.stringify([
            { a: 'NVSGNINJA000000001', b: '20.0' },
            { a: 'NVSGNINJA000000002', b: '5.2' },
            { a: 'NVSGNINJA000000003', b: '1.4' },
          ]);
        default:
          return JSON.stringify([
            { a: 'NVSGNINJA000000001' },
            { a: 'NVSGNINJA000000002' },
            { a: 'NVSGNINJA000000003' },
          ]);
      }
    }

    function onFileSelect(file) {
      ctrl.isUploadFailed = false;
      ctrl.fileSelected.push(file);
      ctrl.fileName = file.name;
      nvCsvParser.parse(file).then(onSucces);

      function onSucces(result) {
        const datas = _.get(result, 'data');
        trackingIds = _(datas)
          .map(data => _.head(data))
          .uniq()
          .compact()
          .value();

        originalDatas = datas;
      }
    }

    function resetSelectedFile() {
      ctrl.fileSelected = [];
    }

    function getFileName() {
      const file = ctrl.fileSelected[0];
      return file.name || '';
    }

    function onCancel() {
      $mdDialog.cancel();
    }

    function onSave($event) {
      ctrl.formSubmitState = 'waiting';

      // start upload
      const promises = [];
      const chunkTrackingIds = _.chunk(trackingIds, MAX_CALL_IN_CHUNK);
      _.forEach(chunkTrackingIds, (theTrackingIds) => {
        const param = {
          search_filters: [
            {
              field: 'tracking_id',
              values: theTrackingIds,
            },
          ],
          search_field: null,
          search_range: null,
        };

        const param2 = _.cloneDeep(param);
        param2.search_filters = [
          {
            field: 'stamp_id',
            values: theTrackingIds,
          },
        ];
        promises.push(Order.elasticSearch(param, { from: 0, size: _.size(theTrackingIds) }));
        promises.push(Order.elasticSearch(param2, { from: 0, size: _.size(theTrackingIds) }));
      });

      $log.debug(`Search start ${new Date().getTime()}`);
      $q.all(promises).then(success, failure);

      function success(responses) {
        $log.debug(`Search end ${new Date().getTime()}`);
        let allSearchData = [];
        _.forEach(responses, (response) => {
          allSearchData = _.concat(allSearchData, response.search_data);
        });

        _(allSearchData)
          .map(sd => sd.order)
          .uniqBy('tracking_id')
          .forEach((o) => {
            validOrders.push(o);
            // pullout tracking id from the trackingIds list
            // the leftout tracking id mean not found
            _.pull(trackingIds, o.tracking_id);
            _.pull(trackingIds, o.stamp_id);
          });
        if (trackingIds && trackingIds.length > 0) {
          bulkActionProgressPayload.errors = trackingIds;
        }

        if (bulkActionProgressPayload.errors.length > 0) {
          showErrorLogDialog();
        } else {
          hideDialog();
        }

        ctrl.formSubmitState = 'idle';
      }

      function failure() {
        ctrl.formSubmitState = 'idle';

        ctrl.isUploadFailed = true;
        resetSelectedFile();
      }

      function showErrorLogDialog() {
        nvDialog.showSingle($event, {
          templateUrl: 'lib/ninja-commons/services/dialogs/bulk-action-progress/bulk-action-progress.dialog.html',
          cssClass: 'bulk-action-progress',
          controller: 'BulkActionProgressDialogController',
          controllerAs: 'ctrl',
          skipHide: true,
          clickOutsideToClose: false,
          locals: {
            payload: bulkActionProgressPayload,
          },
        }).then(progressDialogClosed);

        $timeout(() => {
          // wait progress dialog initialize first then only execute this
          bulkActionProgressPayload.successCount += 1;
        });

        function progressDialogClosed() {
          hideDialog();
        }
      }
    }

    function isFormSubmitDisabled() {
      return ctrl.fileSelected.length <= 0;
    }

    function hideDialog() {
      if (validOrders.length > 0) {
        nvToast.info(
          nvTranslate.instant('container.order.list.matches-with-file-shown-in-table'),
          getFileName()
        );
      }

      $mdDialog.hide({
        validOrders: validOrders,
        originalDatas: originalDatas,
      });
    }
  }
}());
