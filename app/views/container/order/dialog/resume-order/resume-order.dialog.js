(function controller() {
  angular
    .module('nvOperatorApp.controllers')
    .controller('OrderEditResumeOrderDialogController', OrderEditResumeOrderDialogController);

  OrderEditResumeOrderDialogController.$inject = [
    '$mdDialog', 'ordersData', 'Order', 'nvToast', 'nvTranslate',
    'settingsData', 'nvDialog',
  ];

  function OrderEditResumeOrderDialogController(
    $mdDialog, ordersData, Order, nvToast, nvTranslate,
    settingsData, nvDialog
  ) {
    // variables
    let bulkActionProgressPayload;

    const ctrl = this;
    ctrl.formSubmitState = 'idle';
    ctrl.orders = ordersData;
    ctrl.settings = _.defaults(settingsData, {
      isMultiSelect: false,
    });
    ctrl.submitButtonName = 'container.order.edit.resume-order';

    // initialize
    init();

    // functions
    ctrl.onCancel = onCancel;
    ctrl.onSave = onSave;

    // functions details
    function init() {
      if (ordersData.length > 1) {
        ctrl.submitButtonName = 'container.order.edit.resume-orders';
      }
    }

    function onSave($event) {
      if (ctrl.settings.isMultiSelect) {
        multiSelectSaving();
      } else {
        nonMultiSelectSaving();
      }

      function multiSelectSaving() {
        const ordersToProcess = _.cloneDeep(ordersData);
        bulkActionProgressPayload = {
          totalCount: ordersData.length,
          currentIndex: 0,
          errors: [],
        };

        nvDialog.showSingle($event, {
          templateUrl: 'lib/ninja-commons/services/dialogs/bulk-action-progress/bulk-action-progress.dialog.html',
          cssClass: 'bulk-action-progress',
          controller: 'BulkActionProgressDialogController',
          controllerAs: 'ctrl',
          skipHide: true,
          clickOutsideToClose: false,
          locals: {
            payload: bulkActionProgressPayload,
          },
        }).then(progressDialogClosed);

        startMultiSelectSaving(ordersToProcess);

        function progressDialogClosed() {
          if (getSuccessCount() > 0) {
            nvToast.info(
              nvTranslate.instant('container.order.edit.num-order-updated', { num: getSuccessCount() }),
              nvTranslate.instant('container.order.edit.resume-order')
            );
          }

          $mdDialog.hide();
        }

        function getSuccessCount() {
          return bulkActionProgressPayload.totalCount - bulkActionProgressPayload.errors.length;
        }
      }

      function nonMultiSelectSaving() {
        const data = [];
        _.forEach(ordersData, (orderData) => {
          data.push(generateSingleRequestPayload(orderData));
        });

        ctrl.formSubmitState = 'waiting';
        Order.resume(data).then(nonMultiSelectSuccess, () => {
          ctrl.formSubmitState = 'idle';
        });

        function nonMultiSelectSuccess(resumedOrders) {
          ctrl.formSubmitState = 'idle';

          const unResumedOrders = _.differenceBy(ordersData, resumedOrders, 'id');
          if (resumedOrders.length > 0) {
            const resumedTrackingIds = getTrackingIds(resumedOrders);

            nvToast.info(
              nvTranslate.instant('container.order.edit.num-order-resumed', { num: resumedOrders.length }),
              nvTranslate.instant('container.order.edit.order-x-comma-x', { orders: resumedTrackingIds.join(', ') })
            );
          }

          if (unResumedOrders.length > 0) {
            const unResumedTrackingIds = getTrackingIds(unResumedOrders);

            nvToast.error(
              nvTranslate.instant('container.order.edit.resume-num-orders-failed', { num: unResumedOrders.length }),
              nvTranslate.instant('container.order.edit.order-x-comma-x', { orders: unResumedTrackingIds.join(', ') })
            );
          }

          if (resumedOrders.length > 0) {
            $mdDialog.hide();
          }
        }

        function getTrackingIds(orders) {
          const trackingIds = [];

          _.forEach(orders, (order) => {
            trackingIds.push(order.trackingId);
          });

          return trackingIds;
        }
      }
    }

    function onCancel() {
      $mdDialog.cancel();
    }

    function startMultiSelectSaving(orders) {
      let orderToProcess;
      if (orders.length > 0) {
        bulkActionProgressPayload.currentIndex += 1;
        orderToProcess = orders.splice(0, 1)[0];

        ctrl.formSubmitState = 'waiting';
        const data = [generateSingleRequestPayload(orderToProcess)];
        Order.resume(data).then(multiSelectSuccess, multiSelectFailure);
      }

      function multiSelectSuccess(resumedOrders) {
        const unResumedOrders = _.differenceBy([orderToProcess], resumedOrders, 'id');
        if (unResumedOrders.length > 0) {
          bulkActionProgressPayload.errors.push(orderToProcess.trackingId);
        } else {
          bulkActionProgressPayload.successCount += 1;
        }

        if (orders.length > 0) {
          startMultiSelectSaving(orders); // recursive
        } else {
          ctrl.formSubmitState = 'idle';
        }
      }

      function multiSelectFailure() {
        bulkActionProgressPayload.errors.push(orderToProcess.trackingId);

        if (orders.length > 0) {
          startMultiSelectSaving(orders); // recursive
        } else {
          ctrl.formSubmitState = 'idle';
        }
      }
    }

    function generateSingleRequestPayload(order) {
      return {
        trackingId: order.trackingId,
      };
    }
  }
}());
