(function controller() {
  angular
    .module('nvOperatorApp.controllers')
    .controller('OrderPinDialogController', OrderPinDialogController);

  OrderPinDialogController.$inject = ['$mdDialog', 'nvToast', 'passcode', 'nvTranslate'];

  function OrderPinDialogController($mdDialog, nvToast, passcode, nvTranslate) {
    const ctrl = this;

    ctrl.pin = '';
    ctrl.passcode = passcode;
    ctrl.onCancel = onCancel;
    ctrl.onSet = onSet;
    ctrl.onEnter = onEnter;

    function onCancel() {
      $mdDialog.cancel();
    }

    function onSet() {
      if (ctrl.passcode === ctrl.pin) {
        $mdDialog.hide(0);
      } else {
        nvToast.error(
          nvTranslate.instant('container.order.edit.wrong-passcode-entered')
        );
        $mdDialog.hide(-1);
      }
    }

    function onEnter($event) {
      if ($event.charCode === 13) {
        onSet();
      }
    }
  }
}());
