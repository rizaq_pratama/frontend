(function controller() {
  angular
    .module('nvOperatorApp.controllers')
    .controller('OrderPhotosDialogController', OrderPhotosDialogController);

  OrderPhotosDialogController.$inject = ['$mdDialog', 'nvToast', 'orderData', 'nvTranslate', 'Order'];

  function OrderPhotosDialogController($mdDialog, nvToast, orderData, nvTranslate, Order) {
    const ctrl = this;
    const ORDER = _.cloneDeep(orderData);

    ctrl.isPhotosAvailable = isPhotosAvailable;
    ctrl.onCancel = onCancel;
    init();

    function init() {
      ctrl.loading = true;

      Order.getOrderPhotos(_.get(ORDER, 'id'))
        .then(onSuccess, onFailure);

      function onSuccess(datas) {
        let counter = 0;
        ctrl.photos = _.map(datas, (data) => {
          const result = {
            url: data.image_url,
            title: `${data.type} - ${data.job_id}`,
            id: counter,
            isActive: false,
          };
          counter += 1;
          return result;
        });
        ctrl.loading = false;
      }

      function onFailure() {
        $mdDialog.hide(-1);
      }
    }

    function onCancel() {
      $mdDialog.cancel();
    }

    function isPhotosAvailable() {
      return _.size(ctrl.photos) > 0;
    }
  }
}());
