(function controller() {
  angular
    .module('nvOperatorApp.controllers')
    .controller('OrderRescheduleDialogController', OrderRescheduleDialogController);

  OrderRescheduleDialogController.$inject = ['$mdDialog', 'orderData', 'isPickup', 'Timewindow',
    'nvDateTimeUtils', 'nvTimezone', 'nvTranslate', 'nvToast', '$scope', 'Order', 'DistributionPoint',
    '$timeout'];

  const PAGE_STATE = {
    LOADING: 'loading',
    CONTENT: 'content',
  };

  const EMPTY_ARRAY = [];

  /* eslint no-underscore-dangle: ["error", { "allow": ["_form", "_lastDDNT"] }]*/
  function OrderRescheduleDialogController($mdDialog, orderData, isPickup, Timewindow,
    nvDateTimeUtils, nvTimezone, nvTranslate, nvToast, $scope, Order, DistributionPoint,
    $timeout) {
    const ctrl = this;

    ctrl.PAGE_STATE = PAGE_STATE;

    ctrl.order = _.cloneDeep(orderData);
    ctrl.isPickup = isPickup;
    ctrl.isUseDp = false;
    ctrl.timeslotOptions = [];
    ctrl.formSubmitState = 'idle';
    ctrl.order._form = {};
    ctrl.pageState = PAGE_STATE.LOADING;

    ctrl.onCancel = onCancel;
    ctrl.onSave = onSave;
    ctrl.isAbleToSubmit = isAbleToSubmit;
    ctrl.toggleChangeNewAddress = toggleChangeNewAddress;
    ctrl.onAddressFinderSearchModeChanged = onAddressFinderSearchModeChanged;
    ctrl.onAddressFinderCallback = onAddressFinderCallback;
    ctrl.onDpChanged = onDpChanged;

    init();

    function init() {
      DistributionPoint
        .getCapacity({ all: true })
        .then(onGetDpCapacityResult, () => $mdDialog.cancel());

      ctrl.key = {};
      if (ctrl.isPickup) {
        ctrl.key.dialogTitle = 'container.order.edit.reschedule-pickup';
        ctrl.key.basicDetails = 'container.order.edit.sender-details';
        ctrl.key.labelName = 'commons.sender-name';
        ctrl.key.labelContact = 'commons.sender-contact';
        ctrl.key.labelEmail = 'commons.sender-email';
        ctrl.key.inputDate = 'commons.model.pickup-date';
        ctrl.key.inputTimeslot = 'container.order.edit.pickup-timeslot';
      } else {
        ctrl.key.dialogTitle = 'container.order.edit.reschedule-delivery';
        ctrl.key.basicDetails = 'container.order.edit.recipient-details';
        ctrl.key.labelName = 'commons.recipient-name';
        ctrl.key.labelContact = 'commons.recipient-contact';
        ctrl.key.labelEmail = 'commons.recipient-email';
        ctrl.key.inputDate = 'commons.model.delivery-date';
        ctrl.key.inputTimeslot = 'container.order.edit.delivery-timeslot';
      }

      const orderParam = getDeliveryParameter(ctrl.order);
      _.assign(ctrl.order._form, {
        name: ctrl.isPickup ? _.get(ctrl.order, 'fromName', '') : _.get(ctrl.order, 'toName', ''),
        contact: ctrl.isPickup ? _.get(ctrl.order, 'fromContact', '') : _.get(ctrl.order, 'toContact', ''),
        email: ctrl.isPickup ? _.get(ctrl.order, 'fromEmail', '') : _.get(ctrl.order, 'toEmail', ''),
        isChangeNewAddress: false,
        isAddressFinder: false,
        oldAddress1: ctrl.isPickup ? _.get(ctrl.order, 'fromAddress1', '') : _.get(ctrl.order, 'toAddress1', ''),
        oldAddress2: ctrl.isPickup ? _.get(ctrl.order, 'fromAddress2', '') : _.get(ctrl.order, 'toAddress2', ''),
        oldCity: ctrl.isPickup ? _.get(ctrl.order, 'fromCity', '') : _.get(ctrl.order, 'toCity', ''),
        oldCountry: ctrl.isPickup ? _.get(ctrl.order, 'fromCountry', '') : _.get(ctrl.order, 'toCountry', ''),
        oldPostcode: ctrl.isPickup ? _.get(ctrl.order, 'fromPostcode', '') : _.get(ctrl.order, 'toPostcode', ''),
        address1: '',
        address2: '',
        city: '',
        country: '',
        postcode: '',
        latitude: null,
        longitude: null,
        date: orderParam.date,
        timewindowId: orderParam.timewindowId,
      });

      ctrl.timeslotOptions = Timewindow.toOptions([
        Timewindow.TYPE.DAYSLOT,
        Timewindow.TYPE.NIGHTSLOT,
        Timewindow.TYPE.DAYNIGHTSLOT,
        Timewindow.TYPE.TIMESLOT,
      ]);

      function getDeliveryParameter(order) {
        const lastDDNT = order._lastDDNT;
        if (lastDDNT) {
          const startTime = nvDateTimeUtils.toMoment(
            lastDDNT.startTime, nvTimezone.getOperatorTimezone()
          );
          return {
            timewindowId: Timewindow.getTimeWindowId(lastDDNT),
            date: nvDateTimeUtils.toDate(startTime),
          };
        }
        return {
          timewindowId: null,
          date: new Date(),
        };
      }

      function onGetDpCapacityResult(response) {
        const extendedDps = DistributionPoint.extendDps(response.dps);
        ctrl.dpOptions = DistributionPoint.buildOptions(extendedDps);
        ctrl.dpDateOptionsCollection = DistributionPoint.buildDateOptions(extendedDps);
        ctrl.order._form.selectedDp = null;
        ctrl.order._form.selectedDropOffDate = null;
        ctrl.pageState = PAGE_STATE.CONTENT;
      }
    }

    function isAbleToSubmit() {
      return $scope.modelForm && $scope.modelForm.$valid &&
        !ctrl.order._form.isAddressFinder &&
        ctrl.order._form.timewindowId !== null &&
        moment(ctrl.order._form.date).isValid();
    }

    function toggleChangeNewAddress() {
      ctrl.order._form.isChangeNewAddress = !ctrl.order._form.isChangeNewAddress;

      if (ctrl.order._form.isChangeNewAddress) {
        scrollTo('change-new-address-holder');
      }
    }

    function onAddressFinderSearchModeChanged() {
      scrollTo('address-finder-holder');
    }

    function onAddressFinderCallback(address) {
      ctrl.order._form.isAddressFinder = false;

      if (!address) {
        return;
      }

      ctrl.order._form.address1 = address.address1;
      ctrl.order._form.address2 = address.address2;
      ctrl.order._form.city = address.city;
      ctrl.order._form.country = address.country;
      ctrl.order._form.postcode = address.postcode;
      ctrl.order._form.latitude = address.latitude;
      ctrl.order._form.longitude = address.longitude;
    }

    function onSave() {
      const payload = {
        contact: ctrl.order._form.contact,
        email: ctrl.order._form.email,
        name: ctrl.order._form.name,
        date: nvDateTimeUtils.displayDate(ctrl.order._form.date),
        time_window: ctrl.order._form.timewindowId,
      };

      if (ctrl.isUseDp) {
        const dp = _.find(ctrl.dpOptions, dpOption => dpOption.value === ctrl.order._form.selectedDp);
        _.assign(payload, {
          date: nvDateTimeUtils.displayDate(ctrl.order._form.selectedDropOffDate),
          dp_id: dp.value,
          dpms_id: dp.legacyValue,
        });
      }

      if (ctrl.order._form.isChangeNewAddress) {
        _.assign(payload, {
          address_1: ctrl.order._form.address1,
          address_2: ctrl.order._form.address2,
          postal_code: ctrl.order._form.postcode,
          city: ctrl.order._form.city,
          country: ctrl.order._form.country,
          latitude: ctrl.order._form.latitude,
          longitude: ctrl.order._form.longitude,
        });
      }

      ctrl.formSubmitState = 'waiting';
      Order.reschedule(ctrl.order.id, payload)
        .then(success)
        .finally(() => (ctrl.formSubmitState = 'idle'));

      function success(resp) {
        if (Order.isUpdateOrderSuccess(resp)) {
          nvToast.success(nvTranslate.instant('container.order.edit.order-rescheduled-successfully'));
          $mdDialog.hide(0);
        } else {
          nvToast.error(nvTranslate.instant('container.order.edit.failed-to-reschedule'));
        }
      }
    }

    function scrollTo(className) {
      return _.defer(() =>
          angular.element(
            $('.order-edit-reschedule md-dialog > md-dialog-content')
          ).duScrollToElementAnimated(
            angular.element(document.getElementsByClassName(className))
          )
      );
    }

    function onCancel() {
      $mdDialog.cancel();
    }

    function onDpChanged() {
      $timeout(() => {
        ctrl.dateOptions = ctrl.order._form.selectedDp
          ? ctrl.dpDateOptionsCollection[ctrl.order._form.selectedDp].options
          : EMPTY_ARRAY;
        ctrl.order._form.selectedDropOffDate = _.size(ctrl.dateOptions) > 0 ? ctrl.dateOptions[0].value : null;
      }, 0);
    }
  }
}());
