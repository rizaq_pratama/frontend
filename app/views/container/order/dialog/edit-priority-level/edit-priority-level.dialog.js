(function controller() {
  angular
    .module('nvOperatorApp.controllers')
    .controller('OrderEditPriorityLevelDialogController', OrderEditPriorityLevelDialogController);

  OrderEditPriorityLevelDialogController.$inject = ['$mdDialog', 'orderData', 'Order', 'nvToast',
    'nvTranslate'];

  function OrderEditPriorityLevelDialogController($mdDialog, orderData, Order, nvToast,
    nvTranslate) {
    const ctrl = this;
    const orderId = orderData.id;
    const currentPpPriorityLevel = orderData.prioritylevel.pickup || 0; // dont use first
    const currentDdPriorityLevel = orderData.prioritylevel.delivery || 0;

    ctrl.onCancel = onCancel;
    ctrl.onSave = onSave;
    ctrl.isAbleToSubmit = isAbleToSubmit;

    init();

    function init() {
      ctrl.formSubmitState = 'idle';
      ctrl.pickupPriorityLevel = currentPpPriorityLevel;
      ctrl.deliveryPriorityLevel = currentDdPriorityLevel;
    }

    function onCancel() {
      $mdDialog.cancel();
    }

    function onSave() {
      const payload = {
        parcel_job: {
          delivery_priority_level: ctrl.deliveryPriorityLevel,
        },
      };
      Order.updateV2(orderId, payload).then(onSuccess);
      
      function onSuccess(resp) {
        if (Order.isUpdateOrderSuccess(resp)) {
          $mdDialog.hide(0);
        } else {
          const errorMessage = resp.message || nvTranslate.instant('container.order.edit.edit-order-failed');
          nvToast.error(errorMessage);
        }
      }
    }

    function isAbleToSubmit() {
      if (ctrl.pickupPriorityLevel) {
        return ctrl.pickupPriorityLevel >= currentPpPriorityLevel;
      }
      if (ctrl.deliveryPriorityLevel) {
        return ctrl.deliveryPriorityLevel >= currentDdPriorityLevel;
      }
      return false;
    }
  }
}());
