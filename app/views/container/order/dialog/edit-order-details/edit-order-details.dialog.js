(function controller() {
  angular
    .module('nvOperatorApp.controllers')
    .controller('OrderEditDetailsDialogController', OrderEditDetailsDialogController);

  OrderEditDetailsDialogController.$inject = [
    '$q', '$mdDialog', 'DeliveryTypesModel', 'orderData', 'nvDateTimeUtils', '$timeout',
    'Order', 'nvToast', 'nvTranslate', '$scope', 'nvUtils',
  ];

  const SIZE_OPTIONS = [
    { value: { id: 0, name: 'SMALL' }, displayName: 'S' },
    { value: { id: 1, name: 'MEDIUM' }, displayName: 'M' },
    { value: { id: 2, name: 'LARGE' }, displayName: 'L' },
    { value: { id: 3, name: 'EXTRALARGE' }, displayName: 'XL' },
    { value: { id: 4, name: 'XXLARGE' }, displayName: 'XXL' },
  ];

  const STATE = {
    OK: 1,
    WAITING: 0,
    ERROR: -1,
  };

  function OrderEditDetailsDialogController(
    $q, $mdDialog, DeliveryTypesModel, orderData, nvDateTimeUtils, $timeout,
    Order, nvToast, nvTranslate, $scope, nvUtils
  ) {
    // variables
    const ctrl = this;
    ctrl.order = _.cloneDeep(orderData);
    ctrl.view = {};
    ctrl.state = {};
    ctrl.func = {};
    ctrl.view.provided = {};
    ctrl.view.multiparcel = {};
    ctrl.func.multiparcel = {};

    // view
    ctrl.view.trackingId = ctrl.order.trackingId;
    ctrl.view.orderSource = 'API';
    ctrl.view.createdAt = nvDateTimeUtils.displayDateTime(
      nvDateTimeUtils.toMoment(ctrl.order.createdAt));

    ctrl.view.deliveryTypeOptions = [];
    ctrl.view.deliveryType = null;

    ctrl.view.sizeOptions = SIZE_OPTIONS;
    ctrl.view.provided.parcelSize = '-';
    ctrl.view.provided.parcelWeight = formatParcelWeight(ctrl.order.originalWeight);
    ctrl.view.provided.length = getParcelDimensions('length', true);
    ctrl.view.provided.width = getParcelDimensions('width', true);
    ctrl.view.provided.breadth = getParcelDimensions('height', true);
    ctrl.view.newSize = getParcelSize().value;
    ctrl.view.newWeight = ctrl.order.weight;
    ctrl.view.newLength = getParcelDimensions('length');
    ctrl.view.newWidth = getParcelDimensions('width');
    ctrl.view.newBreadth = getParcelDimensions('height');
    ctrl.view.insurance = ctrl.order.insurance;
    ctrl.view.providedInsurance = ctrl.order.insurance;
    ctrl.refDimensions = _.pick(ctrl.view, ['newWeight', 'newLength', 'newWidth', 'newBreadth']);
    // multiparcel view
    ctrl.view.multiparcel.datas = [];
    ctrl.view.multiparcel.minQuery = 5;
    ctrl.view.multiparcel.selectedOrder = null;
    ctrl.view.multiparcel.searchTrackingId = '';
    ctrl.view.multiparcel.isChanged = false;

    // state
    ctrl.state.deliveryTypesState = STATE.OK;
    ctrl.state.saveButtonState = 'idle';
    ctrl.state.tab = 0;

    // functions
    ctrl.func.onCancel = onCancel;
    ctrl.func.onSave = onSave;
    ctrl.func.onTabChanged = onTabChanged;
    ctrl.func.isAbleToSubmit = isAbleToSubmit;
    ctrl.func.multiparcel.onAdd = onAdd;
    ctrl.func.multiparcel.findTrackingId = findTrackingId;
    ctrl.func.multiparcel.remove = multiParcelRemove;

    init();

    // functions implementations
    function onSave() {
      ctrl.state.saveButtonState = 'waiting';
      // 0 is first tab and so on
      if (ctrl.state.tab === 0) {
        const order = getOrderProperties();
        const deliveryType = { deliveryTypeValue: ctrl.view.deliveryType.id };

        $q.all([
          Order.updateV2(ctrl.order.id, order),
          Order.updateDeliveryTypes(ctrl.order.id, deliveryType)])
          .then(successOrderDetails);
      } else {
        const orderIds = _.map(ctrl.view.multiparcel.datas, d => d.id);
        orderIds.push(ctrl.order.id);

        Order.updateMultiParcels(ctrl.order.id,
          { orders: orderIds }
        )
        .then(successMultiParcel)
        .finally(() => (ctrl.state.saveButtonState = 'idle'));
      }

      function successOrderDetails(responses) {
        const updateResponse = responses[0];
        if (Order.isUpdateOrderSuccess(updateResponse)) {
          nvToast.success(nvTranslate.instant('container.order.edit.edit-order-success'));
          $mdDialog.hide(0);
        } else {
          const errorMessage = updateResponse.message || nvTranslate.instant('container.order.edit.edit-order-failed');
          nvToast.error(errorMessage);
        }
      }

      function successMultiParcel() {
        nvToast.success(nvTranslate.instant('container.order.edit.edit-order-multi-parcel-success'));
        $mdDialog.hide(0);
      }
    }

    function onCancel() {
      $mdDialog.cancel();
    }

    function onTabChanged(mode) {
      ctrl.state.tab = mode;
    }

    function init() {
      DeliveryTypesModel.getTypes()
        .then(extractDeliveryTypes, error);

      if (ctrl.order.multiParcelOrders.length > 0) {
        const datas = _.map(ctrl.order.multiParcelOrders, data => ({
          state: STATE.OK,
          id: data.id,
          trackingId: data.trackingId,
          granularStatus: data.granularStatus }));
        _.each(datas, data => ctrl.view.multiparcel.datas.push(data));
      }

      function extractDeliveryTypes(datas) {
        _.each(datas, (d) => {
          const data = { value: { id: d.id }, displayName: d.name };
          ctrl.view.deliveryTypeOptions.push(data);
        });
        ctrl.view.deliveryType = _.find(ctrl.view.deliveryTypeOptions, d =>
          d.displayName === ctrl.order.deliveryType)
          .value;

        ctrl.state.deliveryTypesState = STATE.OK;
      }

      function error() {
        ctrl.state.deliveryTypesState = STATE.ERROR;
      }
    }

    function getParcelSize() {
      if (ctrl.order) {
        const result = _.find(ctrl.view.sizeOptions, size =>
          size.value.name === ctrl.order.parcelSize);
        if (result) {
          return result;
        }
        return ctrl.view.sizeOptions[0];
      }
      return ctrl.view.sizeOptions[0];
    }

    function formatParcelWeight(weight) {
      if (weight < 1000) {
        return Math.round(weight * 100) / 100;
      }
      return '>999';
    }

    function getParcelDimensions(key, provided) {
      if (ctrl.order && ctrl.order.dimensions) {
        if (ctrl.order.dimensions[key]) {
          return ctrl.order.dimensions[key];
        }
        return provided ? '-' : 0;
      }
      return undefined;
    }

    function onAdd(item) {
      ctrl.view.multiparcel.isChanged = true;
      ctrl.view.multiparcel.selectedOrder = null;
      ctrl.view.multiparcel.searchTrackingId = '';
      ctrl.view.multiparcel.datas.push(item);
    }

    function findTrackingId(query) {
      const filterData = {
        search_field: {
          fields: ['tracking_id', 'stamp_id'],
          value: _.toLower(query),
          match_type: Order.ELASTIC_SEARCH_MATCH_TYPE.FULL_TEXT,
        },
      };

      const defer = $q.defer();
      Order.elasticSearch(filterData, {
        from: 0,
        size: 100,
      }).then(success, error);

      return defer.promise;

      function success(response) {
        const orders = extendESOrders(_.map(response.search_data, 'order'));

        defer.resolve(orders);
      }

      function error() {
        defer.resolve([]);
      }

      function extendESOrders(orders) {
        return _.map(orders, order => (
          setCustomESOrderData(order)
        ));

        function setCustomESOrderData(order) {
          _.forEach(order, (value, key) => {
            order[snakeToCamel(key)] = value;
          });

          return order;
        }

        function snakeToCamel(string) {
          return string.replace(/(_\w)/g, m => m[1].toUpperCase());
        }
      }
    }

    function multiParcelRemove(trackingId) {
      ctrl.view.multiparcel.isChanged = true;
      _.remove(ctrl.view.multiparcel.datas, data => data.trackingId === trackingId);
    }

    function getOrderProperties() {
      const order = {};
      const parcelJob = {};
      const dimensionsRef = {
        length: ctrl.refDimensions.newLength,
        width: ctrl.refDimensions.newWidth,
        height: ctrl.refDimensions.newBreadth,
      };
      let dimensions = {};
      _.assign(dimensions, {
        length: parseFloat(ctrl.view.newLength),
        width: parseFloat(ctrl.view.newWidth),
        height: parseFloat(ctrl.view.newBreadth),
      });

      // if there's no change in the dimensions object, remove L, W, H dimension element
      const diff = nvUtils.getObjectDiff(dimensionsRef, dimensions);
      if (_.isEmpty(diff)) {
        dimensions = {};
      }

      // see NV-5144 for details on weight and pricing_weight
      // check if weight changed
      const newWeight = parseFloat(ctrl.view.newWeight);
      if (ctrl.refDimensions.newWeight !== newWeight) {
        dimensions.weight = newWeight;
        dimensions.pricing_weight = newWeight;
      }

      // check changes for parcelSize
      if (ctrl.order.parcelSize !== ctrl.view.newSize.name) {
        dimensions.parcel_size = ctrl.view.newSize.name;
      }
      _.assign(parcelJob, { dimensions: dimensions });
      // only update   insurance value if changed
      // round the insured value to 2 decimal value
      if (ctrl.view.insurance !== ctrl.view.providedInsurance) {
        _.assign(parcelJob, { insurance: _.round(ctrl.view.insurance, 2) });
      }

      _.assign(order, { parcel_job: parcelJob });

      return order;
    }

    function isAbleToSubmit() {
      let result = true;
      if (ctrl.state.tab === 0) {
        result = result && (checkDirtyForm('width') ?
          (ctrl.view.newWidth != null && ctrl.view.newWidth !== '') : true);
        result = result && (checkDirtyForm('breadth') ?
          (ctrl.view.newBreadth != null && ctrl.view.newBreadth !== '') : true);
        result = result && (checkDirtyForm('length') ?
          (ctrl.view.newLength != null && ctrl.view.newLength !== '') : true);
        result = result && (ctrl.view.newSize != null);
        return result;
      } else if (ctrl.state.tab === 1) {
        result = result && ctrl.view.multiparcel.isChanged;
        return result;
      }
      return false;

      function checkDirtyForm(inputName) {
        if ($scope.modelForm && $scope.modelForm[inputName]) {
          return $scope.modelForm[inputName].$dirty;
        }
        return false;
      }
    }
  }
}());
