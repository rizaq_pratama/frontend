(function controller() {
  angular
    .module('nvOperatorApp.controllers')
    .controller('OrderEditCancelRtsDialogController', OrderEditCancelRtsDialogController);

  OrderEditCancelRtsDialogController.$inject = [
    '$mdDialog', 'orderData', 'Order', 'nvToast', 'nvTranslate', '$q',
  ];

  function OrderEditCancelRtsDialogController(
    $mdDialog, orderData, Order, nvToast, nvTranslate, $q
  ) {
    // variables
    const ctrl = this;
    ctrl.orderId = orderData.id;

    ctrl.formSubmitState = 'idle';

    // functions
    ctrl.onCancel = onCancel;
    ctrl.onSave = onSave;

    // functions details
    function onSave() {
      ctrl.formSubmitState = 'waiting';
      Order.update(ctrl.orderId, { rts: false }).then(success, $q.reject);

      function success(response) {
        ctrl.formSubmitState = 'idle';
        if (response.rts === false) {
          nvToast.info(nvTranslate.instant('container.order.edit.rts-cancelled'));
          $mdDialog.hide();
        } else {
          nvToast.error(nvTranslate.instant('container.order.edit.cancel-rts-failed'));
        }
      }
    }

    function onCancel() {
      $mdDialog.cancel();
    }
  }
}());
