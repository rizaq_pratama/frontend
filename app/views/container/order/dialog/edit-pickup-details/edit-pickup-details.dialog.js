(function controller() {
  angular
    .module('nvOperatorApp.controllers')
    .controller('OrderEditPickupDetailsDialogController', OrderEditPickupDetailsDialogController);

  OrderEditPickupDetailsDialogController.$inject = [
    '$mdDialog', 'orderData', 'nvTranslate', 'Order', 'nvToast',
    'nvDateTimeUtils', 'Timewindow', '$scope', 'nvTimezone',
    'Address', 'nvDomain', '$timeout', 'Transaction',
  ];

  function OrderEditPickupDetailsDialogController(
    $mdDialog, orderData, nvTranslate, Order, nvToast,
    nvDateTimeUtils, Timewindow, $scope, nvTimezone,
    Address, nvDomain, $timeout, Transaction
  ) {
    // variables
    const ctrl = this;
    const COUNTRY_WITH_PROVINCES = ['id', 'ph', 'th'];
    ctrl.order = _.cloneDeep(orderData);
    ctrl.pickupTimeslotOptions = [];
    ctrl.formSubmitState = 'idle';
    ctrl.pickupRequiredLock = false; // to handle pickup_required field

    // functions
    ctrl.onCancel = onCancel;
    ctrl.onSave = onSave;
    ctrl.isAbleToSubmit = isAbleToSubmit;
    ctrl.isProvinceListAvailable = isProvinceListAvailable;
    ctrl.isL2ListAvailable = isL2ListAvailable;
    ctrl.isL3ListAvailable = isL3ListAvailable;
    ctrl.onProvinceChange = onProvinceChange;
    ctrl.onL2Change = onL2Change;
    ctrl.toggleChangeNewAddress = toggleChangeNewAddress;
    ctrl.onAddressFinderSearchModeChanged = onAddressFinderSearchModeChanged;
    ctrl.onAddressFinderCallback = onAddressFinderCallback;
    ctrl.isReturnOrder = isReturnOrder;

    // start
    initialize();

    // functions details
    function initialize() {
      if (isCountryWithProvince()) {
        Address
          .dpZones(nvDomain.getDomain().currentCountry)
          .then((datas) => {
            const CLEAN_DATAS = _.map(datas, el => ({
              l1_name: _.trim(el.l1_name),
              l2_name: _.trim(el.l2_name),
              l3_name: _.trim(el.l3_name),
            }));
            ctrl.provinces = _(CLEAN_DATAS).map(el => _.trim(el.l1_name))
              .uniq()
              .map(el => ({
                displayName: el,
                value: el,
              }))
              .sortBy('displayName')
              .value();

            ctrl.l2Names = {};
            _.forEach(CLEAN_DATAS, (el) => {
              const l1Name = el.l1_name;
              if (el.l2_name) {
                if (_.size(ctrl.l2Names[l1Name]) > 0) {
                  ctrl.l2Names[l1Name].push(el.l2_name);
                } else {
                  ctrl.l2Names[l1Name] = [el.l2_name];
                }
              }
            });
            _.forEach(ctrl.l2Names, (val, key) => {
              ctrl.l2Names[key] = _(val).uniq()
                .map(el => ({
                  displayName: el,
                  value: el,
                }))
                .sortBy('displayName')
                .value();
            });

            ctrl.l3Names = {};
            _.forEach(CLEAN_DATAS, (el) => {
              const l2Name = el.l2_name;
              if (el.l3_name) {
                if (_.size(ctrl.l3Names[l2Name]) > 0) {
                  ctrl.l3Names[l2Name].push(el.l3_name);
                } else {
                  ctrl.l3Names[l2Name] = [el.l3_name];
                }
              }
            });
            _.forEach(ctrl.l3Names, (val, key) => {
              ctrl.l3Names[key] = _(val).uniq()
                .map(el => ({
                  displayName: el,
                  value: el,
                }))
                .sortBy('displayName')
                .value();
            });
          });
      }
      const lastPpntTxn = Transaction.getLastPPNT(ctrl.order.transactions);
      const pickupRequired = _.get(lastPpntTxn, 'waypointId') != null;
      if (pickupRequired) {
        ctrl.pickupRequiredLock = true;
      }
      ctrl.order._form = {
        name: ctrl.order.fromName,
        contact: ctrl.order.fromContact,
        email: ctrl.order.fromEmail,
        isChangeNewAddress: false,
        isAddressFinder: false,
        address1: '',
        address2: '',
        city: '',
        country: '',
        postcode: '',
        date: new Date(),
        timewindowId: null,
        routeComments: '',
        shipperRequested: false,
        pickupRequired: pickupRequired,
      };

      ctrl.pickupTimeslotOptions = Timewindow.toOptions([
        Timewindow.TYPE.DAYSLOT,
        Timewindow.TYPE.NIGHTSLOT,
        Timewindow.TYPE.DAYNIGHTSLOT,
        Timewindow.TYPE.TIMESLOT,
      ]);
    }

    function onSave() {
      const fromObject = constructFromObject();
      const parcelJobObject = constructParcelJobObject();
      const payload = {
        from: fromObject,
        parcel_job: parcelJobObject,
      };

      if (isReturnOrder() && !ctrl.pickupRequiredLock) {
        _.assign(payload, { pickup_required: ctrl.order._form.pickupRequired });
      }

      ctrl.formSubmitState = 'waiting';
      Order.updateV2(ctrl.order.id, payload)
        .then(success)
        .finally(() => (ctrl.formSubmitState = 'idle'));

      function success(resp) {
        if (Order.isUpdateOrderSuccess(resp)) {
          nvToast.success(nvTranslate.instant('container.order.edit.pickup-details-updated'));
          $mdDialog.hide(0);
        } else {
          const errorMessage = resp.message || nvTranslate.instant('container.order.edit.edit-order-failed');
          nvToast.error(errorMessage);
        }
      }

      function constructFromObject() {
        const from = {
          name: ctrl.order._form.name,
          email: ctrl.order._form.email,
          phone_number: ctrl.order._form.contact,
        };

        if (ctrl.order._form.isChangeNewAddress) {
          const address = {
            address1: ctrl.order._form.address1,
            address2: ctrl.order._form.address2,
            postcode: ctrl.order._form.postcode,
            city: ctrl.order._form.city,
            country: ctrl.order._form.country,
            latitude: ctrl.order._form.latitude,
            longitude: ctrl.order._form.longitude,
          };

          if (isProvinceListAvailable()) {
            const address2 = _([address.address2, ctrl.order._form.province, ctrl.order._form.l2, ctrl.order._form.l3]).compact().join(', ');
            _.assign(address, { address2: address2, district: ctrl.order._form.province });
          }
          _.assign(from, { address: address });
        }

        return from;
      }

      function constructParcelJobObject() {
        const parcelJob = {
          pickup_date: nvDateTimeUtils.displayDate(ctrl.order._form.date,
            nvTimezone.getOperatorTimezone()), // local date
          pickup_timewindow: +ctrl.order._form.timewindowId,
          shipper_requested: ctrl.order._form.shipperRequested,
        };

        return parcelJob;
      }
    }

    function isAbleToSubmit() {
      return $scope.modelForm && $scope.modelForm.$valid &&
        !ctrl.order._form.isAddressFinder &&
        ctrl.order._form.timewindowId !== null &&
        moment(ctrl.order._form.date).isValid() &&
        (!isL2ListAvailable() || (isL2ListAvailable() && ctrl.order._form.l2 !== null)) &&
        (!isL3ListAvailable() || (isL3ListAvailable() && ctrl.order._form.l3 !== null));
    }

    function toggleChangeNewAddress() {
      ctrl.order._form.isChangeNewAddress = !ctrl.order._form.isChangeNewAddress;

      if (ctrl.order._form.isChangeNewAddress) {
        scrollTo('change-new-address-holder');
      }
    }

    function onAddressFinderSearchModeChanged() {
      scrollTo('address-finder-holder');
    }

    function onAddressFinderCallback(address) {
      ctrl.order._form.isAddressFinder = false;

      if (!address) {
        return;
      }

      ctrl.order._form.address1 = address.address1;
      ctrl.order._form.address2 = address.address2;
      ctrl.order._form.city = address.city;
      ctrl.order._form.country = address.country;
      ctrl.order._form.postcode = address.postcode;
      ctrl.order._form.latitude = address.latitude;
      ctrl.order._form.longitude = address.longitude;
    }

    function onCancel() {
      $mdDialog.cancel();
    }

    function scrollTo(className) {
      return _.defer(() =>
          angular.element(
            $('.order-edit-pickup-details md-dialog > md-dialog-content')
          ).duScrollToElementAnimated(
            angular.element(document.getElementsByClassName(className))
          )
      );
    }

    function isCountryWithProvince() {
      return !!_.find(COUNTRY_WITH_PROVINCES,
        el => el === _.toLower(nvDomain.getDomain().currentCountry));
    }

    function isProvinceListAvailable() {
      if (isCountryWithProvince()) {
        return _.size(ctrl.provinces) > 0;
      }
      return false;
    }

    function isL2ListAvailable() {
      if (ctrl.l2Names && _.get(ctrl, 'order._form.province')) {
        return _.size(ctrl.l2Names[ctrl.order._form.province]) > 0;
      }
      return false;
    }

    function isL3ListAvailable() {
      if (ctrl.l3Names && _.get(ctrl, 'order._form.province') && _.get(ctrl, 'order._form.l2')) {
        return _.size(ctrl.l3Names[ctrl.order._form.l2]) > 0;
      }
      return false;
    }

    function onProvinceChange() {
      $timeout(() => {
        ctrl.order._form.l2 = null;
        ctrl.l2Options = ctrl.l2Names[ctrl.order._form.province];
      }, 0);
    }

    function onL2Change() {
      $timeout(() => {
        ctrl.order._form.l3 = null;
        ctrl.l3Options = ctrl.l3Names[ctrl.order._form.l2];
      }, 0);
    }

    function isReturnOrder() {
      return ctrl.order.type === Order.TYPE.RETURN;
    }
  }
}());
