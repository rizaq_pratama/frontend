(function controller() {
  angular
    .module('nvOperatorApp.controllers')
    .controller('OrderUpdateStatusDialogController', OrderUpdateStatusDialogController);

  OrderUpdateStatusDialogController.$inject = [
    '$mdDialog', 'statusData', 'Order', '$q', 'nvToast',
    'nvTranslate', 'Transaction', 'nvGa',
  ];

  function OrderUpdateStatusDialogController(
    $mdDialog, statusData, Order, $q, nvToast,
    nvTranslate, Transaction, nvGa
  ) {
    // variables
    const ctrl = this;
    ctrl.originalStatusData = statusData;
    ctrl.formData = null;
    ctrl.formSubmitState = 'idle';
    ctrl.statusSelectionOptions = [];
    ctrl.granularStatusSelectionOptions = [];

    // functions
    ctrl.onCancel = onCancel;
    ctrl.onSave = onSave;

    // start
    initialize();

    // functions details
    function initialize() {
      ctrl.statusSelectionOptions = _(Order.STATUS)
        .map(status => ({ displayName: status, value: status }))
        .value();
      ctrl.granularStatusSelectionOptions = _(Order.GRANULAR_STATUS)
        .map(granularStatus => ({ displayName: granularStatus, value: granularStatus }))
        .value();
      ctrl.transactionSelectionOptions = _(Transaction.STATUS)
        .map(status => ({ displayName: status, value: status }))
        .value();

      ctrl.formData = {
        selectedOrderStatus: Order.STATUS[statusData.order.status],
        selectedOrderGranularStatus: Order.GRANULAR_STATUS[statusData.order.granularStatus],
      };

      if (statusData.lastPPNT) {
        ctrl.formData.selectedPPNTStatus = Transaction.STATUS[statusData.lastPPNT.status];
      }

      if (statusData.lastDDNT) {
        ctrl.formData.selectedDDNTStatus = Transaction.STATUS[statusData.lastDDNT.status];
      }
    }

    function onCancel() {
      $mdDialog.cancel();
    }

    function onSave() {
      const promises = [];

      if (isOrderStatusChanged() || isOrderGranularStatusChanged()) {
        if (isOrderStatusChanged()) {
          nvGa.event('order-edit', 'status-update', 'order-status',
            `${Order.STATUS[statusData.order.status]} to ${ctrl.formData.selectedOrderStatus}`);
        }

        if (isOrderGranularStatusChanged()) {
          nvGa.event('order-edit', 'status-update', 'order-granular-status',
            `${Order.GRANULAR_STATUS[statusData.order.granularStatus]} to ${ctrl.formData.selectedOrderGranularStatus}`);
        }
        const orderStatusPayload = {
          statusValue: ctrl.formData.selectedOrderStatus,
          granularStatusValue: ctrl.formData.selectedOrderGranularStatus,
        };

        promises.push(Order.update(statusData.order.id, orderStatusPayload));
      }

      if (statusData.lastPPNT &&
        Transaction.STATUS[statusData.lastPPNT.status] !== ctrl.formData.selectedPPNTStatus) {
        nvGa.event('order-edit', 'status-update', 'pp-transaction',
          `${Transaction.STATUS[statusData.lastPPNT.status]} to ${ctrl.formData.selectedPPNTStatus}`);
        const ppntStatusPayload = {
          status: ctrl.formData.selectedPPNTStatus,
        };

        promises.push(Transaction.update(
          statusData.order.id, statusData.lastPPNT.id, ppntStatusPayload
        ));
      }

      if (statusData.lastDDNT &&
        Transaction.STATUS[statusData.lastDDNT.status] !== ctrl.formData.selectedDDNTStatus) {
        nvGa.event('order-edit', 'status-update', 'dd-transaction',
        `${Transaction.STATUS[statusData.lastDDNT.status]} to ${ctrl.formData.selectedDDNTStatus}`);
        const ddntStatusPayload = {
          status: ctrl.formData.selectedDDNTStatus,
        };

        promises.push(Transaction.update(
          statusData.order.id, statusData.lastDDNT.id, ddntStatusPayload
        ));
      }

      if (_.size(promises) <= 0) {
        nvToast.warning(nvTranslate.instant('commons.no-changes'));
        return;
      }

      ctrl.formSubmitState = 'waiting';
      $q.all(promises).then(success).finally(finallyFn);

      function success() {
        nvToast.success(nvTranslate.instant('container.order.edit.status-updated'));
        $mdDialog.hide();
      }

      function finallyFn() {
        ctrl.formSubmitState = 'idle';
      }

      function isOrderStatusChanged() {
        return Order.STATUS[statusData.order.status] !== ctrl.formData.selectedOrderStatus;
      }

      function isOrderGranularStatusChanged() {
        return Order.GRANULAR_STATUS[statusData.order.granularStatus] !== ctrl.formData.selectedOrderGranularStatus;
      }
    }
  }
}());
