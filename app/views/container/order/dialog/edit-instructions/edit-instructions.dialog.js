(function controller() {
  angular
    .module('nvOperatorApp.controllers')
    .controller('OrderEditInstructionsDialogController', OrderEditInstructionsDialogController);

  OrderEditInstructionsDialogController.$inject = [
    '$mdDialog', 'orderData', 'Order', '$q', 'nvToast',
    'nvTranslate',
  ];

  function OrderEditInstructionsDialogController(
    $mdDialog, orderData, Order, $q, nvToast,
    nvTranslate
  ) {
    // variables
    const ctrl = this;
    ctrl.formData = null;
    ctrl.formSubmitState = 'idle';

    // functions
    ctrl.onCancel = onCancel;
    ctrl.onSave = onSave;

    // start
    initialize();

    // functions details
    function initialize() {
      ctrl.formData = {
        order: {
          id: orderData.id,
          instruction: orderData.instruction,
        },
        pickup: {
          id: orderData._lastPPNT.id,
          instruction: orderData._lastPPNT.instruction,
        },
        delivery: {
          id: orderData._lastDDNT.id,
          instruction: orderData._lastDDNT.instruction,
        },
      };
    }

    function onCancel() {
      $mdDialog.cancel();
    }

    function onSave() {
      ctrl.formSubmitState = 'waiting';

      const payload = {};
      const parcelJob = _.assign({}, {
        order_instruction: ctrl.formData.order.instruction,
        pickup_instruction: ctrl.formData.pickup.instruction,
        delivery_instruction: ctrl.formData.delivery.instruction,
      });

      _.assign(payload, { parcel_job: parcelJob });

      Order.updateV2(ctrl.formData.order.id, payload)
        .then(success)
        .finally(finallyFn);

      function success(resp) {
        if (Order.isUpdateOrderSuccess(resp)) {
          nvToast.success(nvTranslate.instant('container.order.edit.instructions-updated'));
          $mdDialog.hide();
        } else {
          const errorMessage = resp.message || nvTranslate.instant('container.order.edit.edit-order-failed');
          nvToast.error(errorMessage);
        }
      }

      function finallyFn() {
        ctrl.formSubmitState = 'idle';
      }
    }
  }
}());
