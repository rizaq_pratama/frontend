(function controller() {
  angular
    .module('nvOperatorApp.controllers')
    .controller('OrderEditRtsDetailsDialogController', OrderEditRtsDetailsDialogController);

  OrderEditRtsDetailsDialogController.$inject = [
    '$scope', '$mdDialog', 'ordersData', 'Order', 'nvDateTimeUtils',
    'Timewindow', 'nvToast', 'nvTranslate', 'settingsData', 'nvDialog',
    'nvDomain',
  ];

  const STD_REASON = {
    NOBODY_AT_ADDRESS: 'container.order.edit.rts-reason.nobody-at-address',
    UNABLE_TO_FIND_ADDRESS: 'container.order.edit.rts-reason.unable-to-find-address',
    ITEM_REFUSED_AT_DOORSTEP: 'container.order.edit.rts-reason.item-refused-at-doorstep',
    REFUSED_TO_PAY_COD: 'container.order.edit.rts-reason.refused-to-pay-cod',
    CUSTOMER_DELAYED_BEYOND_DELIVERY_PERIOD: 'container.order.edit.rts-reason.customer-delayed-beyond-delivery-period',
    CANCELLED_BY_SHIPPER: 'container.order.edit.rts-reason.cancelled-by-shipper',
    DANGEROUS_GOODS: 'container.order.edit.rts-reason.dangerous-goods',
    OUT_OF_COVERAGE: 'container.order.edit.rts-reason.out-of-coverage',
    OVERSIZED_PARCEL: 'container.order.edit.rts-reason.oversized-parcel',
    OTHER_REASON: 'container.order.edit.rts-reason.other-reason',
  };

  const VN_REASON = {
    NOBODY_AT_ADDRESS: 'Không có người nhận hàng tại địa chỉ',
    UNABLE_TO_FIND_ADDRESS: 'Không thể tìm thấy địa chỉ',
    ITEM_REFUSED_AT_DOORSTEP: 'Đơn hàng bị từ chối',
    REFUSED_TO_PAY_COD: 'Bị từ chối thanh toán đơn hàng',
    CUSTOMER_DELAYED_BEYOND_DELIVERY_PERIOD: 'Khách hàng trì hoãn thời gian giao hàng',
    CANCELLED_BY_SHIPPER: 'Bị hủy bởi người bán',
    DANGEROUS_GOODS: 'Mặt hàng nguy hiểm',
    OUT_OF_COVERAGE: 'Ngoài vùng bao phủ',
    OVERSIZED_PARCEL: 'Bưu kiện quá khổ',
    OTHER_REASON: 'Lý do khác',
  };

  const ADDRESS_EDIT_MODE = {
    READONLY: 0,
    EDIT: 1,
    FINDER: 2,
  };

  function OrderEditRtsDetailsDialogController(
    $scope, $mdDialog, ordersData, Order, nvDateTimeUtils,
    Timewindow, nvToast, nvTranslate, settingsData, nvDialog,
    nvDomain
  ) {
    // variables
    let bulkActionProgressPayload;
    let rtsedOrders = [];
    let unRtsedOrders = [];

    const ctrl = this;

    // todo: vn got special reason value, we hardcode it first
    // changing lang based on opv2 country is not a good solution for now
    const REASON = _.toLower(nvDomain.getDomain().current) !== 'vn' ? STD_REASON : VN_REASON;

    ctrl.orders = _.cloneDeep(ordersData);
    ctrl.ADDRESS_EDIT_MODE = ADDRESS_EDIT_MODE;
    ctrl.REASON = REASON;
    ctrl.view = {
      reason: {},
      recipient: {
        isAbleToEdit: false,
      },
      schedule: {},
      currentAddress: {
        isAbleToEdit: false,
      },
      newAddress: {},
      submitButtonName: 'container.order.edit.set-order-to-rts',
    };
    ctrl.state = {};
    ctrl.settings = _.defaults(settingsData, {
      isMultiSelect: false,
    });
    ctrl.func = {};

    // view
    ctrl.view.reasonOptions = [];
    ctrl.view.timeSlotOptions = Timewindow.toOptions([
      Timewindow.TYPE.DAYSLOT,
      Timewindow.TYPE.NIGHTSLOT,
      Timewindow.TYPE.DAYNIGHTSLOT,
      Timewindow.TYPE.TIMESLOT,
    ]);

    // state
    ctrl.state.saveButtonState = 'idle';
    ctrl.state.addressMode = ADDRESS_EDIT_MODE.READONLY;

    // function
    ctrl.func.onCancel = onCancel;
    ctrl.func.onSave = onSave;
    ctrl.func.isAbleToSubmit = isAbleToSubmit;
    ctrl.func.setAddressMode = setAddressMode;
    ctrl.func.onAddressFinderCallback = onAddressFinderCallback;
    ctrl.func.isPpntHaveDpId = isPpntHaveDpId;

    // start
    init();

    function onCancel() {
      $mdDialog.cancel();
    }

    function onSave($event) {
      if (ctrl.settings.isMultiSelect) {
        multiSelectSaving();
      } else {
        nonMultiSelectSaving();
      }

      function multiSelectSaving() {
        const ordersToProcess = _.cloneDeep(ordersData);
        bulkActionProgressPayload = {
          totalCount: ordersData.length,
          currentIndex: 0,
          errors: [],
        };

        nvDialog.showSingle($event, {
          templateUrl: 'lib/ninja-commons/services/dialogs/bulk-action-progress/bulk-action-progress.dialog.html',
          cssClass: 'bulk-action-progress',
          controller: 'BulkActionProgressDialogController',
          controllerAs: 'ctrl',
          skipHide: true,
          clickOutsideToClose: false,
          locals: {
            payload: bulkActionProgressPayload,
          },
        }).then(progressDialogClosed);

        startMultiSelectSaving(ordersToProcess);

        function progressDialogClosed() {
          if (getSuccessCount() > 0) {
            nvToast.info(
              nvTranslate.instant('container.order.edit.num-order-updated', { num: getSuccessCount() }),
              nvTranslate.instant('container.order.edit.set-selected-to-rts')
            );
          }

          $mdDialog.hide({ success: rtsedOrders, failed: unRtsedOrders });
        }

        function getSuccessCount() {
          return bulkActionProgressPayload.totalCount - bulkActionProgressPayload.errors.length;
        }
      }

      function nonMultiSelectSaving() {
        // non multi select suppose only has one order in ordersData payload
        const theOrder = ctrl.orders[0];

        ctrl.state.saveButtonState = 'waiting';
        Order.returnToSender(theOrder.id, generateSingleRequestPayload())
          .then(nonMultiSelectSuccess, nonMultiSelectError);

        function nonMultiSelectSuccess() {
          ctrl.state.saveButtonState = 'idle';

          nvToast.info(
            nvTranslate.instant('container.order.edit.num-order-rtsed', { num: ordersData.length }),
            nvTranslate.instant('container.order.edit.order-x-comma-x', {
              orders: getTrackingIds(ordersData).join(', '),
            })
          );

          $mdDialog.hide({ success: rtsedOrders, failed: [] });
        }

        function nonMultiSelectError() {
          ctrl.state.saveButtonState = 'idle';
        }

        function getTrackingIds(orders) {
          const trackingIds = [];

          _.forEach(orders, (order) => {
            trackingIds.push(order.tracking_id);
          });

          return trackingIds;
        }
      }
    }

    function startMultiSelectSaving(orders) {
      let orderToProcess;
      if (orders.length > 0) {
        bulkActionProgressPayload.currentIndex += 1;
        orderToProcess = orders.splice(0, 1)[0];

        ctrl.formSubmitState = 'waiting';
        Order.returnToSender(orderToProcess.id, generateSingleRequestPayload())
          .then(multiSelectSuccess, multiSelectFailure);
      } else {
        ctrl.formSubmitState = 'idle';
      }

      function multiSelectSuccess() {
        rtsedOrders = _.concat(rtsedOrders, orderToProcess);
        bulkActionProgressPayload.successCount += 1;

        if (orders.length > 0) {
          startMultiSelectSaving(orders); // recursive
        } else {
          ctrl.formSubmitState = 'idle';
        }
      }

      function multiSelectFailure() {
        unRtsedOrders = _.concat(unRtsedOrders, orderToProcess);
        bulkActionProgressPayload.errors.push(orderToProcess.trackingId);

        if (orders.length > 0) {
          startMultiSelectSaving(orders); // recursive
        } else {
          ctrl.formSubmitState = 'idle';
        }
      }
    }

    function isAbleToSubmit() {
      return $scope.modelForm && $scope.modelForm.$valid &&
        ctrl.state.addressMode !== ADDRESS_EDIT_MODE.FINDER &&
        ctrl.view.schedule.timeslot !== null;
    }

    function generateSingleRequestPayload() {
      const row = {};
      const reason = REASON[ctrl.view.reason.value] !== REASON.OTHER_REASON
        ? nvTranslate.instant(REASON[ctrl.view.reason.value])
        : `${nvTranslate.instant(REASON.OTHER_REASON)}: ${ctrl.view.reason.description}`;

      row.reason = `Return to sender: ${reason}`;
      row.timewindow_id = ctrl.view.schedule.timeslot;
      row.date = nvDateTimeUtils.displayDate(ctrl.view.schedule.date);

      if (ctrl.view.recipient.isAbleToEdit && ctrl.view.currentAddress.isAbleToEdit) {
        row.name = ctrl.view.recipient.name;
        row.contact = ctrl.view.recipient.contact;
        row.email = ctrl.view.recipient.email;

        if (ctrl.state.addressMode === ADDRESS_EDIT_MODE.READONLY) {
          row.address1 = ctrl.view.currentAddress.address1;
          row.address2 = ctrl.view.currentAddress.address2;
          row.city = ctrl.view.currentAddress.city;
          row.country = ctrl.view.currentAddress.country;
          row.postcode = ctrl.view.currentAddress.postcode;
        } else {
          row.address1 = ctrl.view.newAddress.address1;
          row.address2 = ctrl.view.newAddress.address2;
          row.city = ctrl.view.newAddress.city;
          row.country = ctrl.view.newAddress.country;
          row.postcode = ctrl.view.newAddress.postcode;
        }
      }

      return row;
    }

    function setAddressMode(mode) {
      const currentMode = ctrl.state.addressMode;

      if ((mode === ADDRESS_EDIT_MODE.EDIT) &&
        (currentMode === ADDRESS_EDIT_MODE.READONLY)) {
        ctrl.view.newAddress.country = '';
        ctrl.view.newAddress.city = '';
        ctrl.view.newAddress.address1 = '';
        ctrl.view.newAddress.address2 = '';
        ctrl.view.newAddress.postcode = '';

        scrollTo('change-new-address-holder');
      }

      ctrl.state.addressMode = mode;
    }

    function init() {
      if (ctrl.orders.length === 1) {
        const ppnt = _.get(ctrl.orders[0], '_lastPPNT');

        // only allow to edit contact and address info if only 1 order and have ppnt info and ppnt doesn't have dpId
        if (!ctrl.settings.isMultiSelect && ppnt && !isPpntHaveDpId()) {
          ctrl.view.recipient.isAbleToEdit = true;
          ctrl.view.recipient.name = ppnt.name;
          ctrl.view.recipient.contact = ppnt.contact;
          ctrl.view.recipient.email = ppnt.email;
          ctrl.view.recipient.instructions = (ppnt.instruction && ppnt.instruction !== '') ?
            ppnt.instruction : '-';
          ctrl.view.recipient.internalNotes = '-';

          ctrl.view.currentAddress.isAbleToEdit = true;
          ctrl.view.currentAddress.country = ppnt.country;
          ctrl.view.currentAddress.city = ppnt.city;
          ctrl.view.currentAddress.address1 = ppnt.address1;
          ctrl.view.currentAddress.address2 = ppnt.address2;
          ctrl.view.currentAddress.postcode = ppnt.postcode;
        }
      } else {
        ctrl.view.submitButtonName = 'container.order.edit.set-orders-to-rts';
      }

      _.forEach(REASON, (value, key) => {
        ctrl.view.reasonOptions.push({
          displayName: value,
          value: key,
        });
      });

      ctrl.view.reason.value = ctrl.view.reasonOptions[0].value;
      ctrl.view.reason.description = '-';
      ctrl.view.schedule.date = moment().toDate();
      ctrl.view.schedule.timeslot = null;
    }

    function onAddressFinderCallback(address) {
      if (address) {
        ctrl.view.newAddress.country = address.country;
        ctrl.view.newAddress.city = address.city;
        ctrl.view.newAddress.address1 = address.address1;
        ctrl.view.newAddress.address2 = address.address2;
        ctrl.view.newAddress.postcode = address.postcode;
      }
      setAddressMode(ADDRESS_EDIT_MODE.EDIT);
    }

    function scrollTo(className) {
      return _.defer(() =>
          angular.element(
            $('.order-edit-rts-details md-dialog-content')
          ).duScrollToElementAnimated(
            angular.element(document.getElementsByClassName(className))
          )
      );
    }

    function isPpntHaveDpId() {
      if (!ctrl.settings.isMultiSelect) {
        const ppnt = ctrl.orders[0]._lastPPNT;
        return !!_.get(ppnt, 'distributionPointId', false);
      }
      return false;
    }
  }
}());
