(function controller() {
  angular
    .module('nvOperatorApp.controllers')
    .controller('OrderEditDeliveryDetailsDialogController', OrderEditDeliveryDetailsDialogController);

  OrderEditDeliveryDetailsDialogController.$inject = [
    '$mdDialog', 'orderData', 'nvTranslate', 'Order', 'nvToast',
    'nvDateTimeUtils', 'Timewindow', '$scope', 'nvTimezone',
    'nvDomain', 'Address', '$timeout',
  ];

  function OrderEditDeliveryDetailsDialogController(
    $mdDialog, orderData, nvTranslate, Order, nvToast,
    nvDateTimeUtils, Timewindow, $scope, nvTimezone,
    nvDomain, Address, $timeout
  ) {
    /* eslint no-underscore-dangle: ["error", { "allow": ["_form", "_lastDDNT"] }]*/
    // variables
    const ctrl = this;
    const COUNTRY_WITH_PROVINCES = ['id', 'ph', 'th'];
    ctrl.dialogTitle = 'container.order.edit.edit-delivery-details';
    ctrl.order = _.cloneDeep(orderData);
    ctrl.deliveryTimeslotOptions = [];
    ctrl.formSubmitState = 'idle';

    // functions
    ctrl.onCancel = onCancel;
    ctrl.onSave = onSave;
    ctrl.isAbleToSubmit = isAbleToSubmit;
    ctrl.isCompletedOrder = isCompletedOrder;
    ctrl.toggleChangeNewAddress = toggleChangeNewAddress;
    ctrl.onAddressFinderSearchModeChanged = onAddressFinderSearchModeChanged;
    ctrl.onAddressFinderCallback = onAddressFinderCallback;
    ctrl.isProvinceListAvailable = isProvinceListAvailable;
    ctrl.isL2ListAvailable = isL2ListAvailable;
    ctrl.isL3ListAvailable = isL3ListAvailable;
    ctrl.onProvinceChange = onProvinceChange;
    ctrl.onL2Change = onL2Change;
    ctrl.isDeliveryTimingDisabled = isDeliveryTimingDisabled;
    ctrl.hasDeliveryRoute = hasDeliveryRoute;

    // start
    initialize();

    // functions details
    function initialize() {
      if (isCountryWithProvince()) {
        Address
          .dpZones(nvDomain.getDomain().currentCountry)
          .then((datas) => {
            const CLEAN_DATAS = _.map(datas, el => ({
              l1_name: _.trim(el.l1_name),
              l2_name: _.trim(el.l2_name),
              l3_name: _.trim(el.l3_name),
            }));
            ctrl.provinces = _(CLEAN_DATAS).map(el => _.trim(el.l1_name))
              .uniq()
              .map(el => ({
                displayName: el,
                value: el,
              }))
              .sortBy('displayName')
              .value();

            ctrl.l2Names = {};
            _.forEach(CLEAN_DATAS, (el) => {
              const l1Name = el.l1_name;
              if (el.l2_name) {
                if (_.size(ctrl.l2Names[l1Name]) > 0) {
                  ctrl.l2Names[l1Name].push(el.l2_name);
                } else {
                  ctrl.l2Names[l1Name] = [el.l2_name];
                }
              }
            });
            _.forEach(ctrl.l2Names, (val, key) => {
              ctrl.l2Names[key] = _(val).uniq()
                .map(el => ({
                  displayName: el,
                  value: el,
                }))
                .sortBy('displayName')
                .value();
            });

            ctrl.l3Names = {};
            _.forEach(CLEAN_DATAS, (el) => {
              const l2Name = el.l2_name;
              if (el.l3_name) {
                if (_.size(ctrl.l3Names[l2Name]) > 0) {
                  ctrl.l3Names[l2Name].push(el.l3_name);
                } else {
                  ctrl.l3Names[l2Name] = [el.l3_name];
                }
              }
            });
            _.forEach(ctrl.l3Names, (val, key) => {
              ctrl.l3Names[key] = _(val).uniq()
                .map(el => ({
                  displayName: el,
                  value: el,
                }))
                .sortBy('displayName')
                .value();
            });
          });
      }

      if (ctrl.order.rts) {
        ctrl.dialogTitle = 'container.order.edit.edit-return-details';
      }

      const deliveryParam = getDeliveryParameter(ctrl.order);

      ctrl.order._lastDDNT.instruction = ctrl.order._lastDDNT.instruction || '-';

      ctrl.order._form = {
        name: ctrl.order.toName,
        contact: ctrl.order.toContact,
        email: ctrl.order.toEmail,
        isChangeNewAddress: false,
        isAddressFinder: false,
        address1: '',
        address2: '',
        city: '',
        country: '',
        postcode: '',
        date: deliveryParam.date,
        timewindowId: deliveryParam.timewindowId,
        routeComments: ctrl.order._lastDDNT && ctrl.order._lastDDNT.routeComments,
        maxDeliveryDays: getMaxDeliveryDays(ctrl.order),
        shipperRequested: false,
      };

      ctrl.deliveryTimeslotOptions = Timewindow.toOptions([
        Timewindow.TYPE.DAYSLOT,
        Timewindow.TYPE.NIGHTSLOT,
        Timewindow.TYPE.DAYNIGHTSLOT,
        Timewindow.TYPE.TIMESLOT,
      ]);

      function getMaxDeliveryDays(order) {
        return order.deliveryType && order.deliveryType.indexOf('THREE_DAYS') >= 0 ? 3 : 1;
      }

      function getDeliveryParameter(order) {
        const lastDDNT = order._lastDDNT;
        if (lastDDNT) {
          const startTime = nvDateTimeUtils.toMoment(
            lastDDNT.startTime, nvTimezone.getOperatorTimezone()
          );
          return {
            timewindowId: Timewindow.getTimeWindowId(lastDDNT),
            date: nvDateTimeUtils.toDate(startTime),
          };
        }
        return {
          timewindowId: null,
          date: new Date(),
        };
      }
    }

    function onSave() {
      const parcelJobObject = constructParcelJobObject();
      const payload = {
        parcel_job: parcelJobObject,
      };
      if (!isCompletedOrder()) {
        const toObject = constructToObject();
        _.assign(payload, { to: toObject });
      }

      ctrl.formSubmitState = 'waiting';
      Order.updateV2(ctrl.order.id, payload)
        .then(success)
        .finally(() => (ctrl.formSubmitState = 'idle'));

      function success(resp) {
        if (Order.isUpdateOrderSuccess(resp)) {
          nvToast.success(nvTranslate.instant('container.order.edit.delivery-details-updated'));
          $mdDialog.hide(0);
        } else {
          const errorMessage = resp.message || nvTranslate.instant('container.order.edit.edit-order-failed');
          nvToast.error(errorMessage);
        }
      }

      function constructToObject() {
        const to = {
          name: ctrl.order._form.name,
          email: ctrl.order._form.email,
          phone_number: ctrl.order._form.contact,
        };

        if (ctrl.order._form.isChangeNewAddress) {
          const address = {
            address1: ctrl.order._form.address1,
            address2: ctrl.order._form.address2,
            postcode: ctrl.order._form.postcode,
            city: ctrl.order._form.city,
            country: ctrl.order._form.country,
            latitude: ctrl.order._form.latitude,
            longitude: ctrl.order._form.longitude,
          };

          if (isProvinceListAvailable()) {
            const address2 = _([address.address2, ctrl.order._form.province, ctrl.order._form.l2, ctrl.order._form.l3]).compact().join(', ');
            _.assign(address, { address2: address2, district: ctrl.order._form.province });
          }
          _.assign(to, { address: address });
        }

        return to;
      }

      function constructParcelJobObject() {
        let parcelJob;
        if (isCompletedOrder()) {
          parcelJob = {
            delivery_timewindow: +ctrl.order._form.timewindowId,
            allow_delivery_timeslot_override: true,
          };
        } else if (!isDeliveryTimingDisabled()) {
          parcelJob = {
            delivery_date: nvDateTimeUtils.displayDate(ctrl.order._form.date,
              nvTimezone.getOperatorTimezone()), // local date
            delivery_timewindow: +ctrl.order._form.timewindowId,
            shipper_requested: ctrl.order._form.shipperRequested,
          };
        }
        return parcelJob;
      }
    }

    function isAbleToSubmit() {
      return $scope.modelForm && $scope.modelForm.$valid &&
        !ctrl.order._form.isAddressFinder &&
        ctrl.order._form.timewindowId !== null &&
        moment(ctrl.order._form.date).isValid() &&
        (!isL2ListAvailable() || (isL2ListAvailable() && ctrl.order._form.l2 !== null)) &&
        (!isL3ListAvailable() || (isL3ListAvailable() && ctrl.order._form.l3 !== null));
    }

    function toggleChangeNewAddress() {
      ctrl.order._form.isChangeNewAddress = !ctrl.order._form.isChangeNewAddress;

      if (ctrl.order._form.isChangeNewAddress) {
        scrollTo('change-new-address-holder');
      }
    }

    function onAddressFinderSearchModeChanged() {
      scrollTo('address-finder-holder');
    }

    function onAddressFinderCallback(address) {
      ctrl.order._form.isAddressFinder = false;

      if (!address) {
        return;
      }

      ctrl.order._form.address1 = address.address1;
      ctrl.order._form.address2 = address.address2;
      ctrl.order._form.city = address.city;
      ctrl.order._form.country = address.country;
      ctrl.order._form.postcode = address.postcode;
      ctrl.order._form.latitude = address.latitude;
      ctrl.order._form.longitude = address.longitude;
    }

    function onCancel() {
      $mdDialog.cancel();
    }

    function scrollTo(className) {
      return _.defer(() =>
          angular.element(
            $('.order-edit-delivery-details md-dialog > md-dialog-content')
          ).duScrollToElementAnimated(
            angular.element(document.getElementsByClassName(className))
          )
      );
    }

    function isCountryWithProvince() {
      return !!_.find(COUNTRY_WITH_PROVINCES,
        el => el === _.toLower(nvDomain.getDomain().currentCountry));
    }

    function isProvinceListAvailable() {
      if (isCountryWithProvince()) {
        return _.size(ctrl.provinces) > 0;
      }
      return false;
    }

    function isL2ListAvailable() {
      if (ctrl.l2Names && _.get(ctrl, 'order._form.province')) {
        return _.size(ctrl.l2Names[ctrl.order._form.province]) > 0;
      }
      return false;
    }

    function isL3ListAvailable() {
      if (ctrl.l3Names && _.get(ctrl, 'order._form.province') && _.get(ctrl, 'order._form.l2')) {
        return _.size(ctrl.l3Names[ctrl.order._form.l2]) > 0;
      }
      return false;
    }

    function onProvinceChange() {
      $timeout(() => {
        ctrl.order._form.l2 = null;
        ctrl.l2Options = ctrl.l2Names[ctrl.order._form.province];
      }, 0);
    }

    function onL2Change() {
      $timeout(() => {
        ctrl.order._form.l3 = null;
        ctrl.l3Options = ctrl.l3Names[ctrl.order._form.l2];
      }, 0);
    }

    function isDeliveryTimingDisabled() {
      const lastDDNT = ctrl.order._lastDDNT;
      if (lastDDNT) {
        return lastDDNT.distributionPointId !== null;
      }
      return false;
    }

    function hasDeliveryRoute() {
      return !!(ctrl.order._lastDDNT && ctrl.order._lastDDNT.routeId);
    }

    function isCompletedOrder() {
      return _.toLower(ctrl.order.granularStatus) === _.toLower(Order.GRANULAR_STATUS.COMPLETED);
    }
  }
}());
