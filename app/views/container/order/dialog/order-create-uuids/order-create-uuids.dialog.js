(function controller() {
  angular
    .module('nvOperatorApp.controllers')
    .controller('OrderCreateUUIDsDialogController', OrderCreateUUIDsDialogController);

  OrderCreateUUIDsDialogController.$inject = [
    '$mdDialog', 'nvTable', 'batchStatus', 'mode', '$q',
    'Order',
  ];

  function OrderCreateUUIDsDialogController(
    $mdDialog, nvTable, batchStatus, mode, $q,
    Order
  ) {
    // variables
    const ctrl = this;
    ctrl.tableParam = nvTable.createTable();
    ctrl.tableParam.addColumn('uuid', { displayName: 'container.order.create.async-uuid' });

    // functions
    ctrl.onCancel = onCancel;

    // start
    init();

    // functions details
    function init() {
      const orders = _.map(batchStatus[mode], value => ({ uuid: value }));

      ctrl.title = 'container.order.create.processing-orders';
      if (mode === 'failed_orders_async_uuids') {
        ctrl.title = 'container.order.create.failed-orders';

        ctrl.tableParam.addColumn('failure_reason', { displayName: 'container.order.create.failure-reason' });

        const requests = [];
        _.forEach(orders, (order) => {
          requests.push(Order.v4GetErrorResponseFromOrder(batchStatus.batch_id, order.uuid));
        });

        $q.all(requests).then(success, $q.reject);
      } else {
        ctrl.tableParam.setData(orders);
      }

      function success(response) {
        _.forEach(response, (errorOrder, index) => {
          orders[index].failure_reason = getFailurereason(errorOrder);
        });

        ctrl.tableParam.setData(orders);
      }

      function getFailurereason(errorOrder) {
        if (errorOrder.error && errorOrder.error.message) {
          const errorMessages = [errorOrder.error.message];

          if (errorOrder.error.details) {
            _.forEach(errorOrder.error.details, (detail) => {
              errorMessages.push(_.compact([detail.field, detail.message]).join(' '));
            });
          }

          return errorMessages.join('; ');
        } else if (errorOrder.message) {
          return errorOrder.message;
        }

        return JSON.stringify(errorOrder);
      }
    }

    function onCancel() {
      $mdDialog.cancel();
    }
  }
}());
