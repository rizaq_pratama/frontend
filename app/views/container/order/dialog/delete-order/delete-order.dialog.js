(function controller() {
  angular
    .module('nvOperatorApp.controllers')
    .controller('OrderEditDeleteOrderDialogController', OrderEditDeleteOrderDialogController);

  OrderEditDeleteOrderDialogController.$inject = [
    '$mdDialog', 'orderData', 'Order', 'nvToast', 'nvTranslate',
  ];

  function OrderEditDeleteOrderDialogController(
    $mdDialog, orderData, Order, nvToast, nvTranslate
  ) {
    // variables
    const PASSWORD = '1234567890';
    const ctrl = this;

    ctrl.formPassword = null;
    ctrl.formSubmitState = 'idle';

    // functions
    ctrl.onCancel = onCancel;
    ctrl.onSave = onSave;

    // functions details
    function onSave() {
      if (PASSWORD === ctrl.formPassword) {
        ctrl.formSubmitState = 'waiting';
        Order.deleteOrder(orderData.id).then(success, () => {
          ctrl.formSubmitState = 'idle';
        });

        function success() {
          ctrl.formSubmitState = 'idle';
          nvToast.success(nvTranslate.instant('container.order.edit.order-deleted'));
          $mdDialog.hide();
        }
      } else {
        nvToast.error(nvTranslate.instant('container.order.edit.invalid-password'));
      }
    }

    function onCancel() {
      $mdDialog.cancel();
    }
  }
}());
