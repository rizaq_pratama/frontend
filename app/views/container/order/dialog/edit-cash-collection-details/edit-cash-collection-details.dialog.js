(function controller() {
  angular
    .module('nvOperatorApp.controllers')
    .controller('OrderEditCashCollectionDetailsDialogController', OrderEditCashCollectionDetailsDialogController);

  OrderEditCashCollectionDetailsDialogController.$inject = [
    '$scope', '$mdDialog', 'orderData', 'nvCurrency', 'Order', 'nvToast', 'nvTranslate', '$timeout', '$rootScope'];

  function OrderEditCashCollectionDetailsDialogController(
    $scope, $mdDialog, orderData, nvCurrency, Order, nvToast, nvTranslate, $timeout, $rootScope) {
    const ctrl = this;

    const order = _.cloneDeep(orderData);
    const orderId = order.id;
    const orderCod = order.cod;
    const orderCp = order.cash_pickup;
    let oriType = null;

    ctrl.isChanged = false;
    // flag for create / update cod
    ctrl.codAvailable = ((orderCod !== null) || (orderCp !== null));

    ctrl.view = {};
    ctrl.view.pickup = {};
    ctrl.view.delivery = {};
    ctrl.state = {};
    ctrl.func = {};

    // view
    ctrl.view.currency = nvCurrency.getCode($rootScope.countryId);
    ctrl.view.decimal = { decimal: 2, minValue: 0.00, maxValue: 99999999.99 };

    // state
    ctrl.state.saveButtonState = 'idle';

    // function
    ctrl.func.onCancel = onCancel;
    ctrl.func.onSave = onSave;
    ctrl.func.isAbleToSubmit = isAbleToSubmit;

    init();

    function onCancel() {
      $mdDialog.cancel();
    }

    function onSave() {
      ctrl.state.saveButtonState = 'waiting';
      const payload = getUpdatedCod();
      const isPickup = ctrl.view.pickup.boolean === '01';
      const isDelivery = ctrl.view.delivery.boolean === '01';
      let toUpdate = null;

      if (oriType === null) {
        if (isDelivery === true) {
          Order.createOrUpdateCod(orderId, payload)
            .then(success)
            .finally(() => (ctrl.state.saveButtonState = 'idle'));
        } else if (isPickup === true) {
          Order.createOrUpdateCop(orderId, payload)
            .then(success)
            .finally(() => (ctrl.state.saveButtonState = 'idle'));
        } else {
          ctrl.state.saveButtonState = 'idle';
        }
      } else if (oriType === 'DD') {
        if (isDelivery === true) {
          Order.createOrUpdateCod(orderId, payload)
            .then(success)
            .finally(() => (ctrl.state.saveButtonState = 'idle'));
        } else if (isPickup === true) {
          toUpdate = 'PP';
          Order.deleteCod(orderId)
            .then(deleteSuccess);
        } else {
          Order.deleteCod(orderId)
            .then(success)
            .finally(() => (ctrl.state.saveButtonState = 'idle'));
        }
      } else if (oriType === 'PP') {
        if (isPickup === true) {
          Order.createOrUpdateCop(orderId, payload)
            .then(success)
            .finally(() => (ctrl.state.saveButtonState = 'idle'));
        } else if (isDelivery === true) {
          toUpdate = 'DD';
          Order.deleteCop(orderId)
            .then(deleteSuccess);
        } else {
          Order.deleteCop(orderId)
            .then(success)
            .finally(() => (ctrl.state.saveButtonState = 'idle'));
        }
      }

      function deleteSuccess() {
        $timeout(() => {
          if (toUpdate === 'PP') {
            Order.createOrUpdateCop(orderId, payload)
              .then(success)
              .finally(() => (ctrl.state.saveButtonState = 'idle'));
          }
          if (toUpdate === 'DD') {
            Order.createOrUpdateCod(orderId, payload)
              .then(success)
              .finally(() => (ctrl.state.saveButtonState = 'idle'));
          }
        }, 2000);
      }

      function success(resp) {
        if (resp) {
          $mdDialog.hide(0);
        } else {
          nvToast.error(nvTranslate.instant('container.order.edit.edit-order-failed'));
        }
      }
    }

    function isAbleToSubmit() {
      if ((ctrl.codAvailable === false) && (getUpdatedCod() === null)) {
        return false;
      }
      return ctrl.isChanged;
    }

    function init() {
      const cod = orderCod;
      const cp = orderCp;

      if (xor(cod && cp)) {
        // if both empty or both filled
        ctrl.view.current = null;
        zeroAllCod();
      } else if (cod && cod.collectionAt === 'DD') {
        oriType = 'DD';
        ctrl.view.current = 'DD';
        ctrl.view.pickup.boolean = '10';
        ctrl.view.delivery.boolean = '01';
        ctrl.view.delivery.amount = ctrl.tempGoodsAmount = cod.goodsAmount;
      } else if ((cod && cod.collectionAt === 'PP') || cp) {
        oriType = 'PP';
        ctrl.view.current = 'PP';
        ctrl.view.pickup.boolean = '01';
        ctrl.view.delivery.boolean = '10';
        if (cod) {
          ctrl.view.pickup.amount = ctrl.tempGoodsAmount = cod.goodsAmount;
        } else {
          ctrl.view.pickup.amount = ctrl.tempGoodsAmount = cp.goodsAmount;
        }
      } else {
        ctrl.view.current = null;
        zeroAllCod();
      }

      $scope.$watch('ctrl.view.pickup.boolean', swap);
      $scope.$watch('ctrl.view.delivery.boolean', swap);
      $scope.$watch('ctrl.view.delivery.amount', (val, oldVal) => {
        if (val !== oldVal) {
          ctrl.isChanged = true;
        }
      });
      $scope.$watch('ctrl.view.pickup.amount', (val, oldVal) => {
        if (val !== oldVal) {
          ctrl.isChanged = true;
        }
      });

      function xor(a, b) {
        return (a && !b) || (!a && b);
      }
    }

    function zeroAllCod() {
      ctrl.view.pickup.boolean = '10';
      ctrl.view.delivery.boolean = '10';

      ctrl.view.delivery.amount = 0.00;
      ctrl.view.pickup.amount = 0.00;
    }

    function swap(val, oldVal) {
      if (val !== oldVal) {
        ctrl.isChanged = true;
      }

      const isPickup = ctrl.view.pickup.boolean === '01';
      const isDelivery = ctrl.view.delivery.boolean === '01';
      const current = ctrl.view.current;

      if (isPickup && isDelivery) {
        if (current === 'PP') {
          assignCod('delivery', 'pickup');
          ctrl.view.current = 'DD';
          ctrl.view.pickup.boolean = '10';
        } else {
          assignCod('pickup', 'delivery');
          ctrl.view.current = 'PP';
          ctrl.view.delivery.boolean = '10';
        }
      } else if (isPickup && !isDelivery) {
        assignCod('pickup', 'delivery');
        ctrl.view.current = 'PP';
      } else if (!isPickup && isDelivery) {
        assignCod('delivery', 'pickup');
        ctrl.view.current = 'DD';
      } else {
        // both false
        zeroAllCod();
      }

      function assignCod(activeKey, deactiveKey) {
        // preserve data in second swap call
        if (ctrl.view[deactiveKey].amount && (ctrl.view[deactiveKey].amount !== 0)) {
          ctrl.tempGoodsAmount = ctrl.view[deactiveKey].amount;
        }

        ctrl.view[activeKey].amount = ctrl.tempGoodsAmount;
        ctrl.view[deactiveKey].amount = 0.00;
      }
    }

    function getUpdatedCod() {
      const payload = {};
      const isPickup = ctrl.view.pickup.boolean === '01';
      const isDelivery = ctrl.view.delivery.boolean === '01';

      if (isPickup === true) {
        _.assign(payload, { amount: ctrl.view.pickup.amount });
      } else if (isDelivery === true) {
        _.assign(payload, { amount: ctrl.view.delivery.amount });
      } else {
        _.assign(payload, { amount: null });
      }

      return payload;
    }
  }
}());
