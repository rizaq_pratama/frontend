(function controller() {
  angular
    .module('nvOperatorApp.controllers')
    .controller('OrderEditOrderStampDialogController', OrderEditOrderStampDialogController);

  OrderEditOrderStampDialogController.$inject = ['$mdDialog', 'Order', 'orderData', 'nvToast', 'nvTranslate'];

  const API_STATE = {
    IDLE: 'idle',
    WAITING: 'waiting',
  };

  function OrderEditOrderStampDialogController($mdDialog, Order, orderData, nvToast, nvTranslate) {
    const ctrl = this;
    const order = _.cloneDeep(orderData);

    ctrl.orderId = order.id;
    ctrl.initialState = !!order.stampId;
    ctrl.stampId = _.get(order, 'stampId', '');
    ctrl.apiState = API_STATE.IDLE;

    ctrl.onCancel = onCancel;
    ctrl.onApplyStamp = onApplyStamp;
    ctrl.onRemoveStamp = onRemoveStamp;
    ctrl.onUpdateStamp = onUpdateStamp;

    function onApplyStamp(stampId) {
      onUpdateStamp(stampId);
    }

    function onRemoveStamp() {
      onUpdateStamp(null);
    }

    function onUpdateStamp(stampId) {
      ctrl.apiState = API_STATE.WAITING;
      Order
        .updateV2(ctrl.orderId, { parcel_job: { stamp: { stamp_id: stampId } } })
        .then(onSuccess, onError)
        .finally(() => (ctrl.apiState = API_STATE.IDLE));

      function onSuccess(response) {
        if (Order.isUpdateOrderSuccess(response)) {
          if (stampId) {
            nvToast.success(nvTranslate.instant('container.order.edit.stamp-updated', { stampId: stampId }));
          } else {
            nvToast.success(nvTranslate.instant('container.order.edit.stamp-removed'));
          }
          $mdDialog.hide(0);
        } else {
          onError(_.get(response, 'message', 'unknown error'));
        }
      }

      function onError(message) {
        if (_.isString(message)) {
          nvToast.warning(message);
        }
      }
    }

    function onCancel() {
      $mdDialog.hide(-1);
    }
  }
}());

