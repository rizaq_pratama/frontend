(function() {
    'use strict';

    angular
        .module('nvOperatorApp.controllers')
        .controller('OrderController', angular.noop);
})();
