(function controller() {
  angular
    .module('nvOperatorApp.controllers')
    .controller('OrderEditController', OrderEditController);

  OrderEditController.$inject = [
    '$q', '$stateParams', 'Order', 'nvTable', '$state',
    'nvDialog', 'Transaction', 'nvDateTimeUtils', 'nvURL', '$rootScope',
    '$window', 'nvCurrency', '$timeout', '$scope', 'appSidenav',
    'PDF', 'nvRequestUtils', 'Printer', 'nvToast', 'nvTranslate',
    'Ticketing', 'Reservation', 'Events', 'Hub', 'OrderEventUtils',
    'nvPanel', 'Shippers', 'nvEditModelDialog', 'nvEnv',
  ];

  function OrderEditController(
    $q, $stateParams, Order, nvTable, $state,
    nvDialog, Transaction, nvDateTimeUtils, nvURL, $rootScope,
    $window, nvCurrency, $timeout, $scope, appSidenav,
    PDF, nvRequestUtils, Printer, nvToast, nvTranslate,
    Ticketing, Reservation, Events, Hub, OrderEventUtils,
    nvPanel, Shippers, nvEditModelDialog, nvEnv
  ) {
    // ////////////////////////////////////////////////
    // variables
    // ////////////////////////////////////////////////
    const orderId = $stateParams.orderId;
    $scope.Transaction = Transaction;
    $scope.Order = Order;

    const EVENTS_FILTER_MODES = {
      ALL: {
        id: 'container.order.edit.all-events',
        isRaisedButtonStyle: false,
      },
      SCANS_ONLY: {
        id: 'container.order.edit.scans-only',
        isRaisedButtonStyle: true,
      },
      CHANGES_ONLY: {
        id: 'container.order.edit.manual-updates-only',
        isRaisedButtonStyle: true,
      },
    };

    const PAGE_THEME_SETTINGS = [
      {
        class: 'grey',
        conditionFn: isStaging,
      },
      {
        class: 'red',
        conditionFn: isCancelled,
      },
      {
        class: 'blue',
        conditionFn: intermediaryStatuses,
      },
      {
        class: 'green',
        conditionFn: isCompleted,
      },
    ];

    let contentHolderDiv = null;

    let events = null;
    let orderSummaryFirstRowHeight = 0;
    let hubs = [];
    let hubOptions = [];

    const ctrl = this;
    ctrl.order = null;
    ctrl.recoveryTickets = null;
    ctrl.latestEventData = null;
    ctrl.latestEventDataWithRoute = null;

    ctrl.isOrderSummaryShowAllDetails = true;
    ctrl.isContentScrollTopZero = true;

    ctrl.EVENTS_FILTER_MODES = EVENTS_FILTER_MODES;
    ctrl.selectedEventsFilterModeId = EVENTS_FILTER_MODES.ALL.id;

    ctrl.transactionsTableParam = null;
    ctrl.eventsTableParam = null;
    ctrl.cancelledByPetsMessage = null;
    ctrl.orderTags = [];

    // start
    initialize();
    $scope.$on('$destroy', () => {
      setOriginalDocumentTitle(ctrl.order);
      appSidenav.setConfigLockedOpen(true);
    });

    // ////////////////////////////////////////////////
    // functions
    // ////////////////////////////////////////////////
    ctrl.getAll = getAll;
    ctrl.openMultiParcelMenu = openMultiParcelMenu;
    ctrl.goToOrder = gotToOrder;

    // Order Settings
    ctrl.isAbleToRescheduleOrder = isAbleToRescheduleOrder;
    ctrl.isAbleToDeleteOrder = isAbleToDeleteOrder;
    ctrl.showEditOrderDetailsDialog = showEditOrderDetailsDialog;
    ctrl.showEditCashCollectionDetailsDialog = showEditCashCollectionDetailsDialog;
    ctrl.showEditInstructionsDialog = showEditInstructionsDialog;
    ctrl.showCreateRecoveryTicketDialog = showCreateRecoveryTicketDialog;
    ctrl.showEditRecoveryTicketDialog = showEditRecoveryTicketDialog;
    ctrl.addOrUpdateStampId = addOrUpdateStampId;
    ctrl.updateStatus = updateStatus;
    ctrl.showEditPirorityLevelDialog = showEditPirorityLevelDialog;
    ctrl.showDpManagementDialog = showDpManagementDialog;
    ctrl.completeOrder = completeOrder;
    ctrl.cancelOrder = cancelOrder;
    ctrl.resumeOrder = resumeOrder;
    ctrl.rescheduleOrder = rescheduleOrder;
    ctrl.deleteOrder = deleteOrder;

    // Pickup
    ctrl.showEditPickupDetailsDialog = showEditPickupDetailsDialog;
    ctrl.isAbleToEditPickupDetails = isAbleToEditPickupDetails;
    ctrl.isAbleToReverifyTransaction = isAbleToReverifyTransaction;
    ctrl.isAbleToAddToRoute = isAbleToAddToRoute;
    ctrl.isAbleToPullFromRoute = isAbleToPullFromRoute;
    ctrl.reverifyTransaction = reverifyTransaction;
    ctrl.pullFromRoute = pullFromRoute;
    ctrl.addToRoute = addToRoute;

    // Delivery / Return to Sender
    ctrl.showEditDeliveryDetailsDialog = showEditDeliveryDetailsDialog;
    ctrl.showEditRtsDetailsDialog = showEditRtsDetailsDialog;
    ctrl.isAbleToEditDeliveryDetails = isAbleToEditDeliveryDetails;
    ctrl.isAbleToEditRtsDetails = isAbleToEditRtsDetails;
    ctrl.cancelRts = cancelRts;

    // Shipment
    ctrl.navigateToShipmentManagementPage = navigateToShipmentManagementPage;

    // Ticketing
    ctrl.getEnableCreateNewTicket = getEnableCreateNewTicket;
    // View/Print
    ctrl.showPODDialog = showPODDialog;
    ctrl.printAirwayBill = printAirwayBill;
    ctrl.printShippingLabel = printShippingLabel;
    ctrl.loginShipper = loginShipper;
    ctrl.showOrderPhotosDialog = showOrderPhotosDialog;
    ctrl.isSaas = isSaas

    // extra tools
    ctrl.filterEvents = filterEvents;
    ctrl.selectedEventIsRaisedButtonStyle = selectedEventIsRaisedButtonStyle;

    ctrl.navigateToBulkOrderPage = navigateToBulkOrderPage;
    ctrl.navigateToAdminManifestPage = navigateToAdminManifestPage;
    ctrl.navigateToRouteLogsPage = navigateToRouteLogsPage;

    ctrl.getCurrencyWithPrice = getCurrencyWithPrice;
    ctrl.isPriorityOrder = isPriorityOrder;
    ctrl.isOnDemandOrder = isOnDemandOrder;
    ctrl.isLinehaulOrder = isLinehaulOrder;
    ctrl.isCodOrder = isCodOrder;
    ctrl.isCopOrder = isCopOrder;
    ctrl.printWeight = printWeight;
    ctrl.printChargeTo = printChargeTo;
    ctrl.printRecoveryTicketIds = printRecoveryTicketIds;

    ctrl.getThemeClass = getThemeClass;
    ctrl.windowSizeChanged = windowSizeChanged;
    ctrl.orderSummaryDataBlocksStyle = orderSummaryDataBlocksStyle;
    ctrl.removeFromWarehouse = removeFromWarehouse;
    ctrl.isAbleToRemoveFromStorage = isAbleToRemoveFromStorage;
    ctrl.parseShipperRefMetadata = parseShipperRefMetadata;
    ctrl.checkStrLen = checkStrLen;
    ctrl.openShipperRefMetada = openShipperRefMetada;

    // ////////////////////////////////////////////////
    // functions details
    // ////////////////////////////////////////////////
    function initialize() {
      createEventsTable();

      $timeout(() => {
        appSidenav.setConfigLockedOpen(false);
      });
    }

    function getAll() {
      return $q.all([
        Order.get(orderId), Hub.read(), Order.getTags(orderId),
      ]).then(success, $q.reject);

      function success(response) {
        const order = response[0];
        hubs = response[1];
        ctrl.orderTags = _.map(response[2], tag => _.toUpper(tag));
        hubOptions = _.sortBy(hubs, 'name').map(o => (
          {
            displayName: o.name,
            value: o.id,
          }
        ));
        OrderEventUtils.setHub(hubs);
        processOrderData(order);
        getEventsData();
        getTicketingData();
        getReservationsData();

        processOrderSummaryFirstRowHeight();
        processContentScrollEvent();
        changeDocumentTitle(ctrl.order);
      }
    }

    // Order Settings
    function isAbleToRescheduleOrder(order) {
      return Order.isStatusEnum(order, Order.STATUS.PICKUP_FAIL) ||
        Order.isStatusEnum(order, Order.STATUS.DELIVERY_FAIL);
    }

    function isAbleToDeleteOrder(order) {
      return !(Order.isGranularStatusEnum(order, Order.GRANULAR_STATUS.COMPLETED) ||
        Order.isGranularStatusEnum(order, Order.GRANULAR_STATUS.RETURNED_TO_SENDER) ||
        Order.isGranularStatusEnum(order, Order.GRANULAR_STATUS.CANCELLED));
    }

    function showEditOrderDetailsDialog($event) {
      nvDialog.showSingle($event, {
        templateUrl: 'views/container/order/dialog/edit-order-details/edit-order-details.dialog.html',
        cssClass: 'order-edit-details',
        controller: 'OrderEditDetailsDialogController',
        controllerAs: 'ctrl',
        locals: {
          orderData: ctrl.order,
        },
      }).then(onSet);

      function onSet() {
        refreshOrderData();
      }
    }

    function showEditCashCollectionDetailsDialog($event) {
      nvDialog.showSingle($event, {
        templateUrl: 'views/container/order/dialog/edit-cash-collection-details/edit-cash-collection-details.dialog.html',
        cssClass: 'order-edit-cash-collection-details',
        controller: 'OrderEditCashCollectionDetailsDialogController',
        controllerAs: 'ctrl',
        locals: {
          orderData: ctrl.order,
        },
      }).then(onSet);

      function onSet() {
        refreshOrderData();
      }
    }

    function showEditInstructionsDialog($event) {
      nvDialog.showSingle($event, {
        templateUrl: 'views/container/order/dialog/edit-instructions/edit-instructions.dialog.html',
        cssClass: 'order-edit-instructions',
        controller: 'OrderEditInstructionsDialogController',
        controllerAs: 'ctrl',
        locals: {
          orderData: ctrl.order,
        },
      }).then(onSet);

      function onSet() {
        refreshOrderData();
      }
    }

    function showCreateRecoveryTicketDialog($event) {
      const fields = Ticketing.getAddField();
      fields.trackingId.value = ctrl.order.trackingId;

      nvDialog.showSingle($event, {
        templateUrl: 'views/container/recovery-tickets/dialog/recovery-tickets-add/new-recovery-add-dialog.html',
        theme: 'nvGreen',
        cssClass: 'nv-create-ticket-dialog',
        controller: 'NewRecoveryAddDialogController',
        controllerAs: 'ctrl',
        locals: {
          fields: fields,
          resultData: null,
        },
      }).then(onCreate);

      function onCreate() {
        nvToast.success(nvTranslate.instant('container.order.edit.recovery-ticket-created'));
        getTicketingData();
      }
    }

    function showEditRecoveryTicketDialog($event, ticket) {
      const fields = Ticketing.getAddField();
      const ticketObj = ticket || _.first(ctrl.recoveryTickets);

      nvDialog.showSingle($event, {
        templateUrl: 'views/container/recovery-tickets/dialog/recovery-tickets-edit/new-recovery-dialog-edit.html',
        cssClass: 'nv-edit-ticket-dialog',
        controller: 'NewRecoveryDialogEditController',
        controllerAs: 'ctrl',
        locals: {
          fields: fields,
          ticketObj: ticketObj,
          resultData: {
            hubResult: hubs,
          },
        },
      }).then(null, onClose);

      function onClose() {
        getTicketingData();
      }
    }

    function addOrUpdateStampId($event) {
      nvDialog
        .showSingle($event, {
          templateUrl: 'views/container/order/dialog/edit-stamp/edit-stamp.dialog.html',
          cssClass: 'order-edit-stamp-edit-dialog',
          controller: 'OrderEditOrderStampDialogController',
          controllerAs: 'ctrl',
          locals: {
            orderData: ctrl.order,
          },
        })
        .then(onSet);

      function onSet(code) {
        if (code === 0) {
          refreshOrderData();
        }
      }
    }

    function updateStatus($event) {
      nvDialog.showSingle($event, {
        templateUrl: 'views/container/order/dialog/update-status/update-status.dialog.html',
        cssClass: 'order-update-status-dialog',
        controller: 'OrderUpdateStatusDialogController',
        controllerAs: 'ctrl',
        locals: {
          statusData: {
            order: {
              id: ctrl.order.id,
              status: ctrl.order.status,
              granularStatus: ctrl.order.granularStatus,
            },
            lastPPNT: ctrl.order._lastPPNT ? {
              id: ctrl.order._lastPPNT.id,
              status: ctrl.order._lastPPNT.status,
            } : null,
            lastDDNT: ctrl.order._lastDDNT ? {
              id: ctrl.order._lastDDNT.id,
              status: ctrl.order._lastDDNT.status,
            } : null,
          },
        },
      }).then(onSet);

      function onSet() {
        refreshOrderData();
      }
    }

    function showEditPirorityLevelDialog($event) {
      nvDialog.showSingle($event, {
        templateUrl: 'views/container/order/dialog/edit-priority-level/edit-priority-level.dialog.html',
        cssClass: 'order-edit-priority-level-dialog',
        controller: 'OrderEditPriorityLevelDialogController',
        controllerAs: 'ctrl',
        locals: {
          orderData: {
            id: orderId,
            prioritylevel: {
              pickup: ctrl.order._lastPpPriorityLevel,
              delivery: ctrl.order._lastDdPriorityLevel,
            },
          },
        },
      }).then(onSet);

      function onSet() {
        refreshOrderData();
      }
    }

    function showDpManagementDialog($event) {
      nvDialog.showSingle($event, {
        templateUrl: 'views/container/order/dialog/dp-management/dp-management.dialog.html',
        cssClass: 'order-edit-dp-management',
        controller: 'OrderEditDpManagementDialogController',
        controllerAs: 'ctrl',
        locals: {
          orderData: { id: orderId, transactions: ctrl.order.transactions },
        },
      }).then(onSuccess, onClose);

      function onClose() {
        refreshOrderData();
      }

      function onSuccess(val) {
        if (val === 0) {
          refreshOrderData();
        }
      }
    }

    function completeOrder($event) {
      nvDialog.showSingle($event, {
        templateUrl: 'views/container/order/dialog/complete-order/complete-order.dialog.html',
        theme: 'nvGreen',
        cssClass: 'order-edit-complete-order',
        controller: 'OrderEditCompleteOrderDialogController',
        controllerAs: 'ctrl',
        locals: {
          ordersData: [ctrl.order],
          settingsData: {},
        },
      }).then(onCompleteOrder);

      function onCompleteOrder() {
        refreshOrderData();
      }
    }

    function cancelOrder($event) {
      nvDialog.showSingle($event, {
        templateUrl: 'views/container/order/dialog/cancel-order/cancel-order.dialog.html',
        theme: 'nvRed',
        cssClass: 'order-edit-cancel-order',
        controller: 'OrderEditCancelOrderDialogController',
        controllerAs: 'ctrl',
        locals: {
          ordersData: [ctrl.order],
          settingsData: {},
        },
      }).then(onCancel);

      function onCancel() {
        refreshOrderData();
      }
    }

    function resumeOrder($event) {
      nvDialog.showSingle($event, {
        templateUrl: 'views/container/order/dialog/resume-order/resume-order.dialog.html',
        theme: 'nvGreen',
        cssClass: 'order-edit-resume-order',
        controller: 'OrderEditResumeOrderDialogController',
        controllerAs: 'ctrl',
        locals: {
          ordersData: [ctrl.order],
          settingsData: {},
        },
      }).then(onResume);

      function onResume() {
        refreshOrderData();
      }
    }

    function rescheduleOrder($event) {
      let isPickup = false;
      if (Order.isStatusEnum(ctrl.order, Order.STATUS.PICKUP_FAIL)) {
        isPickup = true;
      } else if (Order.isStatusEnum(ctrl.order, Order.STATUS.DELIVERY_FAIL)) {
        isPickup = false;
      }

      nvDialog.showSingle($event, {
        templateUrl: 'views/container/order/dialog/reschedule-order/reschedule-order.dialog.html',
        theme: 'nvBlue',
        cssClass: 'order-edit-reschedule',
        controller: 'OrderRescheduleDialogController',
        controllerAs: 'ctrl',
        locals: {
          orderData: ctrl.order,
          isPickup: isPickup,
        },
      }).then(onSet);

      function onSet() {
        refreshOrderData();
      }
    }

    function deleteOrder($event) {
      nvDialog.showSingle($event, {
        templateUrl: 'views/container/order/dialog/delete-order/delete-order.dialog.html',
        theme: 'nvRed',
        cssClass: 'order-edit-delete-order',
        controller: 'OrderEditDeleteOrderDialogController',
        controllerAs: 'ctrl',
        locals: {
          orderData: ctrl.order,
        },
      }).then(onDelete);

      function onDelete() {
        $state.go('container.order.list');
      }
    }

    // Pickup
    function showEditPickupDetailsDialog($event) {
      nvDialog.showSingle($event, {
        templateUrl: 'views/container/order/dialog/edit-pickup-details/edit-pickup-details.dialog.html',
        cssClass: 'order-edit-pickup-details',
        controller: 'OrderEditPickupDetailsDialogController',
        controllerAs: 'ctrl',
        locals: {
          orderData: ctrl.order,
        },
      }).then(onSet);

      function onSet() {
        refreshOrderData();
      }
    }

    function isAbleToEditPickupDetails(order) {
      return order.type !== Order.TYPE.NORMAL && Order.isStatusEnum(order, Order.STATUS.PENDING);
    }

    function isAbleToReverifyTransaction(txn) {
      return Transaction.isPending(txn) && !txn.transit && txn.waypointId;
    }

    function isAbleToAddToRoute(txn) {
      const isAbleToAdd = !txn.routeId && !txn.transit;

      if (!isAbleToAdd) {
        return isAbleToAdd;
      }

      // not able to add Normal pickup
      if (Transaction.isPickup(txn) && ctrl.order.type === Order.TYPE.NORMAL) {
        return false;
      }

      return isAbleToAdd;
    }

    function isAbleToPullFromRoute(txn) {
      return txn.routeId && !txn.transit;
    }

    function reverifyTransaction(txn) {
      Order.reverify([{
        trackingId: ctrl.order.trackingId,
        transactionType: txn.type,
      }]).then(success, $q.reject);

      function success() {
        nvToast.info(nvTranslate.instant('container.order.edit.reverify-success'));
        refreshOrderData();
      }
    }

    function pullFromRoute($event, pullTxn) {
      const transactionsData = [];
      if (isAbleToPullFromRoute(ctrl.order._lastPPNT)) {
        transactionsData.push(generateTransactionData(ctrl.order._lastPPNT));
      }

      if (isAbleToPullFromRoute(ctrl.order._lastDDNT)) {
        transactionsData.push(generateTransactionData(ctrl.order._lastDDNT));
      }

      const settingsData = {};
      if (_.size(transactionsData) > 1) {
        settingsData.isMultiSelect = true;
      }
      settingsData.pullTxn = pullTxn;

      nvDialog.showSingle($event, {
        templateUrl: 'views/container/order/dialog/pull-from-route/pull-from-route.dialog.html',
        theme: 'nvRed',
        cssClass: 'order-pull-from-route',
        controller: 'OrderEditPullFromRouteDialogController',
        controllerAs: 'ctrl',
        locals: {
          transactionsData: transactionsData,
          settingsData: settingsData,
        },
      }).then(onPullFromRoute);

      function onPullFromRoute() {
        refreshOrderData();
      }

      function generateTransactionData(txn) {
        return _.defaultsDeep(txn, {
          orderId: ctrl.order.id,
          trackingId: ctrl.order.trackingId,
        });
      }
    }

    function addToRoute($event, txLeg = Transaction.TYPE.DELIVERY) {
      nvDialog.showSingle($event, {
        templateUrl: 'views/container/order/dialog/add-to-route/add-to-route.dialog.html',
        theme: 'nvGreen',
        cssClass: 'order-add-to-route',
        controller: 'OrderAddToRouteDialogController',
        controllerAs: 'ctrl',
        locals: {
          ordersData: [ctrl.order],
          settingsData: {
            txLeg: txLeg,
          },
        },
      }).then(onAdd);

      function onAdd() {
        refreshOrderData();
      }
    }

    // Delivery / Return to Sender
    function showEditDeliveryDetailsDialog($event) {
      const cssClass = _.toLower(ctrl.order.granularStatus) === _.toLower(Order.GRANULAR_STATUS.COMPLETED) ?
        'order-edit-delivery-details-simple' :
        'order-edit-delivery-details';

      if (_.toLower(ctrl.order.granularStatus) === _.toLower(Order.GRANULAR_STATUS.COMPLETED)) {
        nvDialog.showSingle(null, {
          templateUrl: 'views/container/order/dialog/order-pin/order-pin.dialog.html',
          controller: 'OrderPinDialogController',
          controllerAs: 'ctrl',
          cssClass: 'order-pin-dialog',
          locals: {
            passcode: ctrl.order.confirmCode,
          },
        }).then((statusCode) => {
          if (statusCode === 0) {
            showDialog();
          }
        });
      } else {
        showDialog();
      }

      function showDialog() {
        nvDialog.showSingle($event, {
          templateUrl: 'views/container/order/dialog/edit-delivery-details/edit-delivery-details.dialog.html',
          cssClass: cssClass,
          controller: 'OrderEditDeliveryDetailsDialogController',
          controllerAs: 'ctrl',
          locals: {
            orderData: ctrl.order,
          },
        }).then(onSet);
      }

      function onSet() {
        refreshOrderData();
      }
    }

    function showOrderPhotosDialog($event) {
      // views/container/order/dialog/order-photos/order-photos.dialog.html
      nvDialog.showSingle($event, {
        templateUrl: 'views/container/order/dialog/order-photos/order-photos.dialog.html',
        controller: 'OrderPhotosDialogController',
        controllerAs: 'ctrl',
        cssClass: 'order-photos-dialog',
        locals: {
          orderData: ctrl.order,
        },
      });
    }

    function isSaas() {
      return nvEnv.isSaas(nv.config.env);
    }

    function removeFromWarehouse(event, nOrderId) {
      nvDialog.confirmSave(event, {
        title: nvTranslate.instant('container.order.edit.confirm-remove-from-warehouse'),
        content: nvTranslate.instant('container.order.edit.confirm-remove-from-warehouse-message'),
        ok: nvTranslate.instant('commons.remove'),
      }).then(() => (Order.removeFromWarehouse(nOrderId).then(onFinished)));

      function onFinished(result) {
        const orders = result.orders;
        if (orders && orders.length > 0) {
          const data = orders[0];
          if (data.status) {
            nvToast.success(nvTranslate.instant('container.order.edit.order-removed-from-warehouse'));
          } else {
            nvToast.error(data.message);
          }
        }
        refreshOrderData();
      }
    }

    function isAbleToRemoveFromStorage(order) {
      return Order.isGranularStatusEnum(order, Order.GRANULAR_STATUS.RETURNED_TO_WAREHOUSE);
    }

    function showEditRtsDetailsDialog($event) {
      nvDialog.showSingle($event, {
        templateUrl: 'views/container/order/dialog/edit-rts-details/edit-rts-details.dialog.html',
        cssClass: 'order-edit-rts-details expand',
        controller: 'OrderEditRtsDetailsDialogController',
        controllerAs: 'ctrl',
        locals: {
          ordersData: [ctrl.order],
          settingsData: {},
        },
      }).then(onSet);

      function onSet() {
        refreshOrderData();
      }
    }

    function cancelRts($event) {
      nvDialog.showSingle($event, {
        templateUrl: 'views/container/order/dialog/cancel-rts/cancel-rts.dialog.html',
        theme: 'nvRed',
        cssClass: 'order-edit-cancel-rts',
        controller: 'OrderEditCancelRtsDialogController',
        controllerAs: 'ctrl',
        locals: {
          orderData: ctrl.order,
        },
      }).then(onSet);

      function onSet() {
        refreshOrderData();
      }
    }

    function isAbleToEditDeliveryDetails(order) {
      return _.toLower(order.granularStatus) === _.toLower(Order.GRANULAR_STATUS.COMPLETED) ||
        !(order._lastDDNT && order._lastDDNT.routeId);
    }

    function isAbleToEditRtsDetails(order) {
      return Order.isCorrectStatusToRTS(order, order._lastDDNT);
    }

    // Shipment
    function navigateToShipmentManagementPage() {
      $window.open(
        $state.href('container.shipment-management')
      );
    }

    // View/Print
    function showPODDialog($event) {
      nvDialog.showSingle($event, {
        templateUrl: 'views/container/order/dialog/proof-of-delivery/proof-of-delivery.dialog.html',
        cssClass: 'order-proof-of-delivery',
        controller: 'OrderProofOfDeliveryDialogController',
        controllerAs: 'ctrl',
        locals: {
          orderData: ctrl.order,
        },
      });
    }

    function printAirwayBill(order) {
      const params = {
        tids: order.trackingId,
        h: 0,
      };

      Order.downloadAWB(params, `awb_${order.trackingId.replace(/[^a-zA-Z0-9 ]/g, '')}.pdf`);
    }

    function printShippingLabel(order) {
      const payload = [{
        recipient_name: order.toName,
        address1: order.toAddress1,
        address2: order.toAddress2,
        contact: order.toContact,
        country: order.toCountry,
        postcode: order.toPostcode,
        shipper_name: order.fromName,
        tracking_id: order.trackingId,
        barcode: order.trackingId,
        route_id: '',
        driver_name: '',
        template: 'hub_shipping_label_70X50',
      }];

      Printer.printByPost(payload)
        .then(() => {
          nvToast.success(nvTranslate.instant('container.order.edit.shipping-label-printed'));
        });
    }

    function loginShipper(shipper) {
      Shippers.loginToSd(shipper.id, shipper.email, window.location.host);
    }

    // extra tools
    function filterEvents(id) {
      let filteredEvents;

      ctrl.selectedEventsFilterModeId = id;
      if (id === EVENTS_FILTER_MODES.SCANS_ONLY.id) {
        filteredEvents = _.filter(events, event =>
          _.indexOf(event.tags, Events.TAG.SCAN) >= 0
        );
      } else if (id === EVENTS_FILTER_MODES.CHANGES_ONLY.id) {
        filteredEvents = _.filter(events, event =>
          _.indexOf(event.tags, Events.TAG.MANUAL_UPDATE) >= 0
        );
      } else { // id === EVENTS_FILTER_MODES.ALL.id
        filteredEvents = events;
      }

      ctrl.eventsTableParam.setData(filteredEvents);
      ctrl.eventsTableParam.refreshData();
    }

    function selectedEventIsRaisedButtonStyle(id) {
      const filterMode = _.find(EVENTS_FILTER_MODES, ['id', id]);
      return filterMode.isRaisedButtonStyle;
    }

    function navigateToBulkOrderPage(bulkId) {
      $window.open(
        $state.href('container.bulk-order', { id: bulkId })
      );
    }

    function navigateToAdminManifestPage(routeId) {
      $window.open(
        $state.href('container.route-manifest', { routeId: routeId })
      );
    }

    function navigateToRouteLogsPage() {
      $window.open(
        $state.href('container.route-logs')
      );
    }

    function getCurrencyWithPrice(price) {
      if (!price) {
        return '-';
      }
      return nvCurrency.getCode($rootScope.countryId) + price;
    }

    function isPriorityOrder(order) {
      // make it > 3 for now (business requirement)
      return order._priorityLevel > 3;
    }

    function isOnDemandOrder(order) {
      return order._lastPPNT._isOnDemand;
    }

    function isLinehaulOrder(order) {
      return order._isLinehaul;
    }

    function isCodOrder(order) {
      return order._totalCashToCollect !== '-';
    }

    function isCopOrder(order) {
      return order._totalCashPickup !== '-';
    }

    function printWeight(weight) {
      if (weight === null || weight === undefined) {
        return null;
      }
      if (weight < 1000) {
        weight = Math.round(weight * 100) / 100;
        return `${weight} kg`;
      }
      return '>999 kg';
    }

    function printChargeTo(chargeTo) {
      switch (chargeTo) {
        case Order.CHARGE_TO.RECIPIENT:
          return 'commons.recipient';
        case Order.CHARGE_TO.SENDER:
          return 'commons.shipper';
        default:
          return '-';
      }
    }

    function printRecoveryTicketIds(recoveryTickets) {
      const ids = [];
      _.forEach(recoveryTickets, (recoveryTicket) => {
        ids.push(recoveryTicket.id);
      });

      return ids.join(', ');
    }

    function getThemeClass(order) {
      let themeClass = 'blue';
      if (!order) {
        return themeClass;
      }

      _.forEach(PAGE_THEME_SETTINGS, (setting) => {
        if (setting.conditionFn(order)) {
          themeClass = setting.class;
          return false;
        }

        return undefined;
      });

      return themeClass;
    }

    function windowSizeChanged() {
      processOrderSummaryFirstRowHeight();
    }

    function orderSummaryDataBlocksStyle() {
      if (!ctrl.isOrderSummaryShowAllDetails) {
        return { height: `${orderSummaryFirstRowHeight}px` };
      }

      return { height: 'auto' };
    }

    // =========== Helpers functions ===========
    function refreshOrderData() {
      getEventsData();
      return Order.get(orderId).then(success);

      function success(response) {
        processOrderData(response);
        return true;
      }
    }

    function processOrderData(order) {
      ctrl.order = extendOrder(order);

      createTransactionsTable(ctrl.order.transactions);
    }

    function processEventsData() {
      OrderEventUtils.extendEvents(events);
    }

    function extendOrder(order) {
      order._status = Order.STATUS[order.status] || order.status;
      order._granularStatus = Order.GRANULAR_STATUS[order.granularStatus] || order.granularStatus;
      order._totalCashToCollect = getTotalCashToCollect(order.cod, 'DD');
      order._totalCashPickup = getTotalCashToCollect(order.cod, 'PP');
      order._fromAddress = [
        order.fromAddress1, order.fromAddress2, order.fromPostcode,
        order.fromCity, order.fromDistrict, order.fromState, order.fromCountry,
      ].join(' ');
      order._toAddress = [
        order.toAddress1, order.toAddress2, order.toPostcode,
        order.toCity, order.toDistrict, order.toState, order.toCountry,
      ].join(' ');

      order._firstPendingNTTxn = Transaction.getFirstPendingNTTransaction(order.transactions) || {};
      order._lastNTTxn = Transaction.getLastNTTransaction(order.transactions) || {};
      order._lastPPNT = Transaction.getLastPPNT(order.transactions) || {};
      order._lastPPNT._failedAttemptsCount = getPickupFailedAttemptsCount(order.transactions);
      order._lastPPNT._instructions = _.compact([
        order.instruction, order._lastPPNT.instruction, order._lastPPNT.routeComments,
      ]).join(', ');

      order._lastDDNT = Transaction.getLastDDNT(order.transactions) || {};
      order._lastDDNT._failedAttemptsCount = getDeliveryFailedAttemptsCount(order.transactions);
      order._lastDDNT._instructions = _.compact([
        order.instruction, order._lastDDNT.instruction, order._lastDDNT.routeComments,
      ]).join(', ');
      order._dimensions = getDimensionsString(order.dimensions);
      order._priorityLevel = getOrderPriorityLevel(order._firstPendingNTTxn);
      order._lastPpPriorityLevel = getOrderPriorityLevel(order._lastPPNT);
      order._lastDdPriorityLevel = getOrderPriorityLevel(order._lastDDNT);
      order._dnrName = getOrderDnrName(
        _.size(order._firstPendingNTTxn) > 0 ? order._firstPendingNTTxn : order._lastNTTxn
      );
      order._isLinehaul = false; // @NOTE - check from shipment scan payload
      // if order granular status is 'ARRIVED AT SORTING HUB' get the latest inbound scan hub
      // and country info
      if (order._granularStatus === Order.GRANULAR_STATUS.ARRIVED_AT_SORTING_HUB) {
        const combinedScan = _.concat(order.inboundScans, order.warehouseSweeps);
        const lastScan = _.chain(combinedScan).sortBy('createdAt').last().value();
        if (lastScan) {
          order._lastHubCountry = lastScan.hubCountry;
          order._lastHubCity = lastScan.hubCity;
          order._lastHub = lastScan.hubShortName;
        }
      }


      extendOriginalDetails(order);
      extendTransactions(order.transactions);

      return order;

      function getTotalCashToCollect(cod, collectionAt) {
        if (!cod || cod.collectionAt !== collectionAt) {
          return getCurrencyWithPrice(0);
        }

        let shippingAmount = parseFloat(cod.shippingAmount);
        if (shippingAmount < 0 || isNaN(shippingAmount)) {
          shippingAmount = 0;
        }

        return getCurrencyWithPrice(parseFloat(cod.goodsAmount) + shippingAmount);
      }

      function extendOriginalDetails(theOrder) {
        theOrder._originalDetails = {
          weight: theOrder.originalWeight,
        };
      }

      function extendTransactions(transactions) {
        return _.map(transactions, txn => (
          setCustomTransactionData(txn)
        ));

        function setCustomTransactionData(txn) {
          txn._startTime = txn.startTime ? nvDateTimeUtils.displayDateTime(
            moment.utc(txn.startTime)
          ) : '';
          txn._endTime = txn.endTime ? nvDateTimeUtils.displayDateTime(
            moment.utc(txn.endTime)
          ) : '';
          txn._serviceEndTime = txn.serviceEndTime ? nvDateTimeUtils.displayDateTime(
            moment.utc(txn.serviceEndTime)
          ) : '';
          txn._routeDate = txn.routeDate ? nvDateTimeUtils.displayDate(
            moment.utc(txn.routeDate)
          ) : '';
          txn._driver = txn.driverName;
          txn._address = [txn.address1, txn.address2, txn.city, txn.country, txn.postcode].join(' ');

          txn._isLate = false;
          if (txn.serviceEndTime && txn.endTime) {
            txn._isLate = moment.utc(txn.serviceEndTime).diff(moment.utc(txn.endTime)) > 0;
          }

          txn._isFailed = Transaction.isFailed(txn);
          txn._isHyperlocal = false;
          txn._isOnDemand = false;
          txn._isNinjaCollect = txn.distributionPointId &&
            txn.dnrId !== Transaction.DNR.SET_ASIDE_NINJA_COLLECT;
          txn._isNinjaEarlyCollect = txn.distributionPointId &&
            txn.dnrId === Transaction.DNR.SET_ASIDE_NINJA_COLLECT;

          return txn;
        }
      }

      function getPickupFailedAttemptsCount(transactions) {
        return _.size(_.filter(transactions, txn =>
          Transaction.isFailed(txn) && Transaction.isPickup(txn)
        ));
      }

      function getDeliveryFailedAttemptsCount(transactions) {
        return _.size(_.filter(transactions, txn =>
          Transaction.isFailed(txn) && Transaction.isDelivery(txn)
        ));
      }

      function getDimensionsString(dimensions) {
        if (!dimensions || !_.isNumber(dimensions.width) ||
          !_.isNumber(dimensions.height) || !_.isNumber(dimensions.length)) {
          return '(L) x (B) x (H) cm';
        }

        return `${[dimensions.width, dimensions.height, dimensions.length].join(' x ')} cm`;
      }

      function getOrderPriorityLevel(txn) {
        return _.size(txn) ? txn.priorityLevel : 0;
      }

      function getOrderDnrName(txn) {
        return _.size(txn) ? txn.dnr : '-';
      }
    }


    function getLatestEventData() {
      if (events) {
        events = _.sortBy(events, '_event_time').reverse();
        return _.first(events);
      }

      return {};
    }

    function getLatestEventDataWithRoute() {
      if (events) {
        events = _.sortBy(events, '_event_time').reverse();
        return _.find(events, event => event._route_id);
      }

      return {};
    }

    function getEventsData() {
      $q.all([
        Events.getOrderEvents(orderId),
        Events.getOrderEventsV2(orderId),
      ]).then((responses) => {
        events = responses[0];
        processEventsData();
        const orderEvents = OrderEventUtils.processOrderEventData(responses[1].data);
        events = events.concat(orderEvents);
        ctrl.latestEventData = getLatestEventData();
        ctrl.latestEventDataWithRoute = getLatestEventDataWithRoute();
        createEventsTable();
      },

      () => {
        // if fail just call old API
        Events.getOrderEvents(orderId).then((response) => {
          events = response;
          processEventsData();
          createEventsTable();
        }, $q.reject);
      });
    }

    /* eslint no-underscore-dangle: ["error", { "allow": ["_status"] }]*/
    function getTicketingData() {
      const trackingIds = [];
      trackingIds.push(ctrl.order.trackingId);
      if (ctrl.order.stampId) {
        trackingIds.push(ctrl.order.stampId);
      }
      Ticketing.searchTicket({
        tracking_ids: trackingIds,
      }).then(success, $q.reject);

      function success(response) {
        ctrl.recoveryTickets = _.orderBy(response.tickets, 'createdAt', 'desc');
        // get the latest ticket
        if (ctrl.order._status === Order.STATUS.CANCELLED) {
          const firstTicket = _.first(ctrl.recoveryTickets);
          if (firstTicket && ((firstTicket.ticketType === 'MISSING') ||
            (firstTicket.ticketType === 'DAMAGED'))) {
            ctrl.cancelledByPetsMessage =
            nvTranslate.instant('container.order.edit.cancelled-pets-outcome-order-x', { x: firstTicket.outcome });
          }
        } else {
          ctrl.cancelledByPetsMessage = null;
        }
        // check for tickets resolve status
        let allResolved = true;
        if (ctrl.order._status === Order.STATUS.ON_HOLD) {
          _.forEach(ctrl.recoveryTickets, (ticket) => {
            allResolved = allResolved && (ticket.status === Ticketing.statuses.RESOLVED
              || ticket.status === Ticketing.statuses.CANCELLED);
          });
          // show alert if order still in on hold status
          if (allResolved) {
            nvDialog.showSingle(null, {
              type: 'nv-dialog',
              theme: 'nvRed',
              templateUrl: 'views/container/order/dialog/invalid-pets-state/invalid-pets-state.dialog.html',
              cssClass: 'save-preset',
              scope: _.assign(nvEditModelDialog.scopeTemplate(), {}),
            });
          }
        }
      }
    }

    function getEnableCreateNewTicket() {
      const unresolvedTicket = _.find(ctrl.recoveryTickets, ticket => (
        ticket.status !== 'CANCELLED' && ticket.status !== 'RESOLVED'
      ));
      if (unresolvedTicket) {
        return false;
      }
      return true;
    }

    function getReservationsData() {
      const wpIds = [];
      _.forEach(ctrl.order.transactions, (txn) => {
        if (Transaction.isPickup(txn)) {
          wpIds.push(txn.waypointId);
        }
      });

      if (_.size(_.compact(wpIds)) <= 0) {
        return;
      }

      Reservation.search({
        waypointId: _.compact(wpIds),
      }).then(success, $q.reject);

      function success(response) {
        processTransactionsData(response);
      }

      function processTransactionsData(reservations) {
        _.forEach(ctrl.order.transactions, (txn) => {
          const reservation = _.find(reservations, [
            'activeWaypoint.id', txn.waypointId,
          ]);

          txn._reservation = null;
          txn._isHyperlocal = false;
          txn._isOnDemand = false;
          if (reservation) {
            txn._reservation = setCustomReservationData(reservation);
            txn._isHyperlocal = Reservation.isHyperlocal(reservation);
            txn._isOnDemand = reservation.onDemand || false;
          }
        });
      }

      function setCustomReservationData(reservation) {
        reservation._readyDatetime = reservation.readyDatetime ? nvDateTimeUtils.displayDateTime(
          moment.utc(reservation.readyDatetime)
        ) : '';
        reservation._latestDatetime = reservation.latestDatetime ? nvDateTimeUtils.displayDateTime(
          moment.utc(reservation.latestDatetime)
        ) : '';

        return reservation;
      }
    }

    function createTransactionsTable(transactions) {
      ctrl.transactionsTableParam = nvTable.createTable(Transaction.fields);
      ctrl.transactionsTableParam.addColumn('_serviceEndTime', { displayName: 'container.order.edit.service-end-time' });
      ctrl.transactionsTableParam.addColumn('_driver', { displayName: 'commons.driver' });
      ctrl.transactionsTableParam.addColumn('_routeDate', { displayName: 'commons.model.route-date' });
      ctrl.transactionsTableParam.addColumn('failure_reason_code', { displayName: 'container.order.edit.failure-reason' });
      ctrl.transactionsTableParam.addColumn('_address', { displayName: 'container.order.edit.destination-address' });
      ctrl.transactionsTableParam.setData(transactions);
    }

    function createEventsTable() {
      if (ctrl.eventsTableParam == null) {
        ctrl.eventsTableParam = nvTable.createTable(Events.FIELDS);
        ctrl.eventsTableParam.addColumn('_event_time', { displayName: 'commons.datetime' });
        ctrl.eventsTableParam.addColumn('_tags', { displayName: 'container.order.edit.event-tags' });
        ctrl.eventsTableParam.addColumn('_user', { displayName: 'commons.user-id' });
        ctrl.eventsTableParam.addColumn('_scan_id', { displayName: 'commons.scan-id' });
        ctrl.eventsTableParam.addColumn('_route_id', { displayName: 'commons.model.route-id' });
        ctrl.eventsTableParam.addColumn('_hub_name', { displayName: 'commons.hub-name' });
        ctrl.eventsTableParam.addColumn('_description', { displayName: 'commons.description' });

        ctrl.eventsTableParam.updateColumnFilter('_description', strFn => (
          (event) => {
            // strfn is the function that will return you the search text
            const lowercase = strFn().toLowerCase();
            let descriptionText = '';
            if (event._description) {
              descriptionText = nvTranslate.instant(event._description, event.data);
            }

            return lowercase.length === 0 ||
              (descriptionText && descriptionText.toLowerCase().indexOf(lowercase) >= 0);
          }
        ));
      }

      if (events) {
        ctrl.eventsTableParam.setData(events);
        ctrl.eventsTableParam.refreshData();
      }
    }

    function intermediaryStatuses(order) {
      return !(isStaging(order) || isCancelled(order) || isCompleted(order));
    }

    function isStaging(order) {
      return Order.isStatusEnum(order, Order.STATUS.STAGING);
    }

    function isCancelled(order) {
      return Order.isStatusEnum(order, Order.STATUS.CANCELLED);
    }

    function isCompleted(order) {
      return Order.isStatusEnum(order, Order.STATUS.COMPLETED);
    }

    function processOrderSummaryFirstRowHeight() {
      if (!ctrl.order) {
        return;
      }

      $timeout(() => {
        orderSummaryFirstRowHeight = 0;

        let dataBlocksWidth = $('#order-summary .data-blocks').width();
        _.forEach($('#order-summary .data-blocks').children(), (dataBlock) => {
          const dataBlockWidth = $(dataBlock).outerWidth(true);
          const dataBlockHeight = $(dataBlock).outerHeight(true);
          dataBlocksWidth -= dataBlockWidth;

          if (dataBlocksWidth < 0) {
            return false;
          }

          if (dataBlockHeight > orderSummaryFirstRowHeight) {
            orderSummaryFirstRowHeight = dataBlockHeight;
          }

          return undefined;
        });
      });
    }

    function processContentScrollEvent() {
      $timeout(() => {
        contentHolderDiv = angular.element(
          $('.view-container .content-holder')
        );

        contentHolderDiv.off('scroll');
        contentHolderDiv.on('scroll', () => {
          ctrl.isContentScrollTopZero = contentHolderDiv.scrollTop() === 0;

          if (!$scope.$$phase) {
            $scope.$apply(); // instant refresh
          }
        });
      });
    }

    function changeDocumentTitle(order) {
      document.title = [order.trackingId, '-', document.title].join(' ');
    }

    function setOriginalDocumentTitle(order) {
      if (order) {
        document.title = document.title.replace([
          order.trackingId, '-', '',
        ].join(' '), '');
      }
    }

    function openMultiParcelMenu($mdOpenMenu, ev) {
      if (ctrl.order.multiParcelOrders && ctrl.order.multiParcelOrders.length) {
        $mdOpenMenu(ev);
      }
    }

    function gotToOrder(order) {
      $window.open(
        $state.href('container.order.edit', { orderId: order.id })
      );
    }

    function checkStrLen(str) {
      return _.size(str);
    }

    function parseShipperRefMetadata(obj) {
      let str = '';
      _.forEach(obj, (value, key) => {
        if (_.isObject(value)) {
          str += `${key}, ${parseShipperRefMetadata(value)}<br>`;
        } else {
          str += `${key}: ${value}<br>`;
        }
      });
      return str || '-';
    }

    function openShipperRefMetada(event) {
      nvPanel.show(event, {
        controller: 'ShipperRefMetadataViewController',
        templateUrl: 'views/container/order/panel/shipper-ref-metadata/shipper-ref-metadata.panel.html',
        panelClass: 'shipper-ref-metadata',
        controllerAs: 'ctrl',
        locals: {
          shipperRefMetadata: parseShipperRefMetadata(ctrl.order.shipperRefMetadata),
        },
      });
    }
  }
}());
