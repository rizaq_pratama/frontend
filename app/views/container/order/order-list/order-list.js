(function controller() {
  angular
    .module('nvOperatorApp.controllers')
    .controller('OrderListController', OrderListController);

  OrderListController.$inject = [
    '$q', '$window', '$state', 'Order', 'nvTable',
    'nvDateTimeUtils', 'Printer', '$scope', 'nvToast', 'nvTranslate',
    'nvRequestUtils', 'OrderFilter', 'OrderFilterTemplate', 'nvBackendFilterUtils', 'nvDialog',
    'Transaction', 'Shippers', 'nvCsvParser', 'nvFileUtils',
  ];

  const CSV_HEADER = ['Tracking ID'];
  const field = ['trackingId'];

  function OrderListController(
    $q, $window, $state, Order, nvTable,
    nvDateTimeUtils, Printer, $scope, nvToast, nvTranslate,
    nvRequestUtils, OrderFilter, OrderFilterTemplate, nvBackendFilterUtils, nvDialog,
    Transaction, Shippers, nvCsvParser, nvFileUtils
  ) {
    // ////////////////////////////////////////////////
    // variables
    // ////////////////////////////////////////////////
    const ctrl = this;
    const PAGES = { FILTER: 'filter', RESULT: 'result' };
    const SEARCH_MODE = {
      SPECIFIC: 'specific',
      FILTER: 'filter',
      CSV: 'csv',
    };
    const SEARCH_CATEGORY = {
      TRACKING_ID: {
        id: 'TRACKING_ID',
        displayName: 'container.order.list.tracking-or-stamp-id',
        fields: ['tracking_id', 'stamp_id'],
      },
      NAME: {
        id: 'NAME',
        displayName: 'container.order.list.name',
        fields: ['from_name', 'to_name'],
      },
      CONTACT: {
        id: 'CONTACT',
        displayName: 'container.order.list.contact-number',
        fields: ['from_contact', 'to_contact'],
      },
      ADDRESS: {
        id: 'ADDRESS',
        displayName: 'container.order.list.recipient-address-line-1',
        fields: ['to_address1'],
      },
    };
    const SEARCH_LOGIC = {
      EXACT: {
        id: Order.ELASTIC_SEARCH_MATCH_TYPE.EXACT,
        displayName: 'container.order.list.exactly-matches',
      },
      WILDCARD: {
        id: Order.ELASTIC_SEARCH_MATCH_TYPE.FULL_TEXT,
        displayName: 'container.order.list.contains',
      },
    };
    const ORDER_VALIDATION_ACTIONS = {
      RTS: 'commons.return-to-sender',
      CANCEL: 'container.order.edit.cancel-order',
      RESUME: 'container.order.edit.resume-order',
      COMPLETE: 'container.order.edit.complete-order',
      PULL_FROM_ROUTE: 'container.order.edit.pull-from-route',
    };

    const MASTER_SHIPPER_NAME = 'master_shippers';
    const SHIPPER_ID = 'shipper_id';

    let currentSearchMode = null;
    let csvOrderIds = [];

    ctrl.PAGES = PAGES;
    ctrl.SEARCH_MODE = SEARCH_MODE;
    ctrl.currentPage = PAGES.FILTER;

    // Filter page
    ctrl.getOrdersState = 'idle';
    ctrl.specificSearch = {
      form: {},
      state: 'idle',
      categoryOptions: [],
      searchLogicOptions: [],
      categoryValue: SEARCH_CATEGORY.TRACKING_ID.id,
      searchLogicValue: SEARCH_LOGIC.WILDCARD.id,
      oneMonthSearch: true,
      text: null,
      oldText: null,
      selectedOrder: null,
      findTrackingId: specificSearchFindTrackingId,
      isDisabled: isSpecificSearchButtonDisabled,
      selectedItemChange: selectedItemChange,
      errors: [{
        key: 'noResult',
        msg: 'directive.table.no-result',
      }],
    };

    ctrl.filterPresets = [];
    ctrl.selectedPreset = null;
    ctrl.selectedFilters = [];
    ctrl.possibleFilters = [];

    // Result page
    ctrl.page = 1;
    ctrl.pageSize = 1000;
    ctrl.ordersTableParam = null;
    ctrl.isApplyActionLoading = false;

    // ////////////////////////////////////////////////
    // functions
    // ////////////////////////////////////////////////
    ctrl.goToPage = goToPage;
    ctrl.isPage = isPage;
    ctrl.init = init;

    // Filter page
    ctrl.getOrders = getOrders;
    ctrl.loadPreset = loadPreset;
    ctrl.createPreset = createPreset;
    ctrl.deletePreset = deletePreset;
    ctrl.updatePreset = updatePreset;
    ctrl.checkNameAvailability = checkNameAvailability;
    ctrl.findOrdersWithCsv = findOrdersWithCsv;

    // Result page
    ctrl.getSelectedCount = getSelectedCount;
    ctrl.returnOrdersToSender = returnOrdersToSender;
    ctrl.cancelOrders = cancelOrders;
    ctrl.resumeOrders = resumeOrders;
    ctrl.completeOrders = completeOrders;
    ctrl.pullOrdersFromRoute = pullOrdersFromRoute;
    ctrl.addOrdersToRoute = addOrdersToRoute;
    ctrl.printAirwayBillsPreview = printAirwayBillsPreview;
    ctrl.printShippingLabels = printShippingLabels;
    ctrl.recalculatePricing = recalculatePricing;
    ctrl.printPod = printPod;
    ctrl.downloadSelectedAsCsv = downloadSelectedAsCsv;
    ctrl.isCsvDownloadEnabled = isCsvDownloadEnabled;

    ctrl.loadMore = loadMore;
    ctrl.editOrder = editOrder;
    ctrl.printAirwayBill = printAirwayBill;
    ctrl.printShippingLabel = printShippingLabel;

    // ////////////////////////////////////////////////
    // functions details
    // ////////////////////////////////////////////////
    function init() {
      ctrl.specificSearch.categoryOptions = searchCategoryToOptions();
      ctrl.specificSearch.searchLogicOptions = searchLogicToOptions();

      return OrderFilterTemplate.read().then(success);

      async function success(data) {
        ctrl.filterPresets = data;

        const filters = await OrderFilter.init();
        _.each(filters.generalFilters, filter => ctrl.possibleFilters.push(filter));
        ctrl.selectedFilters = _.remove(ctrl.possibleFilters,
          filter => filter.showOnInit === true);
        ctrl.selectedPreset = null;
      }

      function searchCategoryToOptions() {
        return _.map(SEARCH_CATEGORY, category => ({
          displayName: category.displayName,
          value: category.id,
        }));
      }

      function searchLogicToOptions() {
        return _.map(SEARCH_LOGIC, row => ({
          displayName: row.displayName,
          value: row.id,
        }));
      }
    }

    function isPage(page) {
      return ctrl.currentPage === page;
    }

    function goToPage(page) {
      if (page === PAGES.FILTER) {
        ctrl.page = 1;
        ctrl.ordersTableParam = null;
      }
      ctrl.currentPage = page;
    }

    function getOrders(searchMode) {
      if (searchMode === SEARCH_MODE.FILTER) {
        ctrl.getOrdersState = 'waiting';
      } else if (searchMode === SEARCH_MODE.SPECIFIC) {
        ctrl.specificSearch.state = 'waiting';
      }

      currentSearchMode = searchMode;

      return searchOrdersPromise().then(success, () => {
        if (searchMode === SEARCH_MODE.FILTER) {
          ctrl.getOrdersState = 'idle';
        } else if (searchMode === SEARCH_MODE.SPECIFIC) {
          ctrl.specificSearch.state = 'idle';
        }
      });

      function success(response) {
        if (searchMode === SEARCH_MODE.FILTER) {
          ctrl.getOrdersState = 'idle';
          processOrdersTableParam(response);
        } else if (searchMode === SEARCH_MODE.SPECIFIC) {
          ctrl.specificSearch.state = 'idle';

          if (response && _.size(response.orders) === 1) {
            ctrl.specificSearch.form.searchTerm.$setValidity('noResult', true);
            editOrder(response.orders[0]);
          } else if (response && _.size(response.orders) > 1) {
            ctrl.possibleFilters = _.concat(ctrl.possibleFilters, ctrl.selectedFilters);
            ctrl.selectedFilters = [];

            processOrdersTableParam(response);
          } else {
            ctrl.specificSearch.form.searchTerm.$setValidity('noResult', false);
          }
        }
      }
    }

    function processOrdersTableParam(response) {
      const selectedFilters = getSelectedFilters();
      _.forEach(selectedFilters, (filter) => {
        filter.description = nvBackendFilterUtils.getDescription(filter);
      });

      if (!ctrl.ordersTableParam) {
        ctrl.ordersTableParam = nvTable.createTable(Order.FIELDS);
        ctrl.ordersTableParam.addColumn('_granularStatus', { displayName: 'commons.model.granular-status' });
        ctrl.ordersTableParam.addColumn('_fromAddress', { displayName: 'commons.from-address' });
        ctrl.ordersTableParam.addColumn('_toAddress', { displayName: 'commons.to-address' });
        ctrl.ordersTableParam.addColumn('_createdAt', { displayName: 'commons.created-at' });
      }

      const orders = extendOrders((response && response.orders) || []);
      ctrl.ordersTableParam.setData(orders);
      ctrl.ordersTableParam.totalItems = response.count;

      goToPage(PAGES.RESULT);
    }

    function getSelectedCount() {
      return ctrl.ordersTableParam.getSelection().length;
    }

    function returnOrdersToSender($event, orders) {
      const ordersValidationErrorData = getOrdersValidationErrorData(
        orders, ORDER_VALIDATION_ACTIONS.RTS
      );
      if (ordersValidationErrorData.errors.length > 0) {
        showSelectionErrorDialog(orders, ordersValidationErrorData, $event, returnOrdersToSender);
        return;
      }

      nvDialog.showSingle($event, {
        templateUrl: 'views/container/order/dialog/edit-rts-details/edit-rts-details.dialog.html',
        cssClass: 'order-edit-rts-details',
        controller: 'OrderEditRtsDetailsDialogController',
        controllerAs: 'ctrl',
        locals: {
          ordersData: orders,
          settingsData: {
            isMultiSelect: true,
          },
        },
      }).then(onSet);

      function onSet(response) {
        const unRtsedOrders = response.failed;
        _.forEach(unRtsedOrders, (order) => {
          order.id = order.order_id;
        });

        refreshOrderData(unRtsedOrders);
      }
    }

    function cancelOrders($event, orders) {
      const ordersValidationErrorData = getOrdersValidationErrorData(
        orders, ORDER_VALIDATION_ACTIONS.CANCEL
      );
      if (ordersValidationErrorData.errors.length > 0) {
        showSelectionErrorDialog(orders, ordersValidationErrorData, $event, cancelOrders);
        return;
      }

      nvDialog.showSingle($event, {
        templateUrl: 'views/container/order/dialog/cancel-order/cancel-order.dialog.html',
        theme: 'nvRed',
        cssClass: 'order-edit-cancel-order',
        controller: 'OrderEditCancelOrderDialogController',
        controllerAs: 'ctrl',
        locals: {
          ordersData: orders,
          settingsData: {
            isMultiSelect: true,
          },
        },
      }).then(success);

      function success() {
        refreshOrderData(orders);
      }
    }

    function resumeOrders($event, orders) {
      const ordersValidationErrorData = getOrdersValidationErrorData(
        orders, ORDER_VALIDATION_ACTIONS.RESUME
      );
      if (ordersValidationErrorData.errors.length > 0) {
        showSelectionErrorDialog(orders, ordersValidationErrorData, $event, resumeOrders);
        return;
      }

      nvDialog.showSingle($event, {
        templateUrl: 'views/container/order/dialog/resume-order/resume-order.dialog.html',
        theme: 'nvGreen',
        cssClass: 'order-edit-resume-order',
        controller: 'OrderEditResumeOrderDialogController',
        controllerAs: 'ctrl',
        locals: {
          ordersData: orders,
          settingsData: {
            isMultiSelect: true,
          },
        },
      }).then(onResume);

      function onResume() {
        refreshOrderData(orders);
      }
    }

    function completeOrders($event, orders) {
      const ordersValidationErrorData = getOrdersValidationErrorData(
        orders, ORDER_VALIDATION_ACTIONS.COMPLETE
      );
      if (ordersValidationErrorData.errors.length > 0) {
        showSelectionErrorDialog(orders, ordersValidationErrorData, $event, completeOrders);
        return;
      }

      nvDialog.showSingle($event, {
        templateUrl: 'views/container/order/dialog/complete-order/complete-order.dialog.html',
        theme: 'nvGreen',
        cssClass: 'order-edit-complete-order',
        controller: 'OrderEditCompleteOrderDialogController',
        controllerAs: 'ctrl',
        locals: {
          ordersData: orders,
          settingsData: {
            isMultiSelect: true,
          },
        },
      }).then(success);

      function success() {
        refreshOrderData(orders);
      }
    }

    function pullOrdersFromRoute($event, orders) {
      const ordersValidationErrorData = getOrdersValidationErrorData(
        orders, ORDER_VALIDATION_ACTIONS.PULL_FROM_ROUTE
      );
      if (ordersValidationErrorData.errors.length > 0) {
        showSelectionErrorDialog(orders, ordersValidationErrorData, $event, pullOrdersFromRoute);
        return;
      }

      nvDialog.showSingle($event, {
        templateUrl: 'views/container/order/dialog/pull-from-route/pull-from-route.dialog.html',
        theme: 'nvRed',
        cssClass: 'order-pull-from-route',
        controller: 'OrderEditPullFromRouteDialogController',
        controllerAs: 'ctrl',
        locals: {
          transactionsData: generatePullFromRouteTransactionsData(orders),
          settingsData: {
            isMultiSelect: true,
            showTrackingId: true,
          },
        },
      }).then(onPullFromRoute);

      function onPullFromRoute() {
        refreshOrderData();
      }
    }

    function addOrdersToRoute($event, orders) {
      nvDialog.showSingle($event, {
        templateUrl: 'views/container/order/dialog/add-to-route/add-to-route.dialog.html',
        theme: 'nvGreen',
        cssClass: 'order-add-to-route',
        controller: 'OrderAddToRouteDialogController',
        controllerAs: 'ctrl',
        locals: {
          ordersData: orders,
          settingsData: {
            isMultiSelect: true,
          },
        },
      }).then(onAdd);

      function onAdd() {
        refreshOrderData();
      }
    }

    function printAirwayBillsPreview($event, orders) {
      nvDialog.showSingle($event, {
        templateUrl: 'views/container/order/dialog/print-airway-bills/print-airway-bills.dialog.html',
        cssClass: 'print-airway-bills',
        controller: 'PrintAirwayBillsDialogController',
        controllerAs: 'ctrl',
        locals: {
          ordersData: orders,
        },
      });
    }

    function printShippingLabels(orders) {
      // elastic search order return limited fields,
      // so have to call core to get extra field(s)
      ctrl.isApplyActionLoading = true;
      Order.search({
        noOfRecords: _.size(orders),
        page: 1,
        orderIds: _.map(orders, 'id'),
        oneMonthSearch: false,
      }).then(success);

      function success(response) {
        const theOrders = _.get(response, 'orders');
        if (_.size(theOrders) <= 0) {
          ctrl.isApplyActionLoading = false;
          return;
        }

        const results = [];
        const payload = [];
        _.forEach(theOrders, order =>
          payload.push(generateShippingLabelPayload(order))
        );

        const chunkPayload = _.chunk(payload, 100);
        _.forEach(chunkPayload, (smallPayload) => {
          results.push(Printer.printByPost(smallPayload));
        });

        $q.all(results).then(() => {
          nvToast.success(nvTranslate.instant('container.order.edit.shipping-label-printed'));
        }).finally(finallyFn);

        function finallyFn() {
          ctrl.isApplyActionLoading = false;
        }
      }
    }

    function recalculatePricing(orders) {
      const payload = _.map(orders, order => ({ trackingId: order.trackingId }));
      Order.batchRecalculate(payload).then((() => nvToast.success(nvTranslate.instant('container.order.list.recalculation-orders-have-been-submitted'))));
    }

    function loadMore() {
      return ctrl.ordersTableParam.mergeInPromise(loadMoreOrders(), 'id');

      function loadMoreOrders() {
        if (ctrl.ordersTableParam.getUnfilteredLength() < ctrl.ordersTableParam.totalItems) {
          ctrl.page += 1;
          return searchOrdersPromise().then(ordersExtend);
        }
        return $q.reject();
      }

      function ordersExtend(response) {
        if (response && response.orders && response.orders.length > 0) {
          return extendOrders((response && response.orders) || []);
        }

        return $q.reject();
      }
    }

    async function searchOrdersPromise() {
      const deferred = $q.defer();
      let filterData;

      switch (currentSearchMode) {
        case SEARCH_MODE.FILTER: {
          filterData = await buildESFilterData();

          Order.elasticSearch(filterData, getElasticSearchUrlParams())
            .then(elasticSearchSuccess, elasticSearchError);

          return deferred.promise;
        }
        case SEARCH_MODE.SPECIFIC: {
          filterData = await buildESFilterData();

          Order.elasticSearch(filterData, getElasticSearchUrlParams())
            .then(elasticSearchSuccess, elasticSearchError);

          return deferred.promise;
        }
        case SEARCH_MODE.CSV: {
          const param = {
            page: ctrl.page,
            noOfRecords: _.size(csvOrderIds),
            orderIds: csvOrderIds,
          };

          return Order.searchSummary(param);
        }
        default: {
          return null;
        }
      }

      function getElasticSearchUrlParams() {
        return {
          from: (ctrl.page - 1) * ctrl.pageSize,
          size: ctrl.pageSize,
        };
      }

      function elasticSearchSuccess(response) {
        const orders = extendESOrders(_.map(response.search_data, 'order'));

        deferred.resolve({
          count: response.total,
          orders: orders,
        });
      }

      function elasticSearchError() {
        deferred.reject();
      }
    }

    function editOrder(order) {
      $window.open(
        $state.href('container.order.edit', { orderId: order.id })
      );
    }

    function printAirwayBill(order) {
      const params = {
        tids: order.trackingId,
        h: 0,
      };

      Order.downloadAWB(params, `awb_${order.trackingId.replace(/[^a-zA-Z0-9 ]/g, '')}.pdf`);
    }

    function printShippingLabel(order) {
      // elastic search order return limited fields,
      // so have to call core to get extra field(s)
      Order.search({
        noOfRecords: 1,
        page: 1,
        orderIds: [order.id],
        oneMonthSearch: false,
      }).then(success);

      function success(response) {
        const theOrder = _.get(response, 'orders[0]');
        if (!theOrder) {
          return;
        }

        const payload = [generateShippingLabelPayload(theOrder)];

        Printer.printByPost(payload)
          .then(() => {
            nvToast.success(nvTranslate.instant('container.order.edit.shipping-label-printed'));
          });
      }
    }

    function generateShippingLabelPayload(order) {
      return {
        recipient_name: order.toName,
        address1: order.toAddress1,
        address2: order.toAddress2,
        contact: order.toContact,
        country: order.toCountry,
        postcode: order.toPostcode,
        shipper_name: order.fromName,
        tracking_id: order.trackingId,
        barcode: order.trackingId,
        route_id: '',
        driver_name: '',
        template: 'hub_shipping_label_70X50',
      };
    }

    function extendOrders(orders) {
      /* eslint no-underscore-dangle: ["error", { "allow": [
        "_fromAddress", "_toAddress", "_createdAt", "_granularStatus"
      ] }]*/

      return _.map(orders, order => (
        setCustomOrderData(order)
      ));

      function setCustomOrderData(order) {
        order._fromAddress = [
          order.fromAddress1, order.fromAddress2, order.fromCity,
        ].join(' ');
        order._toAddress = [
          order.toAddress1, order.toAddress2, order.toCity,
        ].join(' ');
        order._createdAt = order.createdAt ? nvDateTimeUtils.displayDateTime(
          moment.utc(order.createdAt)
        ) : '';
        order._granularStatus = Order.GRANULAR_STATUS[order.granularStatus] || order.granularStatus;

        return order;
      }
    }

    async function buildESFilterData() {
      let filterData;
      switch (currentSearchMode) {
        case SEARCH_MODE.FILTER: {
          filterData = {
            search_filters: [],
          };
          const selectedFilters = getSelectedFilters();
          const marketplaceSellers = await getSellerIds();
          useMarketplaceSeller(marketplaceSellers);
          completeFilter(selectedFilters);
          $scope.$apply();
          break;
        }
        case SEARCH_MODE.SPECIFIC: {
          filterData = {
            search_field: {
              fields: SEARCH_CATEGORY[ctrl.specificSearch.categoryValue].fields,
              value: ctrl.specificSearch.text,
              match_type: ctrl.specificSearch.searchLogicValue,
            },
          };
          if (ctrl.specificSearch.oneMonthSearch) {
            filterData.search_range = {
              field: 'created_at',
              start_time: nvDateTimeUtils.displayFormat(
                nvDateTimeUtils.toSOD(
                  moment(new Date()).subtract(1, 'months')
                ),
                nvDateTimeUtils.FORMAT_ISO_TZ,
                'UTC'
              ),
              end_time: nvDateTimeUtils.displayFormat(
                nvDateTimeUtils.toEOD(
                  moment(new Date())
                ),
                nvDateTimeUtils.FORMAT_ISO_TZ,
                'UTC'
              ),
            };
          }
          break;
        }
        default: {
          filterData = null;
        }
      }
      return filterData;
      function useMarketplaceSeller(marketplaceSellers) {
        if (marketplaceSellers.length > 0) {
          filterData.search_filters.push(
            {
              field: 'shipper_id',
              values: _.map(marketplaceSellers, 'legacy_id'),
            }
          );
        }
        return $q.resolve(filterData);
      }
      function completeFilter(selectedFilters) {
        _.forEach(selectedFilters, (selectedFilter) => {
          switch (selectedFilter.type) {
            case nvBackendFilterUtils.FILTER_OPTIONS:
            case nvBackendFilterUtils.FILTER_AUTOCOMPLETE: {
              const values = _.map(selectedFilter.selectedOptions, 'id');
              if (_.size(values) > 0) {
                if (selectedFilter.columnName === SHIPPER_ID) {
                  // handle shipper ids from master shipper
                  const currentFilter = _.find(filterData.search_filters, { field: 'shipper_id' });
                  if (currentFilter) {
                    currentFilter.values =
                      _.union(_.map(selectedFilter.selectedOptions, 'id'), currentFilter.values);
                  } else {
                    pushFilterData(selectedFilter);
                  }
                } else if (selectedFilter.columnName !== MASTER_SHIPPER_NAME) {
                  pushFilterData(selectedFilter);
                }
              }
              break;
            }
            case nvBackendFilterUtils.FILTER_TIME: {
              filterData.search_range = {
                field: selectedFilter.columnName,
                start_time: nvDateTimeUtils.displayFormat(
                  selectedFilter.fromModel, nvDateTimeUtils.FORMAT_ISO_TZ, 'UTC'
                ),
                end_time: nvDateTimeUtils.displayFormat(
                  selectedFilter.toModel, nvDateTimeUtils.FORMAT_ISO_TZ, 'UTC'
                ),
              };
              break;
            }
            default: {
              return false;
            }
          }

          return _.noop();
        });
      }
      function pushFilterData(selectedFilter) {
        filterData.search_filters.push({
          field: selectedFilter.columnName,
          values: _.map(selectedFilter.selectedOptions, 'id'),
        });
      }
    }

    async function specificSearchFindTrackingId() {
      currentSearchMode = SEARCH_MODE.SPECIFIC;
      ctrl.specificSearch.oldText = ctrl.specificSearch.text;

      const defer = $q.defer();

      const filterData = await buildESFilterData();
      Order.elasticSearch(filterData, {
        from: 0,
        size: 100,
      }).then(success, error);

      return defer.promise;

      function success(response) {
        const orders = extendESOrders(_.map(response.search_data, 'order'));

        defer.resolve(orders);
      }

      function error() {
        defer.resolve([]);
      }
    }

    function extendESOrders(orders) {
      return _.map(orders, order => (
        setCustomESOrderData(order)
      ));

      function setCustomESOrderData(order) {
        _.forEach(order, (value, key) => {
          order[snakeToCamel(key)] = value;
        });

        // set status and granularStatus as enum
        order.status = Order.getStatusEnum(order.status);
        order.granularStatus = Order.getGranularStatusEnum(order.granularStatus);

        return order;
      }

      function snakeToCamel(string) {
        return string.replace(/(_\w)/g, m => m[1].toUpperCase());
      }
    }

    function isSpecificSearchButtonDisabled() {
      return !ctrl.specificSearch.text;
    }

    function loadPreset(preset) {
      nvBackendFilterUtils.convertPresetToFilter(
        ctrl.possibleFilters, ctrl.selectedFilters, preset);
    }

    function createPreset(data) {
      const result = nvBackendFilterUtils.getAddPresetFields(
        data.name, getSelectedFilters()
      );
      return OrderFilterTemplate.create(result);
    }

    function deletePreset(id) {
      return OrderFilterTemplate.delete(id);
    }

    function updatePreset(id, data) {
      const result = nvBackendFilterUtils.getAddPresetFields(
        data.name, getSelectedFilters()
      );
      return OrderFilterTemplate.update(id, result);
    }

    function checkNameAvailability(name) {
      return OrderFilterTemplate.checkNameAvailability(name)
        .then(response => response.isNameAvailable, () => false);
    }

    function findOrdersWithCsv($event) {
      nvDialog.showSingle($event, {
        templateUrl: 'views/container/order/dialog/csv-find-orders/csv-find-orders.dialog.html',
        theme: 'nvYellow',
        cssClass: 'order-find-by-csv',
        controller: 'OrderFindByCsvDialogController',
        controllerAs: 'ctrl',
        locals: {
          extras: null,
        },
      }).then(onSuccess);

      function onSuccess(response) {
        ctrl.possibleFilters = _.concat(ctrl.possibleFilters, ctrl.selectedFilters);
        ctrl.selectedFilters = [];
        extendESOrders(response.validOrders);
        csvOrderIds = _.map(response.validOrders, 'id');
        currentSearchMode = SEARCH_MODE.CSV;

        processOrdersTableParam({
          orders: response.validOrders,
          count: response.validOrders.length,
        });
        ctrl.ordersTableParam.refreshData();
      }
    }

    function selectedItemChange(order) {
      if (order) {
        ctrl.specificSearch.text = ctrl.specificSearch.oldText;
        ctrl.specificSearch.selectedOrder = null;
        editOrder(order);
      }
    }

    function getSelectedFilters() {
      return ctrl.selectedFilters;
    }

    function getFilterValue(filterKey) {
      return _.find(ctrl.selectedFilters, { columnName: filterKey });
    }

    function refreshOrderData(orders) {
      ctrl.page = 1;
      getOrders(currentSearchMode).then(() => {
        // process selection
        ctrl.ordersTableParam.deselect();
        ctrl.ordersTableParam.refreshData();
        _.forEach(orders, (order) => {
          const theOrder = _.find(ctrl.ordersTableParam.data, ['id', order.id]);

          if (theOrder) {
            ctrl.ordersTableParam.select(theOrder);
          }
        });
      });
    }

    async function getSellerIds() {
      const masterShippers = getFilterValue(MASTER_SHIPPER_NAME);
      if (!masterShippers) {
        return $q.resolve([]);
      }
      const masterShipperIds = _(masterShippers.selectedOptions).map('global_id').compact().value();
      return masterShipperIds.length > 0
        ? Shippers.readSellersByIds(masterShipperIds, { time: moment().unix() })
        : $q.resolve([]);
    }

    function getOrdersValidationErrorData(orders, action) {
      const ordersValidationErrorData = {
        action: action,
        errors: [],
      };

      _.forEach(orders, (order) => {
        if (action === ORDER_VALIDATION_ACTIONS.RTS) {
          const isCorrectStatusToRts = Order.isCorrectStatusToRTS(order, null);

          if (order.rts || !isCorrectStatusToRts) {
            ordersValidationErrorData.errors.push(
              generateOrderValidationErrorData(order, 'container.order.list.invalid-status-to-change')
            );
          }
        } else if (action === ORDER_VALIDATION_ACTIONS.CANCEL) {
          if (Order.isStatusEnum(order, Order.STATUS.CANCELLED)) {
            ordersValidationErrorData.errors.push(
              generateOrderValidationErrorData(order, 'container.order.list.invalid-status-to-change')
            );
          }
        } else if (action === ORDER_VALIDATION_ACTIONS.RESUME) {
          if (!Order.isStatusEnum(order, Order.STATUS.CANCELLED)) {
            ordersValidationErrorData.errors.push(
              generateOrderValidationErrorData(order, 'container.order.list.invalid-status-to-change')
            );
          }
        } else if (action === ORDER_VALIDATION_ACTIONS.PULL_FROM_ROUTE) {
          const transactionsData = generatePullFromRouteTransactionsData([order]);
          if (_.size(transactionsData) <= 0) {
            ordersValidationErrorData.errors.push(
              generateOrderValidationErrorData(order, 'container.order.list.invalid-status-to-pull-from-route')
            );
          }
        } else if (action === ORDER_VALIDATION_ACTIONS.COMPLETE) {
          if (Order.isStatusEnum(order, Order.STATUS.COMPLETED)) {
            ordersValidationErrorData.errors.push(
              generateOrderValidationErrorData(order, 'container.order.list.invalid-status-to-change')
            );
          }
        }
      });

      function generateOrderValidationErrorData(order, errorMessage) {
        return {
          id: order.id,
          trackingId: order.trackingId,
          errorMessage: errorMessage,
        };
      }

      return ordersValidationErrorData;
    }

    function showSelectionErrorDialog(orders, ordersValidationErrorData, $event, fn) {
      nvDialog.showSingle($event, {
        templateUrl: 'views/container/order/dialog/selection-error/selection-error.dialog.html',
        theme: 'nvRed',
        cssClass: 'order-selection-error',
        controller: 'OrderSelectionErrorDialogController',
        controllerAs: 'ctrl',
        locals: {
          ordersValidationErrorData: ordersValidationErrorData,
        },
      }).then(success);

      function success() {
        const validOrders = _.differenceBy(orders, ordersValidationErrorData.errors, 'id');

        ctrl.ordersTableParam.deselect();
        _.forEach(validOrders, (order) => {
          const theOrder = _.find(ctrl.ordersTableParam.data, ['id', order.id]);

          if (theOrder) {
            ctrl.ordersTableParam.select(theOrder);
          }
        });

        if (validOrders.length <= 0) {
          nvToast.error(
            nvTranslate.instant('commons.unable-to-apply-action'),
            nvTranslate.instant('commons.no-valid-selection')
          );
        } else {
          fn($event, validOrders);
        }
      }
    }

    function getPullFromRouteTransactionType(order) {
      switch (order._granularStatus) {
        case Order.GRANULAR_STATUS.PENDING_PICKUP:
        case Order.GRANULAR_STATUS.VAN_ENROUTE_TO_PICKUP:
          return Transaction.getTypeEnum(Transaction.TYPE.PICKUP);
        case Order.GRANULAR_STATUS.ARRIVED_AT_SORTING_HUB:
        case Order.GRANULAR_STATUS.ARRIVED_AT_ORIGIN_HUB:
        case Order.GRANULAR_STATUS.ON_VEHICLE_FOR_DELIVERY:
        case Order.GRANULAR_STATUS.ENROUTE_TO_SORTING_HUB:
          return Transaction.getTypeEnum(Transaction.TYPE.DELIVERY);
        default:
          return null;
      }
    }

    function generatePullFromRouteTransactionsData(orders) {
      const transactionsData = [];
      _.forEach(orders, (order) => {
        const transactionType = getPullFromRouteTransactionType(order);
        if (transactionType === null) {
          return;
        }

        transactionsData.push({
          orderId: order.id,
          granularStatus: order.granularStatus,
          trackingId: order.trackingId,
          routeId: 0,
          type: transactionType,
          status: null, // transaction status unknown
        });
      });

      return transactionsData;
    }

    function printPod($event, orders) {
      const LIMIT = 200;
      let requests = orders;
      if (_.size(orders) > LIMIT) {
        requests = _.take(orders, LIMIT);
        nvToast.error(nvTranslate.instant('container.order.edit.pod-more-than-limit', { limit: LIMIT }));
      }
      nvDialog.showSingle($event, {
        templateUrl: 'views/container/order/dialog/print-pod/print-pod.dialog.html',
        theme: 'nvBlue',
        cssClass: 'order-print-pod',
        controller: 'OrderPrintPodDialogController',
        controllerAs: 'ctrl',
        locals: {
          orderData: requests,
        },
      }).then((code) => {
        if (code === 0) {
          ctrl.ordersTableParam.deselect();
        }
      });
    }

    function downloadSelectedAsCsv() {
      const selected = ctrl.ordersTableParam.getSelection();
      nvCsvParser.unparse({
        fields: CSV_HEADER,
        data: _.map(selected, el => _.toArray(_.pick(el, field))),
      }).then((csv) => {
        nvFileUtils.downloadCsv(csv, `tracking-id_${nvDateTimeUtils.displayDateTime(moment())}.csv`);
      });
    }

    function isCsvDownloadEnabled() {
      return getSelectedCount() > 0;
    }
  }
}());
