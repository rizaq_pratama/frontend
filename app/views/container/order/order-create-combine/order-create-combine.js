(function controller() {
  angular
    .module('nvOperatorApp.controllers')
    .controller('OrderCreateCombineController', OrderCreateCombineController);

  const SAMPLE_CSV = [{ 'SHIPPER ID': 235,
                        'ORDER NO': 998822112,
                        'SHIPPER ORDER NO': 'HZ-901-14',
                        'ORDER TYPE': 'C2C',
                        'TO FIRST NAME*': 'Jon',
                        'TO LAST NAME': 'Snow',
                        'TO CONTACT*': 990032122,
                        'TO EMAIL': 'jonsnow@thewall.com',
                        'TO ADDRESS 1*': 'JALAN DART 13/22',
                        'TO ADDRESS 2': 'SEKSYEN 13',
                        'TO POSTCODE': 40100,
                        'TO DISTRICT': 'SELANGOR KL',
                        'TO CITY': 'SHAH ALAM KL',
                        'TO STATE/PROVINCE': 'DISTRICT ABC',
                        'TO COUNTRY': 'SG',
                        'PARCEL SIZE': 1,
                        WEIGHT: 1200,
                        LENGTH: 2,
                        WIDTH: 3,
                        HEIGHT: 12,
                        'DELIVERY DATE': '2016-10-01',
                        'DELIVERY TIMEWINDOW ID': -1,
                        'MAX DELIVERY DAYS': 3,
                        'PICKUP DATE': '2016-10-01',
                        'PICKUP TIMEWINDOW ID': 2,
                        'PICKUP WEEKEND': 'TRUE',
                        'DELIVERY WEEKEND': 'TRUE',
                        'PICKUP INSTRUCTION': 'PICKUP ON TIME PLEASE',
                        'DELIVERY INSTRUCTION': 'DELIVER ON TIME PLEASE',
                        'COD VALUE': 90,
                        'INSURED VALUE': 32.9,
                        'FROM FIRST NAME*': 'Jason Bourne',
                        'FROM LAST NAME': 'The Bourne Collection',
                        'FROM CONTACT*': '',
                        'FROM EMAIL': 'gman@ninjavan.co',
                        'FROM ADDRESS 1*': 'blk 161 hougang st 32',
                        'FROM ADDRESS 2': '#08-10',
                        'FROM POSTCODE': 530161,
                        'FROM DISTRICT': 'sg',
                        'FROM CITY': 'sg',
                        'FROM STATE/PROVINCE': 'sg',
                        'FROM COUNTRY': 'sg',
                        'MULTI PARCEL REF NO': 'A',
                      }];
  const SAMPLE_CSV_HEADER = ['SHIPPER ID', 'ORDER NO', 'SHIPPER ORDER NO', 'ORDER TYPE', 'TO FIRST NAME*', 'TO LAST NAME', 'TO CONTACT*', 'TO EMAIL',
                            'TO ADDRESS 1*', 'TO ADDRESS 2', 'TO POSTCODE', 'TO DISTRICT', 'TO CITY', 'TO STATE/PROVINCE', 'TO COUNTRY', 'PARCEL SIZE',
                            'WEIGHT', 'LENGTH', 'WIDTH', 'HEIGHT', 'DELIVERY DATE', 'DELIVERY TIMEWINDOW ID', 'MAX DELIVERY DAYS', 'PICKUP DATE',
                            'PICKUP TIMEWINDOW ID', 'PICKUP WEEKEND', 'DELIVERY WEEKEND', 'PICKUP INSTRUCTION', 'DELIVERY INSTRUCTION', 'COD VALUE',
                            'INSURED VALUE', 'FROM FIRST NAME*', 'FROM LAST NAME', 'FROM CONTACT*', 'FROM EMAIL', 'FROM ADDRESS 1*', 'FROM ADDRESS 2',
                            'FROM POSTCODE', 'FROM DISTRICT', 'FROM CITY', 'FROM STATE/PROVINCE', 'FROM COUNTRY', 'MULTI PARCEL REF NO'];

  const DATA_COLUMNS = ['handle', 'status', 'message', 'order_ref_no', 'tracking_id'];
  const ERROR_COLUMNS = ['line', 'message'];

  OrderCreateCombineController.$inject = ['nvButtonFilePickerType', 'nvFileSelectDialog', 'nvDialog', '$translate', 'Order', 'nvTableUtils'];

  function OrderCreateCombineController(nvButtonFilePickerType,
    nvFileSelectDialog,
    nvDialog,
    $translate,
    Order,
    nvTableUtils) {
    const ctrl = this;
    ctrl.data = [];
    ctrl.dataTableParam = null;
    ctrl.error = [];
    ctrl.errorTableParam = null;
    ctrl.create = createOrder;

    ctrl.SAMPLE_CSV = SAMPLE_CSV;
    ctrl.SAMPLE_CSV_HEADER = SAMPLE_CSV_HEADER;

        // ////////////////////////////////////////////////

    function createOrder($event) {
      nvFileSelectDialog.show($event, {
        title: $translate.instant('container.order.create.upload-order-csv'),
        fileSelect: {
          accept: nvButtonFilePickerType.EXCEL_AND_CSV,
          maxFileCount: 1,
        },
        buttonOkTitle: $translate.instant('commons.submit'),
        onFinishSelect: onFinishSelect,
        onDone: onDone,
      });
    }
    function onFinishSelect(files) {
      return Order.createCombineByCSV(files)
            .then((data) => {
              reset();
              if (angular.isArray(data)) {
                ctrl.data = data;
                ctrl.dataTableParam = nvTableUtils.generate(data, { columns: DATA_COLUMNS });
              } else {
                ctrl.error = _.map(parse(data), (msg, line) => ({
                  line: line,
                  message: msg || [],
                }));
                ctrl.errorTableParam = nvTableUtils.generate(ctrl.error,
                  { columns: ERROR_COLUMNS });
              }
              function parse(str) {
                try {
                  return JSON.parse(str);
                } catch (e) {
                  return {};
                }
              }
            });
      function reset() {
        ctrl.data = [];
        ctrl.error = [];
        ctrl.dataTableParam = null;
        ctrl.errorTableParam = null;
      }
    }
    function onDone() {

    }
  }
}());
