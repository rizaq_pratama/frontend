(function controller() {
  angular
    .module('nvOperatorApp.controllers')
    .controller('ShipperRefMetadataViewController', ShipperRefMetadataViewController);

  ShipperRefMetadataViewController.$inject = [
    'shipperRefMetadata',
  ];

  function ShipperRefMetadataViewController(
    shipperRefMetadata
  ) {
    // variables
    const ctrl = this;
    ctrl.shipperRefMetadata = shipperRefMetadata;
  }
}());
