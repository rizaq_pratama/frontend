(function controller() {
  angular
    .module('nvOperatorApp.controllers')
    .controller('OrderCreateV4Controller', OrderCreateV4Controller);

  OrderCreateV4Controller.$inject = [
    'nvTranslate', 'nvFileSelectDialog', 'Excel', 'Order', '$q',
    '$interval', 'nvTable', 'nvAddress', 'nvTimezone', 'nvDialog',
    'Shippers', 'nvToast', '$timeout',
  ];

  const NUMBER_PAYLOAD = ['shipper_id', 'insured_value', 'cash_on_delivery',
    'weight', 'volume', 'length', 'width', 'height',
  ];
  const BOOLEAN_PAYLOAD = [
    'is_pickup_required',
    'allow_weekend_delivery',
    'allow_doorstep_dropoff',
    'enforce_delivery_verification',
  ];

  function OrderCreateV4Controller(
    nvTranslate, nvFileSelectDialog, Excel, Order, $q,
    $interval, nvTable, nvAddress, nvTimezone, nvDialog,
    Shippers, nvToast, $timeout
  ) {
    const DEFAULT_BATCH_STATUS = {
      is_uploaded: false,
      is_status_checking: false,
      orders: [],
      successful_batch_response: {
        page: 1,
        value: {},
        total_items: 0,
      },

      batch_id: 0,
      total_number_of_orders: 0,
      number_of_processing_orders: 0,
      number_of_failed_orders: 0,
      number_of_successful_orders: 0,
      processing_orders_async_uuids: [],
      failed_orders_async_uuids: [],
      successful_orders_async_uuids: [],
    };
    const VALIDATE_SELLER_MAX_ATTEMPT = 3;
    const MAX_CALL_IN_CHUNK = 10; // cannot send too much request to backend in one shot
    const SUBMIT_ORDER_MODE = {
      DEFAULT: 'default', // just send in all orders to OC
      CREATE_MARKETPLACE_SELLER: 'create-marketplace-seller', // try to create seller first
    };
    let batchStatusIntervalPromise;

    const ctrl = this;
    ctrl.backdropLoading = false;
    ctrl.batchStatus = DEFAULT_BATCH_STATUS;

    // functions
    ctrl.downloadSampleCsv = showSampleWizard;
    ctrl.create = createOrder;
    ctrl.viewDetails = viewDetails;
    ctrl.loadMore = loadMore;

    // functions details
    function showSampleWizard($event) {
      nvDialog.showSingle($event, {
        templateUrl: 'views/container/order/order-create-v4/dialog/preset-sample.dialog.html',
        cssClass: 'ocv4-preset-sample',
        controller: 'OrderCreateV4PresetController',
        controllerAs: 'ctrl',
      }).then((result) => {
        if (_.isArray(result)) {
          downloadSampleCsv(result);
        }
      });
    }

    function downloadSampleCsv(result) {
      const header = [];
      const content = [];

      _.forEach(result, (row, index) => {
        _.forEach(row, (value, key) => {
          if (index === 0) {
            header.push(key);
          }

          content.push(value);
        });
      });

      const payload = _.head(result);
      const orderType = _.get(payload, 'service_type');
      const shipperId = _.get(payload, 'shipper_id');
      const baseFileName = `sample-ocv4-${shipperId}`;
      let fileName = '';

      switch (orderType) {
        case 'parcel':
          fileName = `${baseFileName}-parcel-${moment()}`;
          break;
        case 'marketplace':
          fileName = `${baseFileName}-marketplace-${moment()}`;
          break;
        case 'return':
          fileName = `${baseFileName}-return-${moment()}`;
          break;
        case 'international':
          fileName = `${baseFileName}-international-${moment()}`;
          break;
        default:
          fileName = `${baseFileName}-unknown-${moment()}`;
          break;
      }

      Excel.write([header, content], `${fileName}.xlsx`, true);
    }

    function createOrder($event) {
      nvFileSelectDialog.show($event, {
        title: nvTranslate.instant('container.order.create.upload-order-csv'),
        fileSelect: {
          accept: '.csv,.xlsx,.xls',
          maxFileCount: 1,
        },
        buttonOkTitle: nvTranslate.instant('commons.submit'),
        onFinishSelect: onFinishSelect,
      });

      function onFinishSelect(files) {
        let excelResponse = {
          cols: [],
          data: [],
        };
        const v4SendOrderToBatchPayloads = [];

        Excel.read(files[0]).then(excelReadSuccess);

        function excelReadSuccess(response) {
          excelResponse = response;

          // create batch
          ctrl.backdropLoading = true;
          Order.v4CreateBatch().then(createBatchSuccess, createBatchFailure);
        }

        function createBatchSuccess(response) {
          // process json data and send to oc
          _.forEach(excelResponse.data, (row) => {
            if (_.size(row) <= 0) {
              return;
            }

            let order = processOrderPayload(row);
            const shipperId = getShipperId(order);

            // add payload that is missing
            order = _.defaultsDeep(order, {
              parcel_job: {
                delivery_timeslot: {
                  timezone: nvTimezone.getOperatorTimezone(),
                },
              },
            });

            // delete custom payload
            delete order.shipper_id;

            v4SendOrderToBatchPayloads.push({
              shipperId: shipperId,
              isSubmitted: false,
              order: order,
            });
          });

          // send order to batch
          sendOrdersToBatch(response.batch_id, v4SendOrderToBatchPayloads);

          function getShipperId(theOrder) {
            return _.toInteger(theOrder && theOrder.shipper_id);
          }
        }

        function createBatchFailure() {
          ctrl.backdropLoading = false;
        }

        function processOrderPayload(row) {
          /*
           * if row = {'a.b': 1, 'c': 2}
           * payload wil be {a: {b: 1}, c: 2}
           */
          const payload = {
            internal_ref: {
              source_id: Order.SOURCE_ID.CSV,
            },
          };

          _.forEach(row, (value, key) => {
            const splitKeys = _.split(key, '.');
            const strVal = _.toString(value);
            _.reduce(splitKeys, (thePayload, theKey, theIndex) => {
              if ((_.size(splitKeys) - 1) === theIndex) {
                // initially, strVal always in string, then convert to proper type
                if (isNumberKey(theKey)) {
                  if (theKey.length > 0) {
                    thePayload[theKey] = _.toNumber(strVal);
                  } else {
                    // empty string goes 0, better make it null
                    thePayload[theKey] = null;
                  }
                } else if (isBooleanKey(theKey)) {
                  thePayload[theKey] = _.toLower(strVal) === 'true';
                } else {
                  thePayload[theKey] = stripEmoji(strVal) || null;
                }
              }

              if (_.isUndefined(thePayload[theKey])) {
                thePayload[theKey] = {};
              }

              return thePayload[theKey];
            }, payload);
          });

          // extra clean up
          if (_.get(payload, 'parcel_job.is_pickup_required', false) === false) {
            _.unset(payload, 'parcel_job.pickup_date');
            _.unset(payload, 'parcel_job.pickup_service_type');
            _.unset(payload, 'parcel_job.pickup_service_level');
            _.unset(payload, 'parcel_job.pickup_address');
            _.unset(payload, 'parcel_job.pickup_timeslot');
          }
          return payload;
        }

        function stripEmoji(string) {
          // Expected payload return to strip out the emoji
          // Eg. APPLE*smile emoji*TEST*frown emoji* to return APPLETEST
          return string.replace(/(?:\u32E1|\u32DB|\u263A|\u2639|\u263B|\u3020|[\u2700-\u27bf]|(?:\ud83c[\udde6-\uddff]){2}|[\ud800-\udbff][\udc00-\udfff]|[\u0023-\u0039]\ufe0f?\u20e3|\u3299|\u3297|\u303d|\u3030|\u24c2|\ud83c[\udd70-\udd71]|\ud83c[\udd7e-\udd7f]|\ud83c\udd8e|\ud83c[\udd91-\udd9a]|\ud83c[\udde6-\uddff]|\ud83c\ude01|\ud83c\uddff]|\ud83c[\ude01-\ude02]|\ud83c\ude1a|\ud83c\ude2f|\ud83c\ude32|\ud83c\ude02|\ud83c\ude1a|\ud83c\ude2f|\ud83c[\ude32-\ude3a]|\ud83c\ude50|\ud83c\ude3a|\ud83c[\ude50-\ude51]|\u203c|\u2049|[\u25aa-\u25ab]|\u25b6|\u25c0|[\u25fb-\u25fe]|\u00a9|\u00ae|\u2122|\u2139|\ud83c\udc04|[\u2600-\u26FF]|\u2b05|\u2b06|\u2b07|\u2b1b|\u2b1c|\u2b50|\u2b55|\u231a|\u231b|\u2328|\u23cf|[\u23e9-\u23f3]|[\u23f8-\u23fa]|\ud83c\udccf|\u2934|\u2935|[\u2190-\u21ff])/g, '');
        }

        function isNumberKey(key) {
          return _.find(NUMBER_PAYLOAD, k => k === key);
        }

        function isBooleanKey(key) {
          return _.find(BOOLEAN_PAYLOAD, k => k === key);
        }
      }
    }

    function viewDetails($event, mode) {
      nvDialog.showSingle($event, {
        templateUrl: 'views/container/order/dialog/order-create-uuids/order-create-uuids.dialog.html',
        cssClass: 'order-create-uuids',
        controller: 'OrderCreateUUIDsDialogController',
        controllerAs: 'ctrl',
        locals: {
          batchStatus: ctrl.batchStatus,
          mode: mode,
        },
      });
    }

    function loadMore() {
      return ctrl.successOrdersTableParam.mergeInPromise(loadMoreOrders(), 'id');

      function loadMoreOrders() {
        if (ctrl.successOrdersTableParam.getUnfilteredLength() <
          ctrl.batchStatus.successful_batch_response.total_items) {
          ctrl.batchStatus.successful_batch_response.page += 1;
          return getSuccessfulOrdersPromise(ctrl.batchStatus.batch_id).then(success);
        }
        return $q.reject();
      }

      function success(response) {
        return extendSuccessfulOrders(response.value) || [];
      }
    }

    function sendOrdersToBatch(batchId, allPayloads) {
      let chunkPayloads;
      let checkIfSellersExistAttempt = 0;
      const uniqShipperToMarketplaceSellers = [];

      // before run the chunk, have to check if shipper is exist
      const shipperIds = _.compact(_.uniq(_.map(allPayloads, 'shipperId')));
      $q.all(generateReadShipperLegacyEndpoints(shipperIds))
        .then(readShipperLegacySuccess, readShipperFailure);

      function generateReadShipperLegacyEndpoints(theShipperIds) {
        const endpoints = [];
        _.forEach(theShipperIds, shipperId =>
            endpoints.push(Shippers.readShipperLegacy(shipperId))
        );

        return endpoints;
      }

      function readShipperLegacySuccess(response) {
        const shipperByLegacyId = _.keyBy(response, 'legacy_id');

        // extend allPayloads
        _.forEach(allPayloads, (payload) => {
          payload.globalId = shipperByLegacyId[payload.shipperId] ?
            shipperByLegacyId[payload.shipperId].id : null;
        });

        // now, check if seller exist
        // if not exist, then have to send one order to create
        checkIfSellersExist(true);
      }

      function readShipperFailure() {
        ctrl.backdropLoading = false;
      }

      function checkIfSellersExist(proceedToCreateIfNotExist = true) {
        _.forEach(allPayloads, (payload) => {
          const marketplaceSellerId = _.get(payload, 'order.marketplace.seller_id');
          if (_.isUndefined(marketplaceSellerId)) {
            return;
          }

          const theMarketplaceSeller = _.find(
            uniqShipperToMarketplaceSellers, uniqShipperToMarketplaceSeller =>
              uniqShipperToMarketplaceSeller.shipperId === payload.shipperId &&
              uniqShipperToMarketplaceSeller.externalRef === marketplaceSellerId
          );

          if (_.isUndefined(theMarketplaceSeller)) {
            uniqShipperToMarketplaceSellers.push({
              globalId: payload.globalId,
              shipperId: payload.shipperId, // this is legacy id
              externalRef: marketplaceSellerId,
              isExistInDB: null,
            });
          } else if (theMarketplaceSeller.isExistInDB === false) {
            theMarketplaceSeller.isExistInDB = null;
          }
        });

        checkIfSellersExistAttempt += 1;

        _.forEach(uniqShipperToMarketplaceSellers, (uniqShipperToMarketplaceSeller) => {
          if (uniqShipperToMarketplaceSeller.isExistInDB === null) {
            readSellers(uniqShipperToMarketplaceSeller);
          }
        });

        postValidateSellers();

        // functions
        function readSellers(uniqShipperToMarketplaceSeller) {
          Shippers.readSellers(uniqShipperToMarketplaceSeller.globalId, {
            external_ref: uniqShipperToMarketplaceSeller.externalRef,
          }).then(readSellersSuccess, readSellersFailure);

          function readSellersSuccess() {
            uniqShipperToMarketplaceSeller.isExistInDB = true;

            postValidateSellers();
          }

          function readSellersFailure() {
            uniqShipperToMarketplaceSeller.isExistInDB = false;
            nvToast.remove();

            postValidateSellers();
          }
        }

        function postValidateSellers() {
          const requestInProcess = _.filter(uniqShipperToMarketplaceSellers, row =>
            row.isExistInDB === null
          );

          if (_.size(requestInProcess) <= 0) {
            if (proceedToCreateIfNotExist ||
              checkIfSellersExistAttempt >= VALIDATE_SELLER_MAX_ATTEMPT) {
              checkIfSellersExistAttempt = 0;
              createSellersIfNotExist();
            } else {
              $timeout(() => {
                // validate again
                checkIfSellersExist(false);
              }, 1000);
            }
          }
        }

        function createSellersIfNotExist() {
          const nonExistMarketplaceSellers = _.filter(uniqShipperToMarketplaceSellers, row =>
            row.isExistInDB === false
          );

          if (_.size(nonExistMarketplaceSellers) > 0) {
            creatingSellers(nonExistMarketplaceSellers);
            return;
          }

          // all sellers are exist, proceed in normal way
          generateChunkPayloadsAndRun();
        }

        function creatingSellers(nonExistMarketplaceSellers) {
          const payloadsToSubmit = [];

          // find order that is not submitted yet and by externalRef and shipperId
          // creating the order will automatically creating new marketplace seller
          _.forEach(nonExistMarketplaceSellers, (marketplaceSeller) => {
            const payloadToSubmit = _.find(allPayloads, payload =>
              payload.isSubmitted === false &&
              payload.shipperId === marketplaceSeller.shipperId &&
                _.get(payload, 'order.marketplace.seller_id') === marketplaceSeller.externalRef
            );

            if (payloadToSubmit) {
              payloadsToSubmit.push(payloadToSubmit);
            }
          });

          chunkPayloads = _.chunk(payloadsToSubmit, MAX_CALL_IN_CHUNK);

          if (_.size(chunkPayloads) <= 0) {
            // no more order to submit to create marketplace seller
            // so just send order in by default mode
            generateChunkPayloadsAndRun();
            return;
          }

          run(chunkPayloads, SUBMIT_ORDER_MODE.CREATE_MARKETPLACE_SELLER);
        }
      }

      function generateChunkPayloadsAndRun() {
        const payloadsToSubmit = _.filter(allPayloads, payload =>
          payload.isSubmitted === false
        );
        chunkPayloads = _.chunk(payloadsToSubmit, MAX_CALL_IN_CHUNK);
        run(chunkPayloads);
      }

      function run(theChunkPayloads, mode = SUBMIT_ORDER_MODE.DEFAULT) {
        let chunkToProcess;

        if (_.size(theChunkPayloads) > 0) {
          chunkToProcess = theChunkPayloads.splice(0, 1)[0];
          const promises = [];
          _.forEach(chunkToProcess, (payload) => {
            promises.push(Order.v4SendOrderToBatch(batchId, payload.shipperId, payload.order));
          });

          $q.all(promises).then(success, failure);
          return;
        }

        switch (mode) {
          case SUBMIT_ORDER_MODE.DEFAULT:
            ctrl.batchStatus.batch_id = batchId;
            ctrl.batchStatus.orders = _.map(allPayloads, payload => payload.order);

            if (batchStatusIntervalPromise) {
              $interval.cancel(batchStatusIntervalPromise);
            }
            batchStatusIntervalPromise = $interval(getBatchStatus, 1000);
            break;
          case SUBMIT_ORDER_MODE.CREATE_MARKETPLACE_SELLER:
            $timeout(() => {
              // validate if all marketplace seller is created
              checkIfSellersExist(false);
            }, 1000);
            break;
          default:
            return;
        }

        function success() {
          updateToSubmitted();
          run(chunkPayloads, mode);
        }

        function failure() {
          updateToSubmitted();
          run(chunkPayloads, mode);
        }

        function updateToSubmitted() {
          _.forEach(chunkToProcess, (payload) => {
            payload.isSubmitted = true;
          });
        }
      }
    }

    function getBatchStatus() {
      if (ctrl.batchStatus.is_status_checking) {
        return;
      }

      ctrl.batchStatus.is_status_checking = true;
      Order.v4GetBatchStatus(ctrl.batchStatus.batch_id).then(success, failure);

      function success(response) {
        ctrl.batchStatus = _.defaultsDeep(response, DEFAULT_BATCH_STATUS);

        $timeout(() => {
          ctrl.batchStatus.is_uploaded = true;
          ctrl.batchStatus.is_status_checking = false;
          ctrl.backdropLoading = false;

          if (ctrl.batchStatus.number_of_processing_orders <= 0) {
            getSuccessfulOrders(ctrl.batchStatus.batch_id);

            $interval.cancel(batchStatusIntervalPromise);
          }
        });
      }

      function failure() {
        ctrl.backdropLoading = false;
        ctrl.batchStatus.is_status_checking = false;
        $interval.cancel(batchStatusIntervalPromise);
      }
    }

    function getSuccessfulOrdersPromise(batchId) {
      const params = {
        page: ctrl.batchStatus.successful_batch_response.page,
      };

      return Order.v4GetSuccessfulOrdersFromBatch(batchId, params);
    }

    function getSuccessfulOrders(batchId) {
      ctrl.successOrdersTableParam = nvTable.createTable();
      ctrl.successOrdersTableParam.addColumn('tracking_number', { displayName: 'commons.tracking-id' });
      ctrl.successOrdersTableParam.addColumn('service_level', {
        displayName: 'container.order.create.service-level',
      });
      ctrl.successOrdersTableParam.addColumn('from.name', { displayName: 'commons.from-name' });
      ctrl.successOrdersTableParam.addColumn('from_address', { displayName: 'commons.from-address' });
      ctrl.successOrdersTableParam.addColumn('to.name', { displayName: 'commons.to-name' });
      ctrl.successOrdersTableParam.addColumn('to_address', { displayName: 'commons.to-address' });
      ctrl.successOrdersTableParam.addColumn('parcel_job.delivery_start_date', {
        displayName: 'container.order.create.delivery-start-date',
      });
      ctrl.successOrdersTableParam.addColumn('delivery_timeslot_time', {
        displayName: 'container.order.create.delivery-timeslot',
      });

      getSuccessfulOrdersPromise(batchId).then(success, $q.reject);

      function success(response) {
        ctrl.batchStatus.successful_batch_response = _.defaultsDeep(
          response, DEFAULT_BATCH_STATUS.successful_batch_response
        );
        ctrl.successOrdersTableParam.setData(extendSuccessfulOrders(response.value));
      }
    }

    function extendSuccessfulOrders(orders) {
      return _.map(orders, order => (
        setCustomOrderData(order)
      ));

      function setCustomOrderData(order) {
        order.from_address = '';
        if (order.from && order.from.address) {
          order.from_address = nvAddress.extract(order.from.address);
        }

        order.to_address = '';
        if (order.to && order.to.address) {
          order.to_address = nvAddress.extract(order.to.address);
        }

        order.delivery_timeslot_time = '';
        if (order.parcel_job && order.parcel_job.delivery_timeslot) {
          order.delivery_timeslot_time = _.compact([
            order.parcel_job.delivery_timeslot.start_time,
            order.parcel_job.delivery_timeslot.end_time,
          ]).join(' - ');
        }

        return order;
      }
    }
  }
}());
