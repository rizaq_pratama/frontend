(function controller() {
  angular
    .module('nvOperatorApp.controllers')
    .controller('OrderCreateV4PresetController', OrderCreateV4PresetController);

  OrderCreateV4PresetController.$inject = ['Timewindow', '$mdDialog', 'nvTimezone', 'nvDateTimeUtils',
    'Shippers', 'Reservation', 'nvDomain', 'nvAutocomplete.Data', '$scope'];

  const STEP_NAME = {
    SERVICE_TYPE: 'service-type',
    SERVICE_LEVEL: 'service-level',
    DELIVERY: 'delivery',
    PICKUP: 'pickup',
    MARKETPLACE: 'marketplace',
    CASH: 'cash',
  };

  const SERVICE_TYPE_OPTIONS = [
    { displayName: 'Parcel', value: 'parcel' },
    { displayName: 'Marketplace', value: 'marketplace' },
    { displayName: 'Return', value: 'return' },
    { displayName: 'International', value: 'international' },
  ];

  const SERVICE_LEVEL_OPTIONS = [
    { displayName: 'Standard (3 day)', value: 'standard' },
    { displayName: 'Express (2 day)', value: 'express' },
    { displayName: 'Next Day (1 day)', value: 'nextday' },
    { displayName: 'Same Day', value: 'sameday' },
  ];

  const CASH_JOB_TYPE_OPTIONS = [
    { displayName: 'Receive (driver will receive the money upon transactions)', value: 'Receive' },
    { displayName: 'Send (driver will pass the money upon transactions)', value: 'Send' },
  ];

  const CASH_JOB_TXN_AT_OPTIONS = [
    { displayName: 'Delivery', value: 'Delivery' },
    { displayName: 'Pickup', value: 'Pickup' },
  ];

  function OrderCreateV4PresetController(Timewindow, $mdDialog, nvTimezone, nvDateTimeUtils,
    Shippers, Reservation, nvDomain, nvAutocompleteData, $scope) {
    const ctrl = this;

    const TIMEWINDOW_OPTIONS = Timewindow.toOptions();

    const PICKUP_SERVICE_TYPE_OPTIONS = _.filter(Shippers.pickupServiceTypeToOptions(), st => _.toLower(st.value) === 'scheduled');
    const PICKUP_SERVICE_LEVEL_OPTIONS = Shippers.pickupServiceLevelToOptions();
    const RESERVATION_VOLUME_OPTIONS = _.map(Reservation.volumeOptions, val => ({
      displayName: val,
      value: val,
    }));

    const STEP_SERVICE_TYPE = {
      name: STEP_NAME.SERVICE_TYPE,
      options: SERVICE_TYPE_OPTIONS,
      selectedServiceType: _.head(SERVICE_TYPE_OPTIONS).value,
      selectedShipper: null,
      callback: (serviceType, shipper, isStampShipper) => {
        let trackingIdPath = 'requested_tracking_number';
        if (isStampShipper) {
          trackingIdPath = 'internal_ref.stamp_id';
        }
        return [
          { path: 'shipper_id', value: shipper.id },
          { path: 'service_type', value: serviceType },
          { path: trackingIdPath, value: '[sample] 0010--002 [sample]' },
        ];
      },
    };

    const STEP_SERVICE_LEVEL = {
      name: STEP_NAME.SERVICE_LEVEL,
      options: SERVICE_LEVEL_OPTIONS,
      selected: _.head(SERVICE_LEVEL_OPTIONS).value,
      callback: (value) => {
        const basePayload = [
          { path: 'service_level', value: value },
          { path: 'reference.merchant_order_number', value: '[sample] SHIP-0001 [shipper side reference] [optional] [sample]' },
        ];
        const fromAddresses = generateAddressFields('from');
        const toAddresses = generateAddressFields('to');
        return _.concat(basePayload, fromAddresses, toAddresses);
      },
    };

    const STEP_DELIVERY = {
      name: STEP_NAME.DELIVERY,
      options: TIMEWINDOW_OPTIONS,
      selected: _.get(_.find(TIMEWINDOW_OPTIONS, tw => tw.value === -1), 'value', _.head(TIMEWINDOW_OPTIONS).value), // get all day as default
      date: new Date(),
      callback: (
        date,
        timewindowId,
        isAllowDoorstepDropoff,
        isEnforceDeliveryVerification
      ) => {
        const timewindow = _.find(Timewindow.TIMEWINDOWS, tw => tw.id === timewindowId);
        const tz = nvTimezone.getOperatorTimezone();

        const payload = [
          { path: 'parcel_job.delivery_start_date', value: nvDateTimeUtils.displayDate(date) },
          { path: 'parcel_job.delivery_timeslot.start_time', value: timewindow.fromTime.format('HH:mm') },
          { path: 'parcel_job.delivery_timeslot.end_time', value: timewindow.toTime.format('HH:mm') },
          { path: 'parcel_job.delivery_timeslot.timezone', value: tz },
          { path: 'parcel_job.delivery_instructions', value: '[sample] order delivery instructions [sample]' },
          { path: 'parcel_job.dimensions.weight', value: '[sample] 2.0 [number only] [sample]' },
        ];

        payload.push({ path: 'parcel_job.allow_doorstep_dropoff', value: isAllowDoorstepDropoff });

        if (isEnforceDeliveryVerification) {
          payload.push({ path: 'parcel_job.enforce_delivery_verification', value: true });
          payload.push({ path: 'reference.merchant_order_metadata.delivery_verification_identity', value: '[sample] IC Value [sample]' });
        }

        return payload;
      },
    };

    const STEP_PICKUP = {
      name: STEP_NAME.PICKUP,
      timewindowOptions: TIMEWINDOW_OPTIONS,
      pickupServiceTypeOptions: PICKUP_SERVICE_TYPE_OPTIONS,
      pickupServiceLevelOptions: PICKUP_SERVICE_LEVEL_OPTIONS,
      reservationVolumeOptions: RESERVATION_VOLUME_OPTIONS,

      selectedTimewindow: _.get(_.find(TIMEWINDOW_OPTIONS, tw => tw.value === -1), 'value', _.head(TIMEWINDOW_OPTIONS).value), // get all day as default
      selectedServiceType: _.head(PICKUP_SERVICE_TYPE_OPTIONS).value,

      // scheduled standard pickup as default
      selectedServiceLevel: _.last(PICKUP_SERVICE_LEVEL_OPTIONS).value,

      selectedReservationVolume: _.head(RESERVATION_VOLUME_OPTIONS).value,
      date: new Date(),

      callback: (
        date, timewindowId, startTime, endTime, serviceType, serviceLevel, reservationVolume
      ) => {
        const timewindow = _.find(Timewindow.TIMEWINDOWS, tw => tw.id === timewindowId);
        const startMoment = serviceLevel === 'Premium' ? startTime.clone() : timewindow.fromTime;
        const endMoment = serviceLevel === 'Premium' ? endTime.clone() : timewindow.toTime;
        const tz = nvTimezone.getOperatorTimezone();

        const baseAddresses = [
          { path: 'parcel_job.is_pickup_required', value: true },
          { path: 'parcel_job.pickup_date', value: nvDateTimeUtils.displayDate(date) },
          { path: 'parcel_job.pickup_timeslot.start_time', value: startMoment.format('HH:mm') },
          { path: 'parcel_job.pickup_timeslot.end_time', value: endMoment.format('HH:mm') },
          { path: 'parcel_job.pickup_timeslot.timezone', value: tz },
          { path: 'parcel_job.pickup_instructions', value: '[sample] order pickup instructions [sample]' },

          { path: 'parcel_job.pickup_address_id', value: '[sample] ADDR-SHIP-001 [shipper side pickup address ref] [sample]' },
          { path: 'parcel_job.pickup_service_type', value: serviceType },
          { path: 'parcel_job.pickup_service_level', value: serviceLevel },
          { path: 'parcel_job.pickup_approximate_volume', value: reservationVolume },
        ];
        const pickupAddresses = generateAddressFields('parcel_job.pickup_address');
        return _.concat(baseAddresses, pickupAddresses);
      },
    };

    const STEP_MARKETPLACE = {
      name: STEP_NAME.MARKETPLACE,
      sellerId: null,
      companyName: null,
      callback: (sellerId, companyName) => [
        { path: 'marketplace.seller_id', value: sellerId },
        { path: 'marketplace.seller_company_name', value: companyName },
      ],
    };

    // cash_job value is from our persepective (receive means we/driver will receive the money)
    const STEP_CASH = {
      name: STEP_NAME.CASH,
      amount: 0,
      txnAtOptions: CASH_JOB_TXN_AT_OPTIONS,
      typeOptions: CASH_JOB_TYPE_OPTIONS,

      selectedTxnAt: _.head(CASH_JOB_TXN_AT_OPTIONS).value,
      selectedType: _.head(CASH_JOB_TYPE_OPTIONS).value,
      callback: (amount, txnAt, type) => [
        { path: 'cash_job.amount', value: amount },
        { path: 'cash_job.transacted_at', value: txnAt },
        { path: 'cash_job.type', value: type },
      ],
    };

    const BASE_STEPS = [STEP_SERVICE_TYPE, STEP_SERVICE_LEVEL, STEP_DELIVERY];

    ctrl.stepName = STEP_NAME;
    ctrl.onServiceTypeChanges = onServiceTypeChanges;
    ctrl.onNextStep = onNextStep;
    ctrl.onPrevStep = onPrevStep;
    ctrl.onCancel = onCancel;
    ctrl.isAbleToSubmit = isAbleToSubmit;

    init();

    function init() {
      ctrl.payload = {};
      ctrl.state = {
        isMarketplace: false,
        isReturn: false,
        isPickupRequired: false,
        isCashEnabled: false,
        isStampShipper: false,
        isAllowDoorstepDropoff: false,
        isEnforceDeliveryVerification: false,
        stepCounter: 0,
        isFinalStep: false,
        startTime: moment().hour(9).minute(0).second(0),
        endTime: moment().hour(22).minute(0).second(0),
        startTimeMin: moment().hour(9).minute(0).second(0),
        startTimeMax: moment()
          .hour(22).minute(0).second(0)
          .subtract(90, 'm'),
        endTimeMin: moment()
          .hour(9).minute(0).second(0)
          .add(90, 'm'),
        endTimeMax: moment().hour(22).minute(0).second(0),
        buttonName: 'commons.next',
      };
      ctrl.steps = _.cloneDeep(BASE_STEPS);
      ctrl.currentStep = ctrl.steps[ctrl.state.stepCounter];

      ctrl.shipperText = '';
      ctrl.shipperService = nvAutocompleteData.getByHandle('shipper-autocomplete');
      ctrl.shipperService.setPossibleOptions(
        Shippers
          .readAll()
          .then(result => _.map(result, shipper => _.defaults(shipper, { displayName: `${shipper.id}-${shipper.name}` }))
      ));

      initListener();
    }

    function onServiceTypeChanges(val) {
      _.set(ctrl.state, 'isMarketplace', val === 'marketplace');
      _.set(ctrl.state, 'isReturn', val === 'return');
    }

    function onNextStep(payloads, firstStep) {
      if (firstStep) {
        if (ctrl.state.isPickupRequired) {
          ctrl.steps.push(STEP_PICKUP);
        }
        if (ctrl.state.isMarketplace) {
          ctrl.steps.push(STEP_MARKETPLACE);
        }
        if (ctrl.state.isCashEnabled) {
          ctrl.steps.push(STEP_CASH);
        }
      }

      _.forEach(payloads, payload => (ctrl.payload[payload.path] = _.toString(payload.value)));

      ctrl.state.stepCounter += 1;
      ctrl.currentStep = ctrl.steps[ctrl.state.stepCounter];

      if (ctrl.state.isFinalStep) {
        onFinish();
        return;
      }

      if (!ctrl.state.isFinalStep && (ctrl.state.stepCounter === (_.size(ctrl.steps) - 1))) {
        ctrl.state.isFinalStep = true;
        ctrl.state.buttonName = 'commons.done';
      }
    }

    function onPrevStep() {
      ctrl.state.isFinalStep = false;
      ctrl.state.buttonName = 'commons.next';

      ctrl.state.stepCounter -= 1;
      ctrl.currentStep = ctrl.steps[ctrl.state.stepCounter];

      if (ctrl.currentStep.name === STEP_NAME.SERVICE_TYPE) {
        ctrl.steps.length = BASE_STEPS.length; // remove pushed step
        ctrl.payload = {};
      }
    }

    function onCancel() {
      $mdDialog.hide(-1);
    }

    function onFinish() {
      $mdDialog.hide([ctrl.payload]);
    }

    function generateAddressFields(prefix) {
      const country = nvDomain.getDomain().current;
      const baseAddresses = [
        { path: 'name', value: '[sample] name [sample]' },
        { path: 'email', value: '[sample] email [sample]' },
        { path: 'phone_number', value: '[sample] phone number [sample]' },
        { path: 'address.address1', value: '[sample] address [sample]' },
        { path: 'address.address2', value: '[sample] address 2 [optional] [sample]' },
      ];
      switch (country) {
        case 'id':
          baseAddresses.push({ path: 'address.kelurahan', value: '[sample] Utan Kayu Utara [sample]' });
          baseAddresses.push({ path: 'address.kecamatan', value: '[sample] Matraman [sample]' });
          baseAddresses.push({ path: 'address.city', value: '[sample] Jakarta [sample]' });
          baseAddresses.push({ path: 'address.province', value: '[sample] DKI Jakarta [sample]' });
          baseAddresses.push({ path: 'address.country', value: 'ID' });
          baseAddresses.push({ path: 'address.postcode', value: '[sample] 13120 [sample]' });
          break;
        case 'my':
          baseAddresses.push({ path: 'address.area', value: '[sample] [sample]' });
          baseAddresses.push({ path: 'address.city', value: '[sample] [sample]' });
          baseAddresses.push({ path: 'address.state', value: '[sample] [sample]' });
          baseAddresses.push({ path: 'address.country', value: 'MY' });
          baseAddresses.push({ path: 'address.postcode', value: '[sample] [sample]' });
          break;
        case 'vn':
          baseAddresses.push({ path: 'address.ward', value: '[sample] [sample]' });
          baseAddresses.push({ path: 'address.district', value: '[sample] [sample]' });
          baseAddresses.push({ path: 'address.city', value: '[sample] [sample]' });
          baseAddresses.push({ path: 'address.country', value: 'VN' });
          break;
        case 'th':
          baseAddresses.push({ path: 'address.sub-district', value: '[sample] [sample]' });
          baseAddresses.push({ path: 'address.district', value: '[sample] [sample]' });
          baseAddresses.push({ path: 'address.province', value: '[sample] [sample]' });
          baseAddresses.push({ path: 'address.country', value: 'TH' });
          baseAddresses.push({ path: 'address.postcode', value: '[sample] [sample]' });
          break;
        case 'ph':
          baseAddresses.push({ path: 'address.subdivision', value: '[sample] Barangay [sample]' });
          baseAddresses.push({ path: 'address.district', value: '[sample] [sample]' });
          baseAddresses.push({ path: 'address.city', value: '[sample] [sample]' });
          baseAddresses.push({ path: 'address.province', value: '[sample] [sample]' });
          baseAddresses.push({ path: 'address.country', value: 'PH' });
          baseAddresses.push({ path: 'address.postcode', value: '[sample] [sample]' });
          break;
        case 'mm':
          baseAddresses.push({ path: 'address.township', value: '[sample] [sample]' });
          baseAddresses.push({ path: 'address.district', value: '[sample] [sample]' });
          baseAddresses.push({ path: 'address.state', value: '[sample] [sample]' });
          baseAddresses.push({ path: 'address.country', value: 'MM' });
          baseAddresses.push({ path: 'address.postcode', value: '[sample] [sample]' });
          break;
        default:
          baseAddresses.push({ path: 'address.country', value: 'SG' });
          baseAddresses.push({ path: 'address.postcode', value: '[sample] 102600 [sample]' });
      }
      return _.map(baseAddresses, ba => _.assign(ba, { path: `${prefix}.${ba.path}` }));
    }

    function isAbleToSubmit() {
      const stepName = _.get(ctrl.currentStep, 'name', '');

      switch (stepName) {
        case STEP_NAME.SERVICE_TYPE:
          if (ctrl.currentStep.selectedShipper === null ||
            ctrl.currentStep.selectedServiceType === null) {
            return false;
          }
          return true;
        case STEP_NAME.SERVICE_LEVEL:
          return true;
        default:
          return true;
      }
    }

    function initListener() {
      $scope.$watch(() => _.toString(ctrl.state.startTime), () => {
        const m = nvDateTimeUtils.toMoment(ctrl.state.startTime);
        if (moment.isMoment(m)) {
          ctrl.state.endTimeMin = m.clone().add(90, 'm');
        }
      });
      $scope.$watch(() => _.toString(ctrl.state.endTime), () => {
        const m = nvDateTimeUtils.toMoment(ctrl.state.endTime);
        if (moment.isMoment(m)) {
          ctrl.state.startTimeMax = m.clone().subtract(90, 'm');
        }
      });
    }
  }
}());
