(function controller() {
  angular
        .module('nvOperatorApp.controllers')
        .controller('RouteCleaningReportController', RouteCleaningReportController);

  RouteCleaningReportController.$inject = ['RouteCleaningReport', 'nvTable', '$scope', '$rootScope',
    '$timeout', '$q', 'Ticketing', 'nvToast', 'Order', '$window', 'nvTranslate', '$state', 'nvCsvParser',
    'nvFileUtils', 'nvDateTimeUtils', 'nvEnv'];

  const TAB = {
    COD: '100',
    PARCEL: '010',
    RESERVATION: '001',
  };

  const TABLE_FIELDS = {
    codInbounded: { displayName: 'container.route-cleaning-report.table-cod-inbound' },
    codExpected: { displayName: 'container.route-cleaning-report.table-cod-expected' },
    routeId: { displayName: 'container.route-cleaning-report.table-route-id' },
    driverName: { displayName: 'container.route-cleaning-report.table-driver-name' },
    trackingId: { displayName: 'container.route-cleaning-report.table-tracking-id' },
    lastScanHubId: { displayName: 'container.route-cleaning-report.table-last-scan-hub-id' },
    exception: { displayName: 'container.route-cleaning-report.table-exception' },
    lastSeen: { displayName: 'container.route-cleaning-report.table-last-seen' },
    shipperName: { displayName: 'container.route-cleaning-report.table-shipper-name' },
    lastScanType: { displayName: 'container.route-cleaning-report.table-last-scan-type' },
    granularStatus: { displayName: 'container.route-cleaning-report.table-granular-status' },
  };

  /**
   * Note:
   * CSV TITLE and FIELDS must be in same order
   */
  const COD_CSV_TITLES = ['Route Date', 'COD Inbounded', 'COD Expected', 'Route ID',
    'Driver Name',
  ];
  const PARCEL_CSV_TITLES = ['Date', 'Tracking ID', 'Granular Status', 'Last Scan Hub ID', 'Comments',
    'Route ID', 'Lase Seen', 'Shipper Name', 'Driver Name',
    'Last Scan Type',
  ];
  const RSVN_CSV_TITLES = ['Date', 'Shipper Name', 'Driver Name', 'Resolved',
    'Comments',
  ];

  const COD_FIELDS = ['date', 'codInbounded', 'codExpected', 'routeId', 'driverName'];
  const PARCEL_FIELDS = ['date', 'trackingId', 'granularStatus', 'lastScanHubId', 'exception',
    'routeId', 'lastSeen', 'shipperName', 'driverName',
    'lastScanType'];
  const RSVN_FIELDS = ['date', 'shipperName', 'driverName', 'resolved', 'exception'];

  function RouteCleaningReportController(RouteCleaningReport, nvTable, $scope, $rootScope,
    $timeout, $q, Ticketing, nvToast, Order, $window, nvTranslate, $state, nvCsvParser,
    nvFileUtils, nvDateTimeUtils, nvEnv) {
    const ctrl = this;

    ctrl.data = {};
    ctrl.function = {};
    ctrl.state = {};

    ctrl.function.fetch = fetch;
    ctrl.function.noop = angular.noop;
    ctrl.function.download = download;
    ctrl.function.createTicket = createTicket;
    ctrl.function.isDownloadDisabled = isDownloadDisabled;
    ctrl.function.getSelected = getSelected;
    ctrl.function.getCurrentCategory = getCurrentCategory;
    ctrl.function.isNinjavan = isNinjavan;

    ctrl.state.tab = TAB;

    // Deprecated - all except saas env
    if (!nvEnv.isSaas(nv.config.env)) {
      $state.go('container.reports'); // redirect to new page
    }

    init();

    function init() {
      ctrl.data.date = new Date();
      ctrl.data.tickets = null;
      ctrl.data.codMaster = [];
      ctrl.data.parcelMaster = [];
      ctrl.data.reservationMaster = [];

      ctrl.state.fetch = 'idle';
      ctrl.state.download = 'idle';
      ctrl.state.initiated = false;
      ctrl.state.loading = false;

      ctrl.state.report = TAB.COD;
      ctrl.state.reportDelayed = TAB.COD;

      initTable();

      $scope.$watch('ctrl.state.report', (val, oldVal) => {
        if (val !== oldVal) {
          ctrl.state.loading = true;
          initTable();

          // put delay to prevent table indefinetely crash
          $timeout(() => {
            ctrl.state.reportDelayed = val;
            ctrl.state.loading = false;
          }, 300);
        }
      });
    }

    function initTable() {
      switch (ctrl.state.report) {
        case TAB.COD:
          ctrl.data.codTableParam = nvTable.createTable(_.pick(TABLE_FIELDS, COD_FIELDS));
          ctrl.data.codTableParam.setData(ctrl.data.codMaster);
          break;
        case TAB.PARCEL:
          ctrl.data.parcelTableParam = nvTable.createTable(_.pick(TABLE_FIELDS, PARCEL_FIELDS));
          ctrl.data.parcelTableParam.setData(ctrl.data.parcelMaster);
          break;
        case TAB.RESERVATION:
          ctrl.data.reservationTableParam = nvTable.createTable(_.pick(TABLE_FIELDS, RSVN_FIELDS));
          ctrl.data.reservationTableParam.setData(ctrl.data.reservationMaster);
          break;
        default:
          break;
      }
    }

    function fetch() {
      ctrl.state.fetch = 'waiting';
      ctrl.state.loading = true;

      RouteCleaningReport.getJSON(ctrl.data.date)
        .then((data) => {
          const trackingIds = _.map(data.parcels, 'trackingId');
          return $q.all([
            data.cod,
            data.parcels,
            data.reservations,
            getTicketByTrackingId(trackingIds),
          ]);
        })
        .then((data) => {
          ctrl.data.codMaster = data[0];
          ctrl.data.parcelMaster = _.map(data[1], el => _.assign(el, { state: 'idle' }));
          ctrl.data.reservationMaster = data[2];
          ctrl.data.tickets = data[3];
        })
        .finally(() => {
          ctrl.state.fetch = 'idle';
          ctrl.state.loading = false;
          ctrl.state.initiated = true;
          initTable();
        }
      );
    }

    function isDownloadDisabled() {
      return getSelected().length === 0;
    }

    function download() {
      const selectedData = getSelected();

      let csvTitles = [];
      let csvFields = [];
      switch (ctrl.state.report) {
        case TAB.COD:
          csvTitles = COD_CSV_TITLES;
          csvFields = COD_FIELDS;
          break;
        case TAB.PARCEL:
          csvTitles = PARCEL_CSV_TITLES;
          csvFields = PARCEL_FIELDS;
          break;
        case TAB.RESERVATION:
          csvTitles = RSVN_CSV_TITLES;
          csvFields = RSVN_FIELDS;
          break;
        default:
          nvToast.warning('Category State not found, contact developer for this');
          return;
      }

      nvCsvParser.unparse({
        fields: csvTitles,
        data: _.map(selectedData, el => _.toArray(_.pick(el, csvFields))),
      }).then((csv) => {
        nvFileUtils.downloadCsv(csv, `${getCurrentCategory()} - ${nvDateTimeUtils.displayDate(ctrl.data.date)}.csv`);
      });
    }

    // //////////////////////////////////////
    function createTicket(order) {
      const payload = [
        { fieldName: 'ticketType',
          fieldId: 17,
          fieldValue: 2 },
        { fieldName: 'entrySource',
          fieldId: 13,
          fieldValue: 8 },
        { fieldName: 'trackingId',
          fieldId: 2,
          fieldValue: order.trackingId },
        { fieldName: 'investigatingParty',
          fieldId: 15,
          fieldValue: 3 },
        { fieldName: 'creatorUserId',
          fieldValue: $rootScope.user.thirdPartyId,
          fieldId: 30 },
        { fieldName: 'creatorUserName',
          fieldValue: _.compact([$rootScope.user.firstName, $rootScope.user.lastName]).join(' '),
          fieldId: 30 },
        { fieldName: 'creatorUserEmail',
          fieldValue: $rootScope.user.email,
          fieldId: 66 },
        { fieldName: 'TICKET_CREATION_SOURCE',
          fieldValue: 'ROUTE_CLEANING',
          fieldId: null,
        }];

      order.state = 'waiting';
      return Ticketing.createOne(payload)
                .then(onSuccess, onError);

      function onSuccess(data) {
        if (data.status === 'Success') {
          Ticketing.getTicketByTrackingIdV2(_.castArray(order.trackingId)).then(showTicket);
          nvToast.success(data.message);
          order.state = 'idle';
        } else {
          nvToast.error(data.message);
          order.state = 'idle';
        }
      }

      function onError() {
        nvToast.error('Failed to create recovery ticket');
      }

      function showTicket(data) {
        const obj = {};
        _.forEach(data, (row) => {
          obj[row.tracking_id] = row;
        });
        _.assign(ctrl.data.tickets, obj);
      }
    }

    function getSelected() {
      switch (ctrl.state.report) {
        case TAB.COD:
          return ctrl.data.codTableParam ? ctrl.data.codTableParam.getSelection() : [];
        case TAB.PARCEL:
          return ctrl.data.parcelTableParam ? ctrl.data.parcelTableParam.getSelection() : [];
        case TAB.RESERVATION:
          return ctrl.data.reservationTableParam
            ? ctrl.data.reservationTableParam.getSelection()
            : [];
        default:
          return [];
      }
    }

    function getCurrentCategory() {
      switch (ctrl.state.report) {
        case TAB.COD:
          return 'COD';
        case TAB.PARCEL:
          return 'Parcel';
        case TAB.RESERVATION:
          return 'Reservation';
        default:
          return 'NULL';
      }
    }

    function isNinjavan() {
      return nvEnv.isNinjavan(nv.config.env);
    }

    function getTicketByTrackingId(trackingIds) {
      const trackingIdsSegments = _.chunk(trackingIds, 200);
      const deferred = $q.defer();

      const requests = [];
      for (let i = 0; i < trackingIdsSegments.length; i++) {
        requests.push(Ticketing.getTicketByTrackingIdV2(trackingIdsSegments[i]));
      }
      $q.all(requests).then(success, error);

      return deferred.promise;

      function success(responses) {
        const result = {};
        _.forEach(responses, (tickets) => {
          const obj = {};
          _.forEach(tickets, (t) => {
            obj[t.tracking_id] = t;
          });
          _.assign(result, obj);
        });
        deferred.resolve(result);
      }

      function error() {
        deferred.reject(-1);
      }
    }
  }
}());
