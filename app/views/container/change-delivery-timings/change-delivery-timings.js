(function controller() {
  angular
        .module('nvOperatorApp.controllers')
        .controller('ChangeDeliveryTimeController', ChangeDeliveryTimeController);

  const COLUMNS = ['tracking_id', 'message'];

  const SAMPLE_CSV = [{ tracking_id: 'EUR101475857QOO', start_time: '2016-03-18', end_time: '2016-03-18', timewindow_id: '0' },
                      { tracking_id: 'SCO100024255A', start_time: '2016-06-17', end_time: '2016-06-21', timewindow_id: '-1' }];
  const SAMPLE_CSV_HEADER = ['Tracking Id', 'Start Date', 'End Date', 'Timewindow ID'];
  const SAMPLE_CSV_ORDER = ['tracking_id', 'start_time', 'end_time', 'timewindow_id'];

  ChangeDeliveryTimeController.$inject = [
    '$translate', 'nvButtonFilePickerType', 'nvFileSelectDialog', 'Order', 'nvToast',
    'nvTableUtils', 'nvTranslate',
  ];

  function ChangeDeliveryTimeController(
    $translate, nvButtonFilePickerType, nvFileSelectDialog, Order, nvToast,
    nvTableUtils, nvTranslate
  ) {
    const ctrl = this;
    ctrl.showSelectCSVDialog = showSelectCSVDialog;
    ctrl.changeDeliverySuccesses = [];
    ctrl.changeDeliveryErrors = [];
    ctrl.changeDeliverySuccessesTableParams = null;
    ctrl.changeDeliveryErrorsTableParams = null;

    ctrl.SAMPLE_CSV = SAMPLE_CSV;
    ctrl.SAMPLE_CSV_HEADER = SAMPLE_CSV_HEADER;
    ctrl.SAMPLE_CSV_ORDER = SAMPLE_CSV_ORDER;

        // //////////////////////
    function showSelectCSVDialog($event) {
      nvFileSelectDialog.show($event, {
        title: $translate.instant('container.change-delivery-timings.upload-title'),
        fileSelect: {
          accept: nvButtonFilePickerType.CSV,
          maxFileCount: 1,
        },
        buttonOkTitle: $translate.instant('container.change-delivery-timings.upload-csv'),
        onFinishSelect: onFinishSelect,
        onDone: onDone,
      });
    }

    function onFinishSelect(files) {
      return Order.changeDeliveryByCSV(files);
    }

    function onDone(data) {
      // toast
      nvToast.success(nvTranslate.instant('container.change-delivery-timings.delivery-time-updated'));

      // show results
      ctrl.changeDeliverySuccessesTableParams = nvTableUtils.generate(
        data.successes, { columns: COLUMNS }
      );
      ctrl.changeDeliveryErrorsTableParams = nvTableUtils.generate(
        data.errors, { columns: COLUMNS }
      );
    }
  }
}());
