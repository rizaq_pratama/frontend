(function controller() {
  angular.module('nvOperatorApp.controllers')
    .controller('OutboundBreakRouteController', OutboundBreakRouteController);

  OutboundBreakRouteController.$inject = ['Route', 'nvDialog', 'nvTranslate', 'nvToast',
    '$log', '$window', '$state', '$stateParams', '$timeout', 'nvExtendUtils', 'Order'];
  function OutboundBreakRouteController(Route, nvDialog, nvTranslate, nvToast,
    $log, $window, $state, $stateParams, $timeout, nvExtendUtils, Order) {
    const ctrl = this;
    ctrl.missingOutbounds = [];
    ctrl.orders = [];
    ctrl.scans = [];
    ctrl.getOutboundBreakByRouteId = getOutboundBreakByRouteId;
    ctrl.viewOrder = viewOrder;
    ctrl.pullFromRoute = pullFromRoute;
    ctrl.checked = false;

    init();
    function init() {
      // check the route params
      ctrl.routeId = $stateParams.routeId;
      if (ctrl.routeId) {
        getOutboundBreakByRouteId(ctrl.routeId);
      }
    }


    function viewOrder(orderId) {
      $window.open(
        $state.href('container.order.edit', { orderId: orderId })
      );
    }

    function getOutboundBreakByRouteId(routeId) {
      if (!routeId) {
        return angular.noop();
      }
      ctrl.checked = false;
      ctrl.missingOutbounds = [];
      ctrl.orders = [];
      ctrl.scans = [];
      setUrlParamWithoutRefresh(routeId);
      return Route.getRouteBreak(routeId)
        .then(success, failure);

      function success(results) {
        ctrl.checked = true;
        ctrl.orders = results;
        ctrl.missingOutbounds = [];        
        ctrl.scans = [];
        _.each(results, (data) => {
          nvExtendUtils.addMoment(data, ['last_scanned_at', 'end_time']);
          if (data.is_outbounded) {
            ctrl.scans.push(data);
          } else {
            ctrl.missingOutbounds.push(data);
          }
        });
      }

      function failure(err) {
        $log.debug(err);
        ctrl.checked = true;
      }
    }

    function setUrlParamWithoutRefresh(routeId) {
      $timeout(() => {
        $state.transitionTo(
            'container.outbound-breakroute',
          {
            routeId: routeId, // New id/$stateParams go here
          },
          {
            location: true, // This makes it update URL
            inherit: true,
            relative: $state.$current,
            notify: false, // This makes it not reload
          }
        );
      });
    }
    function pullFromRoute(order, event) {
      return nvDialog.confirmSave(event,
        {
          title: nvTranslate.instant('container.outbound-routebreak.confirm-pullout'),
          content: nvTranslate.instant('container.outbound-routebreak.are-you-sure-you-want-to-pull', { trackingId: order.tracking_id, routeId: ctrl.routeId }),
          ok: nvTranslate.instant('container.outbound-routebreak.pull-out'),
          cancel: nvTranslate.instant('commons.cancel'),
        }
      ).then(onOk);

      function onOk() {
        // always delivery
        return Order.removeFromRouteV2(order.order_id, { type: 'DELIVERY' })
          .then(onSuccessPull);
      }

      function onSuccessPull(result) {
        if (result && result.status === 'SUCCESS') {
          _.remove(ctrl.missingOutbounds, { order_id: order.order_id });
          _.remove(ctrl.orders, { order_id: order.order_id });
          nvToast.success(nvTranslate.instant('container.outbound-routebreak.success-pullout-tracking-id-x', { x: order.tracking_id }));
        } else {
          nvToast.warning(`error: ${_.get(result, 'message', 'unknown error')}`);
        }
      }
    }
  }
}());
