(function controller() {
  angular
    .module('nvOperatorApp.controllers')
    .controller('NonInboundedListController', NonInboundedListController);

  NonInboundedListController.$inject = ['nvTable', 'nvDateTimeUtils', 'nvTimezone', 'Order', 'nvDialog',
    'nvToast', 'Monitoring', 'NonInboundedFilter', 'nvBackendFilterUtils', 'Shippers', '$q', 'nvCsvParser',
    'nvFileUtils', 'nvEnv'];

  const TABLE_FIELDS = {
    shipper: { displayName: 'container.non-inbounded.table.shipper' },
    trackingId: { displayName: 'container.non-inbounded.table.tracking-id' },
    granularStatus: { displayName: 'container.non-inbounded.table.granular-status' },
    orderPickupDate: { displayName: 'container.non-inbounded.table.order-pickup-date' },
    fromAddress: { displayName: 'container.non-inbounded.table.from-address' },
    reservationCount: { displayName: 'container.non-inbounded.table.reservation-count' },
    lastSuccessReservation: { displayName: 'container.non-inbounded.table.last-success-reservation' },
    nextPendingReservation: { displayName: 'container.non-inbounded.table.next-pending-reservation' },
    orderCreatedAt: { displayName: 'container.non-inbounded.table.order-created-at' },
    reservationAddress: { displayName: 'container.non-inbounded.table.reservation-address' },
    lastRsvnDatetime: { displayName: 'container.non-inbounded.table.last-rsvn-datetime' },
    lastRsvnStatus: { displayName: 'container.non-inbounded.table.last-rsvn-status' },
    lastRsvnFailureDesc: { displayName: 'container.non-inbounded.table.last-rsvn-failure-description' },
    lastRsvnFailureCategory: { displayName: 'container.non-inbounded.table.last-rsvn-failure-category' },
  };

  const API_STATE = {
    IDLE: 'idle',
    WAITING: 'waiting',
  };

  const CSV_HEADER = ['Shipper', 'Tracking ID', 'Granular Status', 'Order Pickup Date',
    'From Address', 'Count of RSVN from OC', 'Last Successful RSVN', 'Next Pending RSVN',
    'Created Datetime', 'Reservation Address', 'Last RSVN datetime', 'Last RSVN status',
    'Last RSVN failure description', 'Last RSVN failure category'];

  const FIELD = ['shipper', 'trackingId', 'granularStatus', 'orderPickupDate',
    'fromAddress', 'reservationCount', 'lastSuccessReservation', 'nextPendingReservation',
    'orderCreatedAt', 'reservationAddress', 'lastRsvnDatetime', 'lastRsvnStatus',
    'lastRsvnFailureDesc', 'lastRsvnFailureCategory'];

  function NonInboundedListController(nvTable, nvDateTimeUtils, nvTimezone, Order, nvDialog,
    nvToast, Monitoring, NonInboundedFilter, nvBackendFilterUtils, Shippers, $q, nvCsvParser,
    nvFileUtils, nvEnv) {
    const ctrl = this;

    ctrl.state = {};
    ctrl.data = {};
    ctrl.function = {};

    ctrl.function.cancelSelectedOrder = cancelSelectedOrder;
    ctrl.function.sendWebhook = sendWebhook;
    ctrl.function.loadFromFilter = loadFromFilter;
    ctrl.function.getSelectedSize = getSelectedSize;
    ctrl.function.bulkActionDisabled = bulkActionDisabled;
    ctrl.function.downloadCsv = downloadCsv;
    ctrl.function.showEditFilter = showEditFilter;
    ctrl.function.isNinjavan = isNinjavan;

    init();

    function init() {
      ctrl.state.loadState = API_STATE.IDLE;
      ctrl.state.page = 0;

      if (isNinjavan()) {
        return;
      }

      // filter
      const filters = NonInboundedFilter.init();
      ctrl.data.possibleFilters = filters;
      ctrl.data.selectedFilters = _.remove(ctrl.data.possibleFilters,
          filter => filter.showOnInit === true);

      ctrl.data.bulkActionProgressPayload = null;
    }

    function loadFromFilter() {
      ctrl.state.loadState = API_STATE.WAITING;

      ctrl.data.tableParam = nvTable.createTable(TABLE_FIELDS);
      _.forEach(ctrl.data.selectedFilters, filter =>
        (filter.description = nvBackendFilterUtils.getDescription(filter)));

      const selectedFilters = ctrl.data.selectedFilters;
      // quick fix: change how to load shippers, ignore seller merge into master shipper
      // requested by PH ops, KH
      const newParams = nvBackendFilterUtils.constructParams(selectedFilters);
      Monitoring.fetchFailedOrder(newParams)
        .then(success, failure);

      function success(data) {
        ctrl.state.loadState = API_STATE.IDLE;

        const nonInboundedList = data.orders;
        ctrl.data.tableParam.setData(extendOrder(nonInboundedList));
        ctrl.state.page = 1;
      }

      function failure() {
        ctrl.state.loadState = API_STATE.IDLE;
        ctrl.state.page = 0;
      }
    }

    function extendOrder(nonInboundedList) {
      return _.map(nonInboundedList, (nonInbounded) => {
        const nextPendingReservation = nvDateTimeUtils.toDate(
          nonInbounded.next_pending_reservation_time, nvTimezone.getOperatorTimezone()
        );
        const orderCreatedAt = nvDateTimeUtils.toDate(
          nonInbounded.order_created_at, nvTimezone.getOperatorTimezone()
        );
        const orderPickupDate = nvDateTimeUtils.toDate(
          nonInbounded.transaction_pickup_start_time, nvTimezone.getOperatorTimezone()
        );
        const lastSuccessReservationTime = nvDateTimeUtils.toDate(
          nonInbounded.last_success_reservation_time, nvTimezone.getOperatorTimezone()
        );
        const lastRsvnDatetime = nvDateTimeUtils.toDate(
          nonInbounded.last_reservation_time, nvTimezone.getOperatorTimezone()
        );
        const reservationCount = nonInbounded.reservation_count != null ? nonInbounded.reservation_count.toString() : 'N/A';
        return {
          id: nonInbounded.order_id,
          shipper: `${nonInbounded.shipper_id} - ${nonInbounded.shipper_name || 'N/A'}`,
          trackingId: nonInbounded.tracking_id,
          granularStatus: nonInbounded.granular_status || 'NULL',
          orderPickupDate: nvDateTimeUtils.displayDateTime(orderPickupDate, nvTimezone.getOperatorTimezone()) || 'NULL',

          fromAddress: nonInbounded.from_address || 'NULL',
          reservationCount: reservationCount,
          lastSuccessReservation:
            nvDateTimeUtils.displayDateTime(lastSuccessReservationTime, nvTimezone.getOperatorTimezone()) || 'NULL',
          nextPendingReservation:
            nvDateTimeUtils.displayDateTime(nextPendingReservation, nvTimezone.getOperatorTimezone()) || 'NULL',

          orderCreatedAt:
            nvDateTimeUtils.displayDateTime(orderCreatedAt, nvTimezone.getOperatorTimezone()) || 'NULL',
          reservationAddress: constructFieldWithPrefix(
            nonInbounded.reservation_address,
            nonInbounded.matching_address
          ) || 'NULL',
          lastRsvnDatetime: nvDateTimeUtils.displayDateTime(lastRsvnDatetime, nvTimezone.getOperatorTimezone()) || 'NULL',
          lastRsvnStatus: nonInbounded.last_reservation_status || 'NULL',
          lastRsvnFailureDesc: nonInbounded.last_reservation_failure_reason || 'NULL',
          lastRsvnFailureCategory: nonInbounded.last_reservation_failure_reason_code || 'NULL',
        };

        function constructFieldWithPrefix(str, isMatched) {
          const noMatchedString = '(No matched address, Address Slot 1 Displayed)';

          if (isMatched) {
            return str;
          }
          return str ? `${noMatchedString} ${str}` : noMatchedString;
        }
      });
    }

    function cancelSelectedOrder($event) {
      const orders = _.map(_.cloneDeep(ctrl.data.tableParam.getSelection()), (order) => {
        // extend order details for cancel-order dialog to use
        order._granularStatus = order.granularStatus;
        return order;
      });

      nvDialog.showSingle($event, {
        templateUrl: 'views/container/order/dialog/cancel-order/cancel-order.dialog.html',
        theme: 'nvRed',
        cssClass: 'order-edit-cancel-order',
        controller: 'OrderEditCancelOrderDialogController',
        controllerAs: 'ctrl',
        locals: {
          ordersData: orders,
          settingsData: {
            isMultiSelect: true,
          },
        },
      }).then(success);

      function success(response) {
        ctrl.data.tableParam.remove(response.success, 'id');
        ctrl.data.tableParam.deselect();
      }
    }

    function getSelectedSize() {
      if (ctrl.data.tableParam) {
        return ctrl.data.tableParam.getSelection().length;
      }
      return 0;
    }

    function sendWebhook($event) {
      nvDialog.showSingle($event, {
        templateUrl: 'views/container/non-inbounded-list/dialog/pickup-failed-webhook/pickup-failed-webhook.dialog.html',
        theme: 'nvBlue',
        cssClass: 'pickup-failed-webhook-dialog',
        controller: 'PickupFailedWebhookDialogController',
        controllerAs: 'ctrl',
        locals: {
          localData: {
            orders: ctrl.data.tableParam && ctrl.data.tableParam.getSelection(),
          },
        },
      });
    }

    function bulkActionDisabled() {
      return getSelectedSize() === 0;
    }

    function downloadCsv() {
      const date = nvDateTimeUtils.toMoment(new Date(), nvTimezone.getOperatorTimezone());
      const fileName = `non-inbounded-list-${nvDateTimeUtils.displayDateTime(date)}.csv`;
      const selected = ctrl.data.tableParam.getSelection();

      nvCsvParser.unparse({
        fields: CSV_HEADER,
        data: _.map(selected, el => _.toArray(_.pick(el, FIELD))),
      }).then((csv) => {
        nvFileUtils.downloadCsv(csv, fileName);
      });
    }

    function showEditFilter() {
      ctrl.data.tableParam = null;
      ctrl.state.page = 0;
    }

    function isNinjavan() {
      return nvEnv.isNinjavan(nv.config.env);
    }
  }
}());
