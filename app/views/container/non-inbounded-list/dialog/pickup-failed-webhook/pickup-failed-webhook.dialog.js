(function controller() {
  angular
    .module('nvOperatorApp.controllers')
    .controller('PickupFailedWebhookDialogController', PickupFailedWebhookDialogController);

  PickupFailedWebhookDialogController.$inject = ['localData', '$mdDialog', '$timeout',
    'Tools', 'nvToast', 'nvTranslate', 'nvCsvParser', 'nvFileUtils'];

  const VIEW = {
    LOADING: 0,
    CONTENT: 1,
  };

  // hardcoded by waiting for better solution, damien
  const FAILURE_REASONS = [
    { value: 'Cannot Make It (CMI)', displayName: 'Cannot Make It (CMI)' },
    { value: 'Insufficient Space - Approx Volume very inaccurate', displayName: 'Insufficient Space - Approx Volume very inaccurate' },
    { value: 'Insufficient Space - Approx Volume Accurate, Vehicle full from previous pick up', displayName: 'Insufficient Space - Approx Volume Accurate, Vehicle full from previous pick up' },
    { value: 'No parcels to pick up at all', displayName: 'No parcels to pick up at all' },
    { value: 'Office closed', displayName: 'Office closed' },
    { value: 'Parcel is not ready for collection', displayName: 'Parcel is not ready for collection' },
    { value: 'Rejected - damaged items', displayName: 'Rejected - damaged items' },
    { value: 'Rejected - no AWB', displayName: 'Rejected - no AWB' },
    { value: 'Rejected - no proper packaging', displayName: 'Rejected - no proper packaging' },
    { value: 'Rejected - Oversized/overweight', displayName: 'Rejected - Oversized/overweight' },
    { value: 'Separate pick up has been done earlier', displayName: 'Separate pick up has been done earlier' },
    { value: 'The address is inaccurate', displayName: 'The address is inaccurate' },
  ];

  function PickupFailedWebhookDialogController(localData, $mdDialog, $timeout,
    Tools, nvToast, nvTranslate, nvCsvParser, nvFileUtils) {
    const ctrl = this;

    const ordersCopy = _.cloneDeep(localData.orders);
    const STATUS_PICKUP_FAIL = 'Pickup Fail';

    ctrl.data = {};
    ctrl.function = {};
    ctrl.state = {};

    ctrl.data.orders = extendOrders(ordersCopy);

    ctrl.function.init = init;
    ctrl.function.onSubmit = onSubmit;
    ctrl.function.onCancel = onCancel;
    ctrl.function.isSubmitDisabled = isSubmitDisabled;
    ctrl.function.failureReasonChanged = failureReasonChanged;

    ctrl.state.masterView = VIEW.LOADING;

    init();

    function init() {
      ctrl.state.submit = 'idle';
      ctrl.state.isSetToAll = false;

      ctrl.data.failureReasons = FAILURE_REASONS;
      ctrl.state.masterView = VIEW.CONTENT;
    }

    function extendOrders(orders) {
      const timeNow = new Date().getTime();
      return _.map(orders, (o) => {
        const comments = (!o.lastRsvnFailureDesc || o.lastRsvnFailureDesc === 'NULL') ?
          '' : o.lastRsvnFailureDesc;
        return {
          tracking_id: o.trackingId,
          new_granular_status: STATUS_PICKUP_FAIL,
          timestamp: timeNow,
          comments: comments,
        };
      });
    }

    function onSubmit() {
      Tools.sendWebhooks(ctrl.data.orders).then(success);

      function success(response) {
        nvToast.success(
          nvTranslate.instant('container.non-inbounded.failed-pickup-webhook.webhook-request-sent-successfully'),
        );
        if (response.length < ctrl.data.orders.length) {
          nvToast.warning(
            nvTranslate.instant('container.non-inbounded.failed-pickup-webhook.webhook-request-sent-with-error'),
            nvTranslate.instant('container.non-inbounded.failed-pickup-webhook.webhook-request-sent-with-error-details', {
              result: ctrl.data.orders.length - response.length,
              data: ctrl.data.orders.length,
            }),
            {
              buttonTitle: 'download as CSV',
              buttonCallback: downloadCsv,
            },
          );
        }
        $mdDialog.hide(0);

        function buildSuccessFailedCsv(requestedTrackingIds, successTrackingIds) {
          const data = [];
          _.forEach(requestedTrackingIds, (val, key) => {
            data.push([val, successTrackingIds[key] || null]);
          });
          return {
            fields: ['Requested', 'Success'],
            data: data,
          };
        }

        function downloadCsv() {
          const requestedTrackingIds = _.map(ctrl.data.orders, order => order.tracking_id);
          const data = buildSuccessFailedCsv(requestedTrackingIds, response);
          nvCsvParser.unparse(data).then(result =>
            nvFileUtils.downloadCsv(result, 'non-inbounded-reques-and-success-list.csv'));
        }
      }
    }

    function onCancel() {
      $mdDialog.hide(-1);
    }

    function isSubmitDisabled() {
      const orders = ctrl.data.orders;
      if (orders) {
        return !!_.find(orders, order => !order.comments);
      }
      return true;
    }

    function failureReasonChanged(order) {
      $timeout(() => {
        if (ctrl.state.isSetToAll) {
          _.forEach(ctrl.data.orders, (theOrder) => {
            theOrder.comments = order.comments;
          });
        }
      });
    }
  }
}());
