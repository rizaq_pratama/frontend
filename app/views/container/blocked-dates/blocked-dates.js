(function() {
    'use strict';

    angular
        .module('nvOperatorApp.controllers')
        .controller('BlockedDatesController', BlockedDatesController);

    BlockedDatesController.$inject = ['BlockedDates', '$q', '$scope', 'nvCalendar.Data', 'nvCalendarList.Data', '$filter', 'nvToast'];

    function BlockedDatesController(BlockedDates, $q, $scope, nvCalendarData, nvCalendarListData, $filter, nvToast) {
        var ctrl = this;
        ctrl.getBlockedDates = getBlockedDates;
        ctrl.save = save;
        ctrl.blockedDates = [];
        ctrl.saveButtonState = 'clean';
        ctrl.undoState = null;
        ctrl.disableCalendar = false;
        ctrl.listData = nvCalendarListData.getByHandle('blocked-dates-calendar');

        var offChange = ctrl.listData.onChange(function(changes){
            if (ctrl.blockedDateForm) {
                if(changes.added.length === 0 && changes.removed.length === 0){
                    ctrl.blockedDateForm.$setPristine();
                }else{
                    ctrl.blockedDateForm.$setDirty();
                }
                $scope.$apply();
            }
        });

        $scope.$on('$destroy', function(){
            offChange();
            nvCalendarListData.removeHandle('blocked-dates-calendar');
        });

        //////////////////////////////////////////////////

        function getBlockedDates() {
            return BlockedDates.read().then(success, $q.reject);

            function success(blockedDates) {
                ctrl.blockedDates = blockedDates;
            }
        }

        function save() {

            function saveBlockedDates(){
                ctrl.saveButtonState = 'saving';
                //get difference
                var changes = ctrl.listData.getChanges();
                //save undo state
                ctrl.undoState = _.clone(changes);
                //save changes
                var added = mapDateToNewObject(changes.added);
                var removed = mapDateToObjectInBlockedDates(changes.removed);
                return saveChanges(added, removed);
            }

            function showSuccessToast(){
                nvToast.custom.withAction(undo).success('Blocked dates updated.');
            }

            function cleanButtonState(){
                ctrl.saveButtonState = 'clean';
            }

            saveBlockedDates()
            .then(getBlockedDates)
            .then(showSuccessToast)
            .finally(cleanButtonState);
        }

        function undo(){
            if(_.isNull(ctrl.undoState)){
                return;
            }

            //do undo
            var addBack = mapDateToNewObject(ctrl.undoState.removed);
            var removeBack = mapDateToObjectInBlockedDates(ctrl.undoState.added);
            
            saveChanges(addBack, removeBack)
            .then(getBlockedDates)
            .then(clearUndoState);
        }

        function saveChanges(added, removed) {
            disableCalendar();

            return $q.all([
                    addBlockedDates(added),
                    deleteBlockedDates(removed)
                ]).then(cleanForm)
                .finally(enableCalendar);
        }

        function addBlockedDates(added){
            return BlockedDates.create(added);
        }

        function deleteBlockedDates(removed){
            if(removed.length > 0){
                return BlockedDates.delete(removed);
            }else{
                return $q.when();
            }
        }

        function clearUndoState(){
            ctrl.undoState = null;
        }

        function disableCalendar(){
            ctrl.disableCalendar = true;
        }

        function enableCalendar(){
            ctrl.disableCalendar = false;
        }

        function cleanForm(){
            ctrl.blockedDateForm.$setPristine();
        }

        function mapDateToNewObject(dates){
            return _.map(dates, function(date){
                return { date: dateToDateString(date) };
            });
        }

        function mapDateToObjectInBlockedDates(dates){
            return _.map(dates, function(date){
                var dateString = dateToDateString(date);
                return _.find(ctrl.blockedDates, function(d){
                    return d.date === dateString;
                });
            });
        }

        function dateToDateString(date){
            return $filter('nvTime')(date, 'no-timezone');
        }
    }
})();
