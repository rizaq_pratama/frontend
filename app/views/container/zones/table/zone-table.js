(function controller() {
  angular
        .module('nvOperatorApp.controllers')
        .controller('ZoneTableController', ZoneTableController);

  ZoneTableController.$inject = [
    '$q', 'Zone', 'nvTable', 'nvTranslate', 'nvToast',
    'nvDialog', 'Hub', 'nvZonesService', '$state', 'nvNavGuard', '$stateParams',
    '$timeout',
  ];

  function ZoneTableController(
        $q, Zone, nvTable, nvTranslate, nvToast,
        nvDialog, Hub, nvZonesService, $state, nvNavGuard, $stateParams,
        $timeout
    ) {
        // variables
    let hubs = null;
    const ctrl = this;
    ctrl.refreshZonesCacheDisabled = false;
    ctrl.stateRefresh = 'idle';

    ctrl.tableParam = nvTable.createTable(Zone.FIELDS);
    ctrl.tableParam.addColumn('hubName', { displayName: 'container.zones.hub-name' });
    ctrl.tableParam.addColumn('latLng', { displayName: 'container.zones.lat-long' });
    ctrl.tableParam.sort('short_name');

        // functions
    ctrl.getAll = getAll;
    ctrl.addZone = addZone;
    ctrl.editZone = editZone;
    ctrl.deleteZone = deleteZone;
    ctrl.refreshZonesCache = refreshZonesCache;
    ctrl.editPolygon = editPolygon;
    ctrl.editPolygons = editPolygons;
    ctrl.state = 'SHOW_TABLE';
    if ($stateParams.refresh) {
      refreshZonesData();
    }
        // function details
    function getAll() {
      hubs = nvZonesService.hubs;
      processZonesTableParam(nvZonesService.zones);
    }

    function refreshZonesData() {
      ctrl.stateRefresh = 'waiting';
      return Zone.read().then(success, $q.reject).finally(() => {
        ctrl.stateRefresh = 'idle';
      });

      function success(zones) {
        processZonesTableParam(zones);
        ctrl.tableParam.refreshData();
      }
    }

    function editPolygon(event, zone) {
      nvZonesService.clearSelectedZones();
            // add zone into service
      Zone.get(zone.id).then((response) => {
        nvZonesService.addToSelectedZones(response);
        $state.go('container.zones.polygon');
      });
    }

    function editPolygons() {
      nvZonesService.clearSelectedZones();
      const selectedZones = ctrl.tableParam.getSelection();
      return $q.all(_.map(selectedZones, z => (Zone.get(z.id)))).then((responses) => {
        nvZonesService.addToSelectedZones(responses);
        $state.go('container.zones.polygon');
      });
    }

    function addZone($event) {
      nvDialog.showSingle($event, {
        templateUrl: 'views/container/zones/dialog/zone-form.dialog.html',
        theme: 'nvGreen',
        cssClass: 'zone-form',
        controller: 'ZoneFormDialogController',
        controllerAs: 'ctrl',
        locals: {
          zoneData: null,
        },
      }).then(onSet);

      function onSet(response) {
        ctrl.tableParam.mergeIn(setCustomZoneData(response), 'id');
      }
    }

    function editZone($event, zone) {
      nvDialog.showSingle($event, {
        templateUrl: 'views/container/zones/dialog/zone-form.dialog.html',
        cssClass: 'zone-form',
        controller: 'ZoneFormDialogController',
        controllerAs: 'ctrl',
        locals: {
          zoneData: zone,
        },
      }).then(onSet);

      function onSet(response) {
        ctrl.tableParam.mergeIn(setCustomZoneData(response), 'id');
      }
    }

    function deleteZone($event, zone) {
      nvDialog.confirmDelete($event).then(onDelete);

      function onDelete() {
        return Zone.deleteRoutingZone(zone.id).then(success);
      }

      function success() {
        ctrl.tableParam.remove(zone, 'id');
      }
    }

    function refreshZonesCache() {
      ctrl.refreshZonesCacheDisabled = true;
      return Zone.reloadRoutingZonesCache().then(success, failure).finally(() => {
        ctrl.stateRefresh = 'idle';
      });
      function success() {
        ctrl.refreshZonesCacheDisabled = false;
        refreshZonesData();
        nvToast.success(nvTranslate.instant('container.zones.zones-cache-refreshed'));
      }

      function failure() {
        ctrl.refreshZonesCacheDisabled = false;
      }
    }

    function processZonesTableParam(zones) {
      ctrl.tableParam.setData(extend(zones));

      function extend(theZones) {
        return _.map(theZones, zone => (
                    setCustomZoneData(zone)
                ));
      }
    }

    function setCustomZoneData(zone) {
      zone.latLng = generateLatLngString(zone);
      zone.hubName = getHubName(zone.hub_id);      
      return zone;
    }

    function generateLatLngString(theZone) {
      if (theZone.latitude !== null && theZone.longitude !== null) {
        return `${theZone.latitude}, ${theZone.longitude}`;
      }

      return '';
    }

    function getHubName(hubId) {
      const hub = _.find(hubs, ['id', hubId]);

      return hub ? hub.name : '';
    }

    function showOrHidePolygonEditor(show) {
      if (show) {
        ctrl.state = 'SHOW_ZONE_POLYGON';
      } else {
        ctrl.state = 'SHOW_TABLE';
      }
    }
  }
}());
