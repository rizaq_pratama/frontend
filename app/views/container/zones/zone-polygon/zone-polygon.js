(function controller() {
  angular
        .module('nvOperatorApp.controllers')
        .controller('ZonePolygonController', ZonePolygonController);

  ZonePolygonController.$inject = [
    '$scope',
    '$http',
    '$timeout',
    '$log',
    '$window',
    'nvMapboxGL',
    'nvRequestUtils',
    'nvDialog',
    'nvZonesService',
    'nvMaps',
    'nvNavGuard',
    'nvEditModelDialog',
    '$state',
    '$q',
    'Zone',
    '$translate',
    'nvToast',
    'nvTranslate',
  ];

  function ZonePolygonController(
        $scope, $http, $timeout, $log, $window,
        nvMapboxGL, nvRequestUtils, nvDialog,
        nvZonesService, nvMaps, nvNavGuard, nvEditModelDialog,
        $state, $q, Zone, $translate, nvToast, nvTranslate
    ) {
    const ctrl = this;
    ctrl.isEditing = false;
    ctrl.isCreating = false; // when the map is in CREATE mode
    ctrl.isDeleting = false; // when the map is in DELETE mode
    ctrl.isChoosing = false; // when the map is in CHOOSE mode
    ctrl.isResizing = false; // when the map is in RESIZE mode
    ctrl.hasAnyPoly = false; // when the map has at least one polygon
    ctrl.hasAnImage = false; // when the map has an image loaded
    ctrl.featureProperties = null; // the properties of the selected feature (polygon)
    ctrl.radius = 0.2; // the radius (in km) of the CHOOSE mode area
    ctrl.opacity = 0.7; // the opacity of the RESIZE mode image
    ctrl.selectedZones = angular.copy(nvZonesService.selectedZones);
    ctrl.zones = angular.copy(nvZonesService.zones);
    ctrl.searchZoneResult = [];
    ctrl.isRevealMode = false;
    ctrl.searchPage = 1;
    ctrl.maximumVertexCount = 3500;
    ctrl.totalFilteredZones = 0;
    ctrl.maxPage = 0;

    ctrl.revealMode = revealMode;
    ctrl.onClickDeletePolygon = onClickDeletePolygon;
    ctrl.onClickClearAllPolygons = onClickClearAllPolygons;
    ctrl.isSelectedPoly = isSelectedPoly;
    ctrl.onClickback = onClickback;
    ctrl.hasUnsavedChanges = hasUnsavedChanges;
    ctrl.onEditZone = onEditZone;
    ctrl.onSearchZone = onSearchZone;
    ctrl.getTotalVertexFromZone = getTotalVertexFromZone;
    ctrl.saveChanges = saveChanges;
    ctrl.removeZone = removeZone;
    ctrl.openEditCoordinateDialog = openEditCoordinateDialog;
    ctrl.addSelectedZoneToList = addSelectedZoneToList;
    ctrl.optimizeVertices = optimizeVertices;
    ctrl.undoOptimize = undoOptimize;


    const THRESHOLD = 20;
    const SQUARE_LENGTH = 40;


    let isMapLoaded = false;
    let normalDragFeatures = []; // holds the features currently involved in dragging
    let adjustFeature = null;
    let createPolyCoords = []; // holds the coordinates of the polygon currently being created
    const resizeCoords = []; // holds the coordinates of the image currently being resized
    let radiusLat = 0;
    let map;
    let cvs;

    // states
    ctrl.savingState = 'idle';

    isLoading(true);
    $timeout(() => {
      const { lat, lng } = nvMaps.getDefaultLatLngForDomain();
      map = nvMapboxGL.initMap({ center: [lng, lat], zoom: 12 });
      cvs = map.getCanvasContainer();
      map.once('load', load);
    });

    $scope.$watch('ctrl.isCreating', (isCreating) => {
      if (!isMapLoaded) {
        return;
      }

      createPolyCoords = [];

      if (isCreating) {
                // turn off NORMAL functions
        map.doubleClickZoom.disable();
        map.off('click', normalClick);
        map.off('mousedown', normalMousedown);
        map.off('mousemove', normalMousemove);
        map.off('mouseup', normalMouseup);

                // turn on CREATE functions
        map.on('click', createClick);
        map.on('dblclick', createDblclick);
        map.on('mousemove', createMousemove);
      } else {
                // turn off CREATE functions
        map.off('click', createClick);
        map.off('dblclick', createDblclick);
        map.off('mousemove', createMousemove);

                // turn on NORMAL functions
        map.doubleClickZoom.enable();
        map.on('click', normalClick);
        map.on('mousedown', normalMousedown);
        map.on('mousemove', normalMousemove);
        map.on('mouseup', normalMouseup);
      }
    });

    $scope.$watch('ctrl.isDeleting', (isDeleting) => {
      if (!isMapLoaded) {
        return;
      }

      if (isDeleting) {
                // turn off NORMAL functions
        map.off('click', normalClick);
        map.off('mousemove', normalMousemove);

                // turn on DELETE functions
        map.on('click', deleteClick);
        map.on('mousemove', deleteMousemove);
      } else {
                // turn off DELETE functions
        map.off('click', deleteClick);
        map.off('mousemove', deleteMousemove);

                // turn on NORMAL functions
        map.on('click', normalClick);
        map.on('mousemove', normalMousemove);
      }
    });

    $scope.$watch('ctrl.isResizing', (isResizing) => {
      if (!isMapLoaded) {
        return;
      }

      if (isResizing) {
                // turn off NORMAL functions
        map.off('click', normalClick);
        map.off('mousedown', normalMousedown);
        map.off('mousemove', normalMousemove);
        map.off('mouseup', normalMouseup);

                // turn on ADJUST functions
        map.on('mousedown', adjustMousedown);
        map.on('mousemove', adjustMousemove);
        map.on('mouseup', adjustMouseup);

        nvMapboxGL.addImageAdjustLayers(map);
      } else {
                // turn on NORMAL functions
        map.on('click', normalClick);
        map.on('mousedown', normalMousedown);
        map.on('mousemove', normalMousemove);
        map.on('mouseup', normalMouseup);

                // turn off ADJUST functions
        map.off('mousedown', adjustMousedown);
        map.off('mousemove', adjustMousemove);
        map.off('mouseup', adjustMouseup);

        nvMapboxGL.removeImageAdjustLayers(map);
      }
    });

    $scope.$on('$destroy', nvMapboxGL.destroy);

        // //////////////////////////////////////////////////
        // LOAD EVENT
        // //////////////////////////////////////////////////

    function load() {
      $log.debug('load');
      map.resize();
      nvMapboxGL.initPolygonDrawing(map);
      isMapLoaded = true;
      //  remove the selected zones form zones
      $timeout(() => {
        _.each(ctrl.selectedZones, (z) => {
          z.vertexCount = calculateVertices(_.get(z, 'polygon'));
          drawPolygonFromZone(z);
        });
        ctrl.hasAnyPoly = nvMapboxGL.hasAnyPolygon();
        isLoading(false);
      }).then(() => {
        panAndZoom();
      });

      map.on('click', normalClick);
      map.on('mousedown', normalMousedown);
      map.on('mousemove', normalMousemove);
      map.on('mouseup', normalMouseup);
    }

    function panAndZoom() {
      const bounds = findSwAndNePoint(ctrl.selectedZones);
      if (
        bounds &&
        bounds.southWest[0] !== bounds.northEast[0] &&
        bounds.southWest[1] !== bounds.northEast[1]) {
        map.fitBounds([
          bounds.southWest,
          bounds.northEast,
        ], {
          padding: 40,
        });
      } else {
        // zoom in into the loaded zone's lat long
        const loadedZone = ctrl.selectedZones[0];
        nvMapboxGL.flyTo(map, loadedZone.longitude, loadedZone.latitude);
      }
    }

    function drawPolygonFromZone(z) {
      if (z.polygon) {
        z.featureId = nvMapboxGL.addPolygon(map, nvMapboxGL.toMultiPolygonGeometry(z.polygon));
      }

      if (z.latitude && z.longitude) {
        z.marker = nvMapboxGL.createMarker(map, { lat: z.latitude, lng: z.longitude });
      }
    }

    function onEditZone(zone) {
      $log.debug('EDIT zone click');
      // clear other flag
      if (ctrl.isSelectedPoly()) {
        // deselect all
        _.each(ctrl.selectedZones, (z) => {
          nvMapboxGL.setPolyInactive(map, z.featureId);
          z.isEditing = false;
        });
      }
      zone.isEditing = true;
      ctrl.isEditing = true;
      ctrl.editingZone = zone;
      if (zone.polygon) {
        ctrl.featureProperties = _.assign(zone.polygon.properties,
          {
            id: nvMapboxGL.findRenderedIndexByFeatureProperties(zone.polygon.properties.id),
          });

        $timeout(() => {
          nvMapboxGL.setPolyActive(map, zone.featureId);
          nvMapboxGL.setActivePolygonData(map, zone.featureId);
        });
      } else {        
        $timeout(() => {
          ctrl.isCreating = true;
          nvMapboxGL.resetActivePolygonData(map);
        });
      }
    }

    function removeZone(zone) {
      if (zone.featureId && nvMapboxGL.existsPolygon(+zone.featureId)) {
        nvMapboxGL.removePolygon(map, +zone.featureId);
      }

      if (zone.marker) {
        nvMapboxGL.removeMarker(zone.marker);
      }
      if (ctrl.editingZone && ctrl.editingZone.id === zone.id) {
        ctrl.editingZone = null;
      }


      _.pull(ctrl.selectedZones, zone);
    }

    function hasUnsavedChanges() {
      let hasChange = false;
      _.each(ctrl.selectedZones, (z) => {
        hasChange = hasChange || z.edited;
      });
      if (hasChange) {
        nvNavGuard.lock();
      } else {
        nvNavGuard.unlock();
      }

      return hasChange;
    }

    /**
     * Search zones based on query and paginate it
     * @param {String} searchQuery
     * @param {Integer} page
     */
    function onSearchZone(event, searchQuery, page = 1) {
      if (page === 0) {
        return;
      }
      ctrl.searchZoneResult.length = 0;
      const itemsPerPage = 5;
      const selectedZoneIds = _.map(ctrl.selectedZones, z => (z.id));
      let zones = [];
      if (selectedZoneIds.length > 0) {
        zones = _.filter(ctrl.zones, z => (selectedZoneIds.indexOf(z.id) < 0));
      } else {
        zones = angular.copy(ctrl.zones);
      }
      const sourceZones = _.filter(zones, z => (
        _.toLower(z.name).includes(_.toLower(_.trim(searchQuery)))
        || _.toLower(z.short_name).includes(_.toLower(_.trim(searchQuery)))
      ));
      ctrl.totalFilteredZones = sourceZones.length;
      ctrl.maxPage = _.ceil(sourceZones.length / itemsPerPage);

      if (page > ctrl.maxPage) {
        return;
      }
      const offset = (page - 1) * itemsPerPage;
      ctrl.searchZoneResult = _.slice(sourceZones, offset, (offset + itemsPerPage));
      ctrl.searchPage = page;
    }

    function addSelectedZoneToList() {
      const selected = _.remove(ctrl.searchZoneResult, z => (z.isSelectedToEdit));
      _.remove(ctrl.zones, z => (_.find(selected, z1 => (z.id === z1.id)) >= 0));
      const zoneDetailPromises = _.map(selected, (z) => Zone.get(z.id));
      $q.all(zoneDetailPromises)
      .then((results) => {
        $timeout(() => {
          _.each(results, (z) => {
            if (ctrl.getTotalVertexFromZone() < ctrl.maximumVertexCount) {
              z.vertexCount = calculateVertices(_.get(z, 'polygon'));
              ctrl.selectedZones.push(z);
              drawPolygonFromZone(z);
            }
          });
        }).then(() => {
          panAndZoom();
        });
      });
    }

    function calculateVertices(polygon) {
      if (!polygon || !polygon.geometry) {
        return 0;
      }
      const coordinatesArray = _.get(polygon, 'geometry.coordinates', []);
      if (polygon.geometry.type === 'Polygon') {
        return _.reduce(coordinatesArray, (sum, coords) => (coords.length), 0) - 1;
      } else if (polygon.geometry.type === 'MultiPolygon') {
        let count = 0;
        _.each(coordinatesArray, (cArr) => {
          count += _.reduce(cArr, (sum, coords) => (coords.length), 0) - 1;
        });
        return count;
      }
      return 0;
    }
        // //////////////////////////////////////////////////
        // CLICK EVENTS
        // //////////////////////////////////////////////////

    function normalClick(event) {
      $log.debug('NORMAL click');
      if (ctrl.isSelectedPoly()) {
        nvMapboxGL.setPolyInactive(map, ctrl.featureProperties.id);
        // deselect all
        _.each(ctrl.selectedZones, (z) => {
          z.isEditing = false;
        });
      }

      const features = map.queryRenderedFeatures(
                event.point,
                { layers: nvMapboxGL.LAYER_GROUPS.POLY_FILL }
            );

      if (features.length) {
        const polySrcId = features[0].properties.id;
        $timeout(() => {
          ctrl.featureProperties = features[0].properties;
          ctrl.editingZone = _.find(ctrl.selectedZones, z => (z.featureId === polySrcId));
          ctrl.editingZone.isEditing = true;
          ctrl.editingZone.vertexCount = calculateVertices(ctrl.editingZone.polygon);
          ctrl.isEditing = true;
          nvMapboxGL.setPolyActive(map, polySrcId);
          nvMapboxGL.setActivePolygonData(map, polySrcId);
        });
      } else {
        $timeout(() => {
          ctrl.featureProperties = null;
          ctrl.isEditing = false;
          if (ctrl.editingZone) {
            ctrl.editingZone.isEditing = false;
            ctrl.editingZone = null;
          }
          nvMapboxGL.resetActivePolygonData(map);
        });
      }
    }

    function createClick(event) {
      $log.debug('CREATE click');
      if (!ctrl.isCreating) {
        return;
      }

      if (createPolyCoords.length === 0) {
        createPolyCoords.push(event.lngLat.toArray());
        createPolyCoords.push(event.lngLat.toArray());
      } else {
        createPolyCoords.splice(createPolyCoords.length - 1, 0, event.lngLat.toArray());
      }

      nvMapboxGL.setCreateLineData(map, createPolyCoords);
    }

    function deleteClick(event) {
      $log.debug('DELETE click');
      if (!ctrl.isDeleting) {
        return;
      }

      const features = map.queryRenderedFeatures(
                nvMapboxGL.computeBoundingBox(event.point, 3),
                { layers: [nvMapboxGL.LAYERS.ACTIVE_POLY_VERT_STROKE] }
            );

      if (features.length) {
        nvMapboxGL.removeVertex(map, features[0]);

        map.dragPan.enable();
        cvs.style.cursor = '';

        const properties = ctrl.featureProperties;
        if (nvMapboxGL.existsPolygon(properties.id)) {
          $timeout(() => {
            nvMapboxGL.updateStablePolyData(map, properties.id, properties.name);
            nvMapboxGL.updateStableLineData(map, properties.id);
            nvMapboxGL.setActivePolygonData(map, properties.id);
            ctrl.editingZone.vertexCount = calculateVertices(ctrl.editingZone.polygon);
            ctrl.editingZone.isOptimized = false;
            delete ctrl.editingZone.oldCoordinate;
          });
        } else {
          $timeout(() => {
            ctrl.isDeleting = false;
            ctrl.featureProperties = null;
            ctrl.hasAnyPoly = nvMapboxGL.hasAnyPolygon();
          });

          nvMapboxGL.resetActivePolygonData(map);
        }
      }
    }

    function chooseClick(event) {
      $log.debug('CHOOSE click');
      if (!ctrl.isRevealMode) {
        return;
      }

      ctrl.isRevealMode = false;

      map.off('mousemove', chooseMousemove);
      map.on('click', normalClick);
      map.on('mousedown', normalMousedown);
      map.on('mousemove', normalMousemove);
      map.on('mouseup', normalMouseup);

      nvMapboxGL.removeChooseCircleArea(map);

      const payload = [{
        latitude: event.lngLat.lat,
        longitude: event.lngLat.lng,
        show_polygon: true,
      }];

      Zone.searchZoneByCoords(payload).then((responses) => {
        const selectedZonesId = _.map(ctrl.selectedZones, z => (z.id));
        const zoneToAdd = _.filter(responses, (z) => {
          if (!_.isEmpty(z) && !!z.id && _.indexOf(selectedZonesId, z.id) < 0) {
            z.vertexCount = calculateVertices(z.polygon);
            if (getTotalVertexFromZone() < ctrl.maximumVertexCount) {
              if (z.polygon) {
                z.featureId = nvMapboxGL.addPolygon(map,
                  nvMapboxGL.toMultiPolygonGeometry(z.polygon));
              }
              z.marker = nvMapboxGL.createMarker(map, { lat: z.latitude, lng: z.longitude });
            }
            return z;
          }
          nvToast.success(nvTranslate.instant('container.zones.polygon.there-are-no-zone-covering-that-area'));
        });
        ctrl.selectedZones = _.concat(ctrl.selectedZones, zoneToAdd);
        panAndZoom();
        ctrl.hasAnyPoly = nvMapboxGL.hasAnyPolygon();
      });
    }

        // //////////////////////////////////////////////////
        // DBLCLICK EVENTS
        // //////////////////////////////////////////////////

    function createDblclick(event) {
      $log.debug('CREATE dblclick');
      if (!ctrl.isCreating) {
        return;
      }

      createPolyCoords.pop();
      createPolyCoords.pop();
      createPolyCoords.push(createPolyCoords[0]); // push the reference

      if (createPolyCoords.length < 4) {
        createPolyCoords = nvMapboxGL.createSquare(map, event, SQUARE_LENGTH);
      }

      const newPolyId = nvMapboxGL.addPolygon(map, {
        geojson: {
          type: 'MultiPolygon',
          coordinates: [[createPolyCoords]],
        },
      });

      nvMapboxGL.setPolyActive(map, newPolyId);
      nvMapboxGL.setActivePolygonData(map, newPolyId);
      nvMapboxGL.resetCreateLineData(map);

      $timeout(() => {
        ctrl.isCreating = false;
        ctrl.featureProperties = { id: newPolyId, name: newPolyId };
        ctrl.hasAnyPoly = true;
        ctrl.editingZone = _.merge(ctrl.editingZone, {
          featureId: newPolyId,
          polygon: {
            geometry: nvMapboxGL.getVertGeometry(newPolyId),
            type: 'Feature',
            properties: {},
          },
          edited: true,
        });
        ctrl.editingZone.vertexCount = calculateVertices(ctrl.editingZone.polygon);
      });
    }

        // //////////////////////////////////////////////////
        // MOUSEDOWN EVENTS
        // //////////////////////////////////////////////////

    function normalMousedown(event) {
      $log.debug('NORMAL mousedown');
      if (!ctrl.isSelectedPoly()) {
        return;
      }

      queryFeatures(event, isVert, isMockVert);
      function isVert(feature, evt) {
        if (event.originalEvent && event.originalEvent.button === 2) {
          ctrl.editingZone.isOptimized = false;
          delete ctrl.editingZone.oldCoordinate;
          ctrl.isDeleting = true;
          deleteClick(evt);
          ctrl.editingZone.edited = true;
        } else {
          ctrl.editingZone.isOptimized = false;
          delete ctrl.editingZone.oldCoordinate;
          map.off('mousemove', normalMousemove);
          map.on('mousemove', normalMousemoveDragging);
          normalDragFeatures = nvMapboxGL.initDragFeatures(
                      feature.properties.pId,
                      feature.properties.vId
                  );
          nvMapboxGL.setActiveLineData(map, normalDragFeatures);
          ctrl.editingZone.edited = true;
        }
      }

      function isMockVert(feature) {
        $timeout(() => {
          map.off('mousemove', normalMousemove);
          map.on('mousemove', normalMousemoveDragging);
          normalDragFeatures = nvMapboxGL.initDragFeatures(
                    feature.properties.pId,
                    nvMapboxGL.insertVertex(feature)
                );
          nvMapboxGL.setActiveLineData(map, normalDragFeatures);
          ctrl.editingZone.edited = true;
          ctrl.editingZone.vertexCount = calculateVertices(ctrl.editingZone.polygon);
          ctrl.editingZone.isOptimized = false;
        });
      }
    }

    function adjustMousedown(event) {
      $log.debug('ADJUST mousedown');
      if (!ctrl.hasAnImage) {
        return;
      }

      const features = map.queryRenderedFeatures(
                nvMapboxGL.computeBoundingBox(event.point, 3),
                { layers: [nvMapboxGL.LAYERS.OVRLAY_IMAGE_VERT_STROKE] }
            );

      cvs.style.cursor = '';
      adjustFeature = null;

      if (features.length) {
        const layerId = features[0].layer.id;
        if (layerId === nvMapboxGL.LAYERS.OVRLAY_IMAGE_VERT_STROKE) {
          map.dragPan.disable();
          map.off('mousemove', adjustMousemove);
          map.on('mousemove', adjustMousemoveDragging);
          adjustFeature = features[0];
        } else {
          map.dragPan.enable();
        }
      } else {
        map.dragPan.enable();
      }
    }

        // //////////////////////////////////////////////////
        // MOUSEMOVE EVENTS
        // //////////////////////////////////////////////////

    function normalMousemove(event) {
      if (!ctrl.isSelectedPoly()) {
        return;
      }

      queryFeatures(event, isVert, isMockVert);

      function isVert() {
        cvs.style.cursor = 'move';
      }

      function isMockVert() {
        cvs.style.cursor = 'cell';
      }
    }

    function normalMousemoveDragging(event) {
      const features = map.queryRenderedFeatures(
                nvMapboxGL.computeBoundingBox(event.point, THRESHOLD),
                { layers: nvMapboxGL.LAYER_GROUPS.POLY_FILL_STROKE }
            );

      const pId = ctrl.featureProperties.id;
      const point = L.point(event.point.x, event.point.y);
      const coordinates = nvMapboxGL.snapCoordinates(map, event, point, features, pId, THRESHOLD);

      normalDragFeatures[0].geometry.coordinates[0] = coordinates.lng || coordinates[0];
      normalDragFeatures[0].geometry.coordinates[1] = coordinates.lat || coordinates[1];
      nvMapboxGL.setActiveLineData(map, normalDragFeatures);
    }

    function createMousemove(event) {
      if (!ctrl.isCreating || createPolyCoords.length === 0) {
        return;
      }

      const lastCoordinates = createPolyCoords[createPolyCoords.length - 1];
      lastCoordinates[0] = event.lngLat.lng;
      lastCoordinates[1] = event.lngLat.lat;
      nvMapboxGL.setCreateLineData(map, createPolyCoords);
    }

    function deleteMousemove(event) {
      if (!ctrl.isDeleting) {
        return;
      }

      const features = map.queryRenderedFeatures(
                nvMapboxGL.computeBoundingBox(event.point, 3),
                { layers: [nvMapboxGL.LAYERS.ACTIVE_POLY_VERT_STROKE] }
            );

      if (features.length) {
        map.dragPan.disable();
        cvs.style.cursor = 'pointer';
      } else {
        map.dragPan.enable();
        cvs.style.cursor = '';
      }
    }

    function chooseMousemove(event) {
      if (!ctrl.isRevealMode) {
        return;
      }

      radiusLat = event.lngLat.lat;
      nvMapboxGL.setCircleData(map, [event.lngLat.lng, radiusLat]);
      nvMapboxGL.setChooseCircleRadius(map, ctrl.radius * 1000, radiusLat);
    }

    function adjustMousemove(event) {
      const features = map.queryRenderedFeatures(
                nvMapboxGL.computeBoundingBox(event.point, 3),
                { layers: [nvMapboxGL.LAYERS.OVRLAY_IMAGE_VERT_STROKE] }
            );

      cvs.style.cursor = '';

      if (features.length) {
        const layerId = features[0].layer.id;
        if (layerId === nvMapboxGL.LAYERS.OVRLAY_IMAGE_VERT_STROKE) {
          map.dragPan.disable();
          cvs.style.cursor = 'move';
        } else {
          map.dragPan.enable();
        }
      } else {
        map.dragPan.enable();
      }
    }

    function adjustMousemoveDragging(event) {
      const idx = adjustFeature.properties.id;
      resizeCoords[idx][0] = event.lngLat.lng;
      resizeCoords[idx][1] = event.lngLat.lat;

      switch (idx) {
        case 0:
          resizeCoords[1][1] = event.lngLat.lat;
          resizeCoords[3][0] = event.lngLat.lng;
          break;
        case 1:
          resizeCoords[0][1] = event.lngLat.lat;
          resizeCoords[2][0] = event.lngLat.lng;
          break;
        case 2:
          resizeCoords[1][0] = event.lngLat.lng;
          resizeCoords[3][1] = event.lngLat.lat;
          break;
        case 3:
          resizeCoords[2][1] = event.lngLat.lat;
          resizeCoords[0][0] = event.lngLat.lng;
          break;
        default:
          break;
      }

      nvMapboxGL.updateImageData(map, resizeCoords);
    }

        // //////////////////////////////////////////////////
        // MOUSEUP EVENTS
        // //////////////////////////////////////////////////

    function normalMouseup() {
      $log.debug('NORMAL mouseup');
      if (!ctrl.isSelectedPoly()) {
        return;
      }

      cvs.style.cursor = '';
      map.dragPan.enable();

      map.off('mousemove', normalMousemoveDragging);
      map.on('mousemove', normalMousemove);

      normalDragFeatures = [];
      nvMapboxGL.setActiveLineData(map, normalDragFeatures);

      const properties = ctrl.featureProperties;
      if (nvMapboxGL.existsPolygon(properties.id)) {
        $timeout(() => {
          nvMapboxGL.updateStablePolyData(map, properties.id, properties.name);
          nvMapboxGL.updateStableLineData(map, properties.id);
          nvMapboxGL.setActivePolygonData(map, properties.id);
        });
      } else {
        $timeout(() => {
          ctrl.isDeleting = false;
          ctrl.featureProperties = null;
          ctrl.hasAnyPoly = nvMapboxGL.hasAnyPolygon();
        });
        nvMapboxGL.resetActivePolygonData(map);
      }
    }

    function adjustMouseup() {
      $log.debug('ADJUST mouseup');

      cvs.style.cursor = '';

      if (adjustFeature) {
        map.dragPan.enable();
        map.off('mousemove', adjustMousemoveDragging);
        map.on('mousemove', adjustMousemove);
      }

      adjustFeature = null;
    }

        // //////////////////////////////////////////////////
        // UI BUTTON EVENTS
        // //////////////////////////////////////////////////

    function revealMode() {
      ctrl.isRevealMode = true;
      if (ctrl.isRevealMode) {
        map.off('click', normalClick);
        map.off('mousedown', normalMousedown);
        map.off('mousemove', normalMousemove);
        map.off('mouseup', normalMouseup);

        map.once('click', chooseClick);
        map.on('mousemove', chooseMousemove);

        nvMapboxGL.initChooseCircleArea(map, { radius: ctrl.radius * 1000 });
      } else {
        map.on('click', normalClick);
        map.on('mousedown', normalMousedown);
        map.on('mousemove', normalMousemove);
        map.on('mouseup', normalMouseup);
      }
    }

    function onClickDeletePolygon($event, zone) {
      return nvDialog.confirmDelete($event, {
        title: $translate.instant('container.zones.polygon.delete-polygon'),
        content: $translate.instant('container.zones.polygon.delete-polygon-text'),
        ok: $translate.instant('commons.delete'),
        cancel: $translate.instant('commons.go-back'),
      }).then(onDelete);

      function onDelete() {
        nvMapboxGL.removePolygon(map, +zone.featureId);
        if (zone.marker) {
          nvMapboxGL.removeMarker(zone.marker);
        }
        ctrl.isDeleting = false;
        ctrl.isCreating = true;
        ctrl.hasAnyPoly = nvMapboxGL.hasAnyPolygon();
        nvMapboxGL.resetActivePolygonData(map);
        _.each(ctrl.selectedZones, (z) => {
          if (zone.id === z.id) {
            z.polygon = null;
            z.vertexCount = 0;
            z.edited = true;
          }
        });
      }
    }

    function onClickClearAllPolygons() {
      nvMapboxGL.removeAllPolygons(map);
      ctrl.isDeleting = false;
      ctrl.isCreating = false;
      ctrl.featureProperties = null;
      ctrl.hasAnyPoly = false;
      nvMapboxGL.resetActivePolygonData(map);
      _.each(ctrl.selectedZones, (z) => {
        if (z.marker) {
          nvMapboxGL.removeMarker(z.marker);
        }
      });
      ctrl.selectedZones.length = 0;
    }

    function undoOptimize() {
      ctrl.editingZone.polygon.geometry.coordinates[0][0] = ctrl.editingZone.oldCoordinate;
      if (ctrl.editingZone.isOptimized) {
        ctrl.editingZone.isOptimized = false;
        ctrl.editingZone.isOptimized = false;
      }
      delete ctrl.editingZone.oldCoordinate;
      redrawPolygon();
    }

    function optimizeVertices($event, zone) {
      ctrl.editingZone.oldCoordinate = angular.copy(zone.polygon.geometry.coordinates[0][0]);
      const result = nvMapboxGL.minimizeVertex(zone.polygon.geometry.coordinates[0][0]);
      ctrl.editingZone.polygon.geometry.coordinates[0][0] = result.newCoords;
      ctrl.editingZone.isOptimized = true;
      ctrl.editingZone.edited = true;
      redrawPolygon();
    }

    function redrawPolygon() {
      // redraw the polygon
      $timeout(() => {
        nvMapboxGL.removePolygon(map, ctrl.editingZone.featureId);
        drawPolygonFromZone(ctrl.editingZone);
      }).then(() => {
        ctrl.editingZone.vertexCount = calculateVertices(ctrl.editingZone.polygon);
      });
    }

    function openEditCoordinateDialog($event, zone) {
      nvDialog.showSingle($event, {
        templateUrl: 'views/container/zones/dialog/set-point-coordinates/set-point-coordinates.dialog.html',
        cssClass: 'set-point-coordinate-dialog',
        scope: _.assign(nvEditModelDialog.scopeTemplate({ onSave: onSave }), {
          fields: {
            latitude: { displayName: 'latitude', value: zone.latitude },
            longitude: { displayName: 'longitude', value: zone.longitude },
          },
        },
      ),
      });

      function onSave(data) {
        ctrl.editingZone = _.merge(ctrl.editingZone, data);
        ctrl.editingZone.edited = true;
        if (ctrl.editingZone.marker) {
          ctrl.editingZone.marker.setLngLat([data.longitude, data.latitude]);
        } else {
          ctrl.editingZone.marker = nvMapboxGL.createMarker(map,
            { lat: data.latitude, lng: data.longitude });
        }
      }
    }

        // //////////////////////////////////////////////////
        // MISC HELPER METHODS
        // //////////////////////////////////////////////////


    function getTotalVertexFromZone() {
      return _.reduce(ctrl.selectedZones, (sum, z) => (sum + z.vertexCount), 0);
    }

    function saveChanges(event) {
      return nvDialog.showSingle(event, {
        templateUrl: 'views/container/zones/dialog/save-zone-polygon-confirm-dialog/save-zone-polygon-confirm-dialog.html',
        cssClass: 'set-point-coordinate-dialog',
        scope: _.assign(nvEditModelDialog.scopeTemplate({ onSave: onSave }), {
          zones: ctrl.selectedZones,
        },
      ),
      });

      function onSave() {
        ctrl.savingState = 'waiting';
        const promisesArray = _.map(ctrl.selectedZones, (z) => {
            // omit some field
          const payload = _.omit(z, ['vertexCount', 'marker']);
          if (z.polygon) {
            nvMapboxGL.convertToPolygonGeometry(z.polygon.geometry);
          }
          return Zone.updateRoutingZone(z.id, payload);
        });
        return $q.all(promisesArray).then(success, error);

        function success() {
          nvNavGuard.unlock();
          ctrl.savingState = 'idle';
          $state.go('container.zones.all', { refresh: true });
        }

        function error(err) {
          $log.debug(err);
        }
      }
    }

    function queryFeatures(event, isVertFn, isMockVertFn) {
      const features = map.queryRenderedFeatures(
                nvMapboxGL.computeBoundingBox(event.point, 3),
                { layers:
                  [nvMapboxGL.LAYERS.ACTIVE_POLY_VERT_STROKE, nvMapboxGL.LAYERS.ACTIVE_MOCK_VERT],
                                }
            );

      cvs.style.cursor = '';

      if (features.length) {
        const layerId = features[0].layer.id;
        if (layerId === nvMapboxGL.LAYERS.ACTIVE_POLY_VERT_STROKE) {
          map.dragPan.disable();
          isVertFn(features[0], event);
        } else if (layerId === nvMapboxGL.LAYERS.ACTIVE_MOCK_VERT) {
          map.dragPan.disable();
          isMockVertFn(features[0]);
        } else {
          map.dragPan.enable();
        }
      } else {
        map.dragPan.enable();
      }
    }

    function isSelectedPoly() {
      return !_.isNull(ctrl.featureProperties);
    }

    function onClickback() {
      nvNavGuard.unlock();
      $state.go('container.zones.all', { refresh: true });
    }

    function isLoading(flag) {
      $scope.contentLoading.backdropLoading = flag;
    }


    // helper for find sw and ne point of of a given polygons
    /**
     *
     * @param {*} array or single object of polygon
     */
    function findSwAndNePoint(selectedZones) {
      const zones = _.castArray(selectedZones);
      let west;
      let north;
      let east;
      let south;
      if (zones.length === 1 && zones[0].polygon === null) {
        return false;
      }
      _.each(zones, (p) => {
        let coordinates = [];
        if (p.polygon) {
          coordinates = getCoordinates(p.polygon);
        } else if (zones.length > 1) {
          coordinates = [[p.longitude, p.latitude]];
        }

        _.each(coordinates, (c) => {
          if (!west || west > c[0]) {
            west = c[0];
          }
          if (!east || east < c[0]) {
            east = c[0];
          }

          if (!north || north < c[1]) {
            north = c[1];
          }

          if (!south || south > c[1]) {
            south = c[1];
          }
        });
      });

      return {
        southWest: [west, south],
        northEast: [east, north],
      };


      function getCoordinates(polygon) {
        const geometry = polygon.geometry;
        if (!geometry) {
          return [];
        }
        const coordinates = _.get(geometry, 'coordinates', []);
        if (geometry.type === 'Polygon') {
          return coordinates[0];
        } else if (geometry.type === 'MultiPolygon') {
          return coordinates[0][0];
        }
        return [];
      }
    }
  }
}());
