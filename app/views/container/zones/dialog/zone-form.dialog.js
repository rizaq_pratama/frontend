(function controller() {
  angular
    .module('nvOperatorApp.controllers')
    .controller('ZoneFormDialogController', ZoneFormDialogController);

  ZoneFormDialogController.$inject = [
    '$q', 'zoneData', '$mdDialog', 'nvTranslate', 'Zone',
    'Hub', '$scope', 'nvToast',
  ];

  function ZoneFormDialogController(
    $q, zoneData, $mdDialog, nvTranslate, Zone,
    Hub, $scope, nvToast
  ) {
    // variables
    const ctrl = this;
    ctrl.title = null;
    ctrl.submitButtonTitle = null;
    ctrl.formSubmitState = 'idle';
    ctrl.hubsSelectionOptions = [];
    ctrl.ZONE_FIELDS = Zone.FIELDS;

    // functions
    ctrl.getAll = getAll;
    ctrl.onSave = onSave;
    ctrl.isAbleToSubmit = isAbleToSubmit;
    ctrl.onCancel = onCancel;

    // start
    initialize();

    // functions details
    function initialize() {
      ctrl.title = nvTranslate.instant('container.zones.add-zone');
      ctrl.submitButtonTitle = nvTranslate.instant('commons.submit');
      if (_.size(zoneData) > 0) {
        ctrl.title = nvTranslate.instant('container.zones.edit-zone');
        ctrl.submitButtonTitle = nvTranslate.instant('commons.update');
      }

      ctrl.zone = _.defaults(_.cloneDeep(zoneData), {
        id: null,
        name: '',
        short_name: '',
        hub_id: null,
        description: '',
        latitude: null,
        longitude: null,
      });
    }

    function getAll() {
      return Hub.read().then(success, $q.reject);

      function success(response) {
        ctrl.hubsSelectionOptions = Hub.toOptions(response);
      }
    }

    function onSave() {
      ctrl.formSubmitState = 'waiting';

      // payload
      const payload = {
        name: ctrl.zone.name,
        short_name: ctrl.zone.short_name,
        hub_id: ctrl.zone.hub_id,
        description: ctrl.zone.description,
        latitude: ctrl.zone.latitude,
        longitude: ctrl.zone.longitude,
      };

      if (ctrl.zone.id > 0) {
        // edit mode
        Zone.updateRoutingZone(ctrl.zone.id, payload).then(success, failure);
      } else {
        // create mode
        Zone.createRoutingZone(payload).then(success, failure);
      }

      function success(response) {
        if (ctrl.zone.id > 0) {
          nvToast.success(nvTranslate.instant('container.zones.zone-updated'));
        } else {
          nvToast.success(nvTranslate.instant('container.zones.zone-created'));
        }

        ctrl.formSubmitState = 'idle';
        $mdDialog.hide(response);
      }

      function failure() {
        ctrl.formSubmitState = 'idle';
      }
    }

    function isAbleToSubmit() {
      return $scope.modelForm && $scope.modelForm.$valid;
    }

    function onCancel() {
      $mdDialog.cancel();
    }
  }
}());
