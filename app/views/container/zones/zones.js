(function controller() {
  angular
      .module('nvOperatorApp.controllers')
      .controller('ZonesController', ZonesController);

  ZonesController.$inject = [
    '$scope', '$timeout', 'Tag', '$q', 'Hub', 'Zone', 'nvZonesService'
  ];

  function ZonesController(
      $scope, $timeout, Tag, $q, Hub, Zone, nvZones
  ) {
    
    // variables (that also use in html)
    const ctrl = this;

    ctrl.contentLoading = {
      status: true,
      message: 'container.zones.load-zones',
      backdropLoading: false,
    };

    // variables and functions (that will use by child controllers)
    $scope.contentLoading = ctrl.contentLoading;
    $scope.windowSizeChanged = windowSizeChanged;

    // functions
    // Initialization
    ctrl.windowSizeChanged = windowSizeChanged;

    // start
    initialize();

    // function details
    // //////////////////////////////////////////////////////////////////////////////
    // Initialization //////////////////////////////////////////////////////////////
    // //////////////////////////////////////////////////////////////////////////////
    function initialize() {
      nvZones.initializeVariables();


      return $q.all([
        Hub.read(), Zone.read(),
      ]).then(success, failure);

      function success(response) {
        nvZones.hubs = response[0] || [];
        nvZones.zones = response[1] || [];

        ctrl.contentLoading.status = false;
        ctrl.contentLoading.message = '';
      }

      function failure() {

      }
    }

    function windowSizeChanged() {
      $timeout(() => {
        nvZones.contentHeight = $scope.$windowHeight - $('.action-toolbar').outerHeight() - $('.nv-container > md-toolbar').outerHeight();
        $('.content').height(`${nvZones.contentHeight}px`);
      });
    }
  }
}());
