(function controller() {
  angular
    .module('nvOperatorApp.controllers')
    .controller('PolygonDrawingController', PolygonDrawingController);

  PolygonDrawingController.$inject = [
    '$scope',
    '$http',
    '$timeout',
    '$log',
    '$window',
    'nvMapboxGL',
    'nvFileUtils',
    'nvRequestUtils',
    'nvDialog',
    'nvEditModelDialog',
    'nvButtonFilePickerType',
    '$state',
    'nvToast',
    'nvTranslate',
  ];

  function PolygonDrawingController(
    $scope, $http, $timeout, $log, $window,
    nvMapboxGL, nvFileUtils, nvRequestUtils, nvDialog, nvEditModelDialog, nvButtonFilePickerType,
    $state, nvToast, nvTranslate
  ) {
    const ctrl = this;

    const PAGE_DISABLED = true;
    if (PAGE_DISABLED) {
      nvToast.error(nvTranslate.instant('commons.page-disabled', { page: 'polygon drawing' }));
      $state.go('container.order.list');
      return;
    }
    ctrl.nvButtonFilePickerType = nvButtonFilePickerType;
    ctrl.UPLOAD_TYPE = nvButtonFilePickerType.join(nvButtonFilePickerType.JSON, nvButtonFilePickerType.GEOJSON);
    ctrl.isCreating = false; // when the map is in CREATE mode
    ctrl.isDeleting = false; // when the map is in DELETE mode
    ctrl.isChoosing = false; // when the map is in CHOOSE mode
    ctrl.isResizing = false; // when the map is in RESIZE mode
    ctrl.hasAnyPoly = false; // when the map has at least one polygon
    ctrl.hasAnImage = false; // when the map has an image loaded
    ctrl.featureProperties = null; // the properties of the selected feature (polygon)
    ctrl.radius = 3; // the radius (in km) of the CHOOSE mode area
    ctrl.opacity = 0.7; // the opacity of the RESIZE mode image

    ctrl.onClickPricingZones = onClickPricingZones;
    ctrl.onClickDemo = onClickDemo;
    ctrl.onClickDeletePolygon = onClickDeletePolygon;
    ctrl.onClickClearAllPolygons = onClickClearAllPolygons;
    ctrl.onClickEditPolygon = onClickEditPolygon;
    ctrl.onClickCenterMap = onClickCenterMap;
    ctrl.onChangeRadius = onChangeRadius;
    ctrl.onChangeOpacity = onChangeOpacity;
    ctrl.isSelectedPoly = isSelectedPoly;

    const THRESHOLD = 20;
    const SQUARE_LENGTH = 40;
    const DEFAULT_LNG = 112.759060;
    const DEFAULT_LAT = -7.286512;

    let isMapLoaded = false;
    let normalDragFeatures = []; // holds the features currently involved in dragging
    let adjustFeature = null;
    let createPolyCoords = []; // holds the coordinates of the polygon currently being created
    let resizeCoords = []; // holds the coordinates of the image currently being resized
    let radiusLat = 0;
    let map;
    let cvs;

    $timeout(() => {
      map = nvMapboxGL.initMap({ center: [DEFAULT_LNG, DEFAULT_LAT], zoom: 12 });
      cvs = map.getCanvasContainer();
      map.once('load', load);
    });

    $scope.$watch('ctrl.isCreating', (isCreating) => {
      if (!isMapLoaded) {
        return;
      }

      createPolyCoords = [];

      if (isCreating) {
        // turn off NORMAL functions
        map.doubleClickZoom.disable();
        map.off('click', normalClick);
        map.off('mousedown', normalMousedown);
        map.off('mousemove', normalMousemove);
        map.off('mouseup', normalMouseup);

        // turn on CREATE functions
        map.on('click', createClick);
        map.on('dblclick', createDblclick);
        map.on('mousemove', createMousemove);
      } else {
        // turn off CREATE functions
        map.off('click', createClick);
        map.off('dblclick', createDblclick);
        map.off('mousemove', createMousemove);

        // turn on NORMAL functions
        map.doubleClickZoom.enable();
        map.on('click', normalClick);
        map.on('mousedown', normalMousedown);
        map.on('mousemove', normalMousemove);
        map.on('mouseup', normalMouseup);
      }
    });

    $scope.$watch('ctrl.isDeleting', (isDeleting) => {
      if (!isMapLoaded) {
        return;
      }

      if (isDeleting) {
        // turn off NORMAL functions
        map.off('click', normalClick);
        map.off('mousemove', normalMousemove);

        // turn on DELETE functions
        map.on('click', deleteClick);
        map.on('mousemove', deleteMousemove);
      } else {
        // turn off DELETE functions
        map.off('click', deleteClick);
        map.off('mousemove', deleteMousemove);

        // turn on NORMAL functions
        map.on('click', normalClick);
        map.on('mousemove', normalMousemove);
      }
    });

    $scope.$watch('ctrl.isResizing', (isResizing) => {
      if (!isMapLoaded) {
        return;
      }

      if (isResizing) {
        // turn off NORMAL functions
        map.off('click', normalClick);
        map.off('mousedown', normalMousedown);
        map.off('mousemove', normalMousemove);
        map.off('mouseup', normalMouseup);

        // turn on ADJUST functions
        map.on('mousedown', adjustMousedown);
        map.on('mousemove', adjustMousemove);
        map.on('mouseup', adjustMouseup);

        nvMapboxGL.addImageAdjustLayers(map);
      } else {
        // turn on NORMAL functions
        map.on('click', normalClick);
        map.on('mousedown', normalMousedown);
        map.on('mousemove', normalMousemove);
        map.on('mouseup', normalMouseup);

        // turn off ADJUST functions
        map.off('mousedown', adjustMousedown);
        map.off('mousemove', adjustMousemove);
        map.off('mouseup', adjustMouseup);

        nvMapboxGL.removeImageAdjustLayers(map);
      }
    });

    $scope.$on('$destroy', nvMapboxGL.destroy);

    // //////////////////////////////////////////////////
    // LOAD EVENT
    // //////////////////////////////////////////////////

    function load() {
      $log.debug('load');
      map.resize();
      nvMapboxGL.initPolygonDrawing(map);
      isMapLoaded = true;
      map.on('click', normalClick);
      map.on('contextmenu', event => (console.log(event)));
      map.on('mousedown', normalMousedown);
      map.on('mousemove', normalMousemove);
      map.on('mouseup', normalMouseup);
    }

    // //////////////////////////////////////////////////
    // CLICK EVENTS
    // //////////////////////////////////////////////////

    function normalClick(event) {
      $log.debug('NORMAL click');
      if (ctrl.isSelectedPoly()) {
        nvMapboxGL.setPolyInactive(map, ctrl.featureProperties.id);
      }

      const features = map.queryRenderedFeatures(
        event.point,
        { layers: nvMapboxGL.LAYER_GROUPS.POLY_FILL }
      );

      if (features.length) {
        const polySrcId = features[0].properties.id;

        $timeout(() => {
          ctrl.featureProperties = features[0].properties;
        });

        nvMapboxGL.setPolyActive(map, polySrcId);
        nvMapboxGL.setActivePolygonData(map, polySrcId);
      } else {
        $timeout(() => {
          ctrl.featureProperties = null;
        });

        nvMapboxGL.resetActivePolygonData(map);
      }
    }

    function createClick(event) {
      $log.debug('CREATE click');
      if (!ctrl.isCreating) {
        return;
      }

      if (createPolyCoords.length === 0) {
        createPolyCoords.push(event.lngLat.toArray());
        createPolyCoords.push(event.lngLat.toArray());
      } else {
        createPolyCoords.splice(createPolyCoords.length - 1, 0, event.lngLat.toArray());
      }

      nvMapboxGL.setCreateLineData(map, createPolyCoords);
    }

    function deleteClick(event) {
      $log.debug('DELETE click');
      if (!ctrl.isDeleting) {
        return;
      }

      const features = map.queryRenderedFeatures(
        nvMapboxGL.computeBoundingBox(event.point, 3),
        { layers: [nvMapboxGL.LAYERS.ACTIVE_POLY_VERT_STROKE] }
      );

      if (features.length) {
        nvMapboxGL.removeVertex(map, features[0]);

        map.dragPan.enable();
        cvs.style.cursor = '';

        const properties = ctrl.featureProperties;
        if (nvMapboxGL.existsPolygon(properties.id)) {
          nvMapboxGL.updateStablePolyData(map, properties.id, properties.name);
          nvMapboxGL.updateStableLineData(map, properties.id);
          nvMapboxGL.setActivePolygonData(map, properties.id);
        } else {
          $timeout(() => {
            ctrl.isDeleting = false;
            ctrl.featureProperties = null;
            ctrl.hasAnyPoly = nvMapboxGL.hasAnyPolygon();
          });

          nvMapboxGL.resetActivePolygonData(map);
        }
      }
    }

    function chooseClick(event) {
      $log.debug('CHOOSE click');
      if (!ctrl.isChoosing) {
        return;
      }

      ctrl.isChoosing = false;

      map.off('mousemove', chooseMousemove);
      map.on('click', normalClick);
      map.on('mousedown', normalMousedown);
      map.on('mousemove', normalMousemove);
      map.on('mouseup', normalMouseup);

      nvMapboxGL.removeChooseCircleArea(map);

      const params = nvRequestUtils.queryParam({
        lat: event.lngLat.lat,
        lng: event.lngLat.lng,
        radius: ctrl.radius,
        with_polygon: true,
      });

      $http.get(`addressing/pricing-zones/search?${params}`).then((response) => {
        nvMapboxGL.addPolygons(map, nvMapboxGL.toMultiPolygonGeometries(response.data));
        ctrl.hasAnyPoly = nvMapboxGL.hasAnyPolygon();
      });
    }

    // //////////////////////////////////////////////////
    // DBLCLICK EVENTS
    // //////////////////////////////////////////////////

    function createDblclick(event) {
      $log.debug('CREATE dblclick');
      if (!ctrl.isCreating) {
        return;
      }

      createPolyCoords.pop();
      createPolyCoords.pop();
      createPolyCoords.push(createPolyCoords[0]); // push the reference

      if (createPolyCoords.length < 4) {
        createPolyCoords = nvMapboxGL.createSquare(map, event, SQUARE_LENGTH);
      }

      const newPolyId = nvMapboxGL.addPolygon(map, {
        geojson: {
          type: 'MultiPolygon',
          coordinates: [[createPolyCoords]],
        },
      });

      nvMapboxGL.setPolyActive(map, newPolyId);
      nvMapboxGL.setActivePolygonData(map, newPolyId);
      nvMapboxGL.resetCreateLineData(map);

      $timeout(() => {
        ctrl.isCreating = false;
        ctrl.featureProperties = { id: newPolyId, name: newPolyId };
        ctrl.hasAnyPoly = true;
      });
    }

    // //////////////////////////////////////////////////
    // MOUSEDOWN EVENTS
    // //////////////////////////////////////////////////

    function normalMousedown(event) {
      $log.debug('NORMAL mousedown');
      if (!ctrl.isSelectedPoly()) {
        return;
      }

      queryFeatures(event, isVert, isMockVert);

      function isVert(feature) {
        map.off('mousemove', normalMousemove);
        map.on('mousemove', normalMousemoveDragging);
        normalDragFeatures = nvMapboxGL.initDragFeatures(
          feature.properties.pId,
          feature.properties.vId
        );
        nvMapboxGL.setActiveLineData(map, normalDragFeatures);
      }

      function isMockVert(feature) {
        map.off('mousemove', normalMousemove);
        map.on('mousemove', normalMousemoveDragging);
        normalDragFeatures = nvMapboxGL.initDragFeatures(
          feature.properties.pId,
          nvMapboxGL.insertVertex(feature)
        );
        nvMapboxGL.setActiveLineData(map, normalDragFeatures);
      }
    }

    function adjustMousedown(event) {
      $log.debug('ADJUST mousedown');
      if (!ctrl.hasAnImage) {
        return;
      }

      const features = map.queryRenderedFeatures(
        nvMapboxGL.computeBoundingBox(event.point, 3),
        { layers: [nvMapboxGL.LAYERS.OVRLAY_IMAGE_VERT_STROKE] }
      );

      cvs.style.cursor = '';
      adjustFeature = null;

      if (features.length) {
        const layerId = features[0].layer.id;
        if (layerId === nvMapboxGL.LAYERS.OVRLAY_IMAGE_VERT_STROKE) {
          map.dragPan.disable();
          map.off('mousemove', adjustMousemove);
          map.on('mousemove', adjustMousemoveDragging);
          adjustFeature = features[0];
        } else {
          map.dragPan.enable();
        }
      } else {
        map.dragPan.enable();
      }
    }

    // //////////////////////////////////////////////////
    // MOUSEMOVE EVENTS
    // //////////////////////////////////////////////////

    function normalMousemove(event) {
      if (!ctrl.isSelectedPoly()) {
        return;
      }

      queryFeatures(event, isVert, isMockVert);

      function isVert() {
        cvs.style.cursor = 'move';
      }

      function isMockVert() {
        cvs.style.cursor = 'cell';
      }
    }

    function normalMousemoveDragging(event) {
      const features = map.queryRenderedFeatures(
        nvMapboxGL.computeBoundingBox(event.point, THRESHOLD),
        { layers: nvMapboxGL.LAYER_GROUPS.POLY_FILL_STROKE }
      );

      const pId = ctrl.featureProperties.id;
      const point = L.point(event.point.x, event.point.y);
      const coordinates = nvMapboxGL.snapCoordinates(map, event, point, features, pId, THRESHOLD);

      normalDragFeatures[0].geometry.coordinates[0] = coordinates.lng || coordinates[0];
      normalDragFeatures[0].geometry.coordinates[1] = coordinates.lat || coordinates[1];
      nvMapboxGL.setActiveLineData(map, normalDragFeatures);
    }

    function createMousemove(event) {
      if (!ctrl.isCreating || createPolyCoords.length === 0) {
        return;
      }

      const lastCoordinates = createPolyCoords[createPolyCoords.length - 1];
      lastCoordinates[0] = event.lngLat.lng;
      lastCoordinates[1] = event.lngLat.lat;
      nvMapboxGL.setCreateLineData(map, createPolyCoords);
    }

    function deleteMousemove(event) {
      if (!ctrl.isDeleting) {
        return;
      }

      const features = map.queryRenderedFeatures(
        nvMapboxGL.computeBoundingBox(event.point, 3),
        { layers: [nvMapboxGL.LAYERS.ACTIVE_POLY_VERT_STROKE] }
      );

      if (features.length) {
        map.dragPan.disable();
        cvs.style.cursor = 'pointer';
      } else {
        map.dragPan.enable();
        cvs.style.cursor = '';
      }
    }

    function chooseMousemove(event) {
      if (!ctrl.isChoosing) {
        return;
      }

      radiusLat = event.lngLat.lat;
      nvMapboxGL.setCircleData(map, [event.lngLat.lng, radiusLat]);
      nvMapboxGL.setChooseCircleRadius(map, ctrl.radius * 1000, radiusLat);
    }

    function adjustMousemove(event) {
      const features = map.queryRenderedFeatures(
        nvMapboxGL.computeBoundingBox(event.point, 3),
        { layers: [nvMapboxGL.LAYERS.OVRLAY_IMAGE_VERT_STROKE] }
      );

      cvs.style.cursor = '';

      if (features.length) {
        const layerId = features[0].layer.id;
        if (layerId === nvMapboxGL.LAYERS.OVRLAY_IMAGE_VERT_STROKE) {
          map.dragPan.disable();
          cvs.style.cursor = 'move';
        } else {
          map.dragPan.enable();
        }
      } else {
        map.dragPan.enable();
      }
    }

    function adjustMousemoveDragging(event) {
      const idx = adjustFeature.properties.id;
      resizeCoords[idx][0] = event.lngLat.lng;
      resizeCoords[idx][1] = event.lngLat.lat;

      switch (idx) {
        case 0:
          resizeCoords[1][1] = event.lngLat.lat;
          resizeCoords[3][0] = event.lngLat.lng;
          break;
        case 1:
          resizeCoords[0][1] = event.lngLat.lat;
          resizeCoords[2][0] = event.lngLat.lng;
          break;
        case 2:
          resizeCoords[1][0] = event.lngLat.lng;
          resizeCoords[3][1] = event.lngLat.lat;
          break;
        case 3:
          resizeCoords[2][1] = event.lngLat.lat;
          resizeCoords[0][0] = event.lngLat.lng;
          break;
        default:
          break;
      }

      nvMapboxGL.updateImageData(map, resizeCoords);
    }

    // //////////////////////////////////////////////////
    // MOUSEUP EVENTS
    // //////////////////////////////////////////////////

    function normalMouseup() {
      $log.debug('NORMAL mouseup');
      if (!ctrl.isSelectedPoly()) {
        return;
      }

      cvs.style.cursor = '';
      map.dragPan.enable();

      map.off('mousemove', normalMousemoveDragging);
      map.on('mousemove', normalMousemove);

      normalDragFeatures = [];
      nvMapboxGL.setActiveLineData(map, normalDragFeatures);

      const properties = ctrl.featureProperties;
      if (nvMapboxGL.existsPolygon(properties.id)) {
        nvMapboxGL.updateStablePolyData(map, properties.id, properties.name);
        nvMapboxGL.updateStableLineData(map, properties.id);
        nvMapboxGL.setActivePolygonData(map, properties.id);
      } else {
        $timeout(() => {
          ctrl.isDeleting = false;
          ctrl.featureProperties = null;
          ctrl.hasAnyPoly = nvMapboxGL.hasAnyPolygon();
        });
        nvMapboxGL.resetActivePolygonData(map);
      }
    }

    function adjustMouseup() {
      $log.debug('ADJUST mouseup');

      cvs.style.cursor = '';

      if (adjustFeature) {
        map.dragPan.enable();
        map.off('mousemove', adjustMousemoveDragging);
        map.on('mousemove', adjustMousemove);
      }

      adjustFeature = null;
    }

    // //////////////////////////////////////////////////
    // UI BUTTON EVENTS
    // //////////////////////////////////////////////////

    function onClickPricingZones() {
      ctrl.isChoosing = true;

      map.off('click', normalClick);
      map.off('mousedown', normalMousedown);
      map.off('mousemove', normalMousemove);
      map.off('mouseup', normalMouseup);

      map.once('click', chooseClick);
      map.on('mousemove', chooseMousemove);

      nvMapboxGL.initChooseCircleArea(map, { radius: ctrl.radius * 1000 });
    }

    function onClickDemo() {
      $http.get('resources/json/polygon-drawing-demo.json').then(success, nvRequestUtils.nvFailure);

      function success(response) {
        nvMapboxGL.addPolygons(map, nvMapboxGL.toMultiPolygonGeometries(response.data));
        $timeout(() => {
          ctrl.hasAnyPoly = nvMapboxGL.hasAnyPolygon();
        });
      }
    }

    function onClickDeletePolygon() {
      nvMapboxGL.removePolygon(map, +ctrl.featureProperties.id);
      ctrl.isDeleting = false;
      ctrl.featureProperties = null;
      ctrl.hasAnyPoly = nvMapboxGL.hasAnyPolygon();
      nvMapboxGL.resetActivePolygonData(map);
    }

    function onClickClearAllPolygons() {
      nvMapboxGL.removeAllPolygons(map);
      ctrl.isDeleting = false;
      ctrl.isCreating = false;
      ctrl.featureProperties = null;
      ctrl.hasAnyPoly = false;
      nvMapboxGL.resetActivePolygonData(map);
    }

    function onClickEditPolygon($event) {
      nvDialog.showSingle($event, {
        templateUrl: 'views/container/polygon-drawing/dialog/polygon-drawing-edit.dialog.html',
        cssClass: 'polygon-drawing-edit',
        scope: _.assign(nvEditModelDialog.scopeTemplate({ onSave: onSave }), {
          fields: {
            id: { displayName: 'ID', value: ctrl.featureProperties.id },
            name: { displayName: 'Name', value: ctrl.featureProperties.name },
          },
        }),
      });

      function onSave(data) {
        ctrl.featureProperties = data;
        nvMapboxGL.setPolygonProperty(data.id, 'id', data.name);
        nvMapboxGL.updateStablePolyData(map, data.id, data.name);
      }
    }

    function onClickCenterMap() {
      const geometries = nvMapboxGL.getAllPolyGeometries();
      if (geometries.length == 0) {
        return;
      }

      const coordinates = getAllCoordinates(geometries).flattenDepth(3).value();
      const xMin = _(coordinates).map(_.head).min();
      const xMax = _(coordinates).map(_.head).max();
      const yMin = _(coordinates).map(_.last).min();
      const yMax = _(coordinates).map(_.last).max();
      map.fitBounds([[xMin, yMin], [xMax, yMax]]);

      function getAllCoordinates(geometries) {
        const first = _.head(geometries);
        if (!!first.geojson) {
          return _(geometries).map('geojson.coordinates');
        } else if (!!first.location) {
          return _(geometries).map('location.coordinates');
        } else if (!!first.geometry) {
          return _(geometries).map('geometry.coordinates');
        } else {
          return _([]);
        }
      }
    }

    // //////////////////////////////////////////////////
    // UI SLIDER EVENTS
    // //////////////////////////////////////////////////

    function onChangeRadius(radius) {
      nvMapboxGL.setChooseCircleRadius(map, radius * 1000, radiusLat);
    }

    function onChangeOpacity(opacity) {
      nvMapboxGL.setImageOpacity(map, opacity);
    }

    // //////////////////////////////////////////////////
    // MISC HELPER METHODS
    // //////////////////////////////////////////////////

    function queryFeatures(event, isVertFn, isMockVertFn) {
      const features = map.queryRenderedFeatures(
        nvMapboxGL.computeBoundingBox(event.point, 3),
        { layers: [nvMapboxGL.LAYERS.ACTIVE_POLY_VERT_STROKE, nvMapboxGL.LAYERS.ACTIVE_MOCK_VERT] }
      );

      cvs.style.cursor = '';

      if (features.length) {
        const layerId = features[0].layer.id;
        if (layerId === nvMapboxGL.LAYERS.ACTIVE_POLY_VERT_STROKE) {
          map.dragPan.disable();
          isVertFn(features[0]);
        } else if (layerId === nvMapboxGL.LAYERS.ACTIVE_MOCK_VERT) {
          map.dragPan.disable();
          isMockVertFn(features[0]);
        } else {
          map.dragPan.enable();
        }
      } else {
        map.dragPan.enable();
      }
    }

    function isSelectedPoly() {
      return !_.isNull(ctrl.featureProperties);
    }
  }
}());
