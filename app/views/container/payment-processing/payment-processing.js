(function PaymentProcessingController() {
  angular.module('nvOperatorApp.controllers')
    .controller('PaymentProcessingController', controller);

  controller.$inject = ['$scope', 'nvTable', 'PaymentProcessing',
    'nvTranslate', 'nvDialog', 'nvBackendFilterUtils', 'nvDateTimeUtils', '$q',
    'nvCurrency', 'nvTimezone', 'Excel'];
  function controller($scope, nvTable, PaymentProcessing,
    nvTranslate, nvDialog, nvBackendFilterUtils, nvDateTimeUtils, $q,
    nvCurrency, nvTimezone, Excel) {
    const FILTER = 'filter';
    const RESULT = 'result';
    const PAGE_STATE_WAITING = 'waiting';
    const PAGE_STATE_IDLE = 'idle';
    const ctrl = this;
    ctrl.csvHeader = [
      nvTranslate.instant('container.payment-processing.request-date'),
      nvTranslate.instant('container.payment-processing.status'),
      nvTranslate.instant('commons.id'),
      nvTranslate.instant('container.payment-processing.withdrawal-ref'),
      nvTranslate.instant('container.payment-processing.phone-number'),
      nvTranslate.instant('container.payment-processing.nco-user-id'),
      nvTranslate.instant('container.payment-processing.acc-holder'),
      nvTranslate.instant('container.payment-processing.bank-account-no'),
      nvTranslate.instant('container.payment-processing.bank-name'),
      nvTranslate.instant('container.payment-processing.currency'),
      nvTranslate.instant('container.payment-processing.req-amount'),
      nvTranslate.instant('container.payment-processing.comments'),
    ];
    ctrl.stateTransitions = [
      {
        from: 'Pending',
        to: 'Cancelled',
        comment: 'Cancelled on ',
        isCommentMandatory: true,
      },
      {
        from: 'Pending',
        to: 'In Progress',
        comment: null,
        isCommentMandatory: false,
      },
      {
        from: 'In Progress',
        to: 'Cancelled',
        comment: 'Cancelld on ',
        isCommentMandatory: true,
      },
      {
        from: 'In Progress',
        to: 'Pending',
        comment: null,
        isCommentMandatory: false,
      },
      {
        from: 'In Progress',
        to: 'Completed',
        comment: 'Payment made on ',
        isCommentMandatory: true,
        fireNotif: true,
      },
      {
        from: 'In Progress',
        to: 'Failed',
        comment: 'Bank transfer failure on ',
        isCommentMandatory: true,
        fireNotif: true,
      },
    ];

    ctrl.csvField = [
      'created_at',
      'formattedStatus',
      'id',
      'ref',
      'contact_no',
      'seller_user_id',
      'account_holder_name',
      'bank_account_no',
      'bank_name',
      'currency',
      'amount',
      'comments',
    ];
    ctrl.showPage = FILTER;
    ctrl.paymentProcessings = [];
    ctrl.csvFileName = 'payment_request.xlsx';

    // ///////////////////////
    // FILTER
    // ///////////////////////

    ctrl.selectedFilters = [];
    ctrl.possibleFilters = [
      {
        type: nvBackendFilterUtils.FILTER_DATE,
        mainTitle: 'Request Date',
        fromModel: moment({ hour: 0 }).toDate(),
        toModel: moment({ hour: 23, minute: 59 }).toDate(),
        transformFn: nvDateTimeUtils.displayDate,
        key: { from: 'from', to: 'to' },
        presetKey: { from: 'from', to: 'to' },
      },
      {
        type: nvBackendFilterUtils.FILTER_OPTIONS,
        mainTitle: 'Status',
        possibleOptions: angular.copy(PaymentProcessing.statuses),
        selectedOptions: [],
        searchBy: 'displayName',
        searchText: {},
        presetKey: 'statuses',
        key: 'statuses',
        transformFn: getFilterValue,
      },
      {
        type: nvBackendFilterUtils.FILTER_OPTIONS,
        mainTitle: 'Currency',
        possibleOptions: angular.copy(nvCurrency.asOptions()),
        selectedOptions: [],
        searchBy: 'displayName',
        searchText: {},
        presetKey: 'currencies',
        key: 'currencies',
        transformFn: getFilterValue,
      },
    ];
    ctrl.filterPresets = [];
    ctrl.selectedPreset = null;
    ctrl.state = PAGE_STATE_IDLE;

    // ///////////////////
    // function definitions
    // //////////////////
    ctrl.onLoad = onLoad;
    ctrl.loadEntries = loadFirstPage;
    ctrl.openEditDialog = openEditDialog;
    ctrl.openFilterPage = openFilterPage;
    ctrl.isBulkEnable = isBulkEnable;
    ctrl.openBulkAction = openBulkAction;
    ctrl.loadMore = loadMore;
    ctrl.downloadXlsx = downloadXlsx;

    // //////////////////////
    // table
    // //////////////////////
    ctrl.tableParam = {};
    ctrl.page = 1;
    ctrl.itemsPerPage = nvTable.DEFAULT_BUFFER_SIZE;
    ctrl.totalItems = Number.MAX_VALUE;


    // //////////////////
    // function bodies
    // ////////////////

    function onLoad() {

    }

    function loadPaymentsPromise() {
      let param = nvBackendFilterUtils.constructParams(ctrl.selectedFilters);
      param = _.merge(param, {
        page: ctrl.page,
        page_size: ctrl.itemsPerPage,
      });
      return PaymentProcessing.search(param);
    }


    function loadPayments() {
      if (ctrl.tableParam.getUnfilteredLength() < ctrl.totalItems) {
        return loadPaymentsPromise().then(extend);
      }
      ctrl.state = PAGE_STATE_IDLE;
      return $q.reject();


      function extend(result) {
        ctrl.state = PAGE_STATE_IDLE;
        ctrl.page += 1;
        const payments = _.map(result.value, pay => (appendAdditionalField(pay)));
        return payments;
      }
    }

    function loadFirstPage() {
      ctrl.state = PAGE_STATE_WAITING;
      ctrl.page = 1;
      return loadPaymentsPromise()
        .then(success).finally(finallyFn);


      function success(result) {
        ctrl.tableParam = nvTable.createTable(PaymentProcessing.tableFields);
        const payments = _.map(result.value, pay => (appendAdditionalField(pay)));
        ctrl.tableParam.setData(payments);
        ctrl.totalItems = result.total_items;
        ctrl.showPage = RESULT;
      }

      function finallyFn() {
        ctrl.state = PAGE_STATE_IDLE;
      }
    }
    function loadMore() {
      if (ctrl.state !== PAGE_STATE_WAITING) {
        ctrl.state = PAGE_STATE_WAITING;
        return ctrl.tableParam.mergeInPromise(loadPayments(), 'id');
      }
      return null;
    }

    function appendAdditionalField(pay) {
      pay.formattedStatus = _.startCase(_.lowerCase(pay.status));
      pay.bank_account_no = _.replace(_.lowerCase(pay.bank_account_no),/\s/g,'');
      if (!pay.contact_no) {
        pay.contact_no = '-';
      }
      return pay;
    }

    function openBulkAction($event) {
      return nvDialog.showSingle($event, {
        templateUrl: 'views/container/payment-processing/dialog/bulk-edit-payment.dialog.html',
        locals: {
          payments: ctrl.tableParam.getSelection(),
          status: ctrl.tableParam.getSelection()[0].status,
        },
        theme: 'nvBlue',
        cssClass: 'nv-bulk-edit-payment-container',
        controllerAs: 'ctrl',
        controller: 'BulkPaymentProcessingDialogController',
      }).then(success);

      function success(result) {
        // merge back the resultappendAdditionalField(result.withdrawal)
        const updatedWithdrawal = _.map(result, w => (appendAdditionalField(w)));
        ctrl.tableParam.mergeIn(updatedWithdrawal, 'id');
      }
    }

    function isBulkEnable() {
      let status = '';
      let isEnabled = true;
      const selectedData = ctrl.tableParam.getSelection();
      if (selectedData && selectedData.length > 1) {
        status = selectedData[0].status;
      } else {
        isEnabled = false;
      }
      _.each(selectedData, (data) => {
        isEnabled = isEnabled && (status === data.status);
      });
      return isEnabled;
    }

    function openEditDialog($event, payment) {
      return nvDialog.showSingle($event, {
        templateUrl: 'views/container/payment-processing/dialog/edit-payment.dialog.html',
        locals: {
          payment: payment,
        },
        theme: 'nvBlue',
        cssClass: 'nv-edit-payment-container',
        controllerAs: 'ctrl',
        controller: 'PaymentProcessingDialogController',
      }).then(success);

      function success(result) {
        ctrl.tableParam.mergeIn(appendAdditionalField(result.withdrawal), 'id');
      }
    }


    function openFilterPage() {
      ctrl.showPage = FILTER;
    }

    function getFilterValue(filter) {
      return filter.value;
    }


    function downloadXlsx() {
      const data = [];
      data.push(ctrl.csvHeader);
      const loadedData = ctrl.tableParam.getTableData();
      _.each(loadedData, (d) => {
        const row = _.map(ctrl.csvField, (field) => {
          return _.get(d, field);
        });
        data.push(row);
      });
      return Excel.write(data, ctrl.csvFileName);
    }
  }
}());
