(function PaymentProcessingDialogController() {
  angular.module('nvOperatorApp.controllers')
        .controller('PaymentProcessingDialogController', controller);

  controller.$inject = ['$q', 'PaymentProcessing', '$mdDialog',
          'payment', 'nvDialog', '$scope', 'nvToast',
          'nvTimezone', 'nvDateTimeUtils', 'nvTranslate'];

  function controller($q, PaymentProcessing, $mdDialog,
        payment, nvDialog, $scope, nvToast,
        nvTimezone, nvDateTimeUtils, nvTranslate) {
    const ctrl = this;
    ctrl.payment = angular.copy(payment);
    ctrl.statuses = [];
    ctrl.oldStatus = angular.copy(payment.status);
    ctrl.state = 'idle';
    ctrl.isStatusDisabled = isStatusDisabled;
    ctrl.isStatusChanged = false;
        // ////////////////
        // Function definitions
        // ////////////////
    ctrl.saveChanges = saveChanges;
    ctrl.onCancel = onCancel;
    ctrl.stateTransition = null;
    ctrl.widthdrawalComments = null;

    init();

    function init() {
      ctrl.statuses = _.map(PaymentProcessing.getPossibleStatus(ctrl.payment.status), obj => (obj));
    }

    function isStatusDisabled() {
      return !ctrl.isStatusChanged && 
          (ctrl.payment.status === PaymentProcessing.CANCELLED
          || ctrl.payment.status === PaymentProcessing.COMPLETED
          || ctrl.payment.status === PaymentProcessing.REJECTED
          || ctrl.payment.status === PaymentProcessing.FAILED);
    }

    $scope.$watch('ctrl.payment.status', (newVal, oldVal)=>{
      if(newVal !== oldVal){
        if(ctrl.oldStatus === newVal){
          ctrl.isStatusChanged = false;
        }else{
          ctrl.isStatusChanged = true;
        }
      }
    });

    // ////////////////
    // Function bodies
    // ////////////////
    function saveChanges() {
      ctrl.state = 'waiting';
      const ref = ctrl.payment.ref;
      return onConfirm();

      function onConfirm() {
        const event = PaymentProcessing.statusToEvent(ctrl.payment.status);
        // pending have different event name
        const transactionPayload = {
          event: event,
          withdrawal_ref: ctrl.payment.ref,
          comments: ctrl.payment.comments,
        };
        return PaymentProcessing.doBulkUpdateTransaction(_.castArray(transactionPayload)).then(success);
      }

      function success(result) {
        // check successes length
        // if < 1, error happen
        if (result.successes.length > 0) {
          const withdrawal = result.successes[0];
          nvToast.success(nvTranslate.instant('container.payment-processing.success-update-withdrawal', { ref: ref }));
          $mdDialog.hide(withdrawal);
        } else {
          // find the reason
          const status = result.failures[0];
          if (status) {
            nvToast.error(nvTranslate.instant('container.payment-processing.failed-to-update', { ref: ref }));
            nvToast.error(status.reason);
          } else {
            nvToast.error(nvTranslate.instant('container.payment-processing.unknown-error'));
          }
        }
        ctrl.state = 'idle';
      }
    }

    function onCancel() {
      ctrl.state = 'idle';
      return $mdDialog.cancel();
    }
  }
}());
