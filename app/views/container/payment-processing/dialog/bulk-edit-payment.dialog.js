(function BulkPaymentProcessingDialogController() {
  angular.module('nvOperatorApp.controllers')
        .controller('BulkPaymentProcessingDialogController', controller);

  controller.$inject = ['PaymentProcessing', '$mdDialog', '$scope',
        'nvDialog', 'nvDateTimeUtils', 'nvTimezone', 'payments', 'status',
        'nvToast', 'nvTranslate'];

  function controller(PaymentProcessing, $mdDialog, $scope,
        nvDialog, nvDateTimeUtils, nvTimezone, payments, status,
        nvToast, nvTranslate) {
    const ctrl = this;
    ctrl.payments = _.cloneDeep(payments);
    ctrl.status = status;
    ctrl.state = 'idle';
    ctrl.oldStatus = angular.copy(status);
    ctrl.statuses = angular.copy(PaymentProcessing.statuses);
    ctrl.stateTransition = null;
    ctrl.isStatusChanged = false;
    ctrl.successUpdate = [];
    ctrl.successWithdrawals = [];

    // ////////////////
    // Function definitions
    // ////////////////
    ctrl.saveChanges = saveChanges;
    ctrl.onCancel = onCancel;
    ctrl.isStatusDisabled = isStatusDisabled;
    ctrl.isStatusChanged = false;

    // ////////////////
    // Function bodies
    // ////////////////
    function init() {
      ctrl.statuses = PaymentProcessing.getPossibleStatus(ctrl.status);
    }

    init();

    function isStatusDisabled() {
      return !ctrl.isStatusChanged && 
                (ctrl.status === PaymentProcessing.CANCELLED
                || ctrl.status === PaymentProcessing.COMPLETED
                || ctrl.status === PaymentProcessing.REJECTED
                || ctrl.status === PaymentProcessing.FAILED);
    }

     $scope.$watch('ctrl.status', (newVal, oldVal)=>{
      if(newVal !== oldVal){
        if(ctrl.oldStatus === newVal){
          ctrl.isStatusChanged = false;
        }else{
          ctrl.isStatusChanged = true;
        }
      }
    });

    function saveChanges() {
      ctrl.state = 'waiting';
      const event = PaymentProcessing.statusToEvent(ctrl.status);
      // pending have different event name
      const withdrawalUpdates = _.map(ctrl.payments, pay => ({
        event: event,
        withdrawal_ref: pay.ref,
        comments: ctrl.comments,
      }));
      return PaymentProcessing.doBulkUpdateTransaction(withdrawalUpdates)
                .then(success);

      function success(result) {
        ctrl.state = 'idle';
                // check successes length are same as the payload
        if (result.successes.length === withdrawalUpdates.length) {
                    // all transaction are success
          nvToast.success(nvTranslate.instant('container.payment-processing.bulk-update-success'));
                    // get all the transaction and return it to table
          ctrl.successWithdrawals = _.map(result.successes, w => (w.withdrawal));
          exportAsCsv(result.successes, result.failures);
          $mdDialog.hide(ctrl.successWithdrawals);
        } else if (result.failures.length > 0 && result.successes.length > 0) {
          nvToast.warning(nvTranslate.instant('container.payment-processing.bulk-update-partial-success'));
          exportAsCsv(result.successes, result.failures);
                    // take out the success withdrawals from ctrl.payments
          _.each(result.successes, (s) => {
            ctrl.successWithdrawals.push(s.withdrawal);
            _.remove(ctrl.payments, p => (p.ref === s.withdrawal.ref));
          });
        } else {
          nvToast.error(nvTranslate.instant('container.payment-processing.bulk-update-failed'));
        }
      }
    }

    function onCancel() {
      return $mdDialog.cancel();
    }

    function exportAsCsv(successes, fails) {
      const csvRows = [];
      csvRows.push('Withdrawal Reference,Status');
      _.each(successes, (success) => {
        csvRows.push(`${success.withdrawal.ref},Success`);
      });
      _.each(fails, (fail) => {
        csvRows.push(`${fail.ref},Failed`);
      });
      const report = document.createElement('a');
      angular.element(report)
                .attr('href', `data:application/csv;charset=utf-8,${encodeURIComponent(_.join(csvRows, '\n'))}`)
                .attr('download', 'bulkreport.csv');
      document.body.appendChild(report);
      report.click();
    }
  }
}());
