(function container() {
  angular
    .module('nvOperatorApp.controllers')
    .controller('ContainerController', ContainerController);

  ContainerController.$inject = [
    '$scope',
    '$state', 
    '$stateParams',
    '$mdMedia', 
    'appSidenav', 
    'nvDomain', 
    'nvTimezone',
  ];

  function ContainerController(
    $scope,
    $state, 
    $stateParams,
    $mdMedia, 
    appSidenav, 
    nvDomain, 
    nvTimezone
  ) {

    const ctrl = this;
    ctrl.title = null;
    ctrl.isFullWidth = isFullWidth;
    ctrl.isLockedOpen = isLockedOpen;
    ctrl.isOpenDriverMessaging = appSidenav.isOpen.driverMessaging;
    ctrl.toggleDriverMessaging = appSidenav.toggle.driverMessaging;
    // ////////////////////////////////////////////////

    nvDomain.consumeStateParam($stateParams);
    nvTimezone.setOperatorTimezoneByDomain(nvDomain.getDomain().current);

    $scope.$watch(
            () => $state.current.name,
            () => { ctrl.title = $state.current.nvHeader; }
        );

    // ////////////////////////////////////////////////

    function isLockedOpen() {
      return appSidenav.isLockedOpen.mainMenu();
    }

    function isFullWidth() {
      return $mdMedia('gt-sm');
    }
  }
}());
