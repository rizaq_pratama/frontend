(function controller() {
  angular
    .module('nvOperatorApp.controllers')
    .controller('OrderLevelTagManagementController', OrderLevelTagManagementController);

  OrderLevelTagManagementController.$inject = ['$q', 'nvBackendFilterUtils', 'Shippers', 'Order',
    'nvCoupledListUtils'];

  const FILTER_ID = {
    MASTER_SHIPPERS: 'master_shipper_ids',
    SHIPPERS: 'shipper_ids',
    ORDER_STATUS: 'statuses',
    ORDER_GRANULAR_STATUS: 'granular_statuses',
    CSV: 'use_csv',
  };

  function OrderLevelTagManagementController($q, nvBackendFilterUtils, Shippers, Order,
    nvCoupledListUtils) {
    const ctrl = this;

    // filter keys
    ctrl.FILTER_ID = FILTER_ID;

    // state params
    ctrl.initialized = false;
    ctrl.masterShippers = null; // shipper ids separated by comma
    ctrl.shippers = null; // string: shipper ids separated by comma
    ctrl.statuses = null;
    ctrl.granularStatuses = null;
    ctrl.useCsv = false; // boolean

    // shared params
    ctrl.selectedFilters = [];
    ctrl.possibleFilters = [];
    ctrl.csvOrders = [];

    ctrl.getData = getData;
    ctrl.readFromPath = readFromPath;
    ctrl.getFilterById = getFilterById;
    ctrl.generateStateParams = generateStateParams;
    ctrl.setCsvOrders = setCsvOrders;
    ctrl.getCsvOrders = getCsvOrders;

    // initializator
    function getData() {
      const masterShippersPromise = Shippers.readAll({ is_marketplace: true });
      return $q.all([
        masterShippersPromise,
      ]).then((responses) => {
        const masterShipperResponse = responses[0];

        const masterShipperOptions = _(masterShipperResponse)
          .map(shipper => _.defaults(shipper, { displayName: `${shipper.id}-${shipper.name}` }))
          .value();

        ctrl.selectedFilters = initFilter(masterShipperOptions);
      });
    }

    function initFilter(masterShipperOptions) {
      const generalFilters = [
        {
          id: FILTER_ID.SHIPPERS,
          type: nvBackendFilterUtils.FILTER_AUTOCOMPLETE,
          callback: getShippers,
          backendKey: 'legacy_ids',
          showOnInit: true,
          selectedOptions: [],
          mainTitle: 'Shipper',
          key: 'shipper_ids',
        },
        {
          id: FILTER_ID.MASTER_SHIPPERS,
          type: nvBackendFilterUtils.FILTER_OPTIONS,
          possibleOptions: masterShipperOptions,
          transformFn: transformMasterShippers,
          selectedOptions: [],
          mainTitle: 'Master Shipper',
          key: 'master_shipper_ids',
        },
        _.defaults({
          id: FILTER_ID.ORDER_STATUS,
          type: nvBackendFilterUtils.FILTER_OPTIONS,
          mainTitle: 'Status',
          key: 'statuses',
        }, getStatus()),
        _.defaults({
          id: FILTER_ID.ORDER_GRANULAR_STATUS,
          type: nvBackendFilterUtils.FILTER_OPTIONS,
          mainTitle: 'Granular Status',
          key: 'granular_statuses',
        }, getGranularStatus()),
      ];
      return generalFilters;

      function getShippers(str) {
        const shipperFilter = getFilterById(ctrl.selectedFilters, FILTER_ID.SHIPPERS);
        return Shippers.filterSearch(str, _.get(shipperFilter, 'selectedOptions'), 'id');
      }

      function transformMasterShippers(shipper) {
        return shipper.id;
      }

      function getStatus() {
        const statuses = _.map(Order.STATUS, value =>
          ({ id: value, displayName: value })
        );
  
        const selectedStatusIds = [Order.STATUS.PENDING]; // Pending
        const defaultStatuses = [];
  
        nvCoupledListUtils.transfer(statuses, defaultStatuses, status =>
        _.findIndex(selectedStatusIds, id => id === status.id) >= 0);
  
        return {
          selectedOptions: defaultStatuses,
          possibleOptions: statuses,
        };
      }

      function getGranularStatus() {
        const granularStatuses = _.map(Order.GRANULAR_STATUS, value =>
          ({ id: value, displayName: value })
        );
  
        const selectedGranularStatusIds = [Order.GRANULAR_STATUS.PENDING_PICKUP]; // Pending Pickup
        const defaultGranularStatus = [];
  
        nvCoupledListUtils.transfer(granularStatuses, defaultGranularStatus, status =>
        _.findIndex(selectedGranularStatusIds, id => id === status.id) >= 0);
  
        return {
          selectedOptions: defaultGranularStatus,
          possibleOptions: granularStatuses,
        };
      }
    }

    function getFilterById(filters, id) {
      return _.find(filters, ['id', id]);
    }

    function generateStateParams() {
      const params = _.mapValues(nvBackendFilterUtils.constructParams(ctrl.selectedFilters),
        val => (_.isArray(val) ? _.join(val, ',') : val)
      );
      return params;
    }

    function setCsvOrders(orders) {
      ctrl.csvOrders = orders;
    }

    function getCsvOrders() {
      return ctrl.csvOrders;
    }

    async function readFromPath($stateParams) {
      // initial conditions: filter has been initialized
      ctrl.useCsv = _.toLower($stateParams[FILTER_ID.CSV]) === 'true';
      ctrl.masterShippers = $stateParams[FILTER_ID.MASTER_SHIPPERS];
      ctrl.shippers = $stateParams[FILTER_ID.SHIPPERS];

      if (ctrl.masterShippers) {
        const masterShipperIds = _(ctrl.masterShippers).split(',').map(s => _.toNumber(s)).value();
        const filter = getFilterById(ctrl.selectedFilters, FILTER_ID.MASTER_SHIPPERS);
        // clean selections
        nvCoupledListUtils.transferAll(filter.selectedOptions, filter.possibleOptions);
        const toBeSelectedItems = _.remove(filter.possibleOptions, shipper =>
          _.find(masterShipperIds, id => id === shipper.id));
        filter.selectedOptions = toBeSelectedItems;
      }
      if (ctrl.shippers) {
        const shipperIds = _(ctrl.shippers).split(',').map(s => _.toNumber(s)).value();
        const filter = getFilterById(ctrl.selectedFilters, FILTER_ID.SHIPPERS);
        // clean selections
        const shippers = await Shippers.searchByIds(shipperIds);
        const toBeSelectedItems = _.map(shippers, shipper => _.defaults(shipper, { displayName: `${shipper.id}-${shipper.name}` }));
        filter.selectedOptions = toBeSelectedItems;
      }
      if (ctrl.statuses) {
        const statuses = _.split(ctrl.statuses, ',');
        const filter = getFilterById(ctrl.selectedFilters, FILTER_ID.ORDER_STATUS);
        // clean selections
        nvCoupledListUtils.transferAll(filter.selectedOptions, filter.possibleOptions);
        const toBeSelectedItems = _.remove(filter.possibleOptions, status =>
          _.find(statuses, s => s === status.value));
        filter.selectedOptions = toBeSelectedItems;
      }
      if (ctrl.granularStatuses) {
        const granularStatuses = _.split(ctrl.granularStatuses, ',');
        const filter = getFilterById(ctrl.selectedFilters, FILTER_ID.ORDER_GRANULAR_STATUS);
        // clean selections
        nvCoupledListUtils.transferAll(filter.selectedOptions, filter.possibleOptions);
        const toBeSelectedItems = _.remove(filter.possibleOptions, status =>
          _.find(granularStatuses, s => s === status.value));
        filter.selectedOptions = toBeSelectedItems;
      }


      setCsvOrders([]);
      ctrl.initialized = true;
    }
  }
}());
