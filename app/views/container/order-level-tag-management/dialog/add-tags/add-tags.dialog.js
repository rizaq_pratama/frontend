(function controller() {
  angular
    .module('nvOperatorApp.controllers')
    .controller('OrderLevelTagManagementAddTagsController', OrderLevelTagManagementAddTagsController);

  OrderLevelTagManagementAddTagsController.$inject = ['$mdDialog', 'parameter', 'Order', 'nvDialog'];

  function OrderLevelTagManagementAddTagsController($mdDialog, parameter, Order, nvDialog) {
    const ctrl = this;
    const orderIds = _.cloneDeep(parameter.orderIds);

    const MAX_TAGS = 4;
    let tagIdx = 0;

    ctrl.tags = [];
    ctrl.save = save;
    ctrl.cancel = cancel;
    ctrl.addTag = addTag;
    ctrl.removeTag = removeTag;
    ctrl.ableToAddMore = ableToAddMore;
    ctrl.isSaveDisabled = isSaveDisabled;

    init();

    function init() {
      addTag();
    }

    function save($event) {
      const requests = _.map(orderIds, constructRequest);
      const successIds = [];

      ctrl.bulkActionProgressPayload = {
        totalCount: _.size(requests),
        currentIndex: 0,
        errors: [],
      };

      nvDialog.showSingle($event, {
        templateUrl: 'lib/ninja-commons/services/dialogs/bulk-action-progress/bulk-action-progress.dialog.html',
        cssClass: 'bulk-action-progress',
        controller: 'BulkActionProgressDialogController',
        controllerAs: 'ctrl',
        skipHide: true,
        clickOutsideToClose: false,
        locals: {
          payload: ctrl.bulkActionProgressPayload,
        },
      }).then(progressDialogClosed);

      startOrdersTagging(requests);

      function constructRequest(orderId) {
        const validTags = _(ctrl.tags)
          .map(t => t.tag)
          .filter(t => _.trim(t) !== '')
          .compact()
          .uniq()
          .value();

        const request = {
          orderId: orderId,
          payload: {
            order_id: orderId,
            tags: validTags,
          },
        };
        return request;
      }

      function progressDialogClosed() {
        let returnCode = 0;
        if (_.size(ctrl.bulkActionProgressPayload.errors) > 0) {
          returnCode = -1;
        }
        $mdDialog.hide(returnCode);
      }

      function startOrdersTagging(theRequests) {
        let theRequest;

        ctrl.bulkActionProgressPayload.currentIndex += 1;
        if (_.size(theRequests) > 0) {
          theRequest = _.head(theRequests.splice(0, 1));
          Order.putTags(theRequest.orderId, theRequest.payload)
            .then(multiSelectSuccess, multiSelectError);
        }

        function multiSelectSuccess() {
          ctrl.bulkActionProgressPayload.successCount += 1;
          successIds.push(theRequest.orderId);
          if (_.size(theRequests) > 0) {
            startOrdersTagging(theRequests);
          }
        }
  
        function multiSelectError(error) {
          ctrl.bulkActionProgressPayload.errors.push(`Order ${theRequest.orderId}: ${_.get(error, 'message', 'Unknown Error')}`);
          if (_.size(theRequests) > 0) {
            startOrdersTagging(theRequests);
          }
        }
      }
    }

    function cancel() {
      $mdDialog.cancel();
    }

    function addTag() {
      const tag = {
        idx: `tag-${tagIdx}`,
        tag: '',
        isLast: true,
      };
      _.forEach(ctrl.tags, (t) => {
        t.isLast = false;
      });
      ctrl.tags.push(tag);
      tagIdx += 1;
    }

    function removeTag(tag) {
      _.remove(ctrl.tags, t => t.idx === tag.idx);
    }

    function ableToAddMore() {
      const nonEmptyTags = _.filter(ctrl.tags, t => _.trim(t.tag) !== '');
      return (_.size(nonEmptyTags) === _.size(ctrl.tags)) && _.size(ctrl.tags) < MAX_TAGS;
    }

    function isSaveDisabled() {
      const validTags = _(ctrl.tags)
        .map(t => t.tag)
        .compact()
        .uniq()
        .filter(t => _.trim(t) !== '')
        .filter(t => t.length <= 10)
        .value();
      return _.size(validTags) !== _.size(ctrl.tags);
    }
  }
}());
