(function controller() {
  angular
    .module('nvOperatorApp.controllers')
    .controller('OrderLevelTagManagementFilterController', HighValueItemFilterController);

  HighValueItemFilterController.$inject = ['$q', '$scope', '$state', '$stateParams', 'Order', 'nvDialog', 'nvToast',
  'nvTranslate'];

  function HighValueItemFilterController($q, $scope, $state, $stateParams, Order, nvDialog, nvToast,
    nvTranslate) {
    const ctrl = this;
    const parentCtrl = $scope.$parent.ctrl;

    ctrl.parentCtrl = parentCtrl;
    ctrl.loadResult = loadResult;
    ctrl.findOrdersWithCsv = findOrdersWithCsv;

    init();

    function init() {
      if (!parentCtrl.initialized) {
        parentCtrl.readFromPath($stateParams);
      }
    }

    function loadResult(useCsv = false) {
      parentCtrl.useCsv = useCsv;
      const params = parentCtrl.generateStateParams();
      params[parentCtrl.FILTER_ID.CSV] = useCsv;
      $state.go('container.order-level-tag-management.result', params);
    }

    function loadResultByCsv() {
      loadResult(true);
    }

    function findOrdersWithCsv($event) {
      nvDialog.showSingle($event, {
        templateUrl: 'views/container/order/dialog/csv-find-orders/csv-find-orders.dialog.html',
        theme: 'nvYellow',
        cssClass: 'order-find-by-csv',
        controller: 'OrderFindByCsvDialogController',
        controllerAs: 'ctrl',
        locals: {
          extras: null,
        },
      }).then(onSuccess);

      function onSuccess(response) {
        const csvOrders = response.validOrders;
        if (_.size(csvOrders) > 0) {
          parentCtrl.setCsvOrders(csvOrders);
          loadResultByCsv();
        } else {
          nvToast.warning(nvTranslate.instant('container.order-level-tag-management.csv-empty'));
        }
      }
    }
  }
}());
