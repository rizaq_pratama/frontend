(function controller() {
  angular
    .module('nvOperatorApp.controllers')
    .controller('OrderLevelTagManagementResultController', OrderLevelTagManagementResultController);

  OrderLevelTagManagementResultController.$inject = ['$q', '$scope', '$state', '$stateParams', 'Shippers',
    'nvBackendFilterUtils', 'Order', 'nvToast', 'nvTranslate', 'nvTable', 'nvDialog'];

  const TABLE_COLS = {
    orderId: { displayName: 'order id' },
    trackingId: { displayName: 'tracking id' },
    granularStatus: { displayName: 'granular status' },
    shipperId: { displayName: 'shipper id' },
    status: { displayName: 'status' },
  };

  function OrderLevelTagManagementResultController($q, $scope, $state, $stateParams, Shippers,
    nvBackendFilterUtils, Order, nvToast, nvTranslate, nvTable, nvDialog) {
    const ctrl = this;
    const parentCtrl = $scope.$parent.ctrl;
    ctrl.parentCtrl = parentCtrl;

    ctrl.function = {
      backToFilter: backToFilter,
      init: init,
      openOrderTagDialog: openOrderTagDialog,
      isBulkActionDisabled: isBulkActionDisabled,
      getSelected: getSelected,
      getSelectedNumber: getSelectedNumber,
    };

    ctrl.table = {
      lazyLoad: defaultLoader,
      distance: 0.2,
      param: nvTable.createTable(TABLE_COLS),
    };

    function init() {
      if (!parentCtrl.initialized) {
        parentCtrl.readFromPath($stateParams);
        // return if page is refreshed and it using CSV
        if (parentCtrl.useCsv) {
          backToFilter();
          return $q.resolve();
        }
      }

      // no lazy load if use CSV
      ctrl.page = 1;
      ctrl.pageSize = 1000;
      if (parentCtrl.useCsv && _.size(parentCtrl.getCsvOrders()) > 0) {
        // use csv, datas is here
        ctrl.table.param.setData(extendOrders(parentCtrl.getCsvOrders()));
        return $q.resolve();
      }
      // use filter, load datas and activate lazyload
      ctrl.table.lazyLoad = loadMore;
      const masterShipperFilter = parentCtrl.getFilterById(
        parentCtrl.selectedFilters, parentCtrl.FILTER_ID.MASTER_SHIPPERS
      );
      const masterShippers = masterShipperFilter ? masterShipperFilter.selectedOptions : [];
      const masterShipperIds = _(masterShippers).map('global_id').compact().value();

      const sellerPromise = _.size(masterShipperIds) === 0 ?
        $q.resolve([]) :
        Shippers.readSellersByIds(masterShipperIds);
      return sellerPromise.then(getOrders);

      function getOrders(sellers) {
        const dirtyParams = nvBackendFilterUtils.constructParams(parentCtrl.selectedFilters);
        _.assign(dirtyParams, {
          shipper_ids: _.concat(
            dirtyParams[parentCtrl.FILTER_ID.SHIPPERS],
            dirtyParams[parentCtrl.FILTER_ID.MASTER_SHIPPERS]
          ),
        });

        const params = _.omit(dirtyParams, ['undefined', parentCtrl.FILTER_ID.MASTER_SHIPPERS]);
        params.shipper_ids = params.shipper_ids && _(sellers)
            .map('legacy_id')
            .concat(params.shipper_ids)
            .compact()
            .uniq()
            .value();

        ctrl.copyFilters = _.cloneDeep(parentCtrl.selectedFilters);
        _.forEach(ctrl.copyFilters, (filter) => {
          filter.description = nvBackendFilterUtils.getDescription(filter);
        });

        ctrl.esParams = buildEsFilter(params);
        // search orders
        return fetchOrders().then((data) => {
          // setup table data
          ctrl.table.param.setData(extendOrders(data.orders));
          ctrl.table.param.totalItems = data.count;
        });
      }
    }

    function backToFilter() {
      const params = parentCtrl.generateStateParams();
      $state.go('container.order-level-tag-management.filter', params);
    }

    function fetchOrders() {
      const deferred = $q.defer();
      Order.elasticSearch(ctrl.esParams, getElasticSearchUrlParams())
        .then(elasticSearchSuccess, elasticSearchError);
      return deferred.promise;

      function elasticSearchSuccess(response) {
        const orders = _.map(response.search_data, 'order');
        deferred.resolve({
          count: response.total,
          orders: orders,
        });
      }

      function elasticSearchError() {
        deferred.reject();
      }
    }

    function loadMore() {
      return ctrl.table.param.mergeInPromise(loadMoreOrders(), 'id');

      function loadMoreOrders() {
        if (ctrl.table.param.getUnfilteredLength() < ctrl.table.param.totalItems) {
          ctrl.page += 1;
          return fetchOrders().then(ordersExtend);
        }
        return $q.reject();
      }

      function ordersExtend(response) {
        if (response && response.orders && response.orders.length > 0) {
          return extendOrders((response && response.orders) || []);
        }

        return $q.reject();
      }
    }

    function getElasticSearchUrlParams() {
      return {
        from: (ctrl.page - 1) * ctrl.pageSize,
        size: ctrl.pageSize,
      };
    }

    function buildEsFilter(params) {
      const searchFilters = [];
      if (params[parentCtrl.FILTER_ID.SHIPPERS]) {
        searchFilters.push({
          field: 'shipper_id',
          values: params[parentCtrl.FILTER_ID.SHIPPERS],
        });
      }
      if (params[parentCtrl.FILTER_ID.ORDER_STATUS]) {
        searchFilters.push({
          field: 'status',
          values: params[parentCtrl.FILTER_ID.ORDER_STATUS],
        });
      }
      if (params[parentCtrl.FILTER_ID.ORDER_GRANULAR_STATUS]) {
        searchFilters.push({
          field: 'granular_status',
          values: params[parentCtrl.FILTER_ID.ORDER_GRANULAR_STATUS],
        });
      }
      return {
        search_filters: searchFilters,
      };
    }

    function extendOrders(orders) {
      return _.map(orders, order => ({
        orderId: order.id,
        trackingId: order.tracking_id,
        granularStatus: order.granular_status,
        shipperId: order.shipper_id,
        status: order.status,
      }));
    }

    function defaultLoader() {
      return $q.reject();
    }

    function openOrderTagDialog($event) {
      const orderIds = _.map(getSelected(), order => order.orderId);
      nvDialog.showSingle($event, {
        templateUrl: 'views/container/order-level-tag-management/dialog/add-tags/add-tags.dialog.html',
        theme: 'nvBlue',
        cssClass: 'order-level-tag-management-dialog-add-tags',
        controller: 'OrderLevelTagManagementAddTagsController',
        controllerAs: 'ctrl',
        escapeToClose: false,
        skipHide: true,
        locals: {
          parameter: {
            orderIds: orderIds,
          },
        },
      }).then(onSuccess);

      function onSuccess(statusCode) {
        if (statusCode === 0) {
          backToFilter();
        }
      }
    }

    function isBulkActionDisabled() {
      return getSelectedNumber() === 0;
    }

    function getSelected() {
      if (ctrl.table.param) {
        return ctrl.table.param.getSelection();
      }
      return [];
    }

    function getSelectedNumber() {
      return _.size(getSelected());
    }
  }
}());
