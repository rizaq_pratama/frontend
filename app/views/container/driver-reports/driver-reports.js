(function controller() {
  angular
    .module('nvOperatorApp.controllers')
    .controller('DriverReportController', DriverReportController);

  DriverReportController.$inject = ['DriverReport', '$mdDialog', '$state', 'nvEnv'];
  function DriverReportController(DriverReport, $mdDialog, $state, nvEnv) {
    const ctrl = this;
    ctrl.generateExcelReport = generateExcelReport;
    ctrl.generateExcelReportState = 'idle';
    ctrl.fromDate = moment().startOf('month').toDate();
    ctrl.toDate = moment().endOf('month').toDate();

    // Deprecated - all except saas env
    if (!nvEnv.isSaas(nv.config.env)) {
      $state.go('container.reports'); // redirect to new page
    }

    function generateExcelReport(event, fromDate, toDate) {
      const fromDateMoment = moment(fromDate);
      const toDateMoment = moment(toDate);
      if (checkDate(event, fromDateMoment, toDateMoment)) {
        ctrl.generateExcelReportState = 'waiting';
        DriverReport.generateExcelReport(fromDateMoment, toDateMoment)
          .then(() => {
            ctrl.generateExcelReportState = 'idle';
          });
      }
    }

    function checkDate(event, fromDate, toDate) {
      if (toDate.isBefore(fromDate)) {
        const alert = $mdDialog.alert()
                        .title('Error')
                        .content('Please select a valid date range')
                        .targetEvent(event)
                        .ok('Ok');
        $mdDialog.show(alert);
        return false;
      }
      return true;
    }
  }
}());
