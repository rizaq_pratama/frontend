(function RecoveryTicketScanningController() {
  angular
        .module('nvOperatorApp.controllers')
        .controller('RecoveryTicketScanningController', controller);

  controller.$inject = ['$timeout', '$rootScope',
    'Ticketing', 'Hub', 'nvToast', '$q', '$mdDialog', 'nvTranslate',
    '$scope', 'nvNavGuard', 'nvDialog'];

  function controller($timeout, $rootScope, Ticketing, Hub, nvToast,
    $q, $mdDialog, nvTranslate, $scope, nvNavGuard, nvDialog) {
    const ctrl = this;
    // status for ticket creation
    const FAIL = 'Fail';
    const PAGE_STATE = {
      IDLE: 'idle',
      WAITING: 'waiting',
    };

    // ticket status
    const RESOLVED = 'RESOLVED';
    const CANCELLED = 'CANCELLED';
    const ORDER_ON_HOLD = 'On Hold';

    ctrl.state = PAGE_STATE.IDLE;
    ctrl.onLoad = onLoad;
    ctrl.loadTicketType = loadTicketType;
    ctrl.onChangeTicketType = onChangeTicketType;
    ctrl.onCheckAllChange = onCheckAllChange;
    ctrl.subTypeExist = false;
    ctrl.ticketTypes = [];
    ctrl.ticketSubTypes = [];
    ctrl.investigatingParties = [];
    ctrl.entrySources = [];
    ctrl.hubs = [];
    ctrl.ticket = Ticketing.getAddFieldScanning();
    ctrl.tickets = [];
    ctrl.onKeyPressed = onKeyPressed;
    ctrl.resetFieldValue = resetFieldValue;
    ctrl.scanState = ''; // SUCCESS if successfully add ticket
    ctrl.promptInvalid = true;
    ctrl.isCheckAll = true;
    ctrl.onSubmit = onSubmit;
    ctrl.isSubmitted = true;
    ctrl.deleteScanned = deleteScanned;
    ctrl.submittedTickets = [];


    $scope.$watch('ctrl.isSubmitted', (val) => {
      if (!val) {
        nvNavGuard.lock();
      } else {
        nvNavGuard.unlock();
      }
    });

    function onLoad() {
      return $q.all([
        Ticketing.getTicketTypes(),
        Ticketing.getGroup(),
        Ticketing.getEntrySource(),
        Hub.read(),
      ]).then(onFinishFetch);

      function onFinishFetch(results) {        
        ctrl.ticketTypes = _.map(results[0], o => ({
          value: o.id,
          displayName: o.name,
        }));
        ctrl.investigatingParties = _.map(results[1], o => ({
          value: o.id,
          displayName: o.name,
        }));
        ctrl.entrySources = _.map(results[2], o => ({
          value: o.id,
          displayName: o.entrySource,
        }));
        ctrl.hubs = _.map(results[3], o => ({
          displayName: o.name,
          value: o.id,
        }));
      }
    }


    function resetFieldValue() {
      _.forEach(ctrl.ticket, (o) => {
        delete o.value;
      });
    }


    function onSubmit() {
      ctrl.state = PAGE_STATE.WAITING;
      const payload = [];
      _.each(ctrl.tickets, (ticket) => {
        if (ticket.check) {
          ctrl.submittedTickets.push(ticket);
          payload.push(ticket.payload);
        }
      });
      return Ticketing.createBulk(payload)
        .then(success);

      function success(res) {
        const ticketCreationStatuses = res.tickets_status;
        const notSubmittedTicket = _.difference(ctrl.tickets, ctrl.submittedTickets);
        let failedCount = 0;

        _.each(ticketCreationStatuses, (status, trackingId) => {
          if (status.status === FAIL) {
            nvToast.error(`${status.message}. Tracking id : ${trackingId}`);
            failedCount += 1;
          } else {
            _.remove(ctrl.submittedTickets, ticket => (ticket.trackingId.value === trackingId));
          }
        });
        if (failedCount === 0) {
          nvToast.success(nvTranslate.instant('container.recovery-ticket-scanning.success-create-tickets'));
        } else {
          nvToast.error(nvTranslate.instant('container.recovery-ticket-scanning.some-ticket-are-failed-to-be-created'));
        }
        ctrl.failedTicket = _.compact(ctrl.submittedTickets);
        ctrl.tickets = _.concat(notSubmittedTicket, ctrl.failedTicket);
        ctrl.state = PAGE_STATE.IDLE;
        ctrl.isSubmitted = true;
      }
    }

    /**
     * Callback method when user type-in the tracking id
     * will check for tracking id validity
     * @param $event
       */
    function onKeyPressed($event) {
      // if ticket type
      // validate the field
      let payload = {};

      if (!ctrl.scanForm.$valid) {
        ctrl.scanForm.trackingId.$setTouched();
        ctrl.scanForm.ticketType.$setTouched();
        ctrl.scanForm.entrySource.$setTouched();
        ctrl.scanForm.investigatingGroup.$setTouched();
        ctrl.scanForm.investigatingHub.$setTouched();
        if (ctrl.scanForm.subTicketType) {
          ctrl.scanForm.subTicketType.$setTouched();
        }
        return $q.reject();
      }

      if (!ctrl.ticket.trackingId.value) {
        return $q.reject();
      }
      if (ctrl.ticket.trackingId.value === null || ctrl.ticket.trackingId === '') {
        return $q.reject();
      }
      if ($event.which === 13) {
        // check if the entered tracking id already in the table
        const alreadyScannedTicket =
          _.find(ctrl.tickets, { trackingId: ctrl.ticket.trackingId });
        if (alreadyScannedTicket) {
          nvToast.error(nvTranslate.instant('container.recovery-ticket-scanning.duplicate-tracking-id'));
          return $q.reject();
        }

        // post the ticket on this enter keypress
        // process the ticket object into array of field as api design doc
        payload = _.map(ctrl.ticket, o => ({
          fieldName: o.fieldName,
          fieldId: o.fieldId,
          fieldValue: o.value,
        }));

        // add additional info field
        // get user id and user name
        const userId = $rootScope.user.thirdPartyId;
        const userName = _.compact([$rootScope.user.firstName, $rootScope.user.lastName]).join(' ');
        const userEmail = $rootScope.user.email;
        payload.push({
          fieldName: 'creatorUserId',
          fieldValue: userId,
          fieldId: 30,
        }, {
          fieldName: 'creatorUserName',
          fieldValue: userName,
          fieldId: 39,
        }, {
          fieldName: 'creatorUserEmail',
          fieldValue: userEmail,
          fieldId: 66,
        }, {
          fieldName: 'TICKET_CREATION_SOURCE',
          fieldValue: 'SCANNING',
          fieldId: null,
        });
        // removing comments field if doesnt exist
        _.remove(payload, n => (n.fieldName === 'SCAN_COMMENTS' && !n.fieldValue
        ));

        return $q.all([
          Ticketing.getTicketByTrackingIdV2(_.castArray(ctrl.ticket.trackingId.value)),
          Ticketing.validateBeforeCreate(ctrl.ticket.trackingId.value),
        ]).then(onSuccessValidate, onInvalidTrackingId);
      }
      return $q.reject();

      function onSuccessValidate(res) {
        const order = res[1];
        // check for on hold status
        if (order && order.status === ORDER_ON_HOLD) {
          return nvToast.error(
            nvTranslate.instant('container.recovery-ticket-scanning.this-order-already-has-a-pending-pets-ticket')
          );
        }

        // check for non resolved and cancelled ticket
        if (Ticketing.hasActiveTickets(res[0])) {
          return nvToast.error(
            nvTranslate.instant('container.recovery-ticket-scanning.tracking-id-x-already-have-a-ticket',
            { x: ctrl.ticket.trackingId.value }
          ));
        }


        return saveToTable();
      }

      function onInvalidTrackingId() {
        if (!ctrl.promptInvalid) {
          return $q.all([
            Ticketing.getTicketByTrackingIdV2(_.castArray(ctrl.ticket.trackingId.value)),
          ]).then(onSuccessValidate);
        }

        return nvDialog.confirmDelete(null, {
          title: nvTranslate.instant('container.recovery-tickets.invalid-tracking-id-title'),
          content: nvTranslate.instant('container.recovery-tickets.invalid-tracking-id-message'),
          ok: nvTranslate.instant('commons.save'),
          cancel: nvTranslate.instant('commons.cancel'),
        }).then(() => ($q.all([
          Ticketing.getTicketByTrackingIdV2(_.castArray(ctrl.ticket.trackingId.value)),
        ]).then(onSuccessValidate)));
      }

      function saveToTable() {
        // put the saved ticket on the table
        const savedTicket = _.cloneDeep(ctrl.ticket);
        const investigatingGroupObj = _.find(ctrl.investigatingParties,
            { value: ctrl.ticket.investigatingParty.value });
        const ticketTypeObj = _.find(ctrl.ticketTypes,
            { value: ctrl.ticket.ticketType.value });
        const entrySourceObj = _.find(ctrl.entrySources,
            { value: ctrl.ticket.entrySource.value });
        let ticketSubTypeObj;
        if (ctrl.ticket.subTicketType.value) {
          ticketSubTypeObj = _.find(ctrl.ticketSubTypes,
              { value: ctrl.ticket.subTicketType.value });
        }
        // set to object
        savedTicket.investigatingGroupName = investigatingGroupObj ? investigatingGroupObj.displayName : '-';
        savedTicket.ticketTypeName = ticketTypeObj ? ticketTypeObj.displayName : '-';
        savedTicket.entrySourceName = entrySourceObj ? entrySourceObj.displayName : '-';
        savedTicket.ticketSubTypeName = ticketSubTypeObj ? ticketSubTypeObj.displayName : '-';
        savedTicket.check = true;
        savedTicket.payload = payload;
        ctrl.tickets.push(savedTicket);
        ctrl.isSubmitted = false;
        // change class
        ctrl.scanState = 'SUCCESS';
        return $timeout(() => {
          ctrl.scanState = '';
          ctrl.ticket.trackingId.value = '';
          // set focus to tracking id
          setFocus();
        }, 200);
      }
    }

    function setFocus() {
      angular.forEach(document.querySelectorAll('#trackingId-input'), (e) => {
        e.focus();
      });
    }

    function onCheckAllChange() {
      // the value changed after click event happen
      // so need to inverse the value
      _.forEach(ctrl.tickets, (o) => {
        o.check = !ctrl.isCheckAll;
      });
    }

    function onChangeTicketType(id) {
      // reset the subTicketType
      ctrl.ticket.subTicketType.value = null;
      if (id === undefined || id === null) {
        return;
      }
      Ticketing.getCustomField(id).then((result) => {
        if (result !== undefined || result !== null) {
          if (!result.sub_types) {
            ctrl.subTypeExist = false;
          } else {
            ctrl.subTypeExist = true;
            // render sub type array
            ctrl.ticketSubTypes = (result.sub_types).filter(o => (!o.archived)).map(o => ({
              value: o.id,
              displayName: o.name,
            }));
          }
        }
      });
    }

    function loadTicketType() {
      Ticketing.getTicketTypes().then((result) => {
        if (result !== undefined || result !== null) {
          ctrl.ticketTypes = _.map(result, o => ({
            value: o.id,
            displayName: o.name,
          }));
        }
      });
    }

    function deleteScanned(ticket) {
      return _.pullAllBy(ctrl.tickets, _.castArray(ticket), 'trackingId');
    }
  }
}());
