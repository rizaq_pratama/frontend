(function controller() {
  angular
    .module('nvOperatorApp.controllers')
    .controller('SettingsController', SettingsController);

  SettingsController.$inject = ['$scope', 'nvTranslate'];

  function SettingsController($scope, nvTranslate) {
    const ctrl = this;
    ctrl.language = nvTranslate.getLanguageCode();
    ctrl.languages = nvTranslate.getAvailableLanguages();

    $scope.$watch('ctrl.language', nvTranslate.setLanguageCode);
  }
}());
