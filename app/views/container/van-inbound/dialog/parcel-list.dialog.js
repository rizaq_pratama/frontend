(function controller() {
  angular.module('nvOperatorApp.controllers')
    .controller('VanInboundParcelListDialogController', VanInboundParcelListDialogController);

  VanInboundParcelListDialogController.$inject = ['nvTranslate', '$mdDialog', 'title', 'data', 'Waypoint',
    '$window', '$state'];

  function VanInboundParcelListDialogController(nvTranslate, $mdDialog, title, data, Waypoint,
    $window, $state) {
    const ctrl = this;
    ctrl.title = _.cloneDeep(title);
    ctrl.data = _.cloneDeep(data);

    ctrl.onClose = onClose;
    ctrl.onViewOrder = onViewOrder;
    init();

    function init() {
      const ids = _(ctrl.data)
        .map('waypointId')
        .compact()
        .uniq()
        .value();
      Waypoint.searchByIds(ids)
        .then(processWaypoint)
        .finally(() => {
        });

      function processWaypoint(wps) {
        _.each(ctrl.data, (order) => {
          const waypoint = _.find(wps, wp => wp.id === order.waypointId);
          let address = ['NOT AVAILABLE'];
          if (waypoint) {
            address = [
              _.get(waypoint, 'addr_line_1'),
              _.get(waypoint, 'addr_line_2'),
              _.get(waypoint, 'addr_city'),
              _.get(waypoint, 'addr_postcode'),
              _.get(waypoint, 'addr_country'),
            ];
          }
          _.assign(order, { address: _.compact(address).join(' ') });
        });
      }
    }

    function onClose() {
      $mdDialog.cancel();
    }

    function onViewOrder(order) {
      const orderId = _.get(order, 'order_id');
      $window.open(
        $state.href('container.order.edit', { orderId: orderId })
      );
    }
  }
}());
