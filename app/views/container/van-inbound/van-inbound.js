(function controller() {
  angular.module('nvOperatorApp.controllers')
    .controller('VanInboundController', VanInboundController);

  VanInboundController.$inject = ['Driver', 'Route', 'Inbound', 'Order', 'nvTranslate', 'nvToast',
    'nvSoundService', 'nvDialog', 'nvNavGuard', '$timeout', '$interval', '$scope', '$q'];
  function VanInboundController(Driver, Route, Inbound, Order, nvTranslate, nvToast,
    nvSoundService, nvDialog, nvNavGuard, $timeout, $interval, $scope, $q) {
    const ctrl = this;
    const VAN_FROM_NINJAVAN = 'VAN_FROM_NINJAVAN';
    const PENDING = 'PENDING';
    const PAGE_STATE = {
      SCAN: 'scan',
      FETCH: 'fetch',
    };

    const API_STATE = {
      IDLE: 'idle',
      WAITING: 'waiting',
    };

    const SCAN_STATE = {
      READY: 'READY',
      EMPTY: 'EMPTY',
      SUCCESS: 'SUCCESS',
    };

    ctrl.fetchRoute = fetchRoute;
    ctrl.openParcelListDialog = openParcelListDialog;
    ctrl.onKeypressRouteId = onKeypressRouteId;
    ctrl.onKeyPressScanTrackingId = onKeyPressScanTrackingId;
    ctrl.startRoute = startRoute;
    ctrl.driverIdName = '';

    ctrl.fetchState = API_STATE.IDLE;
    ctrl.pageState = PAGE_STATE.FETCH;
    ctrl.routeStartState = API_STATE.IDLE;
    ctrl.scanStatus = SCAN_STATE.READY;
    ctrl.countUnscannedOrder = 0;
    ctrl.countScannedOrder = 0;
    ctrl.countOrder = 0;
    ctrl.scannedOrder = [];
    ctrl.unscannedOrder = [];
    ctrl.waypointIds = {};
    ctrl.orderWaypointIds = {};
    ctrl.inboundScanMap = {};
    ctrl.queue = [];
    ctrl.queueOnProgressIdx = [];
    ctrl.wait = false;
    ctrl.lastScannedTrackingId = '';
    ctrl.sound = {
      success: [{
        src: nvSoundService.getSuccessSoundFile(),
        type: 'audio/wav',
      }],
      error: [{
        src: nvSoundService.getErrorSoundFile(),
        type: 'audio/wav',
      }],
    };
    ctrl.invalidTrackingIds = [];

    // scope variables
    $scope.success = ctrl.sound.success;
    $scope.error = ctrl.sound.error;

    const intervalPromise = $interval(() => {
      pullAndPostFromQueue();
      focusToTrackingId();
    }, 500);


    function onKeypressRouteId(event) {
      if (event.which === 13) {
        fetchRoute();
      }
      return angular.noop();
    }

    function onKeyPressScanTrackingId(event) {
      if (event.which === 13) {
        scanTrackingId();
      }
      return angular.noop();
    }

    function openParcelListDialog(scanned, event) {
      let title = '';
      let data = [];
      if (scanned) {
        title = nvTranslate.instant('container.van-inbound.scanned-parcels');
        data = ctrl.scannedOrder;
      } else {
        title = nvTranslate.instant('container.van-inbound.unscanned-parcels');
        data = ctrl.unscannedOrder;
      }
      return nvDialog.showSingle(event, {
        templateUrl: 'views/container/van-inbound/dialog/parcel-list.dialog.html',
        locals: {
          title: title,
          data: data,
        },
        theme: 'nvBlue',
        cssClass: 'nv-van-inbound-view-parcels',
        controllerAs: 'ctrl',
        controller: 'VanInboundParcelListDialogController',
      });
    }

    function fetchRoute() {
      ctrl.fetchState = API_STATE.WAITING;
      ctrl.invalidTrackingIds = [];
      if (ctrl.routeId && ctrl.routeId !== '') {
        findRouteId(ctrl.routeId)
          .then(processRoute)
          .then(readOrdersAndDrivers)
          .finally(() => (ctrl.fetchState = API_STATE.IDLE));
      } else if (ctrl.trackingId) {
        // check the route id for this tracking id
        findRouteByScanTrackingId(ctrl.trackingId)
          .then(readScannedOrder)
          .then(processRoute)
          .then(readOrdersAndDrivers)
          .finally(() => (ctrl.fetchState = API_STATE.IDLE));
      } else {
        ctrl.fetchState = API_STATE.IDLE;
      }
    }

    function findRouteByScanTrackingId(trackingId) {
      return $q.all([
        Route.getRouteByScan(trackingId),
        Order.getTrackingIdV2(trackingId),
      ]);
    }

    function readScannedOrder(results) {
      const routes = results[0];
      const orderDetail = results[1];

      let routeId = null;
      let type = null;
      if (_.get(orderDetail, 'pickup.status') === PENDING) {
        routeId = routes.pickup_route_id;
        type = 'pickup';
      } else if (_.get(orderDetail, 'delivery.status') === PENDING) {
        type = 'delivery';
        routeId = routes.delivery_route_id;
      }

      if (!routeId) {
        nvToast.warning(nvTranslate.instant('container.van-inbound.unable-to-van-inbound-the-parcel', { routeType: type, routeId: routeId }));
        return $q.reject();
      }
      return findRouteId(routeId);
    }

    function findRouteId(routeId) {
      const ROUTE_ID = _.toNumber(_.trim(routeId));
      if (!ROUTE_ID) {
        return $q.reject('no route id');
      }

      const PARAM = {
        id: ROUTE_ID,
        include_waypoints: true,
      };
      ctrl.routeId = ROUTE_ID;

      return $q.all([
        Route.search(PARAM),
        Route.getVanInbounded(ROUTE_ID),
      ]);
    }

    function processRoute(results) {
      const routeResult = _.head(results[0]);
      const vanInboundResult = results[1];

      if (!routeResult) {
        nvToast.error(nvTranslate.instant('container.van-inbound.no-route-found', { routeId: ctrl.routeId }));
        return $q.reject(-1);
      }
      ctrl.countUnscannedOrder = 0;
      ctrl.countScannedOrder = 0;
      ctrl.countOrder = 0;
      ctrl.scannedOrder = [];
      ctrl.unscannedOrder = [];
      ctrl.inboundScanMap = _.keyBy(vanInboundResult, 'scan');

      const wpIds = _.get(routeResult, 'waypoint_ids', []);
      const driverId = _.get(routeResult, 'driver_id');

      if (!driverId) {
        nvToast.error(nvTranslate.instant('container.van-inbound.no-driver-found', { routeId: routeResult.id }));
        return $q.reject(-1);
      }

      const orderSearchPromise = _.size(wpIds) === 0 ?
        $q.when([]) : // endpoint doesnt allow empty waypoints argument
        Order.searchByWaypointIds(wpIds);
      return $q.all([
        Driver.getDriveryById(driverId),
        orderSearchPromise,
      ]);
    }

    function readOrdersAndDrivers(results) {
      const driverResult = results[0];
      const orderResults = results[1];

      if (driverResult.driver) {
        ctrl.driverIdName = `${driverResult.driver.id} - ${driverResult.driver.firstName} ${driverResult.driver.lastName}`;
      }

      _.each(orderResults, parseWaypointOrders);
      countOrders();
      focusToTrackingId(1000);

      ctrl.pageState = PAGE_STATE.SCAN;
      ctrl.fetchState = API_STATE.IDLE;

      function parseWaypointOrders(obj) {
        const wpId = obj.waypoint_id;
        const orders = obj.orders;

        _.each(orders, (order) => {
          ctrl.orderWaypointIds[order.tracking_id] = {
            orderId: order.order_id,
            waypointId: wpId,
          };
          if (order.stamp_id) {
            ctrl.orderWaypointIds[order.stamp_id] = {
              orderId: order.order_id,
              waypointId: wpId,
            };
          }

          // put waypoint id into order object
          order.waypointId = wpId;

          // put into scanned / unscanned holder
          const inboundScan = ctrl.inboundScanMap[order.tracking_id];
          if (inboundScan) {
            ctrl.scannedOrder.push(order);
          } else {
            ctrl.unscannedOrder.push(order);
          }
        });
      }
    }

    function scanTrackingId() {
      const trackingId = _.clone(ctrl.scannedTrackingId);
      ctrl.lastScannedTrackingId = trackingId;
      ctrl.scannedTrackingId = null;
      ctrl.wait = true;
      if (!trackingId || trackingId === '') {
        showEmpty();
        ctrl.wait = false;
        return;
      }

      if (!ctrl.orderWaypointIds[trackingId]) {
        ctrl.invalidTrackingIds.unshift(trackingId);
        showError();
        ctrl.wait = false;
        return;
      }

      const orderWaypoint = ctrl.orderWaypointIds[trackingId];
      const payload = {
        trackingId: trackingId,
        waypointId: orderWaypoint.waypointId,
        type: VAN_FROM_NINJAVAN,
      };
      ctrl.queue.unshift(payload);
      ctrl.wait = false;
      showSuccess();
      if (!nvNavGuard.isLock()) {
        nvNavGuard.lock(nvTranslate.instant('container.van-inbound.van-inbound-page-is-still-submitting-the-scans-data'));
      }
      focusToTrackingId();
    }

    function countOrders() {
      ctrl.countScannedOrder = ctrl.scannedOrder.length;
      ctrl.countUnscannedOrder = ctrl.unscannedOrder.length;
      ctrl.countOrder = ctrl.countUnscannedOrder + ctrl.countScannedOrder;
    }


    function pullAndPostFromQueue() {
      let trackingId;
      if (ctrl.queue && ctrl.queue.length > 0) {
        const payload = ctrl.queue[0];
        trackingId = payload.trackingId;
        ctrl.queueOnProgressIdx.push(trackingId);
        ctrl.queue.shift();
        return Inbound.vanInbound(_.trim(ctrl.routeId), _.castArray(payload))
          .then(success, error);
      }
      tryUnlock();
      return null;

      function tryUnlock() {
        if (ctrl.queueOnProgressIdx.length === 0 && ctrl.queue.length === 0) {
          nvNavGuard.unlock();
        }
      }

      function success(results) {
        _.pull(ctrl.queueOnProgressIdx, trackingId);
        tryUnlock();

        _.each(results, (scanResponse) => {
          const orderWaypoint = ctrl.orderWaypointIds[scanResponse.scan];
          // remove the wp from unscanned list and move it to scanned list
          const order = _.remove(ctrl.unscannedOrder, o => (o.order_id === orderWaypoint.orderId));
          ctrl.scannedOrder = _.concat(ctrl.scannedOrder, order);
          ctrl.scannedTrackingId = '';
          countOrders();
        });
      }
      function error() {
        _.pull(ctrl.queueOnProgressIdx, trackingId);
        tryUnlock();
        nvToast.error(nvTranslate.instant('container.van-inbound.unable-to-process-inbound'));
      }
    }

    function focusToTrackingId(timeout = 0) {
      $timeout(() => {
        angular.forEach(document.querySelectorAll('#tracking-id-scan'), (e) => {
          e.focus();
        });
      }, timeout);
    }

    function startRoute() {
      ctrl.routeStartState = API_STATE.WAITING;
      return Route.startRoute(_.trim(ctrl.routeId))
        .then(success, failure);

      function success() {
        ctrl.routeStartState = API_STATE.IDLE;
        return nvDialog.confirmSave(null, {
          title: nvTranslate.instant('container.van-inbound.route-started-title'),
          content: nvTranslate.instant('container.van-inbound.route-started-content'),
          ok: nvTranslate.instant('container.van-inbound.back-to-route-input-screen'),
          cancel: nvTranslate.instant('commons.close'),
        }).then(clickOk);
        function clickOk() {
          backToRouteInputScreen();
        }
      }

      function failure() {
        ctrl.routeStartState = API_STATE.IDLE;
        nvToast.success(nvTranslate.instant('container.van-inbound.failed-to-start-route-x', { x: ctrl.routeId }));
      }
    }

    function backToRouteInputScreen() {
      ctrl.routeId = null;
      ctrl.pageState = PAGE_STATE.FETCH;
    }

    function showEmpty() {
      ctrl.scanStatus = SCAN_STATE.EMPTY;
      return $timeout(() => {
        showReady();
      }, 1);
    }

    function showError() {
      return $timeout(() => {
        $scope.errorSound.play(0);
        showReady();
      }, 1);
    }

    function showSuccess() {
      ctrl.scanStatus = SCAN_STATE.SUCCESS;
      return $timeout(() => {
        $scope.successSound.play(0);
        showReady();
      }, 1);
    }

    function showReady() {
      return $timeout(() => {
        ctrl.scanStatus = SCAN_STATE.READY;
      }, 2000);
    }

    $scope.$on('$destroy', () => {
      if (intervalPromise) {
        $interval.cancel(intervalPromise);
      }
    });
  }
}());
