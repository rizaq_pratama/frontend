;(function() {
  'use strict';

  angular
    .module('nvOperatorApp.controllers')
    .controller('ReportsController', ReportsController);

  ReportsController.$inject = [
    'nvReports',
    'nvToast',
    '$rootScope',
    'nvTranslate',
  ];

  function ReportsController(nvReports, nvToast, $rootScope, nvTranslate) {
    const { email } = $rootScope.user;
    const ctrl = {}

    ctrl.today = new Date();

    ctrl.codReport = {
      mode: '10',
      date: new Date,
      state: 'idle',
      generate() {
        if (ctrl.driverReport.state === 'waiting') 
          return;

        ctrl.codReport.state = 'waiting';

        const { mode, date } = ctrl.codReport;
        let promise, reportName;

        if (mode === '10') {
          promise = nvReports.subscribeCODReport([email], date);
          reportName = 'COD report';
        } else {
          promise = nvReports.subscribeCODDriverReport([email], date);
          reportName = 'Driver COD report';
        }

        promise
          .then(showToastForReport(reportName))
          .then(() => ctrl.codReport.state = 'idle');
      }
    }

    ctrl.driverReport = {
      fromDate: new Date,
      toDate: new Date,
      state: 'idle',
      checkDateOrder() {
        const { fromDate, toDate } = ctrl.driverReport;
        if (fromDate > toDate) {
          ctrl.driverReport.fromDate = new Date(toDate.valueOf());
        }
      },
      generate() {
        if (ctrl.driverReport.state === 'waiting') 
          return;

        ctrl.driverReport.state = 'waiting';

        const { fromDate, toDate } = ctrl.driverReport;
        nvReports.subscribeDriverReport([email], fromDate, toDate)
          .then(showToastForReport('Driver report'))
          .then(() => ctrl.driverReport.state = 'idle');
      }
    }

    ctrl.routeCleanningReport = {
      mode: '100',
      date: new Date,
      state: 'idle',
      generate() {
        if (ctrl.routeCleanningReport.state === 'waiting') 
          return;

        ctrl.routeCleanningReport.state = 'waiting';

        const { mode, date } = ctrl.routeCleanningReport

        let promise, reportName;
        switch (mode) {
          case '100':
            promise = nvReports.subscribeRouteCleanCOD([email], date);
            reportName = 'Route Cleanning COD report';
            break;
          case '010':
            promise = nvReports.subscribeRouteCleanParcel([email], date);
            reportName = 'Route Cleanning Parcel report';
            break;
          case '001':
            promise = nvReports.subscribeRouteCleanReservation([email], date);
            reportName = 'Route Cleanning Reservation report';
            break;
        }

        if (promise) {
          promise
            .then(showToastForReport(reportName))
            .then(() => ctrl.routeCleanningReport.state = 'idle');
        } 
      }
    }

    function showToastForReport(reportName) {
      return function (data) {
        if (data.status === 200) {
          nvToast.success(
            nvTranslate.instant(
              'container.reports.notifications.schedule-success', 
              { reportName }
            ),
            {
              tapToDismiss: false,
              timeOut: 0,
              extendedTimeOut: 0,
              buttonCallback: () => {},
              icon: 'info',
            }
          )
        }
      }
    }

    return ctrl
  }

})();
