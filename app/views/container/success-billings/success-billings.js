(function controller() {
  angular
    .module('nvOperatorApp.controllers')
    .controller('SuccessBillingsController', SuccessBillingsController);

  SuccessBillingsController.$inject = ['Billings', 'DWH', 'nvTranslate',
    'nvDateTimeUtils', 'nvToast', '$timeout', 'Shippers', 'nvTimezone', 'nvDomain'];

  function SuccessBillingsController(Billings, DWH, nvTranslate,
    nvDateTimeUtils, nvToast, $timeout, Shippers, nvTimezone, nvDomain) {
    const ctrl = this;

    ctrl.view = {};
    ctrl.state = {};
    ctrl.data = {};
    ctrl.function = {};
    ctrl.test = null;
    ctrl.view.emailSearch = '';
    ctrl.view.generated = [false, false, false, false];
    ctrl.view.state = 'idle';

    ctrl.state.emailSearch = {};
    ctrl.state.emailSearch.showAll = true;
    ctrl.state.emailSearch.counter = 0;

    ctrl.data.completed = '10';
    ctrl.data.startDate = moment().startOf('month').toDate();
    ctrl.data.endDate = moment().endOf('month').toDate();
    ctrl.data.allShippers = '10';
    ctrl.data.selectedEmails = [];
    ctrl.data.isOutOfRange = false;
    ctrl.data.consolidateOptions = ['SHIPPER', 'PARENT', 'ALL', 'AGGREGATED'];

    ctrl.function.emailSearch = {};
    ctrl.function.generateSuccessBillings = generateSuccessBillings;
    ctrl.function.isGenerateBillingDisabled = isGenerateBillingDisabled;
    ctrl.function.emailSearch.onClearAll = onClearAll;
    ctrl.function.emailSearch.onKeypress = onKeypress;
    ctrl.function.emailSearch.onRemove = onRemove;
    ctrl.function.emailSearch.onEnterButton = onEnterButton;
    ctrl.function.changeGenerateFileOption = changeGenerateFileOption;
    ctrl.function.calculateDateRange = calculateDateRange;

    init();

    function init() {
      const shipperFilter = {
        callback: getShippers,
        selectedOptions: [],
        mainTitle: 'Shipper',
        key: 'shipperIds',
        presetKey: 'shipperIds',
      };

      ctrl.data.fiters = {};
      ctrl.data.fiters.shipperFilter = shipperFilter;

      function getShippers(text) {
        return Shippers.filterSearch(text, this.getSelectedOptions());
      }
    }

    function changeGenerateFileOption(index) {
      if (index === 0 && !ctrl.view.generated[0]) {
        $timeout(() => {
          ctrl.view.generated = _.map(ctrl.view.generated, (v, k) => {
            let value = v;
            if (k !== 0) {
              value = false;
            }
            return value;
          });
        }, 0);
      }
    }

    function onClearAll() {
      ctrl.data.selectedEmails.length = 0;
    }

    function onKeypress($event) {
      if ($event.which === 13) {
        if (ctrl.view.emailSearch && (ctrl.view.emailSearch.length > 0)) {
          addEmail();
        }
      }
    }

    function onEnterButton() {
      addEmail();
    }

    function addEmail() {
      ctrl.data.selectedEmails.push({
        id: ctrl.state.emailSearch.counter,
        displayName: ctrl.view.emailSearch,
      });
      ctrl.state.emailSearch.counter += 1;
      ctrl.view.emailSearch = '';
    }

    function onRemove(id) {
      _.remove(ctrl.data.selectedEmails, email => email.id === id);
    }

    function isGenerateBillingDisabled() {
      return ctrl.data.selectedEmails.length === 0 || ctrl.data.isOutOfRange;
    }

    function calculateDateRange() {
      $timeout(() => {
        if (ctrl.data && ctrl.data.startDate && ctrl.data.endDate) {
          const range = nvDateTimeUtils.dayBetweenDates(ctrl.data.endDate, ctrl.data.startDate);
          if (range >= 60) {
            ctrl.data.isOutOfRange = true;
          } else {
            ctrl.data.isOutOfRange = false;
          }
        }
      });
    }

    function generateSuccessBillings() {
      if (_.compact(ctrl.view.generated).length <= 0) {
        nvToast.error(
          nvTranslate.instant('container.success-billings.error-submitting-order-billing-report'),
          nvTranslate.instant('container.success-billings.please-select-at-least-one-file-to-generate')
        );
        return false;
      }


      const payload = generateBillingPayload();
      ctrl.view.state = 'waiting';
      return DWH.reportsBilling(payload).then(onSuccess, onFailed);
      function onSuccess() {
        ctrl.view.state = 'idle';
        nvToast.info(
          nvTranslate.instant('container.success-billings.toast-title'),
          [
            `Creation Range: ${moment(ctrl.data.startDate.getTime()).format(nvDateTimeUtils.FORMAT_DATE)} 
              to ${moment(ctrl.data.endDate.getTime()).format(nvDateTimeUtils.FORMAT_DATE)}`,
            `Email to: ${_.join(payload.email_addresses, ',')}`,
          ]
        );
      }

      function onFailed() {
        ctrl.view.state = 'idle';
      }
    }

    function generateBillingPayload() {
      // send in the date in utc format     
      const startDate = nvDateTimeUtils.displayFormat(nvDateTimeUtils.toSOD(ctrl.data.startDate, nvTimezone.getOperatorTimezone()), nvDateTimeUtils.FORMAT_ISO_TZ, 'utc');
      const endDate = nvDateTimeUtils.displayFormat(nvDateTimeUtils.toEOD(ctrl.data.endDate, nvTimezone.getOperatorTimezone()), nvDateTimeUtils.FORMAT_ISO_TZ, 'utc');
      const payload = {
        start_date: startDate,
        end_date: endDate,
        timezone: nvTimezone.getOperatorTimezone(),
        email_addresses: _.map(ctrl.data.selectedEmails, email => email.displayName),
        consolidated_options: _.compact(_.map(ctrl.view.generated, (v, k) => {
          if (v === true) {
            return ctrl.data.consolidateOptions[k];
          }
          return null;
        })),
      };

      if (ctrl.data.completed === '10') {
        // "completed" selected
        payload.type = 'COMPLETED';
      } else {
        payload.type = 'CREATED';
      }

      if (ctrl.data.allShippers === '10') {
        // all shippers
        payload.shipper_ids = [];
      } else {
        const filter = ctrl.data.fiters.shipperFilter;
        payload.shipper_ids = filter.selectedOptions.length === 0 ?
          [] : _.map(filter.selectedOptions, shipper => shipper.id);
      }

      return payload;
    }
  }
}());
