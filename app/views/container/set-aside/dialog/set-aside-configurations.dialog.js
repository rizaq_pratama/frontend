(function controller() {
  angular
    .module('nvOperatorApp.controllers')
    .controller('SetAsideConfigurationsController', SetAsideConfigurationsController);

  SetAsideConfigurationsController.$inject = ['$mdDialog', 'parameter', 'Hub', 'nvAutocomplete.Data', '$q',
    'SetAside', 'Transaction', 'nvToast', 'nvTranslate', 'Zone'];

  const DEFAULT_EMPTY = [];

  const VIEW = {
    LOADING: 0,
    CONTENT: 1,
  };

  function SetAsideConfigurationsController($mdDialog, parameter, Hub, nvAutocompleteData, $q,
    SetAside, Transaction, nvToast, nvTranslate, Zone) {
    const ctrl = this;

    ctrl.data = {};
    ctrl.view = {};
    ctrl.function = {};
    ctrl.state = {};

    ctrl.view.lce = VIEW;

    ctrl.function.onCancel = cancel;
    ctrl.function.getSelectedHubs = getSelectedHubs;
    ctrl.function.removeHub = removeHub;
    ctrl.function.getSelectedZones = getSelectedZones;
    ctrl.function.removeZone = removeZone;
    ctrl.function.getSelectedDnrs = getSelectedDnrs;
    ctrl.function.removeDnr = removeDnr;
    ctrl.function.disableSetAside = disableSetAside;
    ctrl.function.isResetSetAsideDisabled = isResetSetAsideDisabled;
    ctrl.function.resetSetAside = resetSetAside;
    ctrl.function.resetSetAsideCounter = resetSetAsideCounter;
    ctrl.function.recalcCounter = recalcCounter;
    ctrl.function.isValidSetAsideSettings = isValidSetAsideSettings;
    ctrl.function.updateSetAside = updateSetAside;
    ctrl.function.isRecalcCounterDisabled = isRecalcCounterDisabled;
    ctrl.function.isResetCounterDisabled = isResetCounterDisabled;
    ctrl.function.setSoftLimit = setSoftLimit;
    ctrl.function.isSoftLimitDisabled = isSoftLimitDisabled;

    ctrl.state.view = VIEW.LOADING;

    init();

    function cancel() {
      $mdDialog.hide(-1);
    }

    function init() {
      $q.all([Hub.read(), SetAside.readSetAside(), Zone.read()]).then(success);

      function success(responses) {
        const hubs = responses[0];
        const setAside = responses[1];
        const zones = responses[2];
        const dnrs = _.pickBy(Transaction.DNR, e => e >= 10);
        initDnrs(dnrs);
        initViewData(hubs, setAside, dnrs, zones);
        
        initMain();
        initReset();
        initResetCounter();
        initSoftLimit();
        ctrl.state.view = VIEW.CONTENT;
      }
    }

    function initDnrs(dnrs) {
      dnrs.container = _(dnrs)
        .keys()
        .map(e => _.assign({}, { name: e, value: dnrs[e], id: dnrs[e] }))
        .value();
      // extended usage
      dnrs.asOptions = _.map(dnrs.container, e => _.assign({}, { value: e, displayName: e.name }));
      dnrs.containerChunked = _.chunk(dnrs.container, 3);
    }

    function initViewData(hubsResponse, setAsideResponse, dnrs, zones) {
      ctrl.data.setAside = setAsideResponse;
      ctrl.data.hubs = hubsResponse;
      ctrl.data.dnrs = dnrs;
      ctrl.data.zones = zones;

      // main
      ctrl.view.main = _.assign(ctrl.view.main, {
        dnrOptions: _.cloneDeep(dnrs.asOptions),
        hubAutoComplete: nvAutocompleteData.getByHandle('set-aside-hubs-autocomplete'),
        zoneAutoComplete: nvAutocompleteData.getByHandle('set-aside-zones-autocomplete'),
      });

      // reset
      ctrl.view.reset = _.assign(ctrl.view.reset, {
        dnrOptions: _.cloneDeep(dnrs.asOptions),
      });

      // reset counter
      ctrl.view.resetCounter = _.assign(ctrl.view.resetCounter, {
        dnrAutoComplete: nvAutocompleteData.getByHandle('set-aside-dnr-autocomplete'),
      });
    }

    function initMain() {
      const selectedDnr = _.find(ctrl.data.dnrs.asOptions, opt => _.get(opt, 'value.value') === ctrl.data.setAside.setAsideGroup);
      ctrl.data.main = _.assign({}, {
        selectedDnr: _.get(selectedDnr, 'value'),
        maxDeliveryDays: ctrl.data.setAside.setAsideMaxDeliveryDays,
        hubSearchText: null,
        zoneSearchText: null,
      });

      ctrl.state.main = _.assign(ctrl.state.main, {
        disable: 'idle',
        update: 'idle',
      });

      // select selected hubs
      const hubs = Hub.toOptions(ctrl.data.hubs);
      const selectedHubIds = ctrl.data.setAside.setAsideHubs || [];
      const selectedHubs = _.remove(hubs, hub => _.find(selectedHubIds, id => hub.value === id));
      ctrl.view.main.hubAutoComplete.setPossibleOptions(hubs);
      ctrl.view.main.hubAutoComplete.setSelectedOptions(selectedHubs);

      // select selected zones
      const zones = Zone.toOptions(ctrl.data.zones);
      const selectedZoneIds = ctrl.data.setAside.setAsideZones || [];
      const selectedZones = _.remove(zones, zone => _.find(selectedZoneIds, id => zone.value === id));
      ctrl.view.main.zoneAutoComplete.setPossibleOptions(zones);
      ctrl.view.main.zoneAutoComplete.setSelectedOptions(selectedZones);
    }

    function initReset() {
      ctrl.data.reset = _.assign(ctrl.data.reset, {
        selectedDnr: null,
      });

      ctrl.state.reset = _.assign(ctrl.state.reset, {
        reset: 'idle',
      });
    }

    function initResetCounter() {
      ctrl.view.resetCounter.dnrAutoComplete.setPossibleOptions(
        _.cloneDeep(ctrl.data.dnrs.asOptions)
      );
      ctrl.data.resetCounter = _.assign(ctrl.data.resetCounter, {
        searchText: null,
      });

      ctrl.state.resetCounter = _.assign(ctrl.state.resetCounter, {
        reset: 'idle',
        recalc: 'idle',
      });
    }

    function initSoftLimit() {
      ctrl.data.softLimit = _.assign(ctrl.data.softLimit, {
        value: 0,
      });

      ctrl.state.softLimit = _.assign(ctrl.state.softLimit, {
        set: 'idle',
      });
    }

    function getSelectedHubs() {
      if (ctrl.view.main && ctrl.view.main.hubAutoComplete) {
        return ctrl.view.main.hubAutoComplete.getSelectedOptions();
      }
      return DEFAULT_EMPTY;
    }

    function removeHub(item) {
      ctrl.view.main.hubAutoComplete.remove(item);
    }

    function getSelectedZones() {
      if (ctrl.view.main && ctrl.view.main.zoneAutoComplete) {
        return ctrl.view.main.zoneAutoComplete.getSelectedOptions();
      }
      return DEFAULT_EMPTY;
    }

    function removeZone(item) {
      ctrl.view.main.zoneAutoComplete.remove(item);
    }

    function getSelectedDnrs() {
      if (ctrl.view.resetCounter && ctrl.view.resetCounter.dnrAutoComplete) {
        return ctrl.view.resetCounter.dnrAutoComplete.getSelectedOptions();
      }
      return DEFAULT_EMPTY;
    }

    function removeDnr(item) {
      ctrl.view.resetCounter.dnrAutoComplete.remove(item);
    }

    function disableSetAside() {
      ctrl.state.main.disable = 'loading';
      ctrl.state.main.update = 'loading';

      SetAside.disable().then(() => {
        SetAside.readSetAside()
          .then((response) => {
            nvToast.success(nvTranslate.instant('container.set-aside.notifications.set-aside-disabled'));
            ctrl.data.setAside = response;
          })
          .finally(() => {
            initMain();
          });
      }, () => initMain());
    }

    function isResetSetAsideDisabled() {
      return !(ctrl.data.reset && ctrl.data.reset.selectedDnr);
    }

    function resetSetAside() {
      ctrl.state.reset.reset = 'waiting';
      SetAside.resetSetAside(ctrl.data.reset.selectedDnr.value)
        .then(() => {
          nvToast.success(nvTranslate.instant('container.set-aside.notifications.set-aside-reset'));
        })
        .finally(() => {
          ctrl.state.reset.reset = 'idle';
          ctrl.data.reset.selectedDnr = null;
        });
    }

    function resetSetAsideCounter() {
      ctrl.state.resetCounter.reset = 'waiting';
      SetAside.resetSetAsideCounter(_.map(getSelectedDnrs(), dnr => dnr.value.value))
        .then(() => {
          nvToast.success(nvTranslate.instant('container.set-aside.notifications.set-aside-counter-reset'));
        })
        .finally(() => {
          ctrl.state.resetCounter.reset = 'idle';
        });
    }

    function recalcCounter() {
      ctrl.state.resetCounter.recalc = 'waiting';
      SetAside.recalc(_.map(getSelectedDnrs(), dnr => dnr.value.value))
        .then(() => {
          nvToast.success(nvTranslate.instant('container.set-aside.notifications.set-aside-recalc-success'));
        })
        .finally(() => {
          ctrl.state.resetCounter.recalc = 'idle';
        });
    }

    function isValidSetAsideSettings() {
      const maxDeliveryDays = _.get(ctrl, 'data.main.maxDeliveryDays');
      const isValidDeliveryDays = maxDeliveryDays && maxDeliveryDays > 0;
      const isValidDnr = ctrl.data.main.selectedDnr != null;

      return isValidDeliveryDays && isValidDnr;
    }
    
    function updateSetAside() {
      if (!isValidSetAsideSettings()) {
        nvToast.warning('max delivery date must > 0');
      }
      const payload = {
        setAsideGroup: ctrl.data.main.selectedDnr.value,
        setAsideHubs: _.map(getSelectedHubs(), el => el.value),
        setAsideZones: _.map(getSelectedZones(), el => el.value),
        setAsideMaxDeliveryDays: ctrl.data.main.maxDeliveryDays,
      };
      ctrl.state.main.disable = 'loading';
      ctrl.state.main.update = 'loading';

      SetAside.enable(payload).then(() => {
        SetAside.readSetAside()
          .then((response) => {
            nvToast.success(nvTranslate.instant('container.set-aside.notifications.set-aside-updated'));
            ctrl.data.setAside = response;
          })
          .finally(() => {
            initMain();
          }
        );
      }, () => () => initMain());
    }

    function isRecalcCounterDisabled() {
      return getSelectedDnrs().length === 0;
    }

    function isResetCounterDisabled() {
      return getSelectedDnrs().length === 0;
    }

    function setSoftLimit() {
      ctrl.state.softLimit.set = 'waiting';
      const limit = ctrl.data.softLimit.value;
      SetAside.limit(limit)
        .then(() => nvToast.success(nvTranslate.instant('container.set-aside.notifications.set-aside-soft-limit-success')))
        .finally(() => (ctrl.state.softLimit.set = 'idle'));
    }

    function isSoftLimitDisabled() {
      return _.get(ctrl, 'data.softLimit.value') == null;
    }
  }
}());
