(function controller() {
  angular
    .module('nvOperatorApp.controllers')
    .controller('SetAsideController', SetAsideController);

  SetAsideController.$inject = ['Transaction', 'Hub', 'SetAside', '$q', 'nvDialog', 'nvTable',
    'nvDateTimeUtils', 'nvTimezone', 'nvTranslate'];

  const TRANSACTION_FIELDS = {
    orderId: { displayName: 'commons.order-id' },
    transactionId: { displayName: 'container.set-aside.transaction-id' },
    orderStatus: { displayName: 'container.set-aside.order-status' },
    transactionStatus: { displayName: 'container.set-aside.transaction-status' },
    granularStatus: { displayName: 'container.set-aside.granular-status' },
    startTime: { displayName: 'commons.start-time' },
    endTime: { displayName: 'commons.end-time' },
    routeId: { displayName: 'commons.model.route-id' },
    dnr: { displayName: 'commons.model.dnr' },
  };

  function SetAsideController(Transaction, Hub, SetAside, $q, nvDialog, nvTable,
    nvDateTimeUtils, nvTimezone, nvTranslate) {
    const ctrl = this;

    ctrl.data = {};
    ctrl.function = {};
    ctrl.state = {};

    ctrl.function.init = init;
    ctrl.function.openSettings = openSettings;
    ctrl.function.switchToPage = switchToPage;
    ctrl.function.fetchTransactions = fetchTransactions;
    ctrl.function.isTransactionDisabled = isTransactionDisabled;

    function init() {
      switchToPage(0);
      ctrl.state.fetch = 'idle';

      ctrl.data.selectedDnr = null;

      return $q.all([Hub.read(), SetAside.readSetAside(), SetAside.getCounter()]).then(success);

      function success(responses) {
        ctrl.data.hubs = responses[0];
        ctrl.data.setAside = responses[1];
        ctrl.data.counter = responses[2];

        refreshDnrs();
      }
    }

    function openSettings($event) {
      nvDialog.showSingle($event, {
        templateUrl: 'views/container/set-aside/dialog/set-aside-configurations.dialog.html',
        theme: 'nvBlue',
        cssClass: 'set-aside-configurations-dialog',
        controller: 'SetAsideConfigurationsController',
        controllerAs: 'ctrl',
        locals: {
          parameter: {
            hubs: ctrl.data.hubs,
            dnrs: ctrl.data.dnrs,
            setAside: ctrl.data.setAside,
          },
        },
      });
    }

    function refreshDnrs() {
      ctrl.data.dnrs = _.pickBy(Transaction.DNR, e => e >= 10);
      ctrl.data.dnrs.container = _(ctrl.data.dnrs)
        .keys()
        .map(e => _.assign({}, { name: e, value: ctrl.data.dnrs[e], id: ctrl.data.dnrs[e], counter: ctrl.data.counter[e] }))
        .value();

      // extended usage
      ctrl.data.dnrs.asOptions = _.map(ctrl.data.dnrs.container, e =>
        _.assign({}, { value: e, displayName: e.name }));
      ctrl.data.dnrs.containerChunked = _(ctrl.data.dnrs.container).chunk(4).unzip().value();
    }

    function switchToPage(page) {
      ctrl.state.page = page;
    }

    function fetchTransactions($event, dnr) {
      ctrl.state.fetch = 'waiting';
      SetAside.readTransactions(dnr.value)
        .then(success)
        .finally(() => (ctrl.state.fetch = 'idle'));

      function success(response) {
        if (response && response.length === 0) {
          nvDialog.alert($event, {
            title: nvTranslate.instant('container.set-aside.no-transactions-found'),
            content: nvTranslate.instant('container.set-aside.no-transactions-found-on-specified-group'),
          });
        } else {
          const datas = extendData(response);
          initTransactionTable(datas);
          switchToPage(1);
        }
      }

      function extendData(datas) {
        return _.map(datas, (data) => {
          const startTime = nvDateTimeUtils.toMoment(data.startTime, nvTimezone.getOperatorTimezone());
          const endTime = nvDateTimeUtils.toMoment(data.endTime, nvTimezone.getOperatorTimezone());

          return {
            routeId: data.routeId || '-',
            orderId: data.orderId,
            transactionStatus: data.transactionStatus,
            granularStatus: data.granularStatus,
            orderStatus: data.orderStatus,
            dnr: data.dnr,
            transactionId: data.transactionId,
            trackingId: data.trackingId,
            startTime: nvDateTimeUtils.displayDateTime(startTime, nvTimezone.getOperatorTimezone()),
            endTime: nvDateTimeUtils.displayDateTime(endTime, nvTimezone.getOperatorTimezone()),
          };
        });
      }
    }

    function initTransactionTable(datas) {
      ctrl.data.tableParam = nvTable.createTable(TRANSACTION_FIELDS);
      ctrl.data.tableParam.setData(datas);
    }

    function isTransactionDisabled() {
      return ctrl.data.selectedDnr == null;
    }
  }
}());
