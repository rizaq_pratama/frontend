(function controller() {
  angular
    .module('nvOperatorApp.controllers')
    .controller('ThirdPartyOrderController', ThirdPartyOrderController);

  ThirdPartyOrderController.$inject = ['ThirdPartyOrder', 'ThirdPartyShipper', 'nvTable', 'nvDialog',
                                       'nvEditModelDialog', 'nvToast', 'nvTranslate', '$stateParams',
                                       'nvFileSelectDialog', 'nvButtonFilePickerType', 'nvTimezone',
                                       '$state', '$window'];
  function ThirdPartyOrderController(ThirdPartyOrder, ThirdPartyShipper, nvTable, nvDialog,
                                      nvEditModelDialog, nvToast, nvTranslate, $stateParams,
                                      nvFileSelectDialog, nvButtonFilePickerType, nvTimezone,
                                      $state, $window) {
    const ctrl = this;

    ctrl.tableParam = nvTable.createTable(ThirdPartyOrder.FIELDS);
    ctrl.tableParam.setDataPromise(ThirdPartyOrder.read());
    ctrl.tableParam.prefilter($stateParams);

    ctrl.updateOne = updateOne;
    ctrl.untagOne = untagOne;
    ctrl.uploadOne = uploadOne;
    ctrl.uploadCSV = uploadCSV;
    ctrl.editOrder = editOrder;

    ctrl.loadShippers = loadShippers;
    ctrl.nvTimezone = nvTimezone;

    function updateOne($event, order) {
      const model = _.find(ctrl.tableParam.getTableData(), ['id', order.id]);
      const fields = ThirdPartyOrder.getEditFields(model);
      nvDialog.showSingle($event, {
        templateUrl: 'views/container/third-party-order/dialog/third-party-order-edit.dialog.html',
        cssClass: 'third-party-order-edit',
        scope: _.assign(nvEditModelDialog.scopeTemplate({ onSave: update }), {
          fields: fields,
        }),
      }).then(success);

      function update(data) {
        const payload = [{
          id: data.thirdPartyOrder.id,
          trackingId: data.trackingId,
          thirdPartyTrackingId: data.thirdPartyOrder.thirdPartyTrackingId,
          thirdPartyShipperId: data.thirdPartyOrder.thirdPartyShipper.id,
        }];
        return ThirdPartyOrder.update(payload);
      }

      function success(updatedOrder) {
        ctrl.tableParam.mergeIn(updatedOrder, 'id');
        nvToast.success(nvTranslate.instant('container.third-party-order.mapping-created', updatedOrder[0]));
      }
    }

    function untagOne($event, order) {
      nvDialog.confirmDelete($event, {
        title: nvTranslate.instant('container.third-party-order.untag-title'),
        content: nvTranslate.instant('container.third-party-order.untag-content'),
        ok: nvTranslate.instant('container.third-party-order.untag-ok'),
      }).then(done);

      function done() {
        return ThirdPartyOrder.untag([order.thirdPartyOrder.thirdPartyTrackingId])
        .then(deletedOrder => {
          ctrl.tableParam.remove(deletedOrder, 'id');
          nvToast.success(nvTranslate.instant('container.third-party-order.untag-success'));
        });
      }
    }

    function uploadOne($event) {
      const fields = ThirdPartyOrder.getAddFields();

      nvDialog.showSingle($event, {
        templateUrl: 'views/container/third-party-order/dialog/third-party-order-add.dialog.html',
        cssClass: 'third-party-order-add',
        theme: 'nvGreen',
        scope: _.assign(nvEditModelDialog.scopeTemplate({ onSave: create }), {
          fields: fields,
        }),
      }).then(_.partial(showUploadResult, $event));

      function create(data) {
        const payload = [{
          trackingId: data.trackingId,
          thirdPartyTrackingId: data.thirdPartyOrder.thirdPartyTrackingId,
          thirdPartyShipperId: data.thirdPartyOrder.thirdPartyShipper.id,
        }];
        return ThirdPartyOrder.uploadSingle(payload);
      }
    }

    function uploadCSV($event) {
      nvFileSelectDialog.show($event, {
        title: nvTranslate.instant('container.third-party-order.add-order-bulk'),
        fileSelect: {
          accept: nvButtonFilePickerType.CSV,
          maxFileCount: 1,
        },
        buttonOkTitle: nvTranslate.instant('commons.submit'),
        onFinishSelect: onFinishSelect,
        onDone: _.partial(showUploadResult, $event),
        showErrorToast: false,
        sampleCSV: {
          data: [{
            trackingId: 'NV012345',
            thirdPartyShipperId: 30,
            thirdPartyTrackingId: 'A113322',
          }, {
            trackingId: 'NV012346',
            thirdPartyShipperId: 30,
            thirdPartyTrackingId: 'A113323',
          }],
          header: '["NV Tracking ID", "Third Party Shipper ID", "Third Party Tracking Id"]',
          columnOrder: '["trackingId", "thirdPartyShipperId", "thirdPartyTrackingId"]',
          filename: 'sample-third-party-mapping-upload.csv',
        },
      });

      function onFinishSelect(files) {
        return ThirdPartyOrder.uploadCSV(files[0]);
      }
    }

    function editOrder(orderId) {
      $window.open(
        $state.href('container.order.edit', { orderId: orderId })
      );
    }

    function showUploadResult($event, result) {
      ctrl.tableParam.mergeIn(_.filter(result, order => order.resultStatus.code === 200), 'id');
      nvDialog.showSingle($event, {
        templateUrl: 'views/container/third-party-order/dialog/third-party-order-add-result.dialog.html',
        cssClass: 'third-party-order-add-result',
        scope: _.assign(nvEditModelDialog.scopeTemplate(), { result: result }),
      });
    }

    function loadShippers() {
      return ThirdPartyShipper.read().then((data) => {
        ThirdPartyOrder.setThirdPartyOptions(data);
      });
    }
  }
}());
