(function controller() {
  angular
          .module('nvOperatorApp.controllers')
          .controller('HubGroupManagementController', HubGroupManagementController);
  HubGroupManagementController.$inject = ['Hub', '$q', '$timeout',
    'nvTable', 'nvDialog', 'nvEditModelDialog',
    'nvToast', '$log', 'nvTranslate'];

  function HubGroupManagementController(Hub, $q, $timeout,
    nvTable, nvDialog, nvEditModelDialog,
    nvToast, $log, nvTranslate) {
    const ctrl = this;
    const HUBS_CACHE = {};
    ctrl.tableParam = nvTable.createTable(Hub.HUB_GROUP_FIELDS);
    ctrl.loadingSheet = true;
    ctrl.init = init;
    ctrl.createHubGroupDialog = createHubGroupDialog;
    ctrl.editHubGroupDialog = editHubGroupDialog;
    ctrl.deleteSelectedHubGroup = deleteSelectedHubGroup;
    ctrl.showHubInfo = showHubInfo;
    ctrl.init();
    ctrl.hubs = [];

    function init() {
      ctrl.loadingSheet = true;
      return $q.all([
        Hub.getHubGroups(),
        Hub.read(),
      ]).then(onSuccess);

      function onSuccess(results) {
        _.forEach(results[1], (hub) => {
          HUBS_CACHE[hub.id] = hub;
        });
        ctrl.hubs = _.cloneDeep(results[1]);

        const data = _.map(results[0], (hubGroup) => {
          addHubData(hubGroup);
          return hubGroup;
        });
        ctrl.tableParam.setData(data);
        ctrl.tableParam.updateColumnFilter('hubs', strFn => (
          (row) => {
            const lowercase = (strFn() || '').toLowerCase();
            const hubsName = _.map(_.get(row, 'hubs'), (h) => (h.name)).join(',');
            return _.toLower(hubsName).includes(lowercase);
          }
        ));
        ctrl.loadingSheet = false;
      }
    }

    function addHubData(hubGroup) {
      hubGroup.hubs = _.compact(_.map(hubGroup.hub_ids, id => (HUBS_CACHE[id])));
      return hubGroup;
    }

    function editHubGroupDialog($event, hubGroup) {
      return nvDialog.showSingle($event, {
        templateUrl: 'views/container/hub-group-management/dialog/hub-group-add.dialog.html',
        locals: {
          Data: {
            hubs: ctrl.hubs,
            mode: 'edit',
            hubGroup: hubGroup,
          },
        },
        controllerAs: 'ctrl',
        controller: 'HubGroupAddDialogController',
        theme: 'nvBlue',
        clickOutsideToClose: false,
        cssClass: 'nv-hub-group-add-dialog',
      }).then(onSave);
      function onSave(hubGroupResult) {
        nvToast.info(nvTranslate.instant('container.hub-group-management.hub-group-x-successfully-edited', { x: hubGroupResult.id }));
        addHubData(hubGroupResult);
        ctrl.tableParam.mergeIn(hubGroupResult, 'id');
        $log.debug(hubGroupResult);
      }
    }

    function createHubGroupDialog($event) {
      return nvDialog.showSingle($event, {
        templateUrl: 'views/container/hub-group-management/dialog/hub-group-add.dialog.html',
        locals: {
          Data: {
            hubs: ctrl.hubs,
            mode: 'create',
          },
        },
        controllerAs: 'ctrl',
        controller: 'HubGroupAddDialogController',
        theme: 'nvGreen',
        clickOutsideToClose: false,
        cssClass: 'nv-hub-group-add-dialog',
      }).then(onSave);
      function onSave(hubGroup) {
        nvToast.info(nvTranslate.instant('container.hub-group-management.hub-group-x-successfully-created', { x: hubGroup.id }));
        addHubData(hubGroup);
        ctrl.tableParam.mergeIn(hubGroup, 'id');
        $log.debug(hubGroup);
      }
    }

    function showHubInfo($event, model) {
      model.short_name = _.get(model, 'shortName', model.short_name);
      const fields = Hub.getEditFields(model);

      nvDialog.showSingle($event, {
        templateUrl: 'views/container/hub-list/dialog/hub-view.dialog.html',
        cssClass: 'hub-edit',
        scope: _.assign(
          nvEditModelDialog.scopeTemplate({ onSave: angular.noop() }),
          { fields: fields }),
      }).then(success);
      function success(hub) {
        $log.debug(hub);
      }
    }

    function deleteSelectedHubGroup($event, hubGroup) {
      return nvDialog.confirmDelete($event)
        .then(onDelete);

      function onDelete() {
        return Hub.deleteHubGroup(hubGroup.id).then(() => {
          nvToast.info(nvTranslate.instant('container.hub-group-management.hub-group-x-successfully-deleted', { x: hubGroup.id }));
          ctrl.tableParam.remove(hubGroup, 'id');
        });
      }
    }
  }
}());
