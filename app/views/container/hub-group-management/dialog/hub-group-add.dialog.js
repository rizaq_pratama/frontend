(function controller() {
  angular
          .module('nvOperatorApp.controllers')
          .controller('HubGroupAddDialogController', HubGroupAddDialogController);
  HubGroupAddDialogController.$inject = ['Data', 'Hub', 'nvAutocomplete.Data', '$mdDialog', 'nvTranslate'];

  function HubGroupAddDialogController(Data, Hub, nvAutocompleteData, $mdDialog, nvTranslate) {
    const ctrl = this;
    const MODE = {
      EDIT: 'edit',
      CREATE: 'create',
    };
    const STATE = {
      IDLE: 'idle',
      WAITING: 'waiting',
    };

    ctrl.saveState = STATE.IDLE;
    ctrl.mode = Data.mode;
    ctrl.addHub = addHub;
    ctrl.removeHub = removeHub;
    ctrl.saveHubGroup = saveHubGroup;
    ctrl.onCancel = onCancel;
    ctrl.hubOptions = Hub.toOptions(_.get(Data, 'hubs'));
    ctrl.hubSearch = nvAutocompleteData.getByHandle('hubs-auto');
    ctrl.data = {
      name: '',
      hubs: [],
    };
    init();

    function init() {
      if (ctrl.mode === MODE.EDIT) {
        ctrl.dialogTitle = nvTranslate.instant('container.hub-group-management.edit-hub-group');
        ctrl.data = _.defaults(_.cloneDeep(Data.hubGroup), { hubs: [] });
      } else {
        ctrl.dialogTitle = nvTranslate.instant('container.hub-group-management.create-hub-group');
      }

      ctrl.hubSearch.setPossibleOptions(_.cloneDeep(ctrl.hubOptions));
    }

    function addHub(hub) {
      if (!_.find(ctrl.data.hubs, h => (hub.value === h.id || hub.value === h.value))) {
        ctrl.data.hubs.unshift(hub);
      }
      ctrl.selectedHub = null;
      ctrl.hubSearchText = null;
    }

    function removeHub(hub) {
      _.remove(ctrl.data.hubs, hub);
    }

    function saveHubGroup() {
      ctrl.saveState = STATE.WAITING;
      ctrl.data.hub_ids = _.compact(_.map(ctrl.data.hubs, hub => (hub.value || hub.id)));
      if (ctrl.mode === MODE.CREATE) {
        return Hub.createHubGroup(_.omit(ctrl.data, 'hubs')).then(onSuccess);
      }
      return Hub.updateHubGroup(_.omit(ctrl.data, ['hubs', 'id']), ctrl.data.id).then(onSuccess);

      function onSuccess(result) {
        ctrl.saveState = STATE.IDLE;
        return $mdDialog.hide(result);
      }
    }

    function onCancel() {
      return $mdDialog.cancel();
    }
  }
}());
