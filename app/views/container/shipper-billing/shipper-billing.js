(function ShipperBillingController() {
  angular
    .module('nvOperatorApp.controllers')
    .controller('ShipperBillingController', Controller);

  Controller.$inject = ['nvAutocomplete.Data', 'nvModelUtils', 'nvCurrency', '$rootScope', '$timeout', 'Shippers',
    'nvTranslate', 'Order', '$q', 'Wallet', 'nvDialog', 'nvDateTimeUtils', '$scope'];

  function Controller(nvAutocompleteData, nvModelUtils, nvCurrency, $rootScope, $timeout, Shippers,
    nvTranslate, Order, $q, Wallet, nvDialog, nvDateTimeUtils, $scope) {
    const ctrl = this;
    const TEXT_DEDUCTION = `- ${nvCurrency.getCode($rootScope.countryId)} `;
    const TEXT_ADD = `+ ${nvCurrency.getCode($rootScope.countryId)} `;

    const OPTION_REASON_ID = {
      PICKUP_SURCHARGE: 0,
      DELIVERY_FEE: 1,
      REFUND_SERVICE_RECOVERY: 2,
      BANK_TRANSFER: 3,
    };

    const OPTION_REASON = [
      { value: OPTION_REASON_ID.PICKUP_SURCHARGE, displayName: 'container.shipper-billing.reason-pickup-surcharge' },
      { value: OPTION_REASON_ID.DELIVERY_FEE, displayName: nvTranslate.instant('container.shipper-billing.reason-delivery-fee') },
      { value: OPTION_REASON_ID.REFUND_SERVICE_RECOVERY, displayName: nvTranslate.instant('container.shipper-billing.reason-refund-service-recovery') },
      { value: OPTION_REASON_ID.BANK_TRANSFER, displayName: nvTranslate.instant('container.shipper-billing.reason-bank-transfer') },
    ];

    ctrl.onTransferModeChange = onTransferModeChange;
    ctrl.submitState = 'idle';
    ctrl.onReview = onReview;
    ctrl.onSubmit = onSubmit;
    ctrl.onCancel = onCancel;
    ctrl.isReviewDisabled = isReviewDisabled;
    ctrl.reasonIds = OPTION_REASON_ID;
    ctrl.reasonOption = OPTION_REASON;

    init();

    function init() {
      // update shipper billing part
      ctrl.transferMode = '01';
      ctrl.amount = 0;
      ctrl.currencyText = getCurrencyText();
      ctrl.reasonId = null;
      ctrl.bankTransferDetails = '';
      ctrl.pickupSurchargeDetails = '';

      ctrl.shipperSearchText = '';
      ctrl.shipperSearch = nvAutocompleteData.getByHandle('shipper-billing');
      ctrl.shipperSearch.onSelect(onShipperSelect);
      ctrl.shipperSearch.setPossibleOptions(getShippers);

      ctrl.orderSearchText = '';
      ctrl.orderSearch = nvAutocompleteData.getByHandle('order-shipper-billing');
      ctrl.orderSearch.onSelect(onOrderSelect);
      ctrl.orderSearch.setPossibleOptions(getOrders);

      // review details part
      ctrl.isReviewAvailable = false;
      ctrl.review = {};

      function getShippers(text) {
        return Shippers.filterSearch(text);
      }

      function getOrders(trackingId) {
        const deferred = $q.defer();
        const searchFilters = ctrl.selectedShipper ?
          [{ field: 'shipper_id', values: [ctrl.selectedShipper.legacy_id] }] :
          [];
        const filterPayload = {
          search_field: {
            fields: ['tracking_id', 'stamp_id'],
            match_type: 'full_text',
            value: trackingId,
          },
          search_filters: searchFilters,
        };
        Order.elasticSearch(filterPayload).then(elasticSuccess, elasticError);

        return deferred.promise;

        function elasticSuccess(response) {
          const orders = extendESOrders(_.map(response.search_data, 'order'));
          deferred.resolve(orders);
        }

        function elasticError() {
          deferred.reject(-1);
        }

        function extendESOrders(orders) {
          return _.map(orders, order => setCustomESOrderData(order));

          function setCustomESOrderData(order) {
            _.forEach(order, (value, key) => {
              order[snakeToCamel(key)] = value;
            });

            // set status and granularStatus as enum
            order.status = Order.getStatusEnum(order.status);
            order.granularStatus = Order.getGranularStatusEnum(order.granularStatus);
            order.displayName = _.get(order, 'tracking_id');
            return order;
          }

          function snakeToCamel(string) {
            return string.replace(/(_\w)/g, m => m[1].toUpperCase());
          }
        }
      }
    }

    function onShipperSelect(shipper) {
      ctrl.selectedShipper = shipper;
    }

    function onOrderSelect(order) {
      ctrl.selectedOrder = order;
    }

    function onTransferModeChange() {
      ctrl.currencyText = getCurrencyText();
    }

    function isReviewDisabled() {
      if (ctrl.selectedShipper == null) {
        return true;
      }
      if (ctrl.reasonId === OPTION_REASON_ID.DELIVERY_FEE
        || ctrl.reasonId === OPTION_REASON_ID.REFUND_SERVICE_RECOVERY) {
        return ctrl.selectedOrder === null;
      }
      return false;
    }

    function onReview() {
      ctrl.isReviewAvailable = true;

      const isDeduct = !nvModelUtils.fromYesNoBitStringToBool(ctrl.transferMode);

      ctrl.review.shipperId = ctrl.selectedShipper.legacy_id;
      ctrl.review.shipperName = ctrl.selectedShipper.name;
      ctrl.review.mode = isDeduct ? 'Deduct' : 'Add';
      ctrl.review.amount = `${getCurrencyText()}${formatNumber(ctrl.amount)}`;
      ctrl.review.reason = getReason();

      function getReason() {
        const baseReason = _.get(_.find(OPTION_REASON, reason => reason.value === ctrl.reasonId), 'displayName');
        let specificReason;

        switch (ctrl.reasonId) {
          case OPTION_REASON_ID.PICKUP_SURCHARGE:
            specificReason = ctrl.pickupSurchargeDetails;
            break;
          case OPTION_REASON_ID.BANK_TRANSFER:
            specificReason = ctrl.bankTransferDetails;
            break;
          case OPTION_REASON_ID.DELIVERY_FEE:
          case OPTION_REASON_ID.REFUND_SERVICE_RECOVERY:
            specificReason = _.get(ctrl.selectedOrder, 'tracking_id');
            break;
          default :
            specificReason = 'Error';
            break;
        }

        return `${nvTranslate.instant(baseReason)} - ${specificReason}`;
      }

      function formatNumber(number) {
        return parseFloat(Math.round(number * 100) / 100).toFixed(2);
      }
    }

    function onSubmit($event) {
      const payload = generatePayload();

      ctrl.submitState = 'waiting';
      Wallet.readjust(payload)
        .then(onSuccess, onError)
        .finally(() => (ctrl.submitState = 'idle'));

      function onSuccess(response) {
        nvDialog.alert($event, {
          title: nvTranslate.instant('container.shipper-billing.billing-updated'),
          content: nvTranslate.instant('container.shipper-billing.billing-updated-successfully', { refNo: response.transaction_ref }),
          theme: 'nvGreen',
        }).then(() => resetForm());
      }

      function onError(response) {
        const message = _.get(response, 'message');
        let errorMessage = nvTranslate.instant('container.shipper-billing.unknown-error-ocurred');
        if (message) {
          errorMessage = message;
        }
        nvDialog.alert($event, {
          title: nvTranslate.instant('container.shipper-billing.billing-error'),
          content: errorMessage,
        });
      }

      function generatePayload() {
        const isDeduct = !nvModelUtils.fromYesNoBitStringToBool(ctrl.transferMode);
        return {
          external_transaction_ref: generateUuid(),
          currency: nvCurrency.getCode($rootScope.countryId),
          amount: parseFloat(Math.round(ctrl.amount * 100) / 100),
          timestamp: nvDateTimeUtils.displayFormat(moment(), nvDateTimeUtils.FORMAT_ISO_TZ, 'UTC'),
          shipper_id: _.get(ctrl.selectedShipper, 'legacy_id'),
          country: _.toUpper($rootScope.countryId),
          comments: ctrl.review.reason,
          type: isDeduct ? 'DEBIT' : 'CREDIT',
        };
      }

      function generateUuid() {
        const uuid = new UUID(4);
        return uuid.format('std');
      }
    }

    function onCancel() {
      ctrl.isReviewAvailable = false;
    }

    function getCurrencyText() {
      const isDeduct = !nvModelUtils.fromYesNoBitStringToBool(ctrl.transferMode);
      if (isDeduct) {
        return TEXT_DEDUCTION;
      }
      return TEXT_ADD;
    }

    function resetForm() {
      ctrl.isReviewAvailable = false;

      ctrl.amount = 0;
      ctrl.bankTransferDetails = '';
      ctrl.pickupSurchargeDetails = '';

      ctrl.review = {};

      $scope.modelForm.$setPristine();
      $scope.modelForm.$setUntouched();
    }
  }
}());
