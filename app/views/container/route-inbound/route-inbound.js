(function controller() {
  angular
    .module('nvOperatorApp.controllers')
    .controller('RouteInboundController', RouteInboundController);

  RouteInboundController.$inject = [
    '$q', 'Hub', '$log', 'Transaction',
  ];

  function RouteInboundController(
    $q, Hub, $log, Transaction
  ) {
    // variables
    const RESERVATION_STATUS = {
      PENDING: 'commons.pending',
      SUCCESS: 'commons.success',
      FAIL: 'container.route-inbound.failed',
    };

    const ORDER_STATUS = {
      PENDING: 'commons.pending',
      SUCCESS: 'commons.success',
      FAIL: 'container.route-inbound.failed',
    };

    const ctrl = this;

    // shared params
    ctrl.IS_DEBUG = false;
    ctrl.RESERVATION_STATUS = RESERVATION_STATUS;
    ctrl.ORDER_STATUS = ORDER_STATUS;

    ctrl.hubsResult = null;
    ctrl.inboundSummaryResult = {};
    ctrl.inboundSummaryOrdersMap = {};
    ctrl.inboundSummaryStampOrdersMap = {};

    ctrl.backdropLoading = false;
    ctrl.selectedHub = null;

    // functions
    ctrl.getAll = getAll;

    // shared functions
    ctrl.extendInboundSummaryResult = extendInboundSummaryResult;
    ctrl.getPendingWaypoints = getPendingWaypoints;
    ctrl.getPartialWaypoints = getPartialWaypoints;
    ctrl.getFailedWaypoints = getFailedWaypoints;
    ctrl.getSuccessWaypoints = getSuccessWaypoints;
    ctrl.getPendingOrders = getPendingOrders;
    ctrl.getPartialOrders = getPartialOrders;
    ctrl.getFailedOrders = getFailedOrders;
    ctrl.getSuccessOrders = getSuccessOrders;
    ctrl.getPendingReservations = getPendingReservations;
    ctrl.getPartialReservations = getPartialReservations;
    ctrl.getFailedReservations = getFailedReservations;
    ctrl.getSuccessReservations = getSuccessReservations;
    ctrl.getRemainingCashToCollect = getRemainingCashToCollect;
    ctrl.getAllOrders = getAllOrders;
    ctrl.getAllFailedOrders = getAllFailedOrders;
    ctrl.getAllSuccessfulC2CAndReturnPickups = getAllSuccessfulC2CAndReturnPickups;
    ctrl.getReservations = getReservations;
    ctrl.getGrabReservations = getGrabReservations;
    ctrl.getInboundedParcelsCountInReservation = getInboundedParcelsCountInReservation;
    ctrl.getTotalParcelsCountInReservation = getTotalParcelsCountInReservation;
    ctrl.getViewDetailsDialogGeneralFunctions = getViewDetailsDialogGeneralFunctions;
    ctrl.getViewMoneyCollectionDialogData = getViewMoneyCollectionDialogData;
    ctrl.generateOrderLocation = generateOrderLocation;
    ctrl.generateReservationLocation = generateReservationLocation;

    // functions details
    function getAll() {
      return $q.all([
        Hub.read(),
      ]).then(success);

      function success(response) {
        ctrl.hubsResult = response[0];
      }
    }

    function extendInboundSummaryResult(inboundSummaryResult) {
      const reservationInboundScansMap = _.keyBy(_.get(inboundSummaryResult, 'pickup_scanned_reservation_inbound_scans'), inboundScan =>
        _.toUpper(inboundScan.scan)
      );

      ctrl.inboundSummaryResult = _.cloneDeep(inboundSummaryResult);
      ctrl.inboundSummaryResult.totalCodAmountReceived = 0;

      ctrl.inboundSummaryResult.comments = getComments();

      // extra orders
      _.forEach(_.get(ctrl.inboundSummaryResult, 'extra_order_details'), (order) => {
        order.isReservation = true;
        order.reservationId = null;
        order.transactionType = Transaction.TYPE.PICKUP;
        order.transactionStatus = ORDER_STATUS.SUCCESS;

        ctrl.inboundSummaryOrdersMap[_.toUpper(order.tracking_id)] = order;
        if (order.stamp_id) {
          ctrl.inboundSummaryStampOrdersMap[_.toUpper(order.stamp_id)] = order;
        }
      });

      // extend order level data
      _.forEach(getPendingWaypoints(), (wp) => {
        extendWaypointData(wp);
      });

      _.forEach(getPartialWaypoints(), (wp) => {
        extendWaypointData(wp);
      });

      _.forEach(getFailedWaypoints(), (wp) => {
        extendWaypointData(wp);
      });

      _.forEach(getSuccessWaypoints(), (wp) => {
        extendWaypointData(wp);
      });

      $log.debug(ctrl.inboundSummaryResult);
      $log.debug(ctrl.inboundSummaryOrdersMap);
      $log.debug(ctrl.inboundSummaryStampOrdersMap);

      function extendWaypointData(wp) {
        _.forEach(_.get(wp, 'success_pickup_orders'), (order) => {
          ctrl.inboundSummaryResult.totalCodAmountReceived += order.cod_amount;

          order.transactionType = Transaction.TYPE.PICKUP;
          order.transactionStatus = ORDER_STATUS.SUCCESS;

          ctrl.inboundSummaryOrdersMap[_.toUpper(order.tracking_id)] = order;
          if (order.stamp_id) {
            ctrl.inboundSummaryStampOrdersMap[_.toUpper(order.stamp_id)] = order;
          }
        });

        _.forEach(_.get(wp, 'success_delivery_orders'), (order) => {
          ctrl.inboundSummaryResult.totalCodAmountReceived += order.cod_amount;

          order.transactionType = Transaction.TYPE.DELIVERY;
          order.transactionStatus = ORDER_STATUS.SUCCESS;

          ctrl.inboundSummaryOrdersMap[_.toUpper(order.tracking_id)] = order;
          if (order.stamp_id) {
            ctrl.inboundSummaryStampOrdersMap[_.toUpper(order.stamp_id)] = order;
          }
        });

        _.forEach(_.get(wp, 'fail_pickup_orders'), (order) => {
          order.transactionType = Transaction.TYPE.PICKUP;
          order.transactionStatus = ORDER_STATUS.FAIL;
          order.isValidFailure = true;

          ctrl.inboundSummaryOrdersMap[_.toUpper(order.tracking_id)] = order;
          if (order.stamp_id) {
            ctrl.inboundSummaryStampOrdersMap[_.toUpper(order.stamp_id)] = order;
          }
        });

        _.forEach(_.get(wp, 'fail_delivery_orders'), (order) => {
          order.transactionType = Transaction.TYPE.DELIVERY;
          order.transactionStatus = ORDER_STATUS.FAIL;
          order.isValidFailure = true;

          ctrl.inboundSummaryOrdersMap[_.toUpper(order.tracking_id)] = order;
          if (order.stamp_id) {
            ctrl.inboundSummaryStampOrdersMap[_.toUpper(order.stamp_id)] = order;
          }
        });

        _.forEach(_.get(wp, 'invalid_pickup_orders'), (order) => {
          order.transactionType = Transaction.TYPE.PICKUP;
          order.transactionStatus = ORDER_STATUS.FAIL;
          order.isValidFailure = false;

          ctrl.inboundSummaryOrdersMap[_.toUpper(order.tracking_id)] = order;
          if (order.stamp_id) {
            ctrl.inboundSummaryStampOrdersMap[_.toUpper(order.stamp_id)] = order;
          }
        });

        _.forEach(_.get(wp, 'invalid_delivery_orders'), (order) => {
          order.transactionType = Transaction.TYPE.DELIVERY;
          order.transactionStatus = ORDER_STATUS.FAIL;
          order.isValidFailure = false;

          ctrl.inboundSummaryOrdersMap[_.toUpper(order.tracking_id)] = order;
          if (order.stamp_id) {
            ctrl.inboundSummaryStampOrdersMap[_.toUpper(order.stamp_id)] = order;
          }
        });

        _.forEach(_.get(wp, 'pending_pickup_orders'), (order) => {
          order.transactionType = Transaction.TYPE.PICKUP;
          order.transactionStatus = ORDER_STATUS.PENDING;

          ctrl.inboundSummaryOrdersMap[_.toUpper(order.tracking_id)] = order;
          if (order.stamp_id) {
            ctrl.inboundSummaryStampOrdersMap[_.toUpper(order.stamp_id)] = order;
          }
        });

        _.forEach(_.get(wp, 'pending_delivery_orders'), (order) => {
          order.transactionType = Transaction.TYPE.DELIVERY;
          order.transactionStatus = ORDER_STATUS.PENDING;

          ctrl.inboundSummaryOrdersMap[_.toUpper(order.tracking_id)] = order;
          if (order.stamp_id) {
            ctrl.inboundSummaryStampOrdersMap[_.toUpper(order.stamp_id)] = order;
          }
        });

        if (_.get(wp, 'success_reservation')) {
          const successReservation = _.get(wp, 'success_reservation');
          successReservation.orders = [];
          successReservation.totalCollectedAmount = _.get(successReservation, 'meta_data.pricing.total_collected_amount', 0);

          ctrl.inboundSummaryResult.totalCodAmountReceived +=
            successReservation.totalCollectedAmount;

          _.forEach(_.get(successReservation, 'scanned_parcels'), (trackingId) => {
            const order = {
              id: null,
              name: null,
              address1: successReservation.address1,
              address2: successReservation.address2,
              district: null,
              city: successReservation.city,
              state: null,
              postcode: successReservation.postcode,
              country: successReservation.country,
              contact: successReservation.contact,
              tracking_id: trackingId,
              stamp_id: null,
              type: null,
              is_rts: false,
              shipper_id: successReservation.shipper_id,
              shipper_name: successReservation.shipper_name,
              cod_amount: 0,
              cod_collected: 0,
              failure_reason: null,
              scan_details: null,
              cmi_count: 0,

              isGrabReservation: hasGrabMetaData(successReservation),
              isReservation: !hasGrabMetaData(successReservation),
              reservationId: successReservation.id,
              transactionType: Transaction.TYPE.PICKUP,
              transactionStatus: ORDER_STATUS.SUCCESS,
            };

            if (reservationInboundScansMap[_.toUpper(trackingId)]) {
              order.scan_details = reservationInboundScansMap[_.toUpper(trackingId)];
            } else if (ctrl.inboundSummaryOrdersMap[_.toUpper(trackingId)]) {
              order.tracking_id = _.get(ctrl.inboundSummaryOrdersMap[_.toUpper(trackingId)], 'tracking_id');
              order.stamp_id = _.get(ctrl.inboundSummaryOrdersMap[_.toUpper(trackingId)], 'stamp_id');
              order.scan_details = _.get(ctrl.inboundSummaryOrdersMap[_.toUpper(trackingId)], 'scan_details');
            } else if (ctrl.inboundSummaryStampOrdersMap[_.toUpper(trackingId)]) {
              order.tracking_id = _.get(ctrl.inboundSummaryStampOrdersMap[_.toUpper(trackingId)], 'tracking_id');
              order.stamp_id = _.get(ctrl.inboundSummaryStampOrdersMap[_.toUpper(trackingId)], 'stamp_id');
              order.scan_details = _.get(ctrl.inboundSummaryStampOrdersMap[_.toUpper(trackingId)], 'scan_details');
            }

            successReservation.orders.push(order);

            ctrl.inboundSummaryOrdersMap[_.toUpper(order.tracking_id)] = order;
          });
        }

        if (_.get(wp, 'fail_reservation')) {
          const failReservation = _.get(wp, 'fail_reservation');
          failReservation.isValidFailure = true;
        }

        if (_.get(wp, 'invalid_reservation')) {
          const invalidReservation = _.get(wp, 'invalid_reservation');
          invalidReservation.isValidFailure = false;
        }
      }

      function getComments() {
        return _.compact(_.split(ctrl.inboundSummaryResult.route_comments, '###'));
      }
    }

    function getPendingWaypoints() {
      return _.get(ctrl.inboundSummaryResult, 'waypoints.pending') || [];
    }

    function getPartialWaypoints() {
      return _.get(ctrl.inboundSummaryResult, 'waypoints.partial') || [];
    }

    function getFailedWaypoints() {
      return _.get(ctrl.inboundSummaryResult, 'waypoints.failed') || [];
    }

    function getSuccessWaypoints() {
      return _.get(ctrl.inboundSummaryResult, 'waypoints.success') || [];
    }

    function getPendingOrders() {
      let orders = [];

      _.forEach(getPendingWaypoints(), (wp) => {
        orders = _.concat(
          orders,
          _.get(wp, 'pending_pickup_orders') || [],
          _.get(wp, 'pending_delivery_orders') || []
        );
      });

      return orders;
    }

    function getPartialOrders() {
      let orders = [];

      _.forEach(getPartialWaypoints(), (wp) => {
        orders = _.concat(
          orders,
          _.get(wp, 'fail_pickup_orders') || [],
          _.get(wp, 'fail_delivery_orders') || [],
          _.get(wp, 'invalid_pickup_orders') || [],
          _.get(wp, 'invalid_delivery_orders') || [],
          _.get(wp, 'success_pickup_orders') || [],
          _.get(wp, 'success_delivery_orders') || []
        );
      });

      return orders;
    }

    function getFailedOrders() {
      let orders = [];

      _.forEach(getFailedWaypoints(), (wp) => {
        orders = _.concat(
          orders,
          _.get(wp, 'fail_pickup_orders') || [],
          _.get(wp, 'fail_delivery_orders') || [],
          _.get(wp, 'invalid_pickup_orders') || [],
          _.get(wp, 'invalid_delivery_orders') || []
        );
      });

      return orders;
    }

    function getSuccessOrders() {
      let orders = [];

      _.forEach(getSuccessWaypoints(), (wp) => {
        orders = _.concat(
          orders,
          _.get(wp, 'success_pickup_orders') || [],
          _.get(wp, 'success_delivery_orders') || []
        );
      });

      return orders;
    }
    function getPendingReservations() {
      const reservations = [];

      _.forEach(getPendingWaypoints(), (wp) => {
        if (_.get(wp, 'pending_reservation')) {
          reservations.push(_.get(wp, 'pending_reservation'));
        }
      });

      return reservations;
    }

    function getPartialReservations() {
      const reservations = [];

      _.forEach(getPartialWaypoints(), (wp) => {
        if (_.get(wp, 'success_reservation')) {
          reservations.push(_.get(wp, 'success_reservation'));
        }

        if (_.get(wp, 'fail_reservation')) {
          reservations.push(_.get(wp, 'fail_reservation'));
        }

        if (_.get(wp, 'invalid_reservation')) {
          reservations.push(_.get(wp, 'invalid_reservation'));
        }
      });

      return reservations;
    }

    function getFailedReservations() {
      const reservations = [];

      _.forEach(getFailedWaypoints(), (wp) => {
        if (_.get(wp, 'fail_reservation')) {
          reservations.push(_.get(wp, 'fail_reservation'));
        }

        if (_.get(wp, 'invalid_reservation')) {
          reservations.push(_.get(wp, 'invalid_reservation'));
        }
      });

      return reservations;
    }

    function getSuccessReservations() {
      const reservations = [];

      _.forEach(getSuccessWaypoints(), (wp) => {
        if (_.get(wp, 'success_reservation')) {
          reservations.push(_.get(wp, 'success_reservation'));
        }
      });

      return reservations;
    }

    function getRemainingCashToCollect() {
      return _.round(
        _.get(ctrl.inboundSummaryResult, 'totalCodAmountReceived') -
        _.get(ctrl.inboundSummaryResult, 'inbounded_amount'),
        2
      );
    }

    function getAllOrders() {
      return _.concat(
        getPendingOrders(),
        getPartialOrders(),
        getFailedOrders(),
        getSuccessOrders()
      );
    }

    function getAllFailedOrders() {
      let orders = [];

      _.forEach(getPendingWaypoints(), (wp) => {
        orders = _.concat(
          orders,
          _.get(wp, 'fail_pickup_orders') || [],
          _.get(wp, 'fail_delivery_orders') || [],
          _.get(wp, 'invalid_pickup_orders') || [],
          _.get(wp, 'invalid_delivery_orders') || []
        );
      });

      _.forEach(getPartialWaypoints(), (wp) => {
        orders = _.concat(
          orders,
          _.get(wp, 'fail_pickup_orders') || [],
          _.get(wp, 'fail_delivery_orders') || [],
          _.get(wp, 'invalid_pickup_orders') || [],
          _.get(wp, 'invalid_delivery_orders') || []
        );
      });

      _.forEach(getFailedWaypoints(), (wp) => {
        orders = _.concat(
          orders,
          _.get(wp, 'fail_pickup_orders') || [],
          _.get(wp, 'fail_delivery_orders') || [],
          _.get(wp, 'invalid_pickup_orders') || [],
          _.get(wp, 'invalid_delivery_orders') || []
        );
      });

      return orders;
    }

    function getAllSuccessfulC2CAndReturnPickups() {
      let orders = [];

      _.forEach(getPendingWaypoints(), (wp) => {
        orders = _.concat(
          orders,
          _.get(wp, 'success_pickup_orders') || []
        );
      });

      _.forEach(getPartialWaypoints(), (wp) => {
        orders = _.concat(
          orders,
          _.get(wp, 'success_pickup_orders') || []
        );
      });

      _.forEach(getSuccessWaypoints(), (wp) => {
        orders = _.concat(
          orders,
          _.get(wp, 'success_pickup_orders') || []
        );
      });

      return orders;
    }

    function getReservations(isGrabReservation = false) {
      let reservations = [];

      _.forEach(getPendingWaypoints(), (wp) => {
        reservations = _.concat(
          reservations,
          _.get(wp, 'success_reservation') || [],
          _.get(wp, 'fail_reservation') || [],
          _.get(wp, 'invalid_reservation') || [],
          _.get(wp, 'pending_reservation') || []
        );
      });

      _.forEach(getPartialWaypoints(), (wp) => {
        reservations = _.concat(
          reservations,
          _.get(wp, 'success_reservation') || [],
          _.get(wp, 'fail_reservation') || [],
          _.get(wp, 'invalid_reservation') || [],
          _.get(wp, 'pending_reservation') || []
        );
      });

      _.forEach(getFailedWaypoints(), (wp) => {
        reservations = _.concat(
          reservations,
          _.get(wp, 'success_reservation') || [],
          _.get(wp, 'fail_reservation') || [],
          _.get(wp, 'invalid_reservation') || [],
          _.get(wp, 'pending_reservation') || []
        );
      });

      _.forEach(getSuccessWaypoints(), (wp) => {
        reservations = _.concat(
          reservations,
          _.get(wp, 'success_reservation') || [],
          _.get(wp, 'fail_reservation') || [],
          _.get(wp, 'invalid_reservation') || [],
          _.get(wp, 'pending_reservation') || []
        );
      });

      if (isGrabReservation) {
        return _.filter(reservations, reservation =>
          hasGrabMetaData(reservation)
        );
      }

      return _.filter(reservations, reservation =>
        !hasGrabMetaData(reservation)
      );
    }

    function getGrabReservations() {
      return getReservations(true);
    }

    function hasGrabMetaData(reservation) {
      return !_.isUndefined(_.get(reservation, 'meta_data.grab'));
    }

    function getInboundedParcelsCountInReservation(reservation) {
      return _.size(_.filter(reservation.orders, order =>
        _.size(order.scan_details) > 0
      ));
    }

    function getTotalParcelsCountInReservation(reservation) {
      return _.max([
        _.get(reservation, 'received_parcels'),
        _.size(_.get(reservation, 'scanned_parcels')),
      ]);
    }

    function getViewDetailsDialogGeneralFunctions() {
      return {
        RESERVATION_STATUS: RESERVATION_STATUS,
        getInboundedParcelsCountInReservation: getInboundedParcelsCountInReservation,
        getTotalParcelsCountInReservation: getTotalParcelsCountInReservation,
        generateOrderLocation: generateOrderLocation,
        generateReservationLocation: generateReservationLocation,
        hasGrabMetaData: hasGrabMetaData,
      };
    }

    function getViewMoneyCollectionDialogData() {
      const collectedOrders = _.filter(_.concat(
          getSuccessOrders(),
          getPartialOrders()
        ), order =>
        order.cod_amount > 0 &&
        _.includes([
          ORDER_STATUS.SUCCESS,
        ], order.transactionStatus)
      );

      const uncollectedOrders = _.filter(_.concat(
          getPendingOrders(),
          getPartialOrders(),
          getFailedOrders()
        ), order =>
        order.cod_amount > 0 &&
        _.includes([
          ORDER_STATUS.PENDING,
          ORDER_STATUS.FAIL,
        ], order.transactionStatus)
      );

      const collectedReservationFees = _.filter(_.concat(
        getSuccessReservations(),
        getPartialReservations()
      ), reservation =>
        reservation.totalCollectedAmount > 0 &&
        RESERVATION_STATUS[reservation.status] === RESERVATION_STATUS.SUCCESS
      );

      return {
        enableInputTab: false,
        routeId: ctrl.inboundSummaryResult.route_id,
        totalCodAmountReceived: _.get(ctrl.inboundSummaryResult, 'totalCodAmountReceived'),
        remainingCashToCollect: getRemainingCashToCollect(),
        cashInboundHistory: _.get(ctrl.inboundSummaryResult, 'inbound_history') || [],
        collectedOrders: collectedOrders,
        uncollectedOrders: uncollectedOrders,
        collectedReservationFees: collectedReservationFees,
        ORDER_STATUS: ORDER_STATUS,
        generateOrderLocation: generateOrderLocation,
      };
    }

    function generateOrderLocation(order) {
      return [
        order.address1,
        order.address2,
        order.district,
        order.city,
        order.state,
        order.postcode,
        order.country,
      ].join(' ');
    }

    function generateReservationLocation(reservation) {
      return [
        reservation.address1,
        reservation.address2,
        reservation.city,
        reservation.postcode,
        reservation.country,
      ].join(' ');
    }
  }
}());
