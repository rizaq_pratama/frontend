(function controller() {
  angular
    .module('nvOperatorApp.controllers')
    .controller('RouteInboundResultController', RouteInboundResultController);

  RouteInboundResultController.$inject = [
    '$scope', '$state', 'nvNavGuard', '$timeout', 'nvTable',
    'nvDialog', 'nvTranslate', 'Reservation', 'Order', 'Transaction',
  ];

  function RouteInboundResultController(
    $scope, $state, nvNavGuard, $timeout, nvTable,
    nvDialog, nvTranslate, Reservation, Order, Transaction
  ) {
    // variables
    const DETAILS_TYPE = {
      WAYPOINT_PENDING: 'commons.pending',
      WAYPOINT_PARTIAL: 'container.route-inbound.partial',
      WAYPOINT_FAILED: 'container.route-inbound.failed',
      WAYPOINT_SUCCESS: 'commons.completed',
      WAYPOINT_TOTAL: 'commons.total',
      COLLECT_FAILED: 'container.route-inbound.failed-parcels',
      COLLECT_C2C_AND_RETURN: 'container.route-inbound.c2c-and-return',
      RESERVATIONS: 'container.route-inbound.reservations',
    };

    const ctrl = this;
    const parentCtrl = $scope.$parent.ctrl;

    ctrl.parentCtrl = parentCtrl;
    ctrl.DETAILS_TYPE = DETAILS_TYPE;

    ctrl.waypointPerformanceCount = {};
    ctrl.collectionSummaryCount = {};
    ctrl.problematicWaypointsTableParams = null;
    ctrl.problematicParcelsTableParams = null;

    // functions
    ctrl.processInboundSummaryData = processInboundSummaryData; // use in unit test
    ctrl.viewDetails = viewDetails;
    ctrl.viewMoneyCollection = viewMoneyCollection;
    ctrl.goToFilterPage = goToFilterPage;
    ctrl.goToInboundPage = goToInboundPage;

    // start
    initialize();

    // functions details
    function initialize() {
      if (!parentCtrl.IS_DEBUG) {
        nvNavGuard.lock();

        // validation
        if (_.size(parentCtrl.inboundSummaryResult) <= 0) {
          goToFilterPage();
        }

        processInboundSummaryData();
      } else if (_.size(parentCtrl.inboundSummaryResult) <= 0) {
        // load debug data
        parentCtrl.selectedHub = {
          id: 1,
          name: 'DEBUG HUB',
        };

        $.getJSON('views/container/route-inbound/json/inbound-summary-result.json', (inboundSummaryResult) => {
          $timeout(() => {
            loadDebugDataSuccess(inboundSummaryResult);
          });
        });
      } else {
        processInboundSummaryData();
      }

      function loadDebugDataSuccess(inboundSummaryResult) {
        parentCtrl.extendInboundSummaryResult(inboundSummaryResult);
        processInboundSummaryData();
      }
    }

    function processInboundSummaryData() {
      ctrl.waypointPerformanceCount = {
        pending: _.size(parentCtrl.getPendingWaypoints()),
        partial: _.size(parentCtrl.getPartialWaypoints()),
        failed: _.size(parentCtrl.getFailedWaypoints()),
        completed: _.size(parentCtrl.getSuccessWaypoints()),
      };

      ctrl.collectionSummaryCount = {
        cash: parentCtrl.getRemainingCashToCollect(),
        failed: _.size(getRemainingFailedOrdersToCollect()),
        successfulC2CAndReturnPickup: _.size(getRemainingSuccessfulPickupToCollect()),
        reservation: getTotalRemainingParcelsToCollectInReservations(),
      };

      // problematic waypoints table
      const problematicWaypointsData = generateProblematicWaypointsData();
      if (_.size(problematicWaypointsData) > 0) {
        ctrl.problematicWaypointsTableParams = nvTable.createTable({
          shipper_name: { displayName: 'commons.model.shipper-name' },
          location: { displayName: 'commons.location' },
          type: { displayName: 'commons.type' },
          issue: { displayName: 'container.route-inbound.issue' },
        });
        ctrl.problematicWaypointsTableParams.setData(problematicWaypointsData);
      }

      // problematic parcels table
      const problematicParcelsData = generateProblematicParcelsData();
      if (_.size(problematicParcelsData) > 0) {
        ctrl.problematicParcelsTableParams = nvTable.createTable({
          tracking_id: { displayName: 'commons.tracking-id' },
          shipper_name: { displayName: 'commons.model.shipper-name' },
          location: { displayName: 'commons.location' },
          customType: { displayName: 'commons.type' },
          issue: { displayName: 'container.route-inbound.issue' },
        });
        ctrl.problematicParcelsTableParams.setData(problematicParcelsData);
      }

      function getRemainingFailedOrdersToCollect() {
        return _.filter(parentCtrl.getAllFailedOrders(), order =>
          _.size(_.get(order, 'scan_details')) <= 0 &&
            order.transactionType === Transaction.TYPE.DELIVERY
        );
      }

      function getRemainingSuccessfulPickupToCollect() {
        return _.filter(parentCtrl.getAllSuccessfulC2CAndReturnPickups(), order =>
          _.size(_.get(order, 'scan_details')) <= 0 &&
            order.transactionType === Transaction.TYPE.PICKUP
        );
      }

      function getTotalRemainingParcelsToCollectInReservations() {
        const successReservations = _.filter(parentCtrl.getReservations(), reservation =>
          Reservation.STATUS[reservation.status] === Reservation.STATUS.SUCCESS
        );

        let total = 0;
        let inbounded = 0;
        _.forEach(successReservations, (reservation) => {
          total += parentCtrl.getTotalParcelsCountInReservation(reservation);
          inbounded += parentCtrl.getInboundedParcelsCountInReservation(reservation);
        });

        const extraOrders = _.get(parentCtrl.inboundSummaryResult, 'extra_order_details') || [];
        const remaining = total - inbounded - _.size(extraOrders);

        return remaining > 0 ? remaining : 0;
      }

      function generateProblematicWaypointsData() {
        let reservations = parentCtrl.getPendingReservations();

        reservations = _.concat(reservations, _.filter(
          parentCtrl.getPartialReservations(), reservation =>
            reservation.isValidFailure === false
        ));

        reservations = _.concat(reservations, _.filter(
          parentCtrl.getFailedReservations(), reservation =>
            reservation.isValidFailure === false
        ));

        return _.orderBy(
          extend(reservations),
          ['isValidFailure'],
          ['asc']
        );

        function extend(theReservations) {
          return _.map(theReservations, reservation => (
            setCustomReservationData(reservation)
          ));

          function setCustomReservationData(reservation) {
            reservation.location = parentCtrl.generateReservationLocation(reservation);
            reservation.type = nvTranslate.instant('container.route-inbound.pickup-reservation');

            reservation.issue = reservation.failure_reason;
            if (Reservation.STATUS[reservation.status] === Reservation.STATUS.PENDING) {
              reservation.issue = nvTranslate.instant('commons.pending');
            }

            return reservation;
          }
        }
      }

      function generateProblematicParcelsData() {
        let orders = parentCtrl.getPendingOrders();

        orders = _.concat(orders, _.filter(
          parentCtrl.getPartialOrders(), order =>
            order.isValidFailure === false
        ));

        orders = _.concat(orders, _.filter(
          parentCtrl.getFailedOrders(), order =>
            order.isValidFailure === false
        ));

        return _.orderBy(
          extend(orders),
          ['isValidFailure', 'transactionType'],
          ['asc', 'desc']
        );

        function extend(theOrders) {
          return _.map(theOrders, order => (
            setCustomOrderData(order)
          ));

          function setCustomOrderData(order) {
            order.location = parentCtrl.generateOrderLocation(order);

            if (order.transactionType === Transaction.TYPE.DELIVERY) {
              order.customType = nvTranslate.instant('container.route-inbound.delivery-xx', {
                type: getOrderCustomType(order),
              });
            } else { // pickup
              order.customType = nvTranslate.instant('container.route-inbound.pickup-xx', {
                type: getOrderCustomType(order),
              });
            }

            order.issue = order.failure_reason;
            if (order.transactionStatus === parentCtrl.ORDER_STATUS.PENDING) {
              order.issue = nvTranslate.instant('commons.pending');
            }

            return order;

            function getOrderCustomType(theOrder) {
              if (theOrder.isReservation) {
                return nvTranslate.instant('container.route-inbound.reservation');
              } else if (theOrder.isGrabReservation) {
                return nvTranslate.instant('container.route-inbound.grab-pickup');
              }

              return theOrder.is_rts ? nvTranslate.instant('commons.rts') : Order.TYPE[theOrder.type];
            }
          }
        }
      }
    }

    function viewDetails($event, detailsType) {
      let orders = [];
      let reservations = [];
      switch (detailsType) {
        case DETAILS_TYPE.WAYPOINT_PENDING:
          orders = parentCtrl.getPendingOrders();
          reservations = parentCtrl.getPendingReservations();
          break;
        case DETAILS_TYPE.WAYPOINT_PARTIAL:
          orders = parentCtrl.getPartialOrders();
          reservations = parentCtrl.getPartialReservations();
          break;
        case DETAILS_TYPE.WAYPOINT_FAILED:
          orders = parentCtrl.getFailedOrders();
          reservations = parentCtrl.getFailedReservations();
          break;
        case DETAILS_TYPE.WAYPOINT_SUCCESS:
          orders = parentCtrl.getSuccessOrders();
          reservations = parentCtrl.getSuccessReservations();
          break;
        case DETAILS_TYPE.WAYPOINT_TOTAL:
          orders = _.concat(
            parentCtrl.getPendingOrders(),
            parentCtrl.getPartialOrders(),
            parentCtrl.getFailedOrders(),
            parentCtrl.getSuccessOrders()
          );
          reservations = _.concat(
            parentCtrl.getPendingReservations(),
            parentCtrl.getPartialReservations(),
            parentCtrl.getFailedReservations(),
            parentCtrl.getSuccessReservations()
          );
          break;
        case DETAILS_TYPE.COLLECT_FAILED:
          orders = _.filter(parentCtrl.getAllFailedOrders(), order =>
            order.transactionType === Transaction.TYPE.DELIVERY
          );
          break;
        case DETAILS_TYPE.COLLECT_C2C_AND_RETURN:
          orders = parentCtrl.getAllSuccessfulC2CAndReturnPickups();
          break;
        case DETAILS_TYPE.RESERVATIONS:
          reservations = parentCtrl.getReservations();
          break;
        default:
      }

      nvDialog.showSingle($event, {
        templateUrl: 'views/container/route-inbound/dialog/view-details/view-details.dialog.html',
        cssClass: 'route-inbound-view-details',
        controller: 'RouteInboundViewDetailsDialogController',
        controllerAs: 'ctrl',
        locals: {
          localData: _.defaults({
            title: detailsType,
            orders: orders,
            reservations: reservations,
          }, parentCtrl.getViewDetailsDialogGeneralFunctions()),
        },
      });
    }

    function viewMoneyCollection($event) {
      nvDialog.showSingle($event, {
        templateUrl: 'views/container/route-inbound/dialog/money-collection/money-collection.dialog.html',
        cssClass: 'route-inbound-money-collection',
        controller: 'RouteInboundMoneyCollectionDialogController',
        controllerAs: 'ctrl',
        locals: {
          localData: parentCtrl.getViewMoneyCollectionDialogData(),
        },
      });
    }

    function goToFilterPage() {
      nvNavGuard.unlock();
      $state.go('container.route-inbound.filter');
    }

    function goToInboundPage() {
      nvNavGuard.unlock();
      $state.go('container.route-inbound.inbounding');
    }
  }
}());
