(function controller() {
  angular
    .module('nvOperatorApp.controllers')
    .controller('RouteInboundEndIncompleteRouteDialogController', RouteInboundEndIncompleteRouteDialogController);

  RouteInboundEndIncompleteRouteDialogController.$inject = [
    '$mdDialog', 'localData', 'nvToast', 'nvTranslate', 'Route',
  ];

  function RouteInboundEndIncompleteRouteDialogController(
    $mdDialog, localData, nvToast, nvTranslate, Route
  ) {
    // variables
    const REASON = {
      DRIVER_NEEDS_MORE_TIME: 'container.route-inbound.reason.driver-needs-more-time',
      INCOMPLETE_MONEY_COLLECTION: 'container.route-inbound.reason.incomplete-money-collection',
      WAREHOUSE_ISSUES: 'container.route-inbound.reason.warehouse-issues',
      SAVED_FOR_LATER_PROCESSING: 'container.route-inbound.reason.saved-for-later-processing',
      OTHER_REASON: 'container.route-inbound.reason.other-reason',
    };

    const ctrl = this;
    const routeId = localData.routeId;

    ctrl.REASON = REASON;
    ctrl.reasonOptions = [];

    ctrl.formReasonValue = null;
    ctrl.formOtherComment = '';
    ctrl.formSubmitState = 'idle';

    // functions
    ctrl.isAbleToSubmit = isAbleToSubmit;
    ctrl.onSubmit = onSubmit;
    ctrl.onCancel = onCancel;

    // start
    initialize();

    // functions details
    function initialize() {
      _.forEach(REASON, (value, key) => {
        ctrl.reasonOptions.push({
          displayName: value,
          value: key,
        });
      });
    }

    function isAbleToSubmit() {
      if (!ctrl.formReasonValue) {
        return false;
      }

      if (ctrl.formReasonValue === REASON.OTHER_REASON && !ctrl.formOtherComment) {
        return false;
      }

      return true;
    }

    function onSubmit() {
      const payload = {
        comment: REASON[ctrl.formReasonValue] !== REASON.OTHER_REASON ?
          nvTranslate.instant(REASON[ctrl.formReasonValue]) : ctrl.formOtherComment,
      };

      ctrl.formSubmitState = 'waiting';
      Route.routeInboundEndSession(routeId, payload).then(success).finally(finallyFn);

      function success() {
        nvToast.info(
          nvTranslate.instant('container.route-inbound.inbound-updated-for-route-x', {
            routeId: routeId,
          }),
          nvTranslate.instant('container.route-inbound.route-is-still-modifiable')
        );

        $mdDialog.hide();
      }

      function finallyFn() {
        ctrl.formSubmitState = 'idle';
      }
    }

    function onCancel() {
      $mdDialog.cancel();
    }
  }
}());
