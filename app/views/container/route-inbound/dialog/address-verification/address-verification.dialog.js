(function controller() {
  angular
    .module('nvOperatorApp.controllers')
    .controller('RouteInboundAddressVerificationDialogController', RouteInboundAddressVerificationDialogController);

  RouteInboundAddressVerificationDialogController.$inject = [
    '$mdDialog', 'Order', 'nvToast', 'nvTranslate', 'Transaction',
    '$timeout',
  ];

  function RouteInboundAddressVerificationDialogController(
    $mdDialog, Order, nvToast, nvTranslate, Transaction,
    $timeout
  ) {
    // variables
    const ctrl = this;

    ctrl.formSubmitState = 'idle';
    ctrl.formTrackingId = '';
    ctrl.previousTrackingId = '';

    // functions
    ctrl.trackingIdOnKeypress = trackingIdOnKeypress;
    ctrl.isAbleToSubmit = isAbleToSubmit;
    ctrl.onSubmit = onSubmit;
    ctrl.onCancel = onCancel;

    // start
    initialize();

    // functions details
    function initialize() {
      focusTrackingInput();
    }

    function trackingIdOnKeypress(event) {
      if (!ctrl.formTrackingId) {
        return;
      }

      if (event.which === 13) {
        onSubmit();
      }
    }

    function isAbleToSubmit() {
      return !!ctrl.formTrackingId;
    }

    function onSubmit() {
      ctrl.formSubmitState = 'waiting';

      Order.reverify([{
        trackingId: ctrl.formTrackingId,
        transactionType: Transaction.getTypeEnum(Transaction.TYPE.DELIVERY),
      }]).then(success).finally(finallyFn);

      function success() {
        ctrl.previousTrackingId = ctrl.formTrackingId;

        ctrl.formTrackingId = '';
        nvToast.info(nvTranslate.instant('container.order.edit.reverify-success'));

        focusTrackingInput();
      }

      function finallyFn() {
        ctrl.formSubmitState = 'idle';
      }
    }

    function onCancel() {
      $mdDialog.cancel();
    }

    function focusTrackingInput() {
      $timeout(() => { $('#reverify-tracking-id').focus(); });
    }
  }
}());
