(function controller() {
  angular
    .module('nvOperatorApp.controllers')
    .controller('RouteInboundViewDetailsDialogController', RouteInboundViewDetailsDialogController);

  RouteInboundViewDetailsDialogController.$inject = [
    '$mdDialog', 'localData', 'nvTable', 'nvTranslate', 'nvToast',
    'Printer', '$window', '$state', 'Order', 'Transaction',
    'nvDateTimeUtils', 'nvDialog',
  ];

  function RouteInboundViewDetailsDialogController(
    $mdDialog, localData, nvTable, nvTranslate, nvToast,
    Printer, $window, $state, Order, Transaction,
    nvDateTimeUtils, nvDialog
  ) {
    // variables
    let bulkActionProgressPayload;

    const ctrl = this;
    const RESERVATION_STATUS = localData.RESERVATION_STATUS;
    const title = localData.title;
    let reservations = _.cloneDeep(localData.reservations) || [];
    let orders = _.cloneDeep(localData.orders) || [];

    let reservationsByShipper = {};
    let ordersByShipper = {};
    const byShippers = [];

    ctrl.title = title;
    ctrl.chosenShipper = null;
    ctrl.byShippersTableParams = null;
    ctrl.shipperReservationsTableParams = null;
    ctrl.shipperOrdersTableParams = null;
    ctrl.isPrintLabelsLoading = false;

    // functions
    ctrl.printShippingLabels = printShippingLabels;
    ctrl.printShippingLabel = printShippingLabel;
    ctrl.onCancel = onCancel;
    ctrl.viewOrdersOrReservations = viewOrdersOrReservations;
    ctrl.viewShippers = viewShippers;
    ctrl.editOrder = editOrder;

    // start
    initialize();

    // functions details
    function initialize() {
      orders = extendOrders(orders);
      reservations = extendReservations(reservations);

      ordersByShipper = _.groupBy(orders, 'shipper_id');
      reservationsByShipper = _.groupBy(reservations, 'shipper_id');

      _.forEach(ordersByShipper, (theOrders, shipperId) => {
        byShippers.push({
          id: +shipperId,
          name: _.get(theOrders, '[0].shipper_name'),
          scanned: _.size(getScannedOrders(theOrders)),
          total: _.size(theOrders),
        });
      });

      _.forEach(reservationsByShipper, (theReservations, shipperId) => {
        const byShipper = _.find(byShippers, ['id', +shipperId]);
        if (!byShipper) {
          byShippers.push({
            id: +shipperId,
            name: _.get(theReservations, '[0].shipper_name'),
            scanned: getInboundedParcelsCountInReservations(theReservations),
            total: getTotalParcelsCountInReservations(theReservations),
          });
        } else {
          byShipper.scanned += getInboundedParcelsCountInReservations(theReservations);
          byShipper.total += getTotalParcelsCountInReservations(theReservations);
        }
      });

      createByShippersTable();

      function extendOrders(theOrders) {
        return _.map(theOrders, order => (
          setCustomOrderData(order)
        ));

        function setCustomOrderData(order) {
          order.isPrinting = false;
          order.translatedStatus = nvTranslate.instant(order.transactionStatus);
          order.location = localData.generateOrderLocation(order);

          if (order.transactionType === Transaction.TYPE.DELIVERY) {
            order.customType = nvTranslate.instant('container.route-inbound.delivery-xx', {
              type: getOrderCustomType(order),
            });
          } else { // pickup
            order.customType = nvTranslate.instant('container.route-inbound.pickup-xx', {
              type: getOrderCustomType(order),
            });
          }

          order.scanStatus = _.size(_.get(order, 'scan_details')) > 0 ? nvTranslate.instant(
            'container.route-inbound.inbounded'
          ) : null;
          return order;

          function getOrderCustomType(theOrder) {
            if (theOrder.isReservation) {
              return nvTranslate.instant('container.route-inbound.reservation');
            } else if (theOrder.isGrabReservation) {
              return nvTranslate.instant('container.route-inbound.grab-pickup');
            }

            return theOrder.is_rts ? nvTranslate.instant('commons.rts') : Order.TYPE[theOrder.type];
          }
        }
      }

      function extendReservations(theReservations) {
        return _.map(theReservations, reservation => (
          setCustomReservationData(reservation)
        ));

        function setCustomReservationData(reservation) {
          reservation.translatedStatus = RESERVATION_STATUS[reservation.status] ?
            nvTranslate.instant(RESERVATION_STATUS[reservation.status]) : null;
          reservation.location = localData.generateReservationLocation(reservation);
          reservation.totalPickupCount = localData.getTotalParcelsCountInReservation(reservation);

          reservation.readyToLatestTime = null;
          if (_.get(reservation, 'ready_datetime') && _.get(reservation, 'latest_datetime')) {
            reservation.readyToLatestTime = `${nvDateTimeUtils.displayDateTime(
              moment.utc(_.get(reservation, 'ready_datetime'))
            )} - ${nvDateTimeUtils.displayDateTime(
              moment.utc(_.get(reservation, 'latest_datetime'))
            )}`;
          }

          // set custom reservation orders data
          _.forEach(reservation.orders, (order) => {
            order.isPrinting = false;
            order.translatedStatus = nvTranslate.instant(RESERVATION_STATUS.SUCCESS);
            order.location = localData.generateOrderLocation(order);

            order.customType = nvTranslate.instant('container.route-inbound.pickup-xx', {
              type: nvTranslate.instant(
                localData.hasGrabMetaData(reservation) ?
                  'container.route-inbound.grab-pickup' : 'container.route-inbound.reservation'
              ),
            });

            order.scanStatus = _.size(_.get(order, 'scan_details')) > 0 ? nvTranslate.instant(
              'container.route-inbound.inbounded'
            ) : null;
          });

          return reservation;
        }
      }

      function getScannedOrders(theOrders) {
        return _.filter(theOrders, order =>
          _.size(_.get(order, 'scan_details')) > 0
        );
      }

      function getInboundedParcelsCountInReservations(theReservations) {
        let count = 0;
        _.forEach(theReservations, (reservation) => {
          count += localData.getInboundedParcelsCountInReservation(reservation);
        });

        return count;
      }

      function getTotalParcelsCountInReservations(theReservations) {
        let count = 0;
        _.forEach(theReservations, (reservation) => {
          count += reservation.totalPickupCount;
        });

        return count;
      }
    }

    function printShippingLabels($event) {
      ctrl.isPrintLabelsLoading = true;

      const ordersToProcess = _.cloneDeep(ctrl.shipperOrdersTableParams.getSelection());
      bulkActionProgressPayload = {
        totalCount: _.size(ordersToProcess),
        currentIndex: 0,
        errors: [],
        messages: {
          updating: 'container.route-inbound.printing-x-of-y-items',
          updated: 'container.route-inbound.printed-x-of-y-items',
          error: 'container.route-inbound.failed-to-print-x-items',
        },
      };

      nvDialog.showSingle($event, {
        templateUrl: 'lib/ninja-commons/services/dialogs/bulk-action-progress/bulk-action-progress.dialog.html',
        cssClass: 'bulk-action-progress',
        controller: 'BulkActionProgressDialogController',
        controllerAs: 'ctrl',
        skipHide: true,
        clickOutsideToClose: false,
        locals: {
          payload: bulkActionProgressPayload,
        },
      }).then(progressDialogClosed);

      startMultiSelectPrinting(ordersToProcess);

      function progressDialogClosed() {
        if (getSuccessCount() > 0) {
          nvToast.info(
            nvTranslate.instant('container.route-inbound.num-labels-printed', { num: getSuccessCount() })
          );
        }
      }

      function getSuccessCount() {
        return bulkActionProgressPayload.totalCount - bulkActionProgressPayload.errors.length;
      }

      function startMultiSelectPrinting(theOrders) {
        let orderToProcess;
        if (theOrders.length > 0) {
          bulkActionProgressPayload.currentIndex += 1;
          orderToProcess = theOrders.splice(0, 1)[0];

          ctrl.isPrintLabelsLoading = true;
          Order.search({
            noOfRecords: 1,
            page: 1,
            keyword: orderToProcess.tracking_id,
            exact: true,
            searchFields: ['trackingId', 'stampId'],
            oneMonthSearch: false,
          }).then(searchOrderSuccess, searchOrderFailure);
        } else {
          ctrl.isPrintLabelsLoading = false;
        }

        function searchOrderSuccess(response) {
          const responseOrders = _.get(response, 'orders');
          if (_.size(responseOrders) <= 0) {
            bulkActionProgressPayload.errors.push(orderToProcess.tracking_id);
          } else {
            startPrinting(responseOrders[0]);
          }

          function startPrinting(responseOrder) {
            const payload = [generateShippingLabelPayload(responseOrder)];

            Printer.printByPost(payload)
              .then(printSuccess, printError);
          }

          function printSuccess() {
            bulkActionProgressPayload.successCount += 1;

            if (theOrders.length > 0) {
              startMultiSelectPrinting(theOrders);
            } else {
              ctrl.isPrintLabelsLoading = false;
            }
          }

          function printError() {
            bulkActionProgressPayload.errors.push(orderToProcess.tracking_id);

            if (theOrders.length > 0) {
              startMultiSelectPrinting(theOrders);
            } else {
              ctrl.isPrintLabelsLoading = false;
            }
          }
        }

        function searchOrderFailure() {
          bulkActionProgressPayload.errors.push(orderToProcess.tracking_id);

          if (theOrders.length > 0) {
            startMultiSelectPrinting(theOrders);
          } else {
            ctrl.isPrintLabelsLoading = false;
          }
        }
      }
    }

    function printShippingLabel(order) {
      order.isPrinting = true;

      Order.search({
        noOfRecords: 1,
        page: 1,
        keyword: order.tracking_id,
        exact: true,
        searchFields: ['trackingId', 'stampId'],
        oneMonthSearch: false,
      }).then(searchSuccess, searchFailure);

      function searchSuccess(response) {
        const theOrders = _.get(response, 'orders');
        if (_.size(theOrders) <= 0) {
          order.isPrinting = false;
          nvToast.error(nvTranslate.instant('container.route-inbound.unable-to-print-this-order'));
          return;
        }

        const payload = [generateShippingLabelPayload(theOrders[0])];
        Printer.printByPost(payload)
          .then(printSuccess, printFailure);
      }

      function searchFailure() {
        order.isPrinting = false;
      }

      function printSuccess() {
        order.isPrinting = false;
        nvToast.success(nvTranslate.instant('container.route-inbound.shipping-label-printed'));
      }

      function printFailure() {
        order.isPrinting = false;
      }
    }

    function onCancel() {
      $mdDialog.cancel();
    }

    function viewOrdersOrReservations(shipper, isScannedOrdersOnly = false) {
      ctrl.chosenShipper = shipper;

      ctrl.shipperReservationsTableParams = null;
      if (!isScannedOrdersOnly && reservationsByShipper[ctrl.chosenShipper.id]) {
        createShipperReservationsTable(reservationsByShipper[ctrl.chosenShipper.id]);
      }

      ctrl.shipperOrdersTableParams = null;
      if (ordersByShipper[ctrl.chosenShipper.id] || reservationsByShipper[ctrl.chosenShipper.id]) {
        let theOrders = ordersByShipper[ctrl.chosenShipper.id] || [];

        // add reservations' orders
        _.forEach(reservationsByShipper[ctrl.chosenShipper.id], (reservation) => {
          theOrders = _.concat(theOrders, reservation.orders || []);
        });

        if (isScannedOrdersOnly) {
          theOrders = _.filter(theOrders, order =>
            _.size(order.scan_details) > 0
          );
        }

        createShipperOrdersTable(
          _.orderBy(
            theOrders,
            ['isReservation', 'isGrabReservation', 'isValidFailure', 'type'],
            ['asc', 'asc', 'asc', 'desc']
          )
        );
      }
    }

    function viewShippers() {
      ctrl.chosenShipper = null;
      createByShippersTable();
    }

    function editOrder(order) {
      if (order.id) {
        goToEditOrderPage(order.id);
        return;
      }

      Order.search({
        noOfRecords: 1,
        page: 1,
        keyword: order.tracking_id,
        exact: true,
        searchFields: ['trackingId', 'stampId'],
        oneMonthSearch: false,
      }).then(searchOrderSuccess);

      function searchOrderSuccess(response) {
        const responseOrders = _.get(response, 'orders');
        if (_.size(responseOrders) > 0) {
          goToEditOrderPage(_.get(responseOrders, '[0].id'));
        } else {
          nvToast.error(nvTranslate.instant('commons.not-available'));
        }
      }

      function goToEditOrderPage(orderId) {
        $window.open(
          $state.href('container.order.edit', { orderId: orderId })
        );
      }
    }

    function createByShippersTable() {
      ctrl.byShippersTableParams = nvTable.createTable({
        name: { displayName: 'commons.model.shipper-name' },
        scanned: { displayName: 'container.route-inbound.route-inbounded' },
        total: { displayName: 'commons.total' },
      });

      ctrl.byShippersTableParams.setData(byShippers);
    }

    function createShipperReservationsTable(data) {
      ctrl.shipperReservationsTableParams = nvTable.createTable({
        id: { displayName: 'commons.reservation-id' },
        location: { displayName: 'commons.location' },
        readyToLatestTime: { displayName: 'container.route-inbound.ready-to-latest-time' },
        approx_volume: { displayName: 'container.route-inbound.approx-vol' },
        translatedStatus: { displayName: 'commons.status' },
        totalPickupCount: { displayName: 'container.route-inbound.received-parcels' },
      });

      ctrl.shipperReservationsTableParams.setData(data);
    }

    function createShipperOrdersTable(data) {
      ctrl.shipperOrdersTableParams = nvTable.createTable({
        tracking_id: { displayName: 'commons.tracking-id' },
        stamp_id: { displayName: 'commons.stamp-id' },
        location: { displayName: 'commons.location' },
        customType: { displayName: 'commons.type' },
        translatedStatus: { displayName: 'commons.status' },
        cmi_count: { displayName: 'container.route-inbound.cmi-count' },
        scanStatus: { displayName: 'container.route-inbound.route-inbounded-status' },
      });

      ctrl.shipperOrdersTableParams.setData(data);
    }

    function generateShippingLabelPayload(order) {
      return {
        recipient_name: order.toName,
        address1: order.toAddress1,
        address2: order.toAddress2,
        contact: order.toContact,
        country: order.toCountry,
        postcode: order.toPostcode,
        shipper_name: order.fromName,
        tracking_id: order.trackingId,
        barcode: order.trackingId,
        route_id: '',
        driver_name: '',
        template: 'hub_shipping_label_70X50',
      };
    }
  }
}());
