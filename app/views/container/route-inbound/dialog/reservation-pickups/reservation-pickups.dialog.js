(function controller() {
  angular
    .module('nvOperatorApp.controllers')
    .controller('RouteInboundReservationPickupsDialogController', RouteInboundReservationPickupsDialogController);

  RouteInboundReservationPickupsDialogController.$inject = [
    '$mdDialog', 'localData', 'nvTable', 'Order', 'nvToast',
    '$window', '$state', 'nvTranslate', 'Printer',
  ];

  function RouteInboundReservationPickupsDialogController(
    $mdDialog, localData, nvTable, Order, nvToast,
    $window, $state, nvTranslate, Printer
  ) {
    // variables
    const ctrl = this;
    const reservations = _.cloneDeep(localData.reservations) || [];
    const extraOrders = _.cloneDeep(localData.extraOrders) || [];

    ctrl.title = localData.title;
    ctrl.inboundedOrdersTableParams = null;
    ctrl.nonInboundedOrdersTableParams = null;
    ctrl.extraOrdersTableParams = null;

    // functions
    ctrl.onCancel = onCancel;
    ctrl.editOrder = editOrder;
    ctrl.printShippingLabel = printShippingLabel;

    // start
    initialize();

    // functions details
    function initialize() {
      let orders = [];
      _.forEach(reservations, (reservation) => {
        const reservationOrders = extendOrders(reservation.orders || []);

        orders = _.concat(orders, reservationOrders);
      });

      const inboundedOrders = _.filter(orders, order =>
        _.size(_.get(order, 'scan_details')) > 0
      );

      const nonInboundedOrders = _.filter(orders, order =>
        _.size(_.get(order, 'scan_details')) <= 0
      );

      createInboundedOrdersTable(inboundedOrders);
      createNonInboundedOrdersTable(nonInboundedOrders);
      createExtraOrdersTable(extraOrders);

      function extendOrders(theOrders) {
        return _.map(theOrders, order => (
          setCustomOrderData(order)
        ));

        function setCustomOrderData(order) {
          order.isPrinting = false;
          order.location = localData.generateOrderLocation(order);

          return order;
        }
      }

      function createInboundedOrdersTable(theInboundedOrders) {
        ctrl.inboundedOrdersTableParams = nvTable.createTable({
          tracking_id: { displayName: 'commons.tracking-id' },
          shipper_name: { displayName: 'commons.model.shipper-name' },
          reservationId: { displayName: 'commons.reservation-id' },
          location: { displayName: 'commons.location' },
        });

        ctrl.inboundedOrdersTableParams.setData(theInboundedOrders);
      }

      function createNonInboundedOrdersTable(theNonInboundedOrders) {
        ctrl.nonInboundedOrdersTableParams = nvTable.createTable({
          tracking_id: { displayName: 'commons.tracking-id' },
          shipper_name: { displayName: 'commons.model.shipper-name' },
          reservationId: { displayName: 'commons.reservation-id' },
          location: { displayName: 'commons.location' },
        });

        ctrl.nonInboundedOrdersTableParams.setData(theNonInboundedOrders);
      }

      function createExtraOrdersTable(theExtraOrders) {
        ctrl.extraOrdersTableParams = nvTable.createTable({
          tracking_id: { displayName: 'commons.tracking-id' },
          shipper_name: { displayName: 'commons.model.shipper-name' },
        });

        ctrl.extraOrdersTableParams.setData(theExtraOrders);
      }
    }

    function onCancel() {
      $mdDialog.cancel();
    }

    function editOrder(order) {
      if (order.id) {
        goToEditOrderPage(order.id);
        return;
      }

      Order.search({
        noOfRecords: 1,
        page: 1,
        keyword: order.tracking_id,
        exact: true,
        searchFields: ['trackingId', 'stampId'],
        oneMonthSearch: false,
      }).then(searchOrderSuccess);

      function searchOrderSuccess(response) {
        const responseOrders = _.get(response, 'orders');
        if (_.size(responseOrders) > 0) {
          goToEditOrderPage(_.get(responseOrders, '[0].id'));
        } else {
          nvToast.error(nvTranslate.instant('commons.not-available'));
        }
      }

      function goToEditOrderPage(orderId) {
        $window.open(
          $state.href('container.order.edit', { orderId: orderId })
        );
      }
    }

    function printShippingLabel(order) {
      order.isPrinting = true;

      Order.search({
        noOfRecords: 1,
        page: 1,
        keyword: order.tracking_id,
        exact: true,
        searchFields: ['trackingId', 'stampId'],
        oneMonthSearch: false,
      }).then(searchSuccess, searchFailure);

      function searchSuccess(response) {
        const theOrders = _.get(response, 'orders');
        if (_.size(theOrders) <= 0) {
          order.isPrinting = false;
          nvToast.error(nvTranslate.instant('container.route-inbound.unable-to-print-this-order'));
          return;
        }

        const payload = [generateShippingLabelPayload(theOrders[0])];
        Printer.printByPost(payload)
          .then(printSuccess, printFailure);
      }

      function searchFailure() {
        order.isPrinting = false;
      }

      function printSuccess() {
        order.isPrinting = false;
        nvToast.success(nvTranslate.instant('container.route-inbound.shipping-label-printed'));
      }

      function printFailure() {
        order.isPrinting = false;
      }
    }

    function generateShippingLabelPayload(order) {
      return {
        recipient_name: order.toName,
        address1: order.toAddress1,
        address2: order.toAddress2,
        contact: order.toContact,
        country: order.toCountry,
        postcode: order.toPostcode,
        shipper_name: order.fromName,
        tracking_id: order.trackingId,
        barcode: order.trackingId,
        route_id: '',
        driver_name: '',
        template: 'hub_shipping_label_70X50',
      };
    }
  }
}());
