(function controller() {
  angular
    .module('nvOperatorApp.controllers')
    .controller('RouteInboundChooseRouteIdDialogController', RouteInboundChooseRouteIdDialogController);

  RouteInboundChooseRouteIdDialogController.$inject = [
    '$mdDialog', 'localData',
  ];

  function RouteInboundChooseRouteIdDialogController(
    $mdDialog, localData
  ) {
    // variables
    const ctrl = this;

    ctrl.routeIds = localData.routeIds;

    // functions
    ctrl.onFetch = onFetch;
    ctrl.onCancel = onCancel;

    // functions details
    function onFetch(routeId) {
      $mdDialog.hide(routeId);
    }

    function onCancel() {
      $mdDialog.cancel();
    }
  }
}());
