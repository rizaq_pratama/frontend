(function controller() {
  angular
    .module('nvOperatorApp.controllers')
    .controller('RouteInboundCommentsDialogController', RouteInboundCommentsDialogController);

  RouteInboundCommentsDialogController.$inject = [
    '$mdDialog', 'localData', 'nvTable', 'Route',
  ];

  function RouteInboundCommentsDialogController(
    $mdDialog, localData, nvTable, Route
  ) {
    // variables
    const ctrl = this;
    const routeId = localData.routeId;
    const comments = localData.comments;

    ctrl.form = {};
    ctrl.formSubmitting = false;
    ctrl.formComment = '';
    ctrl.commentsTableParams = null;

    // functions
    ctrl.isSubmitDisabled = isSubmitDisabled;
    ctrl.onAdd = onAdd;
    ctrl.onCancel = onCancel;

    // start
    initialize();

    // functions details
    function initialize() {
      ctrl.commentsTableParams = nvTable.createTable({
        comment: { displayName: 'commons.comments' },
      });

      ctrl.commentsTableParams.setData(_.map(comments, value => ({
        comment: value,
      })));
    }

    function isSubmitDisabled() {
      return !ctrl.formComment || ctrl.formSubmitting;
    }

    function onAdd() {
      const payload = {
        comment: ctrl.formComment,
      };

      ctrl.formSubmitting = true;
      Route.routeInboundEndSession(routeId, payload).then(success, failure);

      function success() {
        ctrl.formSubmitting = false;
        $mdDialog.hide();
      }

      function failure() {
        ctrl.formSubmitting = false;
      }
    }

    function onCancel() {
      $mdDialog.cancel();
    }
  }
}());
