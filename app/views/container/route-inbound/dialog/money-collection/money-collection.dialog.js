(function controller() {
  angular
    .module('nvOperatorApp.controllers')
    .controller('RouteInboundMoneyCollectionDialogController', RouteInboundMoneyCollectionDialogController);

  RouteInboundMoneyCollectionDialogController.$inject = [
    '$mdDialog', 'localData', 'nvTable', 'nvTranslate', 'Transaction',
    'Order', 'nvCurrency', '$timeout', 'Inbound', 'nvDateTimeUtils',
    '$rootScope',
  ];

  function RouteInboundMoneyCollectionDialogController(
    $mdDialog, localData, nvTable, nvTranslate, Transaction,
    Order, nvCurrency, $timeout, Inbound, nvDateTimeUtils,
    $rootScope
  ) {
    // variables
    const FORM_VALIDATION = {
      cashCollected: [{
        key: 'eitherCashOrCreditRequired',
        msg: 'container.route-inbound.must-fill-in-either-cash-or-credit',
      }],
      creditCollected: [{
        key: 'eitherCashOrCreditRequired',
        msg: 'container.route-inbound.must-fill-in-either-cash-or-credit',
      }],
    };

    const ctrl = this;
    const routeId = localData.routeId;
    let cashInboundHistory = _.cloneDeep(localData.cashInboundHistory);
    let collectedOrders = _.cloneDeep(localData.collectedOrders);
    let uncollectedOrders = _.cloneDeep(localData.uncollectedOrders);
    const collectedReservationFees = _.cloneDeep(localData.collectedReservationFees);

    ctrl.FORM_VALIDATION = FORM_VALIDATION;

    ctrl.cashInboundHistoryTableParams = null;
    ctrl.collectedOrdersTableParams = null;
    ctrl.uncollectedOrdersTableParams = null;
    ctrl.collectedReservationFeesTableParams = null;
    ctrl.enableInputTab = localData.enableInputTab;
    ctrl.isInputTabSelected = ctrl.enableInputTab;

    ctrl.inputForm = {
      form: {},
      submitState: 'idle',
      outstandingAmount: localData.remainingCashToCollect,
      expectedTotalText: `${getCurrencySymbol()} ${localData.totalCodAmountReceived.toFixed(2)}`,
      outstandingAmountText: `${getCurrencySymbol()} ${localData.remainingCashToCollect.toFixed(2)}`,
      isTransactionIdRequired: false,
      cashCollected: null,
      receiptNumber: null,
      recipient: null,
      creditCollected: null,
      transactionId: null,
    };

    // functions
    ctrl.inputTabSelected = inputTabSelected;
    ctrl.getCurrencySymbol = getCurrencySymbol;
    ctrl.validateCashOrCreditCollected = validateCashOrCreditCollected;
    ctrl.onSubmit = onSubmit;
    ctrl.onCancel = onCancel;

    // start
    initialize();

    // functions details
    function initialize() {
      cashInboundHistory = extendCashInboundHistory(cashInboundHistory);
      collectedOrders = extendOrders(collectedOrders);
      uncollectedOrders = extendOrders(uncollectedOrders);

      createCashInboundHistoryTable();
      createCollectedOrdersTable();
      createUncollectedOrdersTable();
      createCollectedReservationFeesTable();

      function extendOrders(theOrders) {
        return _.map(theOrders, order => (
          setCustomOrderData(order)
        ));

        function setCustomOrderData(order) {
          order.processedCodAmount = (order.cod_amount || 0).toFixed(2);
          order.processedCodCollected = (order.cod_collected || 0).toFixed(2);

          order.location = localData.generateOrderLocation(order);

          order.issue = order.failure_reason;
          if (order.transactionStatus === localData.ORDER_STATUS.PENDING) {
            order.issue = nvTranslate.instant('commons.pending');
          }

          if (order.transactionType === Transaction.TYPE.DELIVERY) {
            order.customType = nvTranslate.instant('container.route-inbound.delivery-xx', {
              type: getOrderCustomType(order),
            });
          } else { // pickup
            order.customType = nvTranslate.instant('container.route-inbound.pickup-xx', {
              type: getOrderCustomType(order),
            });
          }

          return order;

          function getOrderCustomType(theOrder) {
            if (theOrder.isReservation) {
              return nvTranslate.instant('container.route-inbound.reservation');
            } else if (theOrder.isGrabReservation) {
              return nvTranslate.instant('container.route-inbound.grab-pickup');
            }

            return theOrder.is_rts ? nvTranslate.instant('commons.rts') : Order.TYPE[theOrder.type];
          }
        }
      }

      function extendCashInboundHistory(theCashInboundHistory) {
        return _.map(theCashInboundHistory, row => (
          setCustomCashInboundHistory(row)
        ));

        function setCustomCashInboundHistory(row) {
          row.processedAmount = (row.amount || 0).toFixed(2);
          row.processedType = _.upperFirst(_.lowerCase(row.type));
          row.processedCreatedAt = nvDateTimeUtils.displayDateTime(
            moment.utc(_.get(row, 'created_at'))
          );
          return row;
        }
      }
    }

    function createCashInboundHistoryTable() {
      ctrl.cashInboundHistoryTableParams = nvTable.createTable({
        processedAmount: { displayName: 'commons.amount' },
        processedType: { displayName: 'commons.type' },
        inbounded_by: { displayName: 'commons.model.inbounded-by' },
        processedCreatedAt: { displayName: 'container.route-inbound.inbounded-time' },
        receipt_no: { displayName: 'container.route-inbound.receipt-or-transaction-id' },
      });

      ctrl.cashInboundHistoryTableParams.setData(cashInboundHistory);
      ctrl.cashInboundHistoryTableParams.sort('processedCreatedAt');
    }

    function createCollectedOrdersTable() {
      ctrl.collectedOrdersTableParams = nvTable.createTable({
        processedCodAmount: { displayName: 'container.route-inbound.expected-amount' },
        processedCodCollected: { displayName: 'container.route-inbound.amount-collected' },
        tracking_id: { displayName: 'commons.tracking-id' },
        stamp_id: { displayName: 'commons.stamp-id' },
        customType: { displayName: 'commons.type' },
        location: { displayName: 'commons.location' },
        contact: { displayName: 'commons.contact' },
        name: { displayName: 'container.route-inbound.addressee' },
      });

      ctrl.collectedOrdersTableParams.setData(collectedOrders);
    }

    function createUncollectedOrdersTable() {
      ctrl.uncollectedOrdersTableParams = nvTable.createTable({
        processedCodAmount: { displayName: 'container.route-inbound.expected-amount' },
        tracking_id: { displayName: 'commons.tracking-id' },
        stamp_id: { displayName: 'commons.stamp-id' },
        customType: { displayName: 'commons.type' },
        issue: { displayName: 'container.route-inbound.issue' },
        location: { displayName: 'commons.location' },
      });

      ctrl.uncollectedOrdersTableParams.setData(uncollectedOrders);
    }

    function createCollectedReservationFeesTable() {
      ctrl.collectedReservationFeesTableParams = nvTable.createTable({
        totalCollectedAmount: { displayName: 'container.route-inbound.amount-collected' },
        id: { displayName: 'commons.reservation-id' },
        shipper_name: { displayName: 'commons.model.shipper-name' },
      });

      ctrl.collectedReservationFeesTableParams.setData(collectedReservationFees);
    }

    function inputTabSelected(bool) {
      ctrl.isInputTabSelected = bool;
    }

    function getCurrencySymbol() {
      return nvCurrency.getSymbol($rootScope.countryId);
    }

    function onSubmit() {
      validateCashOrCreditCollected();

      $timeout(() => {
        if (ctrl.inputForm.form.$valid) {
          const payload = {};

          if (ctrl.inputForm.cashCollected) {
            payload.cash_receipt = {
              amount: ctrl.inputForm.cashCollected,
              receipt_no: ctrl.inputForm.receiptNumber || '',
            };
          }

          if (ctrl.inputForm.creditCollected) {
            payload.credit_receipt = {
              amount: ctrl.inputForm.creditCollected,
              receipt_no: ctrl.inputForm.transactionId,
            };
          }

          ctrl.inputForm.submitState = 'waiting';
          Inbound.inboundCods(routeId, payload).then(success).finally(finallyFn);
        }
      });

      function success() {
        $mdDialog.hide();
      }

      function finallyFn() {
        ctrl.inputForm.submitState = 'idle';
      }
    }

    function validateCashOrCreditCollected() {
      $timeout(() => {
        if (!ctrl.inputForm.cashCollected && !ctrl.inputForm.creditCollected) {
          ctrl.inputForm.form['cash-collected'].$setValidity('eitherCashOrCreditRequired', false);
          ctrl.inputForm.form['credit-collected'].$setValidity('eitherCashOrCreditRequired', false);
        } else {
          ctrl.inputForm.form['cash-collected'].$setValidity('eitherCashOrCreditRequired', true);
          ctrl.inputForm.form['credit-collected'].$setValidity('eitherCashOrCreditRequired', true);
        }

        ctrl.inputForm.isTransactionIdRequired = false;
        if (ctrl.inputForm.creditCollected) {
          ctrl.inputForm.isTransactionIdRequired = true;
        }

        _.each(ctrl.inputForm.form, (field) => {
          if (_.isObject(field) && !_.isUndefined(field.$validate)) {
            field.$setTouched();
            field.$validate();
          }
        });
      });
    }

    function onCancel() {
      $mdDialog.cancel();
    }
  }
}());
