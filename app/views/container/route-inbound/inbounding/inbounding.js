(function controller() {
  angular
    .module('nvOperatorApp.controllers')
    .controller('RouteInboundInboundingController', RouteInboundInboundingController);

  RouteInboundInboundingController.$inject = [
    '$scope', '$state', 'nvNavGuard', '$timeout', '$window',
    'Inbound', 'Transaction', 'nvDialog', 'Route', 'nvToast',
    'nvTranslate', 'Warehousing', 'Order', 'nvSoundService', 'nvDateTimeUtils',
    'Reservation', '$q',
  ];

  function RouteInboundInboundingController(
    $scope, $state, nvNavGuard, $timeout, $window,
    Inbound, Transaction, nvDialog, Route, nvToast,
    nvTranslate, Warehousing, Order, nvSoundService, nvDateTimeUtils,
    Reservation, $q
  ) {
    // variables
    const VIEW_DETAILS_MODE = {
      ALL: 'all',
      RESERVATION: 'reservation',
      GRAB_RESERVATION: 'grab-reservation',
      ORDER: 'order',
    };
    const CSS_CLASS = {
      DUPLICATE: 'duplicate',
      ORDER_NOT_FOUND: 'order-not-found',
      ORDER_COMPLETED: 'order-completed',
      ORDER_CANCELLED: 'order-cancelled',
      ORDER_RETURNED_TO_SENDER: 'order-returned-to-sender',
      ORDER_TRANSFERRED_TO_3PL: 'order-transferred-to-3pl',
      ON_HOLD: 'on-hold',
      UNKNOWN_ERROR: 'unknown-error',
      PENDING_DELIVERY: 'pending-delivery',
      FAILED_DELIVERY_INVALID: 'failed-delivery-invalid',
      FAILED_DELIVERY_VALID: 'failed-delivery-valid',
      SUCCESSFUL_C2C_OR_RETURN_PICKUP: 'successful-c2c-or-return-pickup',
      RESERVATION: 'reservation',
      GRAB_RESERVATION: 'grab-reservation',
    };

    const ctrl = this;
    const parentCtrl = $scope.$parent.ctrl;

    ctrl.parentCtrl = parentCtrl;
    ctrl.VIEW_DETAILS_MODE = VIEW_DETAILS_MODE;
    ctrl.endSessionState = 'idle';

    ctrl.formTrackingId = '';
    ctrl.formIsUnresolved = false;
    ctrl.remainingCashToCollect = 0;
    ctrl.repeatedCMIOrders = [];
    ctrl.expectedScans = [];
    ctrl.inboundingHistory = [];

    // functions
    ctrl.hasComments = hasComments;
    ctrl.getLatestComment = getLatestComment;
    ctrl.getParcelsProcessedCount = getParcelsProcessedCount;
    ctrl.getParcelsProcessedTotal = getParcelsProcessedTotal;
    ctrl.getScannedOrders = getScannedOrders;
    ctrl.viewMoneyCollection = viewMoneyCollection;
    ctrl.viewDetails = viewDetails;
    ctrl.scanInbound = scanInbound;
    ctrl.onUnresolvedCheckboxChange = onUnresolvedCheckboxChange;
    ctrl.endSession = endSession;
    ctrl.routeInboundComments = routeInboundComments;
    ctrl.addressVerification = addressVerification;
    ctrl.goToRouteManifestPage = goToRouteManifestPage;
    ctrl.goToDriverReportPage = goToDriverReportPage;
    ctrl.goToRelabelStampPage = goToRelabelStampPage;
    ctrl.goToResultPage = goToResultPage;

    // start
    initialize();

    // functions details
    function initialize() {
      if (!parentCtrl.IS_DEBUG) {
        nvNavGuard.lock();

        // validation
        if (_.size(parentCtrl.inboundSummaryResult) <= 0) {
          goToFilterPage();
        }

        processInboundSummaryData();
      } else if (_.size(parentCtrl.inboundSummaryResult) <= 0) {
        // load debug data
        parentCtrl.selectedHub = {
          id: 1,
          name: 'DEBUG HUB',
        };

        $.getJSON('views/container/route-inbound/json/inbound-summary-result.json', (inboundSummaryResult) => {
          $timeout(() => {
            loadDebugDataSuccess(inboundSummaryResult);
          });
        });
      } else {
        processInboundSummaryData();
      }

      focusTrackingInput();

      function loadDebugDataSuccess(inboundSummaryResult) {
        parentCtrl.extendInboundSummaryResult(inboundSummaryResult);
        processInboundSummaryData();
      }
    }

    function processInboundSummaryData() {
      ctrl.inboundingHistory = [];
      ctrl.formIsUnresolved = _.includes([
        Route.STATUS.UNRESOLVED,
        Route.STATUS.ARCHIVED,
      ], Route.STATUS[parentCtrl.inboundSummaryResult.status]);
      ctrl.remainingCashToCollect = parentCtrl.getRemainingCashToCollect();

      // repeated CMI
      ctrl.repeatedCMIOrders = _.filter(parentCtrl.getAllOrders(), order =>
        order.cmi_count > 1
      );

      // expected scans
      // invalid/valid failed deliveries
      const failedDeliveryOrders = _.filter(parentCtrl.getAllFailedOrders(), order =>
        order.transactionType === Transaction.TYPE.DELIVERY
      );
      const invalidFailedDeliveryOrders = _.filter(failedDeliveryOrders, order =>
        order.isValidFailure === false
      );
      const validFailedDeliveryOrders = _.filter(failedDeliveryOrders, order =>
        order.isValidFailure === true
      );

      // pending delivery
      const pendingDeliveryOrders = _.filter(parentCtrl.getPendingOrders(), order =>
        order.transactionType === Transaction.TYPE.DELIVERY
      );

      // C2C / Return Pickups
      const successfulC2CAndReturnPickups = _.filter(
        parentCtrl.getAllSuccessfulC2CAndReturnPickups(), order =>
          order.transactionType === Transaction.TYPE.PICKUP
      );

      // Reservation Pickups
      let reservationPickups = [];
      let reservationPickupsCount = 0;
      _.forEach(parentCtrl.getReservations(), (reservation) => {
        reservationPickups = _.concat(reservationPickups, reservation.orders || []);

        if (Reservation.STATUS[reservation.status] === Reservation.STATUS.SUCCESS) {
          // for reservation count, need to take
          // higher number of scanned_parcels vs received_parcels
          reservationPickupsCount += parentCtrl.getTotalParcelsCountInReservation(reservation);
        }
      });

      // Grab Pickups
      let grabReservationPickups = [];
      let grabReservationPickupsCount = 0;
      _.forEach(parentCtrl.getGrabReservations(), (reservation) => {
        grabReservationPickups = _.concat(grabReservationPickups, reservation.orders || []);

        if (Reservation.STATUS[reservation.status] === Reservation.STATUS.SUCCESS) {
          // for reservation count, need to take
          // higher number of scanned_parcels vs received_parcels
          grabReservationPickupsCount += parentCtrl.getTotalParcelsCountInReservation(reservation);
        }
      });

      ctrl.expectedScans = [
        {
          id: 'container.route-inbound.grab-pickups',
          all: grabReservationPickups,
          count: grabReservationPickupsCount,
          type: VIEW_DETAILS_MODE.GRAB_RESERVATION,
          cssClass: CSS_CLASS.GRAB_RESERVATION,
        },
        {
          id: 'container.route-inbound.pending-deliveries',
          all: pendingDeliveryOrders,
          type: VIEW_DETAILS_MODE.ORDER,
          cssClass: CSS_CLASS.PENDING_DELIVERY,
        },
        {
          id: 'container.route-inbound.failed-deliveries-invalid',
          all: invalidFailedDeliveryOrders,
          type: VIEW_DETAILS_MODE.ORDER,
          cssClass: CSS_CLASS.FAILED_DELIVERY_INVALID,
        },
        {
          id: 'container.route-inbound.failed-deliveries-valid',
          all: validFailedDeliveryOrders,
          type: VIEW_DETAILS_MODE.ORDER,
          cssClass: CSS_CLASS.FAILED_DELIVERY_VALID,
        },
        {
          id: 'container.route-inbound.c2c-or-return-pickups',
          all: successfulC2CAndReturnPickups,
          type: VIEW_DETAILS_MODE.ORDER,
          cssClass: CSS_CLASS.SUCCESSFUL_C2C_OR_RETURN_PICKUP,
        },
        {
          id: 'container.route-inbound.reservation-pickups',
          all: reservationPickups,
          count: reservationPickupsCount,
          type: VIEW_DETAILS_MODE.RESERVATION,
          cssClass: CSS_CLASS.RESERVATION,
        },
      ];

      _.forEach(parentCtrl.inboundSummaryOrdersMap, (order) => {
        if (_.size(order.scan_details) > 0) {
          addInboundingHistoryByOrder(order);
        }
      });
    }

    function addInboundingHistoryByOrder(order, reasonOrAction = [], cssClass = null) {
      let theCssClass = null;
      const statusOrTypes = [];
      let theReasonOrAction = [];

      switch (order.transactionStatus) {
        case parentCtrl.ORDER_STATUS.SUCCESS:
          if (order.transactionType === Transaction.TYPE.PICKUP) {
            // C2C / Return Pickup / Reservation
            if (order.isReservation) {
              theCssClass = CSS_CLASS.RESERVATION;
              statusOrTypes.push(
                nvTranslate.instant('container.route-inbound.reservation-pickup')
              );

              if (order.reservationId === null) {
                theReasonOrAction.push(
                  nvTranslate.instant('container.route-inbound.extra-order')
                );
              }
            } else if (order.isGrabReservation) {
              theCssClass = CSS_CLASS.GRAB_RESERVATION;
              statusOrTypes.push(
                nvTranslate.instant('container.route-inbound.grab-pickup')
              );

              if (order.reservationId === null) {
                theReasonOrAction.push(
                  nvTranslate.instant('container.route-inbound.extra-order')
                );
              }
            } else if (Order.isTypeEnum(order, Order.TYPE.C2C)) {
              theCssClass = CSS_CLASS.SUCCESSFUL_C2C_OR_RETURN_PICKUP;
              statusOrTypes.push(
                nvTranslate.instant('container.route-inbound.c2c-pickup')
              );
            } else {
              theCssClass = CSS_CLASS.SUCCESSFUL_C2C_OR_RETURN_PICKUP;
              statusOrTypes.push(
                nvTranslate.instant('container.route-inbound.return-pickup')
              );
            }
          } else if (order.is_rts) {
            theCssClass = CSS_CLASS.ORDER_RETURNED_TO_SENDER;
            theReasonOrAction.push(
              nvTranslate.instant('container.route-inbound.order-returned-to-sender')
            );
          } else {
            theCssClass = CSS_CLASS.ORDER_COMPLETED;
            theReasonOrAction.push(
              nvTranslate.instant('container.route-inbound.order-completed')
            );
          }
          break;
        case parentCtrl.ORDER_STATUS.FAIL:
          if (order.transactionType === Transaction.TYPE.DELIVERY) {
            if (order.isValidFailure) {
              theCssClass = CSS_CLASS.FAILED_DELIVERY_VALID;
              statusOrTypes.push(
                nvTranslate.instant('container.route-inbound.failed-delivery-valid')
              );
            } else {
              theCssClass = CSS_CLASS.FAILED_DELIVERY_INVALID;
              statusOrTypes.push(
                nvTranslate.instant('container.route-inbound.failed-delivery-invalid')
              );
            }

            theReasonOrAction.push(order.failure_reason);
          } else {
            theCssClass = CSS_CLASS.UNKNOWN_ERROR;
            theReasonOrAction.push(
              nvTranslate.instant('container.route-inbound.pickup-failed')
            );
          }
          break;
        default: // PENDING
          if (order.transactionType === Transaction.TYPE.DELIVERY) {
            theCssClass = CSS_CLASS.PENDING_DELIVERY;
            statusOrTypes.push(
              nvTranslate.instant('container.route-inbound.pending-delivery')
            );
          } else {
            theCssClass = CSS_CLASS.UNKNOWN_ERROR;
            theReasonOrAction.push(
              nvTranslate.instant('container.route-inbound.pending-pickup')
            );
          }
      }

      if (order.cmi_count > 1) {
        statusOrTypes.push(
          nvTranslate.instant('container.route-inbound.repeated-cmi')
        );
      }

      if (_.get(order, 'scan_details.rackSector')) {
        theReasonOrAction.push(_.get(order, 'scan_details.rackSector'));
      }

      if (order.granular_status === Order.GRANULAR_STATUS.ON_HOLD) {
        // override for on hold status
        theCssClass = CSS_CLASS.ON_HOLD;
        theReasonOrAction = [
          nvTranslate.instant('container.route-inbound.recovery'),
        ];
      }

      // overide reasonOrAction
      if (_.size(reasonOrAction) > 0) {
        theReasonOrAction = reasonOrAction;
      }

      // overide cssClass
      if (cssClass !== null) {
        theCssClass = cssClass;
      }

      addInboundingHistory({
        cssClass: theCssClass,
        scan: _.get(order, 'scan_details.scan'),
        status: _.join(statusOrTypes, ', '),
        reason: _.join(theReasonOrAction, ', '),
        createdAt: _.get(order, 'scan_details.created_at'),
      });
    }

    function addInboundingHistory(data) {
      let createdAt = moment(new Date()).unix();
      if (_.isString(data.createdAt)) {
        createdAt = moment(data.createdAt).unix();
      } else if (_.isNumber(data.createdAt)) {
        createdAt = data.createdAt;
      }

      ctrl.inboundingHistory.push({
        cssClass: data.cssClass || null,
        scan: data.scan,
        status: data.status || '-',
        reason: data.reason || '-',
        createdAt: createdAt,
      });
    }

    function hasComments() {
      return _.size(parentCtrl.inboundSummaryResult.comments) > 0;
    }

    function getLatestComment() {
      return _.last(parentCtrl.inboundSummaryResult.comments);
    }

    function getParcelsProcessedCount() {
      return _.size(getAllScannedParcels());
    }

    function getAllScannedParcels() {
      let orders = [];

      _.forEach(ctrl.expectedScans, (expectedScan) => {
        orders = _.concat(orders, getScannedOrders(expectedScan.all));
      });

      return orders;
    }

    function getParcelsProcessedTotal() {
      let count = 0;

      _.forEach(ctrl.expectedScans, (expectedScan) => {
        if (_.includes([
          VIEW_DETAILS_MODE.RESERVATION,
          VIEW_DETAILS_MODE.GRAB_RESERVATION,
        ], expectedScan.type)) {
          count += expectedScan.count;
        } else {
          count += _.size(expectedScan.all);
        }
      });

      return count;
    }

    function getAllParcelsProcessed() {
      let orders = [];

      _.forEach(ctrl.expectedScans, (expectedScan) => {
        orders = _.concat(orders, expectedScan.all);
      });

      return orders;
    }

    function getScannedOrders(orders) {
      return _.filter(orders, order =>
        _.size(_.get(order, 'scan_details')) > 0
      ) || [];
    }

    function viewMoneyCollection($event) {
      nvDialog.showSingle($event, {
        templateUrl: 'views/container/route-inbound/dialog/money-collection/money-collection.dialog.html',
        cssClass: 'route-inbound-money-collection',
        controller: 'RouteInboundMoneyCollectionDialogController',
        controllerAs: 'ctrl',
        locals: {
          localData: _.defaults({
            enableInputTab: true,
          }, parentCtrl.getViewMoneyCollectionDialogData()),
        },
      }).then(onSet).finally(finallyFn);

      function onSet() {
        parentCtrl.backdropLoading = true;
        Inbound.getSummaryByRouteId(parentCtrl.inboundSummaryResult.route_id)
          .then(success);

        function success(response) {
          parentCtrl.backdropLoading = false;

          parentCtrl.extendInboundSummaryResult(response);
          processInboundSummaryData();
        }
      }

      function finallyFn() {
        focusTrackingInput();
      }
    }

    function viewDetails($event, title, orders = [], viewDetailsMode = VIEW_DETAILS_MODE.ORDER) {
      let theOrders = orders;
      let reservations = [];
      switch (viewDetailsMode) {
        case VIEW_DETAILS_MODE.ALL:
          _.forEach(ctrl.expectedScans, (expectedScan) => {
            if (!_.includes([
              VIEW_DETAILS_MODE.RESERVATION,
              VIEW_DETAILS_MODE.GRAB_RESERVATION,
            ], expectedScan.type)) {
              theOrders = _.concat(theOrders, expectedScan.all);
            }
          });

          reservations = _.concat(
            parentCtrl.getReservations(),
            parentCtrl.getGrabReservations()
          );
          break;
        case VIEW_DETAILS_MODE.GRAB_RESERVATION:
          theOrders = [];
          reservations = parentCtrl.getGrabReservations();
          break;
        case VIEW_DETAILS_MODE.RESERVATION:
          theOrders = [];
          reservations = parentCtrl.getReservations();
          break;
        default:
      }

      if (_.includes([
        VIEW_DETAILS_MODE.RESERVATION,
        VIEW_DETAILS_MODE.GRAB_RESERVATION,
      ], viewDetailsMode)) {
        nvDialog.showSingle($event, {
          templateUrl: 'views/container/route-inbound/dialog/reservation-pickups/reservation-pickups.dialog.html',
          cssClass: 'route-inbound-reservation-pickups',
          controller: 'RouteInboundReservationPickupsDialogController',
          controllerAs: 'ctrl',
          locals: {
            localData: _.defaults({
              title: title,
              reservations: reservations,
              extraOrders: _.get(parentCtrl.inboundSummaryResult, 'extra_order_details') || [],
            }, parentCtrl.getViewDetailsDialogGeneralFunctions()),
          },
        }).finally(finallyFn);
      } else {
        nvDialog.showSingle($event, {
          templateUrl: 'views/container/route-inbound/dialog/view-details/view-details.dialog.html',
          cssClass: 'route-inbound-view-details',
          controller: 'RouteInboundViewDetailsDialogController',
          controllerAs: 'ctrl',
          locals: {
            localData: _.defaults({
              title: title,
              orders: theOrders,
              reservations: reservations,
            }, parentCtrl.getViewDetailsDialogGeneralFunctions()),
          },
        }).finally(finallyFn);
      }

      function finallyFn() {
        focusTrackingInput();
      }
    }

    function scanInbound(event) {
      const deferred = $q.defer();
      if (!ctrl.formTrackingId) {
        deferred.reject();
        return deferred.promise;
      }

      // `enter` event triggered
      const scanTrackingId = _.toUpper(_.trim(ctrl.formTrackingId));
      if (event.which === 13) {
        const order = getOrderByStampOrTrackingId(scanTrackingId);
        if (order && _.get(order, 'scan_details')) {
          const payload = [{
            scan: scanTrackingId,
            hubId: parentCtrl.selectedHub.id,
          }];

          Warehousing.sweep(payload);

          addInboundingHistory({
            cssClass: CSS_CLASS.DUPLICATE,
            scan: scanTrackingId,
            reason: nvTranslate.instant('commons.duplicate'),
          });
          playAudio(nvSoundService.getDuplicateSoundFile());
          deferred.reject();
        } else {
          const payload = {
            hub_id: parentCtrl.selectedHub.id,
            inbound_type: Inbound.getTypeEnum(Inbound.TYPE.SORTING_HUB),
            route_id: parentCtrl.inboundSummaryResult.route_id,
            scan: scanTrackingId,
          };

          if (!order) {
            payload.to_show_shipper_info = true;
          }

          Inbound.scanInbound(payload).then(inboundSuccess, inboundFailure);
        }

        ctrl.formTrackingId = '';
      } else {
        deferred.reject();
      }

      return deferred.promise;

      function inboundSuccess(response) {
        let cssClass = null;
        const statusOrTypes = [];
        const reasonOrAction = [];
        let rackSector = null;

        switch (response.status) {
          case Inbound.getStatusEnum(Inbound.STATUS.ORDER_NOT_FOUND):
            cssClass = CSS_CLASS.ORDER_NOT_FOUND;
            reasonOrAction.push(
              nvTranslate.instant('container.route-inbound.order-not-found')
            );

            playAudio(nvSoundService.getMistakeSoundFile());
            break;
          case Inbound.getStatusEnum(Inbound.STATUS.ORDER_COMPLETED):
            cssClass = CSS_CLASS.ORDER_COMPLETED;
            reasonOrAction.push(
              nvTranslate.instant('container.route-inbound.order-completed')
            );

            playAudio(nvSoundService.getMistakeSoundFile());
            break;
          case Inbound.getStatusEnum(Inbound.STATUS.ORDER_CANCELLED):
            cssClass = CSS_CLASS.ORDER_CANCELLED;
            reasonOrAction.push(
              nvTranslate.instant('container.route-inbound.order-cancelled')
            );

            playAudio(nvSoundService.getMistakeSoundFile());
            break;
          case Inbound.getStatusEnum(Inbound.STATUS.ORDER_RETURNED_TO_SENDER):
            cssClass = CSS_CLASS.ORDER_RETURNED_TO_SENDER;
            reasonOrAction.push(
              nvTranslate.instant('container.route-inbound.order-returned-to-sender')
            );

            playAudio(nvSoundService.getMistakeSoundFile());
            break;
          case Inbound.getStatusEnum(Inbound.STATUS.ORDER_TRANSFERRED_TO_3PL):
            cssClass = CSS_CLASS.ORDER_TRANSFERRED_TO_3PL;
            reasonOrAction.push(
              nvTranslate.instant('container.route-inbound.order-transferred-to-3pl')
            );

            playAudio(nvSoundService.getMistakeSoundFile());
            break;
          case Inbound.getStatusEnum(Inbound.STATUS.ON_HOLD):
            cssClass = CSS_CLASS.ON_HOLD;
            reasonOrAction.push(
              nvTranslate.instant('container.route-inbound.recovery', {
                comment: _.get(response, 'comments') || '',
              })
            );

            playAudio(nvSoundService.getRecoverySoundFile());
            break;
          case Inbound.getStatusEnum(Inbound.STATUS.PENDING_DELIVERY):
            cssClass = CSS_CLASS.PENDING_DELIVERY;
            statusOrTypes.push(
              nvTranslate.instant('container.route-inbound.pending-delivery')
            );
            reasonOrAction.push(
              nvTranslate.instant('container.route-inbound.cmi-condition')
            );

            playAudio(nvSoundService.getMistakeSoundFile());
            break;
          case Inbound.getStatusEnum(Inbound.STATUS.SUCCESSFUL_INBOUND):
            if (_.get(response, 'is_international')) {
              rackSector = 'INTL';

              playAudio(nvSoundService.getInternationalSoundFile());
            } else if (_.get(response, 'set_aside')) {
              rackSector = nvTranslate.instant('container.route-inbound.set-aside');

              playAudio(nvSoundService.getSetAsideSoundFile());
            } else {
              rackSector = _.get(response, 'sector');

              playAudio(
                nvSoundService.getRackSoundFileWord(
                  _.toString(rackSector)
                    .replace(/[`~!@#$%^&*()_|+\-=?;:'",.<>\{\}\[\]\\\/]/gi, '')
                )
              );
            }

            reasonOrAction.push(rackSector);

            if (parentCtrl.selectedHub.id !== _.get(response, 'destination_hub_id')) {
              reasonOrAction.push(
                nvTranslate.instant('container.route-inbound.responsible-hub-x', {
                  name: _.get(response, 'destination_hub'),
                })
              );
            }
            break;
          default:
        }

        const order = getOrderByStampOrTrackingId(scanTrackingId);
        if (order) {
          order.scan_details = {
            id: null,
            type: Inbound.getTypeEnum(Inbound.TYPE.SORTING_HUB),
            scan: scanTrackingId,
            rackSector: rackSector,
            created_at: nvDateTimeUtils.displayISO(new Date(), 'utc'),
          };

          if (order.transactionStatus === parentCtrl.ORDER_STATUS.FAIL &&
            cssClass !== CSS_CLASS.ON_HOLD) {
            reasonOrAction.push(order.failure_reason);
          }

          addInboundingHistoryByOrder(
            order, reasonOrAction, cssClass
          );
        } else if (_.includes([
          Inbound.getStatusEnum(Inbound.STATUS.PENDING_DELIVERY),
          Inbound.getStatusEnum(Inbound.STATUS.SUCCESSFUL_INBOUND),
          Inbound.getStatusEnum(Inbound.STATUS.ON_HOLD),
        ], response.status)) {
          const extraOrderDetails = _.get(parentCtrl.inboundSummaryResult, 'extra_order_details');

          extraOrderDetails.push({
            shipper_id: _.get(response, 'shipper.id'),
            shipper_name: _.get(response, 'shipper.name'),
            tracking_id: scanTrackingId,
            scan_details: {
              id: null,
              type: Inbound.getTypeEnum(Inbound.TYPE.SORTING_HUB),
              scan: scanTrackingId,
              rackSector: rackSector,
              created_at: nvDateTimeUtils.displayISO(new Date(), 'utc'),
            },
          });

          let theCssClass = CSS_CLASS.RESERVATION;
          if (cssClass !== CSS_CLASS.ON_HOLD) {
            reasonOrAction.push(nvTranslate.instant('container.route-inbound.extra-order'));
          } else {
            theCssClass = CSS_CLASS.ON_HOLD;
          }

          addInboundingHistory({
            cssClass: theCssClass,
            scan: scanTrackingId,
            status: nvTranslate.instant('container.route-inbound.reservation-pickup'),
            reason: _.join(reasonOrAction, ', '),
          });
        } else {
          addInboundingHistory({
            cssClass: cssClass,
            scan: scanTrackingId,
            status: _.join(statusOrTypes, ', '),
            reason: _.join(reasonOrAction, ', '),
          });
        }

        deferred.resolve(response);
      }

      function inboundFailure(response) {
        addInboundingHistory({
          cssClass: CSS_CLASS.UNKNOWN_ERROR,
          scan: scanTrackingId,
          reason: nvTranslate.instant('container.route-inbound.unknown-error-text', {
            status: `${response.status} ${response.statusText}`,
          }),
        });

        playAudio(nvSoundService.getMistakeSoundFile());
        deferred.reject(response);
      }

      function getOrderByStampOrTrackingId(string) {
        if (parentCtrl.inboundSummaryOrdersMap[string]) {
          return parentCtrl.inboundSummaryOrdersMap[string];
        }

        if (parentCtrl.inboundSummaryStampOrdersMap[string]) {
          return parentCtrl.inboundSummaryStampOrdersMap[string];
        }

        return null;
      }
    }

    function onUnresolvedCheckboxChange() {
      $timeout(() => {
        let statusEnum = null;
        if (ctrl.formIsUnresolved) {
          statusEnum = Route.getStatusEnum(Route.STATUS.UNRESOLVED);
        } else if (isFullyCompleted()) {
          statusEnum = Route.getStatusEnum(Route.STATUS.ARCHIVED);
        } else {
          statusEnum = Route.getStatusEnum(Route.STATUS.IN_PROGRESS);
        }

        if (statusEnum === Route.getStatusEnum(Route.STATUS.ARCHIVED)) {
          if (statusEnum !== parentCtrl.inboundSummaryResult.status) {
            const payload = [{
              routeIds: [parentCtrl.inboundSummaryResult.route_id],
            }];

            Route.archiveRoutes(payload).then(archiveSuccess, failure);
          } else {
            nvToast.error(
              nvTranslate.instant('container.route-inbound.route-already-archived')
            );
            ctrl.formIsUnresolved = true;
          }
        } else {
          const payload = {
            status: statusEnum,
          };

          Route.routeInboundEndSession(
            parentCtrl.inboundSummaryResult.route_id, payload
          ).then(success, failure);
        }

        function archiveSuccess(response) {
          let notArchivedIds = [];
          if (_.size(response.unarchived_route_ids) > 0) {
            notArchivedIds = response.unarchived_route_ids;

            nvToast.error(
              nvTranslate.instant('container.route-logs.routes-ids-have-pending-waypoints', {
                ids: _.join(notArchivedIds, ', '),
              })
            );

            ctrl.formIsUnresolved = true;
            return;
          }

          ctrl.formIsUnresolved = true;
          parentCtrl.inboundSummaryResult.status = statusEnum;
          nvToast.info(nvTranslate.instant('container.route-inbound.updated-successfully'));
        }

        function success() {
          parentCtrl.inboundSummaryResult.status = statusEnum;
          nvToast.info(nvTranslate.instant('container.route-inbound.updated-successfully'));
        }

        function failure() {
          ctrl.formIsUnresolved = !ctrl.formIsUnresolved;
        }
      });
    }

    function endSession($event) {
      if (isFullyCompleted()) {
        archiveRoute();
      } else {
        setToUnresolved();
      }

      function archiveRoute() {
        const payload = [{
          routeIds: [parentCtrl.inboundSummaryResult.route_id],
        }];

        ctrl.endSessionState = 'waiting';
        Route.archiveRoutes(payload).then(success).finally(finallyFn);

        function success(response) {
          let notArchivedIds = [];
          if (_.size(response.unarchived_route_ids) > 0) {
            notArchivedIds = response.unarchived_route_ids;

            nvToast.error(
              nvTranslate.instant('container.route-logs.routes-ids-have-pending-waypoints', {
                ids: _.join(notArchivedIds, ', '),
              })
            );

            return;
          }

          nvToast.info(
            nvTranslate.instant('container.route-inbound.inbound-completed-for-route-x', {
              routeId: parentCtrl.inboundSummaryResult.route_id,
            }),
            nvTranslate.instant('container.route-inbound.route-is-archived')
          );

          goToFilterPage();
        }

        function finallyFn() {
          ctrl.endSessionState = 'idle';
        }
      }

      function setToUnresolved() {
        nvDialog.showSingle($event, {
          theme: 'nvRed',
          templateUrl: 'views/container/route-inbound/dialog/end-incomplete-route/end-incomplete-route.dialog.html',
          cssClass: 'route-inbound-end-incomplete-route',
          controller: 'RouteInboundEndIncompleteRouteDialogController',
          controllerAs: 'ctrl',
          locals: {
            localData: {
              routeId: parentCtrl.inboundSummaryResult.route_id,
            },
          },
        }).then(onSet, onCancel);

        function onSet() {
          goToFilterPage();
        }

        function onCancel() {
          focusTrackingInput();
        }
      }
    }

    function routeInboundComments($event) {
      nvDialog.showSingle($event, {
        templateUrl: 'views/container/route-inbound/dialog/comments/comments.dialog.html',
        cssClass: 'route-inbound-comments',
        controller: 'RouteInboundCommentsDialogController',
        controllerAs: 'ctrl',
        locals: {
          localData: {
            routeId: parentCtrl.inboundSummaryResult.route_id,
            comments: parentCtrl.inboundSummaryResult.comments,
          },
        },
      }).then(onSet).finally(finallyFn);

      function onSet() {
        parentCtrl.backdropLoading = true;
        Inbound.getSummaryByRouteId(parentCtrl.inboundSummaryResult.route_id)
          .then(success);

        function success(response) {
          parentCtrl.backdropLoading = false;

          parentCtrl.extendInboundSummaryResult(response);
        }
      }

      function finallyFn() {
        focusTrackingInput();
      }
    }

    function addressVerification($event) {
      nvDialog.showSingle($event, {
        templateUrl: 'views/container/route-inbound/dialog/address-verification/address-verification.dialog.html',
        cssClass: 'route-inbound-address-verification',
        controller: 'RouteInboundAddressVerificationDialogController',
        controllerAs: 'ctrl',
      }).finally(finallyFn);

      function finallyFn() {
        focusTrackingInput();
      }
    }

    function focusTrackingInput() {
      $timeout(() => { $('#tracking-id').focus(); });
    }

    function goToFilterPage() {
      nvNavGuard.unlock();
      $state.go('container.route-inbound.filter');
    }

    function goToRouteManifestPage() {
      $window.open(
        $state.href('container.route-manifest', {
          routeId: parentCtrl.inboundSummaryResult.route_id,
        })
      );
    }

    function goToDriverReportPage() {
      $window.open(
        $state.href('container.driver-reports')
      );
    }

    function goToRelabelStampPage() {
      $window.open(
        $state.href('container.relabel-stamp')
      );
    }

    function goToResultPage() {
      nvNavGuard.unlock();
      $state.go('container.route-inbound.result');
    }

    function playAudio(sources) {
      $scope.audioPlaylist = [];
      _.forEach(_.castArray(sources), (source) => {
        $scope.audioPlaylist.push({
          src: source,
          type: 'audio/mp3',
        });
      });

      $timeout(() => { $scope.mediaPlayer.play(0); });
    }

    function isFullyCompleted() {
      if (ctrl.remainingCashToCollect > 0) {
        return false;
      }

      const scanOrders = filterOutReservations(getAllScannedParcels());
      const allOrders = filterOutReservations(getAllParcelsProcessed());

      const remainingCount = _.size(allOrders) - _.size(scanOrders);
      if (remainingCount > 0) {
        return false;
      }

      return true;

      function filterOutReservations(orders) {
        return _.filter(orders, order =>
          !(order.isReservation || order.isGrabReservation)
        );
      }
    }
  }
}());
