(function controller() {
  angular
    .module('nvOperatorApp.controllers')
    .controller('RouteInboundFilterController', RouteInboundFilterController);

  RouteInboundFilterController.$inject = [
    '$scope', 'nvAutocomplete.Data', 'Inbound', 'nvDateTimeUtils', 'Route',
    '$state', 'nvDialog', 'nvTranslate',
  ];

  function RouteInboundFilterController(
    $scope, nvAutocompleteData, Inbound, nvDateTimeUtils, Route,
    $state, nvDialog, nvTranslate
  ) {
    // variables
    const SUBMIT_STATE = {
      IDLE: 'idle',
      WAITING: 'waiting',
    };

    const SUBMIT_MODE = {
      BY_ROUTE_ID: 'route-id',
      BY_TRACKING_ID: 'tracking-id',
      BY_DRIVER: 'driver',
    };

    const ctrl = this;
    const parentCtrl = $scope.$parent.ctrl;

    let driverToRoutes = {};

    ctrl.parentCtrl = parentCtrl;

    ctrl.form = {};
    ctrl.formRouteId = null;
    ctrl.formRouteIdSubmitState = SUBMIT_STATE.IDLE;
    ctrl.formTrackingId = null;
    ctrl.formTrackingIdSubmitState = SUBMIT_STATE.IDLE;

    ctrl.hubSelection = {
      searchText: '',
      searchInput: null,
      errorHubRequired: false,
      // selected hub use parentCtrl.selectedHub
    };
    ctrl.driverSearch = {
      searchText: '',
      searchInput: nvAutocompleteData.getByHandle('route-inbound-route-search'),
      driver: null,
      submitState: SUBMIT_STATE.IDLE,
    };

    // functions
    ctrl.hubSelectionOnChange = hubSelectionOnChange;
    ctrl.routeIdSubmit = routeIdSubmit;
    ctrl.trackingIdSubmit = trackingIdSubmit;
    ctrl.driverSubmit = driverSubmit;

    // start
    initialize();

    // functions details
    function initialize() {
      // reset inbound summary result
      parentCtrl.inboundSummaryResult = {};
      parentCtrl.inboundSummaryOrdersMap = {};
      parentCtrl.inboundSummaryStampOrdersMap = {};

      // initialize vars
      ctrl.hubSelection.searchInput = nvAutocompleteData.getByHandle('route-inbound-select-hub');
      ctrl.hubSelection.searchInput.setPossibleOptions(parentCtrl.hubsResult);

      ctrl.driverSearch.searchInput.setPossibleOptions(getRouteFetchingList());

      function getRouteFetchingList() {
        const params = {
          from: nvDateTimeUtils.displayDateTime(nvDateTimeUtils.toSOD(new Date()), 'utc'),
          to: nvDateTimeUtils.displayDateTime(nvDateTimeUtils.toEOD(new Date()), 'utc'),
        };
        return Route.fetchRoute(params).then(success);

        function success(response) {
          driverToRoutes = _.groupBy(response, 'driver_name');

          const driversFilterData = [];
          _.forEach(driverToRoutes, (row, driverName) => {
            driversFilterData.push({
              driverName: driverName,
              routeIds: _.map(row, 'route_id'),
            });
          });

          return driversFilterData;
        }
      }
    }

    function hubSelectionOnChange(hub) {
      if (hub) {
        ctrl.hubSelection.errorHubRequired = false;
      }
    }

    function routeIdSubmit($event) {
      if (!isFormValid()) {
        return;
      }

      getSummaryByRouteId($event, ctrl.formRouteId, SUBMIT_MODE.BY_ROUTE_ID);
    }

    function trackingIdSubmit($event) {
      if (!isFormValid()) {
        return;
      }

      ctrl.formTrackingIdSubmitState = SUBMIT_STATE.WAITING;
      Route.getRouteByScan(ctrl.formTrackingId).then(success, failure);

      function success(response) {
        const routeIds = _.uniq(_.compact([
          _.get(response, 'pickup_route_id'),
          _.get(response, 'delivery_route_id'),
        ]));


        if (_.size(routeIds) === 1) {
          getSummaryByRouteId($event, routeIds[0], SUBMIT_MODE.BY_TRACKING_ID);
        } else {
          // show dialog to let user select
          nvDialog.showSingle($event, {
            templateUrl: 'views/container/route-inbound/dialog/choose-route-id/choose-route-id.dialog.html',
            cssClass: 'route-inbound-choose-route-id',
            controller: 'RouteInboundChooseRouteIdDialogController',
            controllerAs: 'ctrl',
            locals: {
              localData: {
                routeIds: routeIds,
              },
            },
          }).then(onFetch);
        }

        function onFetch(theResponse) {
          getSummaryByRouteId($event, theResponse, SUBMIT_MODE.BY_TRACKING_ID);
        }
      }

      function failure() {
        ctrl.formTrackingIdSubmitState = SUBMIT_STATE.IDLE;
      }
    }

    function driverSubmit($event) {
      if (!isFormValid()) {
        return;
      }

      if (_.size(_.get(ctrl.driverSearch.driver, 'routeIds')) === 1) {
        getSummaryByRouteId($event, _.get(ctrl.driverSearch.driver, 'routeIds[0]'), SUBMIT_MODE.BY_DRIVER);
      } else {
        // show dialog to let user select
        ctrl.driverSearch.submitState = SUBMIT_STATE.WAITING;

        nvDialog.showSingle($event, {
          templateUrl: 'views/container/route-inbound/dialog/choose-route-id/choose-route-id.dialog.html',
          cssClass: 'route-inbound-choose-route-id',
          controller: 'RouteInboundChooseRouteIdDialogController',
          controllerAs: 'ctrl',
          locals: {
            localData: {
              routeIds: _.get(ctrl.driverSearch.driver, 'routeIds'),
            },
          },
        }).then(onFetch, () => (ctrl.driverSearch.submitState = SUBMIT_STATE.IDLE));
      }

      function onFetch(response) {
        getSummaryByRouteId($event, response, SUBMIT_MODE.BY_DRIVER);
      }
    }

    function getSummaryByRouteId($event, routeId, submitMode) {
      switch (submitMode) {
        case SUBMIT_MODE.BY_ROUTE_ID:
          ctrl.formRouteIdSubmitState = SUBMIT_STATE.WAITING;
          break;
        case SUBMIT_MODE.BY_TRACKING_ID:
          ctrl.formTrackingIdSubmitState = SUBMIT_STATE.WAITING;
          break;
        case SUBMIT_MODE.BY_DRIVER:
          ctrl.driverSearch.submitState = SUBMIT_STATE.WAITING;
          break;
        default:
      }

      Inbound.getSummaryByRouteId(routeId).then(success, failure);

      function success(response) {
        parentCtrl.extendInboundSummaryResult(response);

        const driverAttendanceEnum = _.get(response, 'driver_attendance');
        promptDriverAttendanceDialog($event, driverAttendanceEnum);
      }

      function failure() {
        switch (submitMode) {
          case SUBMIT_MODE.BY_ROUTE_ID:
            ctrl.formRouteIdSubmitState = SUBMIT_STATE.IDLE;
            break;
          case SUBMIT_MODE.BY_TRACKING_ID:
            ctrl.formTrackingIdSubmitState = SUBMIT_STATE.IDLE;
            break;
          case SUBMIT_MODE.BY_DRIVER:
            ctrl.driverSearch.submitState = SUBMIT_STATE.IDLE;
            break;
          default:
        }
      }
    }

    function promptDriverAttendanceDialog($event, driverAttendanceEnum) {
      if (Route.DRIVER_ATTENDANCE[driverAttendanceEnum] !== Route.DRIVER_ATTENDANCE.RETURNED) {
        // show dialog
        nvDialog.confirmDelete($event, {
          title: nvTranslate.instant('container.route-inbound.driver-attendance'),
          content: nvTranslate.instant('container.route-inbound.driver-attendance-confirmation'),
          ok: nvTranslate.instant('commons.yes'),
          cancel: nvTranslate.instant('commons.cancel'),
        }).then(onUpdate, onCancel);
      } else {
        // no need to prompt, straight go to result page
        goToResultPage();
      }

      function onUpdate() {
        Route.updateRouteDriverAttendance(parentCtrl.inboundSummaryResult.route_id, {
          action: Route.getDriverAttendanceEnum(Route.DRIVER_ATTENDANCE.RETURNED),
        }).then(updateSuccess);
      }

      function onCancel() {
        goToResultPage();
      }

      function updateSuccess() {
        ctrl.formRouteIdSubmitState = SUBMIT_STATE.IDLE;
        ctrl.formTrackingIdSubmitState = SUBMIT_STATE.IDLE;
        ctrl.driverSearch.submitState = SUBMIT_STATE.IDLE;

        goToResultPage();
      }
    }

    function goToResultPage() {
      $state.go('container.route-inbound.result');
    }

    function isFormValid() {
      ctrl.hubSelection.errorHubRequired = !parentCtrl.selectedHub;
      return !ctrl.hubSelection.errorHubRequired;
    }
  }
}());
