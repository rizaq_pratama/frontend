(function controller() {
  angular
    .module('nvOperatorApp.controllers')
    .controller('GlobalSettingsController', GlobalSettingsController);

  const API_STATE = {
    IDLE: 'idle',
    WAITING: 'waiting',
  };

  GlobalSettingsController.$inject = [
    'nvAutocomplete.Data', 'Shippers', 'Inbound', 'Notifications', 'LockedValues',
    'Outbound', '$q', 'nvToast', 'nvDialog', '$scope',
    'nvTranslate', '$timeout',
  ];

  const PARCEL_ROUTER = {
    LOCK: '10',
    UNLOCK: '01',
  };

  function GlobalSettingsController(
    nvAutocompleteData, Shippers, Inbound, Notifications, LockedValues,
    Outbound, $q, nvToast, nvDialog, $scope,
    nvTranslate, $timeout
  ) {
    const ctrl = this;

    let apiToggle = false;

    ctrl.data = {};
    ctrl.view = {};
    ctrl.state = {};
    ctrl.service = {};
    ctrl.function = {};

    ctrl.data.parcelRouter = PARCEL_ROUTER.LOCK;
    ctrl.data.plannedCapacity = 100;
    ctrl.data.outboundPercentage = 70;
    ctrl.data.enableCmiSms = true;
    ctrl.data.enableVanInboundSMS = true;
    ctrl.data.selectedShipper = [];
    ctrl.data.weightTolerance = {
      form: {},
      errors: [{
        key: 'numericOnly',
        msg: 'container.global-settings.numeric-only',
      }],
      onChange: weightToleranceOnChange,
      state: API_STATE.IDLE,
      value: '',
    };
    ctrl.data.maxWeightLimit = {
      form: {},
      errors: [{
        key: 'numericOnly',
        msg: 'container.global-settings.numeric-only',
      }],
      onChange: maxWeightLimitOnChange,
      state: API_STATE.IDLE,
      value: '',
    };

    ctrl.view.setAside = {};
    ctrl.view.outboundPercentage = {};
    ctrl.view.smsSettings = {};
    ctrl.view.setAside.updateDisabled = false;
    ctrl.view.setAside.recalcDisabled = false;
    ctrl.view.setAside.clearDisabled = false;
    ctrl.view.outboundPercentage.updateDisabled = false;
    ctrl.view.smsSettings.updateDisabled = false;
    ctrl.view.smsSettings.searchText = '';

    ctrl.state.setAside = {};
    ctrl.state.outboundPercentage = {};
    ctrl.state.smsSettings = {};
    ctrl.state.setAside.update = API_STATE.IDLE;
    ctrl.state.setAside.recalc = API_STATE.IDLE;
    ctrl.state.setAside.clear = API_STATE.IDLE;
    ctrl.state.outboundPercentage.update = API_STATE.IDLE;
    ctrl.state.smsSettings.update = API_STATE.IDLE;
    ctrl.state.parcelRouter = 0;

    ctrl.service.subscribedShippers = null;

    ctrl.function.init = init;
    ctrl.function.onRemoveShipper = onRemoveShipper;
    ctrl.function.updateOutboundLimit = updateOutboundLimit;
    ctrl.function.updateInboundLimit = updateInboundLimit;
    ctrl.function.getInboundCounter = getInboundCounter;
    ctrl.function.clearSetAsideData = clearSetAsideData;
    ctrl.function.updateSmsNotificationSettings = updateSmsNotificationSettings;
    ctrl.function.updateWeightTolerance = updateWeightTolerance;
    ctrl.function.updateMaxWeightLimit = updateMaxWeightLimit;
    ctrl.function.noop = angular.noop;

    function init() {
      ctrl.service.subscribedShippers = nvAutocompleteData.getByHandle('global-settings-shipper');
      ctrl.service.subscribedShippers.setPossibleOptions(getShippers);
      ctrl.service.subscribedShippers.setSelectedOptions(ctrl.data.selectedShipper);

      return $q.all([
        LockedValues.read(),
        Notifications.getSmsSettings(),
        Inbound.getLimit(),
        Outbound.getLimit(),
      ]).then(onSuccess);

      function onSuccess(responses) {
        const lockedValues = responses[0];
        const smsSettings = responses[1];
        const inboundLimit = responses[2];
        const outboundLimit = responses[3];

        // locked values
        const parcelRouterLock = _.find(lockedValues, val => val.key === 'parcelrouter_lock');
        ctrl.data.parcelRouter = parcelRouterLock.value === '1' ? PARCEL_ROUTER.LOCK : PARCEL_ROUTER.UNLOCK;
        $scope.$watch('ctrl.data.parcelRouter', executeParcelRouter);

        const weightToleranceObject = _.find(lockedValues, val =>
          val.key === LockedValues.COLUMN_KEY.INBOUND_WEIGHT_TOLERANCE
        );
        const maxWeightLimitObject = _.find(lockedValues, val =>
          val.key === LockedValues.COLUMN_KEY.INBOUND_MAX_WEIGHT
        );
        ctrl.data.weightTolerance.value = weightToleranceObject ? weightToleranceObject.value : '';
        ctrl.data.maxWeightLimit.value = maxWeightLimitObject ? maxWeightLimitObject.value : '';

        // sms part
        ctrl.data.enableCmiSms = smsSettings.allow_cmi || false;
        ctrl.data.enableVanInboundSMS = smsSettings.allow_van_inbound || false;

        // shippers part
        _.forEach(smsSettings.shipper_ids, (shipperId) => {
          ctrl.data.selectedShipper.push({
            id: shipperId,
            displayName: shipperId,
          });
        });
        processSelectedShippers();

        // inbound limit
        ctrl.data.plannedCapacity = inboundLimit || 0;

        // outbound limit
        ctrl.data.outboundPercentage = outboundLimit.value;
      }

      function getShippers(text) {
        return Shippers.filterSearch(text, this.getSelectedOptions());
      }
    }

    function onRemoveShipper(shipper) {
      ctrl.service.subscribedShippers.remove(shipper);
    }

    function updateOutboundLimit() {
      ctrl.state.outboundPercentage.update = API_STATE.WAITING;

      const limitInt = Number.parseInt(ctrl.data.outboundPercentage, 10);
      Outbound.updateLimit(limitInt).then((data) => {
        ctrl.data.outboundPercentage = data.value;
        ctrl.state.outboundPercentage.update = API_STATE.IDLE;
      }, () => {
        ctrl.state.outboundPercentage.update = API_STATE.IDLE;
      });
    }

    function updateInboundLimit() {
      ctrl.state.setAside.update = API_STATE.WAITING;
      const limitInt = Number.parseInt(ctrl.data.plannedCapacity, 10);
      Inbound.updateLimit(limitInt).then((data) => {
        ctrl.data.plannedCapacity = data;
        ctrl.state.setAside.update = API_STATE.IDLE;
      }, () => {
        ctrl.state.setAside.update = API_STATE.IDLE;
      });
    }

    function getInboundCounter() {
      ctrl.state.setAside.recalc = API_STATE.WAITING;
      Inbound.counter().then((data) => {
        nvToast.success(`Recalculation complete: ${data}`);
        ctrl.state.setAside.recalc = API_STATE.IDLE;
      }, () => {
        ctrl.state.setAside.recalc = API_STATE.IDLE;
      });
    }

    function clearSetAsideData() {
      nvDialog.confirmDelete(null,
        { title: 'Confirmation',
          content: 'Are you sure want to clear the set aside tables?',
          ok: 'ok',
          cancel: 'cancel',
        }).then(() => {
          ctrl.state.setAside.clear = API_STATE.WAITING;
          Inbound.clearSetAside().then((data) => {
            if (data === 'ok') {
              nvToast.success('set aside clearance performed successfully');
            } else {
              nvToast.error('set aside clearance error');
            }
            ctrl.state.setAside.clear = API_STATE.IDLE;
          }, () => {
            ctrl.state.setAside.clear = API_STATE.IDLE;
          });
        });
    }

    function updateSmsNotificationSettings() {
      ctrl.state.smsSettings.update = API_STATE.WAITING;
      const selectedShippers = ctrl.service.subscribedShippers.getSelectedOptions();
      Notifications.updateSmsSettings({
        allow_cmi: ctrl.data.enableCmiSms,
        allow_van_inbound: ctrl.data.enableVanInboundSMS,
        shipper_ids: _.map(selectedShippers, shipper => shipper.id),
      }).then((data) => {
        ctrl.data.enableCmiSms = data.allow_cmi;
        ctrl.data.enableVanInboundSMS = data.allow_van_inbound;

        ctrl.state.smsSettings.update = API_STATE.IDLE;
      }, () => {
        ctrl.state.smsSettings.update = API_STATE.IDLE;
      });
    }

    function updateWeightTolerance() {
      ctrl.data.weightTolerance.state = API_STATE.WAITING;

      LockedValues.update({
        key: LockedValues.COLUMN_KEY.INBOUND_WEIGHT_TOLERANCE,
        value: ctrl.data.weightTolerance.value,
      }).then(success).finally(finallyFn);

      function success() {
        nvToast.success(nvTranslate.instant('commons.updated'));
      }

      function finallyFn() {
        ctrl.data.weightTolerance.state = API_STATE.IDLE;
      }
    }

    function weightToleranceOnChange() {
      $timeout(() => {
        if (ctrl.data.weightTolerance.value !== '' &&
          _.isNaN(_.toNumber(ctrl.data.weightTolerance.value))
        ) {
          ctrl.data.weightTolerance.form.inputWeightTolerance.$setValidity('numericOnly', false);
          return;
        }

        ctrl.data.weightTolerance.form.inputWeightTolerance.$setValidity('numericOnly', true);
      });
    }

    function updateMaxWeightLimit() {
      ctrl.data.maxWeightLimit.state = API_STATE.WAITING;

      LockedValues.update({
        key: LockedValues.COLUMN_KEY.INBOUND_MAX_WEIGHT,
        value: ctrl.data.maxWeightLimit.value,
      }).then(success).finally(finallyFn);

      function success() {
        nvToast.success(nvTranslate.instant('commons.updated'));
      }

      function finallyFn() {
        ctrl.data.maxWeightLimit.state = API_STATE.IDLE;
      }
    }

    function maxWeightLimitOnChange() {
      $timeout(() => {
        if (ctrl.data.maxWeightLimit.value !== '' &&
          _.isNaN(_.toNumber(ctrl.data.maxWeightLimit.value))
        ) {
          ctrl.data.maxWeightLimit.form.inputMaxWeightLimit.$setValidity('numericOnly', false);
          return;
        }

        ctrl.data.maxWeightLimit.form.inputMaxWeightLimit.$setValidity('numericOnly', true);
      });
    }

    function executeParcelRouter(val, old) {
      if (val !== old) {
        if (!apiToggle) {
          ctrl.state.parcelRouter = 1;
          LockedValues.toggle('parcelrouter_lock').then((data) => {
            if (data === 'ok') {
              LockedValues.read().then((datas) => {
                const parcelRouterLock = _.find(datas, v => v.key === 'parcelrouter_lock');
                ctrl.data.parcelRouter = parcelRouterLock.value === '1' ? PARCEL_ROUTER.LOCK : PARCEL_ROUTER.UNLOCK;
                ctrl.state.parcelRouter = 0;
                if (ctrl.data.parcelRouter === val) {
                  apiToggle = false;
                } else {
                  apiToggle = true;
                }
              }, () => {
                ctrl.data.parcelRouter = old;
                ctrl.state.parcelRouter = 0;
              });
            } else {
              ctrl.data.parcelRouter = old;
              ctrl.state.parcelRouter = 0;
            }
          }, () => {
            ctrl.data.parcelRouter = old;
            ctrl.state.parcelRouter = 0;
          });
        } else {
          apiToggle = false;
        }
      }
    }

    function processSelectedShippers() {
      Shippers.elasticCompactSearch({
        legacy_ids: _.map(ctrl.data.selectedShipper, 'id'),
        size: _.size(ctrl.data.selectedShipper),
      }).then(readCompactSuccess);

      function readCompactSuccess(response) {
        const shippers = _.get(response, 'details');

        _.forEach(ctrl.data.selectedShipper, (shipper) => {
          const theShipper = _.find(shippers, ['id', shipper.id]);

          if (theShipper) {
            shipper.displayName = `${theShipper.id}-${theShipper.name}`;
          }
        });
      }
    }
  }
}());
