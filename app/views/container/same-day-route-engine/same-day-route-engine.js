(function controller() {
  angular
    .module('nvOperatorApp.controllers')
    .controller('SameDayRouteEngineController', Controller);
  /* eslint no-underscore-dangle: ["error",
    { "allow": ["_driverId","_operatingHours","_driverSearch","_numTransactions"] }]*/
  Controller.$inject = [
    '$q',
    '$rootScope',
    '$timeout',
    '$translate',
    '$window',
    'nvAutocomplete.Data',
    'nvDateTimeUtils',
    '$state',
    'nvToast',
    'Driver',
    'Hub',
    'Route',
    'RouteGroup',
    'Tag',
    'Transaction',
    'nvTranslate',
    'nvDialog',
    'nvTimezone',
    'nvZonalRouting',
  ];

  function Controller(
    $q,
    $rootScope,
    $timeout,
    $translate,
    $window,
    nvAutocompleteData,
    nvDateTimeUtils,
    $state,
    nvToast,
    Driver,
    Hub,
    Route,
    RouteGroup,
    Tag,
    Transaction,
    nvTranslate,
    nvDialog,
    nvTimezone,
    nvZonalRouting
  ) {
    const ctrl = this;

    const DEFAULT_TAG_IDS = [];
    const TIME_SLOT_OPTIONS = [
      { displayName: '', value: null },
      { displayName: '0000 hrs', value: '0000' },
      { displayName: '0100 hrs', value: '0100' },
      { displayName: '0200 hrs', value: '0200' },
      { displayName: '0300 hrs', value: '0300' },
      { displayName: '0400 hrs', value: '0400' },
      { displayName: '0500 hrs', value: '0500' },
      { displayName: '0600 hrs', value: '0600' },
      { displayName: '0700 hrs', value: '0700' },
      { displayName: '0800 hrs', value: '0800' },
      { displayName: '0900 hrs', value: '0900' },
      { displayName: '1000 hrs', value: '1000' },
      { displayName: '1100 hrs', value: '1100' },
      { displayName: '1200 hrs', value: '1200' },
      { displayName: '1300 hrs', value: '1300' },
      { displayName: '1400 hrs', value: '1400' },
      { displayName: '1500 hrs', value: '1500' },
      { displayName: '1600 hrs', value: '1600' },
      { displayName: '1700 hrs', value: '1700' },
      { displayName: '1800 hrs', value: '1800' },
      { displayName: '1900 hrs', value: '1900' },
      { displayName: '2000 hrs', value: '2000' },
      { displayName: '2100 hrs', value: '2100' },
      { displayName: '2200 hrs', value: '2200' },
      { displayName: '2300 hrs', value: '2300' },
    ];

    let rgById = {}; // route groups by id
    let drById = {}; // drivers by id
    let hbById = {}; // hubs by id

    let vehicles = [];
    const vehiclesMap = {};


    ctrl.promise = null;
    ctrl.unroutedTxnCount = 0;
    ctrl.totalTxnCount = 0;
    ctrl.overrideValue = 10;
    ctrl.openRouteDetail = openRouteDetail;
    ctrl.viewUnroutedTransactions = viewUnroutedTransactions;
    ctrl.onChangeCheckboxOverride = onChangeCheckboxOverride;


    // todo refactor this to use ui-router
    ctrl.PAGES = { FILTER: 'filter', RESULT: 'result' };
    ctrl.currentPage = ctrl.PAGES.FILTER;

    // Filter page
    ctrl.routeGroupsSelection = {
      value: null,
      options: [],
    };
    ctrl.hubsSelection = {
      value: null,
      options: [],
    };
    ctrl.routingAlgoritmSelection = {
      value: null,
      options: [
        {
          displayName: nvTranslate.instant('container.same-day-route-engine.bulky-pickup-delivery'),
          value: 'BULKY_PICKUP_DELIVERY',
        },
        {
          displayName: nvTranslate.instant('container.same-day-route-engine.same-day-pickup-delivery'),
          value: 'SAME_DAY_PICKUP_DELIVERY',
        },
      ],
    };

    ctrl.vehicleTypes = [];
    ctrl.runState = 'idle';

    // Result page
    ctrl.routeResponse = {};
    ctrl.isAbleToCreateRoute = false;
    ctrl.routesResponse = [];
    ctrl.createRouteState = 'idle';
    ctrl.unroutedWps = [];
    ctrl.breakValidity = [];

    ctrl.goToPage = goToPage;
    ctrl.isPage = isPage;
    ctrl.getSelectedRouteGroup = getSelectedRouteGroup;
    ctrl.onRouteGroupSelectionChange = onRouteGroupSelectionChange;
    ctrl.getSelectedHub = getSelectedHub;

    // Filter page
    ctrl.getAll = getAll;
    ctrl.onAddMoreVehicleType = onAddMoreVehicleType;
    ctrl.onRemoveVehicleType = onRemoveVehicleType;
    ctrl.updateEndTimeOptions = updateEndTimeOptions;
    ctrl.updateStartTimeOptions = updateStartTimeOptions;
    ctrl.updateStartBreakTimeOptions = updateStartBreakTimeOptions;
    ctrl.updateEndBreakTimeOptions = updateEndBreakTimeOptions;
    ctrl.run = run;
    ctrl.openEditRoutePage = openEditRoutePage;
    ctrl.isRunDisabled = isRunDisabled;

    // Result page
    ctrl.getTotalTxnCount = getTotalTxnCount;
    ctrl.getTotalRouteCount = getTotalRouteCount;
    ctrl.removeFromRouteList = removeFromRouteList;
    ctrl.createRoutes = createRoutes;
    ctrl.getNonRemovedRoutes = getNonRemovedRoutes;
    ctrl.validateResultForm = validateResultForm;
    ctrl.setDriverChange = setDriverChange;

    function getAll() {
      return $q
        .all([
          RouteGroup.read(),
          Driver.searchAll(),
          Hub.read({ archived: false }),
          Tag.all()
        ])
        .then(success, $q.reject);

      function success(response) {
        onAddMoreVehicleType(ctrl.vehicleTypes);

        const routeGroups = _.sortBy(response[0].routeGroups, 'name');
        rgById = _.keyBy(routeGroups, routeGroup => routeGroup.id);
        drById = _.keyBy(response[1].drivers, driver => driver.id);
        hbById = _.keyBy(response[2], hub => hub.id);

        ctrl.routeGroupsSelection.options = _.orderBy(RouteGroup.toOptions(routeGroups),
                                            [rg => rg.displayName.toLowerCase()],
                                            ['asc']);
        ctrl.driversSelectionOptions = Driver.toOptions(response[1]);
        ctrl.hubsSelection.options = _.orderBy(Hub.toOptions(response[2]),
                                      [hub => hub.displayName.toLowerCase()],
                                      ['asc']);

        DEFAULT_TAG_IDS.push(Tag.getTagId('SDD', response[3].tags));
      }
    }

    function onChangeCheckboxOverride() {
      $timeout(() => {
        ctrl.overrideValue = 10;
      }, 0);
    }

    function onAddMoreVehicleType(values) {
      const defaultVehicleType = {
        numVehicles: 1,
        parcelLimit: 10,
        timeslot: {
          start: '0900',
          end: '1500',
        },
        break: {
          start: null,
          end: null,
        },
      };
      defaultVehicleType.startTimeOptions = filterTimeOptions(defaultVehicleType, false);
      defaultVehicleType.endTimeOptions = filterTimeOptions(defaultVehicleType, true);
      defaultVehicleType.startBreakTimeOptions = filterBreakTimeOptions(defaultVehicleType, false);
      defaultVehicleType.endBreakTimeOptions = filterBreakTimeOptions(defaultVehicleType, true);
      values.push(defaultVehicleType);
    }

    function onRemoveVehicleType(values, idx) {
      values.splice(idx, 1);
    }

    function run() {
      const payload = getOptimizeRouteGroupData();
      vehicles = angular.copy(payload.vehicles);
      payload.problem_type = ctrl.routingAlgoritmSelection.value;
      _.forEach(vehicles, (vehicle) => {
        vehiclesMap[vehicle.id] = vehicle;
      });

      executeAndWait(ctrl.routeGroupsSelection.value, payload).then(success, reject);
      ctrl.runState = 'waiting';

      function reject(reason) {
        nvToast.warning(reason);
        ctrl.runState = 'idle';
        return $q.reject(reason);
      }

      function success(response) {
        _.defaultsDeep(response.job_solution, {
          solution: {
            routes: [],
            unassignedJobs: {},
          },
        });
        ctrl.runState = 'idle';
        ctrl.routeResponse = response.job_solution;
        ctrl.routeResponse.solution.routes = extendRoutesDefaultValue(
          ctrl.routeResponse.solution.routes
        );

        // no solution found
        if (ctrl.routeResponse.solution.routes.length === 0) {
          nvToast.warning($translate.instant('container.same-day-route-engine.no-solutions-found-hint'),
            $translate.instant('container.same-day-route-engine.no-solutions-found'));
          return;
        }

        //get the unrouted wps
        ctrl.unroutedWps = getUnroutedWps(ctrl.routeResponse.solution.unassigned_jobs);
        goToPage(ctrl.PAGES.RESULT);
      }

      function getUnroutedWps(unassignedJobs){
        let wps = [];
        wps = _.map(unassignedJobs.services, svc => {
          return svc;
        });

        _.each(unassignedJobs.shipments, shipment => {
          wps.push({
            capacity: shipment.capacity,
            id: shipment.id,
            waypoint: shipment.delivery,
            skills: shipment.skills,
            job_type: 'DELIVERY',
          });
          wps.push({
            capacity: shipment.capacity,
            id: shipment.id,
            waypoint: shipment.pickup,
            skills: shipment.skills,
            job_type: 'PICKUP',
          });
        });
        return wps;
      }
    }

    function executeAndWait(id, payload) {
      const status = 'PENDING';
      return RouteGroup.optimizeAsync(id, payload).then(
        success
      );

      function success(result) {
        const jobId = result.job_id;
        return pool(jobId);
      }

      function pool(jobId) {
        return RouteGroup.poolSolution(jobId)
          .then(successAsync);
      }

      function successAsync(result) {
        const jobId = result.job_id;
        if (result.status === 'PENDING') {
          return $timeout(() => (pool(jobId)), 3000);
        }
        if (result.status === 'ERROR') {
          return $q.reject(result.job_solution);
        }
        return result;
      }
    }

    function createRoutes() {
      const routesJsonData = [];
      _.forEach(getNonRemovedRoutes(), (route) => {
        const routeJsonData = {
          hubId: ctrl.hubsSelection.value,
          driverId: route._driverId,
          vehicleId: getVehicleId(route._driverId),
          zoneId: nvZonalRouting.getAllZonesId(),
          date: nvDateTimeUtils.displayDateTime(new Date()),
          tags: DEFAULT_TAG_IDS,
          waypoints: [],
        };

        _.forEach(route.activities, (activity) => {
          if(!_.includes(activity.id, 'break')){
            routeJsonData.waypoints.push(parseInt(activity.id, 10));
          }
        });

        routesJsonData.push(routeJsonData);

        function getVehicleId(driverId) {
          if (drById[driverId] && drById[driverId].vehicles &&
            drById[driverId].vehicles.length > 0) {
            return drById[driverId].vehicles[0].id;
          }

          return null;
        }
      });

      ctrl.createRouteState = 'waiting';
      Route.createRoutes(routesJsonData).then(success, reject);

      function success(response) {
        ctrl.createRouteState = 'idle';
        nvToast.success($translate.instant('container.same-day-route-engine.success.create-route', { count: response.length }));
        resetFilterPage();
        ctrl.routesResponse = extendRoutesResponse(response);
      }

      function reject(reason) {
        ctrl.createRouteState = 'idle';
        return $q.reject(reason);
      }
    }

    function getNonRemovedRoutes() {
      return ctrl.routeResponse.solution.routes;
    }

    function removeFromRouteList(idx, route) {
      $timeout(() => {
        if (getNonRemovedRoutes().length > 1) {
          ctrl.routeResponse.solution.routes.splice(idx, 1);
          nvToast.error($translate.instant('container.same-day-route-engine.status.vehicle-removed', { vehicleId: route.vehicle_id }));
          validateResultForm();
        } else {
          nvToast.warning($translate.instant('container.same-day-route-engine.error.cannot-remove-last-route'));
        }
      });
    }

    function validateResultForm() {
      $timeout(() => {
        let isAbleToCreateRoute = true;

        _.forEach(ctrl.routeResponse.solution.routes, (route) => {
          if (route._driverId === null) {
            isAbleToCreateRoute = false;
            return false;
          }
        });

        ctrl.isAbleToCreateRoute = isAbleToCreateRoute;
      });
    }

    function isPage(page) {
      return ctrl.currentPage === page;
    }

    function goToPage(page) {
      ctrl.currentPage = page;

      if (ctrl.currentPage === ctrl.PAGES.RESULT) {
        validateResultForm();
      } else {
        $timeout(() => {
          ctrl.modelForm.$setDirty();
        });
      }
    }

    function getSelectedRouteGroup() {
      return getRouteGroupData(ctrl.routeGroupsSelection.value);
    }

    function getSelectedHub() {
      return getHubData(ctrl.hubsSelection.value);
    }

    function onRouteGroupSelectionChange() {
      $timeout(() => {
        ctrl.promise = setUnroutedTxnCount(rgById[ctrl.routeGroupsSelection.value]);
      });
    }

    function setUnroutedTxnCount(routeGroup) {
      return !routeGroup
        ? $q.when(0)
        : Transaction.searchByIds(_.uniq(routeGroup.transactionIds)).then(success, $q.reject);

      function success(txns) {
        ctrl.totalTxnCount = txns.length;
        ctrl.unroutedTxnCount = _.filter(txns, txn => txn.waypointId !== null && txn.routeId === null).length;
        return ctrl.unroutedTxnCount;
      }
    }

    function getTotalTxnCount() {
      let txnCount = 0;
      _.forEach(getNonRemovedRoutes(), (route) => {
        txnCount += route.activities.length;
      });

      return txnCount;
    }

    function getTotalRouteCount() {
      return getNonRemovedRoutes().length;
    }

    function setDriverChange(route, selectedDriver) {
      if (selectedDriver && selectedDriver.length > 0) {
        route._driverId = selectedDriver[0].value;
      } else {
        route._driverId = null;
      }

      validateResultForm();
    }

    function openEditRoutePage() {
      const params = {
        ids: _.join(_.map(ctrl.routesResponse, 'id'), ','),
        unrouted: false,
        cluster: false,
      };

      $window.open(
        $state.href('container.zonal-routing-improved.edit', params)
      );
    }

    function isRunDisabled() {
      return ctrl.unroutedTxnCount === 0;
    }


    function updateEndTimeOptions(vehicleType,index) {
      $timeout(() => {
        vehicleType.endTimeOptions = filterTimeOptions(vehicleType, true);
        updateEndBreakTimeOptions(vehicleType,index);
      });
    }

    function updateStartTimeOptions(vehicleType, index) {
      $timeout(() => {
        vehicleType.startTimeOptions = filterTimeOptions(vehicleType, false);
        updateStartBreakTimeOptions(vehicleType,index);
      });
    }

    function checkBreakValidity(){
      let valid = true;
      _.each(ctrl.breakValidity, (fleet) =>{
        let start = _.get(fleet, 'start', true);
        let end = _.get(fleet, 'end', true);
        valid = valid && start && end;
      });
      ctrl.isBreakTimeInvalid = !valid;
    }

    function updateEndBreakTimeOptions(vehicleType, index) {
      $timeout(() => {
        vehicleType.endBreakTimeOptions = filterBreakTimeOptions(vehicleType, true);
        //check the startBreakTime against startTime
        if(vehicleType.break.start && vehicleType.break.start < vehicleType.timeslot.start){
          ctrl.modelForm[`break-start-${index}`].$setValidity("invalid", false);
          ctrl.breakValidity[index]= _.merge(ctrl.breakValidity[index],{start : false});           
        }else{
          ctrl.modelForm[`break-start-${index}`].$setValidity("invalid", true);
          ctrl.breakValidity[index]= _.merge(ctrl.breakValidity[index],{start : true})          
        }
        checkBreakValidity();
      });
    }

    function updateStartBreakTimeOptions(vehicleType, index) {
      $timeout(() => {
        vehicleType.startBreakTimeOptions = filterBreakTimeOptions(vehicleType, false);

        if(vehicleType.break.end && vehicleType.break.end > vehicleType.timeslot.end){
          ctrl.modelForm[`break-end-${index}`].$setValidity("invalid", false);
          ctrl.breakValidity[index]= _.merge(ctrl.breakValidity[index],{end : false});           
        }else{
          ctrl.modelForm[`break-end-${index}`].$setValidity("invalid", true);
          ctrl.breakValidity[index]= _.merge(ctrl.breakValidity[index],{end : true})          
        }
        checkBreakValidity();
      });
    }

    function filterTimeOptions(vehicleType, isUpdatingEnd) {
      return _.filter(TIME_SLOT_OPTIONS, (option) => {
        if (isUpdatingEnd) {
          return option.value > vehicleType.timeslot.start;
        }
        return option.value < vehicleType.timeslot.end;
      });
    }

    function filterBreakTimeOptions(vehicleType, isUpdatingEnd) {

      return _.filter(TIME_SLOT_OPTIONS, (option) => {
        if (isUpdatingEnd) {
          return option.value > vehicleType.break.start || vehicleType.break.start === null;
        }
        return option.value < vehicleType.break.end || vehicleType.break.end === null;
      });
    }

    function extendRoutesDefaultValue(routes) {
      return _.map(routes, (route) => {
        processRoute(route);
        return route;
      });

      function processRoute(route) {
        route.activities = route.activities || [];
        route._operatingHours = getOperatingHoursText(route.vehicle_id);
        route._driverId = null;
        route._driverSearch = nvAutocompleteData.getByHandle(route.vehicle_id);
        route._driverSearch.setPossibleOptions(angular.copy(ctrl.driversSelectionOptions));
      }

      function getOperatingHoursText(vehicleId) {
        if (vehiclesMap[vehicleId]) {
          const startMoment = moment.utc(vehiclesMap[vehicleId].start, 'HHmm');
          const endMoment = moment.utc(vehiclesMap[vehicleId].end, 'HHmm');
          const hour = moment.duration(endMoment.diff(startMoment)).asHours();

          return `${nvDateTimeUtils.displayInternationalTime(startMoment)} - ${
            nvDateTimeUtils.displayInternationalTime(endMoment)
            } hrs (${hour} hours)`;
        }

        return '-';
      }
    }

    function extendRoutesResponse(routes) {
      const returnedRoutes = angular.copy(routes);
      _.forEach(getNonRemovedRoutes(), (route, idx) => {
        returnedRoutes[idx].createdAt = nvDateTimeUtils.displayDateTime(nvDateTimeUtils.toMoment(returnedRoutes[idx].createdAt, 'utc'));
        returnedRoutes[idx]._operatingHours = route._operatingHours;
        returnedRoutes[idx]._numTransactions = route.activities.length;
      });
      return returnedRoutes;
    }

    function getRouteGroupData(id) {
      return rgById[id];
    }

    function getHubData(id) {
      return hbById[id];
    }

    function getOptimizeRouteGroupData() {
      const vehiclesData = [];
      const vehicleTypePrefix = 'Fleet-Type-';
      const breakPrefix = 'break';
      const startLocation = getHubLocation(ctrl.hubsSelection.value);

      const vehicleTypesData = _.map(ctrl.vehicleTypes, (vehicleType, idx) => {
        return {
          type_id: vehicleTypePrefix + (idx + 1),
          capacity: [vehicleType.parcelLimit],
        };
      });
      _.forEach(ctrl.vehicleTypes, (vehicleType, idx) => {
        for (let i = 0; i < vehicleType.numVehicles; i++) {
          vehiclesData.push({
            id: `${vehicleTypePrefix + (idx + 1)}-${i + 1}`,
            vehicle_type_id: vehicleTypePrefix + (idx + 1),
            start_location: startLocation,
            start: timeToServerFormat(vehicleType.timeslot.start),
            end: timeToServerFormat(vehicleType.timeslot.end),
            vehicle_break: constructBreaks(vehicleType, `${vehicleTypePrefix + (idx + 1)}-${i + 1}`),
            return_to_depot : false,
          });
        }
      });

      const obj = {
        vehicle_types: vehicleTypesData,
        vehicles: vehiclesData,
      };

      if (ctrl.isOverride) {
        obj.routing_params = {
          delivery_service_time: ctrl.overrideValue * 60,
        };
      }

      return obj;

      function timeToServerFormat(time) {
        const standardTime = moment.tz(time, 'HHmm', nvTimezone.getOperatorTimezone());
        return nvDateTimeUtils.displayTime(standardTime, 'utc');
      }

      function getHubLocation(hubId) {
        return {
          latitude: hbById[hubId].latitude,
          longitude: hbById[hubId].longitude,
        };
      }

      function constructBreaks(vehicleType, vehicleId) {
        const dateNowString = nvDateTimeUtils.displayDate(moment());
        const id = `${breakPrefix}-${ctrl.getSelectedRouteGroup().name}-${vehicleId}-${dateNowString}`;
        if (vehicleType.break.start === null
        || vehicleType.break.end === null) {
          return null;
        }

        return {
          id: id,
          waypoint: {
            time_windows: [
              {
                start: timeToServerFormat(vehicleType.break.start),
                end: timeToServerFormat(vehicleType.break.start),
              },
            ],
            service_time: calculateServiceTime(vehicleType.break.start,
                                  vehicleType.break.end
                                 ),
            id: `break${id}`,
            location: {
              id: 'break_location',
            },
          },
        };

        function calculateServiceTime(start, end) {
          return ((end - start) / 100) * 60 * 60;
        }
      }
    }

    function createRandomId() {
      return (`${Math.random().toString(36)}00000000000000000`).slice(2, 8 + 2);
    }


    function openRouteDetail($event, route){
      return nvDialog.showSingle($event, {
        templateUrl:'views/container/same-day-route-engine/dialog/waypoint-detail-dialog.html',
        controller:'SDREWaypointDetailDialogController',
        controllerAs: 'ctrl',
        cssClass:'nv-route-detail-dialog',
        theme:'nvBlue',
        locals: {
          routes: route.activities,
          routeName: route.vehicle_id,
          problem_type : ctrl.routingAlgoritmSelection.value,
        }
      });
    }

    function viewUnroutedTransactions($event){
      let unroutedWps = ctrl.routeResponse.solution.unassigned_jobs;
      let servicesWpIds = _.map(unroutedWps.services, (svc)=>{
        return svc.waypoint.id;
      });

      let shipmentsWpIds = _.map(unroutedWps.shipments, (spmnt)=>{
        if(spmnt.delivery){
          return spmnt.delivery.id;
        }
        if(spmnt.pickup){
          return spmnt.pickup.id;
        }
      })
      
      let wpIds= _.concat(servicesWpIds, shipmentsWpIds);

      return nvDialog.showSingle($event,{
        templateUrl: 'views/container/same-day-route-engine/dialog/unrouted-waypoint-detail-dialog.html',
        controller: 'SDREUnroutedWaypointDetailDialogControler',
        controllerAs: 'ctrl',
        cssClass: 'nv-unrouted-detail-dialog',
        theme: 'nvBlue',
        locals: {
          wpIDs: wpIds
        }
      });
    }

    function resetFilterPage() {
      RouteGroup.read().then(success);

      function success(response) {
        ctrl.routeGroupsSelection.value = null;
        ctrl.vehicleTypes = [];
        onAddMoreVehicleType(ctrl.vehicleTypes);

        const routeGroups = _.sortBy(response.routeGroups, 'name');
        rgById = _.keyBy(routeGroups, routeGroup => routeGroup.id);
        ctrl.routeGroupsSelection.options = RouteGroup.toOptions(routeGroups);
        goToPage(ctrl.PAGES.FILTER);
      }
    }
  }
}());
