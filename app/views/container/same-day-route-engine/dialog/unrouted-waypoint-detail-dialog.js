(function controller() {
  angular
    .module('nvOperatorApp.controllers')
    .controller('SDREUnroutedWaypointDetailDialogControler', Controller);

  Controller.$inject = [
    'Route',
    'RouteGroup',
    'nvTable',
    'Waypoint',
    'wpIDs',
    'nvDateTimeUtils',
    '$mdDialog',
  ];

  function Controller(
    Route,
    RouteGroup,
    nvTable,
    Waypoint,
    wpIDs,
    nvDateTimeUtils,
    $mdDialog
  ) {
    const ctrl = this;
    ctrl.wpData = [];
    ctrl.wpIDs = angular.copy(wpIDs);
    ctrl.onCancel = onCancel;


    ctrl.tableFields = {
      seq: {
        displayName : "container.same-day-route-engine.seq",
      },
      wpId: {
        displayName : "container.same-day-route-engine.wp-id",
      },
      type: {
        displayName : "container.same-day-route-engine.service-type",
      },
      shipper: {
        displayName : "commons.shipper",
      },
      address: {
        displayName : "commons.address", 
      },
      tracking_id: {
        displayName: "commons.tracking-id",
      },
      originalStart: {
        displayName: "container.same-day-route-engine.original-start",
      },
      originalEnd: {
        displayName: "container.same-day-route-engine.original-end",
      }
    };

    ctrl.tableParam = {};
    init();


    function onCancel(){
      return $mdDialog.hide();
    }
    function init(){
      ctrl.tableParam = nvTable.createTable(ctrl.tableFields);
      return Waypoint.search(ctrl.wpIDs)
        .then(success);

      function success(result){
        let data = _.map(wpIDs, (wpId, key)=>{
          let wp = {
            seq: key+1,
            id: wpId,
          };
          let parsedId= Number(wp.id);
          if(parsedId){
            _.merge(wp, _.find(result, {'id': parsedId}));
            //reformat
            if(wp.start_time){
              wp.originalStart = wp.start_time;
            }
            if(wp.end_time){
              wp.originalEnd = wp.end_time;
            }
          }
          return wp;
        });
        ctrl.tableParam.setData(data);
        ctrl.wpData = data;

      }
    }
    
  }
}());
