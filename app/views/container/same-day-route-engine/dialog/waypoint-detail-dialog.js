(function controller() {
  angular
    .module('nvOperatorApp.controllers')
    .controller('SDREWaypointDetailDialogController', Controller);
  /* eslint no-underscore-dangle: ["error",
    { "allow": ["_suggestedStart","_suggestedEnd","_originalStart","_originalEnd"] }]*/
  Controller.$inject = [
    'Route',
    'RouteGroup',
    'nvTable',
    'Waypoint',
    'routes',
    'routeName',
    'problem_type',
    'nvDateTimeUtils',
    '$mdDialog',
    'nvTranslate',
    '$filter',
    'nvTimezone',
    'nvDialog',
    'nvToast',
    'nvURL',
    '$rootScope',
  ];

  function Controller(
    Route,
    RouteGroup,
    nvTable,
    Waypoint,
    routes,
    routeName,
    problem_type,
    nvDateTimeUtils,
    $mdDialog,
    nvTranslate,
    $filter,
    nvTimezone,
    nvDialog,
    nvToast,
    nvURL,
    $rootScope
  ) {
    const ctrl = this;
    ctrl.wpData = [];
    ctrl.routeName = angular.copy(routeName);
    ctrl.onCancel = onCancel;
    ctrl.problem_type = problem_type;
    ctrl.onSaveSuggestedRoute = onSaveSuggestedRoute;
    ctrl.savingState = 'idle';
    ctrl.suggested_date = nvDateTimeUtils.toDate(moment(), nvTimezone.getOperatorTimezone());


    ctrl.tableFields = {
      seq: {
        displayName: 'container.same-day-route-engine.seq',
      },
      wpId: {
        displayName: 'container.same-day-route-engine.wp-id',
      },
      type: {
        displayName: 'container.same-day-route-engine.service-type',
      },
      shipper: {
        displayName: 'commons.shipper',
      },
      priority: {
        displayName: 'commons.priority',
      },
      address: {
        displayName: 'commons.address',
      },
      tracking_id: {
        displayName: 'commons.tracking-id',
      },
      originalStart: {
        displayName: 'container.same-day-route-engine.original-start',
      },
      suggestedStart: {
        displayName: 'container.same-day-route-engine.suggested-start',
      },
      originalEnd: {
        displayName: 'container.same-day-route-engine.original-end',
      },
      suggestedEnd: {
        displayName: 'container.same-day-route-engine.suggested-end',
      },
      trackingUrl: {
        displayName: 'container.same-day-route-engine.tracking-url',
      },
    };

    ctrl.tableParam = {};
    ctrl.csvField = [
      'routeName',
      'seq',
      'id',
      'type',
      'shipper',
      'address',
      'tracking_id',
      '_originalStart',
      '_suggestedStart',
      '_originalEnd',
      '_suggestedEnd',
      'trackingUrl',
    ];

    ctrl.csvHeader = [
      nvTranslate.instant('container.same-day-route-engine.route-name'),
      nvTranslate.instant('container.same-day-route-engine.seq'),
      nvTranslate.instant('container.same-day-route-engine.wp-id'),
      nvTranslate.instant('container.same-day-route-engine.service-type'),
      nvTranslate.instant('commons.shipper'),
      nvTranslate.instant('commons.address'),
      nvTranslate.instant('commons.tracking-id'),
      nvTranslate.instant('container.same-day-route-engine.original-start'),
      nvTranslate.instant('container.same-day-route-engine.suggested-start'),
      nvTranslate.instant('container.same-day-route-engine.original-end'),
      nvTranslate.instant('container.same-day-route-engine.suggested-end'),
      nvTranslate.instant('container.same-day-route-engine.tracking-url'),
    ];

    ctrl.csvFileName = `route-detail-${ctrl.routeName}.csv`;
    init();


    function onCancel() {
      return $mdDialog.hide();
    }

    function onSaveSuggestedRoute() {
      ctrl.savingState = 'waiting';
      const payload = _.compact(_.map(ctrl.wpData, (data) => {
        if (!_.startsWith(data.id, 'break')) {
          return {
            tracking_id: data.tracking_id,
            suggested_start_time: nvDateTimeUtils.displayTime(data.suggestedStart),
            suggested_end_time: nvDateTimeUtils.displayTime(data.suggestedEnd),
            suggested_date: nvDateTimeUtils.displayDate(ctrl.suggested_date),
          };
        }
      }));

      return RouteGroup.saveSuggestedRoute(payload)
        .then(success, error);
      function success(result) {
        ctrl.savingState = 'idle';
        const faileds = _.compact(_.map(result, (res) => {
          if (!res.status) {
            return res;
          }
        }));
        if (faileds && faileds.length > 0) {
          return nvDialog.showSingle(null, {
            skipHide: true,
            controller: 'SaveSuggestedRouteErrorDialogController',
            controllerAs: 'subCtrl',
            templateUrl: 'views/container/same-day-route-engine/dialog/save-suggested-route-error-dialog.html',
            cssClass: 'nv-sdre-save-suggested-detail-dialog',
            theme: 'nvBlue',
            locals: {
              failedUploads: faileds,
            },
          });
        }
        return nvToast.success(nvTranslate.instant('container.same-day-route-engine.timeslots-saved-successfully'));
      }

      function error() {
        ctrl.savingState = 'idle';
      }
    }

    function init() {
      ctrl.tableParam = nvTable.createTable(ctrl.tableFields);
      const waypointsIDs = _.compact(_.map(routes, (route) => {
        if (Number(route.id)) {
          return route.id;
        }
      }));
      return Waypoint.search(waypointsIDs)
        .then(success);

      function success(result) {
        const now = moment();
        const data = _.map(routes, (route, key) => {
          const wp = {
            seq: key + 1,
            id: route.id,
            suggestedStart: nvDateTimeUtils.toSOD(now, 'utc', route.arr_time),
            suggestedEnd: nvDateTimeUtils.toSOD(now, 'utc', route.end_time),
            routeName: ctrl.routeName,
          };

          const parsedId = Number(route.id);
          if (parsedId) {
            _.merge(wp, _.find(result, { id: parsedId }));
            // add tracking if same day pickup
            if (ctrl.problem_type === 'SAME_DAY_PICKUP_DELIVERY') {
              wp.trackingUrl = `${window.location.protocol}${nvURL.buildTrackerUrl(nv.config.env, $rootScope.domain)}&tracking_id=${wp.tracking_id}&token=${wp.uuid}`;
            } else {
              wp.trackingUrl = `${window.location.protocol}${nvURL.buildBulkyTrackerUrl(nv.config.env, $rootScope.domain)}?tracking_id=${wp.tracking_id}&token=${wp.uuid}`;
            }
            // reformat
            if (wp.start_time) {
              wp.originalStart = wp.start_time;
            }
            if (wp.end_time) {
              wp.originalEnd = wp.end_time;
            }
          } else {
            wp.originalStart = null;
            wp.originalEnd = null;
          }
          // add formatted version for csv export
          wp._suggestedStart = $filter('nvTime')(wp.suggestedStart, 'operator', 'datetime');
          wp._suggestedEnd = $filter('nvTime')(wp.suggestedEnd, 'operator', 'datetime');
          wp._originalStart = $filter('nvTime')(wp.originalStart, 'operator', 'datetime');
          wp._originalEnd = $filter('nvTime')(wp.originalEnd, 'operator', 'datetime');
          return wp;
        });
        ctrl.tableParam.setData(data);
        ctrl.wpData = data;
      }
    }
  }
}());
