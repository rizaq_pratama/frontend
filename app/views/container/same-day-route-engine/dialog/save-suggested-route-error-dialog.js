(function controller() {
  angular
    .module('nvOperatorApp.controllers')
    .controller('SaveSuggestedRouteErrorDialogController', Controller);

  Controller.$inject = [
    'failedUploads',
    'nvTable',
    '$mdDialog',
    'nvTranslate',
  ];

  function Controller(
    failedUploads,
    nvTable,
    $mdDialog,
    nvTranslate
  ) {
    const subCtrl = this;

    subCtrl.fields = {
      tracking_id: { displayName: nvTranslate.instant('commons.tracking-id') },
      message: { displayName: nvTranslate.instant('commons.message') },
    };

    subCtrl.tableParam = {};
    subCtrl.failedUploads = angular.copy(failedUploads);
    subCtrl.onCancel = onCancel;

    init();
    function init() {
      subCtrl.tableParam = nvTable.createTable(subCtrl.fields);
      subCtrl.tableParam.setData(subCtrl.failedUploads);
    }

    function onCancel() {
      return $mdDialog.cancel();
    }
  }
}());
