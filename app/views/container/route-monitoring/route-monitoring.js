(function controller() {
  angular
    .module('nvOperatorApp.controllers')
    .controller('RouteMonitoringController', RouteMonitoringController);

  RouteMonitoringController.$inject = [
    '$q', 'Tag', 'Hub', 'nvBackendFilterUtils', 'nvTranslate',
    'nvDateTimeUtils', 'nvTimezone', 'Zone',
  ];

  function RouteMonitoringController(
    $q, Tag, Hub, nvBackendFilterUtils, nvTranslate,
    nvDateTimeUtils, nvTimezone, Zone
  ) {
    // --- variables ---
    const FILTER_ID = {
      DATE: 'date',
      HUBS: 'hubs',
      TAGS: 'tags',
      ZONES: 'zones',
    };
    const ctrl = this;

    // filter keys
    ctrl.FILTER_ID = FILTER_ID;

    // state params
    ctrl.date = null; // in string
    ctrl.hubIds = null; // string: hub ids separated by comma
    ctrl.withTagIds = null; // string: tag ids separated by comma
    ctrl.zoneIds = null; // string: zone ids separated by comma

    // shared params
    ctrl.selectedFilters = [];
    ctrl.possibleFilters = [];

    // --- functions ---
    ctrl.getData = getData;

    // --- functions details ---
    function getData() {
      return $q.all([
        Tag.all(),
        Hub.read({ active_only: false }),
        Zone.read(),
      ]).then(success, $q.reject);

      function success(response) {
        ctrl.tagsResult = response[0];
        ctrl.hubsResult = response[1];
        ctrl.zonesResult = response[2];

        const filters = initFilter();
        ctrl.possibleFilters = filters.generalFilters;
        ctrl.selectedFilters = _.remove(ctrl.possibleFilters,
            filter => filter.showOnInit === true
        );
      }
    }

    function initFilter() {
      const generalFilters = [
        {
          id: FILTER_ID.DATE,
          type: nvBackendFilterUtils.FILTER_DATE,
          showOnInit: true,
          isMandatory: true,
          fromModel: moment().toDate(),
          isRange: false,
          transformFn: transformDate,
          mainTitle: nvTranslate.instant('commons.model.route-date'),
          key: {
            from: 'date',
          },
          presetKey: {
            from: 'date',
          },
        },
        {
          id: FILTER_ID.HUBS,
          type: nvBackendFilterUtils.FILTER_OPTIONS,
          showOnInit: true,
          possibleOptions: Hub.toOptions(ctrl.hubsResult),
          selectedOptions: [],
          searchText: {},
          transformFn: transformOptions,
          mainTitle: 'Hubs',
          key: 'hub_ids',
          presetKey: 'hub_ids',
        },
        {
          id: FILTER_ID.TAGS,
          type: nvBackendFilterUtils.FILTER_OPTIONS,
          showOnInit: true,
          possibleOptions: Tag.toOptions(ctrl.tagsResult),
          selectedOptions: [],
          searchText: {},
          transformFn: transformOptions,
          mainTitle: 'Route Tags',
          key: 'with_tag_ids',
          presetKey: 'tag_ids',
        },
        {
          id: FILTER_ID.ZONES,
          type: nvBackendFilterUtils.FILTER_OPTIONS,
          showOnInit: true,
          possibleOptions: Zone.toOptions(ctrl.zonesResult),
          selectedOptions: [],
          searchText: {},
          transformFn: transformOptions,
          mainTitle: 'commons.zones',
          key: 'zone_ids',
          presetKey: 'zone_ids',
        },
      ];

      return {
        generalFilters: generalFilters,
      };

      function transformDate(date) {
        return nvDateTimeUtils.displayDate(
          nvDateTimeUtils.toSOD(date),
          nvTimezone.getOperatorTimezone()
        );
      }

      function transformOptions(item) {
        return item.value;
      }
    }
  }
}());
