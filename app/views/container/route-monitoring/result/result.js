(function controller() {
  angular
    .module('nvOperatorApp.controllers')
    .controller('RouteMonitoringResultController', RouteMonitoringResultController);

  RouteMonitoringResultController.$inject = [
    '$scope', '$state', '$stateParams', 'nvTable', 'nvBackendFilterUtils',
    'Route', 'nvDateTimeUtils', '$window', 'Waypoint', 'Timewindow', 'nvTimezone',
    'Excel', 'nvCoupledListUtils',
  ];

  const WAYPOINT_MAIN = {
    SUCCESSFUL: 'successful',
    VALID_FAILED: 'valid-failed',
    PENDING: 'pending',
    INVALID_FAILED: 'invalid-failed',
    RESERVATION: 'reservation',
    NONE: 'none',
  };

  const WAYPOINT_OVERLAY = {
    EARLY: 'early',
    LATE: 'late',
    IMPENDING_TIMESLOTS: 'impending-timeslots',
    LATE_PENDING: 'late-pending',
    NONE: 'none',
  };

  const WAYPOINT_BOT_OVERLAY = {
    RETURN_PICKUP: 'return-pickup',
    C2C_PICKUP: 'c2c-pickup',
    NONE: 'none',
  };

  function RouteMonitoringResultController(
    $scope, $state, $stateParams, nvTable, nvBackendFilterUtils,
    Route, nvDateTimeUtils, $window, Waypoint, Timewindow, nvTimezone,
    Excel, nvCoupledListUtils
  ) {
    // variables
    const ctrl = this;
    const parentCtrl = $scope.$parent.ctrl;
    const timewindows = _.keyBy(Timewindow.TIMEWINDOWS, 'id');

    ctrl.loading = false;
    ctrl.routeMonitoringTableParam = null;
    ctrl.parentCtrl = parentCtrl;

    // functions
    ctrl.goToFilterPage = goToFilterPage;
    ctrl.goToRouteManifestPage = goToRouteManifestPage;
    ctrl.testTooltip = testTooltip;
    ctrl.downloadReport = dowloadRouteReport;

    // start
    init();

    // functions details
    function init() {
      parentCtrl.date = $stateParams.date || null;
      parentCtrl.hubIds = $stateParams.hub_ids || null;
      parentCtrl.withTagIds = $stateParams.with_tag_ids || null;
      parentCtrl.zoneIds = $stateParams.zone_ids || null;

      // validation
      if (parentCtrl.date == null) {
        goToFilterPage();
        return;
      }
      processFilter();
      getResult();
    }

    function goToFilterPage() {
      $state.go('container.route-monitoring.filter');
    }

    function goToRouteManifestPage(routeId) {
      $window.open(
        $state.href('container.route-manifest', { routeId: routeId })
      );
    }

    function processFilter() {
      // set date as url
      const dateFilter = _.find(parentCtrl.selectedFilters, ['id', parentCtrl.FILTER_ID.DATE]);
      dateFilter.fromModel = nvDateTimeUtils.toMoment(
        parentCtrl.date, nvTimezone.getOperatorTimezone()
      ).toDate();

      // reset hubFilter selections
      const hubFilter = _.find(parentCtrl.selectedFilters, ['id', parentCtrl.FILTER_ID.HUBS]);
      if (hubFilter) {
        const possibleOptions = hubFilter.possibleOptions;
        const existingSelectedOptions = hubFilter.selectedOptions;
        // clean selections
        nvCoupledListUtils.transferAll(existingSelectedOptions, possibleOptions);

        // apply hub id in url to filter
        if (parentCtrl.hubIds) {
          const hubIds = _(parentCtrl.hubIds).split(',').map(id => parseInt(id, 10)).value();
          const toBeSelectedOptions = _.remove(possibleOptions, opt =>
            _.find(hubIds, id => opt.value === id)
          );
          hubFilter.selectedOptions = toBeSelectedOptions;
        }
      }

      // reset routeTagFilter selections
      const routeTagFilter = _.find(parentCtrl.selectedFilters, ['id', parentCtrl.FILTER_ID.TAGS]);
      if (routeTagFilter) {
        const possibleOptions = routeTagFilter.possibleOptions;
        const existingSelectedOptions = routeTagFilter.selectedOptions;
        // clean selections
        nvCoupledListUtils.transferAll(existingSelectedOptions, possibleOptions);

        // apply route tag id in url to filter
        if (parentCtrl.withTagIds) {
          const tagIds = _(parentCtrl.withTagIds).split(',').map(id => parseInt(id, 10)).value();
          const toBeSelectedOptions = _.remove(possibleOptions, opt =>
            _.find(tagIds, id => opt.value === id)
          );
          routeTagFilter.selectedOptions = toBeSelectedOptions;
        }
      }

      // reset zoneFilter selections
      const zoneFilter = _.find(parentCtrl.selectedFilters, ['id', parentCtrl.FILTER_ID.ZONES]);
      if (zoneFilter) {
        const possibleOptions = zoneFilter.possibleOptions;
        const existingSelectedOptions = zoneFilter.selectedOptions;
        // clean selections
        nvCoupledListUtils.transferAll(existingSelectedOptions, possibleOptions);

        // apply zone id in url to filter
        if (parentCtrl.zoneIds) {
          const zoneIds = _(parentCtrl.zoneIds).split(',').map(id => parseInt(id, 10)).value();
          const toBeSelectedOptions = _.remove(possibleOptions, opt =>
              _.find(zoneIds, id => opt.value === id)
          );
          zoneFilter.selectedOptions = toBeSelectedOptions;
        }
      }

      _.forEach(parentCtrl.selectedFilters, (filter) => {
        filter.description = nvBackendFilterUtils.getDescription(filter);
      });
    }

    function getResult() {
      ctrl.routeMonitoringTableParam = nvTable.createTable();
      ctrl.routeMonitoringTableParam.addColumn('id', { displayName: 'commons.route' });
      ctrl.routeMonitoringTableParam.addColumn('driverName', { displayName: 'commons.driver-name' });
      ctrl.routeMonitoringTableParam.addColumn('zoneName', { displayName: 'commons.zone' });
      ctrl.routeMonitoringTableParam.addColumn('hubName', { displayName: 'container.route-monitoring.inbound-hub' });
      ctrl.routeMonitoringTableParam.addColumn('routeProgression', { displayName: 'container.route-monitoring.route-progression' });
      ctrl.routeMonitoringTableParam.addColumn('totalWaypoint', { displayName: 'container.route-monitoring.total-wp' });
      ctrl.routeMonitoringTableParam.addColumn('completionPercentage', { displayName: 'container.route-monitoring.completion-percentage' });
      ctrl.routeMonitoringTableParam.addColumn('pendingCount', { displayName: 'container.route-monitoring.pending' });
      ctrl.routeMonitoringTableParam.addColumn('successCount', { displayName: 'container.route-monitoring.success' });
      ctrl.routeMonitoringTableParam.addColumn('failedCount', { displayName: 'container.route-monitoring.valid-failed' });
      ctrl.routeMonitoringTableParam.addColumn('cmiCount', { displayName: 'container.route-monitoring.invalid-failed' });
      ctrl.routeMonitoringTableParam.addColumn('earlyCount', { displayName: 'container.route-monitoring.early-wp' });
      ctrl.routeMonitoringTableParam.addColumn('lateCount', { displayName: 'container.route-monitoring.late-wp' });
      ctrl.routeMonitoringTableParam.addColumn('impendingCount', { displayName: 'container.route-monitoring.impending' });
      ctrl.routeMonitoringTableParam.addColumn('lateAndPendingCount', { displayName: 'container.route-monitoring.late-and-pending' });
      ctrl.routeMonitoringTableParam.addColumn('lastSeen', { displayName: 'container.route-monitoring.last-seen' });

      ctrl.routeMonitoringTableParam.updateColumnFilter('routeProgression', strFn =>
        (route) => {
          const str = strFn().toLowerCase().trim();
          return filterWaypoints(str, route.waypoints);
        }
      );

      ctrl.loading = true;
      const payload = {
        date: nvDateTimeUtils.displayDateTime(
          nvDateTimeUtils.toSOD(parentCtrl.date), 'utc'),
      };

      if (parentCtrl.hubIds) {
        _.assign(payload, { hub_ids: parentCtrl.hubIds });
      }
      if (parentCtrl.withTagIds) {
        _.assign(payload, { with_tag_ids: parentCtrl.withTagIds });
      }
      if (parentCtrl.zoneIds) {
        _.assign(payload, { zone_ids: parentCtrl.zoneIds });
      }

      Route.getRouteMonitoring(payload)
        .then(success, failure);

      function success(response) {
        ctrl.loading = false;

        const routes = extendRoutes(_.get(response, 'data.routes') || []);
        ctrl.routeMonitoringTableParam.setData(routes);
      }

      function failure() {
        ctrl.loading = false;
      }

      function extendRoutes(routes) {
        const currentDateTime = nvDateTimeUtils.toMoment(new Date());
        return _.map(routes, route => (
          setCustomRouteData(route)
        ));

        function setCustomRouteData(route) {
          route.totalWaypoint = _.size(route.waypoints);
          route.completionPercentage = 0;
          route.pendingCount = 0;
          route.successCount = 0;
          route.failedCount = 0;
          route.cmiCount = 0;
          route.earlyCount = 0;
          route.lateCount = 0;
          route.impendingCount = 0;
          route.lateAndPendingCount = 0;
          route.lastSeen = '-';

          _.forEach(route.waypoints, (waypoint) => {
            /*
             * baseColor:
             * - success: #03af4b
             * - failed: #f4c1a1
             * - pending: #818181
             * - reservation: #01a8ef
             * - CMI: #ff5454
             *
             * overlayColor:
             * - early: #ffffff
             * - late: #000000
             * - Impending Timeslots: #ffff04
             * - late and pending: #fe0203
             *
             * overlayBottomColor:
             * - return pickup: #492faf
             * - c2c pickup: #FF0000
             * */
            waypoint.baseColor = WAYPOINT_MAIN.NONE;
            waypoint.overlayColor = WAYPOINT_OVERLAY.NONE;
            waypoint.overlayBottomColor = WAYPOINT_BOT_OVERLAY.NONE;
            insertTooltip(waypoint);
            if (Waypoint.isReservation(waypoint)) {
              if (waypoint.status === 'SUCCESS') {
                waypoint.baseColor = WAYPOINT_MAIN.SUCCESSFUL;
                route.successCount += 1;
              } else if (waypoint.status === 'FAIL' && !waypoint.isCmiWaypoint) {
                waypoint.baseColor = WAYPOINT_MAIN.INVALID_FAILED;
                route.failedCount += 1;
              } else {
                waypoint.baseColor = WAYPOINT_MAIN.RESERVATION;
                route.pendingCount += 1;
              }
            } else { // Transaction type waypoint
              const startTime = timewindows[waypoint.timewindowId].fromTime;
              const dueTime = timewindows[waypoint.timewindowId].toTime;

              const firstTxn = _.head(waypoint.transactions);
              const lastTxn = _.last(waypoint.transactions);

              // overlay bottom color
              if (lastTxn) {
                if (lastTxn.transactionType === 'PICKUP' && lastTxn.orderType === 'RETURN') {
                  waypoint.overlayBottomColor = WAYPOINT_BOT_OVERLAY.RETURN_PICKUP;
                } else if (lastTxn.transactionType === 'PICKUP' && lastTxn.orderType === 'C2C') {
                  waypoint.overlayBottomColor = WAYPOINT_BOT_OVERLAY.C2C_PICKUP;
                }
              }
              // base and overlay color
              if (firstTxn) {
                // fail or success txn
                if (firstTxn.transactionStatus === 'FAIL' || firstTxn.transactionStatus === 'SUCCESS') {
                  if (firstTxn.transactionStatus === 'FAIL' && !waypoint.isCmiWaypoint) {
                    waypoint.baseColor = WAYPOINT_MAIN.VALID_FAILED;
                    route.failedCount += 1;
                  } else if (firstTxn.transactionStatus === 'SUCCESS') {
                    waypoint.baseColor = WAYPOINT_MAIN.SUCCESSFUL;
                    route.successCount += 1;
                  }

                  const serviceEndTime = firstTxn.serviceEndTime
                    ? nvDateTimeUtils.toMoment(firstTxn.serviceEndTime)
                    : null;
                  if (serviceEndTime && startTime && serviceEndTime.isBefore(startTime)) {
                    waypoint.overlayColor = WAYPOINT_OVERLAY.EARLY;
                    route.earlyCount += 1;
                  } else if (serviceEndTime && dueTime && serviceEndTime.isAfter(dueTime)) {
                    waypoint.overlayColor = WAYPOINT_OVERLAY.LATE;
                    route.lateCount += 1;
                  }
                } else {
                  // pending
                  route.pendingCount += 1;
                  waypoint.baseColor = WAYPOINT_MAIN.PENDING;

                  if (dueTime && currentDateTime.isAfter(dueTime)) {
                    waypoint.overlayColor = WAYPOINT_OVERLAY.LATE_PENDING;
                    route.lateAndPendingCount += 1;
                  } else if (waypoint.timewindowId !== -1 && waypoint.timewindowId !== -2
                    && startTime && dueTime
                    && currentDateTime.isAfter(startTime) && currentDateTime.isBefore(dueTime)) {
                    waypoint.overlayColor = WAYPOINT_OVERLAY.IMPENDING_TIMESLOTS;
                    route.impendingCount += 1;
                  } else if (waypoint.timewindowId === -1 || waypoint.timewindowId === -2) {
                    let fakeStartTime = null;
                    let fakeDueTime = null;
                    if (waypoint.timewindowId === -1) {
                      fakeStartTime = timewindows[3].fromTime;
                      fakeDueTime = timewindows[3].ToTime;
                    } else {
                      fakeStartTime = timewindows[2].fromTime;
                      fakeDueTime = timewindows[2].ToTime;
                    }

                    if (fakeStartTime && fakeDueTime
                      && currentDateTime.isAfter(fakeStartTime)
                      && currentDateTime.isBefore(fakeDueTime)) {
                      waypoint.overlayColor = WAYPOINT_OVERLAY.IMPENDING_TIMESLOTS;
                      route.impendingCount += 1;
                    }
                  }
                }
              }
            }

            if (waypoint.isCmiWaypoint) {
              waypoint.baseColor = WAYPOINT_MAIN.VALID_FAILED;
              route.cmiCount += 1;
            }
          });

          if (route.driverLastSeen) {
            route.lastSeen = nvDateTimeUtils.displayFormat(moment(route.driverLastSeen), 'HH:mm');
          }

          if (route.totalWaypoint > 0) {
            route.completionPercentage = _.toInteger(
              (1 - (route.pendingCount / route.totalWaypoint)) * 100
            );
          } else {
            route.completionPercentage = 100;
          }

          return route;

          function insertTooltip(wp) {
            const startTime = timewindows[wp.timewindowId].fromTime;
            const dueTime = timewindows[wp.timewindowId].toTime;
            const wpEndTime = nvDateTimeUtils.toMoment(wp.endTime);
            wp.tooltip = true;
            wp.tooltipParam = {
              type: wp.type,
              trackingIds: _.join(wp.trackingIds, ', '),
              timeWindows: `${startTime.format('h:mm A')}-${dueTime.format('h:mm A')}`,
              status: wp.waypointStatus,
              serviceEndTime: nvDateTimeUtils.displayDateTime(wpEndTime, nvTimezone.getOperatorTimezone()),
              name: wp.name,
              contact: wp.contact,
              email: wp.email,
              address: `${wp.address1} ${wp.address2} ${wp.city} ${wp.country} ${wp.postcode}`,
              focus: false,
            };
          }
        }
      }
    }

    function testTooltip($event, $param) {
      $param.showTooltip = !$param.showTooltip;
    }

    function filterWaypoints(str, wps) {
      if (str.length === 0) {
        _.forEach(wps, (wp) => {
          wp.tooltipParam.focus = false;
        });
        return true;
      } else if (wps && wps.length > 0) {
        let found = false;
        _.forEach(wps, (wp) => {
          const param = wp.tooltipParam;
          const tempFound = _.some(param, val =>
            val && _.isString(val) && (_.toLower(val).indexOf(_.toLower(str)) > -1)
          );
          if (tempFound) {
            found = true;
            param.focus = true;
          } else {
            param.focus = false;
          }
        });
        return found;
      }
      return false;
    }

    function dowloadRouteReport() {
      const dateStr = parentCtrl.date;
      Excel.download(`core/reports/getroutecleaningreportV3?date=${dateStr}`,
        `route_cleaning_report_(${dateStr}).xls`);
    }
  }
}());
