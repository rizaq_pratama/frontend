(function controller() {
  angular
    .module('nvOperatorApp.controllers')
    .controller('RouteMonitoringFilterController', RouteMonitoringFilterController);

  RouteMonitoringFilterController.$inject = [
    '$scope', 'nvBackendFilterUtils', '$state', 'nvToast', 'nvTranslate'
  ];

  function RouteMonitoringFilterController(
    $scope, nvBackendFilterUtils, $state, nvToast, nvTranslate
  ) {
    // variables
    const ctrl = this;
    const parentCtrl = $scope.$parent.ctrl;

    ctrl.tags = _.get($scope.ctrl.tagsResult, 'tags');
    ctrl.hubs = $scope.ctrl.hubsResult;

    ctrl.parentCtrl = parentCtrl;

    // functions
    ctrl.goToResultPage = goToResultPage;

    function goToResultPage() {
      const params = _.mapValues(nvBackendFilterUtils.constructParams(parentCtrl.selectedFilters),
        val => (_.isArray(val) ? _.join(val, ',') : val)
      );
      let isValid = true;
      if (!params.zone_ids) {
        nvToast.warning(nvTranslate.instant('container.route-monitoring.zone-can-not-be-empty'));
        isValid = false;
      }
      if (!params.hub_ids) {
        nvToast.warning(nvTranslate.instant('container.route-monitoring.hub-can-not-be-empty'));
        isValid = false;
      }
      if (isValid) {
        $state.go('container.route-monitoring.result', params);
      }
    }
  }
}());
