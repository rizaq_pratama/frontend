(function controller() {
  angular
    .module('nvOperatorApp.controllers')
    .controller('RouteLoadMonitoring', RouteLoadMonitoring);

  RouteLoadMonitoring.$inject = ['Route', 'Driver', 'Zone', 'Order', 'Inbound', '$q', 'nvTable', 'nvTranslate', 'nvTimezone', 'nvDateTimeUtils', 'nvDialog', '$timeout'];

  function RouteLoadMonitoring(Route, Driver, Zone, Order, Inbound, $q,
    nvTable, nvTranslate, nvTimezone, nvDateTimeUtils, nvDialog, $timeout) {
    const DETAILS_TYPE = {
      TOTAL: 'total',
      LOADED: 'loaded',
      PASSED_BACK: 'passed_back',
      MISSING: 'missing',
    };

    const INBOUND_TYPE = {
      VAN_FROM_NINJAVAN: 'VAN_FROM_NINJAVAN',
      TRUCK: 'TRUCK',
    };


    // table fields
    const FIELDS = {
      zone_name: { displayName: nvTranslate.instant('container.route-load-monitoring.zones') },
      id: { displayName: nvTranslate.instant('container.route-load-monitoring.route-id') },
      driver_name: { displayName: nvTranslate.instant('container.route-load-monitoring.driver-name') },
      driver_contact: { displayName: nvTranslate.instant('container.route-load-monitoring.driver-contact') },
      total_parcels_count: { displayName: nvTranslate.instant('container.route-load-monitoring.parcels-assigned') },
      van_parcels_count: { displayName: nvTranslate.instant('container.route-load-monitoring.parcels-loaded') },
      truck_parcels_count: { displayName: nvTranslate.instant('container.route-load-monitoring.parcels-passed-back') },
      missing_parcels_count: { displayName: nvTranslate.instant('container.route-load-monitoring.missing-parcels') },
    };


    const ctrl = this;
    ctrl.DETAILS_TYPE = DETAILS_TYPE;
    ctrl.detailsType = DETAILS_TYPE.TOTAL;
    ctrl.date = new Date();
    ctrl.routes = [];
    ctrl.tableParam = nvTable.createTable(FIELDS, {});
    ctrl.viewDetails = viewDetails;
    ctrl.onChangeCheckbox = onChangeCheckbox;
    ctrl.init = init;
    ctrl.loadingSheet = false;
    ctrl.filter = {
      partiallyLoaded: false,
      passedBack: false,
    };

    // flags
    ctrl.isLoading = false;


    init();

    function onChangeCheckbox() {
      $timeout(() => {
        ctrl.tableParam.setData(_.filter(ctrl.routes, obj => (filterResult(obj))));
        ctrl.tableParam.refreshData();
      });
    }

    function init() {
      ctrl.loadingSheet = true;
      return Route.search({
        fr_date: nvDateTimeUtils.displayDate(ctrl.date, nvTimezone.getOperatorTimezone()),
        to_date: nvDateTimeUtils.displayDate(ctrl.date, nvTimezone.getOperatorTimezone()),
        include_waypoints: true,
      }).then((result) => {
        const driverIds = _.uniq(_.map(result, 'driver_id'));
        const zoneIds = _.uniq(_.map(result, 'zone_id'));
        const waypointsIds = _.uniq(_.flatten(_.map(result, 'waypoint_ids')));
        const routeIds = _.map(result, 'id');
        let orderIdToInboundScanMap = {};

        // checking
        if (_.size(routeIds) <= 0) {
          ctrl.tableParam.setData([]);
          ctrl.tableParam.refreshData();
          ctrl.loadingSheet = false;
          return;
        }

        // get rest of the data
        $q.all([
          Driver.searchByIds(driverIds),
          Zone.searchByIds(zoneIds),
          Order.searchByWaypointIdsInBatch(waypointsIds),
          Inbound.searchByRouteIdsInBatch(routeIds),
        ]).then(onSuccessRestData);

        function onSuccessRestData(results) {
          emptyRouteLoadData();
          const driversMap = _.keyBy(results[0], 'id');
          const zonesMap = _.keyBy(results[1], 'id');
          const waypointToOrdersResponse = results[2];
          const waypointToOrdersMap = {};
          _.each(waypointToOrdersResponse, (row) => {
            waypointToOrdersMap[row.waypoint_id] = row.orders;
          });
          orderIdToInboundScanMap = _.keyBy(results[3], 'order_id');
          extendOrders(results[2]);
          ctrl.routes = result;
          _.each(ctrl.routes, (route) => {
            route.zone_name = zonesMap[route.zone_id] ? zonesMap[route.zone_id].name : `Zone ID ${route.zone_id}`;

            route.driver_name = route.driver_id;
            route.driver_contact = null;
            if (driversMap[route.driver_id]) {
              route.driver_name = _.join(_.compact([
                driversMap[route.driver_id].firstName,
                driversMap[route.driver_id].lastName,
              ]), ' ');
              route.driver_contact = _.get(driversMap[route.driver_id], 'contacts[0].details') || '';
            }
            route.orders = [];
            _.forEach(route.waypoint_ids, (waypointId) => {
              route.orders = _.concat(route.orders, waypointToOrdersMap[waypointId] || []);
            });

            // for sorting and filtering purpose
            route.total_parcels_count = _.size(route.orders);
            route.van_parcels_count = _.size(filterOrders(route.orders, DETAILS_TYPE.LOADED));
            route.truck_parcels_count =
              _.size(filterOrders(route.orders, DETAILS_TYPE.PASSED_BACK));
            route.missing_parcels_count = _.size(filterOrders(route.orders, DETAILS_TYPE.MISSING));
          });
          ctrl.tableParam.setData(_.filter(ctrl.routes, obj => (filterResult(obj))));
          ctrl.loadingSheet = false;
          function extendOrders(response) {
            return _.flatten(_.map(response, (row) => {
              const waypointId = row.waypoint_id;
              return _.map(row.orders, (order) => {
                order.id = order.order_id;
                order.waypoint_id = waypointId;
                order.latestInboundScan = null;
                if (orderIdToInboundScanMap[order.id]) {
                  order.latestInboundScan = orderIdToInboundScanMap[order.id];
                }
                return order;
              });
            }));
          }
        }
      });

      function emptyRouteLoadData() {
        ctrl.tableParam.setData([]);
      }
    }

    function viewDetails(route, type) {
      const parcels = filterOrders(route.orders, type);
      return nvDialog.showSingle(null, {
        templateUrl: 'views/container/route-load-monitoring/dialog/route-load-monitoring-detail.dialog.html',
        locals: {
          routeId: route.id,
          type: type,
          parcels: parcels,
        },
        theme: 'nvBlue',
        cssClass: 'nv-route-load-monitoring-details-dialog',
        controllerAs: 'ctrl',
        controller: 'RouteLoadMonitoringDetail',
      });
    }

    function filterOrders(orders, detailsType) {
      switch (detailsType) {
        case DETAILS_TYPE.LOADED:
          return _.filter(orders, order => (_.includes([
            INBOUND_TYPE.VAN_FROM_NINJAVAN,
          ], _.get(order, 'latestInboundScan.type'))));
        case DETAILS_TYPE.PASSED_BACK:  
          return _.filter(orders, order => (_.includes([
            INBOUND_TYPE.VAN_FROM_NINJAVAN,
          ], _.get(order, 'latestInboundScan.type'))));
        case DETAILS_TYPE.MISSING:
          return _.filter(orders, order => (!_.includes([
            INBOUND_TYPE.TRUCK,
            INBOUND_TYPE.VAN_FROM_NINJAVAN,
          ], _.get(order, 'latestInboundScan.type'))));
        default:
          return orders;
      }
    }

    function filterResult(obj) {
      if (ctrl.filter.partiallyLoaded && (obj.total_parcels_count !== obj.van_parcels_count)) {
        return false;
      } else if (ctrl.filter.passedBack && obj.truck_parcels_count === 0) {
        return false;
      }
      return true;
    }
  }
}());
