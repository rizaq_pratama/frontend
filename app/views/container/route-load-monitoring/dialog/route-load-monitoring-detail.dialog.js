(function controller() {
  angular.module('nvOperatorApp.controllers')
        .controller('RouteLoadMonitoringDetail', RouteLoadMonitoringDetail);

  RouteLoadMonitoringDetail.$inject = ['routeId', 'type', 'parcels', '$mdDialog'];

  function RouteLoadMonitoringDetail(routeId, type, parcels, $mdDialog) {
    const ctrl = this;
    ctrl.routeId = _.clone(routeId);
    ctrl.type = _.clone(type);
    ctrl.parcels = _.clone(parcels);
    ctrl.onCancel = onCancel;
    function onCancel() {
      ctrl.state = 'idle';
      return $mdDialog.cancel();
    }
  }
}());
