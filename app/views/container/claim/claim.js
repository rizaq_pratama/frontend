(function controller() {
  angular
    .module('nvOperatorApp.controllers')
    .controller('ClaimController', ClaimController);

  function ClaimController() {
    const ctrl = this;
    ctrl.url = `${nv.config.servers['operator-react']}/claim`;
  }
}());

