(function controller() {
  angular
        .module('nvOperatorApp.controllers')
        .controller('ReservationsController', ReservationsController);

  ReservationsController.$inject = [
    '$timeout', 'Timewindow', '$q', 'nvScroll', 'nvToast', 'nvDialog',
    'Shippers', 'BlockedDates', 'Reservation', 'Address',
    'nvAutocomplete.Data', 'nvCalendar.Calendar', 'nvCalendar.Data',
    'nvDateTimeUtils', 'DistributionPoint', 'nvTable', 'nvRequestUtils',
    'nvDomain', 'nvMaps', 'Overwatch', 'nvUtils', 'nvTranslate', '$filter', 'nvTimezone',
    'Addressing',
  ];

  function ReservationsController(
    $timeout, Timewindow, $q, nvScroll, nvToast, nvDialog,
    Shippers, BlockedDates, Reservation, Address,
    nvAutocompleteData, nvCalendar, nvCalendarData,
    nvDateTimeUtils, DistributionPoint, nvTable, nvRequestUtils,
    nvDomain, nvMaps, Overwatch, nvUtils, nvTranslate, $filter, nvTimezone,
    Addressing
  ) {
    const ctrl = this;
    const allowPremiumPickupShipperIds = [];

    ctrl.init = init;

    ctrl.shipperSearch = nvAutocompleteData.getByHandle('reservations-shipper');
    ctrl.shipperSearch.onSelect(onShipperSelect);
    ctrl.shipperSearchText = '';

    ctrl.addressSearch = nvAutocompleteData.getByHandle('reservations-address');
    ctrl.addressSearch.onSelect(onAddressSelect);
    ctrl.addressSearchText = '';

    // reservations table
    ctrl.tableParam = null;

    ctrl.addresses = [];
    ctrl.reservations = {};
    ctrl.extendedReservationDetail = _.assign({}, { active: false, date: null });
    ctrl.blockedDates = [];
    ctrl.dpNameMap = [];
    ctrl.distributionPoints = [];
    ctrl.groupedReservations = {};
    ctrl.cities = [];
    ctrl.hyperlocal_shipper = null;
    ctrl.hyperlocal_shipper_ids = [];
    ctrl.isManualChangeDp = false;
    ctrl.showEditReservation = showEditReservation;
    ctrl.isMultipleReservation = {};

    const addressSource = [
      {
        value: 'MANUAL',
        displayName: 'MANUAL',
      },
    ];
    const addressTypes = angular.copy(Address.addressTypes);
    const countries = angular.copy(Address.countries);

    ctrl.calendar = nvCalendar.getByHandle('reservations');
    ctrl.onChangeMonth = fetchShipperReservation;
    ctrl.onDayClicked = onDayClicked;

    ctrl.calendarData = nvCalendarData.getByHandle('reservations');
    ctrl.calendarData.setDayContent(calendarGetContent);
    ctrl.calendarData.setDayDisabled(calendarGetDisabled);

    ctrl.calendarHyperlocal = nvCalendar.getByHandle('reservations-hyperlocal');

    ctrl.onDayClickedHyperlocal = onDayClickedHyperlocal;
    ctrl.onChangeMonthHyperlocal = fetchHyperlocalReservation;

    ctrl.calendarDataHyperlocal = nvCalendarData.getByHandle('reservations-hyperlocal');
    ctrl.calendarDataHyperlocal.setDayContent(calendarGetContentHyperlocal);
    ctrl.calendarDataHyperlocal.setDayDisabled(calendarGetDisabledHyperlocal);

    ctrl.getShipperReservationPromise = null;
    ctrl.selectedShipper = null;
    ctrl.selectedAddress = null;
    ctrl.selectedReservation = null;
    ctrl.country = nvDomain.getDomain() ? nvDomain.getDomain().current : 'SG';
    ctrl.createForm = getDefaultCreateForm();
    ctrl.editForm = {
      saveButtonState: 'clean',
    };
    ctrl.showCreateReservation = showCreateReservation;
    ctrl.onCreateReservation = onCreateReservation;
    ctrl.onDeleteReservation = onDeleteReservation;
    ctrl.onUpdateReservation = onUpdateReservation;
    ctrl.getDpName = getDpName;
    ctrl.openListReservation = openListReservation;
    ctrl.openAddDialog = openAddDialog;
    ctrl.openEditDialog = openEditDialog;

    ctrl.formShowing = '';
    ctrl.volumeOptions = Reservation.volumeOptions;
    ctrl.joinDatesToString = joinDatesToString;
    ctrl.checkHyperlocalReservation = checkHyperlocalReservation;
    ctrl.hideReservationList = hideReservationList;
    ctrl.checkEnableCreateHyperlocalRsvn = checkEnableCreateHyperlocalRsvn;

    // ///////
    function init() {
      return $q.all([
        BlockedDates.read().then(_.partial(_.map, _, '_momentDate')).then(_.sortBy),
        DistributionPoint.listDPs({ is_active: true, is_hub: true }),
        Address.dpZones(ctrl.country),
      ]).then(success, $q.reject);

      function success(datas) {
        ctrl.blockedDates = datas[0];
        ctrl.shipperSearch.setPossibleOptions(getShippers);
        ctrl.distributionPoints = datas[1];
        _.each(datas[2].distributionPoints, (d) => {
          ctrl.dpNameMap[d.id] = d.shortName;
        });
        ctrl.cities = _.uniq(_.map(datas[2], city => (city.l2_name)));
      }

      function getShippers(text) {
        return Shippers.filterSearch(text);
      }
    }

    function calendarGetDisabled(date) {
      return isBlockedDate(date) || isReservedDate(date) || isPastDate(date);
    }

    function calendarGetContent(dateForContent) {
      if (_.isNull(ctrl.getShipperReservationPromise)) {
        return getContent(dateForContent);
      }
      return ctrl.getShipperReservationPromise
                .then(_.partial(getContent, dateForContent));

      function getContent(date) {
        const data = {};
        data.date = date;
        data.isBlockedDate = isBlockedDate(date);
        data.isPastDate = isPastDate(date);
        data.isReserved = isReservedDate(date);
        if (data.isReserved) {
          const reservations = ctrl.reservations[date.format('Y-M-D')] ?
            ctrl.reservations[date.format('Y-M-D')] : null;
          data.reservations = _(reservations)
            .map(rsvn => _.assign(rsvn, { theme: getRsvnTheme(rsvn) }))
            .sortBy(rsvn => rsvn._momentLatestDatetime.valueOf())
            .value();

          data.select = false;
          data.lastRsvn = _.last(data.reservations);
          const pendingRsvn = _.find(data.reservations, rsvn => rsvn.status.toLowerCase() === 'pending');
          data.hiddenRsvn = data.reservations && (data.reservations.length - 2);
          data.sampleReservation = (data.reservations.length > 0 && !pendingRsvn)
            ? data.reservations[0]
            : null;

          data.editReservation = showEditReservation;
          data.deselectDate = deselectDate;
          data.selectDate = selectDate;
          data.deleteReservation = onDeleteReservation;
          data.showDetails = showExtendedDetails;
          data.shipper = ctrl.selectedShipper;
        }

        const stateIfPastDate = data.isPastDate ? 'pastdate' : 'available';
        const stateIfReserved = data.isReserved ? 'reserved' : stateIfPastDate;
        data.state = data.isBlockedDate ? 'blocked' : stateIfReserved;

        const themeIfPastDate = data.isPastDate ? 'nvGrey' : 'nvGreen nv-secondary';
        const themeIfReserved = data.isReserved ? 'nvYellow' : themeIfPastDate;
        data._theme = data.isBlockedDate ? 'nvGrey' : themeIfReserved;
        return data;

        function selectDate(rsvn) {
          data.select = true;
          setMultipleRsvn(true, data);
          ctrl.createForm.selectedDate = [rsvn._momentReadyDatetime];
          showCreateReservation();
        }

        function deselectDate() {
          setMultipleRsvn(false);
          data.select = false;
        }
      }

      function showExtendedDetails(rsvns) {
        // rsvns must be not null and length is greater than 1
        const sampleRsvn = rsvns[0];
        ctrl.extendedReservationDetail = _.assign({}, {
          list: _.map(rsvns, rsvn => _.assign(rsvn, { theme: getRsvnTheme(rsvn) })),
          date: sampleRsvn && sampleRsvn._momentReadyDatetime.format('Y-M-D'),
          active: true,
        });
        ctrl.formShowing = 'expand';
        scrollTo('expand-details-container');
      }

      function getRsvnTheme(rsvn) {
        if (rsvn.status.toLowerCase() === 'pending') {
          return 'nvYellow';
        } else if (rsvn.status.toLowerCase() === 'success') {
          return 'nvGreen';
        } else if (rsvn.status.toLowerCase() === 'fail') {
          return 'nvRed';
        }
        return 'nvYellow';
      }
    }

    // for hyper local
    function calendarGetDisabledHyperlocal(date) {
      return isBlockedDate(date) || isPastDate(date);
    }

    function calendarGetContentHyperlocal(dateForContent) {
      if (_.isNull(ctrl.getShipperReservationPromise)) {
        return getContent(dateForContent);
      }
      return ctrl.getShipperReservationPromise
                .then(_.partial(getContent, dateForContent));

      function getContent(date) {
        const data = {};
        data.date = date;
        data.isBlockedDate = isBlockedDate(date);
        data.isPastDate = isPastDate(date);
        data.isReserved = isReservedDate(date);
        if (data.isReserved) {
          data.reservations = ctrl.reservations[date.format('Y-M-D')];
          data.groupedReservations = _.groupBy(data.reservations, 'reservationType');
        }

        const stateIfPastDate = data.isPastDate ? 'pastdate' : 'available';
        const stateIfReserved = data.isReserved ? 'reserved' : stateIfPastDate;
        data.state = data.isBlockedDate ? 'blocked' : stateIfReserved;

        const themeIfPastDate = data.isPastDate ? 'nvGrey' : 'nvGreen nv-secondary';
        const themeIfReserved = data.isReserved ? 'nvYellow' : themeIfPastDate;
        data._theme = data.isBlockedDate ? 'nvGrey' : themeIfReserved;
        return data;
      }
    }

    function checkHyperlocalShipper(allowedTypes) {
      return _.indexOf(allowedTypes, 'HYPERLOCAL') > -1;
    }

    function onShipperSelect(shipper) {
      ctrl.selectedShipper = shipper;
      ctrl.formShowing = '';
      clearAddressSearch();
      $q.all([
        Shippers.readSpecificSettingV2(shipper.global_id, 'reservation', 'allowed_types'),
        Shippers.elasticReadSellers(shipper.global_id),
        Shippers.readSpecificSettingV2(shipper.global_id, 'pickup'),
      ]).then((responses) => {
        const allowedTypes = responses[0].value;
        const subShippersIds = _.map(responses[1], s => s.legacy_id);

        let allowPremiumPickup = false;
        _.forEach(_.get(responses[2], 'service_type_level'), (serviceTypeLevel) => {
          if (serviceTypeLevel.type === Shippers.PICKUP_SERVICE_TYPE.SCHEDULED.id) {
            switch (serviceTypeLevel.level) {
              case Shippers.PICKUP_SERVICE_LEVEL.PREMIUM.id:
                allowPremiumPickup = true;
                break;
              case Shippers.PICKUP_SERVICE_LEVEL.STANDARD.id:
                allowPremiumPickup = false;
                return false;
              default:
            }
          }

          return _.noop();
        });

        ctrl.selectedShipper.allowPremiumPickup = allowPremiumPickup;
        if (ctrl.selectedShipper.allowPremiumPickup && !_.includes(
            allowPremiumPickupShipperIds,
            ctrl.selectedShipper.id
          )) {
          allowPremiumPickupShipperIds.push(ctrl.selectedShipper.id);
        }

        ctrl.selectedShipper.hyperlocal = checkHyperlocalShipper(allowedTypes);
        if (ctrl.selectedShipper.hyperlocal) {
          ctrl.hyperlocal_shipper_ids = _.concat(
            _.castArray(ctrl.selectedShipper.id), subShippersIds);
          onHyperlocalShipperSelect();
        }
        Shippers.readAddresses(shipper.global_id).then(success);
      });

      function clearAddressSearch() {
        if (angular.isDefined(ctrl.selectedAddress)) {
          ctrl.addressSearchText = '';
          ctrl.addressSearch.remove(ctrl.selectedAddress);
          ctrl.selectedAddress = null;
        }
      }

      function success(data) {
        ctrl.addresses = data;
        ctrl.addressSearch.setPossibleOptions(ctrl.addresses);
      }
    }

    function onHyperlocalShipperSelect() {
      ctrl.calendarDataHyperlocal = nvCalendar.getByHandle('reservations-hyperlocal');
      ctrl.calendarDataHyperlocal = nvCalendarData.getByHandle('reservations-hyperlocal');
      ctrl.calendarDataHyperlocal.setDayContent(calendarGetContentHyperlocal);
      ctrl.calendarDataHyperlocal.setDayDisabled(calendarGetDisabledHyperlocal);
            // clear calendar data
      ctrl.calendarDataHyperlocal.clearAll();
      ctrl.reservations = {};
      refreshHyperlocalCalendar();
    }

    function onAddressSelect(address) {
      // get calendar data
      ctrl.calendar = nvCalendar.getByHandle('reservations');
      ctrl.calendarData = nvCalendarData.getByHandle('reservations');
      ctrl.calendarData.setDayContent(calendarGetContent);
      ctrl.calendarData.setDayDisabled(calendarGetDisabled);
      // clear calendar data
      ctrl.calendarData.clearAll();
      ctrl.reservations = {};
      // select address
      ctrl.selectedAddress = address;
      refreshCalendar();
    }

    function onDayClicked(date) {
      setMultipleRsvn(false);
      return !isReservedDate(date);
    }

    function onDayClickedHyperlocal() {
      return true;
    }

    function checkHyperlocalReservation(dates) {
      let reserved = false;
      _.forEach(dates, (d) => {
        reserved = reserved || isReservedDate(d);
      });
      return reserved;
    }

    function showEditReservation(reservation) {
      ctrl.formShowing = 'edit';
      ctrl.selectedReservation = reservation;
      if (reservation.activeWaypoint.timewindowId === -3) {
        reservation.activeWaypoint.timewindowId = 3;
      } else if (reservation.activeWaypoint.timewindowId === -2) {
        reservation.activeWaypoint.timewindowId = 2;
      }
      scrollTo('reservation-edit-container');
    }

    function showCreateReservation() {
      ctrl.formShowing = 'create';
      scrollTo('reservation-create-container');
    }

    function openListReservation(isRefresh = false) {
      if (!ctrl.tableParam || !isRefresh) {
        ctrl.tableParam = nvTable.createTable(Reservation.TABLE_FIELDS);
      }
      ctrl.createForm.selectedDate = _.sortBy(ctrl.createForm.selectedDate, ['_d'], ['asc']);
      ctrl.reservationData = [];
      _.each(
                _.pick(ctrl.reservations, _.map(ctrl.createForm.selectedDate, date => (date.format('YYYY-M-D')))),
                a => (ctrl.reservationData = _.concat(ctrl.reservationData, a))
            );
      ctrl.groupedReservations = _.groupBy(ctrl.reservationData, 'reservationType');
      _.each(ctrl.reservationData, (reservation) => {
        addRsvnTableField(reservation);
        reservation.formattedDate = $filter('nvTime')(reservation._momentReadyDatetime, 'operator', 'datetime');
      });
      if (isRefresh) {
        ctrl.tableParam.mergeIn(ctrl.reservationData, 'id');
      } else {
        ctrl.tableParam.setData(ctrl.reservationData);
      }
      ctrl.formShowing = 'list';
      scrollTo('reservation-list-container');
    }

    function fetchHyperlocalReservation() {
      const param = {
        startDate: ctrl.calendarHyperlocal.getFirstDayInCalendar()
            .format(nvDateTimeUtils.FORMAT),
        endDate: ctrl.calendarHyperlocal.getLastDayInCalendar()
            .format(nvDateTimeUtils.FORMAT),
        shipperId: ctrl.hyperlocal_shipper_ids,
      };
      ctrl.getShipperReservationPromise = Reservation.search(param).then(success);
      return ctrl.getShipperReservationPromise;
      function success(reservations) {
        ctrl.reservations = extendReservation(reservations);
        ctrl.getShipperReservationPromise = null;
      }
    }


    function fetchShipperReservation() {
      const param = {
        shipper_id: ctrl.selectedShipper.id,
      };
      if (!ctrl.selectedShipper.hyperlocal) {
        param.address_id = ctrl.selectedAddress.id;
        param.start_date = ctrl.calendar.getFirstDayInCalendar().format(nvDateTimeUtils.FORMAT);
        param.end_date = ctrl.calendar.getLastDayInCalendar().format(nvDateTimeUtils.FORMAT);
      } else {
        param.start_date = ctrl.calendarHyperlocal.getFirstDayInCalendar()
            .format(nvDateTimeUtils.FORMAT);
        param.end_date = ctrl.calendarHyperlocal.getLastDayInCalendar()
            .format(nvDateTimeUtils.FORMAT);
      }
      ctrl.getShipperReservationPromise = Reservation.read(param).then(success);
      return ctrl.getShipperReservationPromise;

      function success(reservations) {
        ctrl.reservations = extendReservation(reservations);
        ctrl.getShipperReservationPromise = null;
      }
    }

    function extendReservation(rsvns) {
      return _(rsvns)
        .map(r => _.assign(r, {
          displayTime: moment => nvDateTimeUtils.displayFormat(moment, 'h:mm A', nvTimezone.getOperatorTimezone()),
          displayDate: moment => nvDateTimeUtils.displayFormat(moment, 'D MMMM Y (dddd)', nvTimezone.getOperatorTimezone()),
        }))
        .groupBy(r => (r._momentReadyDatetime.format('Y-M-D')))
        .value();
    }

    function getDpName(dpId) {
      const dp = ctrl.dpNameMap[dpId];
      if (dp) {
        return dp;
      }
      return '-';
    }

    function isReservedDate(date) {
      return angular.isDefined(ctrl.reservations[date.format('Y-M-D')]) && ctrl.reservations[date.format('Y-M-D')].length > 0;
    }

    function isBlockedDate(date) {
            // check if it is in blocked list
      return findDate(ctrl.blockedDates, date);
    }

    function isPastDate(date) {
      const currentDate = new Date();
      currentDate.setHours(0, 0, 0, 0);
      return date < currentDate;
    }

    function findDate(arr, date) {
      return nvDateTimeUtils.compareDate(arr[_.sortedIndex(arr, date)], date);
    }

    function isValidTimewindow(timewindowId, startEndTime) {
      const timewindow = Timewindow.getTimewindowById(timewindowId);
      const timewindowFrom = timewindow.fromTime;
      const timewindowTo = timewindow.toTime;

      timewindowFrom
        .year(startEndTime.start.year())
        .month(startEndTime.start.month())
        .date(startEndTime.start.date());
      timewindowTo
        .year(startEndTime.end.year())
        .month(startEndTime.end.month())
        .date(startEndTime.end.date());

      if (!(startEndTime.start.isSameOrAfter(timewindowFrom) &&
        startEndTime.end.isSameOrBefore(timewindowTo))) {
        nvToast.error(nvTranslate.instant('container.reservations.error-same-timewindow', {
          name: timewindow.name,
        }));
        return false;
      }

      return true;
    }

    function onUpdateReservation() {
      let timewindowId = _.get(ctrl.selectedReservation, 'activeWaypoint.timewindowId');
      if (ctrl.selectedShipper.allowPremiumPickup) {
        timewindowId = -1;
      }

      if (!isValidTimewindow(timewindowId, {
        start: moment(nvDateTimeUtils.displayDateTime(
          ctrl.selectedReservation._momentReadyDatetime
        )),
        end: moment(nvDateTimeUtils.displayDateTime(
          ctrl.selectedReservation._momentLatestDatetime
        )),
      })) {
        return false;
      }

      return Reservation.update(ctrl.selectedReservation)
                .then(() => {
                  nvToast.success('Reservations updated');
                  return scrollTo('reservation-calendar-container');
                })
                .then(() => {
                  ctrl.formShowing = '';
                });
    }

    function onCreateReservation() {
      let pickupServiceLevel = Reservation.PICKUP_SERVICE_LEVEL.STANDARD;
      let timewindowId = ctrl.createForm.timewindowId;
      if (ctrl.selectedShipper.allowPremiumPickup) {
        pickupServiceLevel = Reservation.PICKUP_SERVICE_LEVEL.PREMIUM;
        timewindowId = -1;
      }

      if (!isValidTimewindow(timewindowId, {
        start: ctrl.createForm.readyTime,
        end: ctrl.createForm.latestTime,
      })) {
        return;
      }

      const data = _.map(ctrl.createForm.selectedDate, date =>
        ({
          legacy_shipper_id: ctrl.selectedShipper.id,
          pickup_start_time: Reservation
            .combineDateTimeToString(date, ctrl.createForm.readyTime),
          pickup_end_time: Reservation
            .combineDateTimeToString(date, ctrl.createForm.latestTime),
          pickup_approx_volume: ctrl.createForm.approxVolume,
          pickup_instruction: ctrl.createForm.comments,
          pickup_address_id: ctrl.selectedAddress.id,
          pickup_service_type: Reservation.PICKUP_SERVICE_TYPE.SCHEDULED,
          pickup_service_level: pickupServiceLevel,
          is_on_demand: false,
          priority_level: ctrl.createForm.priorityLevel,
          timewindow_id: timewindowId,
          disable_cutoff_validation: true,
        })
      );
      Reservation.create(data).then((responses) => {
        let hasFailedResponse = false;

        _.forEach(responses, (response) => {
          if (response.state === nvRequestUtils.PROMISE_STATE.REJECTED) {
            hasFailedResponse = true;
          }
        });

        if (hasFailedResponse) {
          nvToast.warning(nvTranslate.instant('container.reservations.reservation-not-created'));
        } else {
          nvToast.success(nvTranslate.instant('container.reservations.reservation-created'));
        }

        // clear data
        ctrl.createForm = getDefaultCreateForm();
        // refresh calendar
        refreshCalendar();

        return scrollTo('reservation-calendar-container');
      }).then(() => {
        ctrl.formShowing = '';
      });
    }

    function onDeleteReservation(event, reservation) {
      nvDialog.confirmDelete(event)
        .then(() => Reservation.updateStatus(reservation.id, {
          status_value: Reservation.STATUS.CANCEL,
        }))
        .then(() => {
          nvToast.custom.withAction(_.partial(undoDeletedReservation, reservation)).success('Reservations deleted');
          refreshCalendar();
          return scrollTo('reservation-calendar-container');
        })
        .then(() => {
          ctrl.formShowing = '';
        });
    }

    function undoDeletedReservation(reservation) {
      let pickupServiceLevel = Reservation.PICKUP_SERVICE_LEVEL.STANDARD;
      if (_.includes(allowPremiumPickupShipperIds, reservation.shipperId)) {
        pickupServiceLevel = Reservation.PICKUP_SERVICE_LEVEL.PREMIUM;
      }

      const data = {
        legacy_shipper_id: reservation.shipperId,
        pickup_start_time: Reservation.combineDateTimeToString(
          reservation._momentReadyDatetime, reservation._momentReadyDatetime
        ),
        pickup_end_time: Reservation.combineDateTimeToString(
          reservation._momentLatestDatetime, reservation._momentLatestDatetime
        ),
        pickup_approx_volume: reservation.approxVolume,
        pickup_instruction: reservation.comments,
        pickup_address_id: reservation.addressId,
        pickup_service_type: Reservation.PICKUP_SERVICE_TYPE.SCHEDULED,
        pickup_service_level: pickupServiceLevel,
        is_on_demand: false,
        priority_level: reservation.priorityLevel,
        timewindow_id: reservation.activeWaypoint.timewindowId,
        disable_cutoff_validation: true,
      };
      Reservation.create([data]).then(() => {
        nvToast.success('Reservations recreated');
        // refresh calendar
        refreshCalendar();
      });
    }

    function scrollTo(id) {
      return _.defer(() =>
                nvScroll.duScrollToElementAnimated(angular.element(document.getElementById(id)))
            );
    }

    function joinDatesToString(dates) {
      return _.map(dates, date =>
                date.format('D-M-Y')
            ).join(', ');
    }

    function clearReservationCache() {
      ctrl.calendarData.clearAll();
      ctrl.reservations = {};
    }

    function clearHyperlocalReservationCache() {
      ctrl.calendarDataHyperlocal.clearAll();
      ctrl.reservations = {};
    }


    function refreshHyperlocalCalendar() {
      clearHyperlocalReservationCache();
      fetchHyperlocalReservation();
      ctrl.calendarDataHyperlocal.refresh();
    }

    function refreshCalendar() {
      clearReservationCache();
      fetchShipperReservation();
      ctrl.calendarData.refresh();
    }

    function getDefaultCreateForm() {
      return {
        selectedDate: [],
        timewindowId: 0,
        readyTime: moment().hour(9).minute(0).second(0)
          .millisecond(0),
        latestTime: moment().hour(12).minute(0).second(0)
          .millisecond(0),
        approxVolume: '',
        comments: '',
        priorityLevel: 0,
        saveButtonState: 'clean',
      };
    }


    // ///////////////////////
    // dialogs
    // ///////////////////////


    // functions for edit reservation dialog (hyperlocal)
    function createReservation() {
      const self = this;
      if (self.state !== 'waiting') {
        self.state = 'waiting';
        const createDate = moment();
        const etaInMinutes = self.nearestDp ? self.nearestDp.estimated_minutes : 60;
        _.merge(self.reservation, {
          timeWindowId: -1,
          reservationTypeValue: 2, // hyperlocal
          readyDatetime: nvDateTimeUtils.displayDateTime(createDate, 'utc'),
          latestDatetime: nvDateTimeUtils.displayDateTime(createDate.add(etaInMinutes, 'minutes'), 'utc'),
        });

        if (self.address) {
          const address = self.address.value;
          _.merge(self.reservation, {
            address1: address.address1,
            address2: address.address2,
            latitude: address.latitude,
            longitude: address.longitude,
            postcode: address.postcode,
            country: address.country,
            city: address.city,
          });
        }

        if (self.dp) {
          _.merge(self.reservation, {
            lodgeInDistributionPointId: self.dp.value.id,
          });
        }

        return Reservation.createHyperlocal(ctrl.hyperlocal_shipper, _.castArray(self.reservation))
                .then(success);
      }
      return $q.reject();

      function success() {
        self.state = 'idle';
        nvToast.success('Reservation created');
        refreshHyperlocalCalendar();
        refreshReservationList();
        self.onCancel();
      }
    }

    function saveReservation() {
      const self = this;
      if (self.state !== 'waiting') {
        self.state = 'waiting';
        _.merge(self.reservation, {
          readyDatetime: nvDateTimeUtils.displayDateTime(moment(self.reservation.readyDatetime), 'utc'),
          latestDatetime: nvDateTimeUtils.displayDateTime(moment(self.reservation.latestDatetime), 'utc'),
        });
        if (self.address) {
          const address = self.address.value;
          _.merge(self.reservation, {
            address1: address.address1,
            address2: address.address2,
            latitude: address.latitude,
            longitude: address.longitude,
            postcode: address.postcode,
            country: address.country,
            city: address.city,
          });
        }
        if (self.dp) {
          _.merge(self.address, {
            lodgeInDistributionPointId: self.dp.value.id,
          });
        }

        return Reservation.updateHyperlocal(_.castArray(self.reservation))
                .then(success);
      }
      return $q.reject();

      function success() {
        self.state = 'idle';
        nvToast.success('Reservation updated');
        refreshHyperlocalCalendar();
        refreshReservationList();
        self.onCancel();
      }
    }

    function refreshReservationList() {
      if (ctrl.formShowing === 'list') {
        if (_.isNull(ctrl.getShipperReservationPromise)) {
          openListReservation(true);
        } else {
          ctrl.getShipperReservationPromise
                .then(_.partial(openListReservation, true));
        }
      }
    }

    function deleteReservation(reservation) {
      const self = this;
      if (self.deleteState !== 'waiting') {
        self.deleteState = 'waiting';
        return Reservation.updateStatus(reservation.id, {
          status_value: Reservation.STATUS.CANCEL,
        }).then(success);
      }
      return $q.reject();
      function success() {
        self.deleteState = 'idle';
        nvToast.success('Reservation deleted');
        refreshHyperlocalCalendar();
        refreshReservationList();
        self.onCancel();
      }
    }

    function openAddAddressDialog() {
      const self = this;
      const address = {
        country: ctrl.country.toUpperCase(),
      };

      self.addressingMode = 'add';
      self.addressObj = Shippers.mapToLocalizedAddress(address);
      self.showDialog(1);
      setupAutocompleteAddress();
    }

    function openManageAddressDialog() {
      const self = this;
      self.addressingMode = 'manage';
      self.deletedAddresses = [];
      self.updatedAddresses = [];
      self.shiperAddresses = _.map(ctrl.addresses, addr => _.assign({}, {
        displayName: `${addr.address1} ${addr.address2}`,
        value: _.cloneDeep(addr),
      }));
      self.showDialog(1);
      setupAutocompleteAddress();
      self.address = null;
    }
    // end of functions for edit reservation dialog


    // functions for manage address dialog
    function openAddressFinder(opened = true) {
      const self = this;
      self.showAddressFinder = opened;
      if (!self.map && opened) {
        self.map = nvMaps.initialize('address-map');
      }
    }

    function findAddress(query) {
      const self = this;
      const domains = nvDomain.getDomain();
      const currentCountry = domains.currentCountry;
      return Addressing.filterSearch(currentCountry, query)
                .then(success);

      function success(result) {
        self.addressResult = result;
      }
    }

    function saveSelectedAddress(address) {
      const child = this;
      const self = child.$parent;
      self.addressObj = _.assign(self.addressObj, Shippers.mapToLocalizedAddress(
        Address.addressingApiToShipperAddress(address)
      ));
      self.showAddressFinder = false;
    }

    function addAddress(address, shipperId) {
      const self = this;
      if (self.cityObj) {
        address.city = self.cityObj.value;
      }
      const genericAddress = Shippers.mapToInternalAddress(address);
      return Shippers.createAddress(shipperId, genericAddress)
                .then(success);

      function success(result) {
        nvToast.success(nvTranslate.instant('container.reservations.addressing.success-add-address'));
        nvUtils.mergeAllIn(ctrl.addresses, _.castArray(result), 'id');
        self.showDialog(0);
        setupAutocomplete({ address: result });
        const addressOption = {
          value: result,
          displayName: `${result.address1} ${result.address2}`,
        };
        self.address = addressOption;
      }
    }

    function updateAddress() {
      const self = this;

      $q.all([
        deleteAddresses(),
        updateAddresses(),
      ]).then(
                () => {
                  $timeout(() => {
                    nvToast.success(nvTranslate.instant('container.reservations.addressing.success-update-address'));
                    self.showDialog(0);
                    const selectedAddresses = ctrl.addressDataService.getSelectedOptions();
                    let address = null;
                    if (selectedAddresses && selectedAddresses.length > 0) {
                      address = selectedAddresses[0];
                    }
                    Shippers.readAddresses(self.shipper.global_id).then((result) => {
                      ctrl.addresses = result;
                      setupAutocomplete({ address: address });
                      self.address = address;
                    });
                  }, 500);
                }
            );

      function updateAddresses() {
        if (self.updatedAddresses.length > 0) {
          return _.each(self.updatedAddresses, (address) => {
            Shippers.updateAddress(ctrl.selectedShipper.global_id, address.id, address);
          });
        }
        return $q.resolve(true);
      }
      function deleteAddresses() {
        if (self.deletedAddresses.length > 0) {
          return _.each(self.deletedAddresses, (address) => {
            Shippers.deleteAddress(ctrl.selectedShipper.global_id, address.id);
          });
        }
        return $q.resolve(true);
      }
    }

    function onChangeLatLong() {
      const self = this;
      if (self.marker) {
        nvMaps.removeMarker(self.map, self.marker);
      }
      if (self.mapLatitude && self.mapLongitude) {
        // create marker
        let options = {
          latitude: self.mapLatitude,
          longitude: self.mapLongitude,
          draggable: true,
        };
        const colorConfig = {
          markerColorCode: 'c67b1e',
          size: 'R',
        };

        options = _.merge(options, nvMaps.getIconData(colorConfig));
        self.marker = nvMaps.addMarker(self.map, options);
        nvMaps.flyToLocation(self.map, {
          latitude: self.mapLatitude,
          longitude: self.mapLongitude,
        });
        self.marker.on('drag', () => {
          $timeout(() => {
            const latLong = self.marker.getLatLng();
            self.mapLatitude = latLong.lat;
            self.mapLongitude = latLong.lng;
          }, 0);
        });
      }
    }

    function onChangeFinderMode(val) {
      const self = this;
      if (val === '01') {
        $timeout(() => {
          self.map.invalidateSize();
        }, 0);
      }
    }

    function reverseGeo(lat, lng) {
      const self = this;
      return nvMaps.reverseGeocoding(lat, lng)
                .then(success);

      function success(result) {
        const feature = result[0];
        if (!self.addressObj) {
          self.addressObj = {};
        }
        if (feature) {
          self.addressObj.address1 = feature.address1;
          self.addressObj.address2 = feature.address2;
        }
        self.addressObj.latitude = self.mapLatitude;
        self.addressObj.longitude = self.mapLongitude;
        self.showAddressFinder = false;
        self.isSearchByName = '10';
      }
    }

    function saveCurrentAddress(address) {
            // check if any changes
      if (!address) {
        return;
      }
      const self = this;
            // find address in the address list
      const oldAddress = _.find(ctrl.addresses, addr => addr.id === address.id);
      if (oldAddress && !address.toDelete) {
                // compare if changed
        const isUpdated = !_.isEqual(address, oldAddress);
        if (isUpdated) {
          if (self.updatedAddresses.length > 0) {
            nvUtils.mergeAllIn(self.updatedAddresses, _.castArray(address), 'id');
          } else {
            self.updatedAddresses.push(address);
          }
        }
      }
    }

    function deleteAddress(address) {
      const self = this;
      address.toDelete = true;
      self.toDelete = true;
      if (self.deletedAddresses.length > 0) {
        nvUtils.mergeAllIn(self.deletedAddresses, _.castArray(address), 'id');
      } else {
        self.deletedAddresses.push(address);
      }
    }

    function undoDeleteAddress(address) {
      const self = this;
      address.toDelete = false;
      self.toDelete = false;
      _.remove(self.deletedAddresses, addr => addr.id === address.id);
    }

    function setupAutocompleteAddress() {
      if (ctrl.country === 'id' || ctrl.country === 'my') {
        const cityDataService = nvAutocompleteData.getByHandle('city-search');
        cityDataService.setPossibleOptions(_.map(ctrl.cities, city => ({
          value: city,
          displayName: city,
        })));
      }
    }

    // end of function for manage address dialog
    function setupAutocomplete({ address = null } = {}) {
      ctrl.addressDataService = nvAutocompleteData
              .getByHandle('pickup-address');
      ctrl.addressDataService.setPossibleOptions(_.map(ctrl.addresses, addr => ({
        value: addr,
        displayName: `${addr.address1} ${addr.address2}`,
      }))).then(() => {
        if (address) {
          const addressOption = _.find(
            ctrl.addressDataService.getPossibleOptions(), a => (a.value.id === address.id));
          $timeout(() => {
            ctrl.addressDataService.select(addressOption);
          }, 0);
        }
      });
    }

    function setupAutoCompleteDp(scope, reservation) {
      ctrl.dpDataService = nvAutocompleteData.getByHandle('dp');
      ctrl.dpDataDpOptions = _.map(ctrl.distributionPoints, dp => (
        { value: dp, displayName: dp.name }
      ));
      ctrl.dpDataService.setPossibleOptions(_.cloneDeep(ctrl.dpDataDpOptions)).then(() => {
        if (reservation && reservation.lodgeInDistributionPointId) {
          const selectedDp = _.find(ctrl.dpDataService.getPossibleOptions(),
            dp => (dp.value.id === reservation.lodgeInDistributionPointId));
          ctrl.dpDataService.select(selectedDp);
          scope.dp = selectedDp;
        }
      });
    }

    function onManualChangeDp(dp) {
      ctrl.hyperlocal_shipper = dp ? dp.value.shipper_id : null;
      if (!ctrl.isManualChangeDp) {
        return $q.reject();
      }
      const self = this;
      // reset if nothing selected
      if (ctrl.dpDataService.getSelectedOptions().length === 0) {
        self.nearestDp = null;
        self.reservation.routeGroupId = null;
        return $q.reject();
      }
      self.reservation.lodgeInDistributionPointId = dp.value ? dp.value.id : null;
      return Overwatch.getETA(dp.value.latitude, dp.value.longitude)
        .then((result) => {
          const data = result.data;
          if (data.shipper_id === dp.value.shipper_id) {
            self.nearestDp = data;
            self.nearestDp.route_group_ids = _.map(self.nearestDp.route_group_ids,
                     id => ({ displayName: id, value: id }));
            const groupId = self.nearestDp.route_group_ids[0] ?
              self.nearestDp.route_group_ids[0].value : null;
            if (self.reservation.routeGroupId !== groupId) {
              self.reservation.routeGroupId = groupId;
            }
          }
        });
    }

    function findNearestDp(address) {
      const self = this;
      ctrl.isManualChangeDp = false;
      if (!address || !address.value) {
        return $q.reject();
      }

      return Overwatch.getETA(address.value.latitude, address.value.longitude)
              .then((result) => {
                self.nearestDp = result.data;
                self.nearestDp.route_group_ids = _.map(self.nearestDp.route_group_ids,
                     id => ({ displayName: id, value: id }));
                if (!self.firstRunEdit) {
                  self.reservation.routeGroupId = null;
                  // select the dp
                  const suggestedDp = _.find(ctrl.dpDataDpOptions,
                    dp => (dp.value.id === result.data.distribution_point_id));
                  ctrl.dpDataService.select(suggestedDp);
                  self.dp = suggestedDp;
                  self.reservation.lodgeInDistributionPointId = result.data.distribution_point_id;
                }
                self.firstRunEdit = false;
                $timeout(() => {
                  const groupId = self.nearestDp.route_group_ids[0] ?
                    self.nearestDp.route_group_ids[0].value : null;
                  self.reservation.routeGroupId = groupId;
                  ctrl.isManualChangeDp = true;
                }, 250);
              });
    }


    function openEditDialog($event, reservation) {
      const reservationObj = mapToReservationRequest(reservation);
      let selectedAddress;
      let addressOption;
      if (reservation.address) {
        selectedAddress = reservation.address;
      } else if (reservation.activeWaypoint) {
        selectedAddress = _.find(ctrl.addresses, {
          latitude: reservation.activeWaypoint.latitude ?
                        reservation.activeWaypoint.latitude.toString() : '0',
          longitude: reservation.activeWaypoint.longitude ?
                        reservation.activeWaypoint.longitude.toString() : '0',
        });
        if (selectedAddress) {
          addressOption = {
            value: selectedAddress,
            displayName: `${selectedAddress.address1} ${selectedAddress.address2}`,
          };
        }
      }

      const myScope = {};
      setupAutocomplete({ address: selectedAddress });
      setupAutoCompleteDp(myScope, reservation);
      myScope.address = addressOption;
      const editDialog = {
        templateUrl: 'views/container/reservations/dialogs/create/create-reservations.dialog.html',
        cssClass: 'nv-reservation-create-container',
        theme: 'nvGreen',
        scope: _.merge(myScope,
          {
            reservation: reservationObj,
            reservationType: 'Hyperlocal',
            dps: ctrl.dps,
            routeGroupes: [],
            selectedDate: ctrl.createForm.selectedDate[0],
            shipper: ctrl.selectedShipper,
            volumes: _.map(Reservation.volumeOptions, v => ({ value: v, displayName: v })),
            createReservation: createReservation,
            saveReservation: saveReservation,
            deleteReservation: deleteReservation,
            openAddAddressDialog: openAddAddressDialog,
            openManageAddressDialog: openManageAddressDialog,
            mode: 'edit',
            nearestDp: {},
            findNearestDp: findNearestDp,
            firstRunEdit: true,
            state: 'idle',
            deleteState: 'idle',
            onManualChangeDp: onManualChangeDp,
          }),
      };

      const manageAddressDialog = {
        templateUrl: 'views/container/reservations/dialogs/address/address.dialog.html',
        cssClass: 'nv-reservation-edit-address-container',
        theme: 'nvGreen',
        scope: _.merge(myScope,
          {
            openAddressFinder: openAddressFinder,
            showAddressFinder: false,
            addressSource: addressSource,
            addressTypes: addressTypes,
            countries: countries,
            findAddress: findAddress,
            isSearchByName: '10',
            addressResult: [],
            saveSelectedAddress: saveSelectedAddress,
            onChangeLatLong: onChangeLatLong,
            onChangeFinderMode: onChangeFinderMode,
            updateAddress: updateAddress,
            addAddress: addAddress,
            cities: ctrl.cities,
            addresses: ctrl.addresses,
            reverseGeo: reverseGeo,
            saveCurrentAddress: saveCurrentAddress,
            undoDeleteAddress: undoDeleteAddress,
            deleteAddress: deleteAddress,
            deletedAddresses: [],
            updatedAddresses: [],
          }),
      };

      return nvDialog.showMultiple($event, [
        editDialog, manageAddressDialog,
      ]);
    }

    function openAddDialog($event) {
      const reservationObj = {};
      const myScope = {};
      setupAutocomplete();
      setupAutoCompleteDp(myScope, null);
      const createDialog = {
        templateUrl: 'views/container/reservations/dialogs/create/create-reservations.dialog.html',
        cssClass: 'nv-reservation-create-container',
        theme: 'nvGreen',
        scope: _.merge(myScope,
          {
            reservation: reservationObj,
            reservationType: 'Hyperlocal',
            dps: ctrl.dps,
            routeGroupes: [],
            selectedDate: ctrl.createForm.selectedDate[0],
            shipper: ctrl.selectedShipper,
            volumes: _.map(Reservation.volumeOptions, v => ({ value: v, displayName: v })),
            createReservation: createReservation,
            saveReservation: saveReservation,
            deleteReservation: deleteReservation,
            openAddAddressDialog: openAddAddressDialog,
            openManageAddressDialog: openManageAddressDialog,
            mode: 'add',
            findNearestDp: findNearestDp,
            nearestDp: {},
            state: 'idle',
            onManualChangeDp: onManualChangeDp,
          }),
      };
      const manageAddressDialog = {
        templateUrl: 'views/container/reservations/dialogs/address/address.dialog.html',
        cssClass: 'nv-reservation-edit-address-container',
        theme: 'nvGreen',
        scope: _.merge(myScope,
          {
            openAddressFinder: openAddressFinder,
            showAddressFinder: false,
            addressSource: addressSource,
            addressTypes: addressTypes,
            countries: countries,
            findAddress: findAddress,
            isSearchByName: '10',
            addressResult: [],
            saveSelectedAddress: saveSelectedAddress,
            onChangeLatLong: onChangeLatLong,
            onChangeFinderMode: onChangeFinderMode,
            updateAddress: updateAddress,
            addAddress: addAddress,
            addresses: ctrl.addresses,
            cities: ctrl.cities,
            reverseGeo: reverseGeo,
            saveCurrentAddress: saveCurrentAddress,
            undoDeleteAddress: undoDeleteAddress,
            deleteAddress: deleteAddress,
            deletedAddresses: [],
            updatedAddresses: [],
          }),
      };
      return nvDialog.showMultiple($event, [
        createDialog, manageAddressDialog,
      ]);
    }


    function mapToReservationRequest(reservationResponse) {
      if (!reservationResponse) {
        return {};
      }
      return {
        timewindowId: reservationResponse.activeWaypoint.timewindowId,
        readyDatetime: reservationResponse.readyDatetime,
        latestDatetime: reservationResponse.latestDatetime,
        approxVolume: reservationResponse.approxVolume,
        comments: reservationResponse.comments,
        reservationTypeValue: reservationResponse.reservationType === 'HYPERLOCAL' ? 2 : 1,
        lodgeInDistributionPointId: reservationResponse.lodgeInDistributionPointId,
        routeGroupId: reservationResponse.routeGroupId,
        address1: reservationResponse.activeWaypoint.address1,
        address2: reservationResponse.activeWaypoint.address2,
        latitude: reservationResponse.activeWaypoint.latitude,
        longitude: reservationResponse.activeWaypoint.longitude,
        postcode: reservationResponse.activeWaypoint.postcode,
        country: reservationResponse.activeWaypoint.country,
        city: reservationResponse.activeWaypoint.city,
        name: reservationResponse.name,
        contact: reservationResponse.contact,
        email: reservationResponse.email,
        id: reservationResponse.id,
        shipperId: reservationResponse.shipperId,
      };
    }

    function checkEnableCreateHyperlocalRsvn() {
      return ctrl.createForm.selectedDate.length > 1 ||
        !nvDateTimeUtils.compareDate(ctrl.createForm.selectedDate[0], moment());
    }

    function hideReservationList() {
      nvScroll.scrollTo('reservation-calendar-hyperlocal-container');
      ctrl.formShowing = '';
      ctrl.tableParam = null;
    }

    function addRsvnTableField(rsvn) {
      _.merge(rsvn, {
        dpName: `${rsvn.lodgeInDistributionPointId || '-'} - ${getDpName(rsvn.lodgeInDistributionPointId)}`,
        pickupAddress: rsvn.activeWaypoint ? `${rsvn.activeWaypoint.address1} ${rsvn.activeWaypoint.address2}` : '-',
      });
    }

    function setMultipleRsvn(boolean, data) {
      const currentStatus = ctrl.isMultipleReservation.status;
      if (boolean && !currentStatus) {
        ctrl.isMultipleReservation.data = data;
        ctrl.createForm.selectedDate.length = 0;
      } else if (!boolean && currentStatus) {
        ctrl.isMultipleReservation.data.select = false;
        ctrl.isMultipleReservation.data = null;
        ctrl.createForm.selectedDate.length = 0;
      }
      ctrl.isMultipleReservation.status = boolean;
    }
  }
}());
