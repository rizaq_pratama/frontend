(function controller() {
  angular
    .module('nvOperatorApp.controllers')
    .controller('ImplantedManifestController', ImplantedManifestController);
  ImplantedManifestController.$inject = [
    '$q', 'nvSoundService', 'Hub', 'Order', 'ShipperPickups', 'nvTable', 'nvTranslate',
    'nvDialog', 'nvDateTimeUtils', 'nvToast', 'nvNavGuard', '$timeout', '$scope', '$filter',
  ];


  function ImplantedManifestController(
    $q, nvSoundService, Hub, Order, ShipperPickups, nvTable, nvTranslate,
    nvDialog, nvDateTimeUtils, nvToast, nvNavGuard, $timeout, $scope, $filter) {
    const ctrl = this;
    const NOT_FOUND = 'NOT FOUND';

    // variables
    ctrl.PAGE_STATE = {
      CREATING: 'creating',
      SELECT: 'select',
      CONFIRMED: 'confirmed',
    };
    ctrl.manifestItems = [];
    ctrl.showSelectHub = true;
    ctrl.tableParam = {};
    ctrl.state = ctrl.PAGE_STATE.SELECT;
    ctrl.pageState = 'idle';
    ctrl.rackInfo = '';
    ctrl.scanReadOnly = false;
    ctrl.prefix = '';

    ctrl.sound = {
      success: {
        src: nvSoundService.getSuccessSoundFile(),
        type: 'audio/wav',
      },
      error: {
        src: nvSoundService.getErrorSoundFile(),
        type: 'audio/wav',
      },
      duplicate: {
        src: nvSoundService.getDuplicateSoundFile(),
        type: 'audio/wav',
      },
    };
    ctrl.tableParam = {};

    ctrl.hubs = [];
    ctrl.hub = null;
    ctrl.manifestItems = [];
    ctrl.loadingSheet = false;

    ctrl.csvOrderedHeaders = ['trackingId', 'createdAt', 'destination', 'addressee', 'rackSector', 'deliverBy'];
    ctrl.getCsvData = () => ctrl.tableParam.getTableData().map(data =>
      _.assign({}, data, { createdAt: $filter('nvTime')(data.createdAt * 1000, 'operator', 'datetime') })
    );

    // functions definition
    ctrl.searchTrackingId = searchTrackingId;
    ctrl.deleteTrackingId = deleteTrackingId;
    ctrl.onStartCreateManifest = onStartCreateManifest;
    ctrl.createManifest = openCreateManifestDialog;
    ctrl.removeAll = removeAll;
    ctrl.showErrorBorder = showErrorBorder;
    ctrl.showSuccessBorder = showSuccessBorder;
    ctrl.createAnother = createAnother;
    ctrl.onDelete = onDelete;
    ctrl.openPrefix = openPrefix;

    onLoad();
    function onLoad() {
      ctrl.loadingSheet = true;
      return Hub.read().then((result) => {
        ctrl.hubs = Hub.toOptionsWithObjectValue(result);
        ctrl.loadingSheet = false;
      });
    }

    function onStartCreateManifest() {
      // Have to initialize here because the translation file is lazy loaded
      const scanningFields = {
        trackingId: {
          displayName: nvTranslate.instant('container.implanted-manifest.tracking-id'),
        },
        destination: {
          displayName: nvTranslate.instant('container.implanted-manifest.destination'),
        },
        addressee: {
          displayName: nvTranslate.instant('container.implanted-manifest.addressee'),
        },
        rackSector: {
          displayName: nvTranslate.instant('container.implanted-manifest.rack-sector'),
        },
        deliverBy: {
          displayName: nvTranslate.instant('container.implanted-manifest.deliver-by'),
        },
        createdAt: {
          displayName: nvTranslate.instant('container.implanted-manifest.scanned-at'),
        },
      };

      ctrl.state = ctrl.PAGE_STATE.CREATING;
      nvNavGuard.lock();
      ctrl.tableParam = nvTable.createTable(scanningFields,
        {
          newItemOnTop: true,
          isStripped: false,
        });
      ctrl.tableParam.currSortCol = 'createdAt';
      ctrl.tableParam.showEndTableDescription = false;
      ctrl.tableParam.sort('createdAt');
      ctrl.tableParam.setData(ctrl.manifestItems);

      ctrl.csvHeaderNames = ctrl.csvOrderedHeaders.map(attr => scanningFields[attr].displayName);
      ctrl.csvFileName = `implanted-manifest-${ctrl.hub.name.replace(/\s+/g, '-')}.csv`;
    }

    function openPrefix($event) {
      return nvDialog.showSingle($event, {
        templateUrl: 'views/container/global-inbound/dialogs/prefix/prefix.html',
        theme: 'nvGreen',
        cssClass: 'nv-global-inbound-prefix',
        controllerAs: 'ctrl',
        controller: 'GlobalInboundPrefixDialogController',
        locals: {
          data: { prefix: ctrl.prefix },
        },
      }).then((result) => {
        ctrl.prefix = result.prefix.trimStart();
      });
    }

    function searchTrackingId(event) {
      if (event && event.charCode !== 13 && event.charCode !== 9) {
        return $q.reject;
      }

      if (!ctrl.trackingId || ctrl.trackingId === '') {
        return $q.reject;
      }
      // check if order already put into the table
      if (checkIsOrderAlreadyInTable(ctrl.trackingId)) {
        playDuplicateSound();
        ctrl.trackingId = null;
        nvToast.warning(nvTranslate.instant('container.implanted-manifest.duplicate-tracking-id'));
        return $q.reject();
      }
      ctrl.scanReadOnly = true;

      return Order.getByScan(ctrl.prefix + ctrl.trackingId)
        .then(onSuccess, onFailure);

      function onSuccess(order) {
        ctrl.scanReadOnly = false;
        ctrl.scannedTrackingId = _.clone(ctrl.prefix + ctrl.trackingId);
        if (order) {
          ctrl.tableParam.mergeIn({
            trackingId: order.tracking_id,
            destination: `${order.to_address1} ${order.to_address2}`,
            addressee: order.to_name,
            rackSector: order.sector,
            deliveryBy: order.delivery_end_date ? nvDateTimeUtils.displayFormat(moment(order.delivery_end_date)) : '-',
            createdAt: moment().unix(),
          }, 'trackingId');
          ctrl.rackInfo = order.sector;
          playRackSound(true);
        } else {
          ctrl.tableParam.mergeIn({
            trackingId: ctrl.prefix + ctrl.trackingId,
            destination: NOT_FOUND,
            addressee: NOT_FOUND,
            rackSector: NOT_FOUND,
            deliveryBy: '-',
            createdAt: moment().unix(),
          }, 'trackingId');
          ctrl.rackInfo = '-';
          playErrorSound();
        }
        ctrl.trackingId = null;
        ctrl.tableParam.refreshData();
      }

      function onFailure() {
        ctrl.scanReadOnly = false;
        nvToast.error(nvTranslate.instant('container.implanted-manifest.unknown-tracking-id'));
        ctrl.tableParam.mergeIn({
          trackingId: ctrl.prefix + ctrl.trackingId,
          destination: NOT_FOUND,
          addressee: NOT_FOUND,
          rackSector: NOT_FOUND,
          deliveryBy: '-',
          createdAt: moment().unix(),
        }, 'trackingId');
        playErrorSound();
        ctrl.trackingId = null;
      }
    }

    function checkIsOrderAlreadyInTable(trackingId) {
      const trackingIds = _.map(ctrl.tableParam.getTableData(), row => (row.trackingId));
      const index = _.indexOf(trackingIds, trackingId);
      return index > -1;
    }
    function deleteTrackingId(event) {
      if (event && event.charCode !== 13 && event.charCode !== 9) {
        return $q.reject;
      }

      if (!ctrl.removeTrackingId || ctrl.removeTrackingId === '') {
        return $q.reject;
      }
      const toRemove = {
        trackingId: ctrl.prefix + ctrl.removeTrackingId,
      };
      ctrl.removeTrackingId = null;
      return ctrl.tableParam.remove(toRemove, 'trackingId');
    }

    function removeAll() {
      return nvDialog.confirmDelete().then(() => {
        ctrl.tableParam.setData([]);
        ctrl.tableParam.refreshData();
        nvToast.info(nvTranslate.instant('container.implanted-manifest.all-scanned-item-has-been-deleted'));
      });
    }
    /**
     * Open the create manifest dialog
     * @param {Event} $event
     */
    function openCreateManifestDialog($event) {
      ctrl.manifestItems = ctrl.tableParam.getTableData();
      return nvDialog.showSingle($event, {
        templateUrl: 'views/container/implanted-manifest/dialog/implanted-manifest-confirm.dialog.html',
        cssClass: 'implanted-manifest-confirm',
        controller: 'ImplantedManifestConfirmDialogController',
        controllerAs: 'ctrl',
      }).then(success);

      function success(reservationId) {
        ctrl.loadingSheet = true;
        // get the pod based on data/ reservation id
        return ShipperPickups.getPod(reservationId).then((result) => {
          if (result) {
            if (result.status !== 'SUCCESS') {
              ctrl.loadingSheet = false;
              nvToast.error(nvTranslate.instant('contianer.implanted-manifest.not-a-success-reservation'));
              openCreateManifestDialog();
              return $q.reject();
            }

            const pod = _.last(result.pods);
            if (pod) {
              // check if pod already contains scans
              if (pod.shipper_scans && pod.shipper_scans.length !== 0) {
                ctrl.loadingSheet = false;
                nvToast.error(nvTranslate.instant('container.implanted-manifest.no-pod-available'));
                return $q.reject();
              }

              // calculate the length
              if (ctrl.manifestItems.length !== pod.submitted_scans_quantity) {
                // boom, not match
                ctrl.loadingSheet = false;
                nvToast.error(nvTranslate.instant('container.implanted-manifest.pod-and-manifest-parcel-count-do-not-match'));
                return $q.reject();
              }

              const trackingIds = _.map(ctrl.manifestItems, item => (item.trackingId));
              // send in chunks
              const dataInChunk = _.chunk(trackingIds, 200);
              return shiftAndSend(dataInChunk, pod).then(onFinishUpdateManifest);
            }
            ctrl.loadingSheet = false;
            nvToast.error(nvTranslate.instant('container.implanted-manifest.pod-not-available-please-re-enter-reservation-id'));
            openCreateManifestDialog();
            return $q.reject();
          }
          ctrl.loadingSheet = false;
          nvToast.error(nvTranslate.instant('container.implanted-manifest.reservtion-id-not-found'));
          openCreateManifestDialog();
          return $q.reject();
        }, () => {
          ctrl.loadingSheet = false;
          nvToast.error(nvTranslate.instant('container.implanted-manifest.reservtion-id-not-found'));
          openCreateManifestDialog();
          return $q.reject();
        });

        function shiftAndSend(dataInChunk, pod) {
          if (dataInChunk.length > 0) {
            const chunk = dataInChunk.shift();
            const payload = {
              scans: chunk,
            };
            return ShipperPickups
              .updatePodManifest(reservationId, pod.id, payload)
              .then(onFinishSend);
          }
          return $q.resolve(dataInChunk);

          function onFinishSend() {
            return shiftAndSend(dataInChunk, pod);
          }
        }
      }

      function onFinishUpdateManifest() {
        ctrl.loadingSheet = false;
        ctrl.state = ctrl.PAGE_STATE.CONFIRMED;
      }
    }

    function createAnother() {
      ctrl.manifestItems = [];
      ctrl.tableParam.setData([]);
      ctrl.tableParam.refreshData();
      ctrl.state = ctrl.PAGE_STATE.SELECT;
    }

    function showErrorBorder() {
      return ctrl.rackInfo === 'INVALID' || ctrl.rackInfo === 'DUPLICATE';
    }

    function showSuccessBorder() {
      return (ctrl.rackInfo !== '' && ctrl.rackInfo !== null
      && ctrl.rackInfo !== 'INVALID' && ctrl.rackInfo !== 'DUPLICATE');
    }

    function playRackSound(success) {
      if (ctrl.rackInfo) {
        const charArray = ctrl.rackInfo.split('');
        let playlist = _.map(charArray, o => ({
          src: nvSoundService.getRackSoundFile(o, ctrl.lang),
          type: 'audio/mp3',
        }));
        if (success) {
          playlist = _.concat(playlist, $scope.playlist1);
        } else {
          playlist = _.concat(playlist, $scope.playlist2);
        }
        $scope.rackSound.$playlist = playlist;
        $scope.playlistRack = playlist;
        return $timeout(() => {
          $scope.rackSound.play(0);
        }, 1);
      }
      return $q.reject();
    }

    function playErrorSound() {
      return $timeout(() => {
        $scope.errorSound.playPause(0);
      }, 1);
    }

    function playDuplicateSound() {
      return $timeout(() => {
        $scope.duplicateSound.playPause(0);
      }, 1);
    }

    function onDelete(event, scan) {
      ctrl.tableParam.remove(scan, 'trackingId');
    }
  }
}());
