(function controller() {
  angular
        .module('nvOperatorApp.controllers')
        .controller('ImplantedManifestConfirmDialogController', ImplantedManifestConfirmDialogController);

  ImplantedManifestConfirmDialogController.$inject = ['$mdDialog', 'Shipping',
    '$rootScope', 'nvDateTimeUtils', '$scope'];
  function ImplantedManifestConfirmDialogController($mdDialog, Shipping,
    $rootScope, nvDateTimeUtils, $scope) {
    const ctrl = this;
    ctrl.onCancel = onCancel;
    ctrl.onCreate = onCreate;

    function onCancel() {
      return $mdDialog.cancel();
    }

    function onCreate() {
      return $mdDialog.hide(ctrl.reservationId);
    }

    $scope.$watch('ctrl.date', () => {
      if (ctrl.date) {
        ctrl.dateChanged = true;
      }
    });
  }
}());
