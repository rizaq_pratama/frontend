(function controller() {
  angular
    .module('nvOperatorApp.controllers')
    .controller('RouteMonitoringPagedController', RouteMonitoringPagedController);

  function RouteMonitoringPagedController() {
    const ctrl = this;
    ctrl.url = `${nv.config.servers['operator-react']}/route-monitoring`;
  }
}());

