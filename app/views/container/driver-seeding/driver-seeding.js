(function controller() {
  angular
        .module('nvOperatorApp.controllers')
        .controller('DriverSeedingController', DriverSeedingController);

  DriverSeedingController.$inject = [
    '$q', 'Zone', 'Driver', 'nvExtendUtils', 'nvMaps',
    '$timeout', '$scope', '$state', '$mdMedia', '$window',
    'nvModelUtils', 'nvUtils', 'nvTranslate',
  ];

  function DriverSeedingController(
    $q, Zone, Driver, nvExtendUtils, nvMaps,
    $timeout, $scope, $state, $mdMedia, $window,
    nvModelUtils, nvUtils, nvTranslate) {
    // ////////////////////////////////////////////////
    // variables
    // ////////////////////////////////////////////////
    const ctrl = this;
    let zoneOptions = [];
    let prevFocusMarker = null;

    ctrl.map = null;
    ctrl.zones = [];
    ctrl.filteredZones = [];
    ctrl.drivers = [];
    ctrl.filteredDrivers = [];
    ctrl.filteredZoneToDrivers = [];
    ctrl.filters = {
      zones: {
        searchText: '',
        possibleOptions: [],
        selectedOptions: [],
        onChangeFn: onFilterChangeFn,
        filterFn: zonesFilterFn,
        isProcessing: false,
      },
    };
    ctrl.checkboxFilters = {
      inactiveDrivers: false,
      allPreferredZone: false,
      reserveFleetDrivers: false,
    };


    // ////////////////////////////////////////////////
    // functions
    // ////////////////////////////////////////////////
    ctrl.windowSizeChanged = windowSizeChanged;
    ctrl.resizeWithOffset = resizeWithOffset;
    ctrl.initializeMap = initializeMap;
    ctrl.searchAll = searchAll;
    ctrl.navigateToDriverStrengthPage = navigateToDriverStrengthPage;
    ctrl.onFilterChangeFn = onFilterChangeFn;
    ctrl.getTagStyle = getTagStyle;
    ctrl.focusMarker = focusMarker;


    // ////////////////////////////////////////////////
    // start
    // ////////////////////////////////////////////////
    ctrl.initializeMap();


    // ////////////////////////////////////////////////
    // functions details
    // ////////////////////////////////////////////////
    function initializeMap() {
      ctrl.map = nvMaps.initialize('driver-seeding-map');
    }

    function windowSizeChanged() {
      $timeout(() => {
        const contentHeight = $scope.$windowHeight - $('.nv-container > md-toolbar').outerHeight();
        $('.nv-container-driver-seeding').height(`${contentHeight}px`);
      });
    }

    function resizeWithOffset() {
      const maxHeight = 300;
      let maxHeightString = `${maxHeight}px`;
      let heightString = 'auto';
      const filtersHolderHeight = $('.filters-holder').height();
      if (filtersHolderHeight > maxHeight && $mdMedia('gt-sm')) {
        let resultsHolderHeight = filtersHolderHeight;
        resultsHolderHeight -= $('.filters-holder > div > .filter-zones').outerHeight();
        resultsHolderHeight -= $('.filters-holder > div > .md-subheader').outerHeight();
        resultsHolderHeight -= $('.filters-holder > div > md-divider').outerHeight();
        resultsHolderHeight -= $('.filters-holder > div > .filter-types').outerHeight();
        resultsHolderHeight -= $('.filters-holder > div > .filter-results > .md-subheader').outerHeight();
        resultsHolderHeight -= $('.filters-holder > div > .filter-results > .footer').outerHeight();

        maxHeightString = `${resultsHolderHeight}px`;
        heightString = `${resultsHolderHeight}px`;
        $('.nv-container-driver-seeding').parent().css('overflow', 'hidden');
      } else {
        maxHeightString = 'none';
        heightString = 'auto';
        $('.nv-container-driver-seeding').parent().css('overflow', 'auto');
      }

      return {
        'max-height': maxHeightString,
        height: heightString,
      };
    }

    function getTagStyle(zoneName, textColorOnly) {
      const activeColor = getActiveColor(zoneName);

      if (textColorOnly) {
        return { color: activeColor };
      }

      return {
        'background-color': activeColor,
        color: activeColor,
      };
    }

    function getActiveColor(zoneName) {
      return $(`.nv-container-driver-seeding .nv-button[aria-label="${zoneName}"]`).css('border-color');
    }

    function navigateToDriverStrengthPage() {
      const url = $state.href('container.driver-strength');
      $window.open(url, '_blank');
    }

    function showFilteredDriversMarker() {
      ctrl.filteredZoneToDrivers = {};
      _.forEach(ctrl.filters.zones.selectedOptions, (selectedOption) => {
        ctrl.filteredZoneToDrivers[selectedOption.displayName] = [];
      });

      _.forEach(ctrl.filteredDrivers, (driver) => {
        _.forEach(driver.zonePreferences, (zonePreference, index) => {
          const hasZone = _.some(ctrl.filters.zones.selectedOptions, selectedOption =>
           zonePreference._zoneName === selectedOption.displayName
          );

          if (hasZone && allPreferredZonesFilterFn(zonePreference)) {
            ctrl.filteredZoneToDrivers[zonePreference._zoneName].push(driver);
            addMarker(driver, index);
          }
        });
      });

      _.forEach(ctrl.filteredZoneToDrivers, (drivers, key) => {
        if (_.size(drivers) <= 0) {
          delete ctrl.filteredZoneToDrivers[key];
        }
      });
    }

    function hideFilteredDriversMarker() {
      _.forEach(ctrl.filteredDrivers, (driver) => {
        removeMarker(driver);
      });
    }

    function addMarker(driver, zonePreferencesNo) {
      if (!(driver && driver.zonePreferences && driver.zonePreferences[zonePreferencesNo])) {
        return false;
      }

      const inactiveColor = 'cccccc';
      const zonePreference = driver.zonePreferences[zonePreferencesNo];
      const activeColor = nvMaps.rgbTohex(getActiveColor(zonePreference._zoneName));

      let options = {
        latitude: zonePreference.latitude,
        longitude: zonePreference.longitude,
        draggable: true,
        label: driver._name,
        driver: driver,
        zonePreferencesNo: zonePreferencesNo,
      };

      const iconConfig = {
        type: 'd_map_xpin_letter',
      };

      if (driver.availability) {
        iconConfig.fillColor = activeColor;

        if (zonePreference.rank > 1) {
          iconConfig.style = 'pin_star';
          iconConfig.starFillColor = inactiveColor;
        }
      } else {
        iconConfig.fillColor = inactiveColor;

        if (zonePreference.rank > 1) {
          iconConfig.style = 'pin_star';
          iconConfig.starFillColor = activeColor;
        }
      }

      if (nvModelUtils.fromYesNoBitStringToBool(driver.tags.RESUPPLY)) {
        iconConfig.type = 'd_map_xpin_icon';
        iconConfig.icon = 'parking';
      }

      options = _.merge(options, nvMaps.getIconData(iconConfig));

      zonePreference._marker = nvMaps.addMarker(ctrl.map, options);
      nvMaps.markerAddEvent(zonePreference._marker, 'dragend', updateLatLng);

      const vehicleType = driver.vehicles && driver.vehicles[0] ? driver.vehicles[0].vehicleType : '-';
      const vehicleId = driver.vehicles && driver.vehicles[0] ? driver.vehicles[0].id : '-';
      const maxCapacity = driver.zonePreferences ? driver.zonePreferences[0].maxWaypoints : '0';
      const specialRequests = driver.comments !== null ? driver.comments : '-';

      let contentString = `<strong>${driver._name}</strong><br>`;
      contentString += `${nvTranslate.instant('container.driver-seeding.driver-id')}: ${driver.id}<br>`;
      contentString += `${nvTranslate.instant('container.driver-seeding.driver-type')}: ${driver.driverType}<br>`;
      contentString += `${nvTranslate.instant('container.driver-seeding.max-capacity')}: ${maxCapacity}<br>`;
      contentString += `${nvTranslate.instant('container.driver-seeding.vehicle-type')}: ${vehicleType} (${vehicleId})<br>`;
      contentString += `${nvTranslate.instant('container.driver-seeding.special-requests')}: ${specialRequests}`;

      const infoWindowData = {
        text: contentString,
      };
      if (driver.availability) {
        infoWindowData.className = 'yellow';
      }

      nvMaps.markerAddInfoWindow(ctrl.map, zonePreference._marker, infoWindowData);

      return true;
    }

    function updateLatLng(marker) {
      let driver = marker.driver || (marker.options && marker.options.driver);

      const zonePreferencesNo = marker.zonePreferencesNo ||
        (marker.options && marker.options.zonePreferencesNo);

      driver.zonePreferences[zonePreferencesNo].latitude = marker.getLatLng().lat;
      driver.zonePreferences[zonePreferencesNo].longitude = marker.getLatLng().lng;

      const data = _.cloneDeep(driver);

      _.forEach(data.zonePreferences, (zonePreference) => {
        delete zonePreference._marker;
      });

      Driver.updateOne(driver.uuid, data).then(success, failure);

      function success(result) {
        driver = extend([result])[0];
        const realDriver = _.find(ctrl.drivers, ['id', driver.id]);
        _.merge(realDriver, driver);

        ctrl.drivers = nvUtils.mergeIn(ctrl.drivers, realDriver, 'uuid', realDriver.uuid);
        filterAll();
      }

      function failure() {}
    }

    function focusMarker(driver, zoneName) {
      if (prevFocusMarker) {
        nvMaps.markerHideInfoWindow(prevFocusMarker);
        nvMaps.markerSetZIndex(prevFocusMarker, 0);
      }

      _.forEach(driver.zonePreferences, (zonePreference) => {
        if (zonePreference._zoneName === zoneName) {
          const marker = zonePreference._marker;

          // focus, show info, and set z-index to highest
          nvMaps.markerShowInfoWindow(marker);
          nvMaps.markerSetZIndex(marker, 10000);
          nvMaps.flyToLocation(ctrl.map, {
            latitude: marker.getLatLng().lat,
            longitude: marker.getLatLng().lng,
          });

          prevFocusMarker = marker;
          return false;
        }
      });
    }

    function removeMarker(driver) {
      let isRemoved = false;

      _.forEach(driver.zonePreferences, (zonePreference) => {
        if (zonePreference._marker) {
          nvMaps.removeMarker(ctrl.map, zonePreference._marker);
          zonePreference._marker = undefined;

          isRemoved = true;
        }
      });

      return isRemoved;
    }

    function searchAll() {
      return read().then(success, $q.reject);

      function read() {
        return $q.all([Zone.read(true), Driver.searchAll()]);
      }

      function success(results) {
        const zoneResults = results[0];
        const driverResults = results[1];

        Driver.setOptions('zonePreferences.zoneId', Zone.toOptions(zoneResults, false));
        zoneOptions = Driver.getOptions('zonePreferences.zoneId');

        ctrl.filters.zones.possibleOptions = Zone.toFilterOptions(zoneResults);

        const drivers = extend(driverResults.drivers);
        ctrl.drivers = _.sortBy(ctrl.drivers.concat(drivers), 'id');
        ctrl.filteredDrivers = [];

        ctrl.zones = zoneResults;
        _.forEach(ctrl.zones, (zone) => {
          const routingZone = _.find(zoneResults, ['id', zone.id]);
          if (routingZone) {
            _.merge(zone, { _routingZone: routingZone });
          }
        });
      }
    }

    function filterAll() {
      $timeout(() => {
        hideFilteredDriversMarker();

        const filteredData = _.reduce(ctrl.filters, reduceFn, ctrl.drivers);

        ctrl.filteredDrivers = _.filter(filteredData, (driver) => {
          if (!ctrl.checkboxFilters.inactiveDrivers) {
            if (!driver.availability) {
              return false;
            }
          }

          if (!ctrl.checkboxFilters.reserveFleetDrivers) {
            if (!(!driver.tags ||
              nvModelUtils.fromYesNoBitStringToBool(driver.tags.RESUPPLY) === false)
            ) {
              return false;
            }
          }

          return true;
        });

        ctrl.filteredDrivers = sortFilteredDrivers(ctrl.filteredDrivers);
        showFilteredDriversMarker(); // All Preferred Zones filter is inside this function

        function reduceFn(acc, val) {
          return val.selectedOptions.length > 0 ? _.filter(
            acc, _.partial(val.filterFn, val.selectedOptions)
          ) : [];
        }
      }, 0);
    }

    function hideFilteredZonesPolygon() {
      _.forEach(ctrl.filteredZones, (zone) => {
        if (!zone._polygon) {
          return;
        }

        nvMaps.removePolygon(ctrl.map, zone._polygon);
        zone._polygon = undefined;
      });
    }

        /** @namespace zone._routingZone.vertices */
    function showFilteredZonesPolygon() {
      _.forEach(ctrl.filteredZones, (zone) => {
        if (!zone._routingZone) {
          return;
        }

        const latLngs = [];
        _.forEach(getVertices(zone._routingZone), (vertice) => {
          latLngs.push([vertice[1], vertice[0]]);
        });
        const polygonData = {
          latLngs: latLngs,
          color: `#${nvMaps.rgbTohex(getActiveColor(zone.name))}`,
          zone: zone,
        };
        zone._polygon = nvMaps.addPolygon(ctrl.map, polygonData);
      });

      function getVertices(z) {
        let geometry = _.get(z, 'polygon.geometry');
        if (!geometry) {
          return [];
        }
        if (geometry.type === 'Polygon' ) {
          return geometry.coordinates[0];
        } else if(geometry.type === 'MultiPolygon') {
          return geometry.coordinates[0][0];
        }
        return [];
      }
    }

    function handlePolygons() {
      $timeout(() => {
        hideFilteredZonesPolygon();

        ctrl.filteredZones = _.filter(ctrl.zones, zone =>
          _.some(ctrl.filters.zones.selectedOptions, selectedOption =>
            zone.id === selectedOption.id
          )
        );

        showFilteredZonesPolygon();
      }, 0);
    }

    function onFilterChangeFn() {
      ctrl.filters.zones.isProcessing = true;

      $timeout(() => {
        handlePolygons();
        filterAll();
        ctrl.filters.zones.isProcessing = false;
      }, 0);
    }

    function zonesFilterFn(selectedOptions, driver) {
      return _.some(driver.zonePreferences, zonePreference =>
        _.some(selectedOptions, _.partial(compareId, zonePreference.zoneId))
      );
    }

    function compareId(id, selectedOption) {
      return id === selectedOption.id;
    }

    function allPreferredZonesFilterFn(zonePreference) {
      // hide non primary marker (rank > 1) if 'All Preferred Zones' is uncheck (return false)
      return !(!ctrl.checkboxFilters.allPreferredZone && zonePreference.rank > 1);
    }

    function extend(drivers) {
      return _.map(drivers, (driver) => {
        _.forEach(driver.vehicles, (vehicle) => {
          _.merge(vehicle, { _ownVehicle: vehicle.ownVehicle === '10' });
        });

        driver.vehicles = nvExtendUtils.addValueForBoolean(driver.vehicles, '_ownVehicle', { true: 'Yes', false: 'No' });
        driver._name = [driver.firstName, driver.lastName].join(' ');

        driver.zonePreferences = _.sortBy(driver.zonePreferences, 'rank');
        _.forEach(driver.zonePreferences, (zonePreference) => {
          const zone = _.find(zoneOptions, ['value', zonePreference.zoneId]);
          if (zone) {
            _.merge(zonePreference, { _zoneName: zone.displayName });
          }
        });

        return driver;
      });
    }

    function sortFilteredDrivers(filteredDrivers) {
      return _.orderBy(filteredDrivers, 'id');
    }
  }
}());
