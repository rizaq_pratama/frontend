(function() {
    'use strict';

    angular
        .module('nvOperatorApp.controllers')
        .controller('DriverWaypointsMapController', DriverWaypointsMapController);

    DriverWaypointsMapController.$inject = ['$http', '$timeout', '$q', 'nvRequestUtils', 'nvGoogleMaps', 'nvAddress'];

    function DriverWaypointsMapController($http, $timeout, $q, nvRequestUtils, nvGoogleMaps, nvAddress) {
        var ctrl = this;
        ctrl.loadForever = $q.defer().promise;
        ctrl.getDrivers = getDrivers;
        ctrl.drivers = [];
        ctrl.onSelectDriver = onSelectDriver;
        ctrl.isLoading = false;
        ctrl.currentDriver = {};

        //////////////////////////////////////////////////

        var map = new google.maps.Map(document.getElementById('google-map'), {
            zoom: nvGoogleMaps.getDefaultZoomLevelForDomain(),
            center: nvGoogleMaps.getDefaultLatLngForDomain(),
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            mapTypeControl: false,
            streetViewControl: false
        });

        var infoWindow = new google.maps.InfoWindow();

        var legend = document.getElementById('google-map-legend');
        map.controls[google.maps.ControlPosition.RIGHT_TOP].push(legend);

        var drivers = document.getElementById('google-map-drivers');
        map.controls[google.maps.ControlPosition.LEFT_TOP].push(drivers);

        google.maps.event.addListener(map, 'idle', function() {
            $timeout(function() {
                ctrl.isLoading = false;
            });
        });

        var markerSets = {};

        //////////////////////////////////////////////////

        function getDrivers() {
            return $http.get('core/drivers').then(
                nvRequestUtils.success,
                nvRequestUtils.nvFailure
            ).then(function(results) {
                ctrl.drivers = _.map(results, function(result) {
                    var driver = _.pick(result, ['id', 'name']);
                    driver._lowercaseName = driver.name && driver.name.toLowerCase();
                    return driver;
                });
                return ctrl.drivers;
            });
        }

        function onSelectDriver(driver) {
            if (ctrl.currentDriver.id !== driver.id) {
                $timeout(function() {
                    getRoutes(driver);
                }, 100); // for animation smoothing
            }
        }

        function getRoutes(driver) {
            ctrl.isLoading = true;

            $http.get('core/drivers/v2/' + driver.id + '/routes').then(
                nvRequestUtils.success,
                nvRequestUtils.nvFailure
            ).then(function(routes) {
                clearMarkers(markerSets);
                addMarkers(map, routes);
                ctrl.currentDriver = driver;
            }, function() {
                ctrl.isLoading = false;
            });
        }

        function clearMarkers(markerSets) {
            _.forIn(markerSets, function(markerSet) {
                nvGoogleMaps.clearMarkers(markerSet);
            });
        }

        function addMarkers(map, routes) {
            var bounds = new google.maps.LatLngBounds();

            _.forEach(routes, function(route) {
                markerSets[route.id] = [];

                _.forEach(route.waypoints, function(wp) {
                    var marker = nvGoogleMaps.addPulsingMarker(map, wp.latitude, wp.longitude, wp.address1, {
                        color: getColor(wp)
                    });

                    bounds.extend(marker.position);

                    marker.addListener('click', function() {
                        infoWindow.setContent(
                            '<h5><span class="nv-font-weight-light">Waypoint Id:</span> ' + wp.id + '</h5>' +
                            '<div>Address: <strong>' + nvAddress.extract(wp) + '</strong></div>' +
                            '<div>Status: <strong>' + wp.status + '</strong></div>' +
                            '<div>Time Window: <strong>' + wp.timeWindow + '</strong></div>'
                        );
                        infoWindow.open(map, marker);
                    });

                    markerSets[route.id].push(marker);
                });
            });

            map.fitBounds(bounds);
        }

        function getColor(waypoint) {
            switch (waypoint.status) {
                case 'PENDING':
                    return nvGoogleMaps.MarkerColor.ORANGE;
                case 'ROUTED':
                    return nvGoogleMaps.MarkerColor.BLUE;
                case 'FAIL':
                    return nvGoogleMaps.MarkerColor.RED;
                case 'SUCCESS':
                    return nvGoogleMaps.MarkerColor.GREEN;
                default:
                    return nvGoogleMaps.MarkerColor.RED;
            }
        }
    }

})();
