(function Controller() {
  angular
    .module('nvOperatorApp.controllers')
    .controller('RecoveryTicketsController', controller)
    ;

  controller.$inject = [
    '$q', '$scope', 'Ticketing',
    'nvDialog', '$rootScope', 'nvToast',
    'nvTable', 'nvBackendFilterUtils', 'nvDateTimeUtils',
    'Order', '$window', 'nvTranslate',
    'nvTimezone', '$state', 'Hub', 'hotkeys',
    'Shippers', 'nvUtils',
  ];

  /* eslint no-underscore-dangle: ["error", { "allow": ["_isNew", "_createdAt"] }]*/

  function controller($q, $scope, Ticketing, nvDialog,
    $rootScope, nvToast,
    nvTable, nvBackendFilterUtils,
    nvDateTimeUtils, Order,
    $window, nvTranslate,
    nvTimezone, $state, Hub, hotkeys,
    Shippers, nvUtils
  ) {
    const ctrl = this;
    // ////////////
    // CONSTANTS
    // ////////////
    const RESOLVED = 'RESOLVED';
    const CANCELLED = 'CANCELLED';
    const PENDING = 'PENDING';
    const IN_PROGRESS = 'IN PROGRESS';
    const ON_HOLD = 'ON HOLD';
    const PAGE_STATE = {
      IDLE: 'idle',
      WAITING: 'waiting',
      FAILED: 'failed',
      LOADING: 'loading',
    };
    const hubNameMap = {};
    let hubsResult = null;
    let groupsResult = null;
    let statusResult = null;
    let typeResult = null;
    let entrySourceResult = null;
    let userResult = null;
    let subTypeResult = null;
    let usersData = [];
    let ticketStatusesFilter = [];
    let pendingAndInProgressIds = [];
    let otherIds = [];
    // //////////////
    // VARIABLES
    // //////////////
    ctrl.state = PAGE_STATE.IDLE;
    ctrl.searchText = '';
    ctrl.searchFilter = {};
    ctrl.showFilter = true;
    ctrl.hubs = [];
    ctrl.hub = 0;
    ctrl.hideOthers = false;
    ctrl.hideResolved = false;
    ctrl.unassigned = false;
    ctrl.query = null;
    ctrl.endOfPage = false;
    ctrl.timezone = nvTimezone.getOperatorTimezone();
    ctrl.params = {};
    ctrl.bulkEnabled = false;
    ctrl.userSelectItem = false;
    ctrl.bulkUpdate = bulkUpdate;
    ctrl.openUploadCsvDialog = openUploadCsvDialog;
    ctrl.additionalMenu = [
      {
        label: 'container.recovery-tickets.show-red-tickets-first',
        onClick: orderTableByColor,
      },
    ];

    ctrl.csvFileName = 'recovery.ticket.csv';
    ctrl.csvHeader = [
      nvTranslate.instant('container.recovery-tickets.days-due'),
      nvTranslate.instant('container.recovery-tickets.created'),
      nvTranslate.instant('container.recovery-tickets.ticket-type-sub-type'),
      nvTranslate.instant('container.recovery-tickets.status'),
      nvTranslate.instant('container.recovery-tickets.assignee'),
      nvTranslate.instant('container.recovery-tickets.investigating-party'),
      nvTranslate.instant('container.recovery-tickets.investigating-hub'),
      nvTranslate.instant('container.recovery-tickets.hub-name'),
      nvTranslate.instant('container.recovery-tickets.tracking-id'),
      nvTranslate.instant('container.recovery-tickets.shipper'),
      nvTranslate.instant('container.recovery-tickets.order-status'),
      nvTranslate.instant('container.recovery-tickets.order-granular-status'),
      nvTranslate.instant('container.recovery-tickets.ticket-creator'),
    ];
    ctrl.csvField = [
      'daysDue', '_createdAt', 'ticketTypeSubType', 'status', 'assigneeName',
      'investigatingParty', 'investigatingHub', 'hubName', 'trackingId', 'shipper', 'orderStatus',
      'orderGranularStatus', 'ticketCreator',
    ];


    // /////////////////////
    // FILTER
    // ////////////////////
    ctrl.selectedFilters = [];
    ctrl.possibleFilters = [];
    ctrl.filterPresets = [];
    ctrl.csvData = [];
    ctrl.selectedPreset = null;
    ctrl.savePreset = savePreset;
    ctrl.updatePreset = updatePreset;
    ctrl.deletePreset = deletePreset;


    // ///////////////////
    // TABLE
    // //////////////////
    ctrl.tableParam = {};
    ctrl.page = 1;
    ctrl.itemsPerPage = 1000;
    ctrl.totalItems = Number.MAX_VALUE;

    ctrl.searchAllTickets = searchAllTickets;
    ctrl.createOne = createOne;
    ctrl.onLoaded = onLoaded;
    ctrl.loadMore = loadMore;
    ctrl.updateOne = updateOne;
    ctrl.deleteOne = deleteOne;
    ctrl.getPageState = getPageState;
    ctrl.openFilter = openFilter;
    ctrl.openEditOrder = openEditOrder;
    ctrl.isHighlighted = isHighlighted;
    ctrl.searchByCsvDialog = searchByCsvDialog;


    // add hotkeys
    hotkeys.bindTo($scope)
            .add({
              combo: 'n+u',
              description: 'Add user',
              callback: addCurrentUser,
            });

    // call after page loaded
    function getPageState() {
      return ctrl.state;
    }

    /**
     * add current user as pets user.
    */
    function addCurrentUser() {
      const user = $rootScope.user;
      // check current list of usersData
      const userByEmail = _.find(usersData, { email: user.email });
      if (userByEmail) {
        // user already a pets user
        return nvToast.error(nvTranslate.instant('container.recovery-tickets.you-already-are-a-pets-user'));
      }
      return nvDialog.confirmSave(null, {
        title: nvTranslate.instant('container.recovery-tickets.add-yourself-as-pets-user'),
        content: nvTranslate.instant('container.recovery-tickets.do-you-want-to-add-yourself-as-a-pets-user'),
        ok: nvTranslate.instant('commons.add'),
      }).then(onConfirmAddUser);

      function onConfirmAddUser() {
        return Ticketing.addUser(user.email, user.firstName, user.lastName)
          .then(() => {
            usersData.push(user);
            nvToast.info(nvTranslate.instant('container.recovery-tickets.success-add-current-user-as-pets-user'));
          }, () => {
            nvToast.info('Failed to add user');
          });
      }
    }

    function openFilter() {
      ctrl.showFilter = true;
      ctrl.page = 1;
      ctrl.endOfPage = false;
    }

    function onLoaded() {
      return $q.all([
        Hub.read({ active_only: false }),
        Ticketing.getTicketFilter(),
        Ticketing.getTicketUser(),
        Ticketing.getGroup(),
        Ticketing.getEntrySource(),
        Ticketing.getTicketStatus(),
        Ticketing.getTicketTypes(),
        Ticketing.getAllSubtypes(),
      ])
        .then(success);
      function success(results) {
        hubsResult = _.cloneDeep(results[0]);
        ctrl.filterPresets = _.map(results[1], (o) => {
          o.payload = angular.fromJson(o.payload);
          return o;
        });
        userResult = _.cloneDeep(results[2]);
        groupsResult = _.cloneDeep(results[3]);
        entrySourceResult = _.map(results[4], (es) => {
          es.name = es.entrySource;
          return es;
        });
        statusResult = _.cloneDeep(results[5]);
        typeResult = _.cloneDeep(results[6]);
        subTypeResult = _.cloneDeep(results[7]);

        ctrl.hubs = Hub.toOptions(hubsResult);
        // create hub map
        _.each(ctrl.hubs, (hub) => {
          hubNameMap[hub.id] = hub.name;
        });

        ticketStatusesFilter = Ticketing.toOptions(statusResult);

        const predefinedStatusFilter = [];
        const possibleStatusFilter = [];
        _.each(statusResult, (status) => {
          const statusParsed = _.replace(status.name, /\([^)]*\)/gm, '').trim();
          if (statusParsed === PENDING || statusParsed === IN_PROGRESS) {
            predefinedStatusFilter.push(status);
          } else {
            possibleStatusFilter.push(status);
          }
        });

        pendingAndInProgressIds = _(statusResult)
                                  .filter(status => (status.name.includes(PENDING)
                                    || status.name.includes(IN_PROGRESS) || status.name.includes(ON_HOLD)))
                                  .map(status => (status.id))
                                  .value();
        otherIds = _(statusResult)
                    .filter(status => (!status.name.includes(PENDING)
                      && !status.name.includes(IN_PROGRESS) && !status.name.includes(ON_HOLD)))
                    .map(status => (status.id))
                    .value();
        ctrl.possibleFilters = _.sortBy([
          {
            type: nvBackendFilterUtils.FILTER_DATE,
            mainTitle: nvTranslate.instant('commons.created-at'),
            fromModel: moment({ hour: 0 }).toDate(),
            toModel: moment({ hour: 23, minute: 59 }).toDate(),
            transformFn: nvDateTimeUtils.displayDate,
            minDate: moment.tz(nvTimezone.getOperatorTimezone()).add(-60, 'd').toDate(),
            key: { from: 'start', to: 'end' },
            maxDateRange: 7,
            presetKey: { from: 'start', to: 'end' },
          },
          {
            type: nvBackendFilterUtils.FILTER_OPTIONS,
            mainTitle: nvTranslate.instant('container.recovery-tickets.investigating-dept'),
            possibleOptions: Ticketing.toOptions(groupsResult),
            selectedOptions: [],
            searchBy: 'displayName',
            searchText: {},
            key: 'investigating_parties',
            presetKey: 'investigatingParty',
          },
          {
            type: nvBackendFilterUtils.FILTER_OPTIONS,
            mainTitle: nvTranslate.instant('container.recovery-tickets.investigating-hub'),
            possibleOptions: _.cloneDeep(ctrl.hubs),
            selectedOptions: [],
            searchBy: 'displayName',
            searchText: {},
            key: 'investigating_hub_ids',
            presetKey: 'investigatingHubIds',
            transformFn: getValue,
          },
          {
            type: nvBackendFilterUtils.FILTER_OPTIONS,
            mainTitle: nvTranslate.instant('commons.hubs'),
            possibleOptions: _.cloneDeep(ctrl.hubs),
            selectedOptions: [],
            searchBy: 'displayName',
            searchText: {},
            key: 'hub_ids',
            presetKey: 'hubs',
            transformFn: getValue,
          },
          {
            type: nvBackendFilterUtils.FILTER_OPTIONS,
            mainTitle: nvTranslate.instant('container.recovery-tickets.entry-source'),
            possibleOptions: Ticketing.toOptions(entrySourceResult),
            selectedOptions: [],
            searchBy: 'displayName',
            searchText: {},
            key: 'entry_sources',
            presetKey: 'sourceOfEntry',
          },
          {
            type: nvBackendFilterUtils.FILTER_BOOLEAN,
            mainTitle: nvTranslate.instant('container.recivery-ticekts.resolved-tickets'),
            booleanModel: '10',
            key: 'resolvedTicket',
            presetKey: 'resolvedTicket',
          },
          {
            type: nvBackendFilterUtils.FILTER_BOOLEAN,
            mainTitle: nvTranslate.instant('container.recovery-tickets.show-tickets'),
            booleanModel: '10',
            trueLabel: nvTranslate.instant('container.recovery-tickets.all-tickets'),
            falseLabel: nvTranslate.instant('container.recovery-tickets.only-my-tickets'),
            key: 'showAll',
            presetKey: 'showAll',
          },
          {
            type: nvBackendFilterUtils.FILTER_BOOLEAN,
            mainTitle: nvTranslate.instant('container.recovery-tickets.show-unassigned'),
            booleanModel: '10',
            trueLabel: nvTranslate.instant('commons.yes'),
            falseLabel: nvTranslate.instant('commons.no'),
            key: 'showUnassigned',
            presetKey: 'showUnassigned',
          },
          {
            type: nvBackendFilterUtils.FILTER_BOOLEAN,
            mainTitle: nvTranslate.instant('container.recovery-tickets.only-show-red-tickets'),
            booleanModel: '10',
            trueLabel: nvTranslate.instant('commons.yes'),
            falseLabel: nvTranslate.instant('commons.no'),
            key: 'onlyShowRedTicket',
            presetKey: 'onlyShowRedTicket',
          },
          {
            type: nvBackendFilterUtils.FILTER_OPTIONS,
            mainTitle: nvTranslate.instant('container.recovery-tickets.ticket-assignee'),
            possibleOptions: Ticketing.toOptions(userResult),
            selectedOptions: [],
            searchBy: 'displayName',
            searchText: {},
            key: 'user_ids',
            presetKey: 'user_ids',
          },
          {
            type: nvBackendFilterUtils.FILTER_AUTOCOMPLETE,
            mainTitle: nvTranslate.instant('container.recovery-tickets.shipper'),
            callback: getShippers,
            backendKey: 'legacy_ids',
            selectedOptions: [],
            key: 'shipper_ids',
            presetKey: 'shipper_ids',
          },
        ], ['mainTitle']);

        ctrl.selectedFilters = [
          {
            type: nvBackendFilterUtils.FILTER_TEXT_FREE,
            mainTitle: nvTranslate.instant('commons.tracking-ids'),
            model: [],
            key: 'tracking_ids',
            presetKey: 'query',
            isMandatory: true,
          },
          {
            type: nvBackendFilterUtils.FILTER_OPTIONS,
            mainTitle: nvTranslate.instant('container.recovery-tickets.ticket-status'),
            possibleOptions: Ticketing.toOptions(possibleStatusFilter),
            selectedOptions: Ticketing.toOptions(predefinedStatusFilter),
            searchBy: 'displayName',
            searchText: {},
            key: 'ticket_statuses',
            presetKey: 'ticketStatus',
            isMandatory: true,
          },
          {
            type: nvBackendFilterUtils.FILTER_OPTIONS,
            mainTitle: nvTranslate.instant('container.recovery-tickets.ticket-type'),
            possibleOptions: [],
            selectedOptions: Ticketing.toOptions(typeResult),
            searchBy: 'displayName',
            searchText: {},
            key: 'ticket_types',
            presetKey: 'ticketType',
            isMandatory: true,
          },
          {
            type: nvBackendFilterUtils.FILTER_OPTIONS,
            mainTitle: nvTranslate.instant('container.recovery-tickets.ticket-sub-type'),
            possibleOptions: [],
            selectedOptions: Ticketing.toOptions(subTypeResult),
            searchBy: 'displayName',
            searchText: {},
            key: 'ticket_sub_types',
            presetKey: 'ticket_sub_types',
          },
        ];
        // add users to the lists
        usersData = results[3];

        function getShippers(text) {
          return Shippers.filterSearch(
            text,
            _.isUndefined(this.getSelectedOptions) ? [] : this.getSelectedOptions()
          );
        }
      }
    }


    function searchByCsvDialog($event) {
      return nvDialog.showSingle($event, {
        templateUrl: 'views/container/recovery-tickets/dialog/csv-search/ticketing-csv-search.dialog.html',
        locals: {
          otherIds: otherIds,
          pendingAndInProgressIds: pendingAndInProgressIds,
        },
        controllerAs: 'ctrl',
        controller: 'TicketingCsvSearchDialogController',
        theme: 'nvBlue',
        cssClass: 'nv-ticket-csv-search-dialog',
      }).then(onCloseSearchDialog);

      function onCloseSearchDialog(result) {
        ctrl.state = PAGE_STATE.WAITING;
        ctrl.tableParam = nvTable.createTable(Ticketing.getTableFields(),
          {
            newItemOnTop: true,
            isStripped: false,
            onSelectItem: onTableItemSelected,
            onDeselectItem: onTableItemDeselected,
          });
        ctrl.tableParam.setHighlightFn(ctrl.isHighlighted);
        setTrackingIdAndStatusFilter(result.filter);
        // show the table
        onSuccessSearch(result.result);
      }
    }

    function setTrackingIdAndStatusFilter(filter) {
      // tracking id
      const trackingIds = filter.tracking_ids;
      const ticketStatuses = filter.ticket_statuses;
      const combinedFilter = _.union(ctrl.possibleFilters, ctrl.selectedFilters);
      // set the tracking id
      const trackingIdFilter = _.find(combinedFilter, { key: 'tracking_ids' });
      if (trackingIdFilter) {
        trackingIdFilter.model = _.map(trackingIds, (val, key) => ({
          id: key,
          displayName: val,
        }));
        // add to selectedfilter and remove it from possible filter
        nvUtils.mergeAllIn(ctrl.selectedFilters, _.castArray(trackingIdFilter), 'key');
        _.remove(ctrl.possibleFilters, (f) => {
          return f.key === trackingIdFilter.key;
        });
      }

      const statusFilter = _.find(combinedFilter, { key: 'ticket_statuses' });
      if (statusFilter) {
        const notPicked = _.remove(ticketStatusesFilter, (opt) => {
          return _.indexOf(ticketStatuses, opt.id) < 0;
        });
        statusFilter.possibleOptions = notPicked;
        statusFilter.selectedOptions = ticketStatusesFilter;
        nvUtils.mergeAllIn(ctrl.selectedFilters, _.castArray(statusFilter), 'key');
        _.remove(ctrl.possibleFilters, (f) => {
          return f.key === statusFilter.key;
        });
      }
    }

    function countDueDay(ticket) {
      if (ticket && (ticket.status !== CANCELLED && ticket.status !== RESOLVED)) {
        const now = moment();
        const created = moment(ticket.createdAt);
        return Math.floor(moment.duration(now.diff(created)).asDays());
      }
      return '-';
    }

    function validateFilters(params) {      
      if (_.size(_.get(params, 'tracking_ids')) === 0 &&
        (_.size(_.get(params, 'ticket_statuses')) === 0 || _.size(_.get(params, 'ticket_types')) === 0)) {
        nvToast.error(nvTranslate.instant('container.recovery-tickets.minimum-filter-warning'));
        return false;
      }
      return true;
    }

    function loadTicketPromise() {
      ctrl.params = nvBackendFilterUtils.constructParams(ctrl.selectedFilters);
      if (validateFilters(ctrl.params)){
        return Ticketing.searchTicket(ctrl.params, ctrl.page, ctrl.itemsPerPage);
      }
      return $q.reject();
    }
    /**
     * search all ticket
     * it will append the filter to the request
     * @returns {*}
     */
    function searchAllTickets() {
      ctrl.state = PAGE_STATE.WAITING;
      ctrl.tableParam = nvTable.createTable(Ticketing.getTableFields(),
        {
          newItemOnTop: true,
          isStripped: false,
          onSelectItem: onTableItemSelected,
          onDeselectItem: onTableItemDeselected,
        });
      ctrl.tableParam.setHighlightFn(ctrl.isHighlighted);
      return loadTicketPromise()
        .then(onSuccessSearch, onFailureSearch);
    }
    function onSuccessSearch(result) {
      ctrl.state = PAGE_STATE.IDLE;
      const ticketSource = _.forEach(result.tickets, o => (extendTicket(o)));
      ctrl.totalItems = result.count;
      ctrl.tableParam.totalItems = ctrl.totalItems;
      const tickets = filterByUI(ticketSource);
      ctrl.tableParam.setData(tickets);
      ctrl.showFilter = false;
      ctrl.page += 1;
    }

    function extendTicket(o) {
      o.ticketTypeSubType = o.ticketType + (o.subTicketType ? ` : ${o.subTicketType}` : '');
      o.daysDue = countDueDay(o);
      o.shipper = _.get(o, 'customFields.shipperName', '');
      o.trackingId = _.get(o, 'trackingId', '-');
      o.isHighlighted = highlightFn(o);
      o.investigatingHub = getHubName(o.investigatingHubId) || '-';
      o.hubName = _.get(hubNameMap, o.hubId, '-');
      o.orderStatus = _.get(o, 'orderDetails.status', '-');
      o.orderGranularStatus = _.get(o, 'orderDetails.granular_status', '-');
      o.ticketCreator = _.get(o, 'creatorUser.userName', '-');
      o._createdAt = nvDateTimeUtils.displayDateTime(moment.utc(o.createdAt));
      return o;
    }

    function onFailureSearch() {
      ctrl.state = PAGE_STATE.IDLE;
      $q.reject();
    }

    function getHubName(hubId) {
      const hubs = _.find(ctrl.hubs, { value: Number(hubId) });
      if (hubs) {
        return hubs.displayName;
      }
      return '';
    }

    // this is for filtering by user and resolve status
    function filterByUI(ticketsSource) {
      let tickets = _.cloneDeep(ticketsSource);
      if (!_.isUndefined(ctrl.params.showAll)
        && !ctrl.params.showAll) {
        const userFullName = `${$rootScope.user.firstName} ${$rootScope.user.lastName}`;
        tickets = _.filter(tickets, (val) => {
          if (val.assigneeName === userFullName) {
            return true;
          }
          return false;
        });
      }

      if (!_.isUndefined(ctrl.params.resolvedTicket)
        && !ctrl.params.resolvedTicket) {
        tickets = _.filter(tickets, (val) => {
          if (val.status !== RESOLVED && val.status !== CANCELLED) {
            return true;
          }
          return false;
        });
      }

      if (!_.isUndefined(ctrl.params.showUnassigned)
        && !ctrl.params.showUnassigned) {
        tickets = _.filter(tickets, (val) => {
          if (val.assigneeName === null || val.assigneeName === '') {
            return ctrl.params.showUnassigned;
          }
          return true;
        });
      }

      if (!_.isUndefined(ctrl.params.onlyShowRedTicket)
        && !ctrl.params.showOnlyRedTicket) {
        tickets = _.filter(tickets, val => (highlightFn(val)));
      }
      return tickets;
    }


    function loadTickets() {
      if (ctrl.tableParam.getUnfilteredLength() < ctrl.totalItems) {
        return loadTicketPromise().then(extend);
      }
      return $q.reject();

      function extend(result) {
        if (result.tickets.length === 0) {
          ctrl.endOfPage = true;
          return [];
        }
        const tickets = _.map(_.castArray(result.tickets), o => (extendTicket(o)));
        ctrl.loadState = 'ready';
        ctrl.page += 1;
        return filterByUI(tickets);
      }
    }

    function loadMore() {
      // on load more event, we decrease total item per page to 500
      ctrl.itemsPerPage = 500;
      if (ctrl.state !== PAGE_STATE.LOADING && !ctrl.endOfPage) {
        ctrl.loadState = PAGE_STATE.LOADING;
        return ctrl.tableParam.mergeInPromise(loadTickets(), 'id');
      }
      return $q.reject();
    }

    /**
    * create ticket function function
    * @param $event
    */
    function createOne($event) {
      const fields = Ticketing.getAddField();
      return nvDialog.showSingle($event, {
        templateUrl: 'views/container/recovery-tickets/dialog/recovery-tickets-add/new-recovery-add-dialog.html',
        locals: {
          fields: fields,
          resultData: {
            hubsResult: hubsResult || _.noop(),
            groupsResult: groupsResult || _.noop(),
            entrySourceResult: entrySourceResult || _.noop(),
            typeResult: typeResult || _.noop(),
          },
        },
        controllerAs: 'ctrl',
        controller: 'NewRecoveryAddDialogController',
        theme: 'nvGreen',
        cssClass: 'nv-©-ticket-dialog',
      }).then(success);


      function success(result) {
        if (result.status === 'Success') {
          const ticket = extendTicket(result.ticket);
          ticket._isNew = true;
          if (ctrl.tableParam && ctrl.tableParam.ready) {
            ctrl.tableParam.mergeIn(ticket, 'id');
          }
          return nvToast.success(nvTranslate.instant('container.recovery-tickets.ticket-created'));
        }
        return nvToast.success('Failed to create ticket');
      }
    }

    /**
    * update ticket function
    * @param $event
    */
    function updateOne($event, obj) {
      const fields = Ticketing.getAddField();
      return nvDialog.showSingle($event, {
        templateUrl: 'views/container/recovery-tickets/dialog/recovery-tickets-edit/new-recovery-dialog-edit.html',
        locals: {
          fields: fields,
          ticketObj: obj,
          resultData: {
            statusResult: statusResult || _.noop(),
            userResult: userResult || _.noop(),
            groupsResult: groupsResult || _.noop(),
            hubResult: hubsResult || _.noop(),
          },
        },
        cssClass: 'nv-edit-ticket-dialog',
        controllerAs: 'ctrl',
        controller: 'NewRecoveryDialogEditController',
      })
        .then((result) => {
          if (result && result.id) {
            result.investigatingHub = getHubName(result.investigating_hub_id);
            ctrl.tableParam.mergeIn(result, 'id');
          }
        });
    }

    /**
    * delete a ticket
    */
    function deleteOne($event, obj) {
      return nvDialog.confirmDelete($event, {
        content: `Are you sure you want to cencel ticket ID ${obj.id}?`,
      }).then(onDelete);

      function onDelete() {
        return Ticketing.deleteOne(obj.id).then(success);
      }

      function success() {
        obj.status = CANCELLED;
        ctrl.tableParam.mergeIn(obj, 'id');
      }
    }

    function highlightFn(ticket) {
      const highlightedType = [
        'MISSING', 'DAMAGED', 'SHIPPER ISSUE',
      ];
      const createdDate = nvDateTimeUtils.toMoment(ticket.createdAt, 'utc');
      const idx = _.indexOf(highlightedType, ticket.ticketType);
      const isNonComplete = ticket.status !== RESOLVED && ticket.status !== CANCELLED;
      const isAfterScan = ticket.orderDetails ?
        nvDateTimeUtils.toMoment(ticket.orderDetails.last_scan_time, 'utc')
          .isAfter(createdDate)
        : false;
      return idx >= 0 && isNonComplete && isAfterScan;
    }
    /**
    * highlight fn, return the highlight class if highlighted
    * return empty string if none
    */
    function isHighlighted(alias) {
      return {
        class: '',
        highlighted: alias.isHighlighted,
      };
    }


    function onTableItemSelected() {
      checkSelection();
    }

    function onTableItemDeselected() {
      checkSelection();
    }

    /**
     * this method will run when user select the checkbox on each row
     * and will evaluate the ticket type
     */
    function checkSelection() {
      const selection = ctrl.tableParam.getSelection();
      ctrl.csvData = angular.copy(selection);
      if (selection.length > 0) {
        const firstType = selection[0].ticketType;
        const firstSubType = selection[0].subTicketType;
        let isSame = true;
        _.each(selection, (ticket) => {
          // checking type
          if (ticket.ticketType !== firstType) {
            isSame = false;
            return false;
          }
          // checking sub type
          if (ticket.subTicketType !== firstSubType) {
            isSame = false;
            return false;
          }
          // checking status
          if (ticket.status === RESOLVED || ticket.status === CANCELLED) {
            isSame = false;
            return false;
          }
          return true;
        });
        ctrl.bulkEnabled = isSame;
        ctrl.userSelectItem = true;
      } else {
        ctrl.bulkEnabled = false;
        ctrl.userSelectItem = false;
      }
    }

    function openEditOrder(trackingId) {
      return Order.getTrackingIdV2(trackingId)
        .then(success, err);
      function success(result) {
        $window.open(
          $state.href('container.order.edit', { orderId: result.id })
        );
      }

      function err() {
        nvToast.error(nvTranslate.instant('container.recovery-tickets.tracking-id-invalid'));
      }
    }
    /**
     * call back for sort by color
    */
    function orderTableByColor() {
      // reset the table sorting
      ctrl.tableParam.currSortIdx = 0;
      ctrl.tableParam.currSortCol = null;

      let tableData = ctrl.tableParam.getTableData();
      tableData = _.sortBy(tableData, 'isHighlighted', 'asc').reverse(); // sortby boolean value always put true in the last, therefore we reverse
      ctrl.tableParam.setData(tableData);
      ctrl.tableParam.refreshData();
    }

    /**
     * bulk update dialog
     */
    function bulkUpdate($event) {
      if (!ctrl.bulkEnabled) {
        nvToast.warning(nvTranslate.instant('container.recovery-tickets.invalid-selection'),
          nvTranslate.instant('container.recovery-tickets.selection-must-be-of-the-same-type')
        );
        return false;
      }
      return nvDialog.showSingle($event,
        {
          templateUrl: 'views/container/recovery-tickets/dialog/bulk-edit/recovery-bulk-edit.html',
          locals: {
            tickets: ctrl.tableParam.getSelection(),
            hubs: ctrl.hubs,
            investigatingParties: Ticketing.toOptions(groupsResult),
          },
          controllerAs: 'ctrl',
          controller: 'RecoveryBulkEditController',
          cssClass: 'nv-bulk-edit-ticket',
          theme: 'nvBlue',
        }).then(success);


      function success(result) {
        ctrl.userSelectItem = false;
        if (result.length > 0) {
          nvToast.info(nvTranslate.instant('container.recovery-tickets.x-tickets-updated', { x: result.length }));
          // set the investigating hub name
          _.each(result, (ticket) => {
            ticket.investigatingHub = getHubName(ticket.investigatingHubId);
          });

          ctrl.tableParam.mergeIn(result, 'id');
          ctrl.tableParam.clearSelect();
        }
      }
    }

    function openUploadCsvDialog(event) {
      return nvDialog.showSingle(event, {
        templateUrl: 'views/container/recovery-tickets/dialog/csv-upload/ticketing-csv-upload.dialog.html',
        locals: {},
        controllerAs: 'ctrl',
        controller: 'TicketingCsvUploadDialogController',
        cssClass: 'nv-ticket-csv-upload-dialog',
        theme: 'nvBlue',
      }).then(onProcessResult);

      function onProcessResult(result) {
        // check the showfilter page
        if (!ctrl.showFilter && ctrl.tableParam) {
          const tickets = _.filter(result, r => r.status === 'Success')
                            .map((o) => {
                              const ticket = o.ticket;
                              ticket.trackingId = o.trackingId;
                              return ticket;
                            });
          _.each(tickets, o => extendTicket(o));
          ctrl.tableParam.mergeIn(tickets);
        }
      }
    }

    function savePreset(data) {
      return Ticketing.createTicketFilter(data).then((response) => {
        response.payload = response.payload ? angular.fromJson(response.payload) : null;
        return response;
      });
    }

    function deletePreset(id) {
      return Ticketing.deleteTicketFilter(id);
    }

    function updatePreset(id, body) {
      return Ticketing.updateTicketFilter(body, id).then((response) => {
        response.payload = response.payload ? angular.fromJson(response.payload) : null;
        return response;
      });
    }

    function getValue(filterHub) {
      return filterHub.value;
    }
  }
}());
