(function controller() {
  angular
        .module('nvOperatorApp.controllers')
        .controller('NewRecoveryAddDialogController', Controller);

  Controller.$inject = [
    '$q', 'resultData', 'Ticketing', '$mdDialog', 'fields', '$filter',
    '$rootScope', 'nvToast', 'Order', '$timeout', 'nvTranslate', 'Hub',
  ];

  function Controller(
    $q, resultData, Ticketing, $mdDialog, fields, $filter,
    $rootScope, nvToast, Order, $timeout, nvTranslate, Hub
  ) {
    const ctrl = this;
    const FIXED_CUSTOM_FIELD_ID = {
      CUST_ZENDESK_ID: 34,
      SHIPPER_ZENDESK_ID: 36,
      TICKET_NOTES: 32,
      COMMENTS: 26,
    };
    const RESOLVED = 'RESOLVED';
    const CANCELLED = 'CANCELLED';


    // variables
    ctrl.fields = fields;
    ctrl.customFields = [];
    ctrl.ticketTypes = [];
    ctrl.investigatingParties = [];
    ctrl.investigatingHubs = [];
    ctrl.entrySources = [];
    ctrl.subTypes = [];
    ctrl.subTypeExist = false;
    ctrl.subTypeId = null;
    ctrl.state = 'idle';
    let payload = {};
    ctrl.ticketTypeName = '';
    ctrl.modelForm = {};

    // function header
    ctrl.onChangeTicketType = onChangeTicketType;
    ctrl.onCreate = onCreate;
    ctrl.onCancel = onCancel;
    ctrl.onLoadPage = onLoadPage;
    ctrl.onSelectSubType = onSelectSubType;
    ctrl.checkTrackingIdValid = checkTrackingIdValid;
        /*
        on select subtype
        */
    function onSelectSubType(id) {
      if (!id) {
        return angular.noop();
      }
      ctrl.subTypeId = id;
      $timeout(() => {
        checkTrackingIdValid(ctrl.fields.trackingId.value);
      }, 0);
      return Ticketing.getSubtype(id)
                .then(onSuccess);
      function onSuccess(result) {
        ctrl.customFieldsSubtype = _.pullAllBy(result.fields, [
                    { id: FIXED_CUSTOM_FIELD_ID.CUST_ZENDESK_ID },
                    { id: FIXED_CUSTOM_FIELD_ID.SHIPPER_ZENDESK_ID },
                    { id: FIXED_CUSTOM_FIELD_ID.TICKET_NOTES },
                    { id: FIXED_CUSTOM_FIELD_ID.COMMENTS },
        ], 'id');
      }
    }


    function onChangeTicketType(id) {
      // remove the sub type
      if (ctrl.fields.subTicketType && ctrl.fields.subTicketType.value) {
        ctrl.fields.subTicketType.value = null;
      }
      if (id === undefined || id === null) {
        return;
      }
      $timeout(() => {
        checkTrackingIdValid(ctrl.fields.trackingId.value);
      }, 0);
      const selectedTicketType = _.find(ctrl.ticketTypes, { value: id });
      ctrl.ticketTypeName = _.capitalize(selectedTicketType.displayName);
      Ticketing.getCustomField(id).then((result) => {
        if (result !== undefined || result !== null) {
          if (!result.sub_types) {
            // remove the shipperZendeskId and custZendeskId
            // both value are coming from static fields
            ctrl.customFields = _.pullAllBy(result.fields, [
              { id: FIXED_CUSTOM_FIELD_ID.CUST_ZENDESK_ID },
              { id: FIXED_CUSTOM_FIELD_ID.SHIPPER_ZENDESK_ID },
              { id: FIXED_CUSTOM_FIELD_ID.TICKET_NOTES },
              { id: FIXED_CUSTOM_FIELD_ID.COMMENTS },
            ], 'id');
            ctrl.customFieldsSubtype = [];
            ctrl.subTypeExist = false;
          } else {
            ctrl.subTypeExist = true;
            ctrl.customFields = [];
            ctrl.subTypes = (result.sub_types).filter(o => (!o.archived)).map(o => ({
              value: o.id,
              displayName: o.name,
            }));
            ctrl.fields.subTicketType.options = ctrl.subTypes;
          }
        }
      });
    }

    function onLoadPage() {
      const promises = [];

      if (_.isUndefined(_.get(resultData, 'typeResult'))) {
        promises.push(Ticketing.getTicketTypes());
      } else {
        promises.push($q.when(_.get(resultData, 'typeResult')));
      }

      if (_.isUndefined(_.get(resultData, 'entrySourceResult'))) {
        promises.push(Ticketing.getEntrySource());
      } else {
        promises.push($q.when(_.get(resultData, 'entrySourceResult')));
      }

      if (_.isUndefined(_.get(resultData, 'groupsResult'))) {
        promises.push(Ticketing.getGroup());
      } else {
        promises.push($q.when(_.get(resultData, 'groupsResult')));
      }

      if (_.isUndefined(_.get(resultData, 'hubsResult'))) {
        promises.push(Hub.read({ active_only: false }));
      } else {
        promises.push($q.when(_.get(resultData, 'hubsResult')));
      }

      return $q.all(promises).then(success);

      function success(results) {
        ctrl.ticketTypes = _.map(results[0], o => ({
          value: o.id,
          displayName: o.name,
        }));
        ctrl.entrySources = _.map(results[1], o => ({ value: o.id, displayName: o.entrySource }));
        ctrl.investigatingParties = _.map(results[2], o => ({ value: o.id, displayName: o.name }));
        ctrl.investigatingHubs = _.map(results[3], o => ({ value: o.id, displayName: o.name }));
      }
    }


    function checkTrackingIdValid(trackingId) {
      if (ctrl.fields.ticketType && ctrl.fields.ticketType.value === 5 &&
        ctrl.fields.subTicketType && ctrl.fields.subTicketType.value === 6) {
        setTrackingIdErrorMsg(true);
        return true;
      }
      if (!trackingId) {
        return true;
      }
      return $q.all([
        Order.getTrackingId(trackingId),
        Ticketing.getTicketByTrackingIdV2(_.castArray(trackingId)),
      ])
      .then(onSuccessCheckTrackingId, onFailedCheckTrackingId);

      function onSuccessCheckTrackingId(res) {
        setTrackingIdErrorMsg(!Ticketing.hasActiveTickets(res[1]), true);
      }
      function onFailedCheckTrackingId() {
        setTrackingIdErrorMsg(false);
      }
    }

    function setTrackingIdErrorMsg(valid, isUsed = false) {
      if (valid) {
        ctrl.fields.trackingId.errorMsgs = null;
        ctrl.modelForm.trackingId.$setValidity('invalid', true);
      } else {
        const errMsg = {
          key: 'invalid',
        };
        if (isUsed) {
          errMsg.msg = nvTranslate.instant('container.recovery-tickets.tracking-id-has-ticket-in-non-resolved');
        } else {
          errMsg.msg = nvTranslate.instant('container.recovery-tickets.invalid-tracking-id');
        }
        ctrl.fields.trackingId.errorMsgs = [errMsg];
        ctrl.modelForm.trackingId.$setValidity('invalid', false);
      }
    }

        /**
        * call to create ticket
        */
    function onCreate() {
      ctrl.state = 'waiting';
      const createdFields = _.map(ctrl.fields, o => ({
        fieldName: o.fieldName,
        fieldId: o.fieldId ? o.fieldId : null,
        fieldValue: o.value ? o.value : null,
      }
            ));
      const customFields = _.map(ctrl.customFields, o => ({
        fieldName: o.name ? o.name : null,
        fieldId: o.id ? o.id : null,
        fieldValue: o.value ? o.value : null,
      }
            ));
      payload = createdFields.concat(customFields);
      if (ctrl.subTypeExist) {
        payload.push({
          fieldName: 'subTicketType',
          fieldId: 17,
          fieldValue: ctrl.subTypeId,
        });
                // add customFields from subtypes
        const subTypeCustomField = _.map(ctrl.customFieldsSubtype, o => ({
          fieldName: o.name ? o.name : null,
          fieldId: o.id ? o.id : null,
          fieldValue: o.value ? o.value : null,
        }
                ));
        payload = _.concat(payload, subTypeCustomField);
      }

            // get user id and user name
      const userId = $rootScope.user.thirdPartyId;
      const userName = _.compact([$rootScope.user.firstName, $rootScope.user.lastName]).join(' ');
      const userEmail = $rootScope.user.email;
      payload.push({
        fieldName: 'creatorUserId',
        fieldValue: userId,
        fieldId: 30,
      }, {
        fieldName: 'creatorUserName',
        fieldValue: userName,
        fieldId: 39,
      }, {
        fieldName: 'creatorUserEmail',
        fieldValue: userEmail,
        fieldId: 66,
      }, {
        fieldName: 'TICKET_CREATION_SOURCE',
        fieldValue: 'TICKET_MANAGEMENT',
        fieldId: null,
      });
      return Ticketing.createOne(payload).then(success, error);

      function success(result) {
        ctrl.state = 'idle';
        if (result.status === 'Fail') {
          nvToast.error('Tracking ID already has a Ticket created!');
        } else {
          $mdDialog.hide(result);
        }
      }

      function error() {
        ctrl.state = 'failed';
      }
    }


    function onCancel() {
      setTrackingIdErrorMsg(true);
      $mdDialog.cancel();
    }
  }
}());
