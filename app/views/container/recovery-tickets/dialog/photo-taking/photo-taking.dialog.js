(function controller() {
  angular
    .module('nvOperatorApp.controllers')
    .controller('RecoveryTicketPhotoTakingDialogController', RecoveryTicketPhotoTakingDialogController);
  /* global Webcam */
  RecoveryTicketPhotoTakingDialogController.$inject = ['$timeout', '$mdDialog', '$scope', 'hotkeys',
    'nvToast', 'nvClientStore', 'nvTranslate'];
  function RecoveryTicketPhotoTakingDialogController($timeout, $mdDialog, $scope, hotkeys,
    nvToast, nvClientStore, nvTranslate) {
    const ctrl = this;

    // function definintion
    ctrl.takePhoto = takePhoto;
    ctrl.onCancel = onCancel;
    ctrl.onClose = onClose;
    ctrl.removeImage = removeImage;
    ctrl.detectMaxResolution = detectMaxResolution;
    ctrl.changeCameraSetting = changeCameraSetting;
    ctrl.resolution = nvClientStore.localStorage.get('cameraResolution');
    ctrl.undetected = true;
    let detectorStream = null;

    const resolutions = [
      {
        label: '4K(UHD)',
        width: 3840,
        height: 2160,
        ratio: '16:9',
      },
      {
        label: '1080p(FHD)',
        width: 1920,
        height: 1080,
        ratio: '16:9',
      },
      {
        label: 'UXGA',
        width: 1600,
        height: 1200,
        ratio: '4:3',
      },
      {
        label: '720p(HD)',
        width: 1280,
        height: 720,
        ratio: '16:9',
      },
      {
        label: 'SVGA',
        width: 800,
        height: 600,
        ratio: '4:3',
      },
      {
        label: 'VGA',
        width: 640,
        height: 480,
        ratio: '4:3',
      },
      {
        label: '360p(nHD)',
        width: 640,
        height: 360,
        ratio: '16:9',
      },
      {
        label: 'CIF',
        width: 352,
        height: 288,
        ratio: '4:3',
      },
      {
        label: 'QVGA',
        width: 320,
        height: 240,
        ratio: '4:3',
      },
    ];

    // variables
    ctrl.images = [];

    init();
    function init() {
      // add hotkeys
      hotkeys.bindTo($scope)
      .add({
        combo: 't',
        description: 'take photo',
        callback: takePhoto,
      });
      if (ctrl.resolution) {
        // prompt the detector
        ctrl.undetected = false;
        $timeout(() => {
          Webcam.set({
            dest_width: ctrl.resolution.width,
            dest_height: ctrl.resolution.height,
            width: ctrl.resolution.width,
            height: ctrl.resolution.height,
          });
          Webcam.attach('#my_camera');
        }, 1000);
      }
    }

    function takePhoto() {
      if (ctrl.images && ctrl.images.length > 9) {
        return;
      }
      Webcam.snap((dataUri) => {
        $timeout(() => {
          ctrl.images.push(dataUri);
        });
      });
    }

    function onCancel() {
      Webcam.reset();
      return $mdDialog.cancel();
    }

    function onClose() {
      Webcam.reset();
      const imageFiles = _.map(ctrl.images, (image) => {
        return dataURItoBlob(image);
      });
      return $mdDialog.hide(imageFiles);
    }

    function removeImage(index) {
      const toRemove = ctrl.images[index];
      _.pull(ctrl.images, toRemove);
      nvToast.info(nvTranslate.instant("container.recovery-ticekts.image-deleted"));
    }

    function dataURItoBlob(dataURI) {
      const binary = atob(dataURI.split(',')[1]);
      const array = [];
      for (let i = 0; i < binary.length; i++) {
        array.push(binary.charCodeAt(i));
      }
      return new Blob([new Uint8Array(array)], { type: 'image/jpeg' });
    }

    /** 
     * detect only the maximum resolution for the default camera
     * from https://webrtchacks.github.io/WebRTC-Camera-Resolution/
     * with modification
    */
    function detectMaxResolution() {
      const video = document.getElementById('detector');
      let r = 0;
      // callback on success load the resolution
      video.onloadedmetadata = displayVideoDimensions;
      doTheCheck();
      function doTheCheck() {
        const constraint = {
          audio: false,
          video: {
            width: { exact: resolutions[r].width },
            height: { exact: resolutions[r].height },
          },
        };
        $timeout(() => {
          navigator.mediaDevices.getUserMedia(constraint)
          .then(gotStream)
          .catch(() => {
            captureResults(false);
          });
        }, 200);
      }
      function gotStream(stream) {
        video.width = resolutions[r].width;
        video.height = resolutions[r].height;
        video.srcObject = stream;
        detectorStream = stream;
      }

      function captureResults(flag) {
        if (!flag) {
          r += 1;
          doTheCheck(); // try next resolutions
        } else {
          nvClientStore.localStorage.set('cameraResolution', {
            width: video.videoWidth,
            height: video.videoHeight,
          });
          ctrl.resolution = {
            width: video.videoWidth,
            height: video.videoHeight,
          };
          nvToast.info(nvTranslate.instant(
            'container.recovery-tickets.camera-resolution-detection-is-complete-your-camera-set-to-x-y',
            { x: ctrl.resolution.width, y: ctrl.resolution.height }
          ));
          ctrl.undetected = false;
          const track = detectorStream.getTracks()[0];
          if (track) {
            try {
              track.stop();
            } catch (error) {
              // no track
            }
          }
          Webcam.set({
            dest_width: ctrl.resolution.width,
            dest_height: ctrl.resolution.height,
            width: ctrl.resolution.width,
            height: ctrl.resolution.height,
          });
          $timeout(() => {
            Webcam.attach('#my_camera');
          }, 1000);
        }
      }

      function displayVideoDimensions() {
        if (!video.videoWidth) {
          $timeout(displayVideoDimensions, 500); // wait for 500 ms
        }

        if (video.videoWidth === resolutions[r].width &&
          video.videoHeight === resolutions[r].height) {
          captureResults(true);
        } else {
          captureResults(false);
        }
      }
    }

    function changeCameraSetting() {
      ctrl.undetected = true;
    }
  }
}());
