 (function RecoveryBulkEditController() {
   angular
        .module('nvOperatorApp.controllers')
        .controller('RecoveryBulkEditController', controller);
   controller.$inject = ['$mdDialog', 'Ticketing', '$q', 'nvAutocomplete.Data', 'nvDialog', 'nvTranslate', 'tickets',
    'hubs', 'investigatingParties', '$rootScope', '$timeout'];

   function controller($mdDialog, Ticketing, $q, nvAutocompleteData,
        nvDialog, nvTranslate, tickets, hubs, investigatingParties, $rootScope, $timeout) {
     const ctrl = this;
     const STATE = {
       IDLE: 'idle',
       WAITING: 'waiting',
     }
     ctrl.tickets = angular.copy(tickets);
     ctrl.hubs = _.cloneDeep(hubs);
     ctrl.investigatingParties = _.cloneDeep(investigatingParties);
     ctrl.userSearch = nvAutocompleteData.getByHandle('users');
     ctrl.outcomes = [];
     ctrl.statuses = [];
     ctrl.state = STATE.IDLE;
     ctrl.cancelState = STATE.IDLE;
     ctrl.ticketType = null;
     ctrl.onLoad = onLoad;
     ctrl.onClose = onClose;
     ctrl.onSave = onSave;
     ctrl.onCancelTickets = onCancelTickets;
     ctrl.onChangeTicketStatus = onChangeTicketStatus;

     function onLoad() {
       const firstTicket = ctrl.tickets[0];
       ctrl.ticketType = firstTicket.ticketType;
       const typeId = firstTicket.ticketTypeId;
       const subTypeId = firstTicket.subTicketTypeId;
       let orderOutcomePromise = angular.noop;
       if (subTypeId) {
         orderOutcomePromise = Ticketing.getOrderOutcomeBySubTypeId(subTypeId);
       } else if (typeId) {
         orderOutcomePromise = Ticketing.getOrderOutcomeByTypeId(typeId);
       }

       return $q.all([
         Ticketing.getTicketUser(),
         Ticketing.getTicketStatus(),
         orderOutcomePromise,
       ])
                .then(success);

       function success(results) {
         ctrl.ticketUsers = _.map(results[0], usr => ({
           displayName: usr.name,
           value: usr,
         }));
         ctrl.userSearch.setPossibleOptions(ctrl.ticketUsers);
        // statuses
         ctrl.statuses = _.map(results[1], stts => ({
           displayName: stts.name,
           value: stts.name,
         }));
        // order outcomes
         const orderOutcome = results[2].orderOutcome;
         ctrl.outcomes = orderOutcome[0] ? _.map(orderOutcome[0].options, (opt) => {
           opt.displayName = opt.display_name;
           return opt;
         }) : [];
       }
     }

     function onClose() {
       return $mdDialog.cancel();
     }


     function onSave($event) {
       ctrl.state = STATE.WAITING;
       const payload = createPayload(ctrl.tickets);
       return nvDialog.showSingle($event, {
         templateUrl: 'views/container/recovery-tickets/dialog/bulk-edit/bulk-update-progress.html',
         controllerAs: 'ctrl',
         controller: 'BulkUpdateProgressController',
         class: 'nv-dialog-progress',
         skipHide: true,
         locals: {
           payload: payload,
         },
       }).then((result) => {
         ctrl.state = STATE.IDLE;
         $mdDialog.hide(result);
       });
     }

     function onChangeTicketStatus() {
       $timeout(() => {
         ctrl.modelForm['order-outcome'].$setTouched();
       });
     }

     function onCancelTickets($event) {
       ctrl.cancelState = STATE.WAITING;
       return nvDialog.confirmDelete($event, {
         skipHide: true,
         title: nvTranslate.instant('container.recovery-tickets.cancel-tickets'),
         content: nvTranslate.instant('container.recovery-tickets.cancelled-tickets-will-not'),
         ok: nvTranslate.instant('container.recovery-tickets.cancel-tickets'),
         cancel: nvTranslate.instant('container.recovery-tickets.go-back'),
       })
        .then(cancelTickets, onGoBack);

       function cancelTickets() {
         const payload = createCancelledPayload(ctrl.tickets);
         return nvDialog.showSingle($event, {
           templateUrl: 'views/container/recovery-tickets/dialog/bulk-edit/bulk-update-progress.html',
           controllerAs: 'ctrl',
           controller: 'BulkUpdateProgressController',
           class: 'nv-dialog-progress',
           skipHide: true,
           locals: {
             payload: payload,
           },
         }).then((result) => {
           ctrl.cancelState = STATE.IDLE;
           $mdDialog.hide(result);
         });
       }

       function onGoBack() {
         ctrl.cancelState = STATE.IDLE;
       }
     }

     function createCancelledPayload(nTickets) {
       const userEmail = $rootScope.user.email;
       return _.map(nTickets, ticket => ({
         ticket_id: ticket.id,
         ticket_status: 'CANCELLED',
         reporter_email: userEmail,
       }));
     }

     function createPayload(nTickets) {
       const userEmail = $rootScope.user.email;
       return _.map(nTickets, (ticket) => {
         const obj = {
           ticket_id: ticket.id,
           ticket_status: ctrl.ticketStatus,
           order_outcome: ctrl.orderOutcome ? ctrl.orderOutcome : null,
           assignee_email: ctrl.assignee ? ctrl.assignee.value.email : null,
           new_instruction: ctrl.newInstruction,
           ticket_notes: ctrl.ticketNotes,
           customer_zendesk_id: ctrl.customerZendeskId,
           shipper_zendesk_id: ctrl.shipperZendeskId,
           reporter_email: userEmail,
           trackingId: getTrackingId(ticket),
           investigating_hub_id: ctrl.investigatingHub,
           investigating_group_id: ctrl.investigatingParty,
         };
         return removeNull(obj);
       });

       function removeNull(obj) {
         return _.omitBy(obj, field => (_.isUndefined(field) || _.isNull(field)));
       }

       function getTrackingId(ticket) {
         return ticket.customFields['TRACKING ID'];
       }
     }
   }
 }());
