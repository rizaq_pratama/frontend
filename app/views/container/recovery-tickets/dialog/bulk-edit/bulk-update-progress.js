 (function BulkUpdateProgressController() {
   angular
        .module('nvOperatorApp.controllers')
        .controller('BulkUpdateProgressController', controller);
   controller.$inject = ['$mdDialog', 'Ticketing', '$q', 'payload'];

   function controller($mdDialog, Ticketing, $q, payload) {
     const ctrl = this;
     const UPDATE_STATUS = {
       SUCCESS: 'Success',
       FAILED: 'Failed',
     };
     const STATE = {
       UPDATING: 'updating',
       UPDATED: 'updated',
     };
     ctrl.uploadedCount = 0;
     ctrl.total = _.castArray(payload).length;
     ctrl.currentIdx = 0;
     ctrl.successCount = 0;
     ctrl.errorTrackingId = [];
     ctrl.progressValue = 0;
     ctrl.state = STATE.UPDATING;
     ctrl.currentTicketId = null;
     ctrl.errorTrackingId = [];
     ctrl.updatedTicket = [];


     // function
     ctrl.closeDialog = closeDialog;


     startUpload().then(() => {
       if (ctrl.errorTrackingId.length === 0) {
         $mdDialog.hide(ctrl.updatedTicket);
       }
     });


     function closeDialog() {
       return $mdDialog.hide(ctrl.updatedTicket);
     }


     function startUpload() {
       const mappedPayload = _.map(payload, val => (val));
       return Ticketing.bulkUpdate(mappedPayload).then(success, error);

       function success(result) {
         ctrl.state = STATE.UPDATED;
         const tickets = _.get(result, 'tickets');
         _.each(tickets, (ticket, id) => {
           if (ticket.status === UPDATE_STATUS.FAILED) {
             ctrl.errorTrackingId.push({
               id: id,
               errorMessage: ticket.message,
             });
           } else {
             ctrl.updatedTicket.push(ticket.ticket);
           }
         });
         return $q.resolve();
       }

       function error() {
         ctrl.state = STATE.UPDATED;
         return $q.reject();
       }
     }
   }
 }());
