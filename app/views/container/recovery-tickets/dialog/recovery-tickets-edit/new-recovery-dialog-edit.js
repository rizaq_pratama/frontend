(function controller() {
  angular
    .module('nvOperatorApp.controllers')
    .controller('NewRecoveryDialogEditController', Controller);

  Controller.$inject = [
    '$q', '$scope', 'Ticketing', 'Hub', '$mdDialog', 'fields', 'ticketObj', 'resultData', '$rootScope', 'nvTableUtils', 'nvToast',
    'nvExtendUtils', 'nvDateTimeUtils', 'nvTranslate', 'nvDialog', '$timeout',
    'nvButtonFilePickerType', 'nvFileSelectDialog', 'Claim'
  ];

  function Controller($q, $scope, Ticketing, Hub, $mdDialog,
    fields, ticketObj, resultData, $rootScope, nvTableUtils,
    nvToast, nvExtendUtils, nvDateTimeUtils, nvTranslate, nvDialog, $timeout,
    nvButtonFilePickerType, nvFileSelectDialog, Claim
  ) {
    const ctrl = this;
    const COLUMNS = ['date', 'user', 'comments'];
    const STATE = {
      IDLE: 'idle',
      WAITING: 'waiting',
    };
    ctrl.exception = {};
    ctrl.ticketObj = _.cloneDeep(ticketObj);
    ctrl.id = ctrl.ticketObj.id;
    ctrl.onEdit = onEdit;
    ctrl.onLoadPage = onLoadPage;
    ctrl.isEditing = false;
    ctrl.loadingSheet = false;
    ctrl.commentsRef = [];
    ctrl.investigatingParties = [];
    ctrl.anotherTicket = [];
    ctrl.ticketImages = [];
    ctrl.investigatingParty = null;
    ctrl.ticketProps = {
      ticketStatus: {
        options: [],
      },
      orderOutcome: {
        options: [],
      },
      ticketCommentUser: {
        options: [],
      },
    };
    ctrl.ticketUsers = [{}];
    ctrl.userRef = {};
    ctrl.investigatingPartyRef = {};
    ctrl.investigatingHubRef = {};
    ctrl.customFieldRefs = {};
    ctrl.isFormEnabled = true;
    ctrl.onCreateFollowUpTicket = onCreateFollowUpTicket;
    ctrl.onChangeTicketStatus = onChangeTicketStatus;
    ctrl.openOtherTicket = openOtherTicket;
    ctrl.openUploadDialog = openUploadDialog;
    ctrl.openTakePhotoDialog = openTakePhotoDialog;

    ctrl.updateTypes = [
      {
        displayName: nvTranslate.instant('container.recovery-tickets.all-updates'),
        value: 'all',
      },
      {
        displayName: nvTranslate.instant('container.recovery-tickets.only-changes'),
        value: 'changes',
      },
      {
        displayName: nvTranslate.instant('container.recovery-tickets.only-comments'),
        value: 'comments',
      },
    ];

    ctrl.refCustomFields = [];
    ctrl.refComments = [];
    ctrl.onCancel = onCancel;
    ctrl.onCancelTicket = onCancelTicket;
    ctrl.onDeleteImage = onDeleteImage;
    ctrl.commentsTableParams = nvTableUtils.generate(ctrl.comments, { columns: COLUMNS });
    ctrl.state = STATE.IDLE;
    ctrl.cancelState = STATE.IDLE;
    ctrl.followUpState = 'idle';
    ctrl.changeCommentsFilter = changeCommentsFilter;


    onLoadPage();

    function onCancel() {
      $mdDialog.cancel(ctrl.ticketObj);
    }

    async function onEdit() {
      ctrl.state = STATE.WAITING;
      ctrl.deleteState = STATE.WAITING;
      let payload = _.cloneDeep(ctrl.ticketProps);
      // add ticket status to customFields
      payload.customFields = transformBackCustomField(ctrl.ticketProps.customFields);

      // check for resolved ticket with type missing or damaged and outcome is `declared`
      const {
        ticketDetails: { ticketType, investigatingHubId, trackingId, id },
        orderDetails: { shipper_id },
        ticketStatus,
      } = ctrl.ticketProps;
      if ((ticketType === 'MISSING' || ticketType === 'DAMAGED')
        && ticketStatus.value === 'RESOLVED') {
        // check for order outcome
        const outcome = ctrl.ticketProps.orderOutcome.value;
        if (outcome === 'LOST - DECLARED'
          || outcome === 'LOST - NO RESPONSE - DECLARED'
          || outcome === 'NV LIABLE - FULL'
          || outcome === 'NV LIABLE - PARTIAL'
        ) {
          // create a claim
          const claimPayload = {
            tracking_id: trackingId,
            shipper_id: shipper_id,
            hub_id: investigatingHubId,
            ticket_id: id,
            status: 'PENDING',
          };
          try {
            const response = await Claim.create(claimPayload);
            console.log(response);
            console.log('Success create claim items');
          } catch (err) {
            console.log(err);
          }


        }
      }

      payload.customFields.push(
        transformBackCustomField([ctrl.ticketProps.ticketStatus], ctrl.ticketStatusRef)[0]);
      if (ctrl.ticketProps.orderOutcome && ctrl.ticketProps.orderOutcome.options.length > 0) {
        payload.customFields.push(
          transformBackCustomField([ctrl.ticketProps.orderOutcome], _.get(ctrl.customFieldRefs[ctrl.ticketProps.orderOutcome.id], 'value'))[0]);
      }
      payload.customFields.push(
        transformBackCustomField([ctrl.ticketProps.custZendeskId], _.get(ctrl.customFieldRefs[ctrl.ticketProps.custZendeskId.id], 'value'))[0]
      );
      payload.customFields.push(
        transformBackCustomField([ctrl.ticketProps.shipperZendeskId], _.get(ctrl.customFieldRefs[ctrl.ticketProps.shipperZendeskId.id], 'value'))[0]
      );
      const ticketNotes = _.cloneDeep(ctrl.customFieldRefs[ctrl.ticketProps.ticketNotes.id]);
      if (ctrl.ticketProps.newInstructions) {
        ticketNotes.value = ctrl.ticketProps.newInstructions;
      }
      payload.customFields.push(
        transformBackCustomField([ticketNotes], _.get(ctrl.customFieldRefs[ctrl.ticketProps.ticketNotes.id], 'value'))[0]
      );

      payload.customFields.push(
        transformBackCustomField([ctrl.ticketProps.trackingId], _.get(ctrl.customFieldRefs[ctrl.ticketProps.trackingId.id], 'value'))[0]
      );

      // add current user to reporterName and reporterId
      if ($rootScope.user) {
        payload.reporterName = _.compact([$rootScope.user.firstName, $rootScope.user.lastName]).join(' ');
        payload.reporterId = $rootScope.user.thirdPartyId;
        payload.reporterEmail = $rootScope.user.email;
      }
      // filter out customfields with updated = false;
      _.remove(payload.customFields, field => (!field.fieldUpdated));
      payload = _.omit(payload,
        [
          'comments',
          'ticketComments',
          'ticketLogs',
          'ticketCommentUser.options',
          'orderDetails',
          'orderOutcome.options',
          'ticketStatus.options',
          'ticketStatus',
          'orderOutcome',
          'ticketCommentUser',
          'ticketDetails',
          'custZendeskId',
          'shipperZendeskId',
          'ticketNotes',
          'trackingId',
        ]);

      return Ticketing.updateOne(payload, ctrl.id).then(success, onError);

      function onError() {
        ctrl.state = STATE.IDLE;
        return $q.reject();
      }
      function success(result) {
        if (result.status === 'FAILED') {
          nvToast.error(result.message);
          return $q.reject();
        }
        const newUpdateTicketPayload = {
          current_assignee_group_id: ctrl.investigatingParty !== ctrl.investigatingPartyRef ?
            ctrl.investigatingParty : null,
        };
        if (ctrl.ticketProps.ticketCommentUser) {
          newUpdateTicketPayload.current_assignee_user_email = ctrl.ticketProps.ticketCommentUser.value !== ctrl.userRef ? _.get(ctrl.ticketProps.ticketCommentUser.value, 'email') : null;
        }
        if (ctrl.investigatingHub) {
          newUpdateTicketPayload.investigating_hub_id =
            ctrl.investigatingHub !== ctrl.investigatingHubRef ?
              ctrl.investigatingHub : null;
        }
        if (!_.isEmpty(_.pickBy(newUpdateTicketPayload))) {
          return uploadUsingV2(result, newUpdateTicketPayload);
        }
        return showSuccessInfoAndPutToTable(result);
      }

      function showSuccessInfoAndPutToTable(result) {
        ctrl.state = STATE.IDLE;
        nvToast.success(nvTranslate.instant('container.recovery-tickets.ticket-id-x-updated', { x: ctrl.id }));
        applyChangesToTicket(result);
        return $mdDialog.hide(ctrl.ticketObj);
      }

      function uploadUsingV2(resultv1, payloadv2) {
        Ticketing.updateTicket(ctrl.id, _.pickBy(payloadv2)).then(successV2, onError);

        function successV2(result) {
          if (result) {
            const ticketDetails = _.assign(_.get(resultv1, 'ticketDetails'), {
              investigatingParty: _.get(result, 'ticket.investigatingParty'),
              assigneeName: _.get(result, 'ticket.assigneeName'),
              investigating_hub_id: _.get(result, 'ticket.investigatingHubId'),
            });
            resultv1.ticketDetails = ticketDetails;
          }
          return showSuccessInfoAndPutToTable(resultv1);
        }
      }
    }

    function applyChangesToTicket(updateResponse) {
      const ticketDetails = updateResponse.ticketDetails;
      _.assign(ctrl.ticketObj, ticketDetails);

      // ticketStatus saved as status in ticket obj
      if (ticketDetails.ticketStatus) {
        ctrl.ticketObj.status = ticketDetails.ticketStatus;
      }


      // iterate customfield
      _.each(updateResponse.customFields, (field) => {
        if (field.fieldName === 'TRACKING ID') {
          ctrl.ticketObj.customFields['TRACKING ID'] = field.fieldValue;
        }

        if (field.fieldName.includes('ORDER OUTCOME')) {
          ctrl.ticketObj.outcome = field.fieldValue;
        }
      });
    }

    /**
    * retrieve ticket detail
    */
    function onLoadPage() {
      const promises = [];

      if (_.isUndefined(_.get(resultData, 'statusResult'))) {
        promises.push(Ticketing.getTicketStatus());
      } else {
        promises.push($q.when(_.get(resultData, 'statusResult')));
      }

      promises.push(Ticketing.getTicket(ctrl.ticketObj.id));

      if (_.isUndefined(_.get(resultData, 'userResult'))) {
        promises.push(Ticketing.getTicketUser());
      } else {
        promises.push($q.when(_.get(resultData, 'userResult')));
      }

      if (_.isUndefined(_.get(resultData, 'groupResult'))) {
        promises.push(Ticketing.getGroup());
      } else {
        promises.push($q.when(_.get(resultData, 'groupResult')));
      }

      promises.push(Ticketing.searchTicketByTrackingId(ctrl.ticketObj.trackingId));
      promises.push(Ticketing.listImages(ctrl.ticketObj.id));

      if (_.isUndefined(_.get(resultData, 'hubResult'))) {
        promises.push(Hub.read({ active_only: false }));
      } else {
        promises.push($q.when(_.get(resultData, 'hubResult')));
      }

      return $q.all(promises)
        .then(success, $q.reject());

      function success(values) {
        // handle the ticket detail call
        ctrl.ticketProps = _.merge(ctrl.ticketProps, values[1]);
        ctrl.ticketProps.ticketStatus.options = _.map(values[0], o => ({
          displayName: o.name,
          value: o.name,
        }
        ));
        ctrl.ticketProps.ticketStatus = _.merge(ctrl.ticketProps.ticketStatus,
          {
            id: 398,
            name: 'TICKET STATUS',
          });

        // set ticketstatus ref
        ctrl.ticketStatusRef = ctrl.ticketProps.ticketDetails.ticketStatus;
        if (ctrl.ticketStatusRef === 'CANCELLED' || ctrl.ticketStatusRef === 'RESOLVED') {
          ctrl.isFormEnabled = false;
        }
        ctrl.ticketProps.ticketStatus.value = ctrl.ticketProps.ticketDetails.ticketStatus;
        const result = values[1];

        ctrl.ticketProps.orderDetails = nvExtendUtils.addMoment(ctrl.ticketProps.orderDetails, 'last_scan_time');
        ctrl.ticketProps.comments = transformComments(result.ticketComments, result.ticketLogs);
        // process the customfields

        ctrl.ticketProps.customFields = _.orderBy(transFormCustomfield(ctrl.ticketProps.customFields), ['name'], ['asc']);

        const orderOutcome = _.remove(ctrl.ticketProps.customFields, field => (_.includes(field.name.toLowerCase(), 'order outcome')));
        if (orderOutcome && orderOutcome.length > 0) {
          ctrl.ticketProps.orderOutcome = orderOutcome[0];
          ctrl.customFieldRefs[orderOutcome[0].id] = _.cloneDeep(ctrl.ticketProps.orderOutcome);
        }

        const custZendeskId = _.remove(ctrl.ticketProps.customFields, field => (_.includes(field.name.toLowerCase(), 'cust zendesk')));
        if (custZendeskId && custZendeskId.length > 0) {
          ctrl.ticketProps.custZendeskId = custZendeskId[0];
          ctrl.customFieldRefs[custZendeskId[0].id] = _.cloneDeep(ctrl.ticketProps.custZendeskId);
        }

        const shipperZendeskId = _.remove(ctrl.ticketProps.customFields, field => (_.includes(field.name.toLowerCase(), 'shipper zendesk')));
        if (shipperZendeskId && shipperZendeskId.length > 0) {
          ctrl.ticketProps.shipperZendeskId = shipperZendeskId[0];
          ctrl.customFieldRefs[shipperZendeskId[0].id] =
            _.cloneDeep(ctrl.ticketProps.shipperZendeskId);
        }

        const ticketNotes = _.remove(ctrl.ticketProps.customFields, field => (_.includes(field.name.toLowerCase(), 'ticket notes')));
        if (ticketNotes && ticketNotes.length > 0) {
          ctrl.ticketProps.ticketNotes = ticketNotes[0];
          ctrl.customFieldRefs[ticketNotes[0].id] = _.cloneDeep(ctrl.ticketProps.ticketNotes);
        }

        const trackingId = _.remove(ctrl.ticketProps.customFields, field => (_.includes(field.name.toLowerCase(), 'tracking id')));
        if (trackingId && trackingId.length > 0) {
          ctrl.ticketProps.trackingId = trackingId[0];
          ctrl.customFieldRefs[trackingId[0].id] = _.cloneDeep(ctrl.ticketProps.trackingId);
        }

        // add ticket comment users
        // using user name from ticketDetails
        ctrl.ticketProps.ticketCommentUser.options = _.map(values[2], value => ({
          value: value,
          displayName: value.name,
        }));
        const user = _.find(ctrl.ticketProps.ticketCommentUser.options, usr =>
          (usr.displayName === ctrl.ticketProps.ticketDetails.assigneeName));
        if (user) {
          ctrl.ticketProps.ticketCommentUser.value = user.value;
          ctrl.userRef = user.value;
        }
        ctrl.ticketProps.ticketDetails.id = ticketObj.id;
        ctrl.investigatingParties = _.map(values[3], o => ({ value: o.id, displayName: o.name }));
        // find the currently selected investigating party
        const selectedInvestigatingParty = _.find(ctrl.investigatingParties,
          { displayName: ctrl.ticketProps.ticketDetails.investigatingParty });
        if (selectedInvestigatingParty) {
          ctrl.investigatingParty = selectedInvestigatingParty.value;
          ctrl.investigatingPartyRef = _.cloneDeep(ctrl.investigatingParty);
        }

        // clone comments for ref
        ctrl.refComments = _.cloneDeep(ctrl.ticketProps.comments);
        ctrl.ticketUsers = _.map(values[2], usr => ({
          displayName: usr.name,
          value: usr,
        }));

        ctrl.anotherTicket = _.compact(_.map(values[4].tickets, (ticket) => {
          if (ticket.id !== ctrl.ticketObj.id) {
            return ticket;
          }
          return null;
        }));

        // images
        ctrl.ticketImages = values[5];
        ctrl.hubs = Hub.toOptions(values[6]);
        const selectedInvestigatingHub = _.find(ctrl.hubs,
          { value: ctrl.ticketProps.ticketDetails.investigatingHubId });
        if (selectedInvestigatingHub) {
          ctrl.investigatingHub = selectedInvestigatingHub.value;
          ctrl.investigatingHubRef = _.cloneDeep(ctrl.investigatingHub);
        }
      }
    }


    function onCancelTicket($event) {
      return nvDialog.confirmDelete($event, {
        title: nvTranslate.instant('container.recovery-tickets.confirm-cancel-ticket'),
        content: nvTranslate.instant('container.recovery-tickets.do-you-want-to-cancel-this-ticket'),
        skipHide: true,
      }).then(() => {
        Ticketing.deleteOne(ticketObj.id).then(() => {
          ticketObj.status = 'CANCELLED';
          nvToast.success(nvTranslate.instant('container.recovery-tickets.ticekt-x-cancelled', { x: ctrl.id }));
          $mdDialog.cancel(ticketObj);
        }
        );
      });
    }


    /**
    * transform two array to one, and add type for each logs or comments
    */
    function transformComments(ticketComments, ticketLogs) {
      const comments = [];
      const TYPE_COMMENTS = 'comments';
      const TYPE_LOGS = 'log';
      _.forEach(ticketComments, (value) => {
        value.type = TYPE_COMMENTS;
        comments.push(value);
      });
      _.forEach(ticketLogs, (value) => {
        value.type = TYPE_LOGS;
        comments.push(value);
      });
      return _.orderBy(comments, ['timestamp'], ['asc']);
    }

    function transFormCustomfield(customFields) {
      const customFieldGens = [];
      // add old reference
      ctrl.refCustomFields = _.cloneDeep(customFields);

      _.forEach(customFields, (value) => {
        const obj = {
          id: value.fieldId,
          name: value.fieldName,
          type: value.fieldType,
          value: value.fieldValue,
          options: value.options,
          disabled: !ctrl.isFormEnabled,
        };
        // hack for disable editing tracking id
        if (value.fieldName === 'TRACKING ID') {
          obj.readonly = true;
        }

        customFieldGens.push(obj);
      });
      return customFieldGens;
    }

    /**
    * rebuild the array to comply with server request model
    */
    function transformBackCustomField(customFieldGen, ref) {
      const customFields = [];
      _.forEach(customFieldGen, (value) => {
        if (value) {
          const obj = {
            fieldId: value.id,
            fieldName: value.name,
            fieldType: value.type,
            fieldValue: value.value,
          };
          // compare with ref object if exist
          if (ref) {
            obj.fieldUpdated = ref !== obj.fieldValue;
          } else {
            // check changed or not
            const refObj = _.find(ctrl.refCustomFields, { fieldId: obj.fieldId });
            if (refObj) {
              if (refObj.fieldType === 'number') {
                obj.fieldUpdated = Number(refObj.fieldValue) !== Number(obj.fieldValue);
              } else {
                obj.fieldUpdated = refObj.fieldValue !== obj.fieldValue;
              }
            } else {
              obj.fieldUpdated = true;// new data
            }
          }


          customFields.push(obj);
        }
      });


      return customFields;
    }

    function onChangeTicketStatus() {
      $timeout(() => {
        if (ctrl.ticketProps.ticketStatus.value === 'RESOLVED' && ctrl.ticketProps.orderOutcome.value) {
          // check for '-'
          if (ctrl.ticketProps.orderOutcome.value === '-') {
            ctrl.modelForm['order-outcome'].$setTouched();
            return onRejectCurrentValue();
          }

          // check is order outcome changed ?
          const customField = transformBackCustomField(ctrl.ticketProps.orderOutcome);
          if (!customField.fieldUpdated) {
            // show the dialog
            nvDialog.confirmSave(null, {
              title: nvTranslate.instant('container.recovery-tickets.order-outcome-not-changed'),
              content: nvTranslate.instant('container.recovery-tickets.do-you-want-to-keep-current-order-outcome'),
              ok: nvTranslate.instant('container.recovery-tickets.keep'),
              cancel: nvTranslate.instant('commons.no'),
              skipHide: true,
            }).then(onKeepCurrentValue, onRejectCurrentValue);
          }
        }
        ctrl.modelForm['order-outcome'].$setTouched();
      });

      function onKeepCurrentValue() {
        // update the ref field, so always be fieldUpdate true
        const refField = _.find(ctrl.customFieldRefs,
          { id: ctrl.ticketProps.orderOutcome.id });
        if (refField) {
          refField.value = `${refField.fieldValue}_`;
        }
      }

      function onRejectCurrentValue() {
        // set order outcome to null
        ctrl.ticketProps.orderOutcome.value = null;
      }
    }

    function openOtherTicket(ticket) {
      const editFields = Ticketing.getAddField();
      return nvDialog.showSingle(null, {
        templateUrl: 'views/container/recovery-tickets/dialog/recovery-tickets-edit/new-recovery-dialog-edit.html',
        locals: {
          fields: editFields,
          ticketObj: ticket,
          resultData: {},
        },
        cssClass: 'nv-edit-ticket-dialog',
        controllerAs: 'ctrl',
        controller: 'NewRecoveryDialogEditController',
      });
    }

    function onCreateFollowUpTicket($event) {
      return nvDialog.showSingle($event, {
        templateUrl: 'views/container/recovery-tickets/dialog/create-follow-up/create-follow-up.html',
        controller: 'CreateFollowUpTicketController',
        controllerAs: 'ctrl',
        locals: {
          ticket: ctrl.ticketObj,
        },
        theme: 'nvGreen',
        cssClass: 'nv-create-follow-up-dialog',
      });
    }

    function changeCommentsFilter() {
      switch (ctrl.updateType) {
        case 'changes':
          ctrl.ticketProps.comments = _.filter(ctrl.refComments, item => (item.type === 'log'));
          break;
        case 'comments':
          ctrl.ticketProps.comments = _.filter(ctrl.refComments, item => (item.type === 'comments'));
          break;
        default:
          ctrl.ticketProps.comments = ctrl.refComments;
          break;
      }
    }

    /** dialog for taking photo */

    function openTakePhotoDialog($event) {
      nvDialog.showSingle($event, {
        templateUrl: 'views/container/recovery-tickets/dialog/photo-taking/photo-taking.dialog.html',
        controller: 'RecoveryTicketPhotoTakingDialogController',
        controllerAs: 'ctrl',
        theme: 'nvGreen',
        cssClass: 'nv-photo-taking-dialog',
        skipHide: true,
      }).then(onFinishSelect);
    }

    function openUploadDialog($event) {
      nvFileSelectDialog.show($event, {
        title: nvTranslate.instant('container.recovery-tickets.attach-images'),
        fileSelect: {
          accept: nvButtonFilePickerType.IMAGE,
          maxFileCount: 10,
        },
        buttonOkTitle: nvTranslate.instant('commons.submit'),
        onFinishSelect: onFinishSelect,
        onDone: angular.noop,
        isImagePicker: true,
        skipHide: true,
      });
    }

    function onFinishSelect(files) {
      ctrl.uploadingImage = true;
      ctrl.success = [];
      ctrl.errors = [];
      showLoadingSheet(true);
      const ticketId = ctrl.ticketProps.ticketDetails.id;
      const promises = _.map(files, file => (Ticketing.uploadImage(file, ticketId)));
      return $q.all(promises).then(onFinishUpload);

      function onFinishUpload(results) {
        showLoadingSheet(false);
        _.each(results, (result) => {
          if (result.status === 'Success') {
            ctrl.ticketImages.push({
              photo_url: result.photo_url,
              thumbnail_url: result.thumbnail_url,
              file_name: result.photo_url,
              ticket_id: ticketId,
              id: result.photo_id,
            });
          }
        });
      }
    }

    function onDeleteImage(id) {
      return Ticketing.deleteImage(ctrl.ticketObj.id, id).then(onFinishDelete);

      function onFinishDelete() {
        _.remove(ctrl.ticketImages, { id: id });
      }
    }

    function showLoadingSheet(flag) {
      ctrl.loadingSheet = flag;
    }
  }
}());
