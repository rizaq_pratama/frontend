(function controller() {
  angular.module('nvOperatorApp.controllers')
        .controller('TicketingCsvSearchDialogController', TicketingCsvSearchDialogController);

  TicketingCsvSearchDialogController.$inject = ['Ticketing', 'otherIds', 'pendingAndInProgressIds', 'Excel', '$mdDialog',
    'nvTranslate', '$timeout', '$window', '$state', 'nvToast', '$q'];

  function TicketingCsvSearchDialogController(Ticketing, otherIds, pendingAndInProgressIds, Excel, $mdDialog,
      nvTranslate, $timeout, $window, $state, nvToast, $q) {
    const ctrl = this;

    ctrl.onCancel = onCancel;
    ctrl.onSelectFile = onSelectFile;
    ctrl.onClickDone = onClickDone;
    ctrl.callSearch = callSearch;
    ctrl.changeFile = changeFile;
    ctrl.searchText = '';
    ctrl.searchTextExtra = '';
    ctrl.state = 'idle';
    ctrl.file = null;
    ctrl.trackingIds = [];
    ctrl.notFoundTrackingIds = [];
    ctrl.isOnlyPendingAndInProgress = true;
    ctrl.STATE = {
      READY: 'ready',
      UPLOADING: 'uploading',
      UPLOADED: 'uploaded',
    };
    ctrl.BUTTON_STATE = {
      IDLE: 'idle',
      WAITING: 'waiting',
    };
    ctrl.SUCCESS_FLAG = {
      SUCCESS: 'Success',
      FAIL: 'Fail',
    };
    ctrl.readyToSearch = false;
    ctrl.showState = ctrl.STATE.READY;
    ctrl.filename = '';
    ctrl.legendItems = [];
    ctrl.legendExtra = [];
    ctrl.SAMPLE_CSV = [{
      tracking_id: 'NVSG-009090',
    },
    {
      tracking_id: 'NVSG-009091',
    }];

    function onCancel() {
      return $mdDialog.cancel();
    }

    function changeFile() {
      ctrl.readyToSearch = false;
      ctrl.filename = '';
      ctrl.trackingIds = [];
    }

    function onSelectFile(file) {
      ctrl.filename = file.name;
      Excel.read(file, false).then(excelReadSuccess);

      function excelReadSuccess(response) {
        // set the dialog state to `uploading`
        ctrl.trackingIds = _(response.data)
                    .filter(data => data['A'] != null)
                    .map(data => (_.trim(data['A'])))
                    .uniq()
                    .value();
        if (ctrl.trackingIds.length === 0) {
          nvToast.warning('CSV doesnt contain any data');
          ctrl.readyToSearch = false;
        }
        ctrl.readyToSearch = true;
      }
    }

    function callSearch(trackingIds) {
      ctrl.showState = ctrl.STATE.UPLOADING;
      // check the if the checkbox checked
      let ticketStatus = pendingAndInProgressIds;
      if (!ctrl.isOnlyPendingAndInProgress) {
        ticketStatus = _.concat(ticketStatus, otherIds);
      }
      ctrl.payload = {
        ticket_statuses: ticketStatus,
        tracking_ids: trackingIds,
      };
      return Ticketing.searchTicket(ctrl.payload, 1, 1000)
        .then(onFinishSearch);
    }


    function onFinishSearch(result) {
      // check the result
      ctrl.searchResult = result;
      const foundTrackingIds = _.uniq(_.map(result.tickets, t => _.trim(t.trackingId)));
      if (foundTrackingIds.length < ctrl.trackingIds.length) {
        // there are differences..
        // and show it
        ctrl.notFoundTrackingIds = _.difference(ctrl.trackingIds, foundTrackingIds);
        ctrl.showState = ctrl.STATE.UPLOADED;
        return 0;
      }
      // return the result
      return $mdDialog.hide({
        result: ctrl.searchResult,
        filter: ctrl.payload,
      });
    }

    function onClickDone() {
      return $mdDialog.hide({
        result: ctrl.searchResult,
        filter: ctrl.payload,
      });
    }
  }
}());
