(function controller() {
  angular.module('nvOperatorApp.controllers')
        .controller('TicketingCsvUploadDialogController', TicketingCsvUploadDialogController);

  TicketingCsvUploadDialogController.$inject = ['Ticketing', 'Excel', '$mdDialog', 
    'nvTranslate', '$timeout', '$window', '$state'];

  function TicketingCsvUploadDialogController(Ticketing, Excel, $mdDialog, 
      nvTranslate, $timeout, $window, $state) {
    const ctrl = this;
    const TRACKING_ID = 'tracking_id';
    const INVALID_TRACKING_ID_REASON = 'Invalid Tracking ID';

    ctrl.onCancel = onCancel;
    ctrl.onSelectFile = onSelectFile;
    ctrl.onUpload = onUpload;
    ctrl.onClickDone = onClickDone;
    ctrl.uploadAnotherFile = uploadAnotherFile;
    ctrl.searchLegend = searchLegend;
    ctrl.searchLegendExtra = searchLegendExtra;
    ctrl.openHubAdministration = openHubAdministration;
    ctrl.toggleInstructions = toggleInstructions;
    ctrl.showCheckDialog = showCheckDialog;
    ctrl.proceed = proceed;
    ctrl.searchText = '';
    ctrl.searchTextExtra = '';
    ctrl.showInstructions = false;
    ctrl.selectedTab = 0;
    ctrl.checkForm = {};
    ctrl.checkForm.searchJobId = checkJobId;


    ctrl.successUploads = [];
    ctrl.failedUploads = [];
    ctrl.state = 'idle';
    ctrl.file = null;
    let legendCache = [];
    let legendExtraCache = [];
    ctrl.STATE = {
      READY: 'ready',
      UPLOADING: 'uploading',
      UPLOADED: 'uploaded',
      VALIDATION_FAILED: 'validation_failed',
      TOO_MANY_ROWS: 'too_many_rows',
      ASYNC_UPLOADED: 'async_uploaded',
    };
    ctrl.BUTTON_STATE = {
      IDLE: 'idle',
      WAITING: 'waiting',
    };
    ctrl.SUCCESS_FLAG = {
      SUCCESS: 'Success',
      FAIL: 'Fail',
    };
    ctrl.showState = ctrl.STATE.READY;
    ctrl.legendItems = [];
    ctrl.legendExtra = [];
    ctrl.SAMPLE_CSV = [{
      tracking_id: 'NVSG-009090',
      type: 'M',
      sub_type: null,
      investigating_group: 'RFS',
      assignee_email: 'cheeyong@ninjavan.sg',
      investigating_hub_id: 1,
    },
    {
      tracking_id: 'NVSG-009091',
      type: 'D',
      sub_type: 'PC',
      investigating_group: 'PMS',
      assignee_email: 'kim.dong.ah@ninjavan.sg',
      investigating_hub_id: 1,
    }];
    ctrl.SAMPLE_CSV_HEADER = ['tracking_id', 'type', 'sub_type', 'investigating_group', 'assignee_email', 'investigating_hub_id'];
    ctrl.ERROR_CSV_HEADER = ['tracking_id', 'type', 'sub_type', 'investigating_group', 'assignee_email', 'investigating_hub_id', 'reason'];
    init();

    function init() {
      // load the acronym here
      return Ticketing.getAcronyms().then(success);

      function success(results) {
        // parse ticket type
        _.each(_.get(results, 'ticket_types'), (type) => {
          if (type.ticketSubtypes && type.ticketSubtypes.length > 0) {
            _.each(_.get(type, 'ticketSubtypes'), (sub) => {
              if (!sub.archived) {
                ctrl.legendItems.push({
                  type: type.name,
                  typeAcronym: type.acronym,
                  subtype: sub.name,
                  subtypeAcronym: sub.acronym,
                });
              }
            });
          } else {
            ctrl.legendItems.push({
              type: type.name,
              typeAcronym: type.acronym,
            });
          }
        });
        // put investigating parties
        legendCache = _.sortBy(ctrl.legendItems, ['type', 'subtype']);
        legendExtraCache = _.sortBy(_.get(results, 'groups'), ['name']);
        ctrl.legendItems = _.cloneDeep(legendCache);
        ctrl.legendExtra = _.cloneDeep(legendExtraCache);
      }
    }

    function toggleInstructions() {
      ctrl.showInstructions = !ctrl.showInstructions;
    }

    function onCancel() {
      return $mdDialog.cancel();
    }

    function onSelectFile(file) {
      ctrl.file = file;
    }

    function onClickDone() {
      return $mdDialog.hide(ctrl.successUploads);
    }

    function uploadAnotherFile() {
      ctrl.file = null;
      ctrl.state = ctrl.BUTTON_STATE.IDLE;
      ctrl.failedUploads = [];
      ctrl.successUploads = [];
      ctrl.showState = ctrl.STATE.READY;
    }

    function checkJobId(jobId) {
      ctrl.checkForm.currentJobId = null;
      ctrl.checkForm.status = null;
      return Ticketing.getCsvJobStatus(jobId).then((response) => {
        ctrl.checkForm.currentJobId = jobId;
        ctrl.checkForm.status = response.status;
      });
    }

    function onUpload() {
      ctrl.state = ctrl.BUTTON_STATE.WAITING;
      ctrl.data = [];
      ctrl.cols = [];
      ctrl.invalidData = [];
      // validationg the tracking id
      return Excel.read(ctrl.file).then(onSuccessRead);
      function onSuccessRead(data) {
        ctrl.data = data.data;
        ctrl.cols = data.cols;
        if (ctrl.data.length > 200) {
          return Ticketing.createByCsvAsync(ctrl.file).then(onSuccessAsync);
        }
        const trackingIds = _.map(ctrl.data, row => (row.tracking_id));
        return Ticketing.validateTrackingIds(trackingIds).then(onSuccessValidate);
      }

      function onSuccessAsync(response) {
        // show the uuid
        ctrl.asyncId = response.uuid;
        ctrl.showState = ctrl.STATE.ASYNC_UPLOADED;
        ctrl.state = ctrl.BUTTON_STATE.READY;
      }

      function onSuccessValidate(result) {
        _.forEach(result, (res) => {
          // pullout invalid tracking id from the csv data
          if (!res.order_id) {
            const pulledDatas = _.remove(ctrl.data, d => (d.tracking_id === res.tracking_id));
            const pulled = _.assign(_.first(pulledDatas), { reason: INVALID_TRACKING_ID_REASON });
            ctrl.invalidData.push(pulled);
          }
        });
        // check for invalid data
        // if all valid continue with upload
        // otherwise show failed row screen
        if (ctrl.invalidData.length === 0) {
          return onUploadCsv();
        }
        ctrl.file = Excel.writeCsv(ctrl.cols, ctrl.data);
        ctrl.showState = ctrl.STATE.VALIDATION_FAILED;
        ctrl.state = ctrl.STATE.READY;
        return angular.noop();
      }
    }

    function onUploadCsv() {
      ctrl.failedUploads = [];
      ctrl.successUploads = [];
      ctrl.showState = ctrl.STATE.UPLOADING;
      return Ticketing.createByCsv(ctrl.file)
        .then(success);
      function success(results) {
        ctrl.state = 'idle';
        ctrl.showState = ctrl.STATE.UPLOADED;
        _.each(results, (result) => {
          if (result.status === ctrl.SUCCESS_FLAG.SUCCESS) {
            ctrl.successUploads.push(result);
          } else if (result.trackingId !== TRACKING_ID) {
            ctrl.failedUploads.push(result);
          }
        });
      }
    }

    function proceed() {
      onUploadCsv();
    }

    function openHubAdministration() {
      $window.open(
        $state.href('container.hub-list')
      );
    }

    function showCheckDialog() {
      ctrl.showState = ctrl.STATE.READY;
      ctrl.state = ctrl.BUTTON_STATE.IDLE;
      ctrl.selectedTab = 1;
      ctrl.file = null;
    }

    function searchLegendExtra() {
      $timeout(() => {
        const loweredSearchText = _.lowerCase(ctrl.searchTextExtra);
        ctrl.legendExtra = _.filter(_.cloneDeep(legendExtraCache), item =>
        ((_.lowerCase(item.name).includes(loweredSearchText)) ||
          (_.lowerCase(item.acronym).includes(loweredSearchText))));
      });
    }

    function searchLegend() {
      $timeout(() => {
        const loweredSearchText = _.lowerCase(ctrl.searchText);
        ctrl.legendItems = _.filter(_.cloneDeep(legendCache), item => (
          (_.lowerCase(item.type).includes(loweredSearchText)) ||
          (_.lowerCase(item.subtype).includes(loweredSearchText)) ||
          (_.lowerCase(item.acronym).includes(loweredSearchText)) ||
          (_.lowerCase(item.subtypeAcronym).includes(loweredSearchText))
        ));
      });
    }
  }
}());
