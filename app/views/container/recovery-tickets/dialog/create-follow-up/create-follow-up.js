(function controller() {
    angular.module('nvOperatorApp.controllers')
        .controller('CreateFollowUpTicketController', controllerFn);

    controllerFn.$inject = ['Ticketing', '$mdDialog', '$q', '$rootScope', 'nvToast', 'ticket'];

    function controllerFn(Ticketing, $mdDialog, $q, $rootScope, nvToast, ticket) {
        const ctrl = this;
        ctrl.followedTicket = angular.copy(ticket);

        //fucntions
        ctrl.onLoad = onLoad;
        ctrl.onCreate = onCreate;
        ctrl.onClose = onClose;
        ctrl.sourceEntries = [];
        ctrl.investigatingParties = [];
        ctrl.ticketUsers = [];
        ctrl.state = 'idle';
        ctrl.ticket = angular.copy(ticket);


        function onLoad() {
            return $q.all([
                Ticketing.getTicketUser(),
                Ticketing.getGroup(),
                Ticketing.getEntrySource()
            ]).then(success);

            function success(results) {
                ctrl.ticketUsers = _.map(results[0], usr => ({
                    displayName: usr.name,
                    value: usr,
                }));
                ctrl.investigatingParties = _.map(results[1], o => ({ value: o.id, displayName: o.name }));
                ctrl.sourceEntries = _.map(results[2], o => ({ value: o.id, displayName: o.entrySource }));
            }

        }

        function onCreate() {
            ctrl.state = 'waiting';
            let payload = {
                investigating_party_group_id: ctrl.investigatingParty,
                entry_source_id: ctrl.sourceOfEntry,
                assignee_user_email: null,
                assignee_user_name: null
            };
            if (ctrl.assignee) {
                payload.assignee_user_name = ctrl.assignee.name;
                payload.assignee_user_email = ctrl.assignee.email;
            } 
            // add current user to reporterName and reporterId
            if ($rootScope.user) {
                payload.creator_user_name = _.compact([$rootScope.user.firstName, $rootScope.user.lastName]).join(' ');
                payload.creator_user_email = $rootScope.user.email;
            }

            return Ticketing.createFollowUp(ctrl.ticket.id, payload)
                .then(success);

            function success(result) {
                ctrl.state = 'idle';
                if(result.status === 'Success'){
                    nvToast.success(result.message);
                    $mdDialog.hide(result.ticket);
                }else{
                    nvToast.error(result.message)
                    $mdDialog.cancel();
                }
                
            }
        }


        function onClose() {
            return $mdDialog.cancel();
        }

    }
}());