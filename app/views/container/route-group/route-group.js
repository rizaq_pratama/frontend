(function routeGroupController() {
  angular
    .module('nvOperatorApp.controllers')
    .controller('RouteGroupController', RouteGroupController);

  RouteGroupController.$inject = [
    '$q', 'RouteGroup', 'Transaction', 'Reservation', 'Shippers',
    'DistributionPoint', 'nvDialog', 'nvEditModelDialog',
    'nvUtils', '$timeout', 'nvDateTimeUtils', '$state',
    'nvToast', '$mdDialog', 'nvTranslate', 'nvTable',
    'Hub',
  ];

  function RouteGroupController(
    $q, RouteGroup, Transaction, Reservation, Shippers,
    DistributionPoint, nvDialog, nvEditModelDialog,
    nvUtils, $timeout, nvDateTimeUtils, $state,
    nvToast, $mdDialog, nvTranslate, nvTable,
    Hub
  ) {
    // variables
    const ctrl = this;

    let dpById = {};   // dps by id
    let txnById = {};  // transactions by id
    let rvnById = {};  // reservations by id
    let hubOptions = []; // hubs options
    let hubOptionsById = {}; // hub options by id
    let hubByRouteGroupId = {};

    ctrl.routeGroups = [];
    ctrl.routeGroupsTableParam = nvTable.createTable(RouteGroup.FIELDS);
    ctrl.routeGroupsTableParam.addColumn('_createdAt', {
      displayName: 'container.route-group.created-date',
    });
    ctrl.routeGroupsTableParam.addColumn('_txnsLength', {
      displayName: 'container.route-group.no-of-txn',
    });
    ctrl.routeGroupsTableParam.addColumn('_routedTxns', {
      displayName: 'container.route-group.no-of-routed-txn',
    });
    ctrl.routeGroupsTableParam.addColumn('_rvnsLength', {
      displayName: 'container.route-group.no-of-rvn',
    });
    ctrl.routeGroupsTableParam.addColumn('_routedRvns', {
      displayName: 'container.route-group.no-of-routed-rvn',
    });
    ctrl.routeGroupsTableParam.addColumn('hub.displayName', {
      displayName: 'commons.model.hub',
    });

    ctrl.showFilterSection = false;
    ctrl.filter = {
      minDate: null,                            // initialized later
      maxDate: moment().add(2, 'day').toDate(), // buffer the max date by 2 days
      frDate: null,                             // initialized later
      toDate: new Date(),
      searchText: '',
      filtersForSearchBox: [filterById, filterByName, filterByTxnId, filterByRvnId],
    };

    // functions
    ctrl.searchAll = searchAll;
    ctrl.createOne = createOne;
    ctrl.updateOne = updateOne;
    ctrl.deleteOne = deleteOne;
    ctrl.deleteSelected = deleteSelected;
    ctrl.editRouteMap = editRouteMap;
    ctrl.filterAll = filterAll;
    ctrl.onDateChange = onDateChange;
    ctrl.translateCount = translateCount;

    function searchAll() {
      return RouteGroup.read().then(success, $q.reject);

      function success(response) {
        ctrl.routeGroups = _.sortBy(extend(response.routeGroups), 'id');

        const txnIds = listTransactionIds(ctrl.routeGroups);
        const rvnIds = listReservationIds(ctrl.routeGroups);

        return $q.all([
          Transaction.searchWithRouteIdsOnlyByIds(txnIds),
          Reservation.searchWithRouteIdsOnlyByIds(rvnIds),
          DistributionPoint.listDPs({ is_active: true, is_public: true }),
          Hub.read(),
          Hub.getRouteGroupOfHubs(),
        ]).then(successAll);
      }

      function successAll([
        transactions,
        reservations,
        distributionPoints,
        hubs,
        hubRouteGroups,
      ]) {
        txnById = _.keyBy(transactions, txn => txn.id);
        rvnById = _.keyBy(reservations, rvn => rvn.id);
        ctrl.routeGroups = extendWithTxns(ctrl.routeGroups);
        ctrl.routeGroups = extendWithRvns(ctrl.routeGroups);
        dpById = _.keyBy(distributionPoints, dp => dp.id);
        hubOptions = Hub.toOptionsWithId(hubs);
        hubOptionsById = _.keyBy(hubOptions, hub => hub.value);
        hubByRouteGroupId = flipRouteGroupByHubId(hubRouteGroups);
        ctrl.routeGroups = extendWithHubs(ctrl.routeGroups);
        RouteGroup.setOptions('hub', hubOptions);
        initFilter(ctrl.filter, ctrl.routeGroups);
        tableHack();
      }
    }

    function createOne($event) {
      const fields = RouteGroup.getAddFields();

      nvDialog.showSingle($event, {
        templateUrl: 'views/container/route-group/dialog/route-group-add.dialog.html',
        cssClass: 'route-group-add',
        scope: _.assign(nvEditModelDialog.scopeTemplate({ onSave: create }), { fields: fields }),
      }).then(success);

      function create(data) {
        return RouteGroup.create(data.name, data.description).then((routeGroup) => {
          if (data.hub !== null) {
            return Hub.addRouteGroupsToHub(data.hub.value, routeGroup.routeGroup.id);
          }
          return $q.resolve(true);
        });
      }

      function success() {
        goToPage('container.transactions-v2');
      }
    }

    function updateOne($event, id) {
      const routeGroup = _.find(ctrl.routeGroups, ['id', id]);
      const fields = RouteGroup.getEditFields(routeGroup);

      const referencesToRemove = {
        transactionIds: [],
        reservationIds: [],
      };

      const dataFields = {
        _idx: { displayName: 'container.route-group.dialogs.idx' },
        id: { displayName: 'container.route-group.dialogs.id' },
        orderId: { displayName: 'container.route-group.dialogs.order-id' },
        trackingId: { displayName: 'container.route-group.dialogs.tracking-id' },
        type: { displayName: 'container.route-group.dialogs.type' },
        shipper: { displayName: 'container.route-group.dialogs.shipper' },
        address: { displayName: 'container.route-group.dialogs.address' },
        routeId: { displayName: 'container.route-group.dialogs.route-id' },
        status: { displayName: 'container.route-group.dialogs.status' },
        sdt: { displayName: 'container.route-group.dialogs.sdt' },
        edt: { displayName: 'container.route-group.dialogs.edt' },
        dp: { displayName: 'container.route-group.dialogs.dp' },
        pickupSize: { displayName: 'container.route-group.dialogs.pickup-size' },
        comments: { displayName: 'container.route-group.dialogs.comments' },
        priorityLevel: { displayName: 'commons.model.priority-level' },
      };
      ctrl.tableParam = nvTable.createTable(dataFields);

      const txnIds = _(routeGroup._txns).keys().map(i => +i).value();
      const rvnIds = _(routeGroup._rvns).keys().map(i => +i).value();

      const promise = $q.all([
        Transaction.searchByIds(txnIds),
        Reservation.searchByIds(rvnIds),
      ]).then(searchSuccess);

      ctrl.tableParam.setDataPromise(promise);

      nvDialog.showSingle($event, {
        templateUrl: 'views/container/route-group/dialog/route-group-edit.dialog.html',
        cssClass: 'route-group-edit',
        scope: _.assign(nvEditModelDialog.scopeTemplate({ onSave: update }), {
          fields: fields,
          routeGroup: routeGroup,
          deleteOne: deleteOne,
          removeFromRouteGroup: removeFromRouteGroup,
          goToPage: goToPage,
          tableParam: ctrl.tableParam,
          dataCsvHeader: {
            displayName: _.map(dataFields, o => nvTranslate.instant(o.displayName)),
            varName: _.keys(dataFields),
          },
        }),
      }).then(success).then(updateHubOfRouteGroup);

      function searchSuccess([
        transactions,
        reservations,
      ]) {
        txnById = _.keyBy(transactions, txn => txn.id);
        rvnById = _.keyBy(reservations, rvn => rvn.id);

        let updatedRouteGroup = routeGroup;
        updatedRouteGroup = extendWithTxns([updatedRouteGroup])[0];
        updatedRouteGroup = extendWithRvns([updatedRouteGroup])[0];
        ctrl.routeGroups = nvUtils.mergeIn(ctrl.routeGroups, updatedRouteGroup, 'id', updatedRouteGroup.id);

        const txns = getTxnJobs(_.values(updatedRouteGroup._txns));
        const rvns = getRvnJobs(_.values(updatedRouteGroup._rvns));

        return extendJobs(_.concat(txns, rvns));
      }

      function update(theData) {
        referencesToRemove.transactionIds = _(referencesToRemove.transactionIds)
          .compact().uniq().value();
        referencesToRemove.reservationIds = _(referencesToRemove.reservationIds)
          .compact().uniq().value();

        const hubDidChanged = theData.hub !== null &&
          (hubByRouteGroupId[theData.id] === undefined ||
            theData.hub.value !== hubByRouteGroupId[theData.id].hub_id);
        const hubDidRemoved = theData.hub === null && hubByRouteGroupId[theData.id] !== undefined;

        return $q.all([
          RouteGroup.update(theData.id, theData.name, theData.description),
          RouteGroup.removeReferences(theData.id, referencesToRemove),
          (hubDidChanged) ? Hub.addRouteGroupsToHub(theData.hub.value, theData.id) : angular.noop,
          ((hubDidChanged && hubByRouteGroupId[theData.id] !== undefined)
            || hubDidRemoved) ? Hub.removeRouteGroupsFromHub(
            hubByRouteGroupId[theData.id].hub_id, theData.id
          ) : angular.noop,
        ]);
      }

      function success(response) {
        const rg0 = _.pick(response[0].routeGroup, ['id', 'name', 'description']);
        const rg1 = _.pick(response[1].routeGroup, ['transactionIds', 'reservationIds']);
        let mergedRG = extend([_.assign({}, rg0, rg1)]);
        mergedRG = extendWithTxns(mergedRG);
        mergedRG = extendWithRvns(mergedRG);
        const theRouteGroup = mergedRG[0];
        nvToast.success(`${nvTranslate.instant('commons.id')}: ${theRouteGroup.id}`, nvTranslate.instant('container.route-group.num-route-group-updated', { num: 1 }));
        ctrl.routeGroups = nvUtils.mergeIn(ctrl.routeGroups, theRouteGroup, 'id', theRouteGroup.id);
        filterAll(ctrl.filter.searchText);
      }

      function removeFromRouteGroup(tableParam, modelForm) {
        if (tableParam.getSelectionCount() <= 0) {
          nvToast.warning(nvTranslate.instant('container.route-group.error-select-transaction'));
          return;
        }
        const jobs = tableParam.getSelection();
        _.forEach(jobs, (job) => {
          switch (job._typeId) {
            case 1: // transaction
              referencesToRemove.transactionIds.push(job.id);
              break;
            case 2: // reservation
              referencesToRemove.reservationIds.push(job.id);
              break;
            default:
              return;
          }
        });
        tableParam.remove(jobs, 'id');
        modelForm.$dirty = true;
      }
    }

    function deleteOne($event, routeGroup) {
      $mdDialog.cancel(); // make sure cancel the prev dialog before open new one

      const id = routeGroup.id;
      const name = routeGroup.name;

      nvDialog.confirmDelete($event, {
        content: nvTranslate.instant('container.route-group.confirm-delete-route-group', { name: name }),
      }).then(onDelete);

      function onDelete() {
        return RouteGroup.delete(id).then(success);
      }

      function success() {
        nvToast.warning(name, nvTranslate.instant('container.route-group.num-route-group-deleted', { num: 1 }));
        _.remove(ctrl.routeGroups, theRouteGroup => id === theRouteGroup.id);
        filterAll(ctrl.filter.searchText);
      }
    }

    function deleteSelected($event, routeGroups) {
      $mdDialog.cancel(); // make sure cancel the prev dialog before open new one

      nvDialog.showSingle($event, {
        templateUrl: 'views/container/route-group/dialog/bulk-delete/bulk-delete.dialog.html',
        theme: 'nvRed',
        cssClass: 'route-group-bulk-delete',
        controller: 'RouteGroupBulkDeleteDialogController',
        controllerAs: 'ctrl',
        locals: {
          routeGroupsData: routeGroups,
        },
      }).then(onDelete);

      function onDelete(response) {
        ctrl.routeGroupsTableParam.clearSelect();

        const deletedRouteGroups = response.success;
        _.forEach(deletedRouteGroups, (routeGroup) => {
          _.remove(ctrl.routeGroups, theRouteGroup => routeGroup.id === theRouteGroup.id);
        });

        filterAll(ctrl.filter.searchText);
      }
    }

    function editRouteMap(routeGroup) {
      $state.go('container.zonal-routing.assignment', {
        route_group_ids: [routeGroup.id],
      });
    }

    function filterAll(searchText) {
      ctrl.filter.searchText = searchText.trim().toLowerCase();

      let wrapped = _(ctrl.routeGroups)
        .filter((routeGroup) => {
          const mm = moment(routeGroup.createdAt);
          const fr = nvDateTimeUtils.toSOD(ctrl.filter.frDate);
          const to = nvDateTimeUtils.toEOD(ctrl.filter.toDate);
          return mm.isBetween(fr, to, null, '[]');
        });

      if (_.size(ctrl.filter.searchText) > 0) {
        wrapped = wrapped.filter(routeGroup =>
          _.some(ctrl.filter.filtersForSearchBox, filter =>
            filter.call(null, ctrl.filter.searchText, routeGroup)
          )
        );
      }

      ctrl.routeGroupsTableParam.setData(wrapped.sortBy('id').value());
      ctrl.routeGroupsTableParam.refreshData();
    }

    function onDateChange() {
      ctrl.showFilterSection = true;
      filterAll(ctrl.filter.searchText);
    }

    function translateCount() {
      return nvTranslate.instant('container.route-group.num-of-count-loaded', { total: ctrl.routeGroups.length });
    }

    function extend(routeGroups) {
      return _.map(routeGroups, (routeGroup) => {
        routeGroup._txnIdsString = routeGroup.transactionIds.join(',');
        routeGroup._rvnIdsString = routeGroup.reservationIds.join(',');
        routeGroup._createdAt = nvDateTimeUtils.displayDateTime(
          moment.utc(routeGroup.createdAt)
        );
        return routeGroup;
      });
    }

    function extendWithTxns(routeGroups) {
      return _.map(routeGroups, (routeGroup) => {
        routeGroup._txns = {};
        routeGroup._routedTxns = 0;
        _(routeGroup.transactionIds)
          .filter(txnId => !!txnById[txnId])
          .forEach((txnId) => {
            const txn = txnById[txnId];
            routeGroup._txns[txnId] = txn;
            routeGroup._routedTxns += (txn.routeId !== null ? 1 : 0);
          });
        routeGroup._txnsLength = _.size(routeGroup._txns);
        return routeGroup;
      });
    }

    function extendWithRvns(routeGroups) {
      return _.map(routeGroups, (routeGroup) => {
        routeGroup._rvns = {};
        routeGroup._routedRvns = 0;
        _(routeGroup.reservationIds)
          .filter(rvnId => !!rvnById[rvnId])
          .forEach((rvnId) => {
            const rvn = rvnById[rvnId];
            routeGroup._rvns[rvnId] = rvn;
            routeGroup._routedRvns += ((rvn.activeWaypointId !== null && rvn.activeRouteId !== null) ? 1 : 0);
          });
        routeGroup._rvnsLength = _.size(routeGroup._rvns);
        return routeGroup;
      });
    }

    function extendWithHubs(routeGroups) {
      return _.map(routeGroups, (routeGroup) => {
        const hub = hubByRouteGroupId[routeGroup.id];
        if (angular.isDefined(hub)) {
          routeGroup.hub = hubOptionsById[hub.hub_id] || null;
        } else {
          routeGroup.hub = null;
        }
        return routeGroup;
      });
    }

    function extendJobs(jobs) {
      return _(jobs)
        .sortBy('id')
        .map((job, idx) => {
          job._idx = idx + 1;
          return job;
        })
        .value();
    }

    function updateHubOfRouteGroup() {
      return Hub.getRouteGroupOfHubs().then((hubRouteGroups) => {
        hubByRouteGroupId = flipRouteGroupByHubId(hubRouteGroups);
        ctrl.routeGroups = extendWithHubs(ctrl.routeGroups);
      });
    }

    function getTxnJobs(txns) {
      return _.map(txns, txn => ({
        id: txn.id,
        orderId: txn.orderId,
        trackingId: txn.trackingId,
        type: getTxnType(txn),
        shipper: getTxnShipper(txn),
        address: getTxnAddress(txn),
        routeId: txn.routeId || '-',
        status: txn.granularStatus,
        sdt: nvDateTimeUtils.displayDateTime(moment(txn.startTime)),
        edt: nvDateTimeUtils.displayDateTime(moment(txn.endTime)),
        dp: getDistributionPoint(txn),
        comments: txn.comments || '-',
        priorityLevel: txn.priorityLevel,
        _typeId: 1, // transactions
      }));

      function getTxnType(txn) {
        if (txn.type === 'DELIVERY' && txn.transit === false) {
          return 'Delivery Transaction (DDNT)';
        }
        if (txn.type === 'DELIVERY' && txn.transit === true) {
          return 'Delivery Transaction (DDT)';
        }
        if (txn.type === 'PICKUP' && txn.transit === false) {
          return 'Pick Up Transaction (PPNT)';
        }
        if (txn.type === 'PICKUP' && txn.transit === true) {
          return 'Pick Up Transaction (PPT)';
        }
        return 'Transaction';
      }

      function getTxnShipper(txn) {
        return txn.fromName || '-';
      }

      function getTxnAddress(txn) {
        return _.compact([
          txn.address1,
          txn.address2,
          txn.city,
          txn.country,
          txn.postcode,
        ]).join(' ');
      }
    }

    function getRvnJobs(rvns) {
      return _.map(rvns, rvn => ({
        id: rvn.id,
        type: getRvnType(rvn),
        shipper: getRvnShipper(rvn),
        address: getRvnAddress(rvn),
        routeId: getRvnRouteId(rvn),
        status: rvn.status,
        sdt: nvDateTimeUtils.displayDateTime(moment(rvn.readyDatetime)),
        edt: nvDateTimeUtils.displayDateTime(moment(rvn.latestDatetime)),
        dp: getDistributionPoint(rvn),
        pickupSize: rvn.approxVolume,
        comments: rvn.comments || '-',
        priorityLevel: rvn.priorityLevel,
        _typeId: 2, // reservations
      }));

      function getRvnType(rvn) {
        return rvn.onDemand ? 'On-Demand Reservation' : 'Reservation';
      }

      function getRvnShipper(rvn) {
        return rvn.name || '';
      }

      function getRvnAddress(rvn) {
        const waypoint = rvn.activeWaypoint || {};
        return _.compact([
          waypoint.address1,
          waypoint.address2,
          waypoint.city,
          waypoint.country,
          waypoint.postcode,
        ]).join(' ');
      }

      function getRvnRouteId(rvn) {
        return (rvn.activeWaypoint && rvn.activeWaypoint.activeRoute && rvn.activeWaypoint.activeRoute.id) || '-';
      }
    }

    function getDistributionPoint(job) {
      const dp = dpById[job.distributionPointId];
      return dp ? `(${dp.id}) - ${dp.name}` : '-';
    }

    function initFilter(filter, routeGroups) {
      const date = getEarliestCreationDate(routeGroups);
      _.assign(filter, { minDate: date, frDate: date });

      function getEarliestCreationDate(theRouteGroups) {
        const theRouteGroup = _.minBy(theRouteGroups, routeGroup => routeGroup.createdAt);
        const createdAt = theRouteGroup && theRouteGroup.createdAt;
        return createdAt
          ? moment(createdAt).startOf('day').subtract(2, 'days').toDate() // buffer the min date by 2 days
          : new Date('2016-01-01');
      }
    }

    function listTransactionIds(routeGroups) {
      return _(routeGroups).flatMap(routeGroup => routeGroup.transactionIds).uniq().value();
    }

    function listReservationIds(routeGroups) {
      return _(routeGroups).flatMap(routeGroup => routeGroup.reservationIds).uniq().value();
    }

    function filterById(searchText, routeGroup) {
      return routeGroup.id.toString().indexOf(searchText) >= 0;
    }

    function filterByName(searchText, routeGroup) {
      return routeGroup.name.toLowerCase().indexOf(searchText) >= 0;
    }

    function filterByTxnId(searchText, routeGroup) {
      return routeGroup._txnIdsString.indexOf(searchText) >= 0;
    }

    function filterByRvnId(searchText, routeGroup) {
      return routeGroup._rvnIdsString.indexOf(searchText) >= 0;
    }

    function goToPage(stateName) {
      $mdDialog.cancel(); // make sure cancel dialog before go to new page
      $state.go(stateName);
    }

    function flipRouteGroupByHubId(hubRouteGroups) {
      // flip it to hub by routeGroupId
      return _(hubRouteGroups.hub_route_groups)
        .flatMap(hub => _.map(hub.route_group_ids, route_group_id => ({
          hub_id: hub.hub_id, route_group_id: route_group_id,
        })))
        .keyBy('route_group_id')
        .value();
    }

    function tableHack() {
      // todo refactor this!
      let head = false;
      let body = false;
      let t = null;
      $timeout(() => {
        $('.table-head').on('scroll', () => {
          if (body) {
            return;
          }
          $('.table-body').scrollLeft($(this).scrollLeft());
          $timeout.cancel(t);
          t = $timeout(stop, 250);
          head = true;
        });
        $('.table-body').on('scroll', () => {
          if (head) {
            return;
          }
          $('.table-head').scrollLeft($(this).scrollLeft());
          $timeout.cancel(t);
          t = $timeout(stop, 250);
          body = true;
        });
        function stop() {
          head = false;
          body = false;
        }
      }, 500);
    }
  }
}());
