(function controller() {
  angular
    .module('nvOperatorApp.controllers')
    .controller('RouteGroupBulkDeleteDialogController', RouteGroupBulkDeleteDialogController);

  RouteGroupBulkDeleteDialogController.$inject = [
    '$mdDialog', 'routeGroupsData', 'nvDialog', 'nvToast', 'nvTranslate',
    'RouteGroup',
  ];

  function RouteGroupBulkDeleteDialogController(
    $mdDialog, routeGroupsData, nvDialog, nvToast, nvTranslate,
    RouteGroup
  ) {
    // variables
    const PASSWORD = '1234567890';
    const ctrl = this;

    ctrl.formPassword = null;
    ctrl.formSubmitState = 'idle';
    ctrl.routeGroups = routeGroupsData;

    // functions
    ctrl.onCancel = onCancel;
    ctrl.onSave = onSave;

    // functions details
    function onSave($event) {
      const routeGroupsToProcess = _.cloneDeep(routeGroupsData);
      const bulkActionProgressPayload = {
        totalCount: routeGroupsToProcess.length,
        currentIndex: 0,
        errors: [],
      };
      const deletedRouteGroups = [];
      const failedDeletedRouteGroups = [];

      if (PASSWORD === ctrl.formPassword) {
        nvDialog.showSingle($event, {
          templateUrl: 'lib/ninja-commons/services/dialogs/bulk-action-progress/bulk-action-progress.dialog.html',
          cssClass: 'bulk-action-progress',
          controller: 'BulkActionProgressDialogController',
          controllerAs: 'ctrl',
          skipHide: true,
          clickOutsideToClose: false,
          locals: {
            payload: bulkActionProgressPayload,
          },
        }).then(progressDialogClosed);

        startMultiSelectDeletion(routeGroupsToProcess);
      } else {
        nvToast.error(nvTranslate.instant('container.order.edit.invalid-password'));
      }

      function progressDialogClosed() {
        if (getSuccessCount() > 0) {
          nvToast.info(
            nvTranslate.instant('container.route-group.num-route-group-deleted', {
              num: getSuccessCount(),
            })
          );
        }

        ctrl.formSubmitState = 'idle';
        $mdDialog.hide({
          success: deletedRouteGroups,
          failed: failedDeletedRouteGroups,
        });
      }

      function getSuccessCount() {
        return bulkActionProgressPayload.totalCount - bulkActionProgressPayload.errors.length;
      }

      function startMultiSelectDeletion(routeGroups) {
        let routeGroupToProcess;
        if (routeGroups.length > 0) {
          bulkActionProgressPayload.currentIndex += 1;
          routeGroupToProcess = routeGroups.splice(0, 1)[0];

          ctrl.formSubmitState = 'waiting';
          RouteGroup.delete(routeGroupToProcess.id).then(multiSelectSuccess, multiSelectFailure);
        } else {
          ctrl.formSubmitState = 'idle';
        }

        function multiSelectSuccess() {
          deletedRouteGroups.push(routeGroupToProcess);
          bulkActionProgressPayload.successCount += 1;

          if (routeGroups.length > 0) {
            startMultiSelectDeletion(routeGroups); // recursive
          } else {
            ctrl.formSubmitState = 'idle';
          }
        }

        function multiSelectFailure() {
          bulkActionProgressPayload.errors.push(routeGroupToProcess.name);

          if (routeGroups.length > 0) {
            startMultiSelectDeletion(routeGroups); // recursive
          } else {
            ctrl.formSubmitState = 'idle';
          }
        }
      }
    }

    function onCancel() {
      $mdDialog.cancel();
    }
  }
}());
