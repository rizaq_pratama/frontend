(function controller() {
  angular
    .module('nvOperatorApp.controllers')
    .controller('ReservationPresetManagementOverviewController', ReservationPresetManagementOverviewController);

  ReservationPresetManagementOverviewController.$inject = [
    '$scope', '$timeout', 'nvTable', 'ReservationPreset', '$state',
    'nvDialog', 'nvToast', 'nvTranslate', '$q', 'nvDateTimeUtils',
    '$stateParams',
  ];

  function ReservationPresetManagementOverviewController(
    $scope, $timeout, nvTable, ReservationPreset, $state,
    nvDialog, nvToast, nvTranslate, $q, nvDateTimeUtils,
    $stateParams
  ) {
    const GROUP_VALIDATION_ACTIONS = {
      CREATE_ROUTE: 'container.reservation-preset-management.create-route',
      ROUTE_PENDING_RESERVATIONS: 'container.reservation-preset-management.route-pending-reservations',
    };

    const ctrl = this;
    const parentCtrl = $scope.$parent.ctrl;

    ctrl.parentCtrl = parentCtrl;
    ctrl.groupsTableParam = nvTable.createTable(ReservationPreset.FIELDS);

    ctrl.date = moment(parentCtrl.routeDate).toDate();
    ctrl.minDate = new Date();
    ctrl.maxDate = moment(ctrl.minDate).add(14, 'days').toDate();

    // functions
    ctrl.showGroupFormDialog = showGroupFormDialog;
    ctrl.getSelectedCount = getSelectedCount;
    ctrl.bulkCreateRoutes = bulkCreateRoutes;
    ctrl.bulkEditGroups = bulkEditGroups;
    ctrl.bulkRoutePendingReservations = bulkRoutePendingReservations;
    ctrl.deleteGroup = deleteGroup;
    ctrl.navigate = navigate;

    // start
    init();

    // functions details
    function init() {
      $scope.$watch('ctrl.date', (newVal) => {
        const date = moment(newVal).format(nvDateTimeUtils.FORMAT_DATE);
        if (date !== parentCtrl.routeDate) {
          parentCtrl.routeDate = date;
          routeDateOnChanged();
        }
      });

      parentCtrl.contentLoading = true;

      const promises = [];
      if ($stateParams.refresh !== 'true' && parentCtrl.groupsResult) {
        promises.push($q.when(parentCtrl.groupsResult));
      } else if (parentCtrl.IS_DEBUG) {
        promises.push(
          $.getJSON('views/container/reservation-preset-management/json/groups.json').then(response =>
            response.data
          )
        );
      } else {
        promises.push(
          ReservationPreset.getGroups({
            date: parentCtrl.routeDate.replace(/[^\d]/g, ''),
          })
        );
      }

      if ($stateParams.refresh === 'true') {
        parentCtrl.refreshPendingTasks();
      }

      $q.all(promises).then(success);

      function success(response) {
        $timeout(() => {
          parentCtrl.contentLoading = false;
        });

        parentCtrl.groupsResult = response[0];

        ctrl.groupsTableParam.addColumn('c_driverName', {
          displayName: 'commons.driver',
        });
        ctrl.groupsTableParam.addColumn('c_hubName', {
          displayName: 'commons.hub',
        });
        ctrl.groupsTableParam.addColumn('c_addressCount', {
          displayName: 'container.reservation-preset-management.no-of-pickup-locations',
        });
        ctrl.groupsTableParam.setHighlightFn(highlightFn);
        ctrl.groupsTableParam.setData(extendGroups(parentCtrl.groupsResult));

        function highlightFn(group) {
          switch (group.status) {
            case ReservationPreset.STATUS.PENDING:
              return {
                class: 'pending',
                highlighted: true,
              };
            case ReservationPreset.STATUS.SUCCESS:
              return {
                class: 'success',
                highlighted: true,
              };
            default:
              return {
                class: '',
                highlighted: false,
              };
          }
        }
      }
    }

    function routeDateOnChanged() {
      $timeout(() => {
        parentCtrl.backdropLoading = true;
        if (parentCtrl.IS_DEBUG) {
          $.getJSON('views/container/reservation-preset-management/json/groups.json', (groups) => {
            $timeout(() => {
              success(groups.data);
            });
          });
        } else {
          ReservationPreset.getGroups({
            date: parentCtrl.routeDate.replace(/[^\d]/g, ''),
          }).then(success);
        }
      });

      function success(response) {
        parentCtrl.backdropLoading = false;

        parentCtrl.groupsResult = response;
        ctrl.groupsTableParam.deselect();
        ctrl.groupsTableParam.setData(extendGroups(parentCtrl.groupsResult));
        ctrl.groupsTableParam.refreshData();
      }
    }

    function showGroupFormDialog($event, groupData = null) {
      nvDialog.showSingle($event, {
        templateUrl: 'views/container/reservation-preset-management/dialog/grouping-form/grouping-form.dialog.html',
        cssClass: 'reservation-preset-grouping-form',
        controller: 'ReservationPresetGroupingFormDialogController',
        controllerAs: 'ctrl',
        locals: {
          groupData: groupData,
          resultData: {
            driversResult: parentCtrl.driversResult,
            hubsResult: parentCtrl.hubsResult,
          },
        },
      }).then(onSet);

      function onSet(response) {
        if (groupData === null) {
          // create mode
          parentCtrl.groupsResult.push(response);
        }

        ctrl.groupsTableParam.mergeIn(
          extendGroups(_.castArray(response)), 'id'
        );
      }
    }

    function getSelectedCount() {
      return _.size(ctrl.groupsTableParam.getSelection());
    }

    function bulkCreateRoutes($event, groups) {
      const groupsValidationErrorData = getGroupsValidationErrorData(
        groups, GROUP_VALIDATION_ACTIONS.CREATE_ROUTE
      );
      if (groupsValidationErrorData.errors.length > 0) {
        showSelectionErrorDialog(groups, groupsValidationErrorData, $event, bulkCreateRoutes);
        return;
      }

      nvDialog.showSingle($event, {
        templateUrl: 'views/container/reservation-preset-management/dialog/create-route/create-route.dialog.html',
        cssClass: 'reservation-preset-create-route',
        controller: 'ReservationPresetCreateRouteDialogController',
        controllerAs: 'ctrl',
        locals: {
          routeDateData: parentCtrl.routeDate,
          groupsData: groups,
        },
      }).then(onCreated);

      function onCreated() {
        _.forEach(groups, (group) => {
          group.status = ReservationPreset.STATUS.PENDING;
        });
      }
    }

    function bulkEditGroups(groups) {
      if (_.size(groups) > 4) {
        nvToast.error(
          nvTranslate.instant('container.reservation-preset-management.please-select-maximum-4-groups')
        );
        return;
      }

      $state.go('container.reservation-preset-management.edit-groupings', {
        ids: _.map(groups, 'id'),
      });
    }

    function bulkRoutePendingReservations($event, groups) {
      const groupsValidationErrorData = getGroupsValidationErrorData(
        groups, GROUP_VALIDATION_ACTIONS.ROUTE_PENDING_RESERVATIONS
      );
      if (groupsValidationErrorData.errors.length > 0) {
        showSelectionErrorDialog(
          groups, groupsValidationErrorData, $event, bulkRoutePendingReservations
        );
        return;
      }

      nvDialog.showSingle($event, {
        templateUrl: 'views/container/reservation-preset-management/dialog/route-pending-reservations/route-pending-reservations.dialog.html',
        cssClass: 'reservation-preset-route-pending-reservations',
        controller: 'ReservationPresetRoutePendingReservationsDialogController',
        controllerAs: 'ctrl',
        locals: {
          routeDateData: parentCtrl.routeDate,
          groupsData: groups,
        },
      });
    }

    function deleteGroup($event, group) {
      nvDialog.confirmDelete($event, {
        title: nvTranslate.instant('container.reservation-preset-management.delete-group'),
        content: nvTranslate.instant('commons.confirm-delete-x', {
          name: group.name,
        }),
      }).then(onDelete);

      function onDelete() {
        parentCtrl.backdropLoading = true;
        if (!parentCtrl.IS_DEBUG) {
          ReservationPreset.deleteGroup(group.id).then(success)
            .finally(finallyFn);
        } else {
          success();
          finallyFn();
        }
      }

      function success() {
        nvToast.success(
          nvTranslate.instant('container.reservation-preset-management.group-deleted')
        );

        _.remove(parentCtrl.groupsResult, ['id', group.id]);
        ctrl.groupsTableParam.remove(group, 'id');

        parentCtrl.refreshPendingTasks();
      }

      function finallyFn() {
        parentCtrl.backdropLoading = false;
      }
    }

    function navigate(stateName, stateParams = null) {
      $state.go(stateName, stateParams);
    }

    function getGroupsValidationErrorData(groups, action) {
      const groupsValidationErrorData = {
        action: action,
        errors: [],
      };

      _.forEach(groups, (group) => {
        if (action === GROUP_VALIDATION_ACTIONS.CREATE_ROUTE) {
          if (_.includes([
            ReservationPreset.STATUS.PENDING,
            ReservationPreset.STATUS.SUCCESS,
          ], group.status)) {
            groupsValidationErrorData.errors.push(
              generateGroupValidationErrorData(group, 'container.reservation-preset-management.invalid-status-to-create-route')
            );
          }
        } else if (action === GROUP_VALIDATION_ACTIONS.ROUTE_PENDING_RESERVATIONS) {
          if (!_.includes([
            ReservationPreset.STATUS.SUCCESS,
          ], group.status)) {
            groupsValidationErrorData.errors.push(
              generateGroupValidationErrorData(group, 'container.reservation-preset-management.invalid-status-to-route-pending-reservations')
            );
          }
        }
      });

      return groupsValidationErrorData;

      function generateGroupValidationErrorData(group, errorMessage) {
        return {
          id: group.id,
          name: group.name,
          errorMessage: errorMessage,
        };
      }
    }

    function showSelectionErrorDialog(groups, groupsValidationErrorData, $event, fn) {
      nvDialog.showSingle($event, {
        templateUrl: 'views/container/reservation-preset-management/dialog/selection-error/selection-error.dialog.html',
        theme: 'nvRed',
        cssClass: 'reservation-preset-selection-error',
        controller: 'ReservationPresetSelectionErrorDialogController',
        controllerAs: 'ctrl',
        locals: {
          groupsValidationErrorData: groupsValidationErrorData,
        },
      }).then(success);

      function success() {
        const validGroups = _.differenceBy(groups, groupsValidationErrorData.errors, 'id');

        ctrl.groupsTableParam.deselect();
        _.forEach(validGroups, (group) => {
          const theGroup = _.find(ctrl.groupsTableParam.data, ['id', group.id]);

          if (theGroup) {
            ctrl.groupsTableParam.select(theGroup);
          }
        });

        if (validGroups.length <= 0) {
          nvToast.error(
            nvTranslate.instant('commons.unable-to-apply-action'),
            nvTranslate.instant('commons.no-valid-selection')
          );
        } else {
          fn($event, validGroups);
        }
      }
    }

    function extendGroups(groups) {
      return _.map(groups, group => (
        setCustomGroupData(group)
      ));

      function setCustomGroupData(group) {
        group.c_driverName = getDriverName(group.driver_id);
        group.c_hubName = _.get(parentCtrl.getHub(group.hub_id), 'name') || null;
        group.c_addressCount = _.size(group.sams_ids);

        return group;

        function getDriverName(driverId) {
          const driver = parentCtrl.getDriver(driverId);
          if (driver) {
            return _.join(_.compact([
              driver.firstName,
              driver.lastName,
            ]), ' ');
          }

          return null;
        }
      }
    }
  }
}());
