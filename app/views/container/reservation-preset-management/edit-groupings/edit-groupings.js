(function controller() {
  angular
    .module('nvOperatorApp.controllers')
    .controller('ReservationPresetManagementEditGroupingsController', ReservationPresetManagementEditGroupingsController);

  ReservationPresetManagementEditGroupingsController.$inject = [
    '$scope', '$timeout', '$stateParams', 'nvDialog', 'nvTranslate', '$q', '$state', 'ReservationPreset',
    'Shippers', 'nvExtendUtils',
  ];

  function ReservationPresetManagementEditGroupingsController(
    $scope, $timeout, $stateParams, nvDialog, nvTranslate, $q, $state, ReservationPreset,
    Shippers, nvExtendUtils
  ) {
    const IDS = _.isArray($stateParams.ids) ?
      _.map($stateParams.ids, _.toInteger) :
      _.castArray(_.toInteger($stateParams.ids));

    const ctrl = this;
    const parentCtrl = $scope.$parent.ctrl;
    ctrl.showNotif = false;
    ctrl.parentCtrl = parentCtrl;
    ctrl.data = {
      hubs: [],
      drivers: [],
      groups: [],
    };
    ctrl.searchTextGrp = {};
    ctrl.shipperMap = {};
    ctrl.openMoveShipperDialog = openMoveShipperDialog;
    ctrl.confirmGrouping = confirmGrouping;
    ctrl.getShipper = getShipper;
    ctrl.deleteSamData = deleteSamData;
    ctrl.isAbleToMoveGroup = isAbleToMoveGroup;
    // functions
    // start
    init();

    // functions details
    function init() {
      // redirect if no group id
      if (!$stateParams.ids) {
       return $state.go('container.reservation-preset-management.overview', { refresh: true });
      }

      parentCtrl.contentLoading = true;
      return fetchGroups().then(getMilkrunSettings);

      function getMilkrunSettings() {
        const promises = [];
        _.forEach(IDS, (id) => {
          const group = parentCtrl.getGroupById(id);
          promises.push(parentCtrl.getMilkrunSettingsByPromise(group ? group.sams_ids : []));
          ctrl.data.groups.push(group);
        });
        return $q.all(promises).then(readResponseAndGetShipperDetails);
      }

      function readResponseAndGetShipperDetails(results) {
        const shipperIds = [];
        _.forEach(results, (value, key) => {
          ctrl.data.groups[key].sams_data = value;
          _.forEach(value, (samData) => {
            shipperIds.push(_.get(extendSamsData(samData), 'address.shipper_id'));
          });
        });
        return Shippers.searchByIds(_.uniq(shipperIds), Shippers.SEARCH_BY.global_id)
          .then(success);

        function extendSamsData(sams) {
          nvExtendUtils.addDisplayAddress(sams.address);
          return sams;
        }
      }

      function success(shipperData) {
        ctrl.shipperMap = _.keyBy(shipperData, 'global_id');
        // add to sams_data
        _.forEach(ctrl.data.groups, (group) => {
          _.forEach(group.sams_data, (samData) => {
            const shipper = ctrl.shipperMap[_.get(samData, 'address.shipper_id')];
            samData.address.shipperName = _.get(shipper, 'name', '-');
          });
        });
        ctrl.data.hubs = parentCtrl.getHubOptions();
        ctrl.data.drivers = parentCtrl.getDriverOptions();
        parentCtrl.contentLoading = false;
      }
    }

    function getShipper(id) {
      return ctrl.shipperMap[id] || {};
    }

    function openMoveShipperDialog($event, group) {
      // get the groups except the passed groupIdx
      const groups = _.differenceBy(ctrl.data.groups, [group], 'id');
      // get the selected shipper in the respective groups
      const samsData = _.filter(group.sams_data, sam => (sam.selected));
      return nvDialog.showSingle($event, {
        templateUrl: 'views/container/reservation-preset-management/dialog/move-shipper-form/move-shipper-form.dialog.html',
        cssClass: 'reservation-preset-move-shipper-form',
        controller: 'ReservationPresetMoveShipperFormDialogController',
        controllerAs: 'ctrl',
        locals: {
          data: {
            groups: groups,
            samsData: samsData,
            shipperMap: ctrl.shipperMap,
          },
        },
      }).then(onFinishDialog);

      function onFinishDialog(groupDestinationid) {
        // pull from source group
        _.pullAllBy(group.sams_data, samsData, 'id');
        updateSamsIds(group);
        // deselect the samsData
        _.forEach(samsData, (sam) => {
          sam.selected = false;
        });
        // push to the destination group
        const selectedGroup = _.find(ctrl.data.groups, { id: groupDestinationid });
        if (selectedGroup) {
          selectedGroup.sams_data = _.union(selectedGroup.sams_data, samsData);
          updateSamsIds(selectedGroup);
        }
        // show the notif
        const shipperNames = _(samsData)
                            .map(sam => (`${sam.address.shipperName} (${sam.address.address1})`))
                            .join(', ');
        ctrl.message = nvTranslate.instant('container.reservation-preset-management.x-has-been-assigned-to-y', { x: shipperNames, y: selectedGroup.name });
        ctrl.showAlert = true;
        $timeout(() => {
          ctrl.showAlert = false;
        }, 2000);
      }
    }
    function updateSamsIds(group) {
      group.sams_ids = _.map(group.sams_data, d => d.id);
    }

    function deleteSamData($event, group) {
      return nvDialog.confirmDelete($event, {}).then(onConfirmDelete);

      function onConfirmDelete() {
        group.sams_data = _.filter(group.sams_data, (sam) => (!sam.selected));
        updateSamsIds(group);
      }
    }
    function fetchGroups() {
      if (parentCtrl.groupsResult != null && parentCtrl.groupsResult.length > 0) {
        return $q.resolve(parentCtrl.groupsResult);
      }

      if (parentCtrl.IS_DEBUG) {
        return $.getJSON('views/container/reservation-preset-management/json/groups.json').then((response) => {
          parentCtrl.groupsResult = response.data;
          return response.data;
        });
      }
      return ReservationPreset.getGroups({
        date: parentCtrl.routeDate.replace(/[^\d]/g, ''),
      }).then(success);

      function success(response) {
        parentCtrl.groupsResult = response;
        return $q.resolve(response);
      }
    }

    function confirmGrouping() {
      parentCtrl.contentLoading = true;
      const payload = _.map(ctrl.data.groups, (group) => {
        return _.pick(group, ['id', 'driver_id', 'hub_id', 'name', 'sams_ids']);
      });
      if (parentCtrl.IS_DEBUG) {
        return $q.resolve(payload).then(onSuccess, onFailed);
      }
      return ReservationPreset.updateGroups(payload).then(onSuccess, onFailed);

      function onSuccess() {
        parentCtrl.contentLoading = true;
        $state.go('container.reservation-preset-management.overview', { refresh: true });
      }

      function onFailed() {
        parentCtrl.contentLoading = false;
      }
    }


    function isAbleToMoveGroup(group) {
      const selectedSam = _.find(group.sams_data, { selected: true });
      if (selectedSam) {
        return true;
      }
      return false;
    }
  }
}());
