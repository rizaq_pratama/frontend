(function controller() {
  angular
    .module('nvOperatorApp.controllers')
    .controller('ReservationPresetManagementPendingController', ReservationPresetManagementPendingController);

  ReservationPresetManagementPendingController.$inject = [
    '$scope', 'nvAddress', 'nvDialog', '$q', 'ReservationPreset',
    '$timeout', '$stateParams', 'Shippers',
  ];

  function ReservationPresetManagementPendingController(
    $scope, nvAddress, nvDialog, $q, ReservationPreset,
    $timeout, $stateParams, Shippers
  ) {
    const ctrl = this;
    const parentCtrl = $scope.$parent.ctrl;

    ctrl.parentCtrl = parentCtrl;
    ctrl.pendingTasks = null;

    // functions
    ctrl.showUnlinkOrAssignDialog = showUnlinkOrAssignDialog;

    // start
    init();

    // functions details
    function init() {
      parentCtrl.contentLoading = true;

      if ($stateParams.refresh === 'true') {
        parentCtrl.refreshPendingTasks().then(refreshSuccess);
      } else {
        refreshSuccess();
      }

      function refreshSuccess() {
        ctrl.pendingTasks = _.cloneDeep(ctrl.parentCtrl.pendingTasksResult);

        const promises = [
          Shippers.searchByIds(_.map(ctrl.pendingTasks, 'shipper_id'), Shippers.SEARCH_BY.global_id),
          parentCtrl.getMilkrunSettingsByPromise(_.map(ctrl.pendingTasks, 'sams_id')),
        ];

        if (parentCtrl.groupsResult === null) {
          if (parentCtrl.IS_DEBUG) {
            promises.push(
              $.getJSON('views/container/reservation-preset-management/json/groups.json').then(response =>
                  response.data
              )
            );
          } else {
            promises.push(ReservationPreset.getGroups({
              date: parentCtrl.routeDate.replace(/[^\d]/g, ''),
            }));
          }
        }

        $q.all(promises).then(success);
      }

      function success(response) {
        $timeout(() => {
          parentCtrl.contentLoading = false;
        });

        if (!_.isUndefined(response[2])) {
          parentCtrl.groupsResult = response[2];
        }

        _.forEach(ctrl.pendingTasks, (pendingTask) => {
          pendingTask.shipper = findShipper(pendingTask.shipper_id);
          pendingTask.milkrunSetting = findMilkrunSetting(pendingTask.sams_id);

          pendingTask.fullAddress = nvAddress.extract(
            _.get(pendingTask.milkrunSetting, 'address') || {}
          ) || null;

          if (!pendingTask.link) {
            pendingTask.hubName = findHubName(pendingTask.sams_id);
          }
        });

        function findShipper(globalId) {
          return _.find(response[0], ['global_id', globalId]) || null;
        }

        function findMilkrunSetting(samsId) {
          return _.find(response[1], ['id', samsId]) || null;
        }

        function findHubName(samsId) {
          const hubNames = [];
          _.forEach(parentCtrl.groupsResult, (group) => {
            if (_.includes(group.sams_ids, samsId)) {
              hubNames.push(
                _.get(parentCtrl.getHub(group.hub_id), 'name')
              );
            }
          });

          return _.join(_.compact(_.uniq(hubNames)), ', ') || '-';
        }
      }
    }

    function showUnlinkOrAssignDialog($event, pendingTask) {
      pendingTask.selected = true;

      nvDialog.showSingle($event, {
        templateUrl: 'views/container/reservation-preset-management/dialog/unlink-or-assign/unlink-or-assign.dialog.html',
        cssClass: 'reservation-preset-unlink-or-assign',
        controller: 'ReservationPresetUnlinkOrAssignDialogController',
        controllerAs: 'ctrl',
        locals: {
          IS_DEBUG: parentCtrl.IS_DEBUG,
          pendingTaskData: pendingTask,
          resultData: {
            groupsResult: parentCtrl.groupsResult,
          },
        },
      }).then(onSet).finally(finallyFn);

      function onSet(response) {
        const groups = parentCtrl.groupsResult;

        if (response.groupId) {
          // assign
          const group = _.find(groups, ['id', response.groupId]);

          if (!group.sams_ids) {
            group.sams_ids = [];
          }

          _(group.sams_ids).unshift(pendingTask.sams_id).value();
        } else {
          // unlink
          _.forEach(groups, (group) => {
            if (_.includes(group.sams_ids, pendingTask.sams_id)) {
              _.pull(group.sams_ids, pendingTask.sams_id);
            }
          });
        }

        // child to process remove
        _.remove(ctrl.pendingTasks, ['id', pendingTask.id]);

        // global to process remove
        _.remove(ctrl.parentCtrl.pendingTasksResult, ['id', pendingTask.id]);
      }

      function finallyFn() {
        pendingTask.selected = false;
      }
    }
  }
}());
