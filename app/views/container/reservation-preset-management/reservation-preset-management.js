(function controller() {
  angular
    .module('nvOperatorApp.controllers')
    .controller('ReservationPresetManagementController', ReservationPresetManagementController);

  ReservationPresetManagementController.$inject = [
    '$q', 'ReservationPreset', '$timeout', '$state', 'Driver',
    'Hub', 'nvDateTimeUtils', 'Address',
  ];

  function ReservationPresetManagementController(
    $q, ReservationPreset, $timeout, $state, Driver,
    Hub, nvDateTimeUtils, Address
  ) {
    // variables
    let DRIVERS_MAP;
    let HUBS_MAP;
    const MILKRUN_SETTINGS_MAP = {};
    const RESERVATION_DAYS = [
      { id: 2, name: 'M' },
      { id: 3, name: 'T' },
      { id: 4, name: 'W' },
      { id: 5, name: 'T' },
      { id: 6, name: 'F' },
      { id: 7, name: 'S' },
      { id: 1, name: 'S' },
    ];

    const ctrl = this;
    ctrl.loading = true;

    // shared params
    ctrl.IS_DEBUG = false;
    ctrl.RESERVATION_DAYS = RESERVATION_DAYS;
    ctrl.backdropLoading = false;
    ctrl.contentLoading = false; // child controller content loading

    ctrl.routeDate = moment().format(nvDateTimeUtils.FORMAT_DATE);

    ctrl.driversResult = null;
    ctrl.hubsResult = null;
    ctrl.pendingTasksResult = null;
    ctrl.groupsResult = null;

    // shared functions
    ctrl.getHub = getHub;
    ctrl.getDriver = getDriver;
    ctrl.getHubOptions = getHubOptions;
    ctrl.getDriverOptions = getDriverOptions;
    ctrl.getMilkrunSettingsByPromise = getMilkrunSettingsByPromise;
    ctrl.contains = contains;
    ctrl.getGroupById = getGroupById;
    ctrl.refreshPendingTasks = refreshPendingTasks;

    // functions
    ctrl.getAll = getAll;
    ctrl.switchTab = switchTab;
    ctrl.isActive = isActive;

    // functions details
    function getAll() {
      if (ctrl.IS_DEBUG) {
        return $.getJSON('views/container/reservation-preset-management/json/pending-tasks.json', (pendingTasks) => {
          $.getJSON('views/container/reservation-preset-management/json/drivers.json', (drivers) => {
            $.getJSON('views/container/reservation-preset-management/json/hubs.json', (hubs) => {
              $timeout(() => {
                $q.when(success([pendingTasks.data, drivers.data, hubs])).finally(finallyFn);
              });
            });
          });
        });
      }

      return $q.all([
        ReservationPreset.getPendingTasks(),
        Driver.searchAll(),
        Hub.read(),
      ]).then(success).finally(finallyFn);

      function success(response) {
        ctrl.pendingTasksResult = response[0];
        ctrl.driversResult = response[1];
        ctrl.hubsResult = response[2];

        DRIVERS_MAP = _.keyBy(_.get(ctrl.driversResult, 'drivers'), 'id');
        HUBS_MAP = _.keyBy(ctrl.hubsResult, 'id');
      }

      function finallyFn() {
        ctrl.loading = false;
      }
    }

    function refreshPendingTasks() {
      return ReservationPreset.getPendingTasks().then(success);

      function success(response) {
        ctrl.pendingTasksResult = response;
      }
    }

    function switchTab(stateName) {
      if ($state.current.name !== stateName) {
        if (stateName === 'container.reservation-preset-management.pending') {
          $state.go(stateName, { refresh: true });
        } else {
          $state.go(stateName);
        }
      }
    }

    function isActive(stateName) {
      switch (stateName) {
        case 'container.reservation-preset-management.overview':
          return _.includes([
            stateName,
            'container.reservation-preset-management.edit-groupings',
            'container.reservation-preset-management.edit-pickup-sequence',
          ], $state.current.name);
        case 'container.reservation-preset-management.pending':
          return $state.current.name === stateName;
        default:
          return false;
      }
    }

    function getHub(hubId) {
      return HUBS_MAP[hubId] || null;
    }

    function getDriver(driverId) {
      return DRIVERS_MAP[driverId] || null;
    }

    function getHubOptions() {
      return Hub.toOptions(ctrl.hubsResult);
    }

    function getDriverOptions() {
      return Driver.toOptions(ctrl.driversResult);
    }

    function getGroupById(id) {
      return _.find(ctrl.groupsResult, { id: id });
    }

    function getMilkrunSettingsByPromise(samsIds) {
      const resultMilkrunSettings = [];
      const samsIdsToSearch = [];
      _.forEach(samsIds, (samsId) => {
        if (MILKRUN_SETTINGS_MAP[samsId]) {
          resultMilkrunSettings.push(MILKRUN_SETTINGS_MAP[samsId]);
        } else {
          samsIdsToSearch.push(samsId);
        }
      });

      if (_.size(samsIdsToSearch) <= 0) {
        return $q.when(resultMilkrunSettings);
      }

      const deferred = $q.defer();
      if (ctrl.IS_DEBUG) {
        $.getJSON('views/container/reservation-preset-management/json/sams.json', (sams) => {
          $timeout(() => {
            success(sams);
          });
        });
      } else {
        Address.readMilkrunSettingsByIds(samsIdsToSearch).then(success, failure);
      }

      return deferred.promise;

      function success(milkrunSettings) {
        const notFound = [];

        _.forEach(samsIdsToSearch, (samsId) => {
          const theMilkrunSetting = _.find(milkrunSettings, ['id', samsId]);

          if (theMilkrunSetting) {
            MILKRUN_SETTINGS_MAP[theMilkrunSetting.id] = theMilkrunSetting;
            resultMilkrunSettings.push(theMilkrunSetting);
          } else {
            notFound.push(samsId);
          }

          return _.noop();
        });

        deferred.resolve(resultMilkrunSettings);
      }

      function failure(response) {
        deferred.reject(response);
      }
    }

    function contains(theArray, value) {
      return _.includes(theArray, value);
    }
  }
}());
