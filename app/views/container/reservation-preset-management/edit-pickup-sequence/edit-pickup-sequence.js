(function controller() {
  angular
    .module('nvOperatorApp.controllers')
    .controller('ReservationPresetManagementEditPickupSequenceController', ReservationPresetManagementEditPickupSequenceController);

  ReservationPresetManagementEditPickupSequenceController.$inject = [
    '$scope', '$timeout', '$stateParams', 'ReservationPreset', '$q',
    'nvExtendUtils', '$state', 'nvToast', 'nvTranslate', 'Shippers',
  ];

  const API_STATE = {
    IDLE: 'idle',
    WAITING: 'waiting',
  };

  function ReservationPresetManagementEditPickupSequenceController(
    $scope, $timeout, $stateParams, ReservationPreset, $q,
    nvExtendUtils, $state, nvToast, nvTranslate, Shippers
  ) {
    const ID = $stateParams.id || 0;

    const ctrl = this;
    const parentCtrl = $scope.$parent.ctrl;

    ctrl.parentCtrl = parentCtrl;

    ctrl.groupData = null;
    ctrl.source = [];
    ctrl.list = _.cloneDeep(ctrl.source);

    // functions
    ctrl.searchText = '';
    ctrl.filterState = false;
    ctrl.onSearchChange = onSearchChange;
    ctrl.onDrop = onDrop;
    ctrl.onConfirmSequence = onConfirmSequence;
    ctrl.selected = null;
    ctrl.confirmState = API_STATE.IDLE;

    // start
    init();

    // functions details
    function init() {
      parentCtrl.contentLoading = true;

      if (parentCtrl.IS_DEBUG) {
        $.getJSON('views/container/reservation-preset-management/json/sams.json', (samsDatas) => {
          $.getJSON('views/container/reservation-preset-management/json/groups.json', (groups) => {
            $timeout(() => {
              ctrl.groupData = parentCtrl.groupsResult || groups.data;
              ctrl.source = _(samsDatas)
                .map((sams, idx) => {
                  nvExtendUtils.addDisplayAddress(sams.address);
                  sams.sequence = idx + 1;
                  return sams;
                })
                .sortBy('sequence')
                .value();
              ctrl.list = _.cloneDeep(ctrl.source);
              parentCtrl.contentLoading = false;
            });
          });
        });
      } else {
        // group promise can be direct result from parent or api call
        let groupPromise = null;
        if (_.size(parentCtrl.groupsResult) > 0) {
          const group = _.find(parentCtrl.groupsResult, g => g.id === ID);
          if (!group) {
            onFail(nvTranslate.instant('container.reservation-preset-management.group-id-not-found', { id: ID }));
          } else {
            groupPromise = $q.resolve(_.castArray(group));
          }
        } else {
          // api call
          groupPromise = ReservationPreset.getGroups({ id: ID });
        }

        groupPromise
          .then((groupResponse) => {
            // get first element (this page only use single group id)
            const groupData = _.head(groupResponse);
            const driver = parentCtrl.getDriver(groupData.driver_id);
            _.assign(groupData, {
              hub_name: _.get(parentCtrl.getHub(groupData.hub_id), 'name'),
              driver_name: `${_.get(driver, 'firstName', 'N/A')} ${_.get(driver, 'lastName', '')}`,
            });

            ctrl.groupData = _.cloneDeep(groupData);
            readSams(groupData.sams_ids);
          }, onFail);
      }


      function onFail(message) {
        if (message) {
          nvToast.error(message);
        }
        redirectToOverViewPage();
      }

      function readSams(samsIds) {
        if (_.size(samsIds) > 0) {
          parentCtrl.getMilkrunSettingsByPromise(_.castArray(samsIds))
            .then(onSuccess, onFail);
        } else {
          onSuccess([]);
        }

        function onSuccess(samsDatas) {
          const shipperIds = [];

          ctrl.source = _(samsIds)
            .map((samsId, idx) => {
              const sams = _.find(samsDatas, s => s.id === samsId);
              if (!sams) {
                // return dummy sams, mark as deleted
                return {
                  id: samsId,
                  is_deleted: true,
                  address: { _address: nvTranslate.instant('container.reservation-preset-management.address-not-available') },
                  sequence: idx + 1,
                };
              }

              shipperIds.push(_.get(sams, 'address.shipper_id'));
              nvExtendUtils.addDisplayAddress(sams.address);
              sams.sequence = idx + 1;
              return sams;
            })
            .sortBy('sequence')
            .value();

          Shippers.searchByIds(_.uniq(shipperIds), Shippers.SEARCH_BY.global_id)
            .then(searchShippersSuccess);
        }

        function searchShippersSuccess(shipperData) {
          ctrl.shipperMap = _.keyBy(shipperData, 'global_id');

          // add to sams_data
          _.forEach(ctrl.source, (sams) => {
            const shipper = ctrl.shipperMap[_.get(sams, 'address.shipper_id')];
            sams.shipperName = _.get(shipper, 'name', '-');
            sams.address._address = `${sams.shipperName} ${sams.address._address}`;
          });

          ctrl.list = _.cloneDeep(ctrl.source);
          parentCtrl.contentLoading = false;
        }
      }
    }

    function onSearchChange() {
      // do filter
      $timeout(() => {
        const text = _.toString(ctrl.searchText).toLowerCase();
        let temp = null;
        let filterState = false;
        if (text.length === 0) {
          temp = _.cloneDeep(ctrl.source);
        } else {
          temp = _.filter(ctrl.source, el =>
            el.address._address && el.address._address.toLowerCase().indexOf(text) > -1);
          filterState = true;
        }
        _.forEach(temp, (el, idx) => (el.sequence = (idx + 1)));
        ctrl.list = temp;
        ctrl.filterState = filterState;
      }, 0);
    }

    function onDrop($index) {
      const movedElement = _.head(ctrl.list.splice($index, 1));
      reNumeriseList(movedElement);

      function reNumeriseList(element) {
        const newIndex = _.findIndex(ctrl.list, el => el.id === element.id);
        const isFirstElement = newIndex === 0;

        if (isFirstElement) {
          // take care main array
          const sourceElement = _.remove(ctrl.source, el => el.id === element.id);

          // put moved element to head of array
          ctrl.source = sourceElement.concat(ctrl.source);
        } else {
          // find element before the moved element
          const predecorsIndex = newIndex - 1;
          const predecorsElement = ctrl.list[predecorsIndex];
          const predecorsElementId = predecorsElement.id;

          // take care main array
          const sourceElement = _.remove(ctrl.source,
            el => el.id === element.id);
          const sourcePredecorsIndex = _.findIndex(ctrl.source,
            el => el.id === predecorsElementId);
          // put moved element to sourcePredecorsIndex + 1
          const tempArray = _.cloneDeep(ctrl.source);
          const tail = tempArray.splice(sourcePredecorsIndex + 1,
            tempArray.length - (sourcePredecorsIndex + 1));
          ctrl.source = tempArray.concat(sourceElement).concat(tail);
        }
        _.forEach(ctrl.source, (el, idx) => (el.sequence = (idx + 1)));
        _.forEach(ctrl.list, (el, idx) => (el.sequence = (idx + 1)));
      }
    }

    function onConfirmSequence() {
      // store
      ctrl.confirmState = API_STATE.WAITING;
      const sams = _.map(ctrl.source, sam => sam.id);
      const payload = _.assign(ctrl.groupData, { sams_ids: sams });
      ReservationPreset.updateGroups(_.castArray(payload))
        .then(() =>
          nvToast.success(
            nvTranslate.instant('container.reservation-preset-management.edit-sequence-success', { groupName: ctrl.groupData.name })
          )
        )
        .then(() => redirectToOverViewPage())
        .finally(() => (ctrl.confirmState = API_STATE.IDLE));
    }

    function redirectToOverViewPage() {
      $state.go('container.reservation-preset-management.overview', { refresh: true });
    }
  }
}());
