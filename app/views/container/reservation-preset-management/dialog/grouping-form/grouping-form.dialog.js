(function controller() {
  angular
    .module('nvOperatorApp.controllers')
    .controller('ReservationPresetGroupingFormDialogController', ReservationPresetGroupingFormDialogController);

  ReservationPresetGroupingFormDialogController.$inject = [
    '$q', 'groupData', '$mdDialog', 'nvTranslate', 'ReservationPreset',
    'Hub', '$scope', 'nvToast', 'Driver', 'resultData',
  ];

  function ReservationPresetGroupingFormDialogController(
    $q, groupData, $mdDialog, nvTranslate, ReservationPreset,
    Hub, $scope, nvToast, Driver, resultData
  ) {
    // variables
    const ctrl = this;
    ctrl.title = null;
    ctrl.submitButtonTitle = null;
    ctrl.formSubmitState = 'idle';
    ctrl.hubsSelectionOptions = [];
    ctrl.RESERVATION_PRESET_FIELDS = ReservationPreset.FIELDS;

    // functions
    ctrl.getAll = getAll;
    ctrl.onSave = onSave;
    ctrl.isAbleToSubmit = isAbleToSubmit;
    ctrl.onCancel = onCancel;

    // start
    initialize();

    // functions details
    function initialize() {
      ctrl.title = nvTranslate.instant('container.reservation-preset-management.add-new-group');
      ctrl.submitButtonTitle = nvTranslate.instant('commons.submit');
      if (_.size(groupData) > 0) {
        ctrl.title = nvTranslate.instant('container.reservation-preset-management.edit-group');
        ctrl.submitButtonTitle = nvTranslate.instant('commons.update');
      }
    }

    function getAll() {
      if (resultData) {
        return $q.when([
          resultData.driversResult,
          resultData.hubsResult,
        ]).then(success);
      }

      return $q.all([
        Driver.searchAll(),
        Hub.read(),
      ]).then(success, $q.reject);

      function success(response) {
        ctrl.driversSelectionOptions = Driver.toOptions(response[0]);
        ctrl.hubsSelectionOptions = Hub.toOptions(response[1]);

        ctrl.group = _.defaults(_.cloneDeep(groupData), {
          id: null,
          name: '',
          driver_id: null,
          hub_id: null,

          driverSelectedItem: null,
          hubSelectedItem: null,
        });

        if (ctrl.group.driver_id) {
          ctrl.group.driverSelectedItem = _.find(
            ctrl.driversSelectionOptions, ['value', ctrl.group.driver_id]
          ) || null;
        }

        if (ctrl.group.hub_id) {
          ctrl.group.hubSelectedItem = _.find(
              ctrl.hubsSelectionOptions, ['value', ctrl.group.hub_id]
            ) || null;
        }
      }
    }

    function onSave() {
      ctrl.formSubmitState = 'waiting';

      // payload
      const payload = {
        name: ctrl.group.name,
        driver_id: _.get(ctrl.group.driverSelectedItem, 'value') || null,
        hub_id: _.get(ctrl.group.hubSelectedItem, 'value') || null,
      };

      if (ctrl.group.id > 0) {
        // edit mode
        payload.id = ctrl.group.id;
        payload.sams_ids = ctrl.group.sams_ids || null;

        ReservationPreset.updateGroups(_.castArray(payload))
          .then(success).finally(finallyFn);
      } else {
        // create mode
        ReservationPreset.createGroup(payload)
          .then(success).finally(finallyFn);
      }

      function success(response) {
        let theResponse = response;
        if (ctrl.group.id > 0) {
          theResponse = payload;

          nvToast.success(nvTranslate.instant('container.reservation-preset-management.group-updated'));
        } else {
          nvToast.success(nvTranslate.instant('container.reservation-preset-management.group-created'));
        }

        $mdDialog.hide(theResponse);
      }

      function finallyFn() {
        ctrl.formSubmitState = 'idle';
      }
    }

    function isAbleToSubmit() {
      return $scope.modelForm && $scope.modelForm.$valid;
    }

    function onCancel() {
      $mdDialog.cancel();
    }
  }
}());
