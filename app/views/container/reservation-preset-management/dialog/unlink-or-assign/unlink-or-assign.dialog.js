(function controller() {
  angular
    .module('nvOperatorApp.controllers')
    .controller('ReservationPresetUnlinkOrAssignDialogController', ReservationPresetUnlinkOrAssignDialogController);

  ReservationPresetUnlinkOrAssignDialogController.$inject = [
    '$q', 'pendingTaskData', '$mdDialog', 'nvTranslate', 'ReservationPreset',
    '$scope', 'nvToast', 'resultData', 'IS_DEBUG', '$timeout',
    'nvDateTimeUtils',
  ];

  function ReservationPresetUnlinkOrAssignDialogController(
    $q, pendingTaskData, $mdDialog, nvTranslate, ReservationPreset,
    $scope, nvToast, resultData, IS_DEBUG, $timeout,
    nvDateTimeUtils
  ) {
    // variables
    const ctrl = this;
    ctrl.title = null;
    ctrl.submitButtonTitle = null;
    ctrl.formSubmitState = 'idle';

    ctrl.pendingTask = _.cloneDeep(pendingTaskData);

    // functions
    ctrl.getAll = getAll;
    ctrl.onSave = onSave;
    ctrl.isAbleToSubmit = isAbleToSubmit;
    ctrl.onCancel = onCancel;

    // start
    initialize();

    // functions details
    function initialize() {
      ctrl.title = nvTranslate.instant('container.reservation-preset-management.assign-shipper');
      ctrl.submitButtonTitle = nvTranslate.instant('container.reservation-preset-management.assign-shipper');
      if (!ctrl.pendingTask.link) {
        ctrl.title = nvTranslate.instant('container.reservation-preset-management.unassign-shipper');
        ctrl.submitButtonTitle = nvTranslate.instant('container.reservation-preset-management.unassign-shipper');
      }
    }

    function getAll() {
      if (resultData) {
        return $q.when([
          resultData.groupsResult,
        ]).then(success);
      }

      if (IS_DEBUG) {
        const deferred = $q.defer();

        $.getJSON('views/container/reservation-preset-management/json/groups.json', (groups) => {
          $timeout(() => {
            deferred.resolve([groups.data]);
          });
        });

        return deferred.promise.then(success);
      }

      return $q.all([
        ReservationPreset.getGroups({
          date: moment().format(nvDateTimeUtils.FORMAT_DATE).replace(/[^\d]/g, ''),
        }),
      ]).then(success);

      function success(response) {
        const groupsResult = response[0];
        ctrl.groupsSelectionOptions = ReservationPreset.groupToOptions(groupsResult);

        if (!ctrl.pendingTask.link && ctrl.pendingTask.sams_id) {
          // unlink mode
          const filteredGroups = _.filter(groupsResult, group =>
            _.includes(group.sams_ids, ctrl.pendingTask.sams_id)
          );

          ctrl.pendingTask.groupName = _.join(_.map(filteredGroups, 'name'), ', ');
        } else {
          // assign mode
          ctrl.pendingTask.groupSelectedItem = null;
        }
      }
    }

    function onSave() {
      ctrl.formSubmitState = 'waiting';

      // payload
      if (!ctrl.pendingTask.link) {
        // unlink mode
        ReservationPreset.unlinkAddressFromShipper(ctrl.pendingTask.id)
          .then(success).finally(finallyFn);
      } else {
        // assign mode
        ReservationPreset.linkAddressToGroup(
          ctrl.pendingTask.groupSelectedItem.value, ctrl.pendingTask.id
        ).then(success).finally(finallyFn);
      }

      function success() {
        const response = {
          id: ctrl.pendingTask.id,
        };

        if (!ctrl.pendingTask.link) {
          nvToast.success(
            nvTranslate.instant('container.reservation-preset-management.pending-task-unlinked', {
              shipperName: _.get(ctrl.pendingTask.shipper, 'name'),
              address: ctrl.pendingTask.fullAddress,
              groupName: ctrl.pendingTask.groupName,
            })
          );
        } else {
          response.groupId = ctrl.pendingTask.groupSelectedItem.value;

          nvToast.success(
            nvTranslate.instant('container.reservation-preset-management.pending-task-assigned', {
              shipperName: _.get(ctrl.pendingTask.shipper, 'name'),
              address: ctrl.pendingTask.fullAddress,
              groupName: ctrl.pendingTask.groupSelectedItem.displayName,
            })
          );
        }

        $mdDialog.hide(response);
      }

      function finallyFn() {
        ctrl.formSubmitState = 'idle';
      }
    }

    function isAbleToSubmit() {
      return $scope.modelForm && $scope.modelForm.$valid;
    }

    function onCancel() {
      $mdDialog.cancel();
    }
  }
}());
