(function controller() {
  angular
    .module('nvOperatorApp.controllers')
    .controller('ReservationPresetRoutePendingReservationsDialogController', ReservationPresetRoutePendingReservationsDialogController);

  ReservationPresetRoutePendingReservationsDialogController.$inject = [
    'groupsData', '$mdDialog', 'nvTranslate', 'routeDateData', 'nvToast',
    'Address', 'Reservation', 'nvDateTimeUtils', '$q', 'nvRequestUtils',
    'ShipperPickups', 'nvDialog',
  ];

  function ReservationPresetRoutePendingReservationsDialogController(
    groupsData, $mdDialog, nvTranslate, routeDateData, nvToast,
    Address, Reservation, nvDateTimeUtils, $q, nvRequestUtils,
    ShipperPickups, nvDialog
  ) {
    // variables
    let bulkActionProgressPayload;
    let successRouted = [];
    let unsuccessRouted = [];
    const ctrl = this;
    ctrl.formSubmitState = 'idle';

    ctrl.groups = groupsData;
    const startDate = nvDateTimeUtils.displayDateTime(
      nvDateTimeUtils.toSOD(routeDateData), 'utc'
    );
    const endDate = nvDateTimeUtils.displayDateTime(
      nvDateTimeUtils.toEOD(routeDateData), 'utc'
    );

    // functions
    ctrl.onSave = onSave;
    ctrl.onCancel = onCancel;

    // functions details
    function onSave($event) {
      ctrl.formSubmitState = 'waiting';

      // payload
      // get address ids by sams_ids
      const samsIds = _.uniq(_.compact(
        _.flatten(_.map(ctrl.groups, 'sams_ids'))
      ));

      Address.readMilkrunSettingsByIds(samsIds)
        .then(readMilkrunSuccess, readMilkrunFailure);

      function readMilkrunSuccess(milkrunSettings) {
        readReservations(milkrunSettings);
      }

      function readMilkrunFailure() {
        ctrl.formSubmitState = 'idle';
      }

      function readReservations(milkrunSettings) {
        // get all reservations info by address id
        const promises = [];
        _.forEach(milkrunSettings, (milkrunSetting) => {
          promises.push(
            Reservation.search({
              includeRoute: true,
              addressId: milkrunSetting.address_id,
              startDate: startDate,
              endDate: endDate,
            })
          );
        });

        $q.all(promises)
          .then(readReservationsSuccess, readReservationsFailure);

        function readReservationsSuccess(reservationsByAddressId) {
          let unroutedReservationsPayload = [];
          _.forEach(reservationsByAddressId, (reservations) => {
            unroutedReservationsPayload = _.concat(
              unroutedReservationsPayload, generateUnroutedReservationsPayload(reservations)
            );
          });
          unroutedReservationsPayload = _.uniqBy(unroutedReservationsPayload, 'reservationId');
          addReservationsToRoute(unroutedReservationsPayload);

          function generateUnroutedReservationsPayload(reservations) {
            const unroutedReservations = _.filter(reservations, reservation =>
              _.isEmpty(_.get(reservation, 'activeWaypoint.activeRoute')) &&
              Reservation.STATUS[reservation.status] === Reservation.STATUS.PENDING
            );
            const routedReservations = _.filter(reservations, reservation =>
              !_.isEmpty(_.get(reservation, 'activeWaypoint.activeRoute'))
            );

            if (_.size(routedReservations) > 0) {
              const routeId = routedReservations[0].activeWaypoint.activeRoute.id;

              return _.map(unroutedReservations, unroutedReservation => ({
                routeId: routeId,
                reservationId: unroutedReservation.id,
              }));
            }

            return [];
          }
        }

        function readReservationsFailure() {
          ctrl.formSubmitState = 'idle';
        }
      }

      function addReservationsToRoute(unroutedReservationsPayload) {
        if (_.size(unroutedReservationsPayload) <= 0) {
          nvToast.info(
            nvTranslate.instant('container.reservation-preset-management.x-reservations-added-to-route', {
              num: 0,
            })
          );

          $mdDialog.hide();
          return;
        }

        bulkActionProgressPayload = {
          totalCount: _.size(unroutedReservationsPayload),
          currentIndex: 0,
          errors: [],
        };

        nvDialog.showSingle($event, {
          templateUrl: 'lib/ninja-commons/services/dialogs/bulk-action-progress/bulk-action-progress.dialog.html',
          cssClass: 'bulk-action-progress',
          controller: 'BulkActionProgressDialogController',
          controllerAs: 'ctrl',
          skipHide: true,
          clickOutsideToClose: false,
          locals: {
            payload: bulkActionProgressPayload,
          },
        }).then(progressDialogClosed);

        startAddingReservationToRoute(_.cloneDeep(unroutedReservationsPayload));

        function progressDialogClosed() {
          if (getSuccessCount() > 0) {
            nvToast.info(
              nvTranslate.instant('container.reservation-preset-management.x-reservations-added-to-route', {
                num: getSuccessCount(),
              })
            );
          }

          $mdDialog.hide();
        }

        function getSuccessCount() {
          return bulkActionProgressPayload.totalCount - bulkActionProgressPayload.errors.length;
        }
      }
    }

    function startAddingReservationToRoute(unroutedReservationsPayload) {
      let payloadToProcess;
      if (unroutedReservationsPayload.length > 0) {
        bulkActionProgressPayload.currentIndex += 1;
        payloadToProcess = unroutedReservationsPayload.splice(0, 1)[0];

        ctrl.formSubmitState = 'waiting';
        ShipperPickups.rerouteReservation(payloadToProcess.reservationId, {
          new_route_id: payloadToProcess.routeId,
          route_index: -1,
          overwrite: true,
        }).then(success, failure);
      } else {
        ctrl.formSubmitState = 'idle';
      }

      function success() {
        successRouted = _.concat(successRouted, payloadToProcess);
        bulkActionProgressPayload.successCount += 1;

        if (unroutedReservationsPayload.length > 0) {
          startAddingReservationToRoute(unroutedReservationsPayload); // recursive
        } else {
          ctrl.formSubmitState = 'idle';
        }
      }

      function failure() {
        unsuccessRouted = _.concat(unsuccessRouted, payloadToProcess);
        bulkActionProgressPayload.errors.push(`Reservation ${payloadToProcess.reservationId} to route ${payloadToProcess.routeId}`);

        if (unroutedReservationsPayload.length > 0) {
          startAddingReservationToRoute(unroutedReservationsPayload); // recursive
        } else {
          ctrl.formSubmitState = 'idle';
        }
      }
    }

    function onCancel() {
      $mdDialog.cancel();
    }
  }
}());
