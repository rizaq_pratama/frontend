(function controller() {
  angular
    .module('nvOperatorApp.controllers')
    .controller('ReservationPresetSelectionErrorDialogController', ReservationPresetSelectionErrorDialogController);

  ReservationPresetSelectionErrorDialogController.$inject = [
    '$mdDialog', 'groupsValidationErrorData',
  ];

  function ReservationPresetSelectionErrorDialogController(
    $mdDialog, groupsValidationErrorData
  ) {
    // variables
    const ctrl = this;
    ctrl.groupsValidationErrorData = groupsValidationErrorData;

    // functions
    ctrl.onCancel = onCancel;
    ctrl.onProceed = onProceed;

    // functions details
    function onCancel() {
      $mdDialog.cancel();
    }

    function onProceed() {
      $mdDialog.hide();
    }
  }
}());
