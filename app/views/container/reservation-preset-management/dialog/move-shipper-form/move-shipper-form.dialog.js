(function controller() {
  angular
    .module('nvOperatorApp.controllers')
    .controller('ReservationPresetMoveShipperFormDialogController', ReservationPresetMoveShipperFormDialogController);

  ReservationPresetMoveShipperFormDialogController.$inject = ['data', '$mdDialog'];

  function ReservationPresetMoveShipperFormDialogController(data, $mdDialog) {

    // variables
    const ctrl = this;
    ctrl.data = _.clone(data);
    ctrl.formState = 'idle';
    ctrl.data = _.cloneDeep(data);
    ctrl.view = {
      shipperNames: '',
    };

    // function def
    ctrl.onChangeGroup = onChangeGroup;
    ctrl.onSubmit = onSubmit;
    ctrl.isAbleToSave = isAbleToSave;

    init();


    // function body
    function init() {
      // get shipper name from sams data
      ctrl.view.shipperNames = _(ctrl.data.samsData)
                                .map(sams => (data.shipperMap[_.get(sams, 'address.shipper_id')] || ''))
                                .map(shipper => (shipper.name))
                                .uniq()
                                .join(', ');
    }

    function onChangeGroup(group) {
      _.each(ctrl.data.groups, (grp) => {
        if (grp.name !== group.name) {
          grp.selected = false;
        } else {
          grp.selected = true;
        }
      });
    }
    // send the selectedGroup to parent ctrl to move it
    function onSubmit() {
      const selectedGroup = _.find(ctrl.data.groups, { selected: true });
      return $mdDialog.hide(selectedGroup.id);
    }

    function isAbleToSave() {
      const selectedGroup = _.find(ctrl.data.groups, { selected: true });
      if (selectedGroup) {
        return true;
      }
      return false;
    }
  }
}());
