(function controller() {
  angular
    .module('nvOperatorApp.controllers')
    .controller('ReservationPresetCreateRouteDialogController', ReservationPresetCreateRouteDialogController);

  ReservationPresetCreateRouteDialogController.$inject = [
    'groupsData', '$mdDialog', 'nvTranslate', 'routeDateData', 'nvToast',
    'ReservationPreset',
  ];

  function ReservationPresetCreateRouteDialogController(
    groupsData, $mdDialog, nvTranslate, routeDateData, nvToast,
    ReservationPreset
  ) {
    // variables
    const ctrl = this;
    ctrl.formSubmitState = 'idle';

    ctrl.groups = groupsData;

    // functions
    ctrl.onSave = onSave;
    ctrl.onCancel = onCancel;

    // functions details
    function onSave() {
      ctrl.formSubmitState = 'waiting';

      // payload
      const payload = {
        date: routeDateData,
        milkrun_group_ids: _.map(ctrl.groups, 'id'),
      };

      ReservationPreset.createRoutes(payload)
        .then(success).finally(finallyFn);

      function success(response) {
        nvToast.success(nvTranslate.instant('container.reservation-preset-management.routes-have-been-created'));
        $mdDialog.hide(response);
      }

      function finallyFn() {
        ctrl.formSubmitState = 'idle';
      }
    }

    function onCancel() {
      $mdDialog.cancel();
    }
  }
}());
