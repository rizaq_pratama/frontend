(function controller() {
  angular
    .module('nvOperatorApp.controllers')
    .controller('OrderWeightUpdateController', OrderWeightUpdateController);

  OrderWeightUpdateController.$inject = ['nvToast', 'nvTranslate', 'nvDialog', 'nvTable',
    'Order', 'nvCsvParser', 'nvFileUtils', 'nvDateTimeUtils', 'nvTimezone'];

  const FIELDS = {
    orderId: { displayName: 'ID' },
    trackingId: { displayName: 'commons.tracking-id' },
    stampId: { displayName: 'commons.stamp-id' },
    status: { displayName: 'commons.status' },
    newWeight: { displayName: 'container.order-weight-update.new-weight' },
    _isValidText: { displayName: 'container.order-weight-update.is-valid' },
  };

  const EMPTY_ARRAY = [];

  function OrderWeightUpdateController(nvToast, nvTranslate, nvDialog, nvTable,
      Order, nvCsvParser, nvFileUtils, nvDateTimeUtils, nvTimezone) {
    const ctrl = this;

    ctrl.invalidOrders = [];
    ctrl.openUploadCsvDialog = openUploadCsvDialog;
    ctrl.getOrderDatasLength = getOrderDatasLength;
    ctrl.cancel = cancel;
    ctrl.execute = execute;
    ctrl.ordersTableParam = null;

    init();

    function init() {
      ctrl.ordersTableParam = nvTable.createTable(FIELDS);
    }

    function openUploadCsvDialog($event) {
      nvDialog.showSingle($event, {
        templateUrl: 'views/container/order/dialog/csv-find-orders/csv-find-orders.dialog.html',
        theme: 'nvYellow',
        cssClass: 'order-find-by-csv',
        controller: 'OrderFindByCsvDialogController',
        controllerAs: 'ctrl',
        locals: {
          extras: {
            dialogType: 'with_weight',
          },
        },
      }).then(onSuccess);

      function onSuccess(response) {
        const { validOrders, originalDatas } = response;
        // map tracking id to weight
        const keyedOriginalDatas = {};
        _.forEach(originalDatas, (data) => {
          const trackingId = _.trim(data[0]);
          const weight = Number(_.trim(data[1]));
          const isValid = _.isFinite(weight);
          keyedOriginalDatas[trackingId] = {
            trackingId: trackingId,
            weight: weight,
            isValid: isValid,
          };
        });
        if (_.size(validOrders) > 0) {
          const extendedOrders = extendOrders(validOrders);
          ctrl.invalidOrders = _.filter(extendedOrders, theOrder => !theOrder.isValid);
          ctrl.ordersTableParam.setData(extendedOrders);

          if (_.size(ctrl.invalidOrders) > 0) {
            nvToast.warning(nvTranslate.instant('container.order-weight-update.some-invalid-weight-found'));
          }
        } else {
          nvToast.warning(nvTranslate.instant('container.order-weight-update.no-valid-orders-found'));
        }

        function extendOrders(datas) {
          return _.map(datas, (data) => {
            const trackingId = _.trim(_.get(data, 'tracking_id'));
            const stampId = _.trim(_.get(data, 'stamp_id'));
            const originalData = _.get(keyedOriginalDatas, trackingId);

            const isValid = _.get(originalData, 'isValid', false);
            return {
              orderId: data.id,
              trackingId: trackingId !== '' ? trackingId : '-',
              stampId: stampId !== '' ? stampId : '-',
              status: _.get(data, 'status'),
              newWeight: isValid ? _.get(originalData, 'weight') : 'INVALID',
              isValid: isValid,
              _isValidText: isValid ? 'VALID' : 'INVALID',
            };
          });
        }
      }
    }

    function getOrderDatas() {
      if (ctrl.ordersTableParam) {
        return ctrl.ordersTableParam.getTableData();
      }
      return EMPTY_ARRAY;
    }

    function getOrderDatasLength() {
      return _.size(getOrderDatas());
    }

    function cancel() {
      init();
    }

    function execute($event) {
      const requests = _(getOrderDatas())
        .filter(rawData => rawData.isValid)
        .map(constructRequest)
        .value();

      const successIds = [];

      ctrl.bulkActionProgressPayload = {
        totalCount: _.size(requests),
        currentIndex: 0,
        errors: [],
      };

      nvDialog.showSingle($event, {
        templateUrl: 'lib/ninja-commons/services/dialogs/bulk-action-progress/bulk-action-progress.dialog.html',
        cssClass: 'bulk-action-progress',
        controller: 'BulkActionProgressDialogController',
        controllerAs: 'ctrl',
        skipHide: true,
        clickOutsideToClose: false,
        locals: {
          payload: ctrl.bulkActionProgressPayload,
        },
      }).then(progressDialogClosed);

      startUpdateOrdersWeight(requests);

      function progressDialogClosed() {
        downloadInvalidOrdersAsCsv();

        if (_.size(successIds) > 0) {
          nvToast.success(nvTranslate.instant('container.order-weight-update.weight-update-success'));
        }
        init();
      }

      function startUpdateOrdersWeight(theRequests) {
        let theRequest;

        ctrl.bulkActionProgressPayload.currentIndex += 1;
        if (_.size(theRequests) > 0) {
          theRequest = _.head(theRequests.splice(0, 1));
          Order.updateWeight(theRequest.orderId, theRequest.payload)
            .then(multiSelectSuccess, multiSelectError);
        }

        function multiSelectSuccess() {
          ctrl.bulkActionProgressPayload.successCount += 1;
          successIds.push(theRequest.orderId);
          if (_.size(theRequests) > 0) {
            startUpdateOrdersWeight(theRequests);
          }
        }

        function multiSelectError(error) {
          ctrl.bulkActionProgressPayload.errors.push(`Order ${theRequest.orderId}: ${_.get(error, 'message', 'Unknown Error')}`);
          if (_.size(theRequests) > 0) {
            startUpdateOrdersWeight(theRequests);
          }
        }
      }

      function constructRequest(rawData) {
        return {
          orderId: rawData.orderId,
          payload: {
            weight: rawData.newWeight,
          },
        };
      }

      function downloadInvalidOrdersAsCsv() {
        if (_.size(ctrl.invalidOrders) > 0) {
          const invalidOrderCsvDatas = _.map(ctrl.invalidOrders, theOrder => ([
            theOrder.trackingId,
            theOrder.newWeight,
          ]));

          nvCsvParser
            .unparse(invalidOrderCsvDatas)
            .then(content => nvFileUtils.downloadCsv(content, `invalid-weight-update-orders-${nvDateTimeUtils.displayDateTime(moment(), nvTimezone.getOperatorTimezone())}.csv`));
        }
      }
    }
  }
}());
