(function controller() {
  angular.module('nvOperatorApp.controllers')
      .controller('ShipmentUploadOrdersDialog', ShipmentUploadOrdersDialog);
  ShipmentUploadOrdersDialog.$inject = ['uploadResult', '$mdDialog'];
  function ShipmentUploadOrdersDialog(uploadResult, $mdDialog) {
    const ctrl = this;

    ctrl.onCancel = onCancel;
    ctrl.uploadResult = _.cloneDeep(uploadResult);


    function onCancel() {
      $mdDialog.cancel();
    }
  }
}());
