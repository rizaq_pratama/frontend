(function controller() {
  angular
        .module('nvOperatorApp.controllers')
        .controller('ShipmentDialogController', ShipmentDialogController);
  ShipmentDialogController.$inject = [
    'Shipping', '$mdDialog', 'hubs', 'mode', 'shipment', 'shipmentTypes', 'nvToast',
    'nvDateTimeUtils', '$rootScope', '$scope', 'nvTranslate', 'nvTimezone',
    'nvCsvParser', '$q', 'nvDialog', '$log', '$timeout', 'nvDomain'
  ];
  function ShipmentDialogController(Shipping, $mdDialog, hubs, mode, shipment,
                                    shipmentTypes, nvToast, nvDateTimeUtils,
                                    $rootScope, $scope, nvTranslate,
                                    nvTimezone, nvCsvParser, $q, nvDialog, $log, $timeout, nvDomain) {
    const ctrl = this;
    const STATUS = {
      PENDING : 'Pending',
      SUCCESS : 'Success',
      COMPLETED : 'Completed',
      CANCELLED : 'Cancelled',
    };
    let oldShipment = null;

    ctrl.onUpdate = onUpdate;
    ctrl.shipment = {};
    ctrl.onCancel = onCancel;
    ctrl.onSave = onSave;
    ctrl.onForceSuccess = onForceSuccess;
    ctrl.onCancelShipment = onCancelShipment;
    ctrl.onFileSelect = onFileSelect;
    ctrl.onFileReject = onFileReject;
    ctrl.mode = mode;
    ctrl.hubs = _.cloneDeep(hubs);
    ctrl.shipmentTypes = _.cloneDeep(shipmentTypes);
    ctrl.oldEndDate = null;
    ctrl.oldMawb = null;

    // flags
    ctrl.endDateChanged = false;
    ctrl.createState = 'idle';
    ctrl.isCreateAnother = false;
    ctrl.isWaitingForConfirm = false;
    init();

    function init() {
      if (mode === 'edit') {
        ctrl.shipment = shipment;
        oldShipment = _.cloneDeep(shipment);
        ctrl.shipment.eta =
          nvDateTimeUtils.toMoment(shipment.arrival_datetime, nvTimezone.getOperatorTimezone());
        ctrl.shipment.eda =
          nvDateTimeUtils.toDate(shipment.arrival_datetime, nvTimezone.getOperatorTimezone());
        ctrl.shipment.endDateObj = shipment.end_date ?
          nvDateTimeUtils.toDate(shipment.end_date, 'utc') : null;
        ctrl.oldMawb = _.cloneDeep(shipment.mawb);
        ctrl.oldEndDate = _.cloneDeep(ctrl.shipment.endDateObj);
      } else {
        ctrl.shipment.eta = moment().tz(nvTimezone.getOperatorTimezone());
        ctrl.shipment.eta.minute(0);
        ctrl.shipment.eta.second(0);
        ctrl.shipment.eda = nvDateTimeUtils.toDate(new Date(), nvTimezone.getOperatorTimezone());
      }
    }
    function resetForm() {
      ctrl.shipment.comments = null;
    }

    /**
     * 
     * @param {Object} nShipment
     * @param {Date} eda
     * @param {Moment} eta
     */
    function setEta(nShipment, eda, nEta) {
      nEta.set({
        year: eda.getFullYear(),
        month: eda.getMonth(),
        date: eda.getDate(),
      });
      nShipment.arrival_datetime = nvDateTimeUtils.displayFormat(nEta, nvDateTimeUtils.FORMAT_LONG, 'utc');
      return nShipment;
    }
    /*
    * create function
    */
    function onSave(isCreateAnother = false) {
      ctrl.isCreateAnother = isCreateAnother;
      ctrl.createState = 'waiting';
      const origHub = _.find(ctrl.hubs, { value: ctrl.shipment.orig_hub_id });
      // const destHub = _.find(ctrl.hubs, { value: ctrl.shipment.dest_hub_id }); changes made regarding systemId
      let savedShipment = null;
      // complete the shipment object for saving process
      ctrl.shipment.orig_hub_country = nvDomain.getDomain().current; // origHub.country
      ctrl.shipment.dest_hub_country = nvDomain.getDomain().current; // destHub.country;
      ctrl.shipment.curr_hub_country = nvDomain.getDomain().current; // origHub.country;
      ctrl.shipment.curr_hub_id = origHub.id;
      // handle the eta date
      ctrl.shipment = setEta(ctrl.shipment, ctrl.shipment.eda, ctrl.shipment.eta);
      ctrl.shipment.status = STATUS.PENDING;
      const payload = {};
      payload.shipment = _.omit(ctrl.shipment, ['eta', 'eda']);
      ctrl.isWaitingForConfirm = true;
      // send to server
      return Shipping.create(payload).then(onSuccess, onError);
      function onSuccess(result) {
        savedShipment = result;
        ctrl.createState = 'idle';
        return nvToast.info(nvTranslate.instant('container.shipment-management.success-create',
        { shipmentId: result.shipment.id }), null,
        { buttonCallback: onClickOkToast, buttonTitle: 'OK', timeOut: 0, extendedTimeOut: 0 });
      }

      function onClickOkToast() {
        ctrl.isWaitingForConfirm = false;
        if (ctrl.isCreateAnother) {
          resetForm();
        } else {
          $mdDialog.hide(savedShipment);
        }
      }

      function onError(err) {
        nvToast.error(err);
      }
    }
    /**
    * update function
    */
    function onUpdate() {
      if (ctrl.modelForm.$invalid) {
        return false;
      }
      const oldMawb = ctrl.oldMawb || '';
      const newMawb = ctrl.shipment.mawb || '';
      if (oldMawb !== newMawb) {
        // show confirm save dialog
        return nvDialog.confirmSave(null,
          {
            content:
              nvTranslate.instant('container.shipment-management.you-are-changing-master-awb-proceed'),
            skipHide: true,
          }
        ).then(doTheSaving);
      }
      return doTheSaving();

      function doTheSaving() {
        ctrl.shipment = setEta(ctrl.shipment, ctrl.shipment.eda, ctrl.shipment.eta);
        // const origHub = _.find(ctrl.hubs, { value: ctrl.shipment.orig_hub_id }); changes made regarding systemId
        // const destHub = _.find(ctrl.hubs, { value: ctrl.shipment.dest_hub_id });

        ctrl.shipment.end_date = ctrl.shipment.endDateObj ?
          nvDateTimeUtils.displayDate(ctrl.shipment.endDateObj) : null;
        const nShipment = _.omit(_.cloneDeep(ctrl.shipment), ['sn', 'endDateObj', 'eta', 'eda', 'orders']);
        const shipmentDataToSend = getObjectDiff(oldShipment, nShipment);
        // add country
        if (shipmentDataToSend.orig_hub_id) {
          shipmentDataToSend.orig_hub_country = nvDomain.getDomain().current;
        }

        if (shipmentDataToSend.dest_hub_id) {
          shipmentDataToSend.dest_hub_country = nvDomain.getDomain().current;
        }
        const requests = [Shipping.update({ shipment: shipmentDataToSend }, ctrl.shipment.id)];
        if (ctrl.endDateChanged) {
          requests.push(Shipping.updateShipmentDate(nShipment, ctrl.shipment.end_date));
        }
        return $q.all(requests)
          .then(onSuccess);
      }

      function onSuccess(result) {
        $mdDialog.hide(result[0]);
      }
    }
    /**
     * get difference between source and new object, only return properties from new object that changed
     * compared to source object
     * note : this method unable to comparing array
     * @param {*} obj1 source object
     * @param {*} obj2 new object
     */
    function getObjectDiff(obj1, obj2) {
      return Object.keys(obj2).reduce((diff, key) => {
        if (obj1[key] === obj2[key]) {
          return diff;
        }
        diff[key] = obj2[key];
        return diff;
      }, {});
    }

    function onForceSuccess() {
      const payload = {
        shipment: {
          status: 'Completed',
        },
      };

      return Shipping.update(payload, ctrl.shipment.id)
                .then(onSuccess);

      function onSuccess(result) {
        nvToast.info(nvTranslate.instant('container.shipment-management.'));
        $mdDialog.hide(result);
      }
    }

    function onCancelShipment() {
      const payload = {
        shipment: {
          status: 'Cancelled',
        },
      };

      return Shipping.update(payload, ctrl.shipment.id)
                .then(onSuccess);

      function onSuccess(result) {
        nvToast.info(nvTranslate.instant('container.shipment-management.success-change-status',
            { shipmentId: ctrl.shipment.id }));
        $mdDialog.hide(result);
      }
    }

    function onCancel() {
      $mdDialog.cancel();
    }

    function onFileReject() {
      nvToast.error('CSV Rejected');
    }

    function onFileSelect(selectedFile) {
      let file = null;
      let reader = null;
      if (window.File && window.FileReader && window.FileList && window.Blob) {
        reader = new FileReader();
        file = selectedFile;
        if ((file != null) && (file.size < 2 * 1024 * 1024)) {
          reader.onload = onFileLoaded;
          reader.readAsText(file);
        } else if ((file != null) && (file.size >= 2 * 1024 * 1024)) {
          $log.info('file is too large');
        } else {
          $log.info('No api available');
        }
      }

      function onFileLoaded() {
        const text = reader.result;
        const cfg = {
          dynamicTyping: true,
          header: false,
        };
        nvCsvParser.parse(text, cfg).then(onParseComplete);
      }

      function onParseComplete(result) {
        // do update shipment's order
        if (result.data && result.data.length > 0) {
          const payload = {};
          const orders = _.map(result.data, row => ({
            order_country: $rootScope.domain,
            tracking_id: row[0],
            action_type: 'ADD',
          }));
          payload.orders = orders;
          return Shipping.updateOrder(payload, ctrl.shipment.id)
            .then(onSuccessUploadOrders);
        }
        return $q.reject();
      }

      function onSuccessUploadOrders(result) {
        const success = [];
        const failed = [];
        _.each(result.orders, (row) => {
          if (row.status) {
            success.push(row);
          } else {
            failed.push(row);
          }
        });


        return nvDialog.showSingle(null, {
          cssClass: 'nv-shipment-upload-order-result-dialog',
          templateUrl: 'views/container/shipment-management/dialog/upload-shipment-order-result/upload-shipment-order-result.dialog.html',
          controller: 'ShipmentUploadOrdersDialog',
          controllerAs: 'ctrl',
          theme: 'nvBlue',
          skipHide: true,
          clickOutsideToClose: true,
          locals: {
            uploadResult: {
              successUpload: success,
              failedUpload: failed,
            },
          },
        });
      }
    }

    $scope.$watch('ctrl.shipment.endDateObj', () => {
      $timeout(() => {
        if (ctrl.modelForm['container.shipment-management.routing-date'] && ctrl.modelForm['container.shipment-management.routing-date'].$dirty) {
          ctrl.endDateChanged = true;
        } else {
          ctrl.endDateChanged = false;
        }
      });
    });
  }
}());
