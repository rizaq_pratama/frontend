(function controllerFn() {
  angular
      .module('nvOperatorApp.controllers')
      .controller('ShipmentManagementController', controller);
  controller.$inject = [
    'Shipping', 'nvDialog', '$timeout', 'nvToast', '$q', '$mdDialog', 'nvTable', '$filter', 'nvDateTimeUtils',
    '$rootScope', 'nvBackendFilterUtils', 'ShippingFilterTemplate', 'nvTimezone',
    'nvTranslate', '$window', '$state', 'Excel', 'Hub',
  ];

  function controller(Shipping, nvDialog, $timeout, nvToast,
                      $q, $mdDialog, nvTable, $filter, nvDateTimeUtils,
                      $rootScope, nvBackendFilterUtils, ShippingFilterTemplate,
                      nvTimezone, nvTranslate, $window, $state, Excel, Hub
                      ) {
    // //////////////////////
    // Variables
    // /////////////////////

    const ctrl = this;
    const STATE = {
      IDLE: 'idle',
      WAITING: 'waiting',
    };
    let REPORT_META;
    ctrl.selectLogic = null;
    ctrl.createDateStart = null;
    ctrl.createDateStop = null;
    ctrl.showFilter = true;
    ctrl.shipments = [];
    ctrl.shipmentsRef = [];
    ctrl.FILTER_SHOW = true;
    ctrl.FILTER_HIDE = false;
    ctrl.hubs = [];
    ctrl.activeHubs = [];
    ctrl.nvTimezone = nvTimezone;
    ctrl.loadState = 'ready'; // else "loading"
    ctrl.searchState = STATE.IDLE;
    ctrl.isNoMoreResult = false;


    // ///////////////////////
    // FILTER
    // ///////////////////////

    ctrl.selectedFilters = [];
    ctrl.possibleFilters = [];
    ctrl.filterPresets = [];
    ctrl.selectedPreset = null;

    // ////////////////////
    // table
    // ////////////////////
    ctrl.tableParam = {};
    ctrl.page = 1;
    ctrl.itemsPerPage = 300;
    ctrl.totalItems = Number.MAX_VALUE;


    // /////////////////////
    // function definition
    // ////////////////////
    ctrl.onSearch = onSearch;
    ctrl.toggleFilter = toggleFilter;
    ctrl.openFilter = openFilter;
    ctrl.createOne = createOne;
    ctrl.onLoad = onLoad;
    ctrl.updateOne = updateOne;
    ctrl.onForceSuccess = onForceSuccess;
    ctrl.deleteOne = deleteOne;
    ctrl.isFilterNonEmpty = isFilterNonEmpty;
    ctrl.showDetails = showDetails;
    ctrl.printAwb = printAwb;
    ctrl.loadMore = loadMore;
    ctrl.savePreset = savePreset;
    ctrl.deletePreset = deletePreset;
    ctrl.updatePreset = updatePreset;
    ctrl.generateReports = generateReports;

    // ///////////////////
    // Function Bodies
    // //////////////////

    function onForceSuccess($event, shipment) {
      nvDialog.confirmSave($event, {
        title: nvTranslate.instant('container.shipment-management.confirm-force-success'),
        content: nvTranslate.instant('container.shipment-management.force-success-confirmation-message', { x: shipment.id }),
        ok: nvTranslate.instant('commons.confirm'),
      })
      .then(onContinue);

      function onContinue() {
        const payload = {
          shipment: {
            status: 'Completed',
          },
        };

        return Shipping.update(payload, shipment.id)
            .then(onSuccess);
      }

      function onSuccess(result) {
        let targetShipment = result.shipment;
        targetShipment.comments_f = targetShipment.comments ? $filter('limitTo')(targetShipment.comments, 50) : '-';
        targetShipment = attachHubsName(targetShipment);
        ctrl.tableParam.mergeIn(targetShipment, 'id');
        nvToast.info(
          nvTranslate.instant('container.shipment-management.success-force-success',
          { shipmentId: targetShipment.id }));
      }
    }


    function onLoad() {
      return $q.all([
        Hub.read({ active_only: false }),
        ShippingFilterTemplate.getFilter(),
        Shipping.getShipmentTypes(),
      ])
          .then(success);


      function success(res) {
        const hubs = res[0];
        ctrl.filterPresets = res[1].templates;
        ctrl.shipmentTypes = res[2];

        ctrl.hubs = _(hubs)
          .map(hub => (
            {
              value: hub.id,
              displayName: hub.name,
              country: hub.country,
              lowercaseName: hub.name.toLowerCase(),
            }
          ))
          .sortBy('displayName')
          .value();

        ctrl.activeHubs = _(hubs)
          .filter(hub => hub.active)
          .map(hub => (
            {
              value: hub.id,
              displayName: hub.name,
              country: hub.country,
              lowercaseName: hub.name.toLowerCase(),
            }
          ))
          .sortBy('displayName')
          .value();

        ctrl.possibleFilters = _.sortBy([
          {
            type: nvBackendFilterUtils.FILTER_OPTIONS,
            mainTitle: nvTranslate.instant('container.shipment-management.shipment-type'),
            possibleOptions: _.cloneDeep(ctrl.shipmentTypes),
            selectedOptions: [],
            searchBy: 'displayName',
            searchText: {},
            key: 'shipment_type',
            presetKey: 'shipment_type',
            transformFn: Shipping.mapToShipmentTypeRequest,
            comparator: shipmentComparator,
          },
          {
            type: nvBackendFilterUtils.FILTER_TIME,
            mainTitle: nvTranslate.instant('container.shipment-management.shipment-date'),
            fromModel: moment().tz(nvTimezone.getOperatorTimezone()),
            toModel: moment().tz(nvTimezone.getOperatorTimezone()),
            transformFn: _.partial(nvDateTimeUtils.displayFormat, _, 'YYYY-MM-DDTHH:mm:ss[Z]', 'utc'),
            key: { from: 'from_created_date', to: 'to_created_date' },
            presetKey: { from: 'from_created_date', to: 'to_created_date' },
          },
          {
            type: nvBackendFilterUtils.FILTER_TIME,
            mainTitle: nvTranslate.instant('container.shipment-management.transit-date-time'),
            fromModel: moment().tz(nvTimezone.getOperatorTimezone()),
            toModel: moment().tz(nvTimezone.getOperatorTimezone()),
            transformFn: _.partial(nvDateTimeUtils.displayFormat, _, 'YYYY-MM-DDTHH:mm:ss[Z]', 'utc'),
            key: { from: 'from_transit_date', to: 'to_transit_date' },
            presetKey: { from: 'from_transit_date', to: 'to_transit_date' },
          },
          {
            type: nvBackendFilterUtils.FILTER_OPTIONS,
            mainTitle: nvTranslate.instant('container.shipment-management.shipment-status'),
            possibleOptions: _.cloneDeep(Shipping.getStatuses()),
            selectedOptions: [],
            searchBy: 'displayName',
            searchText: {},
            key: 'shipment_status',
            presetKey: 'shipment_status',
            transformFn: Shipping.mapToShipmentTypeRequest,
            comparator: shipmentComparator,
          },
          {
            type: nvBackendFilterUtils.FILTER_OPTIONS,
            mainTitle: nvTranslate.instant('container.shipment-management.start-hub'),
            possibleOptions: _.cloneDeep(ctrl.hubs),
            selectedOptions: [],
            searchBy: 'displayName',
            searchText: {},
            key: 'orig_hub',
            presetKey: 'orig_hub',
            transformFn: Shipping.mapToHubRequest,
            comparator: hubComparator,
          },
          {
            type: nvBackendFilterUtils.FILTER_OPTIONS,
            mainTitle: nvTranslate.instant('container.shipment-management.last-inbound-hub'),
            possibleOptions: _.cloneDeep(ctrl.hubs),
            selectedOptions: [],
            searchBy: 'displayName',
            searchText: {},
            key: 'curr_hub',
            presetKey: 'curr_hub',
            transformFn: Shipping.mapToHubRequest,
            comparator: hubComparator,
          },
          {
            type: nvBackendFilterUtils.FILTER_OPTIONS,
            mainTitle: nvTranslate.instant('container.shipment-management.end-hub'),
            possibleOptions: angular.copy(ctrl.hubs),
            selectedOptions: [],
            searchBy: 'displayName',
            searchText: {},
            key: 'dest_hub',
            presetKey: 'dest_hub',
            transformFn: Shipping.mapToHubRequest,
            comparator: hubComparator,
          },
          {
            type: nvBackendFilterUtils.FILTER_TIME,
            mainTitle: nvTranslate.instant('container.shipment-management.eta-date-time'),
            fromModel: nvDateTimeUtils.toSOD(moment().tz(nvTimezone.getOperatorTimezone())),
            toModel: nvDateTimeUtils.toSOD(moment().tz(nvTimezone.getOperatorTimezone())),
            transformFn: _.partial(nvDateTimeUtils.displayFormat, _, 'YYYY-MM-DDTHH:mm:ss[Z]', 'utc'),
            key: { from: 'from_arrival_date', to: 'to_arrival_date' },
            presetKey: { from: 'from_arrival_date', to: 'to_arrival_date' },
          },
          {
            type: nvBackendFilterUtils.FILTER_TIME,
            mainTitle: nvTranslate.instant('container.shipment-management.shipment-completion-date-time'),
            fromModel: moment().tz(nvTimezone.getOperatorTimezone()),
            toModel: moment().tz(nvTimezone.getOperatorTimezone()),
            transformFn: _.partial(nvDateTimeUtils.displayFormat, _, 'YYYY-MM-DDTHH:mm:ss[Z]', 'utc'),
            key: { from: 'from_completed_date', to: 'to_completed_date' },
            presetKey: { from: 'from_completed_date', to: 'to_completed_date' },
          },
          {
            type: nvBackendFilterUtils.FILTER_TEXT_FREE,
            mainTitle: nvTranslate.instant('container.shipment-management.mawb'),
            model: [],
            key: 'mawb',
            presetKey: 'mawb',
          },
        ], ['mainTitle']);
      }
    }

    function openFilter() {
      ctrl.showFilter = true;
      ctrl.page = 1;
    }

    function toggleFilter(state) {
      if (angular.isDefined(state)) {
        ctrl.showFilter = state;
      }
      ctrl.showFilter = !ctrl.showFilter;
    }

    /**
     check if filter has been filled or not
     */
    function isFilterNonEmpty() {
      return ctrl.selectedFilters.length > 0;
    }

    function filterByDateTimeString(strFn, fieldName) {
      return (shipment) => {
        const lowercase = strFn();
        return lowercase.length === 0 ||
          (shipment[fieldName] !== null && 
              ($filter('nvTime')(shipment[fieldName], 'operator', 'datetime')).indexOf(lowercase) >= 0);
      };
    }


    function onSearch() {
      ctrl.searchState = STATE.WAITING;
      ctrl.tableParam = nvTable.createTable(Shipping.TABLE_FIELDS);
      ctrl.tableParam.updateColumnFilter('created_at', _.partial(filterByDateTimeString, _, 'created_at'));
      ctrl.tableParam.updateColumnFilter('transit_at', _.partial(filterByDateTimeString, _, 'transit_at'));
      ctrl.tableParam.updateColumnFilter('arrival_datetime', _.partial(filterByDateTimeString, _, 'arrival_datetime'));
      ctrl.tableParam.updateColumnFilter('completed_at', _.partial(filterByDateTimeString, _, 'completed_at'));
      return loadShipmentPromise().then(success);
      function success(result) {
        ctrl.searchState = STATE.IDLE;
        if (result.shipments.length === 0) {
          ctrl.isNoMoreResult = true;
          return;
        }
        const shipments = _.map(result.shipments, (o) => {
          o.comments_f = o.comments ? $filter('limitTo')(o.comments, 50) : '-';
          return o;
        });
        ctrl.tableParam.setData(shipments);
        ctrl.showFilter = false;
        ctrl.page += 1;
        ctrl.totalItems = result.count;
      }
    }


    /**
     * return a promise for searching
     * @returns {*}
     */
    function loadShipmentPromise() {
      if (isFilterNonEmpty()) {
        // find 'em
        const params = nvBackendFilterUtils.constructParams(ctrl.selectedFilters);
        const dateParams = _.pickBy(params, (val,key) => {
          return key.endsWith('date');
        });
        // remove it
        const params2 = _.omitBy(params, (val,key) => {
          return key.endsWith('date');
        });
        _.each(dateParams, (val, key) => {
          const arr = _.split(key, '_');
          // the pattern is <from/to>_<field_name>_date
          if (_.isUndefined(params2[arr[1]])) {
            params2[arr[1]] = {};
          }
          params2[arr[1]][arr[0]] = val;
        });
        return Shipping.searchPagev1(
          params2, ctrl.itemsPerPage, ctrl.page
        );
      }
      // find all
      return Shipping.getAllPage(ctrl.itemsPerPage, ctrl.page);
    }

    function loadShipment() {
      if (ctrl.tableParam.getUnfilteredLength() < ctrl.totalItems && !ctrl.isNoMoreResult) {
        return loadShipmentPromise().then(extend);
      }
      return $q.reject();

      function extend(result) {
        if (result.shipments.length === 0) {
          ctrl.isNoMoreResult = true;
          return [];
        }
        const shipments = _.map(_.castArray(result.shipments), (o) => {
          o.comments_f = o.comments ? $filter('limitTo')(o.comments, 50) : '-';
          return o;
        });
        ctrl.loadState = 'ready';
        ctrl.page += 1;
        return shipments;
      }
    }


    function loadMore() {
      if (ctrl.state !== 'loading') {
        ctrl.loadState = 'loading';
        return ctrl.tableParam.mergeInPromise(loadShipment(), 'id');
      }
      return null;
    }


    function showDetails($event, shipment) {
      $window.open(
        $state.href('container.shipment-details', { shipmentId: shipment.id })
      );
    }

    function generateReports(event, datas) {
      // to avoid translation file not loaded when the variable created
      // translation nvTranslate called on onLoad method
      REPORT_META = {
        HEADER: [
          nvTranslate.instant('container.shipment-management.shipment-type'),
          nvTranslate.instant('container.shipment-management.shipment-id'),
          nvTranslate.instant('container.shipment-management.creation-date-time'),
          nvTranslate.instant('container.shipment-management.transit-date-time'),
          nvTranslate.instant('container.shipment-management.status'),
          nvTranslate.instant('container.shipment-management.start-hub-title'),
          nvTranslate.instant('container.shipment-management.last-inbound-hub-title'),
          nvTranslate.instant('container.shipment-management.end-hub-title'),
          nvTranslate.instant('container.shipment-management.eta-date-time'),
          nvTranslate.instant('container.shipment-management.completion-date-time'),
          nvTranslate.instant('container.shipment-management.total-parcels'),
          nvTranslate.instant('container.shipment-management.total-scanned'),
          nvTranslate.instant('container.shipment-management.total-missing'),
          nvTranslate.instant('container.shipment-management.total-received'),
          nvTranslate.instant('container.shipment-management.total-extra-parcels'),
          nvTranslate.instant('container.shipment-management.total-removed'),
          nvTranslate.instant('container.shipment-management.comments'),
          nvTranslate.instant('container.shipment-management.mawb'),
        ],
        FIELDS: [
          'shipment_type',
          'shipment_id',
          'created_at',
          'transit_at',
          'status',
          'orig_hub',
          'curr_hub',
          'dest_hub',
          'arrival_datetime',
          'completed_at',
          'total_parcels',
          'total_scanned',
          'total_missing',
          'total_received',
          'total_extra',
          'total_removed',
          'comments',
          'mawb',
        ],
      };

      const shipmentIds = _.map(datas, data => (data.id));
      return Shipping.getShipmentReports(shipmentIds).then(downloadReport);

      function downloadReport(reportData) {
        const reportContent = createCsvData(reportData.shipments);
        const fileName = 'Shipment Report.xlsx';
        return Excel.write(reportContent, fileName);

        function createCsvData(data) {
          const nData = [];
          nData.push(REPORT_META.HEADER);
          _.each(data, (d) => {
            const row = _.map(REPORT_META.FIELDS, f => (_.get(d, f, '-')));
            nData.push(row);
          });
          return nData;
        }
      }
    }

    /**
     * create ticket function function
     * @param $event
     */
    function createOne($event) {
      const defer = $q.defer();
      nvDialog.showSingle($event, {
        templateUrl: 'views/container/shipment-management/dialog/create-shipment/create-shipment.html',
        locals: {
          hubs: ctrl.activeHubs,
          mode: 'create',
          shipment: null,
          shipmentTypes: ctrl.shipmentTypes,
        },
        theme: 'nvGreen',
        cssClass: 'shipment-add',
        controllerAs: 'ctrl',
        controller: 'ShipmentDialogController',
      }).then(success, defer.reject);
      function success(result) {
        let shipment = result.shipment;
        shipment.comments_f = shipment.comments ? $filter('limitTo')(shipment.comments, 50) : '-';
        shipment = attachHubsName(shipment);
        if (ctrl.tableParam && ctrl.tableParam.ready) {
          ctrl.tableParam.mergeIn(shipment, 'id');
        }
        defer.resolve(result);
      }

      return defer.promise;
    }

    function attachHubsName(shipment) {
      // find startHubName
      _.forEach(ctrl.hubs, (o) => {
        if (o.value === shipment.orig_hub_id
            && o.country === shipment.orig_hub_country) {
          shipment.orig_hub_name = o.displayName;
        }

        if (o.value === shipment.curr_hub_id
            && o.country === shipment.curr_hub_country) {
          shipment.curr_hub_name = o.displayName;
        }

        if (o.value === shipment.dest_hub_id
            && o.country === shipment.dest_hub_country) {
          shipment.dest_hub_name = o.displayName;
        }
      });
      return shipment;
    }

    /**
     * will popup edit dialog
     * @param $event
     * @param nShipment
     */
    function updateOne($event, nShipment) {
      const defer = $q.defer();
      nvDialog.showSingle($event, {
        templateUrl: 'views/container/shipment-management/dialog/create-shipment/create-shipment.html',
        locals: {
          hubs: ctrl.activeHubs,
          mode: 'edit',
          shipment: nShipment,
          shipmentTypes: ctrl.shipmentTypes,
        },
        theme: 'nvBlue',
        cssClass: 'shipment-edit',
        controllerAs: 'ctrl',
        controller: 'ShipmentDialogController',
      }).then(success, defer.reject);
      function success(result) {
        nvToast.info(
          nvTranslate.instant(
            'container.shipment-management.success-update',
            { shipmentId: result.shipment.id }
        ));
        let shipment = result.shipment;
        shipment.comments_f = shipment.comments ? $filter('limitTo')(shipment.comments, 50) : '-';
        shipment = attachHubsName(shipment);
        ctrl.tableParam.mergeIn(shipment, 'id');
        defer.resolve(result);
      }
      return defer;
    }


    /**
     * delete operation
     * @param $event
     * @param nShipmentId
     */
    function deleteOne($event, shipment) {
      // show delete confirmation
      const nShipmentId = shipment.id;
      nvDialog.confirmDelete($event, {
        title: nvTranslate.instant('container.shipment-management.confirm-cancel'),
        content: nvTranslate.instant(
          'container.shipment-management.confirm-cancel-shipment',
          { id: nShipmentId }
        ),
        ok: nvTranslate.instant('container.shipment-management.cancel-shipment'),
      }).then(onDelete);

      function onDelete() {
        const payload = {
          shipment: {
            status: 'Cancelled',
          },
        };
        return Shipping.update(payload, nShipmentId)
                  .then(success);
      }

      function success(result) {
        nvToast.info(nvTranslate.instant(
          'container.shipment-management.success-change-status',
          { shipmentId: nShipmentId }
        ));
        ctrl.tableParam.mergeIn(attachHubsName(_.get(result, 'shipment')), 'id');
      }
    }


    function printAwb(shipmentId) {
      return Shipping.shipmentAwb(shipmentId);
    }


    function savePreset(data) {
      return ShippingFilterTemplate.create(data).then(response => response.template);
    }

    function deletePreset(id) {
      return ShippingFilterTemplate.deleteFilter(id);
    }

    function updatePreset(id, body) {
      return ShippingFilterTemplate.update(id, body).then(response => response.template);
    }

    function shipmentComparator(object, filterObj) {
      return _.lowerCase(object.displayName) === _.lowerCase(filterObj);
    }

    function hubComparator(object, filterObj) {
      return filterObj.system_id === object.country && filterObj.id === object.value;
    }
  }
}());
