(function controller() {
  angular.module('nvOperatorApp.controllers')
        .controller('ShipmentDetailsController', ShipmentDetailsController);
  ShipmentDetailsController.$inject = ['$q', 'Shipping', 'Order',
    '$mdDialog', '$stateParams', 'Excel', 'nvTranslate', 'nvTable'];
  function ShipmentDetailsController($q, Shipping, Order, $mdDialog,
    $stateParams, Excel, nvTranslate, nvTable) {
    const ctrl = this;
    let shipment = null;
    ctrl.shipmentId = $stateParams.shipmentId;
    ctrl.onLoad = onLoad;
    ctrl.onCancel = onCancel;
    ctrl.downloadReport = downloadReport;
    ctrl.orders = [];
    ctrl.extraOrders = [];
    ctrl.scans = [];
    ctrl.shipmentScans = [];
    ctrl.orderz = [];
    ctrl.tableParamMissingOrders = {};
    ctrl.tableParamExtraScans = {};
    ctrl.tableParamOrdersRemoved = {};
    ctrl.tableParamOrders = {};
    ctrl.tableParamScans = {};
    ctrl.mappedOrders = {};
    ctrl.shipmentDetailsPromise = null;
    /**
     * source: { displayName: nvTranslate.instant('container.shipment-management.source') },
        result: { displayName: nvTranslate.instant('container.shipment-management.result') },
        hub: { displayName: nvTranslate.instant('commons.hub') },
        created_at: { displayName: nvTranslate.instant('commons.created-at') },
     */

    function onLoad() {
      const orderIds = [];
      let tableParamOrders = [];
      let tableParamOrdersRemoved = [];
      let tableParamMissingOrders = [];
      ctrl.shipmentDetailsPromise = Shipping.getDetail(ctrl.shipmentId)
      return ctrl.shipmentDetailsPromise.then((result) => {
        ctrl.tableParamMissingOrders = nvTable.createTable(Shipping.DETAILS_FIELDS.MISSING);
        ctrl.tableParamExtraScans = nvTable.createTable(Shipping.DETAILS_FIELDS.EXTRA);
        ctrl.tableParamOrdersRemoved = nvTable.createTable(Shipping.DETAILS_FIELDS.REMOVED);
        ctrl.tableParamOrders = nvTable.createTable(Shipping.DETAILS_FIELDS.ORDERS);
        ctrl.tableParamScans = nvTable.createTable(Shipping.DETAILS_FIELDS.SCANS);
        shipment = result.shipment;
        tableParamOrders = _.map(shipment.orders, (o) => {
          orderIds.push(o.order_id);
          return {
            order_id: o.order_id,
          };
        });


        tableParamMissingOrders = _.map(shipment.scans.missing_scans, (o) => {
          orderIds.push(o.order_id);
          return {
            tracking_id: o.scan_value,
            order_id: o.order_id,
            last_hub: o.hub_name,
            created_at: o.created_at,
          };
        });

        tableParamOrdersRemoved = _.map(shipment.scans.removed_scans, (o) => {
          orderIds.push(o.order_id);
          return {
            tracking_id: o.scan_value,
            order_id: o.order_id,
            last_hub: o.hub_name,
            created_at: o.created_at,
          };
        });
        ctrl.tableParamExtraScans.setData(_.map(shipment.scans.extra_scans,
          o => ({ tracking_id: o.scan_value, last_hub: o.hub_name, last_scan: o.created_at })));
        ctrl.tableParamScans.setData(_.map(shipment.scans.shipment_scans,
          o => ({ source: o.source,
                  user_id: o.user_id,
                  result: o.result,
                  hub: o.hub_name,
                  created_at: o.created_at })
        ));
        return Order.getTrackingIds(orderIds).then(onSuccessfetchOrder);

        function onSuccessfetchOrder(orderResult) {
          _.each(orderResult, (o) => {
            ctrl.mappedOrders[o.id] = o;
          });
          const scans = ctrl.tableParamScans.getTableData();

          ctrl.tableParamOrders.setData(_.map(tableParamOrders, (o) => {
            const order = ctrl.mappedOrders[o.order_id];
            const filteredScans = _.filter(scans, { order_id: o.order_id });
            let obj = null;
            if (order) {
              obj = {
                tracking_id: order.trackingId,
                granular_status: order.granularStatus,
                hub_name: '',
                created_at: '',
              };
            } else {
              obj = {
                tracking_id: searchTrackingIdFromScans(o.order_id),
                granular_status: 'NOT FOUND',
                hub_name: '',
                created_at: '',
              };
            }
            if (filteredScans.length > 0) {
              // get the last one and put it into order obj
              const orderScan = _.orderBy(filteredScans, 'created_at', 'desc')[0];
              obj.hub_name = orderScan.hub_name || null;
              obj.created_at = orderScan.created_at || null;
            }
            return obj;
          }));

          ctrl.tableParamOrdersRemoved.setData(_.map(tableParamOrdersRemoved, (o) => {
            const order = ctrl.mappedOrders[o.order_id] || {};
            o.granular_status = order.granularStatus || '-';
            return o;
          }));


          ctrl.tableParamMissingOrders.setData(_.map(tableParamMissingOrders, (o) => {
            const order = ctrl.mappedOrders[o.order_id] || {};
            o.granular_status = order.granularStatus || '-';
            return o;
          }));
        }
      });
    }


    function onCancel() {
      $mdDialog.cancel();
    }

    /**
     * search tracking id in scans by order id
     * return '-' if find nothing
     * @param {*} orderId
     */
    function searchTrackingIdFromScans(orderId) {
      const scansData = shipment.scans;
      const scans = _.union(scansData.extra_scans,
                      scansData.missing_scans,
                      scansData.removed_scans);
      const scan = _.find(scans, { order_id: orderId });
      if (scan) {
        return scan.scan_value;
      }
      return '-';
    }


    function downloadReport() {
      const CSV_HEADERS = {
        orders: [
          nvTranslate.instant('container.shipment-management.tracking-id'),
          nvTranslate.instant('container.shipment-management.granular-status'),
          nvTranslate.instant('container.shipment-management.last-hub'),
          nvTranslate.instant('container.shipment-management.last-scan'),
        ],
        missing: [
          nvTranslate.instant('container.shipment-management.tracking-id'),
          nvTranslate.instant('container.shipment-management.granular-status'),
          nvTranslate.instant('container.shipment-management.last-hub'),
          nvTranslate.instant('container.shipment-management.last-scan'),
        ],
        removed: [
          nvTranslate.instant('container.shipment-management.tracking-id'),
          nvTranslate.instant('container.shipment-management.granular-status'),
          nvTranslate.instant('container.shipment-management.last-hub'),
          nvTranslate.instant('container.shipment-management.last-scan'),
        ],
        'extra-scans': [
          nvTranslate.instant('container.shipment-management.tracking-id'),
          nvTranslate.instant('container.shipment-management.last-hub'),
          nvTranslate.instant('container.shipment-management.last-scan'),
        ],
        'shipment-scans': [
          nvTranslate.instant('container.shipment-management.shipment-id'),
          nvTranslate.instant('container.shipment-management.source'),
          nvTranslate.instant('container.shipment-management.result'),
          nvTranslate.instant('container.shipment-management.hub-name'),
          nvTranslate.instant('commons.created-at'),
        ],
      };
      const CSV_FIELDS = {
        orders: ['tracking_id', 'granular_status', 'hub_name', 'created_at'],
        removed: ['tracking_id', 'granular_status', 'hub_name', 'created_at'],
        missing: ['tracking_id', 'granular_status', 'hub_name', 'created_at'],
        'extra-scans': ['tracking_id', 'last_hub', 'last_scan'],
        'shipment-scans': ['scan_value', 'source', 'result', 'hub_name', 'created_at'],
      };
      const datas = [createCsvData(ctrl.tableParamMissingOrders.getTableData(), 'missing'),
                            createCsvData(ctrl.tableParamExtraScans.getTableData(), 'extra-scans'),
                            createCsvData(ctrl.tableParamOrdersRemoved.getTableData(), 'removed'),
                            createCsvData(ctrl.tableParamOrders.getTableData(), 'orders'),
                            createCsvData(ctrl.tableParamScans.getTableData(), 'shipment-scans'),
                        ];
      const sheetNames = [nvTranslate.instant('container.shipment-management.missing-orders'),
                                nvTranslate.instant('container.shipment-management.extra-scans'),
                                nvTranslate.instant('container.shipment-management.orders-removed-from-shipment'),
                                nvTranslate.instant('container.shipment-management.current-orders'),
                                nvTranslate.instant('container.shipment-management.shipment-scan')];
      const fileName = `Shipment Scans Report ID: ${ctrl.shipmentId}.xlsx`;
      return Excel.writeMultiSheet(datas, sheetNames, fileName);

      function createCsvData(data, tableType) {
        const nDatas = [];
        nDatas.push(CSV_HEADERS[tableType]);
        _.each(data, (d) => {
          const row = _.map(CSV_FIELDS[tableType], f => (_.get(d, f, '-')));
          nDatas.push(row);
        });
        return nDatas;
      }
    }
    onLoad();
  }
}());
