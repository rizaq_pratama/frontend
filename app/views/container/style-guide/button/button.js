(function controller() {
  angular
    .module('nvOperatorApp.controllers')
    .controller('ButtonDemoController', ButtonDemoController);

  ButtonDemoController.$inject = ['$window', 'nvDialog', 'nvEditModelDialog'];
  function ButtonDemoController(nvDialog, nvEditModelDialog) {
    const ctrl = this;
    ctrl.disabled = false;

    ctrl.openDialog = openDialog;
    ctrl.openDialogDark = openDialogDark;

    function openDialog($event) {
      nvDialog.showSingle($event, {
        templateUrl: 'views/container/style-guide/dialog/input.dialog.html',
        cssClass: 'demo-dialog',
        theme: 'nvGreen',
        scope: _.assign(nvEditModelDialog.scopeTemplate({}), { fields: {} }),
      });
    }
    function openDialogDark($event) {
      nvDialog.showSingle($event, {
        templateUrl: 'views/container/style-guide/dialog/input-dark.dialog.html',
        cssClass: 'demo-dialog',
        theme: 'nvBlue',
        scope: _.assign(nvEditModelDialog.scopeTemplate({}), { fields: {} }),
      });
    }
  }
}());