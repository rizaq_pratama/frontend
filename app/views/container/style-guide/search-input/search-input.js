(function controller() {
  angular
    .module('nvOperatorApp.controllers')
    .controller('SearchInputDemoController', SearchInputDemoController);

  SearchInputDemoController.$inject = ['nvAutocomplete.Data', 'nvURL', 'Shippers', '$window', '$rootScope'];
  function SearchInputDemoController(nvAutocompleteData, nvURL, Shippers, $window, $rootScope) {
    const ctrl = this;
    ctrl.disabled = false;

    ctrl.searchProgressive = '';
    ctrl.searchProgressiveInput = nvAutocompleteData.getByHandle('progressive');
    ctrl.searchProgressiveInput.setPossibleOptions(getShippers());

    ctrl.searchProgressiveDark = '';
    ctrl.searchProgressiveInputDark = nvAutocompleteData.getByHandle('progressive-dark');
    ctrl.searchProgressiveInputDark.setPossibleOptions(getShippers());

    ctrl.shipperSearchText = '';
    ctrl.shipperSearchInput = nvAutocompleteData.getByHandle('reservations-shipper');
    ctrl.shipperSearchInput.setPossibleOptions(getShippers());

    ctrl.shipperSearchTextDark = '';
    ctrl.shipperSearchInputDark = nvAutocompleteData.getByHandle('reservations-shipper-dark');
    ctrl.shipperSearchInputDark.setPossibleOptions(getShippers());

    ctrl.goto = (shipper) => {
      $window.open(`${nvURL.buildV1Url(nv.config.env, $rootScope.domain)}/ng#/shippers/${shipper.id}`, '_blank');
    };

    function getShippers() {
      return Shippers.elasticReadAll().then(response => response);
    }
  }
}());
