(function controller() {
  angular
    .module('nvOperatorApp.controllers')
    .controller('TableDemoController', Controller);

  Controller.$inject = ['nvTable', 'Driver', 'nvExtendUtils', '$log'];

  function Controller(nvTable, Driver, nvExtendUtils, $log) {
    const ctrl = this;

    ctrl.tableParam = nvTable.createTable(Driver.FIELDS);
    ctrl.tableParam.addColumn('name', { displayName: 'Name' });
    ctrl.tableParam.addColumn('availability', { displayName: 'Coming' });

    // can use setData or setDataPromise
    ctrl.tableParam.setDataPromise(Driver.searchAll().then(extend));

    // override with custom sort function
    ctrl.tableParam.setSortFunction('vehicles.vehicleType', (drivers, order) => {
      return _.orderBy(drivers, [sortVehicles, 'id'], order);

      function sortVehicles(data) {
        return _(data.vehicles)
          .map('vehicleType')
          .orderBy(_.identity)
          .value()
          .join(' ');
      }
    });

    // column should already have filter functions applied,
    // it will filter based on property value
    // update column filter, if you need custom filter function
    ctrl.tableParam.updateColumnFilter('vehicles.vehicleType', (strFn) => (
      (driver) => {
        // strfn is the function that will return you the search text
        const lowercase = strFn().toLowerCase();
        return lowercase.length === 0 || _.some(driver.vehicles, vehicle =>
          vehicle.vehicleType && vehicle.vehicleType.toLowerCase().indexOf(lowercase) >= 0
        );    
      }
    ));

    // button function
    ctrl.deleteOne = (event, driver) => {
      $log.log(event);
      $log.log(JSON.stringify(driver));
    };

    function extend(result) {
      return _.map(result.drivers, (driver) => {
        _.forEach(driver.vehicles, (vehicle) => {
          _.merge(vehicle, { _ownVehicle: vehicle.ownVehicle === '10' });
        });

        driver.vehicles = nvExtendUtils.addValueForBoolean(driver.vehicles, '_ownVehicle', { true: 'Yes', false: 'No' });
        driver.name = `${driver.firstName} ${driver.lastName}`;
        return driver;
      });
    }
  }
}());
