(function controller() {
  angular
    .module('nvOperatorApp.controllers')
    .controller('FilterDemoController', Controller);

  Controller.$inject = ['Shippers', 'Hub', 'Driver'];
  function Controller(Shippers, Hub, Driver) {
    const ctrl = this;
    ctrl.selectedFilters = [];
    ctrl.possibleFilters = [{
      type: 1,
      mainTitle: 'Route Creation Date',
      fromModel: new Date(),
      toModel: new Date(),
    }, {
      type: 0,
      mainTitle: 'Archived Routes',
      booleanModel: '10',
    }, {
      type: 2,
      mainTitle: 'Hub',
      possibleOptions: Hub.read(),
      selectedOptions: [],
      searchBy: 'name',
      searchText: {},
    }, {
      type: 8,
      mainTitle: 'Shipper',
      callback: getShippers,
      selectedOptions: [],
      includeModel: [{ displayName: 'Include' }, { displayName: 'Exclude' }],
    }, {
      type: 2,
      mainTitle: 'Driver',
      possibleOptions: Driver.searchAll().then(response => response.drivers),
      selectedOptions: [],
      searchBy: 'firstName',
      searchText: {},
    },
    ];

    ctrl.onClick = () => console.log(ctrl.selectedFilters);

    function getShippers(text) {
      return Shippers.filterSearch(text, this.getSelectedOptions());
    }
  }
}());