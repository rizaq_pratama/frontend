(function controller() {
  angular
    .module('nvOperatorApp.controllers')
    .controller('ZonalRoutingRoutingEditController', ZonalRoutingRoutingController);

  ZonalRoutingRoutingController.$inject = [
    'nvZonalRouting', 'appSidenav', '$scope', 'nvNavGuard', '$timeout',
    'Driver', 'nvMaps', 'nvDialog', 'nvDateTimeUtils', 'nvToast',
    'nvTranslate', 'Route', '$rootScope', 'Waypoint', '$stateParams',
    '$state', '$compile', 'nvPanel', 'nvCurrency', 'hotkeys',
    'nvAddress', 'Order', '$window', 'nvModelUtils', 'nvAutocomplete.Data',
    '$mdMedia', 'nvBackendFilterUtils', 'Timewindow', 'Shippers', 'Reservation',
    '$q', 'Tag',
  ];

  function ZonalRoutingRoutingController(
    nvZonalRouting, appSidenav, $scope, nvNavGuard, $timeout,
    Driver, nvMaps, nvDialog, nvDateTimeUtils, nvToast,
    nvTranslate, Route, $rootScope, Waypoint, $stateParams,
    $state, $compile, nvPanel, nvCurrency, hotkeys,
    nvAddress, Order, $window, nvModelUtils, nvAutocompleteData,
    $mdMedia, nvBackendFilterUtils, Timewindow, Shippers, Reservation,
    $q, Tag
  ) {
    // variables
    const POLYGON_TOOLS_ACTION = {
      ADD: {
        icon: 'add',
        shortText: 'commons.add',
        text: 'container.zonal-routing.add-to-route',
      },
      REMOVE: {
        icon: 'remove',
        shortText: 'commons.remove',
        text: 'container.zonal-routing.remove-from-route',
      },
    };

    const toCluster = $stateParams.cluster === 'true';
    const routeIds = _.map(_.compact(_.split($stateParams.ids, ',')), _.toInteger);
    const zoneIds = _.map(_.compact(_.split($stateParams.zones, ',')), _.toInteger);
    const fetchUnroutedWaypoints = $stateParams.unrouted === 'true';
    let resequencedTagId = 0;

    let drawControl = null;
    let featureGroup = null;
    let orphanPolygonLayers = [];
    let moveendTimer = null;
    let loadingMarkers = false;
    let isMoveendTimerTriggerring = false;
    let routes = [];
    let drivers = [];
    let driversSelectionOptions = [];

    const parentContentLoading = $scope.contentLoading;
    const hotkeyEvents = {
      isWaypointDetailsTableLocked: false,
      hoveredMarker: null,
    };

    // variables (that also use in html)
    const ctrl = this;

    ctrl.Order = Order;
    ctrl.POLYGON_TOOLS_ACTION = POLYGON_TOOLS_ACTION;
    ctrl.waypointsDndAllowedTypes = ['waypoint'];

    ctrl.map = null;
    ctrl.waypointDetailsTableParams = {
      isTransactionWp: true,
      list: [],
      waypointId: null,
      sizeCounter: null,
      address: null,
    };
    ctrl.hotkeyEvents = hotkeyEvents;

    ctrl.assignedRoutesSelectionOptions = [];
    ctrl.selectionActiveRouteId = 0;
    ctrl.activeRoute = null;
    ctrl.activeRouteId = 0;
    ctrl.activeRoutePolygonData = null;

    ctrl.routeStyling = {
      backgroundColor: routeBackgroundColorStyling,
      borderColor: routeBorderColorStyling,
    };

    ctrl.routingFilters = {
      selectedFilters: [],
      possibleFilters: [],
      filterMode: nvModelUtils.fromBoolToYesNoBitString(true),
      conditionCheckFn: {
        deliveryType: deliveryTypeConditionCheckFn,
        parcelSize: parcelSizeConditionCheckFn,
        priorityLevel: priorityLevelConditionCheckFn,
        timewindow: timewindowConditionCheckFn,
        shipper: shipperConditionCheckFn,
        approxVolumn: approxVolumeConditionCheckFn,
        cash: codConditionCheckFn,
        searchComments: searchCommentsCheckFn,
      },
    };

    // functions
    // Top panel
    ctrl.discardChanges = discardChanges;
    ctrl.preSubmitRoutes = preSubmitRoutes;

    // Map panel / Bottom panel
    ctrl.toggleIsWaypointDetailsTableLocked = toggleIsWaypointDetailsTableLocked;
    ctrl.navigateToEditOrderPage = navigateToEditOrderPage;
    ctrl.getParcelSizeShortName = getParcelSizeShortName;

    // Right Panel
    ctrl.filterMarkerSubmit = filterMarkerSubmit;
    ctrl.activeRouteSelectionChanged = activeRouteSelectionChanged;
    ctrl.setActiveRoute = setActiveRoute;
    ctrl.unsetActiveRoute = unsetActiveRoute;
    ctrl.openRouteMenu = openRouteMenu;
    ctrl.hasPolygon = hasPolygon;
    ctrl.openPolygonToolsActionMenu = openPolygonToolsActionMenu;
    ctrl.polygonToolsPerformAction = polygonToolsPerformAction;
    ctrl.deleteAllPolygons = deleteAllPolygons;
    ctrl.addRelated = addRelated;

    // Route List Panel
    ctrl.deleteRoute = deleteRoute;
    ctrl.resetRouteMarkers = resetRouteMarkers;
    ctrl.dndLabelOnClose = dndLabelOnClose;
    ctrl.refreshWaypointOnDrop = refreshWaypointOnDrop;

    // start
    initialize();
    $scope.$on('$destroy', () => {
      appSidenav.setConfigLockedOpen(true);
    });

    // functions details
    function initialize() {
      nvZonalRouting.initializeVariables();

      // validation
      if (_.size(routeIds) <= 0) {
        goToRouteLogsPage();
        return;
      }

      // prepare
      hotkeys.add({
        combo: 'l',
        description: 'To lock/unlock waypoint-details-table',
        callback: toggleIsWaypointDetailsTableLocked,
      });

      const filters = initFilter();
      ctrl.routingFilters.possibleFilters = filters.generalFilters;
      ctrl.routingFilters.selectedFilters = _.remove(ctrl.routingFilters.possibleFilters,
          filter => filter.showOnInit === true
      );

      $timeout(() => {
        if (!$mdMedia('gt-lg')) {
          // only hide sidebar if screen size < 1920
          appSidenav.setConfigLockedOpen(false);
        }

        plot();
      });
    }

    function initFilter() {
      const generalFilters = [
        {
          type: nvBackendFilterUtils.FILTER_OPTIONS,
          showOnInit: true,
          isMandatory: true,
          possibleOptions: getDeliveryTypes(),
          selectedOptions: [],
          searchText: {},
          mainTitle: 'commons.delivery-type',
          key: 'deliveryType',
          presetKey: 'deliveryType',
        },
        {
          type: nvBackendFilterUtils.FILTER_OPTIONS,
          showOnInit: true,
          isMandatory: true,
          possibleOptions: getTimewindows(),
          selectedOptions: [],
          sortBy: 'index',
          searchText: {},
          mainTitle: 'container.zonal-routing.timeslots',
          key: 'timewindow',
          presetKey: 'timewindow',
        },
        {
          type: nvBackendFilterUtils.FILTER_OPTIONS,
          showOnInit: true,
          isMandatory: true,
          possibleOptions: getApproxVolumns(),
          selectedOptions: [],
          searchText: {},
          mainTitle: 'container.zonal-routing.reservation-size',
          key: 'approxVolumn',
          presetKey: 'approxVolumn',
        },
        {
          type: nvBackendFilterUtils.FILTER_OPTIONS,
          showOnInit: true,
          isMandatory: true,
          possibleOptions: Order.PARCEL_SIZE,
          selectedOptions: [],
          sortBy: 'id',
          searchText: {},
          mainTitle: 'commons.parcel-size',
          key: 'parcelSize',
          presetKey: 'parcelSize',
        },
        {
          type: nvBackendFilterUtils.FILTER_AUTOCOMPLETE,
          showOnInit: true,
          isMandatory: true,
          callback: getShippers,
          backendKey: 'legacy_ids',
          selectedOptions: [],
          mainTitle: 'commons.shipper',
          key: 'shipper',
          presetKey: 'shipper',
        },
        {
          type: nvBackendFilterUtils.FILTER_OPTIONS,
          showOnInit: true,
          isMandatory: true,
          possibleOptions: getPriorityLevels(),
          selectedOptions: [],
          sortBy: 'index',
          searchText: {},
          mainTitle: 'commons.model.priority-level',
          key: 'priorityLevel',
          presetKey: 'priorityLevel',
        },
        {
          type: nvBackendFilterUtils.FILTER_BOOLEAN,
          showOnInit: true,
          isMandatory: true,
          mainTitle: 'container.zonal-routing.cash-cod-cop',
          trueLabel: '{{ \'commons.yes\' | translate }}',
          falseLabel: '{{ \'commons.no\' | translate }}',
          booleanModel: nvModelUtils.fromBoolToYesNoBitString(true),
          key: 'cash',
          presetKey: 'cash',
        },
        {
          type: nvBackendFilterUtils.FILTER_TEXT,
          showOnInit: true,
          isMandatory: true,
          mainTitle: 'commons.comments',
          placeholder: 'container.zonal-routing.search-comments-hint',
          model: null,
          key: 'searchComments',
          presetKey: 'searchComments',
        },
      ];

      return {
        generalFilters: generalFilters,
      };

      function getDeliveryTypes() {
        return [
          {
            displayName: 'commons.model.pickup',
            verifyKey: 'isReservation',
          },
          {
            displayName: 'container.zonal-routing.return-pickup',
            verifyKey: 'hasReturnPickup',
          },
          {
            displayName: 'container.zonal-routing.c2c',
            verifyKey: 'hasC2C',
          },
          {
            displayName: 'container.zonal-routing.dp',
            verifyKey: 'hasDP',
          },
          {
            displayName: 'container.zonal-routing.dp-reservation',
            verifyKey: 'hasDPReservation',
          },
          {
            displayName: 'container.zonal-routing.bulk-move-quantity',
            verifyKey: 'hasBulkMoveQty',
          },
          {
            displayName: 'container.zonal-routing.rescheduled',
            verifyKey: 'hasReschedule',
          },
          {
            displayName: 'container.zonal-routing.have-route-comments',
            verifyKey: 'hasRouteComments',
          },
          {
            displayName: 'container.zonal-routing.sameday',
            verifyKey: 'hasSameday',
          },
        ];
      }

      function getPriorityLevels() {
        return [
          {
            index: 0,
            displayName: 'container.zonal-routing.non-priority-tag',
            min: null,
            max: 0,
          },
          {
            index: 1,
            displayName: 'container.zonal-routing.low-priority-tag',
            min: 1,
            max: 3,
          },
          {
            index: 2,
            displayName: 'container.zonal-routing.mid-priority-tag',
            min: 4,
            max: 6,
          },
          {
            index: 3,
            displayName: 'container.zonal-routing.high-priority-tag',
            min: 7,
            max: null,
          },
        ];
      }

      function getTimewindows() {
        const allDayList = [];
        const daySlotList = [];
        const nightSlotList = [];
        const timeslotList = [];
        _.forEach(Timewindow.TIMEWINDOWS, (timewindow) => {
          switch (timewindow.type) {
            case Timewindow.TYPE.NIGHTSLOT:
              nightSlotList.push(
                generateTimewindowItem(timewindow, 'container.zonal-routing.night-slot')
              );
              break;
            case Timewindow.TYPE.DAYSLOT:
              daySlotList.push(
                generateTimewindowItem(timewindow, 'container.zonal-routing.day-slot')
              );
              break;
            case Timewindow.TYPE.DAYNIGHTSLOT:
              allDayList.push(generateTimewindowItem(timewindow, 'container.zonal-routing.all-day'));
              break;
            case Timewindow.TYPE.TIMESLOT:
              timeslotList.push(generateTimewindowItem(timewindow));
              break;
            default:
              allDayList.push(generateTimewindowItem(timewindow));
          }
        });

        let timewindows = _.concat(allDayList, daySlotList, nightSlotList, timeslotList);
        timewindows = _.map(timewindows, (timewindow, index) => {
          timewindow.index = index;
          return timewindow;
        });

        return timewindows;

        function generateTimewindowItem(timewindow, customLabel = null) {
          return {
            displayName: customLabel || timewindow.name,
            timewindowId: timewindow.id,
          };
        }
      }

      function getShippers(text) {
        return Shippers.filterSearch(
          text,
          _.isUndefined(this.getSelectedOptions) ? [] : this.getSelectedOptions()
        );
      }

      function getApproxVolumns() {
        return _.orderBy(_.map(Reservation.volumeOptions, volumeOption =>
            generateApproxVolumeItem(volumeOption)
        ), 'displayName', 'asc');

        function generateApproxVolumeItem(volumeOption) {
          return {
            displayName: volumeOption,
            value: volumeOption,
          };
        }
      }
    }

    function codConditionCheckFn(marker, filter) {
      return nvModelUtils.fromYesNoBitStringToBool(
          filter.booleanModel
        ) === marker.options.extraInfo.hasCOD;
    }

    function timewindowConditionCheckFn(marker, filterItem) {
      return filterItem.timewindowId === marker.options.extraInfo.timewindowId;
    }

    function parcelSizeConditionCheckFn(marker, filterItem) {
      if (marker.options.extraInfo.isReservation) {
        return false;
      }

      let isCorrectParcelSize = false;
      _.forEach(marker.options.transactions, (transaction) => {
        if (transaction.order && transaction.order.parcelSize === filterItem.enumValue) {
          isCorrectParcelSize = true;
          return false;
        }

        return undefined;
      });

      return isCorrectParcelSize;
    }

    function approxVolumeConditionCheckFn(marker, filterItem) {
      if (!marker.options.extraInfo.isReservation) {
        return false;
      }

      let isCorrectApproxVolume = false;
      _.forEach(marker.options.transactions, (reservation) => {
        if (reservation.approxVolume === filterItem.value) {
          isCorrectApproxVolume = true;
          return false;
        }

        return undefined;
      });

      return isCorrectApproxVolume;
    }

    function priorityLevelConditionCheckFn(marker, filterItem) {
      let isCorrectCondition = true;

      if (filterItem.min !== null) {
        isCorrectCondition = filterItem.min <= marker.options.extraInfo.priorityLevel;
      }

      if (isCorrectCondition && filterItem.max !== null) {
        isCorrectCondition = marker.options.extraInfo.priorityLevel <= filterItem.max;
      }

      return isCorrectCondition;
    }

    function deliveryTypeConditionCheckFn(marker, filterItem) {
      return marker.options.extraInfo[filterItem.verifyKey];
    }

    function shipperConditionCheckFn(marker, filterItem) {
      let isPartOfSelected = false;
      const shipperIds = _.castArray(filterItem.id);
      _.forEach(marker.options.transactions, (transaction) => {
        let shipperId = 0;
        if (marker.options.extraInfo.isReservation) {
          shipperId = transaction.shipperId;
        } else {
          shipperId = transaction.order && transaction.order.shipperId;
        }

        if (shipperIds.indexOf(shipperId) >= 0) {
          isPartOfSelected = true;
          return false;
        }

        return undefined;
      });

      return isPartOfSelected;
    }

    function searchCommentsCheckFn(marker, filter) {
      const wording = _.toLower(_.trim(filter.model));
      if (!wording) {
        return true;
      }

      let isMatch = false;
      if (Waypoint.isTransaction(marker.options.waypoint)) {
        _.forEach(marker.options.transactions, (txn) => {
          const comments = _.toLower(_.trim(_.get(txn, 'order.comments')));
          if (comments === wording) {
            isMatch = true;
            return false;
          }

          return _.noop();
        });
      } else { // Reservation
        const reservation = marker.options.transactions[0];
        const comments = _.toLower(_.trim(_.get(reservation, 'comments')));

        if (comments === wording) {
          isMatch = true;
        }
      }

      return isMatch;
    }

    function discardChanges($event) {
      nvDialog.confirmDelete($event, {
        title: nvTranslate.instant('commons.discard-changes'),
        content: nvTranslate.instant('container.zonal-routing.discard-changes-description'),
        ok: nvTranslate.instant('commons.leave'),
      }).then(onConfirmLeaving);

      function onConfirmLeaving() {
        $window.close();

        // if close window failed, then redirect user to route logs page
        goToRouteLogsPage();
      }
    }

    function goToRouteLogsPage() {
      nvZonalRouting.discardRoutingPageChanges();
      nvNavGuard.unlock();
      $state.go('container.route-logs');
    }

    function deletePolygonsByRoute(route) {
      _.forEach(route.nvPolygons, (polygon) => {
        featureGroup.removeLayer(polygon);
      });

      route.nvPolygons = [];
    }

    function resequenceMarkerByRoute(route) {
      route.seqNo = 1;

      _.forEach(route.addedMarkers, (marker, index) => {
        if (!(hotkeyEvents.isWaypointDetailsTableLocked &&
          marker === hotkeyEvents.hoveredMarker)) {
          nvZonalRouting.setRoutedWaypointMarkerIcon(marker, route);
        }

        ctrl.waypointDndLabelsMap[marker.options.id].index = index;
        ctrl.waypointDndLabelsMap[marker.options.id].label = generateDndLabelHtml(marker, index);
        route.seqNo += 1;
      });
    }

    function plot() {
      ctrl.map = nvMaps.initialize('routing-map', {
        controlPosition: nvZonalRouting.MAP_CONTROL_POSITION,
      });

      featureGroup = L.featureGroup().addTo(ctrl.map);
      drawControl = new L.Control.Draw({
        edit: {
          featureGroup: featureGroup,
        },
        draw: {
          polygon: {
            shapeOptions: {
              color: '#333333',
              fillColor: '#333333',
              fillOpacity: 0.5,
              opacity: 1,
            },
          },
          polyline: false,
          rectangle: false,
          circle: false,
          marker: false,
        },
        position: nvZonalRouting.MAP_CONTROL_POSITION,
      });
      ctrl.map.addControl(drawControl);

      ctrl.map.on('draw:created', (e) => {
        createPolygon(e);
      });

      ctrl.map.on('draw:deleted', (e) => {
        const deletedIds = [];
        e.layers.eachLayer((layer) => {
          deletedIds.push(layer._leaflet_id);
        });

        _.forEach(nvZonalRouting.assignedRoutes, (assignedRoute) => {
          assignedRoute.nvPolygons = _.filter(assignedRoute.nvPolygons, nvPolygon =>
            _.indexOf(deletedIds, nvPolygon._leaflet_id) <= -1
          );
        });
      });

      ctrl.map.on('move', () => {
        loadingMarkers = true;

        if (isMoveendTimerTriggerring === true) {
          return;
        }

        $timeout(() => {
          triggerMoveOrMoveEndAction();
        });
      });

      ctrl.map.on('moveend', () => {
        loadingMarkers = true;

        if (isMoveendTimerTriggerring === true) {
          return;
        }

        $timeout(() => {
          triggerMoveOrMoveEndAction();
        });
      });

      nvZonalRouting.addClusterGroupControl(ctrl.map, $scope, $compile);

      if (toCluster) {
        nvZonalRouting.updateClustering(ctrl.map, 0);
      }

      parentContentLoading.backdropLoading = true;
      const readRouteParams = {
        routeIds: _.join(routeIds, ','),
        include_waypoints: true,
      };

      Route.read(readRouteParams)
        .then(readRouteSuccess);

      function readRouteSuccess(response) {
        routes = response;
        ctrl.map.allWaypointMarkers = [];
        ctrl.map.allWaypointMarkersMap = {};
        ctrl.waypointDndLabelsMap = {};

        const promises = [
          Driver.searchByIds(_.map(routes, 'driverId')),
          Tag.all(),
        ];

        if (fetchUnroutedWaypoints && _.size(routes) > 0) {
          const latestRouteDate = _.maxBy(routes, 'date').date;

          const latestRouteDateUtc = nvDateTimeUtils.displayDateTime(
            nvDateTimeUtils.toSOD(moment(latestRouteDate)), 'utc'
          );

          const zoneTransactionsRequestPayload = {
            routeDate: latestRouteDateUtc,
            orderCreationDate: latestRouteDateUtc,
          };

          if (_.size(zoneIds) > 0) {
            zoneTransactionsRequestPayload.zoneIds = zoneIds;
          }

          promises.push(Route.getZoneTransactionsManual(zoneTransactionsRequestPayload));
        } else {
          promises.push($q.when({}));
        }
        $q.all(promises)
          .then(getAllSuccess);

        function getAllSuccess(responses) {
          drivers = responses[0];
          driversSelectionOptions = Driver.toOptions({
            drivers: drivers,
          });

          const tags = _.get(responses[1], 'tags');
          resequencedTagId = _.get(_.find(tags, ['name', Tag.RESERVED_TAG.RESEQUENCED]), 'id', 0);

          processRoutes();
          unroutedWaypointsPlotting(responses[2]);
          addSpiderfyEvent();
          parentContentLoading.backdropLoading = false;
        }
      }

      function processRoutes() {
        _.forEach(routes, (routeData) => {
          const routeId = createNewRoute(routeData);
          refreshDriverSelectionOptions(routeData.id, routeData.driverId);

          if (routeData.waypoints) {
            let index = 0;
            _.forEach(routeData.waypoints, (waypoint) => {
              const transactions = [];
              if (Waypoint.isTransaction(waypoint)) {
                const txns = waypoint.transactions;
                if (!txns) {
                  return;
                }

                _.forEach(txns, (txn) => {
                  txn.waypoint = waypoint;
                  txn.order = txn.activeOrder;

                  transactions.push(txn);
                });
              } else if (Waypoint.isReservation(waypoint)) {
                transactions.push(waypoint.reservations[0]);
              }

              const marker = nvZonalRouting.addWaypointMarker(
                ctrl.map, waypoint, transactions, toCluster
              );

              ctrl.waypointDndLabelsMap[marker.options.id] = generateDndLabel(marker, index);
              index += 1;
              addMouseListener(marker);

              ctrl.map.allWaypointMarkers.push(marker);
              ctrl.map.allWaypointMarkersMap[waypoint.id] = marker;

              addMarkerToRoute(marker, routeId);
            });
          }
        });

        refreshAssignedRouteSelectionOptions();
      }

      function unroutedWaypointsPlotting(routeWaypoints) {
        _.forEach(routeWaypoints, (waypoint) => {
          let transactions = [];
          if (Waypoint.isTransaction(waypoint)) {
            transactions = waypoint.transactions;
          } else { // Reservation
            transactions.push(waypoint.reservation);
          }

          const marker = nvZonalRouting.addWaypointMarker(
            ctrl.map, waypoint, transactions, toCluster
          );
          ctrl.waypointDndLabelsMap[marker.options.id] = generateDndLabel(marker);
          addMouseListener(marker);

          ctrl.map.allWaypointMarkers.push(marker);
          ctrl.map.allWaypointMarkersMap[waypoint.id] = marker;
        });
      }

      function addSpiderfyEvent() {
        const oms = nvZonalRouting.spiderfy(ctrl.map);

        _.forEach(ctrl.map.allWaypointMarkers, (marker) => {
          oms.addMarker(marker);
        });

        oms.addListener('unspiderfy', unspiderfy);

        function unspiderfy() {
          _.forEach(ctrl.map.allWaypointMarkers, (marker) => {
            if (isVisibleAndNotRouted(marker)) {
              nvZonalRouting.setWaypointMarkerIcon(marker);
            }
          });
        }
      }

      function triggerMoveOrMoveEndAction() {
        // moveend will trigger per action,
        // so have to cancel previous call.
        // if no action in 500ms,
        // then will treat this as valid action and proceed to process data
        $timeout.cancel(moveendTimer);

        moveendTimer = $timeout(() => {
          isMoveendTimerTriggerring = true;
          onTriggerMoveOrMoveEndAction();

          if (!hotkeyEvents.isWaypointDetailsTableLocked) {
            resetWaypointDetailsTableParams();
            refresh();
          }
        }, 300);
      }

      function onTriggerMoveOrMoveEndAction() {
        const bounds = ctrl.map.getBounds().pad(getPadRadio());

        _.forEach(ctrl.map.allWaypointMarkers, (marker) => {
          // hide when marker is outside the bound, to solve performance issue
          if (!marker.options.extraInfo.isHideByMapViewSettings &&
            !marker.options.extraInfo.isHideByMapViewOption) {
            // only process those that still visible in map
            if (!marker.options.extraInfo.isHideWhenOutsideBound &&
              !bounds.contains(marker.getLatLng())) {
              // if marker is outside the bound, and in visible status, proceed to hide it
              nvZonalRouting.toggleVisibilityMarker(marker, false);
              marker.options.extraInfo.isHideWhenOutsideBound = true;
            } else if (marker.options.extraInfo.isHideWhenOutsideBound &&
              bounds.contains(marker.getLatLng())) {
              // if marker is inside the bound, and in hide status, proceed to show it
              nvZonalRouting.toggleVisibilityMarker(marker, true);
              marker.options.extraInfo.isHideWhenOutsideBound = false;
            }

            // if marker is outside the bound, and in hide status, then do nothing
            // if marker is inside the bound, and in visible status, then do nothing
          }
        });

        loadingMarkers = false;
        isMoveendTimerTriggerring = false;
      }

      function getPadRadio() {
        const zoomLevel = ctrl.map.getZoom();
        if (zoomLevel <= 15) {
          return 0.05;
        }

        switch (ctrl.map.getZoom()) {
          case 16: return 0.4;
          case 17: return 0.7;
          default: return 1; // >= 18
        }
      }
    }

    function generateDndLabel(marker, index = 0) {
      return {
        id: marker.options.id,
        index: index,
        dndType: ctrl.waypointsDndAllowedTypes[0],
        label: generateDndLabelHtml(marker, index),
      };
    }

    function generateDndLabelHtml(marker, index) {
      let priorityHtml = '';
      if (marker.options.extraInfo.priorityLevel > 0) {
        priorityHtml = '<i class="material-icons priority">star</i>';
      }

      const waypointDate = nvDateTimeUtils.displayDate(
        moment(marker.options.waypoint.date), 'utc'
      );

      let shipperName;
      let comments;
      if (Waypoint.isTransaction(marker.options.waypoint)) {
        shipperName = _.get(marker.options.transactions, '[0].order.fromName') || '-';
        comments = _.get(marker.options.transactions, '[0].order.comments') || '-';
      } else { // Reservation
        const reservation = marker.options.transactions[0];

        shipperName = reservation.name || '-';
        comments = reservation.comments || '-';
      }

      const timewindow = Timewindow.getTimewindowById(marker.options.waypoint.timewindowId);

      const wpIdRow = `<div class="waypoint-id">${index + 1}. <span>WP ID: ${marker.options.id}</span></div>`;
      const timewindowRow = `<div class="timewindow">${waypointDate}, ${timewindow.name}</div>`;
      const addressRow = `<div class="address">${marker.options.extraInfo.address}</div>`;
      const shipperRow = `<div class="shipper-name">${shipperName}</div>`;
      const commentsRow = `<div class="comments">Comments: ${comments}</div>`;

      let content = '<div class="waypoint-content">';
      content += wpIdRow;
      content += timewindowRow;
      content += addressRow;
      content += shipperRow;
      content += commentsRow;
      content += '</div>';

      return `${priorityHtml}${content}`;
    }

    function createPolygon(e) {
      featureGroup.addLayer(e.layer);

      if (ctrl.activeRoutePolygonData) {
        assignPolygonToActiveRoute(e.layer);
      } else {
        orphanPolygonLayers.push(e.layer);
        setPolygonStyle(e.layer, '#000000');
      }
    }

    function setPolygonStyle(layer, color) {
      const styleConfig = {
        fillOpacity: 0.5,
        opacity: 1,
        color: color,
        fillColor: color,
      };

      layer.setStyle(styleConfig);
    }

    function assignPolygonToActiveRoute(layer) {
      ctrl.activeRoute.nvPolygons = ctrl.activeRoute.nvPolygons || [];
      ctrl.activeRoute.nvPolygons.push(layer);

      setPolygonStyle(layer, ctrl.activeRoutePolygonData.color);
    }

    function isVisible(marker) {
      return !marker.options.extraInfo.isHideByMapViewSettings &&
        !marker.options.extraInfo.isHideByMapViewOption;
    }

    function isVisibleAndNotRouted(marker) {
      return isVisible(marker) && !marker.options.routeId;
    }

    function isVisibleAndRouted(marker) {
      return isVisible(marker) && marker.options.routeId > 0;
    }

    function addMarkerToRoute(marker, routeId, addToIndex = null) {
      if (hotkeyEvents.isWaypointDetailsTableLocked) {
        return false;
      }

      // prepare
      const route = nvZonalRouting.getAssignedRouteData(routeId);

      // process
      // only get visible && not routed waypoints
      if (isVisibleAndNotRouted(marker)) {
        // set icon
        nvZonalRouting.setRoutedWaypointMarkerIcon(marker, route);

        marker.options.routeId = route.id;

        if (addToIndex !== null) {
          route.addedMarkers.splice(addToIndex, 0, marker);
          route.addedDndLabels.splice(addToIndex, 0, ctrl.waypointDndLabelsMap[marker.options.id]);
        } else {
          route.addedMarkers.push(marker);
          route.addedDndLabels.push(ctrl.waypointDndLabelsMap[marker.options.id]);
        }

        route.seqNo += 1;

        if (marker.options.extraInfo.isPriority) {
          route.priority += 1;
        } else {
          route.nonPriority += 1;
        }

        refresh();
        return true;
      }

      return false;
    }

    function removeMarkerFromRoute(marker, forceRemove = false) {
      if (hotkeyEvents.isWaypointDetailsTableLocked) {
        return false;
      }

      if (!forceRemove && !isVisibleAndRouted(marker)) {
        // only visible and routed marker can remove
        return false;
      }

      const route = nvZonalRouting.getAssignedRouteData(marker.options.routeId);
      nvZonalRouting.setWaypointMarkerIcon(marker);

      route.addedMarkers = _.filter(route.addedMarkers, theMarker =>
        theMarker.options.id !== marker.options.id
      );
      route.addedDndLabels = _.filter(route.addedDndLabels, dndLabel =>
        dndLabel.id !== marker.options.id
      );

      if (marker.options.extraInfo.isPriority) {
        route.priority -= 1;
      } else {
        route.nonPriority -= 1;
      }

      delete marker.options.routeId;

      if (route.seqNo > 1) { // prevent seqno going negative
        route.seqNo -= 1;
      }

      refresh();
      return true;
    }

    function addMouseListener(marker) {
      nvMaps.markerAddEvent(marker, 'mouseover', mouseover);
      nvMaps.markerAddEvent(marker, 'mouseout', mouseout);
      nvMaps.markerAddEvent(marker, 'click', click);

      function mouseover(theMarker) {
        if (hotkeyEvents.isWaypointDetailsTableLocked || loadingMarkers) {
          return;
        }
        hotkeyEvents.hoveredMarker = theMarker;

        ctrl.waypointDetailsTableParams.waypointId = theMarker.options.waypoint.id;
        ctrl.waypointDetailsTableParams.sizeCounter = null;
        ctrl.waypointDetailsTableParams.address = nvAddress.extract(theMarker.options.waypoint);
        ctrl.waypointDetailsTableParams.list = [];

        if (Waypoint.isTransaction(theMarker.options.waypoint)) {
          ctrl.waypointDetailsTableParams.isTransactionWp = true;
          ctrl.waypointDetailsTableParams.sizeCounter = 0;

          const transactions = theMarker.options.transactions;
          _.forEach(transactions, (transaction) => {
            const txn = newTxnObject(transaction, theMarker);
            ctrl.waypointDetailsTableParams.list.push(txn);

            const parcelSize = Order.getParcelSizeByEnum(txn.parcelSize);
            if (parcelSize) {
              ctrl.waypointDetailsTableParams.sizeCounter += parcelSize.capacity;
            }
          });
        } else { // Reservation
          ctrl.waypointDetailsTableParams.isTransactionWp = false;
          const reservation = theMarker.options.transactions[0];

          ctrl.waypointDetailsTableParams.list.push(newReservationObject(reservation));
        }

        function newTxnObject(transaction, mrk) {
          let cod = '-';
          let parcelSize = '-';
          let orderType = '-';
          let fromName = '';
          let orderComments = '';
          let orderId = 0;
          let trackingId = '';
          const transactionType = transaction.type ? transaction.type.toLowerCase() : '';

          if (_.size(transaction.order) > 0) {
            if (_.size(transaction.order.cod) > 0) {
              cod = `${nvCurrency.getCode($rootScope.countryId)} ${transaction.order.cod.goodsAmount ? transaction.order.cod.goodsAmount : 0}`;
            }

            orderId = transaction.order.id;
            trackingId = transaction.order.trackingId;
            parcelSize = transaction.order.parcelSize;
            orderType = transaction.order.type ? transaction.order.type.toLowerCase() : '';
            fromName = transaction.order.fromName;
            orderComments = transaction.order.comments === null ? '' : transaction.order.comments;
          }

          return {
            orderId: orderId,
            trackingId: trackingId,
            shipperName: fromName,
            type: `${orderType} ${transactionType}`,
            isPriority: mrk.options.extraInfo.isPriority,
            priorityLevel: transaction.priorityLevel,
            startTime: transaction.startTime,
            endTime: transaction.endTime,
            cod: cod,
            parcelSize: parcelSize,
            orderComments: orderComments,
          };
        }

        function newReservationObject(reservation) {
          return {
            shipperName: reservation.name,
            startTime: reservation.readyDatetime,
            endTime: reservation.latestDatetime,
            approxVolume: reservation.approxVolume,
            comments: reservation.comments,
            dp: reservation.distributionPointId,
          };
        }

        refresh();
      }

      function mouseout() {
        if (!hotkeyEvents.isWaypointDetailsTableLocked && !loadingMarkers) {
          resetWaypointDetailsTableParams();
          refresh();
        }
      }

      function click(theMarker) {
        if (!ctrl.activeRouteId) {
          let errorMessage = nvTranslate.instant('container.zonal-routing.error-add-new-route');
          if (_.size(nvZonalRouting.assignedRoutes) > 0) {
            errorMessage = nvTranslate.instant('container.zonal-routing.error-select-route');
          }

          nvToast.error(errorMessage);
        }

        if (!theMarker.options.routeId) { // assign to active route if there is
          addMarkerToRoute(theMarker, ctrl.activeRouteId);
        } else { // remove the waypoint from route
          removeMarkerFromRoute(theMarker);
        }

        resequenceMarkerByRoute(
          nvZonalRouting.getAssignedRouteData(ctrl.activeRouteId)
        );
      }
    }

    function filterMarkerSubmit() {
      filterMarker();

      function filterMarker() {
        _.forEach(ctrl.map.allWaypointMarkers, (marker) => {
          // skip those that are routed
          if (marker.options.routeId) {
            return undefined;
          }

          // start checking whether the marker meet the filter condition
          let isCorrectCondition = true;

          _.forEach(ctrl.routingFilters.selectedFilters, (filter) => {
            const isMarkerMeetFilterCondition = getIsMarkerMeetFilterCondition(filter, marker);
            if (isMarkerMeetFilterCondition !== null) {
              isCorrectCondition = isMarkerMeetFilterCondition;

              if (isCorrectCondition === false) {
                return false; // no necessary to scan for next item already
              }
            }

            return undefined;
          });

          // process marker opacity
          const isVisibleBool = isCorrectCondition === nvModelUtils.fromYesNoBitStringToBool(
              ctrl.routingFilters.filterMode
            );
          marker.options.extraInfo.isHideByMapViewSettings = !isVisibleBool;

          nvZonalRouting.toggleVisibilityMarker(marker, isVisibleBool);

          return undefined;
        });
      }

      function getIsMarkerMeetFilterCondition(filter, marker) {
        switch (filter.type) {
          case nvBackendFilterUtils.FILTER_OPTIONS:
          case nvBackendFilterUtils.FILTER_AUTOCOMPLETE:
            return filterByOptions(filter, marker);
          case nvBackendFilterUtils.FILTER_BOOLEAN:
            return filterByBoolean(filter, marker);
          case nvBackendFilterUtils.FILTER_TEXT:
            return filterByText(filter, marker);
          default:
            return false;
        }
      }

      function filterByOptions(filter, marker) {
        if (_.size(filter.selectedOptions) > 0) {
          let isOrCorrectCondition = false;
          _.forEach(filter.selectedOptions, (option) => {
            isOrCorrectCondition = ctrl.routingFilters.conditionCheckFn[filter.key](marker, option);

            if (isOrCorrectCondition) {
              return false; // no necessary to scan for next item already
            }

            return undefined;
          });

          return isOrCorrectCondition;
        }

        return null;
      }

      function filterByBoolean(filter, marker) {
        return ctrl.routingFilters.conditionCheckFn[filter.key](marker, filter);
      }

      function filterByText(filter, marker) {
        if (_.size(_.trim(filter.model)) > 0) {
          return ctrl.routingFilters.conditionCheckFn[filter.key](marker, filter);
        }

        return null;
      }
    }

    function openRouteMenu($event, route) {
      nvPanel.show($event, {
        controller: 'ZonalRoutingRouteMenuController',
        templateUrl: 'views/container/zonal-routing/panel/route/route.panel.html',
        panelClass: 'route-menu',
        xPosition: 'ALIGN_END',
        locals: {
          routeData: route,
          onClose: onClose,
        },
      });

      function onClose(response) {
        if (response.action === nvZonalRouting.ROUTE_PANEL_ACTION.DELETE_ROUTE) {
          deleteRoute($event, route);
        } else if (response.action === nvZonalRouting.ROUTE_PANEL_ACTION.PURGE_ROUTE) {
          removeWaypoints($event, route);
        }
      }
    }

    function activeRouteSelectionChanged() {
      $timeout(() => {
        const route = nvZonalRouting.getAssignedRouteData(ctrl.selectionActiveRouteId);
        setActiveRoute(route);
      });
    }

    function setActiveRoute(route) {
      if (ctrl.activeRouteId !== route.id) {
        ctrl.selectionActiveRouteId = route.id;
        ctrl.activeRouteId = route.id;
        ctrl.activeRoute = route;

        initPolygonDrawControl(route);

        if (_.size(orphanPolygonLayers) > 0) {
          _.forEach(orphanPolygonLayers, (layer) => {
            assignPolygonToActiveRoute(layer);
          });

          orphanPolygonLayers = [];
        }
      }
    }

    function unsetActiveRoute() {
      ctrl.activeRoute = null;
      ctrl.activeRouteId = 0;
      ctrl.activeRoutePolygonData = null;

      initPolygonDrawControl();
    }

    function refresh() {
      if (!$scope.$$phase) {
        $scope.$apply(); // instant refresh
      }
    }

    function addWaypointsInPolygon(
      route, type = nvZonalRouting.POLYGON_TOOLS_ADD_ACTION.UNROUTED
    ) {
      const processed = {
        priority: 0,
        nonPriority: 0,
      };

      _.forEach(ctrl.map.allWaypointMarkers, (marker) => {
        _.forEach(route.nvPolygons, (polygon) => {
          if (nvZonalRouting.inPolygon(polygon.getLatLngs(), marker.options.position)) {
            if ((
                  (type === nvZonalRouting.POLYGON_TOOLS_ADD_ACTION.UNROUTED) ||
                  (type === nvZonalRouting.POLYGON_TOOLS_ADD_ACTION.UNROUTED_PRIORITY &&
                    marker.options.extraInfo.isPriority) ||
                  (type === nvZonalRouting.POLYGON_TOOLS_ADD_ACTION.UNROUTED_NON_PRIORITY &&
                    !marker.options.extraInfo.isPriority)
                ) && !marker.options.routeId) {
              const isSuccess = addMarkerToRoute(marker, route.id);

              if (isSuccess && marker.options.extraInfo.isPriority) {
                processed.priority += 1;
              } else if (isSuccess && !marker.options.extraInfo.isPriority) {
                processed.nonPriority += 1;
              }
            } else if (type === nvZonalRouting.POLYGON_TOOLS_ADD_ACTION.ALL) {
              if (marker.options.routeId) {
                removeMarkerFromRoute(marker);
              }

              const isSuccess = addMarkerToRoute(marker, route.id);

              if (isSuccess && marker.options.extraInfo.isPriority) {
                processed.priority += 1;
              } else if (isSuccess && !marker.options.extraInfo.isPriority) {
                processed.nonPriority += 1;
              }
            }
          }
        });
      });

      resequenceMarkerByRoute(route);

      return processed;
    }

    function removeWaypointsInPolygon(
      route, type = nvZonalRouting.POLYGON_TOOLS_REMOVE_ACTION.ROUTED
    ) {
      const processed = {
        priority: 0,
        nonPriority: 0,
      };

      _.forEach(route.nvPolygons, (polygon) => {
        _.forEach(route.addedMarkers, (marker) => {
          if (nvZonalRouting.inPolygon(polygon.getLatLngs(), marker.options.position)) {
            if ((type === nvZonalRouting.POLYGON_TOOLS_REMOVE_ACTION.ROUTED) ||
              (type === nvZonalRouting.POLYGON_TOOLS_REMOVE_ACTION.ROUTED_PRIORITY &&
                marker.options.extraInfo.isPriority) ||
              (type === nvZonalRouting.POLYGON_TOOLS_REMOVE_ACTION.ROUTED_NON_PRIORITY &&
                !marker.options.extraInfo.isPriority)) {
              const isSuccess = removeMarkerFromRoute(marker);

              if (isSuccess && marker.options.extraInfo.isPriority) {
                processed.priority += 1;
              } else if (isSuccess && !marker.options.extraInfo.isPriority) {
                processed.nonPriority += 1;
              }
            }
          }
        });
      });

      resequenceMarkerByRoute(route);

      return processed;
    }

    function removeWaypoints($event, route) {
      nvDialog.confirmDelete($event, {
        title: nvTranslate.instant('container.zonal-routing.purge-route'),
        content: nvTranslate.instant('container.zonal-routing.purge-route-description'),
        ok: nvTranslate.instant('container.zonal-routing.purge-route'),
        cancel: nvTranslate.instant('commons.go-back'),
      }).then(onConfirm);

      function onConfirm() {
        resetRouteMarkers(route);
      }
    }

    function addRelated(route, checkOnly = false) {
      let isAbleToAddRelated = false;

      const postcodes = [];
      _.forEach(route.addedMarkers, (marker) => {
        postcodes.push(marker.options.waypoint && marker.options.waypoint.postcode);
      });

      _.forEach(ctrl.map.allWaypointMarkers, (marker) => {
        if (!isVisibleAndNotRouted(marker)) {
          return undefined;
        }

        if (marker.options.waypoint && marker.options.waypoint.postcode &&
        postcodes.indexOf(marker.options.waypoint.postcode) >= 0) {
          isAbleToAddRelated = true;

          if (checkOnly === false) {
            addMarkerToRoute(marker, route.id);
          }
          return false;
        }

        return undefined;
      });

      resequenceMarkerByRoute(route);
      return isAbleToAddRelated;
    }

    function removeRoute(route) {
      resetRouteMarkers(route);
      deletePolygonsByRoute(route);

      _.remove(nvZonalRouting.assignedRoutes, ['id', route.id]);
      refreshAssignedRouteSelectionOptions();

      if (route.id === ctrl.activeRouteId) {
        unsetActiveRoute();
      }
    }

    function resetRouteMarkers(route) {
      _.forEach(route.addedMarkers, (marker) => {
        nvZonalRouting.setWaypointMarkerIcon(marker);
        delete marker.options.routeId;
      });

      route.priority = 0;
      route.nonPriority = 0;
      route.addedMarkers = [];
      route.addedDndLabels = [];
      route.seqNo = 1;
    }

    function dndLabelOnClose(item) {
      const routeId = ctrl.map.allWaypointMarkersMap[item.id].options.routeId;
      removeMarkerFromRoute(ctrl.map.allWaypointMarkersMap[item.id], true);
      resequenceMarkerByRoute(
        nvZonalRouting.getAssignedRouteData(routeId)
      );
    }

    function refreshWaypointOnDrop(response) {
      const itemId = response.item.id;

      const marker = ctrl.map.allWaypointMarkersMap[itemId];
      const originalRouteId = marker.options.routeId;
      const targetRouteId = +response.target.id;
      let moveToIndex = response.index;

      const originalRoute = nvZonalRouting.getAssignedRouteData(originalRouteId);
      if (originalRouteId === targetRouteId) {
        if (moveToIndex > 0 &&
          moveToIndex > response.item.index) {
          moveToIndex -= 1;
        }
      }

      removeMarkerFromRoute(marker);
      addMarkerToRoute(marker, targetRouteId, moveToIndex);

      resequenceMarkerByRoute(originalRoute);

      if (originalRouteId !== targetRouteId) {
        const targetRoute = nvZonalRouting.getAssignedRouteData(targetRouteId);
        resequenceMarkerByRoute(targetRoute);
      }
    }

    function preSubmitRoutes($event) {
      // resequenced flag dialog
      nvDialog.confirmSave($event, {
        title: nvTranslate.instant('container.zonal-routing.confirm-resequenced'),
        content: nvTranslate.instant('container.zonal-routing.confirm-resequenced-description'),
        ok: nvTranslate.instant('commons.yes'),
        cancel: nvTranslate.instant('commons.no'),
      }).then(onOk, onCancel);

      function onOk() {
        submitRoutes($event, true);
      }

      function onCancel() {
        submitRoutes($event, false);
      }
    }

    function submitRoutes($event, isResequenced = false) {
      const routesPayload = [];
      const waypointIds = [];

      // validation and preparation
      const errorRouteIds = {
        noWaypoint: [], // cannot proceed if have
        noDriver: [], // cannot proceed if have
      };
      _.forEach(nvZonalRouting.assignedRoutes, (assignedRoute) => {
        assignedRoute.driverId = 0;
        if (assignedRoute.driverSelection.driver) {
          // move driverId to parent level if driver is assigned
          assignedRoute.driverId = assignedRoute.driverSelection.driver.value;
        }

        const route = {
          id: assignedRoute.id,
          tags: _.get(_.find(routes, ['id', assignedRoute.id]), 'tags') || [],
          waypoints: [],
        };

        if (isResequenced && _.indexOf(route.tags, resequencedTagId) <= -1) {
          route.tags.push(resequencedTagId);
        }

        if (_.size(assignedRoute.addedMarkers) <= 0) {
          errorRouteIds.noWaypoint.push(assignedRoute.id);
        }

        if (!assignedRoute.driverId) {
          errorRouteIds.noDriver.push(assignedRoute.id);
        } else {
          route.driverId = +assignedRoute.driverId;

          const driver = _.find(drivers, ['id', route.driverId]);
          if (driver) {
            route.vehicleId = _.get(driver, 'vehicles[0].id', 0);
          }
        }

        _.forEach(assignedRoute.addedMarkers, (marker) => {
          route.waypoints.push(marker.options.id);
          waypointIds.push(marker.options.id);
        });

        routesPayload.push(route);
      });

      const duplicateWaypointIds = _.filter(waypointIds, (value, index, iteratee) =>
          _.includes(iteratee, value, index + 1)
      ); // cannot proceed if duplicate waypoint found

      // show validation error (if have)
      if (errorRouteIds.noWaypoint.length > 0 ||
        errorRouteIds.noDriver.length > 0 ||
        duplicateWaypointIds.length > 0) {
        if (errorRouteIds.noWaypoint.length > 0) {
          nvToast.error(nvTranslate.instant('container.zonal-routing.error-no-waypoints-added-for-routes', {
            routes: errorRouteIds.noWaypoint.join(', '),
          }));
        }

        if (errorRouteIds.noDriver.length > 0) {
          nvToast.error(nvTranslate.instant('container.zonal-routing.error-no-driver-added-for-routes', {
            routes: errorRouteIds.noDriver.join(', '),
          }));
        }

        if (duplicateWaypointIds.length > 0) {
          nvToast.error(nvTranslate.instant('container.zonal-routing.error-waypoints-duplicate', {
            waypoints: duplicateWaypointIds.join(', '),
          }));
        }

        return;
      }

      // submitting
      nvDialog.confirmSave($event, {
        title: nvTranslate.instant('commons.save-changes'),
      }).then(submittingRoutes);

      function submittingRoutes() {
        parentContentLoading.backdropLoading = true;
        Route.updateRoutes(routesPayload)
          .then(success)
          .finally(finallyFn);

        function success() {
          nvToast.success(
            nvTranslate.instant('container.zonal-routing.routes-updated')
          );
        }

        function finallyFn() {
          parentContentLoading.backdropLoading = false;
        }
      }
    }

    function toggleIsWaypointDetailsTableLocked() {
      const newTableLockedValue = !hotkeyEvents.isWaypointDetailsTableLocked;

      if (hotkeyEvents.hoveredMarker && newTableLockedValue) {
        nvZonalRouting.setHoveredWaypointMarkerIcon(hotkeyEvents.hoveredMarker);

        hotkeyEvents.isWaypointDetailsTableLocked = newTableLockedValue;
      } else if (hotkeyEvents.hoveredMarker && !newTableLockedValue) {
        if (hotkeyEvents.hoveredMarker.options.routeId > 0) {
          nvZonalRouting.setRoutedWaypointMarkerIcon(
            hotkeyEvents.hoveredMarker,
            nvZonalRouting.getAssignedRouteData(hotkeyEvents.hoveredMarker.options.routeId)
          );
        } else {
          nvZonalRouting.setWaypointMarkerIcon(hotkeyEvents.hoveredMarker);
        }

        resetWaypointDetailsTableParams();
        hotkeyEvents.isWaypointDetailsTableLocked = newTableLockedValue;
      }
    }

    function navigateToEditOrderPage(orderId) {
      $window.open(
        $state.href('container.order.edit', { orderId: orderId })
      );
    }

    function getParcelSizeShortName(parcelSizeEnum) {
      const parcelSize = Order.getParcelSizeByEnum(parcelSizeEnum);
      if (parcelSize) {
        return parcelSize.displayNameShort;
      }

      return null;
    }

    function createNewRoute(routeData) {
      const route = newRouteObj();

      nvZonalRouting.assignedRoutes.push(route);

      if (!ctrl.activeRouteId) {
        setActiveRoute(route);
      }

      return route.id;

      function newRouteObj() {
        const routeNum = generateRouteNum();

        return {
          id: routeData.id,
          num: routeNum,
          date: nvZonalRouting.routeDate,
          seqNo: 1,
          priority: 0,
          nonPriority: 0,
          driverId: 0,
          driverSelection: {
            searchText: '',
            driver: null,
          },
          polygonSelection: {
            ADD: nvZonalRouting.POLYGON_TOOLS_ADD_ACTION.UNROUTED,
            REMOVE: nvZonalRouting.POLYGON_TOOLS_REMOVE_ACTION.ROUTED,
          },
          color: nvZonalRouting.getRouteColor(routeNum),
          addedMarkers: [],
          addedDndLabels: [],
          nvPolygons: [],
        };

        function generateRouteNum() {
          const lastRoute = _.findLast(nvZonalRouting.assignedRoutes);

          return lastRoute ? lastRoute.num + 1 : 1;
        }
      }
    }

    function refreshAssignedRouteSelectionOptions() {
      ctrl.assignedRoutesSelectionOptions = _.map(nvZonalRouting.assignedRoutes, route => ({
        value: route.id,
        displayName: route.id,
      }));
    }

    function refreshDriverSelectionOptions(routeId, driverId) {
      $timeout(() => {
        const route = nvZonalRouting.getAssignedRouteData(routeId);
        const driverSelectAutocomplete = nvAutocompleteData.getByHandle(`route-list-pane-select-driver-${route.id}`);
        const driverSelectAutocomplete2 = nvAutocompleteData.getByHandle(`right-pane-select-driver-${route.id}`);

        const clonedDriversSelectionOptions = _.cloneDeep(driversSelectionOptions);
        driverSelectAutocomplete.setPossibleOptions(clonedDriversSelectionOptions);
        driverSelectAutocomplete2.setPossibleOptions(clonedDriversSelectionOptions);
        if (driverId) {
          setDriverDataToRoute(route, _.find(
            clonedDriversSelectionOptions, ['value', driverId]
          ));
        }
      });
    }

    function deleteRoute($event, route) {
      nvDialog.confirmDelete($event, {
        title: nvTranslate.instant('container.zonal-routing.delete-route'),
        content: nvTranslate.instant('container.zonal-routing.delete-route-description'),
        ok: nvTranslate.instant('container.zonal-routing.delete-route'),
        cancel: nvTranslate.instant('commons.go-back'),
      }).then(onConfirm);

      function onConfirm() {
        if (route.id === ctrl.activeRouteId) {
          // if this is active route, then toggle setActiveRoute to unset
          setActiveRoute(route);
        }

        parentContentLoading.backdropLoading = true;
        Route.delete([{
          id: route.id,
        }]).then(success).finally(finallyFn);

        function success() {
          removeRoute(route);

          nvToast.warning(
            nvTranslate.instant('container.zonal-routing.route-x-deleted', { x: route.id })
          );
        }

        function finallyFn() {
          parentContentLoading.backdropLoading = false;
        }
      }
    }

    function hasPolygonInRoute() {
      const filteredRoutes = _.filter(nvZonalRouting.assignedRoutes, route =>
        _.size(route.nvPolygons) > 0
      );

      return _.size(filteredRoutes) > 0;
    }

    function hasPolygon() {
      return hasPolygonInRoute() || _.size(orphanPolygonLayers) > 0;
    }

    function openPolygonToolsActionMenu($event, actionKey, route) {
      let polygonToolsAction = nvZonalRouting.POLYGON_TOOLS_ADD_ACTION;
      if (POLYGON_TOOLS_ACTION[actionKey].icon === POLYGON_TOOLS_ACTION.REMOVE.icon) {
        polygonToolsAction = nvZonalRouting.POLYGON_TOOLS_REMOVE_ACTION;
      }

      nvPanel.show($event, {
        controller: 'ZonalRoutingPolygonToolsMenuController',
        templateUrl: 'views/container/zonal-routing/panel/polygon-tools/polygon-tools.panel.html',
        panelClass: 'polygon-tools-menu',
        xPosition: 'ALIGN_END',
        locals: {
          onClose: onClose,
          polygonToolsAction: polygonToolsAction,
        },
      });

      function onClose(response) {
        polygonToolsPerformAction(actionKey, response.action, route);
      }
    }

    function polygonToolsPerformAction(actionKey, type, route) {
      if (POLYGON_TOOLS_ACTION[actionKey].icon === POLYGON_TOOLS_ACTION.ADD.icon) {
        // ADD action performed
        route.polygonSelection.ADD = type;

        addWaypointsInPolygon(
          route, route.polygonSelection.ADD
        );
      } else {
        // REMOVE action performed
        route.polygonSelection.REMOVE = type;

        removeWaypointsInPolygon(
          route, route.polygonSelection.REMOVE
        );
      }

      deletePolygonsByRoute(route);
    }

    function deleteAllPolygons() {
      _.forEach(nvZonalRouting.assignedRoutes, (route) => {
        deletePolygonsByRoute(route);
      });
    }

    function setDriverDataToRoute(theRoute, theDriver) {
      if (theDriver) {
        theRoute.driverId = theDriver.value;
        theRoute.driverSelection.searchText = theDriver.displayName;
        theRoute.driverSelection.driver = theDriver;
      }
    }

    function initPolygonDrawControl(route) {
      if (route) {
        ctrl.activeRoutePolygonData = {
          color: route.color.primary,
          routeId: route.id,
        };
      }

      drawControl.setDrawingOptions({
        polygon: {
          shapeOptions: {
            color: route ? ctrl.activeRoutePolygonData.color : '#000000',
            fillColor: route ? ctrl.activeRoutePolygonData.color : '#000000',
            fillOpacity: 0.5,
            opacity: 1,
          },
        },
        polyline: false,
        rectangle: false,
        circle: false,
        marker: false,
      });
    }

    function resetWaypointDetailsTableParams() {
      hotkeyEvents.hoveredMarker = null;

      ctrl.waypointDetailsTableParams.waypointId = null;
      ctrl.waypointDetailsTableParams.sizeCounter = null;
      ctrl.waypointDetailsTableParams.address = null;
      ctrl.waypointDetailsTableParams.list = [];
    }

    function routeBackgroundColorStyling(route) {
      return {
        'background-color': route.color.primary,
      };
    }

    function routeBorderColorStyling(route) {
      return {
        'border-color': route.color.primary,
      };
    }
  }
}());
