(function controller() {
  angular
    .module('nvOperatorApp.controllers')
    .controller('ZonalRoutingFilterDialogController', ZonalRoutingFilterDialogController);

  ZonalRoutingFilterDialogController.$inject = [
    'nvModelUtils', 'nvZonalRouting', 'mapData', 'Timewindow', 'Order',
    'Reservation', 'nvBackendFilterUtils', 'nvTranslate', '$mdDialog', 'Shippers',
  ];

  function ZonalRoutingFilterDialogController(
    nvModelUtils, nvZonalRouting, mapData, Timewindow, Order,
    Reservation, nvBackendFilterUtils, nvTranslate, $mdDialog, Shippers
  ) {
    // variables
    const ctrl = this;

    ctrl.routingFilters = {
      selectedFilters: [],
      possibleFilters: [],
      filterMode: nvModelUtils.fromBoolToYesNoBitString(true),
      conditionCheckFn: {
        deliveryType: deliveryTypeConditionCheckFn,
        parcelSize: parcelSizeConditionCheckFn,
        priorityLevel: priorityLevelConditionCheckFn,
        timewindow: timewindowConditionCheckFn,
        shipper: shipperConditionCheckFn,
        approxVolumn: approxVolumeConditionCheckFn,
        cash: codConditionCheckFn,
        searchAddress: searchAddressCheckFn,
        priority: priorityConditionCheckFn,
      },
    };

    // functions
    ctrl.onSave = onSave;
    ctrl.onCancel = onCancel;

    // start
    initialize();

    // functions details
    function initialize() {
      if (_.size(nvZonalRouting.routingFilters) <= 0) {
        const filters = initFilter();
        ctrl.routingFilters.possibleFilters = filters.generalFilters;
        ctrl.routingFilters.selectedFilters = _.remove(ctrl.routingFilters.possibleFilters,
            filter => filter.showOnInit === true
        );
      } else {
        ctrl.routingFilters = _.cloneDeep(nvZonalRouting.routingFilters);
      }

      function initFilter() {
        const generalFilters = [
          {
            type: nvBackendFilterUtils.FILTER_OPTIONS,
            showOnInit: true,
            possibleOptions: getDeliveryTypes(),
            selectedOptions: [],
            searchText: {},
            mainTitle: nvTranslate.instant('commons.delivery-type'),
            key: 'deliveryType',
            presetKey: 'deliveryType',
          },
          {
            type: nvBackendFilterUtils.FILTER_OPTIONS,
            showOnInit: true,
            possibleOptions: Order.PARCEL_SIZE,
            selectedOptions: [],
            sortBy: 'id',
            searchText: {},
            mainTitle: nvTranslate.instant('commons.parcel-size'),
            key: 'parcelSize',
            presetKey: 'parcelSize',
          },
          {
            type: nvBackendFilterUtils.FILTER_OPTIONS,
            showOnInit: true,
            possibleOptions: getPriorityLevels(),
            selectedOptions: [],
            sortBy: 'index',
            searchText: {},
            mainTitle: nvTranslate.instant('commons.model.priority-level'),
            key: 'priorityLevel',
            presetKey: 'priorityLevel',
          },
          {
            type: nvBackendFilterUtils.FILTER_OPTIONS,
            showOnInit: true,
            possibleOptions: getTimewindows(),
            selectedOptions: [],
            sortBy: 'index',
            searchText: {},
            mainTitle: nvTranslate.instant('container.zonal-routing.timeslots'),
            key: 'timewindow',
            presetKey: 'timewindow',
          },
          {
            type: nvBackendFilterUtils.FILTER_AUTOCOMPLETE,
            showOnInit: true,
            callback: getShippers,
            backendKey: 'legacy_ids',
            selectedOptions: [],
            mainTitle: nvTranslate.instant('commons.shipper'),
            key: 'shipper',
            presetKey: 'shipper',
          },
          {
            type: nvBackendFilterUtils.FILTER_OPTIONS,
            showOnInit: true,
            possibleOptions: getApproxVolumns(),
            selectedOptions: [],
            searchText: {},
            mainTitle: nvTranslate.instant('container.zonal-routing.reservation-size'),
            key: 'approxVolumn',
            presetKey: 'approxVolumn',
          },
          {
            type: nvBackendFilterUtils.FILTER_BOOLEAN,
            showOnInit: true,
            mainTitle: nvTranslate.instant('container.zonal-routing.cash-cod-cop'),
            trueLabel: nvTranslate.instant('commons.yes'),
            falseLabel: nvTranslate.instant('commons.no'),
            booleanModel: nvModelUtils.fromBoolToYesNoBitString(true),
            key: 'cash',
            presetKey: 'cash',
          },
          {
            type: nvBackendFilterUtils.FILTER_TEXT,
            showOnInit: true,
            mainTitle: nvTranslate.instant('container.zonal-routing.search-address'),
            placeholder: nvTranslate.instant('container.zonal-routing.search-address-hint'),
            model: null,
            key: 'searchAddress',
            presetKey: 'searchAddress',
          },
          {
            type: nvBackendFilterUtils.FILTER_BOOLEAN,
            showOnInit: true,
            mainTitle: nvTranslate.instant('commons.priority'),
            trueLabel: nvTranslate.instant('commons.priority'),
            falseLabel: nvTranslate.instant('commons.non-priority'),
            booleanModel: nvModelUtils.fromBoolToYesNoBitString(true),
            key: 'priority',
            presetKey: 'priority',
          },
        ];

        return {
          generalFilters: generalFilters,
        };

        function getDeliveryTypes() {
          return [
            {
              displayName: nvTranslate.instant('commons.model.pickup'),
              verifyKey: 'isReservation',
            },
            {
              displayName: nvTranslate.instant('container.zonal-routing.return-pickup'),
              verifyKey: 'hasReturnPickup',
            },
            {
              displayName: nvTranslate.instant('container.zonal-routing.c2c'),
              verifyKey: 'hasC2C',
            },
            {
              displayName: nvTranslate.instant('container.zonal-routing.dp'),
              verifyKey: 'hasDP',
            },
            {
              displayName: nvTranslate.instant('container.zonal-routing.dp-reservation'),
              verifyKey: 'hasDPReservation',
            },
            {
              displayName: nvTranslate.instant('container.zonal-routing.bulk-move-quantity'),
              verifyKey: 'hasBulkMoveQty',
            },
            {
              displayName: nvTranslate.instant('container.zonal-routing.rescheduled'),
              verifyKey: 'hasReschedule',
            },
            {
              displayName: nvTranslate.instant('container.zonal-routing.have-route-comments'),
              verifyKey: 'hasRouteComments',
            },
            {
              displayName: nvTranslate.instant('container.zonal-routing.sameday'),
              verifyKey: 'hasSameday',
            },
          ];
        }

        function getPriorityLevels() {
          return [
            {
              index: 0,
              displayName: 'container.zonal-routing.non-priority-tag',
              min: null,
              max: 0,
            },
            {
              index: 1,
              displayName: 'container.zonal-routing.low-priority-tag',
              min: 1,
              max: 3,
            },
            {
              index: 2,
              displayName: 'container.zonal-routing.mid-priority-tag',
              min: 4,
              max: 6,
            },
            {
              index: 3,
              displayName: 'container.zonal-routing.high-priority-tag',
              min: 7,
              max: null,
            },
          ];
        }

        function getTimewindows() {
          const allDayList = [];
          const daySlotList = [];
          const nightSlotList = [];
          const timeslotList = [];
          _.forEach(Timewindow.TIMEWINDOWS, (timewindow) => {
            switch (timewindow.type) {
              case Timewindow.TYPE.NIGHTSLOT:
                nightSlotList.push(
                  generateTimewindowItem(timewindow, 'container.zonal-routing.night-slot')
                );
                break;
              case Timewindow.TYPE.DAYSLOT:
                daySlotList.push(
                  generateTimewindowItem(timewindow, 'container.zonal-routing.day-slot')
                );
                break;
              case Timewindow.TYPE.DAYNIGHTSLOT:
                allDayList.push(generateTimewindowItem(timewindow, 'container.zonal-routing.all-day'));
                break;
              case Timewindow.TYPE.TIMESLOT:
                timeslotList.push(generateTimewindowItem(timewindow));
                break;
              default:
                allDayList.push(generateTimewindowItem(timewindow));
            }
          });

          let timewindows = _.concat(allDayList, daySlotList, nightSlotList, timeslotList);
          timewindows = _.map(timewindows, (timewindow, index) => {
            timewindow.index = index;
            return timewindow;
          });

          return timewindows;

          function generateTimewindowItem(timewindow, customLabel = null) {
            return {
              displayName: nvTranslate.instant(customLabel || timewindow.name),
              timewindowId: timewindow.id,
            };
          }
        }

        function getShippers(text) {
          return Shippers.filterSearch(
            text,
            _.isUndefined(this.getSelectedOptions) ? [] : this.getSelectedOptions()
          );
        }

        function getApproxVolumns() {
          return _.orderBy(_.map(Reservation.volumeOptions, volumeOption =>
              generateApproxVolumeItem(volumeOption)
          ), 'displayName', 'asc');

          function generateApproxVolumeItem(volumeOption) {
            return {
              displayName: volumeOption,
              value: volumeOption,
            };
          }
        }
      }
    }

    function onSave() {
      // start filter process
      filterMarker();
      const filterSummary = generateFilterSummary();

      // store the filter data into global
      nvZonalRouting.routingFilters = ctrl.routingFilters;

      $mdDialog.hide(filterSummary);
    }

    function onCancel() {
      $mdDialog.cancel();
    }

    function filterMarker() {
      _.forEach(mapData.allWaypointMarkers, (marker) => {
        // skip those that are routed
        if (marker.options.routeId) {
          return undefined;
        }

        // start checking whether the marker meet the filter condition
        let isCorrectCondition = true;

        _.forEach(ctrl.routingFilters.selectedFilters, (filter) => {
          const isMarkerMeetFilterCondition = getIsMarkerMeetFilterCondition(filter, marker);
          if (isMarkerMeetFilterCondition !== null) {
            isCorrectCondition = isMarkerMeetFilterCondition;

            if (isCorrectCondition === false) {
              return false; // no necessary to scan for next item already
            }
          }

          return undefined;
        });

        // process marker opacity
        const isVisible = isCorrectCondition === nvModelUtils.fromYesNoBitStringToBool(
          ctrl.routingFilters.filterMode
        );
        marker.options.extraInfo.isHideByMapViewSettings = !isVisible;

        nvZonalRouting.toggleVisibilityMarker(marker, isVisible);

        return undefined;
      });

      function getIsMarkerMeetFilterCondition(filter, marker) {
        switch (filter.type) {
          case nvBackendFilterUtils.FILTER_OPTIONS:
          case nvBackendFilterUtils.FILTER_AUTOCOMPLETE:
            return filterByOptions(filter, marker);
          case nvBackendFilterUtils.FILTER_BOOLEAN:
            return filterByBoolean(filter, marker);
          case nvBackendFilterUtils.FILTER_TEXT:
            return filterByText(filter, marker);
          default:
            return false;
        }
      }

      function filterByOptions(filter, marker) {
        if (_.size(filter.selectedOptions) > 0) {
          let isOrCorrectCondition = false;
          _.forEach(filter.selectedOptions, (option) => {
            isOrCorrectCondition = ctrl.routingFilters.conditionCheckFn[filter.key](marker, option);

            if (isOrCorrectCondition) {
              return false; // no necessary to scan for next item already
            }

            return undefined;
          });

          return isOrCorrectCondition;
        }

        return null;
      }

      function filterByBoolean(filter, marker) {
        return ctrl.routingFilters.conditionCheckFn[filter.key](marker, filter);
      }

      function filterByText(filter, marker) {
        if (_.size(_.trim(filter.model)) > 0) {
          return ctrl.routingFilters.conditionCheckFn[filter.key](marker, filter);
        }

        return null;
      }
    }

    function generateFilterSummary() {
      const filterSummary = [];
      _.forEach(ctrl.routingFilters.selectedFilters, (filter) => {
        let booleanContent = null;

        switch (filter.type) {
          case nvBackendFilterUtils.FILTER_OPTIONS:
          case nvBackendFilterUtils.FILTER_AUTOCOMPLETE:
            if (_.size(filter.selectedOptions) > 0) {
              let key = 'displayName';

              // for parcel size, show short form of size (eg: 'S' instead of 'Small')
              if (filter.key === 'parcelSize') {
                key = 'displayNameShort';
              }

              filterSummary.push({
                title: filter.mainTitle,
                content: _.map(filter.selectedOptions, option =>
                  nvTranslate.instant(option[key])
                ).join(', '),
              });
            }
            return;
          case nvBackendFilterUtils.FILTER_BOOLEAN:
            booleanContent = filter.trueLabel;
            if (filter.booleanModel === false) {
              booleanContent = filter.falseLabel;
            }

            filterSummary.push({
              title: filter.mainTitle,
              content: booleanContent,
            });
            return;
          case nvBackendFilterUtils.FILTER_TEXT:
            if (_.size(_.trim(filter.model)) > 0) {
              filterSummary.push({
                title: filter.mainTitle,
                content: filter.model,
              });
            }
            return;
          default:
            return;
        }
      });

      return filterSummary;
    }

    function codConditionCheckFn(marker, filter) {
      return nvModelUtils.fromYesNoBitStringToBool(
          filter.booleanModel
      ) === marker.options.extraInfo.hasCOD;
    }

    function timewindowConditionCheckFn(marker, filterItem) {
      return filterItem.timewindowId === marker.options.extraInfo.timewindowId;
    }

    function parcelSizeConditionCheckFn(marker, filterItem) {
      if (marker.options.extraInfo.isReservation) {
        return false;
      }

      let isCorrectParcelSize = false;
      _.forEach(marker.options.transactions, (transaction) => {
        if (transaction.order && transaction.order.parcelSize === filterItem.enumValue) {
          isCorrectParcelSize = true;
          return false;
        }

        return undefined;
      });

      return isCorrectParcelSize;
    }

    function approxVolumeConditionCheckFn(marker, filterItem) {
      if (!marker.options.extraInfo.isReservation) {
        return false;
      }

      let isCorrectApproxVolume = false;
      _.forEach(marker.options.transactions, (reservation) => {
        if (reservation.approxVolume === filterItem.value) {
          isCorrectApproxVolume = true;
          return false;
        }

        return undefined;
      });

      return isCorrectApproxVolume;
    }

    function priorityConditionCheckFn(marker, filter) {
      return nvModelUtils.fromYesNoBitStringToBool(
          filter.booleanModel
      ) === marker.options.extraInfo.isPriority;
    }

    function priorityLevelConditionCheckFn(marker, filterItem) {
      let isCorrectCondition = true;

      if (filterItem.min !== null) {
        isCorrectCondition = filterItem.min <= marker.options.extraInfo.priorityLevel;
      }

      if (isCorrectCondition && filterItem.max !== null) {
        isCorrectCondition = marker.options.extraInfo.priorityLevel <= filterItem.max;
      }

      return isCorrectCondition;
    }

    function deliveryTypeConditionCheckFn(marker, filterItem) {
      return marker.options.extraInfo[filterItem.verifyKey];
    }

    function shipperConditionCheckFn(marker, filterItem) {
      let isPartOfSelected = false;
      const shipperIds = _.castArray(filterItem.id);
      _.forEach(marker.options.transactions, (transaction) => {
        let shipperId = 0;
        if (marker.options.extraInfo.isReservation) {
          shipperId = transaction.shipperId;
        } else {
          shipperId = transaction.order && transaction.order.shipperId;
        }

        if (shipperIds.indexOf(shipperId) >= 0) {
          isPartOfSelected = true;
          return false;
        }

        return undefined;
      });

      return isPartOfSelected;
    }

    function searchAddressCheckFn(marker, filter) {
      const wordings = _.split(filter.model.toLowerCase(), ' ');
      const address = marker.options.extraInfo.address ?
        marker.options.extraInfo.address.toLowerCase() : '';

      let isAllMatch = true;
      _.forEach(wordings, (wording) => {
        if (address.indexOf(wording) <= -1) {
          isAllMatch = false;
          return false;
        }

        return undefined;
      });

      return isAllMatch;
    }
  }
}());
