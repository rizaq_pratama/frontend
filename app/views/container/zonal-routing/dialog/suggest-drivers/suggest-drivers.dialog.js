(function controller() {
  angular
    .module('nvOperatorApp.controllers')
    .controller('ZonalRoutingSuggestDriversDialogController', ZonalRoutingSuggestDriversDialogController);

  ZonalRoutingSuggestDriversDialogController.$inject = [
    '$mdDialog', 'nvZonalRouting', 'nvBackendFilterUtils', 'nvTranslate', 'DriverType',
    'Zone', 'Hub', 'nvModelUtils', 'Driver', 'settingsData',
  ];

  function ZonalRoutingSuggestDriversDialogController(
    $mdDialog, nvZonalRouting, nvBackendFilterUtils, nvTranslate, DriverType,
    Zone, Hub, nvModelUtils, Driver, settingsData
  ) {
    // variables
    const NULL_ZONE = nvZonalRouting.getNullZoneData();
    const UNAV_ZONE = nvZonalRouting.getUnavailableZoneData();
    const INVALID_ZONES = [NULL_ZONE, UNAV_ZONE];
    const SORT_BY_TYPES = [undefined, 'asc', 'desc']; // no-sorting, ascending, descending
    const SORT_OPTION = {
      NAME: 'name',
    };
    const ctrl = this;
    ctrl.SORT_OPTION = SORT_OPTION;

    ctrl.driverFilters = {
      selectedFilters: [],
      possibleFilters: [],
      conditionCheckFn: {
        driverType: driverTypeConditionCheckFn,
        zoneId: zoneIdConditionCheckFn,
        hubId: hubIdConditionCheckFn,
        isRFDriver: isRFDriverConditionCheckFn,
        searchDriver: searchDriverConditionCheckFn,
      },
    };
    ctrl.suggestedDriversParam = {
      currSortIdx: 0,
      currSortOpt: null,
      list: [],
    };
    ctrl.addedDriversParam = {
      currSortIdx: 0,
      currSortOpt: null,
      list: [],
    };

    // functions
    ctrl.loadFilterSuggestions = loadFilterSuggestions;
    ctrl.performSortDriver = performSortDriver;
    ctrl.showDriversSortIcon = showDriversSortIcon;
    ctrl.addDriver = addDriver;
    ctrl.removeAddedDriver = removeAddedDriver;
    ctrl.onSave = onSave;
    ctrl.onCancel = onCancel;

    // start
    initialize();

    // functions details
    function initialize() {
      const filters = initFilter();
      ctrl.driverFilters.possibleFilters = filters.generalFilters;
      ctrl.driverFilters.selectedFilters = _.remove(ctrl.driverFilters.possibleFilters,
        filter => filter.showOnInit === true
      );

      function initFilter() {
        const generalFilters = [
          {
            type: nvBackendFilterUtils.FILTER_OPTIONS,
            showOnInit: true,
            possibleOptions: getDriverTypes(),
            selectedOptions: [],
            searchText: {},
            mainTitle: nvTranslate.instant('container.zonal-routing.driver-type'),
            key: 'driverType',
            presetKey: 'driverType',
          },
          {
            type: nvBackendFilterUtils.FILTER_OPTIONS,
            showOnInit: false,
            possibleOptions: getZones(),
            selectedOptions: [],
            searchText: {},
            mainTitle: nvTranslate.instant('container.zonal-routing.zone'),
            key: 'zoneId',
            presetKey: 'zoneId',
          },
          {
            type: nvBackendFilterUtils.FILTER_OPTIONS,
            showOnInit: false,
            possibleOptions: getHubs(),
            selectedOptions: [],
            searchText: {},
            mainTitle: nvTranslate.instant('commons.hub'),
            key: 'hubId',
            presetKey: 'hubId',
          },
          {
            type: nvBackendFilterUtils.FILTER_BOOLEAN,
            showOnInit: false,
            mainTitle: nvTranslate.instant('container.zonal-routing.is-rf-driver'),
            trueLabel: nvTranslate.instant('commons.yes'),
            falseLabel: nvTranslate.instant('commons.no'),
            booleanModel: nvModelUtils.fromBoolToYesNoBitString(true),
            key: 'isRFDriver',
            presetKey: 'isRFDriver',
          },
          {
            type: nvBackendFilterUtils.FILTER_TEXT,
            showOnInit: false,
            mainTitle: nvTranslate.instant('container.zonal-routing.search-driver'),
            placeholder: nvTranslate.instant('commons.enter-search-term'),
            model: null,
            key: 'searchDriver',
            presetKey: 'searchDriver',
          },
        ];

        return {
          generalFilters: generalFilters,
        };

        function getDriverTypes() {
          return _.sortBy(
            DriverType.toOptions(nvZonalRouting.driverTypes, 'id'), 'displayName'
          );
        }

        function getZones() {
          const editedZonesResult = _.cloneDeep(nvZonalRouting.zonesResult);
          editedZonesResult.zones = _.concat(editedZonesResult.zones, INVALID_ZONES);

          return _.sortBy(
            Zone.toOptions(editedZonesResult), 'displayName'
          );
        }

        function getHubs() {
          return _.sortBy(
            Hub.toOptions(nvZonalRouting.hubs), 'displayName'
          );
        }
      }
    }

    function loadFilterSuggestions() {
      ctrl.suggestedDriversParam.list = _.filter(
        _.cloneDeep(nvZonalRouting.driversTable.available), (driver) => {
          const driversToFilterOut = _.concat(
            nvZonalRouting.chosenZone.drivers, ctrl.addedDriversParam.list
          );
          const isFound = _.some(driversToFilterOut, theDriver =>
            driver.id === theDriver.id
          );

          if (isFound) {
            return false;
          }

          let isCorrectCondition = true;
          _.forEach(ctrl.driverFilters.selectedFilters, (filter) => {
            const isDriverMeetFilterCondition = getIsDriverMeetFilterCondition(filter, driver);
            if (isDriverMeetFilterCondition !== null) {
              isCorrectCondition = isDriverMeetFilterCondition;
              return false;
            }

            return _.noop();
          });

          return isCorrectCondition;
        }
      );
    }

    function performSortDriver(tableParam, option) {
      if (tableParam.currSortOpt === option) {
        tableParam.currSortIdx = (tableParam.currSortIdx + 1) % 3;
      } else {
        tableParam.currSortIdx = 1;
      }

      tableParam.currSortOpt = tableParam.currSortIdx === 0 ? null : option;
      sortingDriver(tableParam);
    }

    function sortingDriver(tableParam) {
      tableParam.list = !tableParam.currSortOpt
        ? _.sortBy(tableParam.list, 'id')
        : _.orderBy(tableParam.list, tableParam.currSortOpt, SORT_BY_TYPES[tableParam.currSortIdx]);
    }

    function showDriversSortIcon(tableParam, option) {
      if (tableParam.currSortOpt === option) {
        return (tableParam.currSortIdx === 1 ? 'fa-sort-asc' : 'fa-sort-desc');
      }

      return 'fa-sort';
    }

    function addDriver(driver) {
      _.remove(ctrl.suggestedDriversParam.list, ['id', driver.id]);
      ctrl.addedDriversParam.list.push(driver);

      if (ctrl.addedDriversParam.currSortOpt) {
        sortingDriver(ctrl.addedDriversParam, ctrl.addedDriversParam.currSortOpt);
      }
    }

    function removeAddedDriver(driver) {
      _.remove(ctrl.addedDriversParam.list, ['id', driver.id]);
      ctrl.suggestedDriversParam.list.push(driver);

      if (ctrl.suggestedDriversParam.currSortOpt) {
        sortingDriver(ctrl.suggestedDriversParam, ctrl.suggestedDriversParam.currSortOpt);
      }
    }

    function onSave() {
      _.forEach(ctrl.addedDriversParam.list, (driver) => {
        // check if assigned to another zone
        // remove driver from the zone first if assigned
        if (nvZonalRouting.isAssignedToZone(driver)) {
          if (settingsData.isImprovedVersion) {
            nvZonalRouting.addDriverToDriversByZonesImproved(driver);
          } else {
            nvZonalRouting.addDriverToDriversByZones(driver);
          }
        }

        // add driver to this zone
        nvZonalRouting.addDriverToZone(driver.id, nvZonalRouting.chosenZone.id);
      });

      $mdDialog.hide();
    }

    function onCancel() {
      $mdDialog.cancel();
    }

    function getIsDriverMeetFilterCondition(filter, driver) {
      switch (filter.type) {
        case nvBackendFilterUtils.FILTER_OPTIONS:
          return filterByOptions(filter, driver);
        case nvBackendFilterUtils.FILTER_BOOLEAN:
          return filterByBoolean(filter, driver);
        case nvBackendFilterUtils.FILTER_TEXT:
          return filterByText(filter, driver);
        default:
          return false;
      }
    }

    function filterByOptions(filter, driver) {
      if (_.size(filter.selectedOptions) > 0) {
        return _.some(filter.selectedOptions, option =>
          ctrl.driverFilters.conditionCheckFn[filter.key](driver, option)
        );
      }

      return null;
    }

    function filterByBoolean(filter, driver) {
      return ctrl.driverFilters.conditionCheckFn[filter.key](driver, filter);
    }

    function filterByText(filter, driver) {
      if (_.size(_.trim(filter.model)) > 0) {
        return ctrl.driverFilters.conditionCheckFn[filter.key](driver, filter);
      }

      return null;
    }

    function driverTypeConditionCheckFn(driver, filterItem) {
      return driver.driverTypeId === filterItem.value;
    }

    function zoneIdConditionCheckFn(driver, filterItem) {
      return nvZonalRouting.getDriverZoneId(driver) === filterItem.value;
    }

    function hubIdConditionCheckFn(driver, filterItem) {
      return driver.hubId === filterItem.value;
    }

    function isRFDriverConditionCheckFn(driver, filterItem) {
      return nvModelUtils.fromYesNoBitStringToBool(
        filterItem.booleanModel
      ) === Driver.isReserveFleetDriver(driver);
    }

    function searchDriverConditionCheckFn(driver, filterItem) {
      return nvZonalRouting.filterByDriverName(filterItem.model, driver);
    }
  }
}());
