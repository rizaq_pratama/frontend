(function controller() {
  angular
    .module('nvOperatorApp.controllers')
    .controller('ZonalRoutingDeleteRoutesDialogController', ZonalRoutingDeleteRoutesDialogController);

  ZonalRoutingDeleteRoutesDialogController.$inject = [
    '$mdDialog', 'nvZonalRouting', 'nvTable', 'nvTranslate',
  ];

  function ZonalRoutingDeleteRoutesDialogController(
    $mdDialog, nvZonalRouting, nvTable, nvTranslate
  ) {
    // variables
    const ctrl = this;
    ctrl.routesTableParams = null;

    // functions
    ctrl.onCancel = onCancel;
    ctrl.isDeleteButtonDisabled = isDeleteButtonDisabled;
    ctrl.deleteRoutes = deleteRoutes;

    // start
    initialize();

    // functions details
    function initialize() {
      ctrl.routesTableParams = nvTable.createTable({
        routeName: { displayName: 'commons.number-shortform' },
        driverName: { displayName: 'commons.driver' },
        capacity: { displayName: 'container.zonal-routing.capacity' },
      });
      ctrl.routesTableParams.setData(toRoutesData(nvZonalRouting.assignedRoutes));
    }

    function onCancel() {
      $mdDialog.cancel();
    }

    function isDeleteButtonDisabled() {
      return ctrl.routesTableParams.getSelection().length <= 0;
    }

    function deleteRoutes() {
      const ids = [];
      _.forEach(ctrl.routesTableParams.getSelection(), (route) => {
        ids.push(route.id);
      });

      $mdDialog.hide({ ids: ids });
    }

    function toRoutesData(routes) {
      return _.map(routes, route => (
        setCustomRouteData(route)
      ));

      function setCustomRouteData(theRoute) {
        const route = {
          id: theRoute.id,
          routeName: nvZonalRouting.getRouteName(theRoute.id),
          driverName: nvZonalRouting.getChosenZoneDriverName(
            theRoute.driverSelection.driver ? theRoute.driverSelection.driver.value : null
          ),
          capacity: theRoute.sizeCounter,
        };

        if (theRoute.driverSelection.driver) {
          route.capacity = nvTranslate.instant('commons.num-of-total', {
            num: theRoute.sizeCounter,
            total: nvZonalRouting.getChosenZoneDriverMaxCapacity(
              theRoute.driverSelection.driver.value
            ),
          });
        }

        return route;
      }
    }
  }
}());
