(function controller() {
  angular
    .module('nvOperatorApp.controllers')
    .controller('ZonalRoutingAutoAssignDialogController', ZonalRoutingAutoAssignDialogController);

  ZonalRoutingAutoAssignDialogController.$inject = [
    'nvZonalRouting', 'DriverTypeEligible', '$mdDialog', 'nvToast',
    'Route', 'nvTranslate', '$timeout', '$scope', 'nvTable',
  ];

  function ZonalRoutingAutoAssignDialogController(
    nvZonalRouting, DriverTypeEligible, $mdDialog, nvToast,
    Route, nvTranslate, $timeout, $scope, nvTable
  ) {
    // variables
    const DEBUG = nvZonalRouting.isDebug();
    const ctrl = this;

    let saveButtonState = 'clean';

    ctrl.form = {};
    ctrl.errors = [{
      key: 'setAsideExceedNP',
      msg: 'container.zonal-routing.set-aside-exceed-non-priority',
    }];

    // functions
    ctrl.isSaveButtonDisabled = isSaveButtonDisabled;
    ctrl.onCancel = onCancel;
    ctrl.autoAssignDrivers = autoAssignDrivers;
    ctrl.getMaxBuffer = nvZonalRouting.getMaxBuffer;
    ctrl.setAsideOnchange = setAsideOnchange;

    // start
    initialize();

    // functions details
    function initialize() {
      // filter out non-primary zones
      const primaryZones = _.filter(nvZonalRouting.zones, zone =>
        _.size(_.find(nvZonalRouting.zonesResult, ['id', zone.id])) > 0
      );

      ctrl.zonesTableParams = nvTable.createTable({
        name: { displayName: 'container.zonal-routing.area' },
        totalParcels: { displayName: 'container.zonal-routing.parcels' },
        priority: { displayName: 'container.zonal-routing.priorities' },
        parcelsToDo: { displayName: 'container.zonal-routing.parcels-to-do' },
        buffer: { displayName: 'container.zonal-routing.set-aside' },
      });
      ctrl.zonesTableParams.setData(primaryZones);
      ctrl.zonesTableParams.sort('name');
    }
    $scope.$on('$destroy', () => {
      ctrl.zonesTableParams.clearSelect();
    });

    function autoAssignDrivers() {
      // get selected zones to auto assign to
      const zones = [];
      _.forEach(ctrl.zonesTableParams.getSelection(), (zone) => {
        zones.push({
          id: zone.id,
          min: zone.priority,
          max: zone.priority + zone.nonPriority,
          buffer: +zone.buffer || 0,
        });
      });

      saveButtonState = 'saving';
      DriverTypeEligible.getPriorityIds().then((priorityIds) => {
        // get list of drivers by availability,
        // and the zone preferences for the available drivers
        const drivers = (nvZonalRouting.driversTable.all || [])
          .filter((driver) => {
            return driver.availability;
          })
          .map((driver) => {
            return {
              id: driver.id,
              zone_preferences: [{ // todo - only one for now!
                id: driver.zoneId,
                min: driver.min,
                max: driver.max,
                costs: driver.cost,
                assign_priorities: _.includes(priorityIds, driver.driverTypeId),
              }],
            };
          });

        if (drivers.length === 0) {
          nvToast.error(nvTranslate.instant('container.zonal-routing.error-no-available-drivers'));
          return;
        }

        // run the algorithm to assign drivers to zones
        if (DEBUG) {
          $.getJSON('views/container/zonal-routing/json/auto-assign-drivers.json', (autoAssignDriversResponse) => {
            $timeout(() => {
              success(autoAssignDriversResponse);
            });
          });
        } else {
          Route.autoAssignDrivers({
            zones: zones,
            drivers: drivers,
          }).then(success, failure);
        }

        function success(response) {
          const zoneAssignments = (response && response.zone_assignments) || [];
          if (zoneAssignments.length === 0) {
            nvToast.error(nvTranslate.instant('container.zonal-routing.error-no-zone-assignment'));
          } else {
            // "drag" each driver to the appropriate zone
            _.forEach(zoneAssignments, (zoneAssignment) => {
              _.forEach((zoneAssignment.driver_ids || []), (driverId) => {
                // ignore "rescueDriverForPs", which is returned if there is insufficient
                // drivers to fulfill the priority parcels for a particular zone
                if (!isNaN(+driverId)) {
                  // prevent duplicate add
                  const driver = nvZonalRouting.getDriverData(driverId);
                  if (driver && !nvZonalRouting.isAssignedToZone(driver)) {
                    nvZonalRouting.addDriverToZone(driverId, zoneAssignment.id);
                  }
                }
              });
            });

            $mdDialog.hide();
          }

          saveButtonState = 'clean';
        }

        function failure(error) {
          saveButtonState = 'clean';
          nvToast.error(
            nvTranslate.instant('container.zonal-routing.error-get-zone-assignment-from-vrp'),
            error.errorMessage
          );
        }
      });
    }

    function setAsideOnchange(zone, id) {
      $timeout(() => {
        if (zone.buffer <= ctrl.getMaxBuffer(zone)) {
          ctrl.form[id].$setValidity('setAsideExceedNP', true);
        } else {
          ctrl.form[id].$setValidity('setAsideExceedNP', false);
        }
      });
    }

    function isSaveButtonDisabled() {
      return !ctrl.zonesTableParams.getSelection().length ||
        saveButtonState === 'saving' ||
        (ctrl.form && _.size(ctrl.form.$error));
    }

    function onCancel() {
      $mdDialog.cancel();
    }
  }
}());
