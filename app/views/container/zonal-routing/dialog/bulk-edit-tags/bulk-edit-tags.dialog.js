(function controller() {
  angular
    .module('nvOperatorApp.controllers')
    .controller('ZonalRoutingBulkEditTagsDialogController', ZonalRoutingBulkEditTagsDialogController);

  ZonalRoutingBulkEditTagsDialogController.$inject = [
    '$mdDialog', 'routesData', 'Tag', 'nvZonalRouting', 'Route',
    'nvToast', 'nvTranslate',
  ];

  function ZonalRoutingBulkEditTagsDialogController(
    $mdDialog, routesData, Tag, nvZonalRouting, Route,
    nvToast, nvTranslate
  ) {
    // variables
    const ctrl = this;
    ctrl.formSubmitState = 'idle';
    ctrl.tagsSelectionOptions = Tag.toOptions({ tags: nvZonalRouting.routeTags });
    ctrl.selectedTags = [];

    // functions
    ctrl.onSave = onSave;
    ctrl.onCancel = onCancel;

    // functions details
    function onSave() {
      ctrl.formSubmitState = 'waiting';

      const payload = [];
      const routeIds = _.map(routesData, 'id');
      const tagNames = [];
      _.forEach(routesData, (route) => {
        payload.push({
          routeId: route.id,
          tagIds: ctrl.selectedTags,
        });
      });

      _.forEach(ctrl.selectedTags, (tagId) => {
        tagNames.push(getTagName(tagId));
      });

      Route.updateTags(payload).then(success, failure);

      function success() {
        ctrl.formSubmitState = 'idle';

        nvToast.info(
          nvTranslate.instant('container.zonal-routing.num-route-tagged', { num: routeIds.length }),
          nvTranslate.instant('container.zonal-routing.num-route-tagged-description', { routes: routeIds.join(', '), tags: tagNames.join(', ') || '-' })
        );

        $mdDialog.hide({
          routeIds: routeIds,
          tagIds: ctrl.selectedTags,
        });
      }

      function failure() {
        ctrl.formSubmitState = 'idle';
      }
    }

    function onCancel() {
      $mdDialog.cancel();
    }

    function getTagData(tagId) {
      return _.find(nvZonalRouting.routeTags, ['id', tagId]);
    }

    function getTagName(tagId) {
      const tag = getTagData(tagId);

      if (tag) {
        return tag.name;
      }

      return '-';
    }
  }
}());
