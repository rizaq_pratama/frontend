(function controller() {
  angular
    .module('nvOperatorApp.controllers')
    .controller('ZonalRoutingSetParcelsAsideDialogController', ZonalRoutingSetParcelsAsideDialogController);

  ZonalRoutingSetParcelsAsideDialogController.$inject = [
    '$mdDialog', 'nvZonalRouting', 'selectedZone', '$timeout',
  ];

  function ZonalRoutingSetParcelsAsideDialogController(
    $mdDialog, nvZonalRouting, selectedZone, $timeout
  ) {
    // variables
    const ctrl = this;
    ctrl.buffer = selectedZone.buffer;
    ctrl.saveButtonState = 'clean';

    ctrl.form = {};
    ctrl.errors = [{
      key: 'setAsideExceedNP',
      msg: 'container.zonal-routing.set-aside-exceed-non-priority',
    }];

    // functions
    ctrl.getMaxBuffer = getMaxBuffer;
    ctrl.onCancel = onCancel;
    ctrl.onSave = onSave;
    ctrl.setAsideOnchange = setAsideOnchange;

    // functions details
    function getMaxBuffer() {
      return nvZonalRouting.getMaxBuffer(selectedZone);
    }

    function onSave() {
      selectedZone.buffer = ctrl.buffer || 0;
      $mdDialog.hide();
    }

    function setAsideOnchange(id) {
      $timeout(() => {
        if (ctrl.buffer <= ctrl.getMaxBuffer(selectedZone)) {
          ctrl.form[id].$setValidity('setAsideExceedNP', true);
        } else {
          ctrl.form[id].$setValidity('setAsideExceedNP', false);
        }
      });
    }

    function onCancel() {
      $mdDialog.cancel();
    }
  }
}());
