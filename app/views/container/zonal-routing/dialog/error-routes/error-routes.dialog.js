(function controller() {
  angular
    .module('nvOperatorApp.controllers')
    .controller('ZonalRoutingErrorRoutesDialogController', ZonalRoutingErrorRoutesDialogController);

  ZonalRoutingErrorRoutesDialogController.$inject = ['$mdDialog', 'nvTranslate', 'errorRoutes', 'Waypoint',
    'nvToast', 'Route'];

  const API_STATE = {
    IDLE: 'idle',
    WAITING: 'waiting',
  };

  function ZonalRoutingErrorRoutesDialogController($mdDialog, nvTranslate, errorRoutes, Waypoint,
    nvToast, Route) {
    const ctrl = this;
    const SUBMITTED_ROUTES = []; // container for successfully created route

    ctrl.errorRoutes = _.cloneDeep(errorRoutes);

    ctrl.onCancel = onCancel;
    ctrl.onClose = onClose;
    ctrl.containsError = containsError;
    ctrl.setWaypointToPending = setWaypointToPending;

    init();

    function init() {
      _.forEach(ctrl.errorRoutes, (route) => {
        route.state = API_STATE.IDLE;
        route.disabled = false;
        route.error = true;
      });
    }
    function onCancel() {
      nvToast.error(
        nvTranslate.instant('container.zonal-routing.error-routes-can-not-cancel')
      );
    }

    function setWaypointToPending(route) {
      route.state = API_STATE.WAITING;
      const payload = [];
      _.forEach(route.wpIds, (wpId) => {
        payload.push({
          id: wpId,
          status: 'PENDING',
          rawAddressFlag: false,
        });
      });

      Waypoint.update(payload)
        .then(() => {
          Route.createRoutes(route.request)
            .then((response) => {
              route.error = false;
              SUBMITTED_ROUTES.push(_.head(response));
              nvToast.success(nvTranslate.instant('container.zonal-routing.route-created', {
                routeId: _.get(_.head(response), 'id'),
              }));
            }, (response) => {
              if (response.status === 400 && _.isArray(response.data)) {
                route.wpIds = response.data;
              }
            })
            .finally(() => (route.state = API_STATE.IDLE));
        }, () => (route.state = API_STATE.IDLE));
    }

    function containsError() {
      return _.filter(ctrl.errorRoutes, route => route.error).length > 0;
    }

    function onClose() {
      $mdDialog.hide(SUBMITTED_ROUTES);
    }
  }
}());
