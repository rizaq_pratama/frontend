(function controller() {
  angular
    .module('nvOperatorApp.controllers')
    .controller('ZonalRoutingSetHubForRoutesDialogController', ZonalRoutingSetHubForRoutesDialogController);

  ZonalRoutingSetHubForRoutesDialogController.$inject = [
    '$mdDialog', 'nvZonalRouting', 'nvTable', 'Hub',
  ];

  function ZonalRoutingSetHubForRoutesDialogController(
    $mdDialog, nvZonalRouting, nvTable, Hub
  ) {
    // variables
    const ctrl = this;
    ctrl.routesTableParams = null;
    ctrl.hubSelection = {
      options: [],
      searchText: '',
      hub: null,
    };

      // functions
    ctrl.onCancel = onCancel;
    ctrl.isSetHubForButtonDisabled = isSetHubForButtonDisabled;
    ctrl.setHubForRoutes = setHubForRoutes;

    // start
    initialize();

    // functions details
    function initialize() {
      ctrl.routesTableParams = nvTable.createTable({
        routeName: { displayName: 'commons.number-shortform' },
        driverName: { displayName: 'commons.driver' },
        hubName: { displayName: 'container.zonal-routing.current-hub' },
      });
      ctrl.routesTableParams.setData(toRoutesData(nvZonalRouting.assignedRoutes));

      ctrl.hubSelection.options = Hub.toOptions(nvZonalRouting.hubs);
    }

    function onCancel() {
      $mdDialog.cancel();
    }

    function isSetHubForButtonDisabled() {
      return ctrl.routesTableParams.getSelection().length <= 0 ||
        ctrl.hubSelection.hub === null;
    }

    function setHubForRoutes() {
      const routeIds = [];
      _.forEach(ctrl.routesTableParams.getSelection(), (route) => {
        routeIds.push(route.id);
      });

      $mdDialog.hide({
        routeIds: routeIds,
        hubId: ctrl.hubSelection.hub.value,
      });
    }

    function toRoutesData(routes) {
      return _.map(routes, route => (
        setCustomRouteData(route)
      ));

      function setCustomRouteData(theRoute) {
        const route = {
          id: theRoute.id,
          routeName: nvZonalRouting.getRouteName(theRoute.id),
          driverName: nvZonalRouting.getChosenZoneDriverName(
            theRoute.driverSelection.driver ? theRoute.driverSelection.driver.value : null
          ),
          hubName: null,
        };

        if (theRoute.hubSelection.hub) {
          route.hubName = nvZonalRouting.getHubName(
            theRoute.hubSelection.hub.value
          );
        }

        return route;
      }
    }
  }
}());
