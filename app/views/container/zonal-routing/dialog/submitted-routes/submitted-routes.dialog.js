(function controller() {
  angular
    .module('nvOperatorApp.controllers')
    .controller('ZonalRoutingSubmittedRoutesDialogController', ZonalRoutingSubmittedRoutesDialogController);

  ZonalRoutingSubmittedRoutesDialogController.$inject = [
    '$mdDialog', 'nvTranslate', 'submittedRoutes', 'nvTable',
    'nvToast', 'Tag', 'nvZonalRouting', 'Route', 'nvDialog',
  ];

  function ZonalRoutingSubmittedRoutesDialogController(
    $mdDialog, nvTranslate, submittedRoutes, nvTable,
    nvToast, Tag, nvZonalRouting, Route, nvDialog
  ) {
    // variables
    const ctrl = this;

    // functions
    ctrl.onCancel = onCancel;
    ctrl.updateTag = updateTag;
    ctrl.getSelectedCount = getSelectedCount;
    ctrl.bulkEditTags = bulkEditTags;

    // start
    initialize();

    // functions details
    function initialize() {
      ctrl.tagsSelectionOptions = Tag.toOptions({ tags: nvZonalRouting.routeTags });

      ctrl.routesTableParams = nvTable.createTable({
        id: { displayName: 'commons.id' },
        driverName: { displayName: 'commons.driver-name' },
        tags: { displayName: 'container.zonal-routing.tags' },
      });
      ctrl.routesTableParams.setData(extend(submittedRoutes));
    }

    function onCancel() {
      $mdDialog.cancel();
    }

    function updateTag(route) {
      if (_.isEqual(route.tags, route.previousTags)) { return; }

      const payload = [{
        routeId: route.id,
        tagIds: route.tags,
      }];
      const routeIds = [route.id];
      const tagNames = [];

      _.forEach(route.tags, (tagId) => {
        tagNames.push(getTagName(tagId));
      });

      Route.updateTags(payload).then(success);

      function success() {
        route.previousTags = route.tags;
        nvToast.info(
          nvTranslate.instant('container.zonal-routing.num-route-tagged', { num: routeIds.length }),
          nvTranslate.instant('container.zonal-routing.num-route-tagged-description', { routes: routeIds.join(', '), tags: tagNames.join(', ') || '-' })
        );
      }
    }

    function getSelectedCount() {
      return ctrl.routesTableParams.getSelection().length;
    }

    function bulkEditTags($event) {
      nvDialog.showSingle($event, {
        templateUrl: 'views/container/zonal-routing/dialog/bulk-edit-tags/bulk-edit-tags.dialog.html',
        cssClass: 'zonal-routing-bulk-edit-tags',
        controller: 'ZonalRoutingBulkEditTagsDialogController',
        controllerAs: 'ctrl',
        skipHide: true,
        locals: {
          routesData: ctrl.routesTableParams.getSelection(),
        },
      }).then(success);

      function success(response) {
        _.forEach(response.routeIds, (routeId) => {
          const route = _.find(ctrl.routesTableParams.data, ['id', routeId]);
          if (route) {
            route.tags = response.tagIds;
          }
        });

        ctrl.routesTableParams.clearSelect();
      }
    }

    function extend(routes) {
      return _.map(routes, route => (
        setCustomRouteData(route)
      ));
    }

    function setCustomRouteData(route) {
      route.driverName = route.driver !== null ? _.compact([
        _.trim(route.driver.firstName), _.trim(route.driver.lastName)
      ]).join(' ')  : 'NA';
      route.previousTags = route.tags;

      return route;
    }

    function getTagData(tagId) {
      return _.find(nvZonalRouting.routeTags, ['id', tagId]);
    }

    function getTagName(tagId) {
      const tag = getTagData(tagId);

      if (tag) {
        return tag.name;
      }

      return '-';
    }
  }
}());
