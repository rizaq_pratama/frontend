(function controller() {
  angular
    .module('nvOperatorApp.controllers')
    .controller('ZonalRoutingViewWaypointsDialogController', ZonalRoutingViewWaypointsDialogController);

  ZonalRoutingViewWaypointsDialogController.$inject = [
    '$mdDialog', 'nvTranslate', 'selectedRoute', 'nvTable',
    'nvDateTimeUtils',
  ];

  function ZonalRoutingViewWaypointsDialogController(
    $mdDialog, nvTranslate, selectedRoute, nvTable,
    nvDateTimeUtils
  ) {
    // variables
    const ctrl = this;
    const addedMarkersMap = {};

    // functions
    ctrl.onCancel = onCancel;
    ctrl.isRemoveButtonDisabled = isRemoveButtonDisabled;
    ctrl.removeWaypoints = removeWaypoints;

    // start
    initialize();

    // functions details
    function initialize() {
      const addedMarkers = _.cloneDeep(selectedRoute.addedMarkers);
      _.forEach(addedMarkers, (marker) => {
        addedMarkersMap[marker.options.waypoint.id] = marker;
      });

      ctrl.waypointsTableParams = nvTable.createTable({
        id: { displayName: 'commons.id' },
        type: { displayName: 'commons.type' },
        date: { displayName: 'commons.model.date' },
        timewindowId: { displayName: 'container.zonal-routing.timewindow-id' },
        address: { displayName: 'commons.address' },
        isPriority: { displayName: 'commons.priority' },
      });
      ctrl.waypointsTableParams.setData(toWaypointsData(addedMarkers));
    }

    function onCancel() {
      $mdDialog.cancel();
    }

    function isRemoveButtonDisabled() {
      return ctrl.waypointsTableParams.getSelection().length <= 0;
    }

    function removeWaypoints() {
      const markersToRemove = [];
      _.forEach(ctrl.waypointsTableParams.getSelection(), (waypoint) => {
        markersToRemove.push(addedMarkersMap[waypoint.id]);
      });

      $mdDialog.hide({ markers: markersToRemove });
    }

    function toWaypointsData(addedMarkers) {
      return _.map(addedMarkers, marker => (
        setCustomWaypointData(marker)
      ));
    }

    function setCustomWaypointData(marker) {
      const waypoint = {};
      _.defaultsDeep(waypoint, marker.options.waypoint);

      waypoint.date = nvDateTimeUtils.displayDateTime(
        nvDateTimeUtils.toMoment(waypoint.date), 'UTC'
      );

      waypoint.address = _.compact([
        waypoint.address1, waypoint.address2, waypoint.postcode,
      ]).join(' ');

      if (marker.options.extraInfo.isPriority) {
        waypoint.isPriority = nvTranslate.instant('commons.yes');
      } else {
        waypoint.isPriority = nvTranslate.instant('commons.no');
      }

      return waypoint;
    }
  }
}());
