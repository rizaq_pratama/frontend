(function controller() {
  angular
    .module('nvOperatorApp.controllers')
    .controller('ZonalRoutingMapViewOptionsMenuController', ZonalRoutingMapViewOptionsMenuController);

  ZonalRoutingMapViewOptionsMenuController.$inject = [
    'mdPanelRef', 'onClose', 'nvZonalRouting',
  ];

  function ZonalRoutingMapViewOptionsMenuController(
    mdPanelRef, onClose, nvZonalRouting
  ) {
    // variables
    const ctrl = this;

    ctrl.mapViewOptions = nvZonalRouting.MAP_VIEW_OPTIONS;

    // functions
    ctrl.chooseAction = chooseAction;

    // functions details
    function chooseAction(actionValue) {
      mdPanelRef.close().then(() => {
        onClose({ action: actionValue });
      });
    }
  }
}());
