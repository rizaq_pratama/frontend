(function controller() {
  angular
    .module('nvOperatorApp.controllers')
    .controller('ZonalRoutingPolygonToolsMenuController', ZonalRoutingPolygonToolsMenuController);

  ZonalRoutingPolygonToolsMenuController.$inject = [
    'mdPanelRef', 'onClose', 'polygonToolsAction',
  ];

  function ZonalRoutingPolygonToolsMenuController(
    mdPanelRef, onClose, polygonToolsAction
  ) {
    // variables
    const ctrl = this;

    ctrl.polygonToolsAction = polygonToolsAction;

    // functions
    ctrl.chooseAction = chooseAction;

    // functions details
    function chooseAction(actionValue) {
      mdPanelRef.close().then(() => {
        onClose({ action: actionValue });
      });
    }
  }
}());
