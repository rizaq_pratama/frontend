(function controller() {
  angular
    .module('nvOperatorApp.controllers')
    .controller('ZonalRoutingRouteMenuController', ZonalRoutingRouteMenuController);

  ZonalRoutingRouteMenuController.$inject = [
    'mdPanelRef', 'nvZonalRouting', 'onClose', 'routeData',
  ];

  function ZonalRoutingRouteMenuController(
    mdPanelRef, nvZonalRouting, onClose, routeData
  ) {
    // variables
    const ctrl = this;

    ctrl.ROUTE_PANEL_ACTION = nvZonalRouting.ROUTE_PANEL_ACTION;

    // functions
    ctrl.chooseAction = chooseAction;
    ctrl.isMenuDisabled = isMenuDisabled;

    // functions details
    function chooseAction(actionValue) {
      mdPanelRef.close().then(() => {
        onClose({ action: actionValue });
      });
    }

    function isMenuDisabled(actionValue) {
      if (actionValue === nvZonalRouting.ROUTE_PANEL_ACTION.PURGE_ROUTE) {
        return routeData.priority + routeData.nonPriority <= 0;
      }

      return false;
    }
  }
}());
