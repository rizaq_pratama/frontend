(function controller() {
  angular
    .module('nvOperatorApp.controllers')
    .controller('ZonalRoutingAllRoutesMenuController', ZonalRoutingAllRoutesMenuController);

  ZonalRoutingAllRoutesMenuController.$inject = [
    'mdPanelRef', 'nvZonalRouting', 'onClose',
  ];

  function ZonalRoutingAllRoutesMenuController(
    mdPanelRef, nvZonalRouting, onClose
  ) {
    // variables
    const ctrl = this;

    ctrl.ALL_ROUTES_PANEL_ACTION = nvZonalRouting.ALL_ROUTES_PANEL_ACTION;

    // functions
    ctrl.chooseAction = chooseAction;
    ctrl.isMenuDisabled = isMenuDisabled;

    // functions details
    function chooseAction(actionValue) {
      mdPanelRef.close().then(() => {
        onClose({ action: actionValue });
      });
    }

    function isMenuDisabled(actionValue) {
      if (actionValue === nvZonalRouting.ALL_ROUTES_PANEL_ACTION.DELETE_ROUTES) {
        return _.size(nvZonalRouting.assignedRoutes) <= 0;
      } else if (actionValue === nvZonalRouting.ALL_ROUTES_PANEL_ACTION.PURGE_ROUTES) {
        let hasRoutedMarkers = false;
        _.forEach(nvZonalRouting.assignedRoutes, (route) => {
          if ((route.priority + route.nonPriority) > 0) {
            hasRoutedMarkers = true;
            return false;
          }

          return undefined;
        });

        return !hasRoutedMarkers;
      } else if (actionValue === nvZonalRouting.ALL_ROUTES_PANEL_ACTION.SET_HUB_FOR_ROUTES) {
        return _.size(nvZonalRouting.assignedRoutes) <= 0;
      }

      return false;
    }
  }
}());
