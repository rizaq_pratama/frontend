(function controller() {
  angular
    .module('nvOperatorApp.controllers')
    .controller('ZRPolygonToolsApplyActionController', ZRPolygonToolsApplyActionController);

  ZRPolygonToolsApplyActionController.$inject = [
    'mdPanelRef', 'nvZonalRouting', 'onClose',
  ];

  function ZRPolygonToolsApplyActionController(
    mdPanelRef, nvZonalRouting, onClose
  ) {
    // variables
    const ctrl = this;

    ctrl.POLYGON_TOOLS_APPLY_ACTION = nvZonalRouting.POLYGON_TOOLS_APPLY_ACTION;

    // functions
    ctrl.chooseAction = chooseAction;

    // functions details
    function chooseAction(value) {
      mdPanelRef.close().then(() => {
        onClose({ value: value });
      });
    }
  }
}());
