(function controller() {
  angular
    .module('nvOperatorApp.controllers')
    .controller('ZonalRoutingAssignmentController', ZonalRoutingAssignmentController);

  ZonalRoutingAssignmentController.$inject = [
    'nvZonalRouting', 'Zone', 'appSidenav', '$scope', 'nvNavGuard',
    '$timeout', 'Driver', 'DriverTypeEligible', '$q', 'nvMaps',
    'Route', 'nvDialog', 'nvDateTimeUtils', 'nvToast', 'nvFileUtils',
    'DriverTemporaryZone', 'Waypoint', 'Transaction', 'nvTranslate', '$state',
    'nvTable', '$stateParams', 'RouteGroup', 'Order', '$interval',
    'DriverType', 'Hub',
  ];

  function ZonalRoutingAssignmentController(
    nvZonalRouting, Zone, appSidenav, $scope, nvNavGuard,
    $timeout, Driver, DriverTypeEligible, $q, nvMaps,
    Route, nvDialog, nvDateTimeUtils, nvToast, nvFileUtils,
    DriverTemporaryZone, Waypoint, Transaction, nvTranslate, $state,
    nvTable, $stateParams, RouteGroup, Order, $interval,
    DriverType, Hub
  ) {
    // variables
    const DEBUG = nvZonalRouting.isDebug();
    const ALL_ZONES_ID = nvZonalRouting.getAllZonesId();
    const DEFAULT_LAT = nvMaps.getDefaultLatLngForDomain().lat;
    const DEFAULT_LNG = nvMaps.getDefaultLatLngForDomain().lng;
    const NULL_ZONE = nvZonalRouting.getNullZoneData();
    const UNAV_ZONE = nvZonalRouting.getUnavailableZoneData();
    const RESERVATION_ZONE = nvZonalRouting.getReservationZoneData();
    const INVALID_ZONES = [NULL_ZONE, UNAV_ZONE];
    const ROUTE_TYPE = {
      NORMAL: 'normal',
      BULKY: 'bulky',
      VOLUMETRIC: 'volumetric',
    };

    const ZONE_CONDITIONS = {
      INSUFFICIENT: 'container.zonal-routing.insufficient',
      SUFFICIENT: 'container.zonal-routing.sufficient',
      PRIORITY_SUFFICIENT: 'container.zonal-routing.priority-sufficient',
      NA: 'container.zonal-routing.na',
      UNKNOWN: 'container.zonal-routing.unknown',
    };

    const DND_DRIVER_FROM_DRIVER_LIST_TYPE = nvZonalRouting.getDndDriverFromDriverList();
    const DND_DRIVERS_BY_ZONE_TYPE = nvZonalRouting.getDndDriversByZone();
    const DND_DRIVER_FROM_ZONE_TYPE = nvZonalRouting.getDndDriverFromZoneAssignment();


    const parentContentLoading = $scope.contentLoading;

    // variables (that also use in html)
    const ctrl = this;

    ctrl.contentLoading = {
      status: true,
      message: '',
    };

    ctrl.zonesDndAllowedTypes = [
      DND_DRIVERS_BY_ZONE_TYPE,
      DND_DRIVER_FROM_DRIVER_LIST_TYPE,
      DND_DRIVER_FROM_ZONE_TYPE,
    ];
    ctrl.DND_DRIVER_FROM_DRIVER_LIST_TYPE = DND_DRIVER_FROM_DRIVER_LIST_TYPE;
    ctrl.DND_DRIVERS_BY_ZONE_TYPE = DND_DRIVERS_BY_ZONE_TYPE;
    ctrl.DND_DRIVER_FROM_ZONE_TYPE = DND_DRIVER_FROM_ZONE_TYPE;
    ctrl.ZONE_CONDITIONS = ZONE_CONDITIONS;
    ctrl.ALL_ZONES_ID = ALL_ZONES_ID;
    ctrl.ROUTE_TYPE = ROUTE_TYPE;

    ctrl.zonesForm = null;
    ctrl.routeGroupOptions = [];
    ctrl.zoneOptions = [];
    ctrl.isFilterExpanded = false;

    // functions
    ctrl.windowSizeChanged = $scope.windowSizeChanged;

    // Action Bar functions
    ctrl.editFetchTransactionsSetting = editFetchTransactionsSetting;
    ctrl.isAbleToFetchTransactions = isAbleToFetchTransactions;
    ctrl.fetchTransactions = fetchTransactions;

    ctrl.showAutoAssignDriversModal = showAutoAssignDriversModal;
    ctrl.updateDriverAssignment = updateDriverAssignment;
    ctrl.resetDriversAssignment = resetDriversAssignment;
    ctrl.emptyDriversAssignment = emptyDriversAssignment;

    ctrl.isNormalDriver = isNormalDriver;
    ctrl.isReserveFleetDriver = isReserveFleetDriver;

    // Zone Assignment and Driver List functions
    ctrl.routeZone = routeZone;
    ctrl.sendDriversAssignment = sendDriversAssignment;
    ctrl.mergeTransactions = mergeTransactions;
    ctrl.unmergeTransactions = unmergeTransactions;
    ctrl.setBuff = setBuff;
    ctrl.clearDriversByZone = clearDriversByZone;
    ctrl.resizeWithOffset = resizeWithOffset;
    ctrl.getZoneCondition = getZoneCondition;

    ctrl.refreshDriverAssignmentOnDrop = refreshDriverAssignmentOnDrop;
    ctrl.driverPreferredZonesSelectionChanged = driverPreferredZonesSelectionChanged;
    ctrl.hubsSelectionChanged = hubsSelectionChanged;
    ctrl.driverTypesSelectionChanged = driverTypesSelectionChanged;
    ctrl.showRFDriversCheckboxChanged = showRFDriversCheckboxChanged;

    // start
    initialize();

    // functions details
    // //////////////////////////////////////////////////////////////////////////////
    // Initialization //////////////////////////////////////////////////////////////
    // //////////////////////////////////////////////////////////////////////////////
    function initialize() {
      appSidenav.setConfigLockedOpen(true);
      ctrl.routeGroupOptions = RouteGroup.toOptions(nvZonalRouting.routeGroups, 'value', 'desc');
      ctrl.zoneOptions = Zone.toOptions(nvZonalRouting.zonesResult);
      if (nvZonalRouting.zoneTransactionsResult !== null) {
        // if clicking `back` button from routing page
        if (!DEBUG) {
          nvNavGuard.lock();
        }

        ctrl.contentLoading.status = false;
        parentContentLoading.backdropLoading = false;

        if (nvZonalRouting.routesSubmitted) {
          fetchTransactions();
          nvZonalRouting.routesSubmitted = false;
        } else {
          // user may remove assigned driver in routing page
          // so have to refresh zone assignment data
          updateZoneAssignmentData();
        }
      } else {
        nvZonalRouting.routeGroupIds = getRouteGroupIdsFromParam();
        if (_.size(nvZonalRouting.routeGroupIds) > 0) {
          nvZonalRouting.fetchTransactionsMode = '01';
        }

        loadZoneDrivers().then(loadZoneDriversSuccess, loadZoneDriversFailure);
      }

      function getRouteGroupIdsFromParam() {
        const ids = [];
        if (_.size($stateParams.route_group_ids)) {
          if (_.isArray($stateParams.route_group_ids)) {
            _.forEach($stateParams.route_group_ids, (id) => {
              if (!_.isNaN(_.parseInt(id))) {
                ids.push(_.parseInt(id));
              }
            });
          } else if (!_.isNaN(_.parseInt($stateParams.route_group_ids))) {
            ids.push(_.parseInt($stateParams.route_group_ids));
          }
        }

        return ids;
      }

      function loadZoneDriversSuccess() {
        if (nvZonalRouting.isFetchByRouteGroup()) {
          fetchTransactions();
        }

        ctrl.windowSizeChanged();
        ctrl.contentLoading.status = false;
      }

      function loadZoneDriversFailure() {
        ctrl.contentLoading.status = false;
      }
    }

    function loadZoneDrivers(isResetZoneDrivers = false) {
      if (!parentContentLoading.backdropLoading) {
        ctrl.contentLoading.status = true;
        ctrl.contentLoading.message = 'container.zonal-routing.load-data';
      }

      if (DEBUG) {
        return $.getJSON('views/container/zonal-routing/json/drivers.json', (drivers) => {
          $.getJSON('views/container/zonal-routing/json/driver-temporary-zones.json', (driverTemporaryZones) => {
            $timeout(() => {
              success([drivers.data, driverTemporaryZones.data]);
            });
          });
        });
      }

      return $q.all(
        [Driver.searchAll(), DriverTemporaryZone.read()]
      ).then(success);

      function success(responses) {
        // store to cache
        nvZonalRouting.driversResult = _.cloneDeep(responses[0]);
        nvZonalRouting.driverTemporaryZonesResult = _.cloneDeep(responses[1]);

        // processing default data
        nvZonalRouting.driversWithTemporaryZones = nvZonalRouting
          .driverTemporaryZonesResult.drivers;
        nvZonalRouting.originalDriversWithTemporaryZones = _.cloneDeep(
          nvZonalRouting.driversWithTemporaryZones
        );

        let sessionZones;
        if (isResetZoneDrivers) {
          sessionZones = _.cloneDeep(nvZonalRouting.zones);
        }

        processZoneAssignmentTableDatas();
        processDriverListTableDatas();

        // processing custom data
        moveTempZoneDriversToZones();

        if (isResetZoneDrivers) {
          // reload back zone assignment session data
          _.forEach(sessionZones, (sessionZone) => {
            const zone = nvZonalRouting.getZoneData(sessionZone.id);
            zone.buffer = sessionZone.buffer;
            if (zone.buffer) {
              updateZoneParcelCount(zone, {
                priority: zone.priority,
                nonPriority: zone.nonPriority,
              });
            }
          });
        }
      }
    }

    function processZoneAssignmentTableDatas() {
      // common vars
      const zones = nvZonalRouting.zonesResult;

      // processing data
      nvZonalRouting.zones = [];

      _.concat(zones, INVALID_ZONES, RESERVATION_ZONE).forEach((zone) => {
        nvZonalRouting.zones.push(newZoneObj(zone));
      });
      nvZonalRouting.zonesTableParams = nvTable.createTable({
        name: { displayName: 'container.zonal-routing.area' },
        totalParcels: { displayName: 'container.zonal-routing.parcels' },
        priority: { displayName: 'container.zonal-routing.priorities' },
        parcelsToDo: { displayName: 'container.zonal-routing.parcels-to-do' },
        buffer: { displayName: 'container.zonal-routing.set-aside' },
        condition: { displayName: 'container.zonal-routing.condition' },
        capacity: { displayName: 'container.zonal-routing.assigned-drivers' },
      });
      nvZonalRouting.zonesTableParams.setData(nvZonalRouting.zones);
      nvZonalRouting.zonesTableParams.sort('name');
      nvZonalRouting.zonesTableParams.setSortFunction('condition', conditionSortFunction);
      nvZonalRouting.zonesTableParams.setSortFunction('capacity', capacitySortFunction);

      updateZonePriorities(nvZonalRouting.zoneTransactions);

      function newZoneObj(zone) {
        const zoneAssignmentObj = {
          id: zone.id,
          name: zone.name,
          hubId: zone.hub_id,
          min: 0,
          max: 0,
          priority: 0,
          nonPriority: 0,
          totalParcels: 0,
          buffer: 0,
          parcelsToDo: 0,
          driversTabExpanded: true,
          drivers: [],
          isCustomZone: false,
        };

        // extra vars for custom zone
        if (zone.id === RESERVATION_ZONE.id) {
          zoneAssignmentObj.isCustomZone = true;
          zoneAssignmentObj.extraData = {
            waypoints: [],
            count: {
              reservations: 0,
              returns: 0,
              rts: 0,
              bulkMove: 0,
              extraLarge: 0,
            },
            zoneId: RESERVATION_ZONE.zoneId, // the real zone id
          };
        }

        return zoneAssignmentObj;
      }

      function conditionSortFunction(theZones, order) {
        return _.orderBy(theZones, [sortConditions, 'id'], order);

        function sortConditions(zone) {
          return getZoneCondition(zone);
        }
      }

      function capacitySortFunction(theZones, order) {
        return _.orderBy(theZones, [sortCapacity, 'id'], order);

        function sortCapacity(zone) {
          return zone.max;
        }
      }
    }

    function processDriverListTableDatas() {
      // common vars
      nvZonalRouting.driversTable.all = nvZonalRouting.driversResult.drivers;
      nvZonalRouting.driversTable.available = [];
      const zones = nvZonalRouting.zonesResult;

      // selection box
      let editedZonesResult = _.cloneDeep(nvZonalRouting.zonesResult);
      editedZonesResult = _.concat(editedZonesResult, INVALID_ZONES);

      if (_.size(nvZonalRouting.driverPreferredZonesSelection.options) <= 0) {
        nvZonalRouting.driverPreferredZonesSelection.options = _.sortBy(
          Zone.toOptions(editedZonesResult), 'displayName'
        );
      }

      if (_.size(nvZonalRouting.hubsSelection.options) <= 0) {
        nvZonalRouting.hubsSelection.options = _.sortBy(
          Hub.toOptions(nvZonalRouting.hubs), 'displayName'
        );
      }

      if (_.size(nvZonalRouting.driverTypesSelection.options) <= 0) {
        nvZonalRouting.driverTypesSelection.options = _.sortBy(
          DriverType.toOptions(nvZonalRouting.driverTypes, 'id'), 'displayName'
        );
      }

      // processing data
      nvZonalRouting.driversByZones = [];

      _.concat(zones, INVALID_ZONES).forEach((zone) => {
        nvZonalRouting.driversByZones.push(newDriversByZoneObj(zone));
      });

      nvZonalRouting.driversByZonesTable.filteredDriversByZones = filterDriversByZonesTable();

      nvZonalRouting.driversTable.all.forEach((driver) => {
        // set driver extra property
        driver.name = [driver.firstName, driver.lastName].join(' ');

        const zProps = getZonePreferenceProps(driver);
        driver.zoneId = zProps.zoneId;
        driver.zoneName = getZoneName(driver.zoneId);
        driver.min = zProps.min;
        driver.max = zProps.max;
        driver.cost = zProps.cost;
        driver.seedLatitude = zProps.seedLatitude;
        driver.seedLongitude = zProps.seedLongitude;

        const vProps = getVehicleProps(driver);
        driver.vehicleId = vProps.vehicleId;
        driver.vehicleType = vProps.vehicleType;
        driver.vehicleCapacity = vProps.vehicleCapacity;
        driver.label = getDriverLabel(driver);

        // ignore routed drivers
        if (_.find(nvZonalRouting.currentRoutes, ['driverId', driver.id]) !== undefined) {
          return;
        }

        nvZonalRouting.addDriverToDriversByZones(driver, false);
        nvZonalRouting.driversTable.available.push(driver);

        function getDriverLabel(theDriver) {
          return `${"<div class='driver-item-label'>" +
                      "<span class='zone-name nv-text-ellipsis nv-text-left'>"}${theDriver.zoneName}</span>` +
                      `<span class='driver-name nv-text-ellipsis nv-text-left'>${theDriver.name}</span>` +
                      '<span class=\'vehicle-capacity nv-text-ellipsis nv-text-center\'>' +
                        '<i class=\'material-icons\'>tab</i>' +
                        `<span>${driver.max}</span>` +
                      '</span>' +
                    '</div>';
        }

        function getZoneName(zoneId) {
          const zone = nvZonalRouting.getZoneData(zoneId);
          return zone ? zone.name : '';
        }
      });

      nvZonalRouting.driversTable.filteredDrivers = nvZonalRouting.filterDriversTable();

      nvZonalRouting.driversByZones.forEach((zone) => {
        zone.drivers = _.sortBy(zone.drivers, 'name');
      });

      function newDriversByZoneObj(zone) {
        const driversByZone = {
          id: zone.id,
          label: '',
          name: zone.name,
          count: 0,
          min: 0,
          max: 0,
          dndType: DND_DRIVERS_BY_ZONE_TYPE,
          drivers: [],
        };

        driversByZone.label = nvZonalRouting.getDriverByZoneLabel(driversByZone);

        return driversByZone;
      }

      function getZonePreferenceProps(driver) {
        const zonePreferences = _.sortBy(driver.zonePreferences, 'rank') || [];
        const selectedZonePreference = zonePreferences[0] || null;

        if (selectedZonePreference === null) {
          return {
            zoneId: -1,
            min: 0,
            max: 0,
            cost: 0,
            seedLatitude: DEFAULT_LAT,
            seedLongitude: DEFAULT_LNG,
          };
        }

        if (!_.find(zones, ['id', selectedZonePreference.zoneId])) {
          return {
            zoneId: -1,
            min: selectedZonePreference.minWaypoints,
            max: selectedZonePreference.maxWaypoints,
            cost: selectedZonePreference.cost,
            seedLatitude: selectedZonePreference.latitude,
            seedLongitude: selectedZonePreference.longitude,
          };
        }

        return {
          zoneId: selectedZonePreference.zoneId,
          min: selectedZonePreference.minWaypoints,
          max: selectedZonePreference.maxWaypoints,
          cost: selectedZonePreference.cost,
          seedLatitude: selectedZonePreference.latitude,
          seedLongitude: selectedZonePreference.longitude,
        };
      }

      function getVehicleProps(driver) {
        const vehicles = driver.vehicles || [];

        let activeVehicle = null;
        vehicles.forEach((vehicle) => {
          // use the active vehicle
          if (vehicle.active) {
            activeVehicle = vehicle;
          }
        });

        if (activeVehicle === null) {
          return {
            vehicleId: null,
            vehicleType: null,
            vehicleCapacity: 0,
          };
        }

        return {
          vehicleId: activeVehicle.id,
          vehicleType: activeVehicle.vehicleType,
          vehicleCapacity: activeVehicle.capacity,
        };
      }
    }

    // //////////////////////////////////////////////////////////////////////////////
    // Action Bar functions /////////////////////////////////////////////////////////
    // //////////////////////////////////////////////////////////////////////////////
    function editFetchTransactionsSetting() {
      nvZonalRouting.isEditFetchTransactionsSetting = true;
    }

    function isAbleToFetchTransactions() {
      const isFormValid = ctrl.zonesForm && ctrl.zonesForm.$valid;
      const isRouteDateValid = moment(nvZonalRouting.routeDate).isValid();

      if (!(isFormValid && isRouteDateValid)) {
        return false;
      }

      if (nvZonalRouting.isFetchByRouteGroup()) {
        return _.size(nvZonalRouting.routeGroupIds) > 0;
      }

      return moment(nvZonalRouting.orderCreationDate).isValid();
    }

    function fetchTransactions() {
      nvZonalRouting.isEditFetchTransactionsSetting = false;
      parentContentLoading.backdropLoading = true;

      if (DEBUG) { // debug mode
        $.getJSON('views/container/zonal-routing/json/zone-transactions.json', (zoneTransactions) => {
          $.getJSON('views/container/zonal-routing/json/current-routes.json', (currentRoutesResult) => {
            $timeout(() => {
              success([zoneTransactions, currentRoutesResult]);
            });
          });
        });
      } else {
        nvNavGuard.lock();
        read().then(success, failure);
      }

      function read() {
        const routeDateFrom = nvDateTimeUtils.displayDateTime(
          nvDateTimeUtils.toMoment(nvZonalRouting.routeDate).startOf('day'), 'UTC'
        );
        const routeDateTo = nvDateTimeUtils.displayDateTime(
          nvDateTimeUtils.toMoment(nvZonalRouting.routeDate).endOf('day'), 'UTC'
        );

        const orderCreationDate = nvDateTimeUtils.displayDateTime(
          nvDateTimeUtils.toMoment(nvZonalRouting.orderCreationDate).hour(0).minute(0).second(0), 'UTC'
        );

        const zoneTransactionsRequestPayload = {
          routeDate: routeDateFrom,
          orderCreationDate: orderCreationDate,
        };

        if (nvZonalRouting.isFetchByRouteGroup()) {
          zoneTransactionsRequestPayload.routeGroupIds = nvZonalRouting.routeGroupIds;
        } else if (_.size(nvZonalRouting.zoneIds) > 0) {
          // not fetch by route group, and select zone filter
          zoneTransactionsRequestPayload.zoneIds = nvZonalRouting.zoneIds;
        }

        return $q.all([
          Route.getZoneTransactions(zoneTransactionsRequestPayload),
          Route.read({
            from: routeDateFrom,
            to: routeDateTo,
          }),
        ]);
      }

      function success(response) {
        // store the original response data so that when user click reset button,
        // no need to call the api again
        nvZonalRouting.zoneTransactionsResult = _.cloneDeep(response[0]);
        nvZonalRouting.currentRoutesResult = _.cloneDeep(response[1]);

        // start processing
        const zoneTransactions = response[0];
        nvZonalRouting.currentRoutes = response[1];

        nvZonalRouting.zoneTransactions = zoneTransactions;

        updateZonePriorities(nvZonalRouting.zoneTransactions);
        updateZoneAssignmentData();

        parentContentLoading.backdropLoading = false;
      }

      function failure() {
        parentContentLoading.backdropLoading = false;
      }
    }

    function emptyDriversAssignment() {
      _.forEach(nvZonalRouting.zones, (zone) => {
        clearDriversByZone(zone.id);
      });
    }

    function isNormalDriver(driver) {
      return !isReserveFleetDriver(driver);
    }

    function isReserveFleetDriver(driver) {
      return Driver.isReserveFleetDriver(driver);
    }

    function resetDriversAssignment() {
      parentContentLoading.backdropLoading = true;
      nvZonalRouting.driversByZonesTable.filteredDriversByZones = [];
      nvZonalRouting.driversTable.filteredDrivers = [];

      loadZoneDrivers(true).then(() => {
        parentContentLoading.backdropLoading = false;
      }, () => {
        parentContentLoading.backdropLoading = false;
      });
    }

    function updateDriverAssignment() {
      parentContentLoading.backdropLoading = true;

      const drivers = [];
      _.forEach(nvZonalRouting.zones, (zone) => {
        _.forEach(zone.drivers || [], (driver) => {
          drivers.push({ id: driver.id, zoneId: parseInt(zone.id, 10) });
        });
      });
      DriverTemporaryZone.delete().then(() => {
        DriverTemporaryZone.update({ drivers: drivers }).then(success, failure);
      }, failure);

      function success() {
        nvToast.success(nvTranslate.instant('container.zonal-routing.temporary-zone-assignment-updated'));
        parentContentLoading.backdropLoading = false;
      }

      function failure() {
        parentContentLoading.backdropLoading = false;
      }
    }

    function showAutoAssignDriversModal($event) {
      nvDialog.showSingle($event, {
        templateUrl: 'views/container/zonal-routing/dialog/auto-assign/auto-assign.dialog.html',
        cssClass: 'zonal-routing-auto-assign',
        controller: 'ZonalRoutingAutoAssignDialogController',
        controllerAs: 'ctrl',
        locals: {},
      }).then(success);

      function success() {
        nvZonalRouting.driversTable.filteredDrivers = nvZonalRouting.filterDriversTable();
        updateZoneAssignmentData();
      }
    }

    // //////////////////////////////////////////////////////////////////////////////
    // Zone Assignment and Driver List functions ////////////////////////////////////
    // //////////////////////////////////////////////////////////////////////////////

    function routeZone(zoneId, $event) {
      const zone = nvZonalRouting.getZoneData(zoneId);

      if (!zone.isCustomZone && zone.drivers.length <= 0) {
        nvDialog.confirmDelete($event, {
          title: nvTranslate.instant('container.zonal-routing.edit-routes'),
          content: nvTranslate.instant('container.zonal-routing.edit-routes-confirmation'),
          ok: nvTranslate.instant('commons.yes'),
          cancel: nvTranslate.instant('commons.no'),
        }).then(onRouteZone);
      } else {
        onRouteZone();
      }

      function onRouteZone() {
        nvZonalRouting.waypointTransactionsToRoute = {};
        if (zoneId === ALL_ZONES_ID) {
          _.forEach(nvZonalRouting.zones, (theZone) => {
            processWaypointTransactionsToRoute(nvZonalRouting.zoneTransactions[theZone.id]);
          });
        } else if (zoneId === RESERVATION_ZONE.id) {
          const reservationZone = nvZonalRouting.getZoneData(RESERVATION_ZONE.id);
          processWaypointTransactionsToRoute(reservationZone.extraData.waypoints, true);

          reservationZone.drivers = _.cloneDeep(nvZonalRouting.driversTable.available);
        } else {
          processWaypointTransactionsToRoute(nvZonalRouting.zoneTransactions[zoneId]);
        }

        goToRoutingPage(zoneId, 1);
      }
    }

    function sendDriversAssignment(zoneId, $event, routingType) {
      const zone = nvZonalRouting.getZoneData(zoneId);

      parentContentLoading.backdropLoading = true;

      nvZonalRouting.chosenZone = zone;
      const driversData = getDriversData(zone.drivers);
      let waypointsByPriority = nvZonalRouting.zoneTransactions[zone.id];

      // accumulate all waypoints for all zones placeholder
      // (because nvZonalRouting.zoneTransactions[ALL_ZONES_ID] will be undefined)
      if (zone.id === ALL_ZONES_ID) {
        waypointsByPriority = {};
        _.forEach(nvZonalRouting.zoneTransactions, (priorityNonPriorityWaypoints) => {
          _.merge(waypointsByPriority, priorityNonPriorityWaypoints);
        });
      }

      getWaypointsDataFromTransactions(waypointsByPriority, routingType)
        .then((waypointsData) => {
          if (!waypointsData || waypointsData.length === 0) {
            parentContentLoading.backdropLoading = false;
            return;
          }

          const overallData = {
            max_iterations: 1000,
            number_of_runs: 10,
            drivers: driversData,
            waypoints: waypointsData,
          };

          const MAX_ITERATION = 10; // for status check
          let intervalPromise;
          let jobId;
          let currentIteration = 1;

          if (routingType === ROUTE_TYPE.BULKY) {
            Route.algoCreateRoutesBulky(overallData).then(success, failure);
          } else if (routingType === ROUTE_TYPE.VOLUMETRIC) {
            Route.algoCreateRoutesBulky(overallData).then(success, failure);
          } else {
            Route.algoCreateRoutes(overallData).then(success, failure);
          }

          const routeDate = nvDateTimeUtils.displayDate(nvZonalRouting.routeDate);
          nvFileUtils.downloadJson(overallData, `JSONOUTPUT-${zone.id}-${routeDate}`);

          function success(response) {
            jobId = response.job_id || '';
            setStatusCheckIntervalPromise();
          }

          function failure() {
            parentContentLoading.backdropLoading = false;
          }

          function setStatusCheckIntervalPromise() {
            intervalPromise = $interval(createRoutesStatusCheck, 2000);
          }

          function createRoutesStatusCheck() {
            Route.algoGetSolutions(jobId).then(
              createRoutesStatusCheckSuccess, createRoutesStatusCheckFailure
            );

            function createRoutesStatusCheckSuccess(response) {
              const status = (response && response.status) || '';
              currentIteration += 1;

              if (status === 'DONE') {
                nvZonalRouting.algoRouteAssignments = (
                    response && response.job_solution && response.job_solution.route_assignments
                  ) || [];
                processWaypointTransactionsToRoute(waypointsByPriority);

                $interval.cancel(intervalPromise);
                parentContentLoading.backdropLoading = false;

                goToRoutingPage(zoneId, 0);
              } else if (status === 'ERROR') {
                nvToast.error(response && response.job_solution);

                $interval.cancel(intervalPromise);
                parentContentLoading.backdropLoading = false;
              } else if (currentIteration > MAX_ITERATION) {
                let title = 'container.zonal-routing.assign-rbe-and-algo';
                if (routingType === ROUTE_TYPE.BULKY) {
                  title = 'container.zonal-routing.assign-bulky-rbe-and-algo';
                }

                $interval.cancel(intervalPromise);
                nvDialog.confirmDelete($event, {
                  title: nvTranslate.instant(title),
                  content: nvTranslate.instant('container.zonal-routing.assign-rbe-waiting-text'),
                  ok: nvTranslate.instant('commons.yes'),
                  cancel: nvTranslate.instant('commons.no'),
                }).then(onContinue, onStop);
              }
            }

            function createRoutesStatusCheckFailure() {
              $interval.cancel(intervalPromise);
              parentContentLoading.backdropLoading = false;
            }

            function onContinue() {
              setStatusCheckIntervalPromise();
              currentIteration = 1;
            }

            function onStop() {
              parentContentLoading.backdropLoading = false;
            }
          }
        });
    }

    function mergeTransactions(zoneId) {
      const transactionIds = getMergeTransactionIds(zoneId);

      if (transactionIds) {
        parentContentLoading.backdropLoading = true;

        Transaction.merge(transactionIds).then(() => {
          parentContentLoading.backdropLoading = false;

          nvToast.success(nvTranslate.instant('container.zonal-routing.merge-waypoints-success'));
        });
      }
    }

    function unmergeTransactions(zoneId) {
      const transactionIds = getMergeTransactionIds(zoneId);

      if (transactionIds) {
        parentContentLoading.backdropLoading = true;

        Transaction.unmerge({
          transactionIds: transactionIds,
        }).then(success, failure);
      }

      function success(response) {
        parentContentLoading.backdropLoading = false;

        nvToast.success(nvTranslate.instant('container.zonal-routing.unmerge-waypoints-success'));
        nvFileUtils.downloadJson(response, 'transaction-ids');
      }

      function failure(response) {
        parentContentLoading.backdropLoading = false;

        nvFileUtils.downloadJson(response.message, 'failed-order-ids');
      }
    }

    function setBuff(zone, $event) {
      nvDialog.showSingle($event, {
        templateUrl: 'views/container/zonal-routing/dialog/set-parcels-aside/set-parcels-aside.dialog.html',
        cssClass: 'zonal-routing-set-parcels-aside',
        controller: 'ZonalRoutingSetParcelsAsideDialogController',
        controllerAs: 'ctrl',
        locals: {
          selectedZone: zone,
        },
      }).then(onSetBuff);

      function onSetBuff() {
        updateZoneParcelCount(zone, {
          priority: zone.priority,
          nonPriority: zone.nonPriority,
        });
      }
    }

    function clearDriversByZone(zoneId) {
      const zone = nvZonalRouting.getZoneData(zoneId);

      const driversToClear = _.cloneDeep(zone.drivers);

      // handle drivers in nvZonalRouting.zones
      zone.drivers = [];

      // handle nvZonalRouting.driversByZones
      _.forEach(driversToClear, (driver) => {
        nvZonalRouting.addDriverToDriversByZones(driver);
      });

      updateZoneAssignmentData();
      nvZonalRouting.driversTable.filteredDrivers = nvZonalRouting.filterDriversTable();
    }

    function refreshDriverAssignmentOnDrop(response) {
      const items = _.castArray(response.item);

      // process
      switch (response.target.id) {
        case DND_DRIVER_FROM_DRIVER_LIST_TYPE:
          // if drag to Unassigned Drivers -> Drivers table
          _.forEach(items, (driver) => {
            nvZonalRouting.addDriverToDriversByZones(driver);
          });

          break;
        case DND_DRIVERS_BY_ZONE_TYPE:
          // if drag to Unassigned Drivers -> Quickdraws (All drivers of listed type) table
          _.forEach(items, (driver) => {
            nvZonalRouting.addDriverToDriversByZones(driver);
          });

          break;
        default:
          // if drag to Zones assignment table
          if (items[0].dndType === DND_DRIVER_FROM_DRIVER_LIST_TYPE) {
            // if drag from Unassigned Drivers -> Drivers table
            // grab without searchTextFiltering because sometimes user may filter by text
            const selectedDrivers = _.filter(nvZonalRouting.filterDriversTable(false), driver =>
              driver.dndSelected === true
            );

            _.forEach(selectedDrivers, (driver) => {
              nvZonalRouting.addDriverToZone(driver.id, +response.target.id);
            });
          } else if (items[0].dndType === DND_DRIVERS_BY_ZONE_TYPE) {
            // if drag from Unassigned Drivers -> Quickdraws (All drivers of listed type) table
            const zoneRow = items[0];
            const zone = nvZonalRouting.getZoneData(+response.target.id);

            // handle drivers in nvZonalRouting.zones
            _.forEach(zoneRow.drivers, (driver) => {
              driver.dndType = DND_DRIVER_FROM_ZONE_TYPE; // refresh dndType
            });
            zone.drivers = zone.drivers.concat(zoneRow.drivers);

            // handle nvZonalRouting.driversByZones
            const driversByZone = nvZonalRouting.getDriversByZone(zoneRow.id);
            driversByZone.drivers = [];
            driversByZone.count = 0;
            driversByZone.min = 0;
            driversByZone.max = 0;
            driversByZone.label = nvZonalRouting.getDriverByZoneLabel(driversByZone);
          } else if (items[0].dndType === DND_DRIVER_FROM_ZONE_TYPE) {
            // if drag from Zones assignment table (one zone to another zone)
            const zone = nvZonalRouting.getZoneData(+response.target.id);
            _.forEach(items, (driver) => {
              driver.dndSelected = false;
              zone.drivers.push(driver);
            });
          }
      }

      nvZonalRouting.driversTable.filteredDrivers = nvZonalRouting.filterDriversTable();
      updateZoneAssignmentData();
    }

    function driverPreferredZonesSelectionChanged() {
      $timeout(() => {
        nvZonalRouting.driversByZonesTable.filteredDriversByZones = filterDriversByZonesTable();
        nvZonalRouting.driversTable.filteredDrivers = nvZonalRouting.filterDriversTable();
      });
    }

    function hubsSelectionChanged() {
      $timeout(() => {
        // filtering happen in:
        // - nvZonalRouting.addDriverToDriversByZones function
        processDriverListTableDatas();
      });
    }

    function driverTypesSelectionChanged() {
      $timeout(() => {
        // filtering happen in:
        // - nvZonalRouting.addDriverToDriversByZones function
        processDriverListTableDatas();
      });
    }

    function showRFDriversCheckboxChanged() {
      $timeout(() => {
        // filtering happen in:
        // - nvZonalRouting.addDriverToDriversByZones function
        processDriverListTableDatas();
      });
    }

    function filterDriversByZonesTable() {
      let driversByZones = [];
      _.forEach(nvZonalRouting.driverPreferredZonesSelection.value, (zoneId) => {
        driversByZones = _.concat(driversByZones, nvZonalRouting.getDriversByZone(zoneId));
      });

      if (_.size(nvZonalRouting.driverPreferredZonesSelection.value) > 0) {
        return sortDriversByZones(driversByZones);
      }

      return sortDriversByZones(nvZonalRouting.driversByZones);

      function sortDriversByZones(theDriversByZones) {
        // sort INVALID_ZONES to last
        const sortedDriversByZones = _.pullAllBy(
          _.sortBy(theDriversByZones, 'name'), INVALID_ZONES, 'id'
        );

        let removedZones = [];
        _.forEach(INVALID_ZONES, (zone) => {
          if (nvZonalRouting.driverPreferredZonesSelection.value.indexOf(zone.id) >= 0) {
            removedZones.push(nvZonalRouting.getDriversByZone(zone.id));
          }
        });
        removedZones = _.sortBy(removedZones, 'name');

        return sortedDriversByZones.concat(removedZones);
      }
    }

    function resizeWithOffset() {
      return {
        'max-height': `${nvZonalRouting.contentHeight}px`,
      };
    }


    // //////////////////////////////////////////////////////////////////////////////
    // Helper Functions /////////////////////////////////////////////////////////////
    // //////////////////////////////////////////////////////////////////////////////
    function updateZoneAssignmentData() {
      _.forEach(nvZonalRouting.zones, (zone) => {
        zone.min = 0;
        zone.max = 0;

        _.forEach(zone.drivers, (driver) => {
          zone.min += (driver.min == null ? 0 : parseInt(driver.min, 10));
          zone.max += (driver.max == null ? 0 : parseInt(driver.max, 10));
        });

        updateZoneParcelCount(zone, zone.priority, zone.nonPriority);
      });

      nvZonalRouting.zonesTableParams.setData(nvZonalRouting.zones);
    }

    function updateZonePriorities(zoneTransactions) {
      let totalPriority = 0;
      let totalNonPriority = 0;
      const reservationZone = nvZonalRouting.getZoneData(RESERVATION_ZONE.id);

      // reset before doing calculation
      _.forEach(nvZonalRouting.zones, (zone) => {
        zone.priority = 0;
        zone.nonPriority = 0;
      });

      // start calculate
      _.forEach(zoneTransactions, (zoneTransaction, zoneId) => {
        if (zoneTransactions.hasOwnProperty(zoneId)) {
          calculateCustomZonesCount(zoneTransaction);

          if (zoneId !== ALL_ZONES_ID) {
            calculateNonDPPickUp(zoneTransactions, zoneId);
          }
        }
      });

      const allZone = nvZonalRouting.getZoneData(ALL_ZONES_ID);
      updateZoneParcelCount(allZone, {
        priority: totalPriority,
        nonPriority: totalNonPriority,
      });

      function calculateCustomZonesCount(theZoneTransaction) {
        // currently only have one custom zone to process (reservationZone)
        _.forEach(theZoneTransaction, (waypointTransactions) => {
          _.forEach(waypointTransactions, (waypoint) => {
            if (Waypoint.isReservation(waypoint)) {
              reservationZone.extraData.waypoints.push(waypoint);
              reservationZone.extraData.count.reservations += 1;
            } else if (Waypoint.isTransaction(waypoint)) {
              if (!waypoint.transactions) {
                return;
              }

              _.forEach(waypoint.transactions, (transaction) => {
                if (!transaction.order) {
                  return;
                }

                if (transaction.order.parcelSize === 'EXTRALARGE') {
                  reservationZone.extraData.waypoints.push(waypoint);
                  reservationZone.extraData.count.extraLarge += 1;
                } else if (transaction.order.bulkMoveQty > 0) {
                  reservationZone.extraData.waypoints.push(waypoint);
                  reservationZone.extraData.count.bulkMove += 1;
                } else if (Order.isTypeEnum(transaction.order, Order.TYPE.RETURN) &&
                  Transaction.isDelivery(transaction)) {
                  reservationZone.extraData.waypoints.push(waypoint);
                  reservationZone.extraData.count.returns += 1;
                } else if (Order.isTypeEnum(transaction.order, Order.TYPE.NORMAL) &&
                  Transaction.isDelivery(transaction) && transaction.order.rts) {
                  reservationZone.extraData.waypoints.push(waypoint);
                  reservationZone.extraData.count.rts += 1;
                }
              });
            }
          });
        });

        updateZoneParcelCount(reservationZone, {
          extraData: {
            count: reservationZone.extraData.count,
          },
        });
      }

      function calculateNonDPPickUp(transactions, id) {
        let priorityCount = 0;
        let nonPriorityCount = 0;

        if (transactions[id][1] == null) {
          priorityCount = 0;
        } else {
          const priorityWaypoints = zoneTransactions[id][1];
          _.forEach(priorityWaypoints, (wp) => {
            if (Waypoint.isTransaction(wp)) {
              const txns = wp.transactions;
              if (!nvZonalRouting.isDPPickup(txns)) {
                priorityCount += 1;
              }
            } else {
              // it's not transaction waypoint
              priorityCount += 1;
            }
          });
        }

        if (transactions[id][0] == null) {
          nonPriorityCount = 0;
        } else {
          const nonPriorityWaypoints = zoneTransactions[id][0];
          _.forEach(nonPriorityWaypoints, (wp) => {
            if (Waypoint.isTransaction(wp)) {
              const txns = wp.transactions;
              if (!nvZonalRouting.isDPPickup(txns)) {
                nonPriorityCount += 1;
              }
            } else {
              nonPriorityCount += 1;
            }
          });
        }

        updateZoneParcelCount(nvZonalRouting.getZoneData(id), {
          priority: priorityCount,
          nonPriority: nonPriorityCount,
        });

        totalPriority += priorityCount;
        totalNonPriority += nonPriorityCount;
      }
    }

    function getZoneCondition(zone) {
      if (zone.isCustomZone) {
        return ZONE_CONDITIONS.NA;
      } else if (zone.max >= zone.parcelsToDo) {
        return ZONE_CONDITIONS.SUFFICIENT;
      } else if (zone.max >= zone.priority) {
        return ZONE_CONDITIONS.PRIORITY_SUFFICIENT;
      } else if (zone.max < zone.parcelsToDo) {
        return ZONE_CONDITIONS.INSUFFICIENT;
      }

      return ZONE_CONDITIONS.UNKNOWN;
    }

    function updateZoneParcelCount(zone, count) {
      _.forEach(count, (value, key) => {
        if (_.isObject(value)) {
          updateCustomZoneParcelCount(zone, value);
          return;
        }

        zone[key] = value;
      });

      if (!zone.isCustomZone) {
        zone.totalParcels = zone.priority + zone.nonPriority;
      } else {
        zone.totalParcels = 0;
        _.forEach(zone.extraData.count, (value) => {
          zone.totalParcels += value;
        });
      }

      zone.parcelsToDo = zone.totalParcels - zone.buffer;

      function updateCustomZoneParcelCount(customZone, extraData) {
        _.forEach(extraData.count, (value, key) => {
          customZone.extraData.count[key] = value;
        });
      }
    }

    function moveTempZoneDriversToZones() {
      (nvZonalRouting.driversWithTemporaryZones || []).forEach((row) => {
        nvZonalRouting.addDriverToZone(row.id, row.zoneId);
      });

      nvZonalRouting.driversTable.filteredDrivers = nvZonalRouting.filterDriversTable();
      updateZoneAssignmentData();
    }

    function processWaypointTransactionsToRoute(
      priorityWaypointTransactions, isCustomZone = false
    ) {
      if (isCustomZone) {
        processingWaypointTransactionsToRoute(priorityWaypointTransactions);
      } else {
        _.forEach(priorityWaypointTransactions, (waypointTransactions) => {
          processingWaypointTransactionsToRoute(waypointTransactions);
        });
      }

      function processingWaypointTransactionsToRoute(theWaypointTransactions) {
        _.forEach(theWaypointTransactions, (waypoint, waypointId) => {
          const transactions = [];
          if (Waypoint.isTransaction(waypoint)) {
            if (!isCustomZone && nvZonalRouting.isDPPickup(waypoint.transactions)) {
              // just hide all PICKUP txn that is tagged to dpId
              return;
            }

            _.forEach(waypoint.transactions, (transaction) => {
              transactions.push(transaction);
            });
          } else {
            transactions.push(waypoint.reservation);
          }

          // Initalize structure
          if (nvZonalRouting.waypointTransactionsToRoute[waypointId] === undefined) {
            nvZonalRouting.waypointTransactionsToRoute[waypointId] = [];
          }

          _.forEach(transactions, (transaction) => {
            transaction.waypoint = waypoint;
            nvZonalRouting.waypointTransactionsToRoute[waypointId].push(transaction);
          });
        });
      }
    }

    function getWaypointsDataFromTransactions(waypointsByPriority, routingType = ROUTE_TYPE.BULKY) {
      let waypoints = [];
      let priorities = [];

      _.forEach(waypointsByPriority, (wpObject, key) => {
        const isPriority = _.toInteger(key) === 1; // key can be 0 (non-priority) or 1 (priority)

        const wps = Object
          .keys(wpObject)
          .map(wpId => wpObject[wpId]);

        const prs = Array
          .apply(null, Array(wps.length))
          .map(Boolean.prototype.valueOf, isPriority);

        waypoints = waypoints.concat(wps);
        priorities = priorities.concat(prs);
      });

      return DriverTypeEligible.getBulkIdsRemote(waypoints, priorities).then((driverTypeIds) => {
        if (!waypoints || !priorities || !driverTypeIds) { return []; }
        if (waypoints.length !== priorities.length ||
          waypoints.length !== driverTypeIds.length) {
          return [];
        }

        return waypoints.map((waypoint, i) => {
          let waypointCod = 0;
          let jobPriorityNumeric = 0;
          let itemSize = 0;
          (waypoint.transactions || []).forEach((transaction) => {
            const goodsAmount = transaction.order &&
                                transaction.order.cod &&
                                transaction.order.cod.goodsAmount;
            const dimensions = _.get(transaction, 'order.dimensions');

            if (goodsAmount) {
              waypointCod += goodsAmount;
            }

            if (routingType === ROUTE_TYPE.VOLUMETRIC && dimensions) {
              const volume = dimensions.width * dimensions.height * dimensions.length;
              if (volume) {
                itemSize += volume;
              }
            } else if (routingType === ROUTE_TYPE.BULKY || routingType === ROUTE_TYPE.NORMAL) {
              const parcelSizeEnum = transaction.order && transaction.order.parcelSize;
              const parcelSize = Order.getParcelSizeByEnum(parcelSizeEnum);
              if (parcelSize) {
                itemSize += parcelSize.itemSize;
              }
            }

            // check for non-transit transaction
            // and if it is higher than the current priority level
            if (!transaction.transit && transaction.priorityLevel > jobPriorityNumeric) {
              jobPriorityNumeric = transaction.priorityLevel;
            }
          });

          return {
            id: parseInt(waypoint.id, 10),
            postcode: waypoint.postcode,
            job_priority: priorities[i],
            job_priority_numeric: jobPriorityNumeric,
            latitude: waypoint.latitude,
            longitude: waypoint.longitude,
            waypoint_cod: waypointCod,
            eligible_driver_type_id_list: driverTypeIds[i],
            item_size: itemSize,
          };
        });
      });
    }

    function getDriversData(drivers) {
      return (drivers || []).map(driver => ({
        id: driver.id,
        min: driver.min,
        max: driver.max,
        cod_limit: driver.codLimit,
        seed_latitude: driver.seedLatitude,
        seed_longitude: driver.seedLongitude,
        driver_type_id: driver.driverTypeId,
        vehicle_capacity: driver.vehicleCapacity,
      }));
    }

    function getMergeTransactionIds(zoneId) {
      let transactionIdsMap = {};

      if (zoneId !== ALL_ZONES_ID) {
        // get the transaction ids for a specific zone
        const priorities = nvZonalRouting.zoneTransactions[zoneId];
        if (!priorities) { return []; }
        transactionIdsMap = mergeTransactionIdsMap(transactionIdsMap, priorities);
      } else {
        // get the transaction ids for all zones
        _.forEach(nvZonalRouting.zoneTransactions, (priorities) => {
          transactionIdsMap = mergeTransactionIdsMap(transactionIdsMap, priorities);
        });
      }

      const transactionIds = [];
      _.forEach(transactionIdsMap, (txn, id) => {
        transactionIds.push(+id);
      });

      return transactionIds;

      function mergeTransactionIdsMap(map, priorities) {
        _.forEach(priorities, (waypoints) => {
          _.forEach(waypoints, (waypoint) => {
            if (waypoint.transactions && waypoint.transactions.length > 0) {
              _.forEach(waypoint.transactions, (transaction) => {
                if (transaction.id && transaction.distributionPointId === null) {
                  map[transaction.id] = true;
                }
              });
            }
          });
        });
        return map;
      }
    }

    function goToRoutingPage(zoneId, toCluster = 0) {
      nvZonalRouting.chosenZone = nvZonalRouting.getZoneData(zoneId);

      nvNavGuard.unlock();
      $state.go('container.zonal-routing.routing', { toCluster: toCluster });
    }
  }
}());
