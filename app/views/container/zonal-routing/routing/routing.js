(function controller() {
  angular
    .module('nvOperatorApp.controllers')
    .controller('ZonalRoutingRoutingController', ZonalRoutingRoutingController);

  ZonalRoutingRoutingController.$inject = [
    'nvZonalRouting', 'appSidenav', '$scope', 'nvNavGuard', '$timeout',
    'Driver', 'nvMaps', 'nvDialog', 'nvDateTimeUtils', 'nvToast',
    'nvTranslate', 'Route', 'Hub', 'Waypoint', '$stateParams',
    '$state', '$compile', 'nvPanel', 'nvCurrency', 'hotkeys',
    'nvAddress', 'Order', '$window', 'nvModelUtils', 'nvAutocomplete.Data',
    '$mdMedia', '$rootScope',
  ];

  function ZonalRoutingRoutingController(
    nvZonalRouting, appSidenav, $scope, nvNavGuard, $timeout,
    Driver, nvMaps, nvDialog, nvDateTimeUtils, nvToast,
    nvTranslate, Route, Hub, Waypoint, $stateParams,
    $state, $compile, nvPanel, nvCurrency, hotkeys,
    nvAddress, Order, $window, nvModelUtils, nvAutocompleteData,
    $mdMedia, $rootScope
  ) {
    // variables
    const DEBUG = nvZonalRouting.isDebug();
    const POLYGON_TOOLS_ACTION = {
      ADD: {
        icon: 'add',
        currentTypeSelection: nvZonalRouting.POLYGON_TOOLS_ADD_ACTION.UNROUTED,
      },
      REMOVE: {
        icon: 'remove',
        currentTypeSelection: nvZonalRouting.POLYGON_TOOLS_REMOVE_ACTION.ROUTED,
      },
    };

    const toCluster = !!$stateParams.toCluster;
    let drawControl = null;
    let featureGroup = null;
    let orphanPolygonLayers = [];
    let animationTimer = null;
    let moveendTimer = null;
    let loadingMarkers = false;
    let isMoveendTimerTriggerring = false;
    const driverSelectAutocomplete = nvAutocompleteData.getByHandle('zonal-routing-select-driver');
    const addDriverAutoComplete = nvAutocompleteData.getByHandle('routing-add-driver');

    const parentContentLoading = $scope.contentLoading;
    const hotkeyEvents = {
      isWaypointDetailsTableLocked: false,
      hoveredMarker: null,
    };

    // variables (that also use in html)
    const ctrl = this;

    ctrl.Order = Order;

    ctrl.hubsSelectionOptions = null;
    ctrl.addDriver = {
      searchText: '',
      driver: null,
      selectionOptions: [],
      onChange: addDriverOnChange,
    };
    ctrl.selectedFilters = [];
    ctrl.currentMapViewOption = nvZonalRouting.MAP_VIEW_OPTIONS.ALL;
    ctrl.visibleMarkersCount = 0;

    ctrl.POLYGON_TOOLS_APPLY_ACTION = nvZonalRouting.POLYGON_TOOLS_APPLY_ACTION;
    ctrl.POLYGON_TOOLS_ACTION = POLYGON_TOOLS_ACTION;
    ctrl.currentApplyActionSelection = ctrl.POLYGON_TOOLS_APPLY_ACTION.ACTIVE_ROUTE;
    ctrl.polygonToolsPerformActionResult = {
      isSuccess: false,
      action: null, // action icon string
      processed: {
        priority: 0,
        nonPriority: 0,
      },
      hasWPProcessed: polygonToolsHasWPProcessed,
      getSuccessText: polygonToolsGetSuccessText,
    };

    ctrl.map = null;
    ctrl.waypointDetailsTableParams = {
      isTransactionWp: true,
      list: [],
      waypointId: null,
      sizeCounter: null,
      address: null,
    };
    ctrl.hotkeyEvents = hotkeyEvents;

    ctrl.activeRoute = null;
    ctrl.activeRouteId = 0;
    ctrl.activeRoutePolygonData = null;
    ctrl.activeRouteSelectWaypointMode = {
      isActive: false,
      ableToAddAnyWp: nvModelUtils.fromBoolToYesNoBitString(true),
    };
    ctrl.expandRouteId = 0;
    ctrl.addRelatedSuccess = false;

    ctrl.routeStyling = {
      backgroundColor: routeBackgroundColorStyling,
    };
    ctrl.unroutedNonPriorityWPsCount = 0;
    ctrl.unroutedPriorityWPsCount = 0;
    ctrl.selectedPolygon = {
      onHover: false,
      count: {
        routedPriority: 0,
        routedNonPriority: 0,
        unroutedPriority: 0,
        unroutedNonPriority: 0,
      },
      getUnroutedText: getSelectedPolygonUnroutedText,
      getRoutedText: getSelectedPolygonRoutedText,
    };
    ctrl.isDriversTabSelected = false;

    // functions
    // Top panel
    ctrl.windowSizeChanged = $scope.windowSizeChanged;
    ctrl.discardChanges = discardChanges;
    ctrl.submitRoutes = submitRoutes;
    ctrl.getAssignedDriversCount = getAssignedDriversCount;

    // Map panel / Bottom panel
    ctrl.toggleIsWaypointDetailsTableLocked = toggleIsWaypointDetailsTableLocked;
    ctrl.navigateToEditOrderPage = navigateToEditOrderPage;
    ctrl.getParcelSizeShortName = getParcelSizeShortName;

    // Right Panel
    ctrl.openMapViewOptionsMenu = openMapViewOptionsMenu;
    ctrl.showSelectFiltersDialog = showSelectFiltersDialog;
    ctrl.isFilterModeShowWps = isFilterModeShowWps;
    ctrl.createNewRoute = createNewRoute;
    ctrl.openAllRoutesMenu = openAllRoutesMenu;
    ctrl.openRouteMenu = openRouteMenu;
    ctrl.setActiveRoute = setActiveRoute;
    ctrl.unsetActiveRoute = unsetActiveRoute;
    ctrl.toggleActiveRouteSelectWaypointMode = toggleActiveRouteSelectWaypointMode;
    ctrl.toggleExpandRoute = toggleExpandRoute;
    ctrl.routeTableResizeWithOffset = routeTableResizeWithOffset;
    ctrl.driverTableResizeWithOffset = driverTableResizeWithOffset;
    ctrl.hasPolygon = hasPolygon;
    ctrl.isAbleToPerformPolygonAction = isAbleToPerformPolygonAction;
    ctrl.openPolygonToolsApplyActionMenu = openPolygonToolsApplyActionMenu;
    ctrl.openPolygonToolsActionMenu = openPolygonToolsActionMenu;
    ctrl.polygonToolsPerformAction = polygonToolsPerformAction;
    ctrl.deleteAllPolygons = deleteAllPolygons;
    ctrl.addRelated = addRelated;
    ctrl.getCurrencyWithPrice = getCurrencyWithPrice;
    ctrl.showRouteWaypointsDialog = showRouteWaypointsDialog;
    ctrl.driversTabSelected = driversTabSelected;
    ctrl.suggestDrivers = suggestDrivers;
    ctrl.removeChosenZoneDriver = removeChosenZoneDriver;
    ctrl.getAssignedHubNameByDriver = getAssignedHubNameByDriver;
    ctrl.getAssignedRouteNameByDriver = getAssignedRouteNameByDriver;

    // start
    initialize();
    $scope.$on('$destroy', () => {
      appSidenav.setConfigLockedOpen(true);
    });

    // functions details
    function initialize() {
      if (!DEBUG) {
        nvNavGuard.lock();
      }

      // validation
      if (_.size(nvZonalRouting.chosenZone) <= 0) {
        goToAssignmentPage();
        return;
      }

      // prepare
      hotkeys.add({
        combo: 'l',
        description: 'To lock/unlock waypoint-details-table',
        callback: toggleIsWaypointDetailsTableLocked,
      });

      ctrl.hubsSelectionOptions = Hub.toOptions(nvZonalRouting.hubs);
      refreshDriverSelectionOptions();
      refreshAddDriverSelectionOptions();

      if (!$mdMedia('gt-lg')) {
        // only hide sidebar if screen size < 1920
        appSidenav.setConfigLockedOpen(false);
      }
      $timeout(() => {
        plot();
        ctrl.windowSizeChanged();
      });
    }

    function discardChanges($event) {
      nvDialog.confirmDelete($event, {
        title: nvTranslate.instant('commons.discard-changes'),
        content: nvTranslate.instant('container.zonal-routing.discard-changes-description'),
        ok: nvTranslate.instant('commons.leave'),
      }).then(onConfirmLeaving);

      function onConfirmLeaving() {
        goToAssignmentPage();
      }
    }

    function goToAssignmentPage() {
      nvZonalRouting.discardRoutingPageChanges();
      nvNavGuard.unlock();
      $state.go('container.zonal-routing.assignment');
    }

    function openMapViewOptionsMenu($event) {
      nvPanel.show($event, {
        controller: 'ZonalRoutingMapViewOptionsMenuController',
        templateUrl: 'views/container/zonal-routing/panel/map-view-options/map-view-options.panel.html',
        panelClass: 'map-view-options-menu',
        darkTheme: true,
        xPosition: 'ALIGN_END',
        locals: {
          onClose: onClose,
        },
      });

      function onClose(response) {
        ctrl.currentMapViewOption = response.action;

        processMapViewOption();
        generateVisibleMarkersCount();
      }
    }

    function showSelectFiltersDialog($event) {
      nvDialog.showSingle($event, {
        templateUrl: 'views/container/zonal-routing/dialog/select-filters/select-filters.dialog.html',
        cssClass: 'zonal-routing-select-filters',
        controller: 'ZonalRoutingFilterDialogController',
        controllerAs: 'ctrl',
        locals: {
          mapData: ctrl.map,
        },
      }).then(onSave);

      function onSave(response) {
        ctrl.selectedFilters = response;

        processMapViewOption();
        generateVisibleMarkersCount();
      }
    }

    function isFilterModeShowWps() {
      return nvModelUtils.fromYesNoBitStringToBool(
        nvZonalRouting.routingFilters.filterMode
      );
    }

    function processMapViewOption() {
      _.forEach(ctrl.map.allWaypointMarkers, (marker) => {
        marker.options.extraInfo.isHideByMapViewOption = false;

        switch (ctrl.currentMapViewOption) {
          case nvZonalRouting.MAP_VIEW_OPTIONS.ALL:
            // show routed & unrouted markers
            if (!marker.options.extraInfo.isHideByMapViewSettings) {
              nvZonalRouting.toggleVisibilityMarker(marker, true);
            }
            return;
          case nvZonalRouting.MAP_VIEW_OPTIONS.ROUTED:
            // show routed markers and hide unrouted markers
            if (!marker.options.extraInfo.isHideByMapViewSettings) {
              nvZonalRouting.toggleVisibilityMarker(marker, !!marker.options.routeId);
              marker.options.extraInfo.isHideByMapViewOption = !marker.options.routeId;
            }
            return;
          case nvZonalRouting.MAP_VIEW_OPTIONS.UNROUTED:
            // hide routed markers and show unrouted markers
            if (!marker.options.extraInfo.isHideByMapViewSettings) {
              nvZonalRouting.toggleVisibilityMarker(marker, !marker.options.routeId);
              marker.options.extraInfo.isHideByMapViewOption = !!marker.options.routeId;
            }
            return;
          default:
            return;
        }
      });
    }

    function generateVisibleMarkersCount() {
      const visibleMarkers = _.filter(ctrl.map.allWaypointMarkers, marker =>
        isVisible(marker)
      );

      ctrl.visibleMarkersCount = _.size(visibleMarkers);
    }

    function deletePolygonsByRoute(route) {
      _.forEach(route.nvPolygons, (polygon) => {
        featureGroup.removeLayer(polygon);
      });

      route.nvPolygons = [];
    }

    function resequenceMarkerByRoute(route) {
      route.seqNo = 1;

      _.forEach(route.addedMarkers, (marker) => {
        if (!(hotkeyEvents.isWaypointDetailsTableLocked &&
          marker === hotkeyEvents.hoveredMarker)) {
          nvZonalRouting.setRoutedWaypointMarkerIcon(marker, route);
        }

        route.seqNo += 1;
      });
    }

    function plot() {
      ctrl.map = nvMaps.initialize('routing-map', {
        controlPosition: nvZonalRouting.MAP_CONTROL_POSITION,
      });

      featureGroup = L.featureGroup().addTo(ctrl.map);
      drawControl = new L.Control.Draw({
        edit: {
          featureGroup: featureGroup,
        },
        draw: {
          polygon: {
            shapeOptions: {
              color: '#333333',
              fillColor: '#333333',
              fillOpacity: 0.5,
              opacity: 1,
            },
          },
          polyline: false,
          rectangle: false,
          circle: false,
          marker: false,
        },
        position: nvZonalRouting.MAP_CONTROL_POSITION,
      });
      ctrl.map.addControl(drawControl);

      ctrl.map.on('draw:created', (e) => {
        createPolygon(e);
      });

      ctrl.map.on('draw:deleted', (e) => {
        const deletedIds = [];
        e.layers.eachLayer((layer) => {
          deletedIds.push(layer._leaflet_id);
        });

        _.forEach(nvZonalRouting.assignedRoutes, (assignedRoute) => {
          assignedRoute.nvPolygons = _.filter(assignedRoute.nvPolygons, nvPolygon =>
            _.indexOf(deletedIds, nvPolygon._leaflet_id) <= -1
          );
        });
      });

      ctrl.map.on('move', () => {
        loadingMarkers = true;

        if (isMoveendTimerTriggerring === true) {
          return;
        }

        $timeout(() => {
          triggerMoveOrMoveEndAction();
        });
      });

      ctrl.map.on('moveend', () => {
        loadingMarkers = true;

        if (isMoveendTimerTriggerring === true) {
          return;
        }

        $timeout(() => {
          triggerMoveOrMoveEndAction();
        });
      });

      nvZonalRouting.addClusterGroupControl(ctrl.map, $scope, $compile);

      if (toCluster) {
        nvZonalRouting.updateClustering(ctrl.map, 0);
      }

      parentContentLoading.backdropLoading = true;
      $timeout(() => {
        plotting();
      }, 500);

      function plotting() {
        ctrl.map.allWaypointMarkers = [];
        ctrl.map.allWaypointMarkersMap = {};
        ctrl.map.priorityWaypointMarkers = [];
        ctrl.map.nonPriorityWaypointMarkers = [];

        _.forEach(nvZonalRouting.waypointTransactionsToRoute, (transactions) => {
          const waypoint = transactions[0].waypoint;

          const marker = nvZonalRouting.addWaypointMarker(
            ctrl.map, waypoint, transactions, toCluster
          );
          addMouseListener(marker);

          ctrl.map.allWaypointMarkers.push(marker);
          ctrl.map.allWaypointMarkersMap[waypoint.id] = marker;

          if (marker.options.extraInfo.isPriority) {
            ctrl.map.priorityWaypointMarkers.push(marker);
          } else {
            ctrl.map.nonPriorityWaypointMarkers.push(marker);
          }
        });

        const oms = nvZonalRouting.spiderfy(ctrl.map);

        _.forEach(ctrl.map.allWaypointMarkers, (marker) => {
          oms.addMarker(marker);
        });

        oms.addListener('unspiderfy', unspiderfy);

        visualizeAlgoRoutes();
        generateVisibleMarkersCount();
        setUnroutedNonPriorityWPsCount();
        setUnroutedPriorityWPsCount();
        parentContentLoading.backdropLoading = false;

        function unspiderfy() {
          _.forEach(ctrl.map.allWaypointMarkers, (marker) => {
            if (isVisibleAndNotRouted(marker)) {
              nvZonalRouting.setWaypointMarkerIcon(marker);
            }
          });
        }
      }

      function triggerMoveOrMoveEndAction() {
        // moveend will trigger per action,
        // so have to cancel previous call.
        // if no action in 500ms,
        // then will treat this as valid action and proceed to process data
        $timeout.cancel(moveendTimer);

        moveendTimer = $timeout(() => {
          isMoveendTimerTriggerring = true;
          onTriggerMoveOrMoveEndAction();

          if (!hotkeyEvents.isWaypointDetailsTableLocked) {
            resetWaypointDetailsTableParams();
            refresh();
          }
        }, 300);
      }

      function onTriggerMoveOrMoveEndAction() {
        const bounds = ctrl.map.getBounds().pad(getPadRadio());

        _.forEach(ctrl.map.allWaypointMarkers, (marker) => {
          // hide when marker is outside the bound, to solve performance issue
          if (!marker.options.extraInfo.isHideByMapViewSettings &&
            !marker.options.extraInfo.isHideByMapViewOption) {
            // only process those that still visible in map
            if (!marker.options.extraInfo.isHideWhenOutsideBound &&
              !bounds.contains(marker.getLatLng())) {
              // if marker is outside the bound, and in visible status, proceed to hide it
              nvZonalRouting.toggleVisibilityMarker(marker, false);
              marker.options.extraInfo.isHideWhenOutsideBound = true;
            } else if (marker.options.extraInfo.isHideWhenOutsideBound &&
              bounds.contains(marker.getLatLng())) {
              // if marker is inside the bound, and in hide status, proceed to show it
              nvZonalRouting.toggleVisibilityMarker(marker, true);
              marker.options.extraInfo.isHideWhenOutsideBound = false;
            }

            // if marker is outside the bound, and in hide status, then do nothing
            // if marker is inside the bound, and in visible status, then do nothing
          }
        });

        loadingMarkers = false;
        isMoveendTimerTriggerring = false;
      }

      function getPadRadio() {
        const zoomLevel = ctrl.map.getZoom();
        if (zoomLevel <= 15) {
          return 0.05;
        }

        switch (ctrl.map.getZoom()) {
          case 16: return 0.4;
          case 17: return 0.7;
          default: return 1; // >= 18
        }
      }
    }

    function createPolygon(e) {
      featureGroup.addLayer(e.layer);

      if (ctrl.activeRoutePolygonData) {
        assignPolygonToActiveRoute(e.layer);
      } else {
        orphanPolygonLayers.push(e.layer);
        setPolygonStyle(e.layer, '#000000');
      }

      e.layer.on('mouseover', () => {
        ctrl.selectedPolygon.onHover = true;

        setSelectedPolygonData(e.layer);
        refresh();
      });

      e.layer.on('mouseout', () => {
        ctrl.selectedPolygon.onHover = false;

        unsetSelectedPolygonData();
        refresh();
      });

      function setSelectedPolygonData(polygon) {
        unsetSelectedPolygonData();

        _.forEach(ctrl.map.allWaypointMarkers, (marker) => {
          if (nvZonalRouting.inPolygon(polygon.getLatLngs(), marker.options.position)) {
            if (isVisibleAndNotRouted(marker)) {
              if (marker.options.extraInfo.isPriority) {
                ctrl.selectedPolygon.count.unroutedPriority += 1;
              } else {
                ctrl.selectedPolygon.count.unroutedNonPriority += 1;
              }
            } else if (isVisibleAndRouted(marker)) {
              if (marker.options.extraInfo.isPriority) {
                ctrl.selectedPolygon.count.routedPriority += 1;
              } else {
                ctrl.selectedPolygon.count.routedNonPriority += 1;
              }
            }
          }
        });
      }

      function unsetSelectedPolygonData() {
        // reset all to 0
        _.forEach(ctrl.selectedPolygon.count, (value, key) => {
          ctrl.selectedPolygon.count[key] = 0;
        });
      }
    }

    function setPolygonStyle(layer, color) {
      const styleConfig = {
        fillOpacity: 0.5,
        opacity: 1,
        color: color,
        fillColor: color,
      };

      layer.setStyle(styleConfig);
    }

    function assignPolygonToActiveRoute(layer) {
      ctrl.activeRoute.nvPolygons = ctrl.activeRoute.nvPolygons || [];
      ctrl.activeRoute.nvPolygons.push(layer);

      setPolygonStyle(layer, ctrl.activeRoutePolygonData.color);
    }

    function visualizeAlgoRoutes() {
      if (_.size(nvZonalRouting.algoRouteAssignments) <= 0) {
        return;
      }

      _.forEach(nvZonalRouting.algoRouteAssignments, (routeData) => {
        const routeId = createNewRoute();

        if (routeData.waypoints) {
          _.forEach(routeData.waypoints, (waypointId) => {
            const waypointMarker = ctrl.map.allWaypointMarkersMap[waypointId];
            if (waypointMarker === undefined) {
              return;
            }
            addMarkerToRoute(waypointMarker, routeId);
          });
        }

        const driver = _.find(ctrl.driversSelectionOptions, ['value', +routeData.driver_id]);
        if (driver) {
          const route = nvZonalRouting.getAssignedRouteData(routeId);
          route.driverSelection.searchText = driver.displayName;
          route.driverSelection.driver = driver;
        }
      });
    }

    function isVisible(marker) {
      return !marker.options.extraInfo.isHideByMapViewSettings &&
        !marker.options.extraInfo.isHideByMapViewOption;
    }

    function isVisibleAndNotRouted(marker) {
      return isVisible(marker) && !marker.options.routeId;
    }

    function isVisibleAndRouted(marker) {
      return isVisible(marker) && marker.options.routeId > 0;
    }

    function addMarkerToRoute(marker, routeId) {
      if (hotkeyEvents.isWaypointDetailsTableLocked) {
        return false;
      }

      // prepare
      const route = nvZonalRouting.getAssignedRouteData(routeId);

      // process
      // only get visible && not routed waypoints
      if (isVisibleAndNotRouted(marker)) {
        // set icon
        nvZonalRouting.setRoutedWaypointMarkerIcon(marker, route);

        marker.options.routeId = route.id;
        route.addedMarkers.push(marker);
        route.seqNo += 1;

        _.forEach(marker.options.transactions, (transaction) => {
          if (transaction.order === undefined) {
            return;
          }

          if (_.size(transaction.order.cod) > 0) {
            route.totalCODWps += 1;
            route.totalCOD += transaction.order.cod.goodsAmount;
          }

          let parcelSizeEnum = null;
          if (_.size(transaction.order.parcelSize) > 0) {
            parcelSizeEnum = transaction.order.parcelSize;
          }

          const parcelSize = Order.getParcelSizeByEnum(parcelSizeEnum);
          if (parcelSize) {
            route.sizeCount[parcelSize.enumValue] += 1;
            route.sizeCounter += parcelSize.capacity;
          }
        });

        if (marker.options.extraInfo.isPriority) {
          route.priority += 1;
        } else {
          route.nonPriority += 1;
        }

        refresh();
        return true;
      }

      return false;
    }

    function removeMarkerFromRoute(marker, forceRemove = false) {
      if (hotkeyEvents.isWaypointDetailsTableLocked) {
        return false;
      }

      if (!forceRemove && !isVisibleAndRouted(marker)) {
        // only visible and routed marker can remove
        return false;
      }

      const route = nvZonalRouting.getAssignedRouteData(marker.options.routeId);
      nvZonalRouting.setWaypointMarkerIcon(marker);

      route.addedMarkers = _.filter(route.addedMarkers, theMarker =>
        theMarker.options.id !== marker.options.id
      );

      _.forEach(marker.options.transactions, (transaction) => {
        if (transaction.order === undefined) {
          return;
        }

        if (_.size(transaction.order.cod) > 0) {
          route.totalCODWps -= 1;
          route.totalCOD -= transaction.order.cod.goodsAmount;
        }

        let parcelSizeEnum = null;
        if (_.size(transaction.order.parcelSize) > 0) {
          parcelSizeEnum = transaction.order.parcelSize;
        }

        const parcelSize = Order.getParcelSizeByEnum(parcelSizeEnum);
        if (parcelSize) {
          route.sizeCount[parcelSize.enumValue] -= 1;
          route.sizeCounter -= parcelSize.capacity;
        }
      });

      if (marker.options.extraInfo.isPriority) {
        route.priority -= 1;
      } else {
        route.nonPriority -= 1;
      }

      delete marker.options.routeId;

      if (route.seqNo > 1) { // prevent seqno going negative
        route.seqNo -= 1;
      }

      refresh();
      return true;
    }

    function addMouseListener(marker) {
      nvMaps.markerAddEvent(marker, 'mouseover', mouseover);
      nvMaps.markerAddEvent(marker, 'mouseout', mouseout);
      nvMaps.markerAddEvent(marker, 'click', click);

      function mouseover(theMarker) {
        if (hotkeyEvents.isWaypointDetailsTableLocked || loadingMarkers) {
          return;
        }
        hotkeyEvents.hoveredMarker = theMarker;

        ctrl.waypointDetailsTableParams.waypointId = theMarker.options.waypoint.id;
        ctrl.waypointDetailsTableParams.sizeCounter = null;
        ctrl.waypointDetailsTableParams.address = nvAddress.extract(theMarker.options.waypoint);
        ctrl.waypointDetailsTableParams.list = [];

        if (Waypoint.isTransaction(theMarker.options.waypoint)) {
          ctrl.waypointDetailsTableParams.isTransactionWp = true;
          ctrl.waypointDetailsTableParams.sizeCounter = 0;

          const transactions = theMarker.options.transactions;
          _.forEach(transactions, (transaction) => {
            const txn = newTxnObject(transaction, theMarker);
            ctrl.waypointDetailsTableParams.list.push(txn);

            const parcelSize = Order.getParcelSizeByEnum(txn.parcelSize);
            if (parcelSize) {
              ctrl.waypointDetailsTableParams.sizeCounter += parcelSize.capacity;
            }
          });
        } else { // Reservation
          ctrl.waypointDetailsTableParams.isTransactionWp = false;
          const reservation = theMarker.options.transactions[0];

          ctrl.waypointDetailsTableParams.list.push(newReservationObject(reservation));
        }

        function newTxnObject(transaction, mrk) {
          let cod = '-';
          let parcelSize = '-';
          let orderType = '-';
          let fromName = '';
          let orderComments = '';
          let orderId = 0;
          let trackingId = '';
          const transactionType = transaction.type ? transaction.type.toLowerCase() : '';

          if (_.size(transaction.order) > 0) {
            if (_.size(transaction.order.cod) > 0) {
              cod = `${nvCurrency.getCode($rootScope.countryId)} ${transaction.order.cod.goodsAmount ? transaction.order.cod.goodsAmount : 0}`;
            }

            orderId = transaction.order.id;
            trackingId = transaction.order.trackingId;
            parcelSize = transaction.order.parcelSize;
            orderType = transaction.order.type ? transaction.order.type.toLowerCase() : '';
            fromName = transaction.order.fromName;
            orderComments = transaction.order.comments === null ? '' : transaction.order.comments;
          }

          return {
            orderId: orderId,
            trackingId: trackingId,
            shipperName: fromName,
            type: `${orderType} ${transactionType}`,
            isPriority: mrk.options.extraInfo.isPriority,
            priorityLevel: transaction.priorityLevel,
            startTime: transaction.startTime,
            endTime: transaction.endTime,
            cod: cod,
            parcelSize: parcelSize,
            orderComments: orderComments,
          };
        }

        function newReservationObject(reservation) {
          return {
            shipperName: reservation.name,
            startTime: reservation.readyDatetime,
            endTime: reservation.latestDatetime,
            approxVolume: reservation.approxVolume,
            comments: reservation.comments,
            dp: reservation.distributionPointId,
          };
        }

        refresh();
      }

      function mouseout() {
        if (!hotkeyEvents.isWaypointDetailsTableLocked && !loadingMarkers) {
          resetWaypointDetailsTableParams();
          refresh();
        }
      }

      function click(theMarker) {
        if (!ctrl.activeRouteId) {
          let errorMessage = nvTranslate.instant('container.zonal-routing.error-add-new-route');
          if (_.size(nvZonalRouting.assignedRoutes) > 0) {
            errorMessage = nvTranslate.instant('container.zonal-routing.error-select-route');
          }

          nvToast.error(errorMessage);
        }

        if (!ctrl.activeRouteSelectWaypointMode.isActive) {
          // cannot add marker to route if 'Select WP' is not active
          return;
        }

        if (theMarker.options.routeId && theMarker.options.routeId !== ctrl.activeRouteId) {
          if (nvModelUtils.fromYesNoBitStringToBool(
              ctrl.activeRouteSelectWaypointMode.ableToAddAnyWp
            )) {
            // if the marker is belongs to other route
            // but user need to add this to current activeRoute
            // then have to remove this marker from route first
            const oriMarkerRouteId = theMarker.options.routeId;

            removeMarkerFromRoute(theMarker);
            resequenceMarkerByRoute(
              nvZonalRouting.getAssignedRouteData(oriMarkerRouteId)
            );
          } else {
            // if ableToAddAnyWp is false, then do nothing
            return;
          }
        }

        if (!theMarker.options.routeId) { // assign to active route if there is
          addMarkerToRoute(theMarker, ctrl.activeRouteId);
        } else { // remove the waypoint from route
          removeMarkerFromRoute(theMarker);
          resequenceMarkerByRoute(
            nvZonalRouting.getAssignedRouteData(ctrl.activeRouteId)
          );
        }

        setUnroutedNonPriorityWPsCount();
        setUnroutedPriorityWPsCount();
      }
    }

    function openAllRoutesMenu($event) {
      nvPanel.show($event, {
        controller: 'ZonalRoutingAllRoutesMenuController',
        templateUrl: 'views/container/zonal-routing/panel/all-routes/all-routes.panel.html',
        panelClass: 'all-routes-menu',
        darkTheme: true,
        xPosition: 'ALIGN_END',
        locals: {
          onClose: onClose,
        },
      });

      function onClose(response) {
        if (response.action === nvZonalRouting.ALL_ROUTES_PANEL_ACTION.DELETE_ROUTES) {
          deleteRoutes($event);
        } else if (response.action === nvZonalRouting.ALL_ROUTES_PANEL_ACTION.PURGE_ROUTES) {
          removeWaypointsByRoutes($event);
        } else if (response.action === nvZonalRouting.ALL_ROUTES_PANEL_ACTION.SET_HUB_FOR_ROUTES) {
          setHubForRoutes($event);
        }
      }
    }

    function openRouteMenu($event, route) {
      nvPanel.show($event, {
        controller: 'ZonalRoutingRouteMenuController',
        templateUrl: 'views/container/zonal-routing/panel/route/route.panel.html',
        panelClass: 'route-menu',
        xPosition: 'ALIGN_END',
        locals: {
          routeData: route,
          onClose: onClose,
        },
      });

      function onClose(response) {
        if (response.action === nvZonalRouting.ROUTE_PANEL_ACTION.DELETE_ROUTE) {
          deleteRoute($event, route);
        } else if (response.action === nvZonalRouting.ROUTE_PANEL_ACTION.PURGE_ROUTE) {
          removeWaypoints($event, route);
        }
      }
    }

    function setActiveRoute(route, expandContentIfCollapse = false) {
      if (ctrl.activeRouteId !== route.id) {
        ctrl.activeRouteId = route.id;
        ctrl.activeRoute = route;

        initPolygonDrawControl(route);
        toggleActiveRouteSelectWaypointMode(route, true); // default active

        if (_.size(orphanPolygonLayers) > 0) {
          _.forEach(orphanPolygonLayers, (layer) => {
            assignPolygonToActiveRoute(layer);
          });

          orphanPolygonLayers = [];
        }
      }

      if (expandContentIfCollapse && route.id !== ctrl.expandRouteId) {
        toggleExpandRoute(route);
      }
    }

    function unsetActiveRoute() {
      ctrl.activeRoute = null;
      ctrl.activeRouteId = 0;
      ctrl.activeRoutePolygonData = null;

      toggleActiveRouteSelectWaypointMode(null, false);
      initPolygonDrawControl();
    }

    function toggleActiveRouteSelectWaypointMode(
      route, toggleIsActive = !ctrl.activeRouteSelectWaypointMode.isActive
    ) {
      let isActive = toggleIsActive;

      if (route && route.id !== ctrl.activeRouteId) {
        setActiveRoute(route);

        isActive = true;
      }

      if (route && route.id !== ctrl.expandRouteId && isActive) {
        // expand route content if it is collapse
        toggleExpandRoute(route);
      }

      ctrl.activeRouteSelectWaypointMode = {
        isActive: isActive,
        ableToAddAnyWp: nvModelUtils.fromBoolToYesNoBitString(true),
      };
    }

    function toggleExpandRoute(route) {
      if (ctrl.expandRouteId !== route.id) {
        ctrl.expandRouteId = route.id;
      } else {
        ctrl.expandRouteId = 0;
      }
    }

    function getHubsSelectionOptionsData(hubId) {
      return _.find(ctrl.hubsSelectionOptions, ['value', hubId]);
    }

    function refresh() {
      if (!$scope.$$phase) {
        $scope.$apply(); // instant refresh
      }
    }

    function addWaypointsInPolygon(
      route, type = nvZonalRouting.POLYGON_TOOLS_ADD_ACTION.UNROUTED
    ) {
      const processed = {
        priority: 0,
        nonPriority: 0,
      };

      _.forEach(ctrl.map.allWaypointMarkers, (marker) => {
        _.forEach(route.nvPolygons, (polygon) => {
          if (nvZonalRouting.inPolygon(polygon.getLatLngs(), marker.options.position)) {
            if ((
                  (type === nvZonalRouting.POLYGON_TOOLS_ADD_ACTION.UNROUTED) ||
                  (type === nvZonalRouting.POLYGON_TOOLS_ADD_ACTION.UNROUTED_PRIORITY &&
                    marker.options.extraInfo.isPriority) ||
                  (type === nvZonalRouting.POLYGON_TOOLS_ADD_ACTION.UNROUTED_NON_PRIORITY &&
                    !marker.options.extraInfo.isPriority)
                ) && !marker.options.routeId) {
              const isSuccess = addMarkerToRoute(marker, route.id);

              if (isSuccess && marker.options.extraInfo.isPriority) {
                processed.priority += 1;
              } else if (isSuccess && !marker.options.extraInfo.isPriority) {
                processed.nonPriority += 1;
              }
            } else if (type === nvZonalRouting.POLYGON_TOOLS_ADD_ACTION.ALL) {
              if (marker.options.routeId) {
                removeMarkerFromRoute(marker);
              }

              const isSuccess = addMarkerToRoute(marker, route.id);

              if (isSuccess && marker.options.extraInfo.isPriority) {
                processed.priority += 1;
              } else if (isSuccess && !marker.options.extraInfo.isPriority) {
                processed.nonPriority += 1;
              }
            }
          }
        });
      });

      resequenceMarkerByRoute(route);

      return processed;
    }

    function removeWaypointsInPolygon(
      route, type = nvZonalRouting.POLYGON_TOOLS_REMOVE_ACTION.ROUTED
    ) {
      const processed = {
        priority: 0,
        nonPriority: 0,
      };

      _.forEach(route.nvPolygons, (polygon) => {
        _.forEach(route.addedMarkers, (marker) => {
          if (nvZonalRouting.inPolygon(polygon.getLatLngs(), marker.options.position)) {
            if ((type === nvZonalRouting.POLYGON_TOOLS_REMOVE_ACTION.ROUTED) ||
              (type === nvZonalRouting.POLYGON_TOOLS_REMOVE_ACTION.ROUTED_PRIORITY &&
                marker.options.extraInfo.isPriority) ||
              (type === nvZonalRouting.POLYGON_TOOLS_REMOVE_ACTION.ROUTED_NON_PRIORITY &&
                !marker.options.extraInfo.isPriority)) {
              const isSuccess = removeMarkerFromRoute(marker);

              if (isSuccess && marker.options.extraInfo.isPriority) {
                processed.priority += 1;
              } else if (isSuccess && !marker.options.extraInfo.isPriority) {
                processed.nonPriority += 1;
              }
            }
          }
        });
      });

      resequenceMarkerByRoute(route);

      return processed;
    }

    function removeWaypoints($event, route) {
      nvDialog.confirmDelete($event, {
        title: nvTranslate.instant('container.zonal-routing.purge-route'),
        content: nvTranslate.instant('container.zonal-routing.purge-route-description'),
        ok: nvTranslate.instant('container.zonal-routing.purge-route'),
        cancel: nvTranslate.instant('commons.go-back'),
      }).then(onConfirm);

      function onConfirm() {
        resetRouteMarkers(route);
      }
    }

    function removeWaypointsByRoutes($event) {
      nvDialog.showSingle($event, {
        theme: 'nvRed',
        templateUrl: 'views/container/zonal-routing/dialog/purge-routes/purge-routes.dialog.html',
        cssClass: 'zonal-routing-purge-routes',
        controller: 'ZonalRoutingPurgeRoutesDialogController',
        controllerAs: 'ctrl',
        locals: {},
      }).then(onPurge);

      function onPurge(response) {
        _.forEach(nvZonalRouting.assignedRoutes, (route) => {
          if (_.indexOf(response.ids, route.id) >= 0) {
            resetRouteMarkers(route);
          }
        });
      }
    }

    function addRelated(route, checkOnly = false) {
      let isAbleToAddRelated = false;

      const postcodes = [];
      _.forEach(route.addedMarkers, (marker) => {
        postcodes.push(marker.options.waypoint && marker.options.waypoint.postcode);
      });

      _.forEach(ctrl.map.allWaypointMarkers, (marker) => {
        if (!isVisibleAndNotRouted(marker)) {
          return undefined;
        }

        if (marker.options.waypoint && marker.options.waypoint.postcode &&
        postcodes.indexOf(marker.options.waypoint.postcode) >= 0) {
          isAbleToAddRelated = true;

          if (checkOnly === false) {
            addMarkerToRoute(marker, route.id);

            ctrl.addRelatedSuccess = true;
            setUnroutedNonPriorityWPsCount();
            setUnroutedPriorityWPsCount();

            $timeout.cancel(animationTimer);
            animationTimer = $timeout(() => {
              ctrl.addRelatedSuccess = false;
            }, 2000);
          }
          return false;
        }

        return undefined;
      });

      resequenceMarkerByRoute(route);
      return isAbleToAddRelated;
    }

    function removeRoute(route) {
      resetRouteMarkers(route);
      deletePolygonsByRoute(route);

      _.remove(nvZonalRouting.assignedRoutes, ['id', route.id]);

      if (route.id === ctrl.activeRouteId) {
        unsetActiveRoute();
      }
    }

    function resetRouteMarkers(route) {
      _.forEach(route.addedMarkers, (marker) => {
        nvZonalRouting.setWaypointMarkerIcon(marker);
        delete marker.options.routeId;
      });

      route.priority = 0;
      route.nonPriority = 0;
      route.totalCOD = 0;
      route.totalCODWps = 0;
      route.addedMarkers = [];
      route.seqNo = 1;
      route.sizeCounter = 0;
      route.sizeCount = generateSizeCount();

      setUnroutedNonPriorityWPsCount();
      setUnroutedPriorityWPsCount();
    }

    function submitRoutes($event) {
      const routes = [];
      const waypointIds = [];

      // validation and preparation
      const errorRouteIds = {
        invalidDate: [], // cannot proceed if have
        noWaypoint: [], // cannot proceed if have
        noHub: [], // cannot proceed if have
        noDriver: [], // still can proceed, but need to prompt dialog
      };
      _.forEach(nvZonalRouting.assignedRoutes, (assignedRoute) => {
        const routeDateFrom = nvDateTimeUtils.displayDateTime(
          nvDateTimeUtils.toMoment(assignedRoute.date).startOf('day'), 'UTC'
        );

        assignedRoute.hubId = 0;
        if (assignedRoute.hubSelection.hub) {
          // move driverId to parent level if driver is assigned
          assignedRoute.hubId = assignedRoute.hubSelection.hub.value;
        }

        assignedRoute.driverId = 0;
        if (assignedRoute.driverSelection.driver) {
          // move driverId to parent level if driver is assigned
          assignedRoute.driverId = assignedRoute.driverSelection.driver.value;
        }

        const route = {
          date: routeDateFrom,
          zoneId: getZoneId(nvZonalRouting.chosenZone),
          hubId: +assignedRoute.hubId,
          waypoints: [],
        };

        if (!assignedRoute.date) {
          errorRouteIds.invalidDate.push(assignedRoute.id);
        }

        if (_.size(assignedRoute.addedMarkers) <= 0) {
          errorRouteIds.noWaypoint.push(assignedRoute.id);
        }

        if (!assignedRoute.hubId) {
          errorRouteIds.noHub.push(assignedRoute.id);
        } else {
          route.hubId = +assignedRoute.hubId;
        }

        if (!assignedRoute.driverId) {
          errorRouteIds.noDriver.push(assignedRoute.id);
        } else {
          route.driverId = +assignedRoute.driverId;

          const driver = nvZonalRouting.getChosenZoneDriverData(route.driverId);
          if (driver) {
            route.vehicleId = +driver.vehicleId;
          }
        }

        _.forEach(assignedRoute.addedMarkers, (marker) => {
          route.waypoints.push(marker.options.id);
          waypointIds.push(marker.options.id);
        });

        routes.push(route);
      });

      const duplicateWaypointIds = _.filter(waypointIds, (value, index, iteratee) =>
          _.includes(iteratee, value, index + 1)
      ); // cannot proceed if duplicate waypoint found

      // show validation error (if have)
      if (errorRouteIds.invalidDate.length > 0 || errorRouteIds.noWaypoint.length > 0 ||
        errorRouteIds.noHub.length > 0 || duplicateWaypointIds.length > 0) {
        if (errorRouteIds.invalidDate.length > 0) {
          nvToast.error(nvTranslate.instant('container.zonal-routing.error-invalid-date-for-routes', {
            routes: errorRouteIds.invalidDate.join(', '),
          }));
        }

        if (errorRouteIds.noWaypoint.length > 0) {
          nvToast.error(nvTranslate.instant('container.zonal-routing.error-no-waypoints-added-for-routes', {
            routes: errorRouteIds.noWaypoint.join(', '),
          }));
        }

        if (errorRouteIds.noHub.length > 0) {
          nvToast.error(nvTranslate.instant('container.zonal-routing.error-no-hub-for-routes', {
            routes: errorRouteIds.noHub.join(', '),
          }));
        }

        if (duplicateWaypointIds.length > 0) {
          nvToast.error(nvTranslate.instant('container.zonal-routing.error-waypoints-duplicate', {
            waypoints: duplicateWaypointIds.join(', '),
          }));
        }

        return;
      }

      // submitting
      let confirmSaveContent = _.noop();
      if (errorRouteIds.noDriver.length > 0) {
        confirmSaveContent = nvTranslate.instant('container.zonal-routing.alert-no-driver-added-for-routes', {
          routes: errorRouteIds.noDriver.join(', '),
        });
      }

      nvDialog.confirmSave($event, {
        title: nvTranslate.instant('commons.save-changes'),
        content: confirmSaveContent,
      }).then(submittingRoutes);

      function getZoneId(theZone) {
        if (theZone.isCustomZone) {
          return theZone.extraData.zoneId;
        }

        return +theZone.id;
      }

      function submittingRoutes() {
        parentContentLoading.backdropLoading = true;

        if (DEBUG) {
          $.getJSON('views/container/zonal-routing/json/submitted-routes.json', (submittedRoutes) => {
            $timeout(() => {
              createRoutesSuccess(submittedRoutes);
            });
          });
        } else {
          createRouteByProgressBar(routes);
        }

        function createRouteByProgressBar(datas) {
          const BULK_RESULT = {
            successRoute: [],
            failedRoute: [],
          };

          const BULK_ACTION_PROGRESS = {
            totalCount: _.size(datas),
            successCount: 0,
            currentIndex: 0,
            errors: [],
          };

          if (_.size(datas) === 0) {
            return;
          }

          nvDialog.showSingle($event, {
            templateUrl: 'lib/ninja-commons/services/dialogs/bulk-action-progress/bulk-action-progress.dialog.html',
            cssClass: 'bulk-action-progress',
            controller: 'BulkActionProgressDialogController',
            controllerAs: 'ctrl',
            skipHide: true,
            clickOutsideToClose: false,
            locals: {
              payload: BULK_ACTION_PROGRESS,
            },
          }).then(progressDialogClosed);

          startCreateRoute(datas);

          function progressDialogClosed() {
            if (getSuccessCount() > 0) {
              // todo: toast success
            }
            if (getErrorCount() > 0) {
              nvToast.error(nvTranslate.instant('container.zonal-routing.routes-failed'));
              showSubmittedRoutesErrorDialog($event, BULK_RESULT.failedRoute)
                .then((successRoutes) => {
                  const allSuccessRoutes = _.concat(BULK_RESULT.successRoute, successRoutes);
                  if (_.size(allSuccessRoutes) > 0) {
                    showSubmittedRoutesDialog($event, allSuccessRoutes);
                  } else {
                    // in case no routes created
                    nvToast.warn(nvTranslate.instant('container.zonal-routing.no-routes-created'));
                    goToAssignmentPage();
                  }
                });
            } else {
              showSubmittedRoutesDialog($event, BULK_RESULT.successRoute);
            }
            parentContentLoading.backdropLoading = false;
          }

          function startCreateRoute(requests) {
            let request;
            if (_.size(requests) > 0) {
              // request is an array with single element
              request = requests.splice(0, 1);
              BULK_ACTION_PROGRESS.currentIndex += 1;
              Route.createRoutes(request)
                .then(onCreateSuccess, onCreateFailed);
            }

            function onCreateSuccess(response) {
              BULK_RESULT.successRoute = _.concat(BULK_RESULT.successRoute, response);
              BULK_ACTION_PROGRESS.successCount += 1;
              if (_.size(requests) > 0) {
                startCreateRoute(requests);
              } else {
                finish();
              }
            }

            function onCreateFailed(response) {
              let routedWaypointIds = [];
              if (response.status === 400 && _.isArray(response.data)) {
                routedWaypointIds = response.data;
              }
              BULK_RESULT.failedRoute = _.concat(BULK_RESULT.failedRoute, {
                request: request,
                wpIds: routedWaypointIds,
              });
              BULK_ACTION_PROGRESS.errors.push(
                nvTranslate.instant('container.zonal-routing.waypoint-ids-in-status-routed', {
                  ids: _.join(routedWaypointIds, ', '),
                })
              );
              if (_.size(requests) > 0) {
                startCreateRoute(requests);
              } else {
                finish();
              }
            }

            function finish() {}
          }

          function getSuccessCount() {
            return _.size(BULK_RESULT.successRoute);
          }

          function getErrorCount() {
            return _.size(BULK_RESULT.failedRoute);
          }
        }

        function createRoutesSuccess(submittedRoutes) {
          nvZonalRouting.routesSubmitted = true;

          parentContentLoading.backdropLoading = false;
          showSubmittedRoutesDialog($event, submittedRoutes);
        }
      }
    }

    function setUnroutedNonPriorityWPsCount() {
      if (ctrl.map) {
        ctrl.unroutedNonPriorityWPsCount = _.size(
          _.filter(ctrl.map.nonPriorityWaypointMarkers, marker =>
            !marker.options.routeId
        ));
      }
    }

    function setUnroutedPriorityWPsCount() {
      if (ctrl.map) {
        ctrl.unroutedPriorityWPsCount = _.size(
          _.filter(ctrl.map.priorityWaypointMarkers, marker =>
            !marker.options.routeId
        ));
      }
    }

    function getAssignedDriversCount() {
      if (_.size(nvZonalRouting.chosenZone) <= 0) {
        return null;
      }

      return nvZonalRouting.chosenZone.drivers.length - ctrl.driversSelectionOptions.length;
    }

    function toggleIsWaypointDetailsTableLocked() {
      const newTableLockedValue = !hotkeyEvents.isWaypointDetailsTableLocked;

      if (hotkeyEvents.hoveredMarker && newTableLockedValue) {
        nvZonalRouting.setHoveredWaypointMarkerIcon(hotkeyEvents.hoveredMarker);

        hotkeyEvents.isWaypointDetailsTableLocked = newTableLockedValue;
      } else if (hotkeyEvents.hoveredMarker && !newTableLockedValue) {
        nvZonalRouting.setWaypointMarkerIcon(hotkeyEvents.hoveredMarker);

        resetWaypointDetailsTableParams();
        hotkeyEvents.isWaypointDetailsTableLocked = newTableLockedValue;
      }
    }

    function navigateToEditOrderPage(orderId) {
      $window.open(
        $state.href('container.order.edit', { orderId: orderId })
      );
    }

    function getParcelSizeShortName(parcelSizeEnum) {
      const parcelSize = Order.getParcelSizeByEnum(parcelSizeEnum);
      if (parcelSize) {
        return parcelSize.displayNameShort;
      }

      return null;
    }

    function showSubmittedRoutesDialog($event, submittedRoutes) {
      nvDialog.showSingle($event, {
        templateUrl: 'views/container/zonal-routing/dialog/submitted-routes/submitted-routes.dialog.html',
        theme: 'nvGreen',
        cssClass: 'zonal-routing-submitted-routes',
        controller: 'ZonalRoutingSubmittedRoutesDialogController',
        controllerAs: 'ctrl',
        locals: {
          submittedRoutes: submittedRoutes,
        },
      }).finally(onClose);

      function onClose() {
        goToAssignmentPage();
      }
    }

    function showSubmittedRoutesErrorDialog($event, errorRoutes) {
      return nvDialog.showSingle($event, {
        templateUrl: 'views/container/zonal-routing/dialog/error-routes/error-routes.dialog.html',
        theme: 'nvBlue',
        cssClass: 'zonal-routing-error-routes',
        controller: 'ZonalRoutingErrorRoutesDialogController',
        controllerAs: 'ctrl',
        escapeToClose: false,
        locals: {
          errorRoutes: errorRoutes,
        },
      });
    }

    function createNewRoute() {
      const route = newRouteObj();
      assignHubToRoute(route);

      nvZonalRouting.assignedRoutes.push(route);

      if (!ctrl.activeRouteId) {
        setActiveRoute(route);
      }

      return route.id;

      function newRouteObj() {
        const routeId = generateRouteId();

        return {
          id: routeId,
          date: nvZonalRouting.routeDate,
          seqNo: 1,
          priority: 0,
          nonPriority: 0,
          totalCOD: 0,
          totalCODWps: 0,
          sizeCounter: 0,
          sizeCount: generateSizeCount(),
          hubId: 0,
          hubSelection: {
            searchText: '',
            hub: null,
          },
          driverId: 0,
          driverSelection: {
            searchText: '',
            driver: null,
          },
          color: nvZonalRouting.getRouteColor(routeId),
          addedMarkers: [],
          nvPolygons: [],
        };

        function generateRouteId() {
          const lastRoute = _.findLast(nvZonalRouting.assignedRoutes);

          return lastRoute ? lastRoute.id + 1 : 1;
        }
      }
    }

    function deleteRoutes($event) {
      nvDialog.showSingle($event, {
        theme: 'nvRed',
        templateUrl: 'views/container/zonal-routing/dialog/delete-routes/delete-routes.dialog.html',
        cssClass: 'zonal-routing-delete-routes',
        controller: 'ZonalRoutingDeleteRoutesDialogController',
        controllerAs: 'ctrl',
        locals: {},
      }).then(onDelete);

      function onDelete(response) {
        const assignedRoutesMap = _.keyBy(nvZonalRouting.assignedRoutes, 'id');
        _.forEach(assignedRoutesMap, (route) => {
          if (_.indexOf(response.ids, route.id) >= 0) {
            removeRoute(route);
          }
        });
      }
    }

    function showRouteWaypointsDialog($event, route) {
      nvDialog.showSingle($event, {
        templateUrl: 'views/container/zonal-routing/dialog/view-waypoints/view-waypoints.dialog.html',
        cssClass: 'zonal-routing-view-waypoints',
        controller: 'ZonalRoutingViewWaypointsDialogController',
        controllerAs: 'ctrl',
        locals: {
          selectedRoute: route,
        },
      }).then(onRemove);

      function onRemove(response) {
        if (response.markers) {
          _.forEach(response.markers, (marker) => {
            const theMarker = _.find(route.addedMarkers, ['options.id', marker.options.id]);
            if (theMarker) {
              removeMarkerFromRoute(theMarker, true);
            }
          });

          resequenceMarkerByRoute(route);
          setUnroutedNonPriorityWPsCount();
          setUnroutedPriorityWPsCount();
        }
      }
    }

    function driversTabSelected() {
      ctrl.isDriversTabSelected = true;
    }

    function suggestDrivers($event) {
      nvDialog.showSingle($event, {
        templateUrl: 'views/container/zonal-routing/dialog/suggest-drivers/suggest-drivers.dialog.html',
        cssClass: 'zonal-routing-suggest-drivers',
        controller: 'ZonalRoutingSuggestDriversDialogController',
        controllerAs: 'ctrl',
        locals: {
          settingsData: {
            isImprovedVersion: false,
          },
        },
      }).then(onSet);

      function onSet() {
        // refresh
        refreshDriverSelectionOptions();
        refreshAddDriverSelectionOptions();
      }
    }

    function removeChosenZoneDriver(driver) {
      nvZonalRouting.addDriverToDriversByZones(driver);

      _.forEach(nvZonalRouting.assignedRoutes, (assignedRoute) => {
        if (_.get(assignedRoute, 'driverSelection.driver.value') === driver.id) {
          assignedRoute.driverSelection.searchText = '';
          assignedRoute.driverSelection.driver = null;
          return false;
        }

        return _.noop();
      });

      refreshDriverSelectionOptions();
      refreshAddDriverSelectionOptions();
    }

    function addDriverOnChange(selectionOptions) {
      const driverId = _.get(selectionOptions, '[0].value');
      if (driverId) {
        const driver = nvZonalRouting.getDriverData(driverId);

        // check if assigned to another zone
        // remove driver from the zone first if assigned
        if (nvZonalRouting.isAssignedToZone(driver)) {
          nvZonalRouting.addDriverToDriversByZones(driver);
        }

        // add driver to zone
        nvZonalRouting.addDriverToZone(driverId, nvZonalRouting.chosenZone.id);

        // reset input field
        ctrl.addDriver.searchText = '';
        ctrl.addDriver.driver = '';

        // refresh
        refreshDriverSelectionOptions();
        refreshAddDriverSelectionOptions();
      }
    }

    function refreshDriverSelectionOptions() {
      // process driversSelectionOptions, excluding those already chosen
      ctrl.driversSelectionOptions = Driver.toOptions({
        drivers: _.filter(nvZonalRouting.chosenZone.drivers, theDriver =>
            !_.some(nvZonalRouting.assignedRoutes, assignedRoute =>
              _.get(assignedRoute, 'driverSelection.driver.value') === theDriver.id
            )
        ),
      });

      $timeout(() => {
        driverSelectAutocomplete.setPossibleOptions(ctrl.driversSelectionOptions);
      });
    }

    function refreshAddDriverSelectionOptions() {
      ctrl.addDriver.selectionOptions = Driver.toOptions({
        drivers: _.filter(nvZonalRouting.driversTable.available, driver =>
            !_.some(nvZonalRouting.chosenZone.drivers, chosenZoneDriver =>
              driver.id === chosenZoneDriver.id
            )
        ),
      });

      $timeout(() => {
        addDriverAutoComplete.setPossibleOptions(ctrl.addDriver.selectionOptions);
      });
    }

    function getAssignedHubNameByDriver(driver) {
      const route = getAssignedRouteDataByDriverId(driver.id);

      if (route && route.hubSelection.hub) {
        return nvZonalRouting.getHubName(route.hubSelection.hub.value);
      }

      return null;
    }

    function getAssignedRouteNameByDriver(driver) {
      const route = getAssignedRouteDataByDriverId(driver.id);

      if (route) {
        return nvZonalRouting.getRouteName(route.id);
      }

      return null;
    }

    function deleteRoute($event, route) {
      nvDialog.confirmDelete($event, {
        title: nvTranslate.instant('container.zonal-routing.delete-route'),
        content: nvTranslate.instant('container.zonal-routing.delete-route-description'),
        ok: nvTranslate.instant('container.zonal-routing.delete-route'),
        cancel: nvTranslate.instant('commons.go-back'),
      }).then(onConfirm);

      function onConfirm() {
        if (route.id === ctrl.activeRouteId) {
          // if this is active route, then toggle setActiveRoute to unset
          setActiveRoute(route);
        }

        if (route.id === ctrl.expandRouteId) {
          // if this is expanded route, then toggle toggleExpandRoute to unset
          toggleExpandRoute(route);
        }

        removeRoute(route);
      }
    }

    function hasPolygonInRoute() {
      const filteredRoutes = _.filter(nvZonalRouting.assignedRoutes, route =>
        _.size(route.nvPolygons) > 0
      );

      return _.size(filteredRoutes) > 0;
    }

    function hasPolygon() {
      return hasPolygonInRoute() || _.size(orphanPolygonLayers) > 0;
    }

    function isAbleToPerformPolygonAction() {
      return (ctrl.activeRouteId &&
      ctrl.currentApplyActionSelection === ctrl.POLYGON_TOOLS_APPLY_ACTION.ACTIVE_ROUTE)
      || (ctrl.currentApplyActionSelection === ctrl.POLYGON_TOOLS_APPLY_ACTION.ALL_POLYGONS &&
        hasPolygonInRoute());
    }

    function openPolygonToolsApplyActionMenu($event) {
      nvPanel.show($event, {
        controller: 'ZRPolygonToolsApplyActionController',
        templateUrl: 'views/container/zonal-routing/panel/polygon-tools-apply-action/polygon-tools-apply-action.panel.html',
        panelClass: 'polygon-tools-apply-action-menu',
        darkTheme: true,
        xPosition: 'ALIGN_END',
        locals: {
          onClose: onClose,
        },
      });

      function onClose(response) {
        ctrl.currentApplyActionSelection = response.value;
      }
    }

    function openPolygonToolsActionMenu($event, actionKey) {
      let polygonToolsAction = nvZonalRouting.POLYGON_TOOLS_ADD_ACTION;
      if (POLYGON_TOOLS_ACTION[actionKey].icon === POLYGON_TOOLS_ACTION.REMOVE.icon) {
        polygonToolsAction = nvZonalRouting.POLYGON_TOOLS_REMOVE_ACTION;
      }

      nvPanel.show($event, {
        controller: 'ZonalRoutingPolygonToolsMenuController',
        templateUrl: 'views/container/zonal-routing/panel/polygon-tools/polygon-tools.panel.html',
        panelClass: 'polygon-tools-menu',
        darkTheme: true,
        xPosition: 'ALIGN_END',
        locals: {
          onClose: onClose,
          polygonToolsAction: polygonToolsAction,
        },
      });

      function onClose(response) {
        polygonToolsPerformAction(actionKey, response.action);
      }
    }

    function polygonToolsPerformAction(actionKey, type) {
      ctrl.polygonToolsPerformActionResult.processed.priority = 0;
      ctrl.polygonToolsPerformActionResult.processed.nonPriority = 0;

      if (POLYGON_TOOLS_ACTION[actionKey].icon === POLYGON_TOOLS_ACTION.ADD.icon) {
        // ADD action performed
        POLYGON_TOOLS_ACTION.ADD.currentTypeSelection = type;
        ctrl.polygonToolsPerformActionResult.action = POLYGON_TOOLS_ACTION.ADD.icon;

        if (ctrl.currentApplyActionSelection === ctrl.POLYGON_TOOLS_APPLY_ACTION.ALL_POLYGONS) {
          _.forEach(nvZonalRouting.assignedRoutes, (route) => {
            const processed = addWaypointsInPolygon(
              route, POLYGON_TOOLS_ACTION.ADD.currentTypeSelection
            );

            ctrl.polygonToolsPerformActionResult.processed.priority += processed.priority;
            ctrl.polygonToolsPerformActionResult.processed.nonPriority += processed.nonPriority;
          });
        } else {
          const processed = addWaypointsInPolygon(
            ctrl.activeRoute, POLYGON_TOOLS_ACTION.ADD.currentTypeSelection
          );

          ctrl.polygonToolsPerformActionResult.processed.priority += processed.priority;
          ctrl.polygonToolsPerformActionResult.processed.nonPriority += processed.nonPriority;
        }
      } else {
        // REMOVE action performed
        POLYGON_TOOLS_ACTION.REMOVE.currentTypeSelection = type;
        ctrl.polygonToolsPerformActionResult.action = POLYGON_TOOLS_ACTION.REMOVE.icon;

        if (ctrl.currentApplyActionSelection === ctrl.POLYGON_TOOLS_APPLY_ACTION.ALL_POLYGONS) {
          _.forEach(nvZonalRouting.assignedRoutes, (route) => {
            const processed = removeWaypointsInPolygon(
              route, POLYGON_TOOLS_ACTION.REMOVE.currentTypeSelection
            );

            ctrl.polygonToolsPerformActionResult.processed.priority += processed.priority;
            ctrl.polygonToolsPerformActionResult.processed.nonPriority += processed.nonPriority;
          });
        } else {
          const processed = removeWaypointsInPolygon(
            ctrl.activeRoute, POLYGON_TOOLS_ACTION.REMOVE.currentTypeSelection
          );

          ctrl.polygonToolsPerformActionResult.processed.priority += processed.priority;
          ctrl.polygonToolsPerformActionResult.processed.nonPriority += processed.nonPriority;
        }
      }

      ctrl.polygonToolsPerformActionResult.isSuccess = true;
      setUnroutedNonPriorityWPsCount();
      setUnroutedPriorityWPsCount();

      $timeout.cancel(animationTimer);
      animationTimer = $timeout(() => {
        ctrl.polygonToolsPerformActionResult.isSuccess = false;
      }, 2000);
    }

    function polygonToolsHasWPProcessed() {
      return (ctrl.polygonToolsPerformActionResult.processed.priority +
        ctrl.polygonToolsPerformActionResult.processed.nonPriority) > 0;
    }

    function polygonToolsGetSuccessText() {
      if (!polygonToolsHasWPProcessed()) {
        return nvTranslate.instant('commons.no-changes');
      }

      let languageKey = 'container.zonal-routing.num-wp-x-added';
      if (ctrl.polygonToolsPerformActionResult.action === POLYGON_TOOLS_ACTION.REMOVE.icon) {
        languageKey = 'container.zonal-routing.num-wp-x-removed';
      }

      return nvTranslate.instant(languageKey, {
        num: ctrl.polygonToolsPerformActionResult.processed.nonPriority +
        ctrl.polygonToolsPerformActionResult.processed.priority,
        details: getNPAndPText(
          ctrl.polygonToolsPerformActionResult.processed.nonPriority,
          ctrl.polygonToolsPerformActionResult.processed.priority
        ),
      });
    }

    function deleteAllPolygons() {
      _.forEach(nvZonalRouting.assignedRoutes, (route) => {
        deletePolygonsByRoute(route);
      });
    }

    function getNPAndPText(nonPriority, priority) {
      const textArray = [];

      if (nonPriority > 0) {
        textArray.push(nvTranslate.instant('container.zonal-routing.num-non-priority-abbr', {
          num: nonPriority,
        }));
      }

      if (priority > 0) {
        textArray.push(nvTranslate.instant('container.zonal-routing.num-priority-abbr', {
          num: priority,
        }));
      }

      return textArray.join(', ');
    }

    function getSelectedPolygonUnroutedText() {
      return getNPAndPText(
        ctrl.selectedPolygon.count.unroutedNonPriority, ctrl.selectedPolygon.count.unroutedPriority
      );
    }

    function getSelectedPolygonRoutedText() {
      return getNPAndPText(
        ctrl.selectedPolygon.count.routedNonPriority, ctrl.selectedPolygon.count.routedPriority
      );
    }

    function setHubForRoutes($event) {
      nvDialog.showSingle($event, {
        templateUrl: 'views/container/zonal-routing/dialog/set-hub-for-routes/set-hub-for-routes.dialog.html',
        cssClass: 'zonal-routing-set-hub-for-routes',
        controller: 'ZonalRoutingSetHubForRoutesDialogController',
        controllerAs: 'ctrl',
        locals: {},
      }).then(onSet);

      function onSet(response) {
        _.forEach(nvZonalRouting.assignedRoutes, (route) => {
          if (_.indexOf(response.routeIds, route.id) >= 0) {
            setHubDataToRoute(route, getHubsSelectionOptionsData(response.hubId));
          }
        });
      }
    }

    function assignHubToRoute(route) {
      if (!nvZonalRouting.chosenZone.hubId && nvZonalRouting.hubs[0]) {
        setHubDataToRoute(route, getHubsSelectionOptionsData(nvZonalRouting.hubs[0].id));
        return;
      }

      setHubDataToRoute(route, getHubsSelectionOptionsData(nvZonalRouting.chosenZone.hubId));
    }

    function setHubDataToRoute(theRoute, theHub) {
      if (theHub) {
        theRoute.hubId = theHub.value;
        theRoute.hubSelection.searchText = theHub.displayName;
        theRoute.hubSelection.hub = theHub;
      }
    }

    function initPolygonDrawControl(route) {
      if (route) {
        ctrl.activeRoutePolygonData = {
          color: route.color.primary,
          routeId: route.id,
        };
      }

      drawControl.setDrawingOptions({
        polygon: {
          shapeOptions: {
            color: route ? ctrl.activeRoutePolygonData.color : '#000000',
            fillColor: route ? ctrl.activeRoutePolygonData.color : '#000000',
            fillOpacity: 0.5,
            opacity: 1,
          },
        },
        polyline: false,
        rectangle: false,
        circle: false,
        marker: false,
      });
    }

    function getCurrencyWithPrice(amount) {
      if (!amount) {
        return `${nvCurrency.getCode($rootScope.countryId)} ${0}`;
      }

      return `${nvCurrency.getCode($rootScope.countryId)} ${amount}`;
    }

    function generateSizeCount() {
      const size = {};
      _.forEach(Order.PARCEL_SIZE, (parcelSize) => {
        size[parcelSize.enumValue] = 0;
      });

      return size;
    }

    function getAssignedRouteDataByDriverId(driverId) {
      let route = null;
      _.forEach(nvZonalRouting.assignedRoutes, (assignedRoute) => {
        if (assignedRoute.driverSelection.driver &&
          assignedRoute.driverSelection.driver.value === driverId) {
          route = assignedRoute;
          return false;
        }

        return undefined;
      });

      return route;
    }

    function resetWaypointDetailsTableParams() {
      hotkeyEvents.hoveredMarker = null;

      ctrl.waypointDetailsTableParams.waypointId = null;
      ctrl.waypointDetailsTableParams.sizeCounter = null;
      ctrl.waypointDetailsTableParams.address = null;
      ctrl.waypointDetailsTableParams.list = [];
    }

    function routeBackgroundColorStyling(route) {
      return {
        'background-color': route.color.primary,
      };
    }

    function routeTableResizeWithOffset() {
      const divHeight = $('.content').height() -
        $('md-tabs-wrapper').outerHeight() -
        $('.wp-filter-holder').outerHeight() -
        $('.route-control-holder').outerHeight() -
        $('.polygon-tools-holder').outerHeight() - 23;

      const maxHeightString = `${divHeight}px`;

      return {
        'max-height': maxHeightString,
      };
    }

    function driverTableResizeWithOffset() {
      const divHeight = $('.content').height() -
        $('md-tabs-wrapper').outerHeight() -
        $('.add-driver-autocomplete-holder').outerHeight() -
        $('.driver-control-holder').outerHeight();

      const maxHeightString = `${divHeight}px`;

      return {
        height: maxHeightString,
      };
    }
  }
}());
