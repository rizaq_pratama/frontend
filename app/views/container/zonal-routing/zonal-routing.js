(function controller() {
  angular
        .module('nvOperatorApp.controllers')
        .controller('ZonalRoutingController', ZonalRoutingController);

  ZonalRoutingController.$inject = [
    '$scope', '$timeout', 'Tag', '$q',
    'nvZonalRouting', 'Hub', 'RouteGroup', 'DriverType', 'Zone',
    '$rootScope', '$state', 'nvDomain', '$window',
  ];

  function ZonalRoutingController(
    $scope, $timeout, Tag, $q,
    nvZonalRouting, Hub, RouteGroup, DriverType, Zone,
    $rootScope, $state, nvDomain, $window
  ) {
    // variables
    const DEBUG = nvZonalRouting.isDebug();

    // variables (that also use in html)
    const ctrl = this;

    ctrl.contentLoading = {
      status: true,
      message: 'container.zonal-routing.load-data',
      backdropLoading: false,
    };

    // variables and functions (that will use by child controllers)
    $scope.contentLoading = ctrl.contentLoading;
    $scope.nvZonalRouting = nvZonalRouting;
    $scope.windowSizeChanged = windowSizeChanged;

    // functions
    // Initialization
    ctrl.windowSizeChanged = windowSizeChanged;

    // start
    initialize();

    // function details
    // //////////////////////////////////////////////////////////////////////////////
    // Initialization //////////////////////////////////////////////////////////////
    // //////////////////////////////////////////////////////////////////////////////
    function initialize() {
      const domain = nvDomain.getDomain();
      if ($rootScope.domain !== domain.systemIdFromUrl) {
        $window.location.reload();
        return _.noop();
      }

      nvZonalRouting.initializeVariables();

      if ($state.current.name === 'container.zonal-routing-improved.edit') {
        // do not call any endpoints for Edit Route page
        // let Edit Route page itself to handle
        ctrl.contentLoading.status = false;
        ctrl.contentLoading.message = '';
        return $q.when();
      }

      if (DEBUG) {
        return $.getJSON('views/container/zonal-routing/json/route-tags.json', (tags) => {
          $.getJSON('views/container/zonal-routing/json/hubs.json', (hubs) => {
            $.getJSON('views/container/zonal-routing/json/route-groups.json', (routeGroups) => {
              $.getJSON('views/container/zonal-routing/json/driver-types.json', (driverTypes) => {
                $.getJSON('views/container/zonal-routing/json/zones.json', (zones) => {
                  $timeout(() => {
                    success([tags.data, hubs, routeGroups.data, driverTypes.data, zones]);
                  });
                });
              });
            });
          });
        });
      }

      return $q.all([
        Tag.all(), Hub.read({ active_only: false }), RouteGroup.read(), DriverType.searchAll(), Zone.read(),
      ]).then(success, failure);

      function success(response) {
        nvZonalRouting.routeTags = response[0].tags || [];
        nvZonalRouting.hubs = response[1] || [];
        nvZonalRouting.routeGroups = response[2].routeGroups || [];
        nvZonalRouting.driverTypes = response[3].driverTypes || [];
        nvZonalRouting.zonesResult = response[4];

        ctrl.contentLoading.status = false;
        ctrl.contentLoading.message = '';
      }

      function failure() {

      }
    }

    function windowSizeChanged() {
      if ($state.current.name === 'container.zonal-routing-improved.edit') {
        return;
      }

      $timeout(() => {
        nvZonalRouting.contentHeight = $scope.$windowHeight - $('.action-toolbar').outerHeight() - $('.nv-container > md-toolbar').outerHeight();
        $('.content').height(`${nvZonalRouting.contentHeight}px`);
      });
    }
  }
}());
