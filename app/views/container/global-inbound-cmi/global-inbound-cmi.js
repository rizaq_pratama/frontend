(function controller() {
  angular.module('nvOperatorApp.controllers')
    .controller('GlobalInboundCmiController', GlobalInboundCmiController);

  GlobalInboundCmiController.$inject = ['Hub', 'nvAutocomplete.Data', 'hotkeys', '$q',
    'Order', 'Warehousing', 'Inbound', 'nvDialog', '$scope',
    '$window', 'nvToast', '$timeout', 'nvSoundService', '$interval',
    'nvTranslate',
  ];
  function GlobalInboundCmiController(Hub, nvAutocompleteData, hotkeys, $q, Order,
    Warehousing, Inbound, nvDialog, $scope,
    $window, nvToast, $timeout, nvSoundService, $interval,
    nvTranslate
  ) {
    const ctrl = this;
    const SORTING_HUB_SCAN_TYPE = 'SORTING_HUB';
    ctrl.loadingSheet = false;
    ctrl.DEVICE_STATUS = {
      DEVICE_CONNECTED: 'Device Connected',
      CONNECTION: 'Connecting to device',
      DISCONNECTED: 'Device disconnected',
      NO_DEVICE: 'No Device',
    };

    const SCAN_RESULT = {
      RECEIVED_BY_SORTING_HUB: 'RECEIVED_BY_SORTING_HUB',
      ORDER_NOT_FOUND: 'ORDER_NOT_FOUND',
      PENDING_DELIVERY: 'PENDING_DELIVERY',
      ORDER_COMPLETED: 'ORDER_COMPLETED',
      ORDER_RETURNED_TO_SENDER: 'ORDER_RETURNED_TO_SENDER',
      ORDER_CANCELLED: 'ORDER_CANCELLED',
      SUCCESSFUL_INBOUND: 'SUCCESSFUL_INBOUND',
      ORDER_TRANSFERRED_TO_3PL: 'ORDER_TRANSFERRED_TO_3PL',
      ON_HOLD: 'ON_HOLD',
    };

    ctrl.data = {};
    ctrl.state = {};
    ctrl.const = {};
    ctrl.showPage = 'filter';
    ctrl.loadState = 'idle';
    ctrl.hubSearch = nvAutocompleteData.getByHandle('hubs-auto');
    ctrl.data.dnrs = [];
    ctrl.data.setAsides = [];
    // ctrl.data.hub = '30JKB';
    ctrl.data.deviceId = null;
    ctrl.data.scanCounter = 0;
    ctrl.const.sizes = [
      {
        value: 'S',
        longName: 'SMALL',
        displayName: 'S',
      },
      {
        value: 'M',
        longName: 'MEDIUM',
        displayName: 'M',
      },
      {
        value: 'L',
        longName: 'LARGE',
        displayName: 'L',
      },
      {
        value: 'XL',
        longName: 'EXTRALARGE',
        displayName: 'XL',
      },
    ];
    ctrl.state.dimensionStateAuto = false;
    ctrl.state.sizingStateAuto = true;
    ctrl.state.weightingStateAuto = false;
    ctrl.state.ready = true;
    ctrl.state.isNewDimming = false;
    ctrl.state.internetConnection = true;
    ctrl.state.internetConnectionMessage = 'container.global-inbound.online';
    ctrl.state.isPrefixSet = false;
    ctrl.state.weightHigher = false;
    ctrl.state.dimensionsError = false;
    ctrl.state.isRecovery = false;
    ctrl.state.weightExceedMaxLimit = false;

    ctrl.socket = null;

    const DOMAIN = $window.location.hostname.substr($window.location.hostname.indexOf('.'));
    const ENV_MATCH = $window.location.hostname.match(/(-.*)\.ninja/);
    const ENV = (ENV_MATCH && ENV_MATCH[1]) || '';
    const SOCKET_URL = `https://iot${ENV}${DOMAIN}`;

    // functions
    ctrl.startInbounding = startInbounding;
    ctrl.openSettings = openSettings;
    ctrl.openPrefix = openPrefix;
    ctrl.openCheatsheet = onOpenCheatsheet;
    ctrl.closeCheatsheet = onCloseCheatsheet;
    ctrl.onKeyPressTrackingId = onScan;
    ctrl.toggleState = toggleState;
    ctrl.onKeyPressManualInput = onKeyPressManualInput;

    init();
    const intervalPromise = $interval(checkConnection, 10000);

    function init() {
      ctrl.loadingSheet = true;
      // add hot keys
      hotkeys.bindTo($scope)
            .add({
              combo: '.+i',
              description: 'Change Device ID',
              callback: changeDeviceId,
            })
            .add({
              combo: '.+w',
              description: 'Weight Input',
              callback: inputWeight,
            })
            .add({
              combo: '.+k',
              description: 'Keep Original Size',
              callback: keepOriginalSize,
            })
            .add({
              combo: '.+s',
              description: 'Small',
              callback: changeToSmall,
            })
            .add({
              combo: '.+m',
              description: 'Medium',
              callback: changeToMedium,
            })
            .add({
              combo: '.+l',
              description: 'Large',
              callback: changeToLarge,
            })
            .add({
              combo: '.+x',
              description: 'X-Large',
              callback: changeToXLarge,
            })
            .add({
              combo: '.+e',
              description: 'Input Length',
              callback: changeLength,
            })
            .add({
              combo: '.+r',
              description: 'Input Width',
              callback: changeWidth,
            })
            .add({
              combo: '.+t',
              description: 'Input Height',
              callback: changeHeight,
            });

      Hub.read().then(onSuccess);


      function onSuccess(result) {
        ctrl.data.hubs = _.sortBy(result, ['name']).map(o => ({
          value: o,
          displayName: o.name,
        }));

        ctrl.hubSearch.setPossibleOptions(_.cloneDeep(ctrl.data.hubs));
        ctrl.loadingSheet = false;
      }
    }

    function onKeyPressManualInput(event) {
      if (event.which === 13) {
        focusTrackingInput();
      }
    }

    function checkConnection() {
      if (navigator.onLine) {
        if (!ctrl.state.internetConnection) {
          setupSocketConnection(ctrl.data.deviceId);
        }
        ctrl.state.internetConnection = true;
        ctrl.state.internetConnectionMessage = 'container.global-inbound.online';
      } else {
        ctrl.state.internetConnection = false;
        if (ctrl.data.deviceId) {
          ctrl.state.deviceStatus = ctrl.DEVICE_STATUS.DISCONNECTED;
        }
        ctrl.state.internetConnectionMessage = 'container.global-inbound.offline';
      }
    }

    function changeDeviceId() {
      openSettings();
    }
    function keepOriginalSize() {
      $timeout(() => {
        ctrl.state.sizingStateAuto = true;
        ctrl.data.size = '-';
        focusTrackingInput();
      });
    }
    function changeToSmall() {
      $timeout(() => {
        ctrl.state.sizingStateAuto = false;
        ctrl.data.size = 'S';
        focusTrackingInput();
      });
    }
    function changeToMedium() {
      $timeout(() => {
        ctrl.state.sizingStateAuto = false;
        ctrl.data.size = 'M';
        focusTrackingInput();
      });
    }
    function changeToLarge() {
      $timeout(() => {
        ctrl.state.sizingStateAuto = false;
        ctrl.data.size = 'L';
        focusTrackingInput();
      });
    }
    function changeToXLarge() {
      $timeout(() => {
        ctrl.state.sizingStateAuto = false;
        ctrl.data.size = 'XL';
        focusTrackingInput();
      });
    }
    function inputWeight() {
      $timeout(() => {
        $('#input-weight').focus();
      });
    }
    function changeHeight() {
      $timeout(() => {
        $('#input-height').focus();
      });
    }
    function changeLength() {
      $timeout(() => {
        $('#input-length').focus();
      });
    }
    function changeWidth() {
      $timeout(() => {
        $('#input-width').focus();
      });
    }

    /**
     * DO the inbound scan call
     * @param {*} event
     * @param {*} data
     */
    function onScan(event, data) {
      let payload = {};
      resetPlayList();
      // if user press ., need to outfocus this input
      if (event.which === 46) {
        // move the focus somewhere to trigger the hotkeys
        const target = event.target;
        target.blur();
      }

      if (!data) {
        return $q.reject();
      }
      if (data === '') {
        return $q.reject();
      }

      // do the inbound
      if (event.which === 13) {
        const prefix = ctrl.data.prefix || '';
        payload = {
          hub_id: ctrl.data.hub.value.id,
          scan: prefix + data.toUpperCase().trim(),
          inbound_type: SORTING_HUB_SCAN_TYPE,
          route_id: null,
        };
        payload.dimensions = {};
        if (ctrl.data.weight) {
          payload.dimensions.weight = ctrl.data.weight;
        }
        if (ctrl.data.height && ctrl.data.length && ctrl.data.width) {
          payload.dimensions = _.assign(payload.dimensions, {
            height: ctrl.data.height,
            length: ctrl.data.length,
            width: ctrl.data.width,
          });
          ctrl.state.dimensionsError = false;
        } else if (ctrl.data.height || ctrl.data.length || ctrl.data.width) {
          ctrl.state.dimensionsError = true;
        }

        if (!ctrl.state.sizingStateAuto) {
          const size = _.find(ctrl.const.sizes, { value: ctrl.data.size });
          if (size) {
            payload.parcel_size = size.longName;
          }
        }
        return Inbound.scanInboundCmi(payload)
          .then(onSuccess, onError);
      }
      return $q.reject();

      function onSuccess(inboundScanData) {
        switch (inboundScanData.status) {
          case SCAN_RESULT.ORDER_NOT_FOUND :
            nvToast.error('Invalid Tracking ID');
            playSingleAudio(nvSoundService.getMistakeSoundFile());
            ctrl.data.lastScanned = _.cloneDeep(ctrl.data.trackingId);
            break;
          case SCAN_RESULT.ORDER_COMPLETED :
            // warn user !
            nvToast.error(inboundScanData.status);
            playSingleAudio(nvSoundService.getMistakeSoundFile());
            ctrl.data.trackingId = null;
            break;
          case SCAN_RESULT.ORDER_CANCELLED :
            // warn user too !
            nvToast.error(inboundScanData.status);
            playSingleAudio(nvSoundService.getMistakeSoundFile());
            ctrl.data.trackingId = null;

            break;
          case SCAN_RESULT.ORDER_RETURNED_TO_SENDER :
            // warn user also
            nvToast.error(inboundScanData.status);
            playSingleAudio(nvSoundService.getMistakeSoundFile());
            ctrl.data.trackingId = null;

            break;
          case SCAN_RESULT.ORDER_TRANSFERRED_TO_3PL :
            // scold user
            nvToast.error(inboundScanData.status);
            playSingleAudio(nvSoundService.getMistakeSoundFile());
            ctrl.data.trackingId = null;
            break;
          case SCAN_RESULT.ON_HOLD :
            ctrl.state.isRecovery = true;
            ctrl.data.rackSector = `RECOVERY ${inboundScanData.comments || ''}`;
            playSingleAudio(nvSoundService.getRecoverySoundFile());
            ctrl.data.trackingId = null;
            break;
          case SCAN_RESULT.PENDING_DELIVERY :
            nvToast.error(nvTranslate.instant('container.global-inbound.tracking-id-is-a-cmi-order', { x: ctrl.data.trackingId }));
            showInboundResult(inboundScanData);
            break;
          case SCAN_RESULT.SUCCESSFUL_INBOUND :
            // proceed inbound status
            showInboundResult(inboundScanData);
            break;
          default:
        }
        ctrl.data.scanCounter += 1;
        $timeout(() => {
        //   // reset scan state
          ctrl.state.ready = true;
          ctrl.data.responsibleHub = null;
          ctrl.data.rackSector = null;
          ctrl.state.weightDiffData = null;
          ctrl.state.weightDiff = false;
          ctrl.state.setAsideExceed = false;
          ctrl.state.errorCodeColor = 'none';
          ctrl.state.dimensionsError = false;
          ctrl.state.isRecovery = false;
          ctrl.state.weightExceedMaxLimit = false;
        }, 1500);
      }

      function showInboundResult(inboundScanData) {
        ctrl.data.rackSector = inboundScanData.sector;
        ctrl.data.responsibleHub = inboundScanData.destination_hub;
        // play hub sound
        resetPlayList();
        if (!inboundScanData.is_international && !inboundScanData.set_aside) {
          addToPlaylist(nvSoundService.getRackSoundFileWord(
            ctrl.data.rackSector.replace(/[`~!@#$%^&*()_|+\-=?;:'",.<>\{\}\[\]\\\/]/gi, '')));
        } else {
        // play the hub name instead
          addToPlaylist(nvSoundService.getRackSoundFileWord(
          ctrl.data.responsibleHub.replace(/[`~!@#$%^&*()_|+\-=?;:'",.<>\{\}\[\]\\\/]/gi, '')));
        }
        playAudio();
        // handling for weight tolerance
        // use the new calculation for threshold
        ctrl.state = _.assign(ctrl.state, Inbound.calculateWeightDiff(inboundScanData));
        // handling for exceeding weight limit
        if (_.get(inboundScanData, 'weight_difference.exceed_inbound_max_weight')) {
          ctrl.state.weightExceedMaxLimit = true;
          playSingleAudio(nvSoundService.getExceedWeightLimitSoundFile());
        }

        // handle international condition
        if (inboundScanData.is_international) {
          ctrl.data.rackSector = 'INTL';
          playSingleAudio(nvSoundService.getInternationalSoundFile());
        }
        if (inboundScanData.set_aside && !inboundScanData.is_international) {
          ctrl.data.rackSector = _.toUpper(nvTranslate.instant('container.global-inbound.set-aside'));
          ctrl.data.setAsideGroup = inboundScanData.set_aside.set_aside_group_name;
          ctrl.data.setAsideCounter = inboundScanData.set_aside.set_aside_counter;
          if (inboundScanData.set_aside.exceeed_set_aside_limit) {
            // exceed limit
            ctrl.state.setAsideExceed = true;
          }
          playSingleAudio(nvSoundService.getSetAsideSoundFile());
        }
        setPriorityLevelInfo(inboundScanData);
        // check for error code color
        ctrl.state.errorCodeColor = inboundScanData.failure_reason_color_code || 'none';

        ctrl.state.ready = false;
        ctrl.data.lastScanned = _.cloneDeep(ctrl.data.trackingId);
        ctrl.data.trackingId = null;
      }

      function onError() {
        playSingleAudio(nvSoundService.getMistakeSoundFile());
      }
    }

    function toggleState(stateKey) {
      const curval = ctrl.state[stateKey];
      if (stateKey !== 'sizingStateAuto' && !ctrl.data.deviceId) {
        return;
      }
      ctrl.state[stateKey] = !curval;
      if (stateKey === 'sizingStateAuto' && ctrl.state[stateKey]) {
        ctrl.data.size = '-';
      }
      return;
    }

    function openSettings($event) {
      return nvDialog.showSingle($event, {
        templateUrl: 'views/container/global-inbound-cmi/dialogs/scan-settings/' +
                'scan-settings.html',
        theme: 'nvBlue',
        cssClass: 'nv-global-inbound-scan-settings',
        controllerAs: 'ctrl',
        controller: 'GlobalInboundChangeSettingsDialogController',
        locals: {
          data: ctrl.data,
          hubs: _.cloneDeep(ctrl.data.hubs),
        },

      }).then((result) => {
        ctrl.data.hub = result.hub;
        ctrl.data.deviceId = result.deviceId;
        setupSocketConnection(result.deviceId);
        focusTrackingInput();
      });
    }
    /**
     *
     * @param {*} parcelData
     *  0 (Non Priority) = no indication
     *  1 (Normal Priority) = Yellow background
     *  2-90 (Late priorities) = Orange Background
     *  91 and above (Urgent priorities) = Red backgroun
     */
    function setPriorityLevelInfo(parcelData) {
      if (parcelData.priority_level != null && parcelData.priority_level >= 0) {
        ctrl.data.priorityLevel = _.toString(parcelData.priority_level);
        if (parcelData.priority_level === 0) {
          ctrl.data.priorityLevelColorCode = '';
        } else if (parcelData.priority_level === 1) {
          ctrl.data.priorityLevelColorCode = 'yellow';
        } else if (parcelData.priority_level > 1 && parcelData.priority_level < 91) {
          ctrl.data.priorityLevelColorCode = 'orange';
        } else {
          ctrl.data.priorityLevelColorCode = 'red';
        }
      }
    }

    function openPrefix($event) {
      return nvDialog.showSingle($event, {
        templateUrl: 'views/container/global-inbound-cmi/dialogs/prefix/' +
                'prefix.html',
        theme: 'nvGreen',
        cssClass: 'nv-global-inbound-prefix',
        controllerAs: 'ctrl',
        controller: 'GlobalInboundPrefixDialogController',
        locals: {
          data: ctrl.data,
        },

      }).then((result) => {
        ctrl.data.prefix = result.prefix;
        if (ctrl.data.prefix) {
          ctrl.state.isPrefixSet = true;
        } else {
          ctrl.state.isPrefixSet = false;
        }
      });
    }

    function startInbounding() {
      ctrl.loadState = 'waiting';
      if (ctrl.data.deviceId) {
        ctrl.state.dimensionStateAuto = true;
        ctrl.state.weightingStateAuto = true;
      } else {
        ctrl.state.deviceStatus = ctrl.DEVICE_STATUS.NO_DEVICE;
      }
      setupSocketConnection(ctrl.data.deviceId);
      return $q.all([
        Order.getSetAside(),
        Order.getSetAsideCurrentCounter(),
        Warehousing.getDimmingOffset(),
      ])
        .then(onSuccessLoad);

      function onSuccessLoad(results) {
        ctrl.data.setAsides = results[0];
        ctrl.data.setAsideCounter = results[1];
        ctrl.data.dimmingOffset = results[2].dimming_offset;
        ctrl.loadState = 'idle';
        ctrl.showPage = 'scanning';
        focusTrackingInput();
      }
    }
    function focusTrackingInput() {
      $timeout(() => { $('#scan').focus(); });
    }

    // sockets

    function setupSocketConnection(deviceId) {
      let socketUrl = SOCKET_URL;

       // remove socket if any
      if (ctrl.socket) {
        ctrl.socket.off('event');
        ctrl.socket.disconnect();
        ctrl.socket = null;
      }

      if (!deviceId) {
        // reset the value to null
        ctrl.data.width = null;
        ctrl.data.height = null;
        ctrl.data.length = null;
        ctrl.data.weight = null;
        ctrl.state.weightingStateAuto = false;
        ctrl.state.dimensionStateAuto = false;
        ctrl.state.deviceStatus = ctrl.DEVICE_STATUS.NO_DEVICE;
        return;
      }

      // set state to auto
      ctrl.state.weightingStateAuto = true;
      ctrl.state.dimensionStateAuto = true;

      if (deviceId.startsWith('dim_')) {
        // using new dimming module
        ctrl.state.isNewDimming = true;
        socketUrl += '/dim';
      } else {
        // using raspi
        ctrl.state.isNewDimming = false;
        ctrl.state.dimensionStateAuto = false;
      }
      ctrl.socket = io(socketUrl);
      ctrl.socket.on('connect', onDimConnect);
      ctrl.socket.on('reconnect', onDimConnect);
      ctrl.socket.on('disconnect', onDimDisconnect);
      if (ctrl.state.isNewDimming) {
        ctrl.socket.on('event', onDimEvent);
      } else {
        ctrl.socket.on(deviceId, onDimEvent);
      }
    }

    function onDimConnect() {
      if (ctrl.state.isNewDimming) {
        ctrl.socket.emit('join', ctrl.data.deviceId, () => {
          $timeout(() => {
            ctrl.state.deviceStatus = ctrl.DEVICE_STATUS.DEVICE_CONNECTED;
          });
        });
      }
      ctrl.state.deviceStatus = ctrl.DEVICE_STATUS.DEVICE_CONNECTED;
    }

    function onDimDisconnect() {
      $timeout(() => {
        ctrl.state.deviceStatus = ctrl.DEVICE_STATUS.DISCONNECTED;
      });
    }

    function onDimEvent(data) {
      if (!ctrl.state.dimensionStateAuto && !ctrl.state.weightingStateAuto) {
        return;
      }

      $timeout(() => {
        // new dimming will update dimension too

        // update the weight value
        const json = JSON.parse(data);
        if (ctrl.state.weightingStateAuto) {
          ctrl.data.weight = parseFloat(json.weight);
        }
        if (ctrl.state.isNewDimming) {
          // update the l x w x h values for new dimming
          if (ctrl.state.dimensionStateAuto) {
            const ln = +Math.max(parseFloat(json.length) - ctrl.data.dimmingOffset, 0).toFixed(2);
            const wd = +Math.max(parseFloat(json.width) - ctrl.data.dimmingOffset, 0).toFixed(2);
            const ht = +Math.max(parseFloat(json.height) - ctrl.data.dimmingOffset, 0).toFixed(2);
            if (increasedFrZero(ln, wd, ht)) {
              playSingleAudio(nvSoundService.getByName('tone'));
            } else if (decreasedToZero(ln, wd, ht)) {
              playSingleAudio(nvSoundService.getByName('fryingpan'));
            }
            ctrl.data.length = ln;
            ctrl.data.width = wd;
            ctrl.data.height = ht;
          }
        }


      // update the time
        $scope.lastUpdated = moment(json.timestamp)
          .tz('utc')
          .format('[Last Updated:] (ddd Do MMM) h:mm:ssa');
      });

      function increasedFrZero(ln, wd, ht) {
        return ctrl.data.length === 0 && ctrl.data.width === 0
          && ctrl.data.height === 0 && ln > 0 && wd > 0 && ht > 0;
      }

      function decreasedToZero(ln, wd, ht) {
        return ctrl.data.length > 0 && ctrl.data.width > 0 && ctrl.data.height > 0
          && ln === 0 && wd === 0 && ht === 0;
      }
    }

    function onCloseCheatsheet() {
      ctrl.showCheatsheet = false;
    }

    function onOpenCheatsheet() {
      ctrl.showCheatsheet = true;
    }

    // audio function
    function playAudio() {
      $timeout(() => { $scope.mediaPlayer.play(0); });
    }

    function playSingleAudio(src) {
      $scope.audioPlaylist.push({
        src: src,
        type: 'audio/mp3',
      });
      $timeout(() => { $scope.mediaPlayer.play(0); });
    }

    function addToPlaylist(src) {
      $scope.audioPlaylist = _.concat($scope.audioPlaylist, _.map(_.castArray(src), s => ({
        src: s,
        type: 'audio/mp3',
      })));
    }

    function resetPlayList() {
      $scope.audioPlaylist = [];
    }

    // watchers

    $scope.$on('$destroy', () => {
      if (ctrl.socket) {
        ctrl.socket.disconnect();
      }

      if (intervalPromise) {
        $interval.cancel(intervalPromise);
      }
    });
  }
}());
