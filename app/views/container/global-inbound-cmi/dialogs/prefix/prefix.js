(function controller() {
  angular
        .module('nvOperatorApp.controllers')
        .controller('GlobalInboundPrefixDialogController', GlobalInboundPrefixDialogController);

  GlobalInboundPrefixDialogController.$inject = ['$mdDialog', 'data'];
  function GlobalInboundPrefixDialogController($mdDialog, data) {
    const ctrl = this;
    ctrl.onCancel = onCancel;
    ctrl.onSave = onSave;
    ctrl.data = _.cloneDeep(data);

    function onCancel($event) {
      return $mdDialog.cancel();
    }

    function onSave($event) {
      return $mdDialog.hide(ctrl.data);

    }

  }
}());
