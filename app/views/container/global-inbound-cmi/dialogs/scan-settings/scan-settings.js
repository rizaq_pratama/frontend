(function controller() {
  angular
        .module('nvOperatorApp.controllers')
        .controller('GlobalInboundChangeSettingsDialogController', GlobalInboundChangeSettingsDialogController);

  GlobalInboundChangeSettingsDialogController.$inject = ['$mdDialog', 'hubs', 'data', 'nvAutocomplete.Data',];
  function GlobalInboundChangeSettingsDialogController($mdDialog, hubs, data, nvAutocompleteData) {
    const ctrl = this;
    ctrl.onCancel = onCancel;
    ctrl.onSave = onSave;
    ctrl.hubs = _.cloneDeep(hubs);
    ctrl.data = _.cloneDeep(data);
    ctrl.hubSearch = nvAutocompleteData.getByHandle('hubs-auto');

    init();

    function init() {
      ctrl.hubSearch.setPossibleOptions(ctrl.hubs);
    }

    function onCancel() {
      return $mdDialog.cancel();
    }

    function onSave() {
      return $mdDialog.hide(ctrl.data);

    }

  }
}());
