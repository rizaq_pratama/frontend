(function controller() {
  angular
        .module('nvOperatorApp.controllers')
        .controller('LatLngCleanupController', LatLngCleanupController);

  LatLngCleanupController.$inject = [
    'nvDialog',
  ];
  function LatLngCleanupController(
    nvDialog
  ) {
    const ctrl = this;

    ctrl.editWaypointDetails = editWaypointDetails;

    // //////////////////////

    function editWaypointDetails($event) {
      nvDialog.showSingle($event, {
        templateUrl: 'views/container/lat-lng-cleanup/dialog/waypoint-edit/waypoint-edit.dialog.html',
        cssClass: 'waypoint-edit',
        controller: 'WaypointEditDetailsDialogController',
        controllerAs: 'ctrl',
      });
    }
  }
}());
