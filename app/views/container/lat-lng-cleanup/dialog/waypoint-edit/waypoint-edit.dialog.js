(function controller() {
  angular
    .module('nvOperatorApp.controllers')
    .controller('WaypointEditDetailsDialogController', WaypointEditDetailsDialogController);

  WaypointEditDetailsDialogController.$inject = [
    '$mdDialog', 'Waypoint', 'nvToast', 'nvTranslate',
  ];

  function WaypointEditDetailsDialogController(
    $mdDialog, Waypoint, nvToast, nvTranslate
  ) {
    // variables
    const ctrl = this;
    ctrl.searchWaypoint = {
      form: {},
      value: null,
      state: 'idle',
      isAbleToSearchWaypoint: isAbleToSearchWaypoint,
      onSearch: onSearchWaypoint,
      errors: [{
        key: 'invalidWaypointId',
        msg: 'container.lat-lng-cleanup.error.waypoint-id-cannot-be-found',
      }],
    };
    ctrl.editWaypoint = {
      form: {},
      data: null,
      state: 'idle',
      isAbleToEditWaypoint: isAbleToEditWaypoint,
      onUpdate: onUpdate,
      isUpdated: false,
    };

    // functions
    ctrl.onCancel = onCancel;

    // functions details
    function onCancel() {
      $mdDialog.cancel();
    }

    function isAbleToSearchWaypoint() {
      return ctrl.searchWaypoint.value && ctrl.searchWaypoint.value > 0;
    }

    function onSearchWaypoint() {
      ctrl.searchWaypoint.state = 'waiting';
      Waypoint.getByIds([ctrl.searchWaypoint.value]).then(success, failure);

      function success(response) {
        ctrl.editWaypoint.data = response[0] || null;
        ctrl.searchWaypoint.state = 'idle';
        ctrl.editWaypoint.isUpdated = false;

        if (_.size(ctrl.editWaypoint.data) > 0) {
          ctrl.searchWaypoint.value = null;

          ctrl.searchWaypoint.form.searchWaypointId.$setValidity('invalidWaypointId', true);
        } else {
          // no result found
          ctrl.searchWaypoint.form.searchWaypointId.$setValidity('invalidWaypointId', false);
        }
      }

      function failure() {
        ctrl.editWaypoint.data = null;
        ctrl.searchWaypoint.state = 'idle';
        ctrl.editWaypoint.isUpdated = false;

        ctrl.searchWaypoint.form.searchWaypointId.$setValidity('invalidWaypointId', false);
      }
    }

    function isAbleToEditWaypoint() {
      return ctrl.editWaypoint.data && _.size(ctrl.editWaypoint.form) > 0 &&
        ctrl.editWaypoint.form.$valid;
    }

    function onUpdate() {
      ctrl.editWaypoint.state = 'waiting';
      const singlePayload = _.defaultsDeep(ctrl.editWaypoint.data, {
        rawAddressFlag: false,
      });

      Waypoint.update([singlePayload]).then(success, failure);

      function success() {
        nvToast.success(nvTranslate.instant('container.lat-lng-cleanup.waypoint-details-updated'));

        ctrl.editWaypoint.state = 'idle';
        ctrl.editWaypoint.isUpdated = true;
      }

      function failure() {
        ctrl.editWaypoint.state = 'idle';
      }
    }
  }
}());
