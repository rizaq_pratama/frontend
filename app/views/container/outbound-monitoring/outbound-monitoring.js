(function controller() {
  angular
    .module('nvOperatorApp.controllers')
    .controller('OutboundMonitoringController', OutboundMonitoringController);

  OutboundMonitoringController.$inject = ['nvBackendFilterUtils', 'MonitoringFilter', 'Monitoring',
    'nvTable', '$window', 'nvURL', 'nvDomain', 'nvDialog', 'Hub', '$state'];

  const API_STATE = {
    IDLE: 'idle',
    WAITING: 'waiting',
  };

  const PAGE = {
    FILTER: 0,
    DATA: 1,
  };

  function OutboundMonitoringController(nvBackendFilterUtils, MonitoringFilter, Monitoring,
    nvTable, $window, nvURL, nvDomain, nvDialog, Hub, $state) {
    const ctrl = this;

    ctrl.state = {};
    ctrl.data = {};
    ctrl.function = {};

    ctrl.data.tableParam = null;

    ctrl.function.init = init;
    ctrl.function.loadFromFilter = loadFromFilter;
    ctrl.function.showEditFilter = showEditFilter;
    ctrl.function.flagRoute = flagRoute;
    ctrl.function.commentRoute = commentRoute;
    ctrl.function.editRoute = editRoute;
    ctrl.function.refreshTable = refreshTable;

    function init() {
      ctrl.state.page = PAGE.FILTER;
      ctrl.state.loadState = API_STATE.IDLE;
      ctrl.state.refreshTable = API_STATE.IDLE;

      // filter
      const filters = MonitoringFilter.init();
      ctrl.data.possibleFilters = filters;
      ctrl.data.selectedFilters = _.remove(ctrl.data.possibleFilters,
          filter => filter.showOnInit === true);
      
      ctrl.data.routeSummary = {
        complete: { displayName: 'Complete', value: 0 },
        notComplete: { displayName: 'In Progress', value: 0 },
        flagged: { displayName: 'Marked', value: 0 },
      };

      ctrl.data.driverSummary = {
        atHub: { displayName: 'Not Here/At Hub', value: 0 },
        onGoing: { displayName: 'On Going', value: 0 },
        returned: { displayName: 'Returned', value: 0 },
      };

      return true;
    }

    function loadFromFilter() {
      ctrl.state.loadState = API_STATE.WAITING;
      initTable();
      const param = nvBackendFilterUtils.constructParams(ctrl.data.selectedFilters);
      _.forEach(ctrl.data.selectedFilters, (filter) => {
        filter.description = nvBackendFilterUtils.getDescription(filter);
      });

      Monitoring.read(param).then((data) => {
        const processedData = extendDatas(data);
        calculateRouteSummary(processedData);
        calculateDriverSummary(processedData);
        ctrl.data.tableParam.setData(processedData);

        ctrl.state.loadState = API_STATE.IDLE;
        ctrl.state.page = PAGE.DATA;
      }, () => {
        ctrl.state.loadState = API_STATE.IDLE;
      });
    }

    function initTable() {
      ctrl.data.tableParam = nvTable.createTable({
        routeId: { displayName: 'container.outbound-monitoring.table.col-route-id' },
        totalParcel: { displayName: 'container.outbound-monitoring.table.col-total-parcel' },
        onVehicle: { displayName: 'container.outbound-monitoring.table.col-on-vehicle' },
        scanned: { displayName: 'container.outbound-monitoring.table.col-scanned' },
        unscanned: { displayName: 'container.outbound-monitoring.table.col-unscanned' },
        unscannedExternal: { displayName: 'container.outbound-monitoring.table.col-unscanned-external' },
        unscannedInternal: { displayName: 'container.outbound-monitoring.table.col-unscanned-internal' },
        outboundStatus: { displayName: 'container.outbound-monitoring.table.col-outbound-status' },
        driverStatus: { displayName: 'container.outbound-monitoring.table.col-driver-status' },
        driver: { displayName: 'container.outbound-monitoring.table.col-driver' },
        zones: { displayName: 'container.outbound-monitoring.table.col-zones' },
        comments: { displayName: 'container.outbound-monitoring.table.col-comments' },
      }, { isStripped: false });

      ctrl.data.tableParam.updateColumnFilter('zones', strFn =>
        (item) => {
          const str = split(strFn(), ',');
          return str.length === 0 ||
            (str.length === 1 && (str[0] === '')) ||
            (str.length > 0 && filter(str, split(item.zones, ',')));
        }
      );

      function split(str, separator) {
        const arr = _.split(str.toLowerCase(), separator);
        return _.map(arr, el => el.trim());
      }

      function filter(source, dest) {
        const result = _.filter(source, s => _.find(dest, d => d === s));
        return result.length > 0;
      }
    }

    function flagRoute($event, routeId) {
      Monitoring.putFlag(routeId).then((data) => {
        ctrl.data.tableParam.mergeIn(extendDatas(data), 'routeId');
        ctrl.data.tableParam.refreshData();
        const datas = ctrl.data.tableParam.getTableData();
        calculateRouteSummary(datas);
        calculateDriverSummary(datas);
      });
    }

    function commentRoute($event, data) {
      nvDialog.showSingle($event, {
        theme: 'nvGreen',
        templateUrl: 'views/container/outbound-monitoring/dialog/comments.dialog.html',
        cssClass: 'outbound-monitoring-comments-dialog',
        controller: 'OutboundMonitoringCommentsController',
        controllerAs: 'ctrl',
        locals: {
          localData: { id: data.routeId, comments: data.comments },
        },
      }).then((result) => {
        ctrl.data.tableParam.mergeIn(extendDatas(result), 'routeId');
      });
    }

    function editRoute($event, routeId) {
      $window.open(
        $state.href('container.outbound-breakroute', { routeId: routeId })
      );
    }

    function showEditFilter() {
      ctrl.data.tableParam = null;

      ctrl.state.page = PAGE.FILTER;
    }

    function extendDatas(datas) {
      return _.map(_.castArray(datas), (data) => {
        // possible values: marked, completed, incomplete
        let outboundStatus = '';
        let cssClass = '';
        if (data.is_marked === true) {
          cssClass = 'row-yellow';
          outboundStatus = 'Marked';
        } else if (data.outbound_status.toLowerCase() === 'complete') {
          cssClass = 'row-green';
          outboundStatus = 'Complete';
        } else {
          // incomplete
          cssClass = 'row-red';
          outboundStatus = 'In Progress';
        }

        const zones = _(data.rack_count).map(d => d.rack).join(', ') || '-';
        return {
          routeId: data.route_id,
          totalParcel: data.parcels_in_route || '0',
          onVehicle: data.parcels_on_vehicle || '0',
          scanned: data.parcels_scanned || '0',
          unscanned: data.parcels_not_scanned || '0',
          unscannedExternal: data.parcels_not_scanned_not_in_hub || '0',
          unscannedInternal: data.parcels_not_scanned_in_hub || '0',
          outboundStatus: outboundStatus,
          driverStatus: data.driver_status,
          driver: data.driver_name,
          zones: zones,
          comments: data.comments,
          flagged: data.is_marked,
          cssClass: cssClass,
        };
      });
    }

    function calculateRouteSummary(datas = []) {
      const completeRoutes = _.filter(datas, data =>
        data.outboundStatus.toLowerCase() === 'complete' && data.flagged === false);

      const markedRoutes = _.filter(datas, data =>
        data.flagged === true);

      const incompleteRoutes = _.filter(datas, data =>
        data.outboundStatus.toLowerCase() === 'in progress' && data.flagged === false);

      ctrl.data.routeSummary.complete.value = completeRoutes.length;
      ctrl.data.routeSummary.notComplete.value = incompleteRoutes.length;
      ctrl.data.routeSummary.flagged.value = markedRoutes.length;
    }

    function calculateDriverSummary(datas = []) {
      const atHub = _.filter(datas, data => data.driverStatus.toLowerCase() === 'not here / at hub');
      const onGoing = _.filter(datas, data => data.driverStatus.toLowerCase() === 'on going');
      const returned = _.filter(datas, data => data.driverStatus.toLowerCase() === 'returned');

      ctrl.data.driverSummary.atHub.value = atHub ? atHub.length : 0;
      ctrl.data.driverSummary.onGoing.value = onGoing ? onGoing.length : 0;
      ctrl.data.driverSummary.returned.value = returned ? returned.length : 0;
    }

    function refreshTable() {
      ctrl.state.refreshTable = API_STATE.WAITING;
      const param = nvBackendFilterUtils.constructParams(ctrl.data.selectedFilters);
      Monitoring.read(param).then((data) => {
        const processedData = extendDatas(data);
        calculateRouteSummary(processedData);
        calculateDriverSummary(processedData);
        
        initTable();
        ctrl.data.tableParam.setData(processedData);
        ctrl.state.refreshTable = API_STATE.IDLE;
      }, () => {
        ctrl.state.refreshTable = API_STATE.IDLE;
      });
    }
  }
}());
