(function controller() {
  angular
    .module('nvOperatorApp.controllers')
    .controller('OutboundMonitoringCommentsController', OutboundMonitoringCommentsController);

  OutboundMonitoringCommentsController.$inject = ['$mdDialog', 'localData', 'Monitoring'];

  function OutboundMonitoringCommentsController($mdDialog, localData, Monitoring) {
    const ctrl = this;

    ctrl.comments = localData.comments || '';
    ctrl.onCancel = onCancel;
    ctrl.onSubmit = onSubmit;
    ctrl.submitState = 'idle';

    function onCancel() {
      $mdDialog.cancel(0);
    }

    function onSubmit() {
      ctrl.submitState = 'loading';
      Monitoring.comment(localData.id, ctrl.comments).then((data) => {
        ctrl.submitState = 'idle';
        $mdDialog.hide(data);
      }, () => {
        ctrl.submitState = 'idle';
      });
    }
  }
}());
