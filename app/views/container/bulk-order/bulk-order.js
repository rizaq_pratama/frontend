(function controller() {
  angular
    .module('nvOperatorApp.controllers')
    .controller('BulkOrderController', BulkOrderController);

  BulkOrderController.$inject = ['Order', 'nvTable', 'nvDateTimeUtils', 'nvTimezone',
    'nvDialog', 'Printer', 'nvToast', 'nvTranslate', '$translate', '$q', 'nvFileUtils', '$stateParams'];

  const TABLE_FIELD = {
    id: { displayName: 'container.bulk-order.table-col-id' },
    shipperId: { displayName: 'container.bulk-order.table-col-shipper-id' },
    qty: { displayName: 'container.bulk-order.table-col-total-orders' },
    autoRescheduleDays: { displayName: 'container.bulk-order.table-col-auto-reschedule-days' },
    pickupDate: { displayName: 'container.bulk-order.table-col-pickup-date' },
    pickupTimewindowId: { displayName: 'container.bulk-order.table-col-pickup-timewindow-id' },
    deliveryTimewindowId: { displayName: 'container.bulk-order.table-col-delivery-timewindow-id' },
    hasDaynight: { displayName: 'container.bulk-order.table-col-has-daynight' },
    processStatus: { displayName: 'container.bulk-order.table-col-process-status' },
    rollbackTime: { displayName: 'container.bulk-order.table-col-rollback-time' },
    createdAt: { displayName: 'container.bulk-order.table-col-created-at' },
  };

  function BulkOrderController(Order, nvTable, nvDateTimeUtils, nvTimezone,
    nvDialog, Printer, nvToast, nvTranslate, $translate, $q, nvFileUtils, $stateParams) {
    const ctrl = this;

    ctrl.search = search;
    ctrl.isSearchEnabled = isSearchEnabled;
    ctrl.rollback = rollback;
    ctrl.print = print;
    ctrl.downloadCsv = downloadCsv;
    ctrl.seeDetails = seeDetails;
    ctrl.onKeypress = onKeypress;

    init();

    function init() {
      ctrl.resultState = 'init';
      ctrl.bulkId = $stateParams.id;
      ctrl.bulkIdCopy = '';
      ctrl.searchState = 'idle';
      ctrl.printState = 'idle';
      ctrl.tableParam = nvTable.createTable(TABLE_FIELD);
      if (ctrl.bulkId) {
        search(ctrl.bulkId);
      }
    }

    function search(bulkId) {
      ctrl.resultState = 'loading';
      ctrl.bulkIdCopy = bulkId;
      Order.getBulkOrderDetails(bulkId).then((result) => {
        ctrl.tableParam = nvTable.createTable(TABLE_FIELD);
        ctrl.tableParam.setData(extendData(result));
        ctrl.resultState = 'content';
      }, () => {
        ctrl.resultState = 'error';
      });
    }

    function isSearchEnabled() {
      if (!ctrl.bulkId) {
        return false;
      }
      return ctrl.bulkId.length !== 0;
    }

    function extendData(datas) {
      return _.map(_.castArray(datas), (data) => {
        const orders = data.orders;
        const pickupTimewindowId = data.pickup_timewindow_id != null ? data.pickup_timewindow_id.toString() : '-';
        const deliveryTimewindowId = data.delivery_timewindow_id != null ? data.delivery_timewindow_id.toString() : '-';
        const createdAt = nvDateTimeUtils.toDate(data.created_at);
        const rollbackTime = nvDateTimeUtils.toDate(data.rollback_time);
        return _.assign(data, {
          created_at: nvDateTimeUtils.displayDateTime(createdAt, nvTimezone.getOperatorTimezone()),
          rollback_time: nvDateTimeUtils.displayDateTime(rollbackTime, nvTimezone.getOperatorTimezone()),
          has_daynight: data.has_daynight ? 'Yes' : 'No',
          pickup_timewindow_id: pickupTimewindowId,
          delivery_timewindow_id: deliveryTimewindowId,
          orders: orders,
          isDownloadingCsv: false,
        });
      });
    }

    function rollback($event, bulkId) {
      nvDialog.showSingle($event, {
        templateUrl: 'views/container/bulk-order/dialog/rollback/rollback.dialog.html',
        theme: 'nvRed',
        cssClass: 'nv-container-bulk-order-rollback',
        controller: 'BulkOrderRollbackController',
        controllerAs: 'ctrl',
        locals: {
          localData: { id: bulkId },
        },
      }).then(onRollback);

      function onRollback(arg) {
        if (arg === 0) {
          init();
        }
      }
    }

    function print($event, orders) {
      nvDialog.confirmDelete($event, {
        title: $translate.instant('container.bulk-order.dialog-title'),
        content: $translate.instant('container.bulk-order.dialog-body'),
        ok: $translate.instant('commons.print'),
        cancel: $translate.instant('commons.cancel'),
      }).then(onPrint);

      function onPrint() {
        ctrl.printState = 'waiting';
        const chunkedOrdersCollection = _.chunk(orders, 100);
        const results = [];

        _.forEach(chunkedOrdersCollection, (smallOrders) => {
          results.push(Printer.printByPost(convertToPrintModel(smallOrders)));
        });
        $q.all(results).then(() => {
          nvToast.success(nvTranslate.instant('container.order.edit.shipping-label-printed'));
        }).finally(() => (ctrl.printState = 'idle'));
      }

      function convertToPrintModel(datas) {
        return _.map(datas, data => ({
          recipient_name: data.to_name,
          address1: data.to_address1,
          address2: data.to_address2,
          contact: data.to_contact,
          country: data.to_country,
          postcode: data.to_postcode,
          shipper_name: data.from_name,
          tracking_id: data.tracking_id,
          barcode: data.tracking_id,
          route_id: '',
          driver_name: '',
          template: 'hub_shipping_label_70X50',
        }));
      }
    }

    function downloadCsv(bulk) {
      bulk.isDownloadingCsv = true;
      Order.getBulkOrderCsvData(bulk.id).then(success).finally(finallyFn);

      function success(response) {
        if (!response.data) {
          nvToast.error(nvTranslate.instant('container.bulk-order.no-csv-data', {
            id: bulk.id,
          }));
        } else {
          nvFileUtils.downloadCsv(response.data, `bulk_${bulk.id}.csv`);
          nvToast.success(nvTranslate.instant('container.bulk-order.csv-downloaded', {
            id: bulk.id,
          }));
        }
      }

      function finallyFn() {
        bulk.isDownloadingCsv = false;
      }
    }

    function seeDetails($event, data) {
      const orders = data.orders;
      if (!orders || orders.length < 1) {
        return;
      }

      nvDialog.showSingle($event, {
        templateUrl: 'views/container/bulk-order/dialog/order-list/order-list.dialog.html',
        theme: 'nvBlue',
        cssClass: 'nv-container-bulk-order-order-list',
        controller: 'BulkOrderListOrderController',
        controllerAs: 'ctrl',
        locals: {
          localData: { orders: orders, bulkId: data.id },
        },
      });
    }

    function onKeypress($event) {
      // catch enter key
      if ($event.which === 13) {
        if (ctrl.bulkId && (ctrl.bulkId > 0)) {
          search(ctrl.bulkId);
        }
      }
    }
  }
}());
