(function controller() {
  angular
    .module('nvOperatorApp.controllers')
    .controller('BulkOrderListOrderController', BulkOrderListOrderController);

  BulkOrderListOrderController.$inject = ['localData', '$mdDialog', 'nvDateTimeUtils', 'nvTimezone'];

  function BulkOrderListOrderController(localData, $mdDialog, nvDateTimeUtils, nvTimezone) {
    const ctrl = this;
    const orders = localData.orders;

    ctrl.bulkId = localData.bulkId;
    ctrl.orders = [];
    ctrl.onCancel = onCancel;

    init();

    function init() {
      let idx = 0;
      ctrl.orders = _.map(orders, (order) => {
        idx += 1;
        const createdAt = nvDateTimeUtils.displayDateTime(
          nvDateTimeUtils.toMoment(order.created_at),
          nvTimezone.getOperatorTimezone()
        );
        return _.assign({}, {
          id: order.id,
          no: idx,
          trackingId: order.tracking_id,
          type: order.type,
          fromName: order.from_name,
          from: _.join(_.compact([order.from_address1, order.from_address2]), ' '),
          fromPostcode: order.from_postcode,
          toName: order.to_name,
          to: _.join(_.compact([order.to_address1, order.to_address2]), ' '),
          toPostcode: order.to_postcode,
          status: order.status,
          granularStatus: order.granular_status,
          createdAt: createdAt,
        });
      });
    }

    function onCancel() {
      $mdDialog.hide(0);
    }
  }
}());
