(function controller() {
  angular
    .module('nvOperatorApp.controllers')
    .controller('BulkOrderRollbackController', BulkOrderRollbackController);

  BulkOrderRollbackController.$inject = ['Order', 'localData', '$mdDialog', 'nvToast', 
    'nvTranslate'];

  function BulkOrderRollbackController(Order, localData, $mdDialog, nvToast,
    nvTranslate) {
    const ctrl = this;
    const bulkId = localData.id;

    const PASSWORD = '1234567890';

    ctrl.submitState = 'idle';

    ctrl.formPassword = '';

    ctrl.onCancel = onCancel;
    ctrl.onSubmit = onSubmit;

    function onCancel() {
      $mdDialog.hide(-1);
    }

    function onSubmit() {
      if (isValidPassword()) {
        rollback();
      } else {
        nvToast.error(nvTranslate.instant('container.order.edit.invalid-password'));
      }

      function isValidPassword() {
        return ctrl.formPassword === PASSWORD;
      }

      function rollback() {
        ctrl.submitState = 'waiting';
        Order.deleteBulkOrders(bulkId).then(() => {
          ctrl.submitState = 'idle';
          $mdDialog.hide(0);
        }, () => {
          ctrl.submitState = 'idle';
        });
      }
    }
  }
}());
