(function module() {
  angular
    .module('nvOperatorApp', [

      /* angular-core */
      'ngCookies',
      'ngMessages',
      'ngSanitize',

      /* angular-plugins */
      'ui.router',
      'ngMaterial',
      'pascalprecht.translate',
      'dndLists',
      'ngTable',
      'ngCsv',
      'ngclipboard',
      'duScroll',
      'ui.ace',
      'mediaPlayer',
      'cfp.hotkeys',
      'ngPromiseExtras',

      /* nv-commons */
      'nv',
      'nvCommons.configs',
      'nvCommons.runs',
      'nvCommons.models',
      'nvCommons.filters',
      'nvCommons.services',
      'nvCommons.directives',

      /* nv-operator */
      'nvOperatorApp.configs',
      'nvOperatorApp.runs',
      'nvOperatorApp.templates',
      'nvOperatorApp.services',
      'nvOperatorApp.directives',
      'nvOperatorApp.controllers',

    ]);
}());
