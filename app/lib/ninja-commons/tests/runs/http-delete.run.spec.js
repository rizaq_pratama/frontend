(function test() {
  describe('http-delete.run', () => {
    const module = angular.mock.module;
    const inject = angular.mock.inject;

    describe('http.delete2', () => {
      beforeEach(() => {
        module('nvCommons.runs');
      });

      let $httpBackend;
      let requestHandler;
      let $rootScope;

      beforeEach(inject(($injector, _$rootScope_) => {
        $httpBackend = $injector.get('$httpBackend');
        requestHandler = $httpBackend.whenDELETE('test').respond({success: true});
        $rootScope = _$rootScope_;
      }));

      afterEach(() => {
        $httpBackend.verifyNoOutstandingExpectation();
        $httpBackend.verifyNoOutstandingRequest();
      });

      it('should make DELETE call with json data', inject(($http) => {
        $httpBackend.expect(
          'DELETE', 
          'test', 
          { data: 1 }, 
          { "Content-Type":"application/json; charset=UTF-8",
            "Accept":"application/json, text/plain, */*" });
        let result;
        $http.delete2('test', {data: 1}).then((value) => {
          result = value.data;
        });
        $httpBackend.flush();
        $rootScope.$apply();
        expect(result).toEqual({success: true});
      }));

      it('should reject promise if it fails', inject(($http) => {
        requestHandler.respond(401, '');
        let failed = false;
        $http.delete2('test', {data: 1}).catch(() => {
          failed = true;
        });
        $httpBackend.flush();
        $rootScope.$apply();
        expect(failed).toBe(true);
      }));
    });
  });
}());
