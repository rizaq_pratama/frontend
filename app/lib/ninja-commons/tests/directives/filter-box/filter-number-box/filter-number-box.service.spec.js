(function test() {
  describe('nvFilterNumberBoxService.service', () => {
    const module = angular.mock.module;
    const inject = angular.mock.inject;

    beforeEach(module('nvCommons.directives'));

    it('invalid preset', inject((nvFilterNumberBoxService) => {
      expect(nvFilterNumberBoxService.isPresetValid({})).toBeFalsy();
      expect(nvFilterNumberBoxService.isPresetValid({
        type: 'invalid',
      })).toBeFalsy();
    }));

    it('range preset', inject((nvFilterNumberBoxService) => {
      expect(nvFilterNumberBoxService.isPresetValid({
        type: 'range',
        from: 1,
        to: 2,
      })).toBeTruthy();
    }));

    it('value preset', inject((nvFilterNumberBoxService) => {
      expect(nvFilterNumberBoxService.isPresetValid({
        type: 'value',
        operator: '==',
        operand: 1,
      })).toBeTruthy();

      expect(nvFilterNumberBoxService.isPresetValid({
        type: 'value',
        operator: '!=',
        operand: 1,
      })).toBeFalsy();
    }));
  });
}());
