(function() {
    'use strict';

    describe('icon-button.directive', function() {

        var module = angular.mock.module;
        var inject = angular.mock.inject;

        beforeEach(module('ngMaterial'));
        beforeEach(module('nvCommons.services'));
        beforeEach(module('nvCommons.directives'));
        beforeEach(module('nvCommons.templates'));

        var translateMode = 1;
        beforeEach(module('pascalprecht.translate', function($filterProvider){
            // mock translate filter
            $filterProvider.register('translate', function(){
                function translateFilter(text) {
                    if (translateMode === 1) {
                        return (text) ? '-' + text + '-' : 'ninjavan';
                    } else {
                        return (text) ? '=' + text + '=' : '=ninjavan=';
                    }
                }
                // angular-translate filter is stateful
                translateFilter.$stateful = true;
                return translateFilter;
            });
        }));

        var $compile, $scope, $rootScope, element, $document;

        function getTemplate(classList, theme) {
            return '<nv-icon-button '  +
                    'class="' + (classList || []).join(' ') + 
                    '" ' +
                    'form="data.form" ' + 
                    'disabled="data.disabled" ' +
                    'on-click="data.onClick($event)" ' +
                    'icon="{{data.icon}}" ' +
                    'md-theme="' + (theme || 'nvBlue') + '">' + 
                    '</nv-icon-button>';            
        }

        function createDirective(data, classList, theme) {
            var elem;
            
            // Setup scope state
            $scope.data = data || {};

            // Create directive
            var precompiled = angular.element(getTemplate(classList, theme));
            elem = $compile(precompiled)($scope);

            // put it into document
            angular.element(document).find('body').append(elem);

            $rootScope.$apply();
            return elem;
        }

        beforeEach(inject(function(_$compile_, _$rootScope_, _$document_){
            $compile = _$compile_;
            $scope = _$rootScope_.$new();
            $rootScope = _$rootScope_;
            $document = _$document_;
        }));

        afterEach(function(){
            if(element) element.remove();
        });

        describe('ui', function(){
             it('should transfer class to .nv-button', inject(function() {
                element = $(createDirective({}, ['raised', 'flat', 'ghost', 'alternate', 'p', 'h4', 'h5', 'dense']));
                expect(element).toContainElement('button.nv-button');

                var button = element.find('button.nv-button');
                expect(button).toHaveClass('raised');
                expect(button).toHaveClass('flat');
                expect(button).toHaveClass('ghost');
                expect(button).toHaveClass('alternate');
                expect(button).toHaveClass('p');
                expect(button).toHaveClass('h4');
                expect(button).toHaveClass('h5');
                expect(button).toHaveClass('dense');
            }));

            describe('button size', function(){
                it('should have default size', function() {
                    var themes = ['nvGreen', 'nvRed', 'nvBlue', 'nvYellow'];
                    var types = [['raised'], ['flat'], ['ghost'], ['flat', 'alternate']];
                    _.each(themes, function(theme){
                        _.each(types, function(type){
                            element = $(createDirective({}, type, theme));
                            var button = element.find('button.nv-button');
                            expect(button).toHaveCss({
                                fontSize: '14px',
                                height: '35px',
                                width: '35px',
                                borderRadius: '4px',
                            });
                            element.remove();
                        });
                    });
                });

                it('should have h5 size', function() {
                    var themes = ['nvGreen', 'nvRed', 'nvBlue', 'nvYellow'];
                    var types = [['raised', 'h5'], ['flat', 'h5'], ['ghost', 'h5'], ['flat', 'alternate', 'h5']];
                    _.each(themes, function(theme){
                        _.each(types, function(type){
                            element = $(createDirective({}, type, theme));
                            var button = element.find('button.nv-button');
                            expect(button).toHaveCss({
                                fontSize: '18px',
                                height: '45px',
                                width: '45px',
                                borderRadius: '4px',
                            });
                            element.remove();
                        });
                    });
                });

                it('should have h4 size', function() {
                    var themes = ['nvGreen', 'nvRed', 'nvBlue', 'nvYellow'];
                    var types = [['raised', 'h4'], ['flat', 'h4'], ['ghost', 'h4'], ['flat', 'alternate', 'h4']];
                    _.each(themes, function(theme){
                        _.each(types, function(type){
                            element = $(createDirective({}, type, theme));
                            var button = element.find('button.nv-button');
                            expect(button).toHaveCss({
                                fontSize: '22px',
                                height: '55px',
                                width: '55px',
                                borderRadius: '4px',
                            });
                            element.remove();
                        });
                    });
                });

                it('should have dense size', function() {
                    var themes = ['nvGreen', 'nvRed', 'nvBlue', 'nvYellow'];
                    var types = [['raised', 'dense'], ['flat', 'dense'], ['ghost', 'dense'], ['flat', 'alternate', 'dense']];
                    _.each(themes, function(theme){
                        _.each(types, function(type){
                            element = $(createDirective({}, type, theme));
                            var button = element.find('button.nv-button');
                            expect(button).toHaveCss({
                                fontSize: '14px',
                                height: '28px',
                                width: '28px',
                                borderRadius: '4px',
                            });
                            element.remove();
                        });
                    });
                });

                it('should have dense h5 size', function() {
                    var themes = ['nvGreen', 'nvRed', 'nvBlue', 'nvYellow'];
                    var types = [['raised', 'h5', 'dense'], ['flat', 'h5', 'dense'], ['ghost', 'h5', 'dense'], ['flat', 'alternate', 'h5', 'dense']];
                    _.each(themes, function(theme){
                        _.each(types, function(type){
                            element = $(createDirective({}, type, theme));
                            var button = element.find('button.nv-button');
                            expect(button).toHaveCss({
                                fontSize: '18px',
                                height: '36px',
                                width: '36px',
                                borderRadius: '4px',
                            });
                            element.remove();
                        });
                    });
                });

                it('should have dense h4 size', function() {
                    var themes = ['nvGreen', 'nvRed', 'nvBlue', 'nvYellow'];
                    var types = [['raised', 'h4', 'dense'], ['flat', 'h4', 'dense'], ['ghost', 'h4', 'dense'], ['flat', 'alternate', 'h4', 'dense']];
                    _.each(themes, function(theme){
                        _.each(types, function(type){
                            element = $(createDirective({}, type, theme));
                            var button = element.find('button.nv-button');
                            expect(button).toHaveCss({
                                fontSize: '22px',
                                height: '44px',
                                width: '44px',
                                borderRadius: '4px',
                            });
                            element.remove();
                        });
                    });
                });
            });

            describe('button icon', function(){
                it('should have default icon size', function() {
                    element = $(createDirective({ name: 'buttonname', icon: 'save' }));
                    var i = element.find('button.nv-button').find('i');
                    expect(i).toHaveCss({
                        fontSize: '20px', //'19.6px', phantomJS does not support subpixel rendering
                    });
                });
                it('should have default h5 icon size', function() {
                    element = $(createDirective({ name: 'buttonname', icon: 'save' }, ['h5']));
                    var i = element.find('button.nv-button').find('i');
                    expect(i).toHaveCss({
                        fontSize: '25px', //'25.2px', phantomJS does not support subpixel rendering
                    });
                });
                it('should have default h4 icon size', function() {
                    element = $(createDirective({ name: 'buttonname', icon: 'save' }, ['h4']));
                    var i = element.find('button.nv-button').find('i');
                    expect(i).toHaveCss({
                        fontSize: '31px', //'30.8px', phantomJS does not support subpixel rendering
                    });
                });
            });
        });

        describe('scope behaviour', function(){
            it('should show correct icon', function(){
                element = $(createDirective({ name: 'buttonname', icon: 'save' }));

                var button = element.find('button.nv-button');
                expect(button).toContainElement('i');

                var i = button.find('i');
                expect(i).toContainText('save');
            });

            it('should watch icon change', function(){
                element = $(createDirective({ name: 'buttonname', icon: 'save' }));

                var i = element.find('button.nv-button').find('i');
                expect(i).toContainText('save');

                $scope.data.icon = 'delete';
                $scope.$apply();

                expect(i).toContainText('delete');
            });

            it('should disable based on disable', function() {
                element = $(createDirective({ disabled: false }));
                var button = element.find('button.nv-button');

                expect(button).not.toHaveAttr('disabled');

                $scope.data.disabled = true;
                $scope.$apply();
                expect(button).toHaveAttr('disabled', 'disabled');
            });

            it('should call onClick when clicked', function(done){
                element = $(createDirective({ disabled: false }));

                $scope.data.onClick = jasmine.createSpy().and.callFake(function(event){
                    expect(event).toEqual(jasmine.any(Event));
                    done();
                });
                $scope.$apply();

                var button = element.find('button.nv-button');
                button.click();
                expect($scope.data.onClick).toHaveBeenCalled();
            });
        });

    });

})();
