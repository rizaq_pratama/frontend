(function test() {
  describe('tag.model', () => {
    const module = angular.mock.module;
    const inject = angular.mock.inject;

    beforeEach(module('nvCommons.services'));
    beforeEach(module('nvCommons.models'));

    describe('Tag.all', () => {
      let $httpBackend;
      let requestHandler;
      let $rootScope;

      beforeEach(inject(($injector, _$rootScope_) => {
        $httpBackend = $injector.get('$httpBackend');
        requestHandler = $httpBackend.whenGET('route/1.0/tags').respond({
          nvVersion: '1.0',
          data: {
            totalItems: 1,
            tags: [{
              id: 123,
              name: 'FLT',
              description: 'For core fleet, auto tagged when submitting route thru zonal routing',
            }],
          },
        });
        $rootScope = _$rootScope_;
      }));

      afterEach(() => {
        $httpBackend.verifyNoOutstandingExpectation();
        $httpBackend.verifyNoOutstandingRequest();
      });

      it('should call the right endpoint', inject((Tag) => {
        $httpBackend.expectGET('route/1.0/tags');
        Tag.all();
        $httpBackend.flush();
      }));

      it('should return resolved promise when succeed', inject((Tag) => {
        let result;
        Tag.all().then((value) => {
          result = value;
        });
        $httpBackend.flush();
        $rootScope.$apply();
        expect(result).toEqual({
          totalItems: 1,
          tags: [{
            id: 123,
            name: 'FLT',
            description: 'For core fleet, auto tagged when submitting route thru zonal routing',
          }],
        });
      }));

      it('should reject resolved promise when fail', inject((Tag) => {
        requestHandler.respond(401, '');
        let failed = false;
        Tag.all().catch(() => {
          failed = true;
        });
        $httpBackend.flush();
        $rootScope.$apply();
        expect(failed).toBe(true);
      }));
    });

    describe('Tag.create', () => {
      let $httpBackend;
      let requestHandler;
      let $rootScope;

      beforeEach(inject(($injector, _$rootScope_) => {
        $httpBackend = $injector.get('$httpBackend');
        requestHandler = $httpBackend.whenPOST('route/1.0/tags').respond({
          nvVersion: '1.0',
          data: {
            totalItems: 1,
            tags: [{
              id: 123,
              name: 'FLT',
              description: 'For core fleet, auto tagged when submitting route thru zonal routing',
            }],
          },
        });
        $rootScope = _$rootScope_;
      }));

      afterEach(() => {
        $httpBackend.verifyNoOutstandingExpectation();
        $httpBackend.verifyNoOutstandingRequest();
      });

      it('should call the right endpoint', inject((Tag) => {
        $httpBackend.expectPOST('route/1.0/tags', [{
          name: 'FLT',
          description: 'For core fleet, auto tagged when submitting route thru zonal routing',
        }]);
        Tag.create([{
          name: 'FLT',
          description: 'For core fleet, auto tagged when submitting route thru zonal routing',
        }]);
        $httpBackend.flush();
      }));

      it('should return resolved promise when succeed', inject((Tag) => {
        let value;
        Tag.create([{
          name: 'FLT',
          description: 'For core fleet, auto tagged when submitting route thru zonal routing',
        }]).then((data) => {
          value = data;
        });
        $httpBackend.flush();
        $rootScope.$apply();
        expect(value).toEqual({
          totalItems: 1,
          tags: [{
            id: 123,
            name: 'FLT',
            description: 'For core fleet, auto tagged when submitting route thru zonal routing',
          }],
        });
      }));

      it('should reject resolved promise when fail', inject((Tag) => {
        requestHandler.respond(401, '');
        let failed = false;
        Tag.create([{
          name: 'FLT',
          description: 'For core fleet, auto tagged when submitting route thru zonal routing',
        }]).catch(() => {
          failed = true;
        });
        $httpBackend.flush();
        $rootScope.$apply();
        expect(failed).toBe(true);
      }));
    });

    describe('Tag.updateOne', () => {
      let $httpBackend;
      let requestHandler;
      let $rootScope;

      beforeEach(inject(($injector, _$rootScope_) => {
        $httpBackend = $injector.get('$httpBackend');
        requestHandler = $httpBackend.whenPUT(/^route\/1.0\/tags\/\d+$/).respond({
          nvVersion: '1.0',
          data: {
            totalItems: 1,
            tag: {
              id: 123,
              name: 'FLT',
              description: 'For core fleet, auto tagged when submitting route thru zonal routing',
            },
          },
        });
        $rootScope = _$rootScope_;
      }));

      afterEach(() => {
        $httpBackend.verifyNoOutstandingExpectation();
        $httpBackend.verifyNoOutstandingRequest();
      });

      it('should call the right endpoint', inject((Tag) => {
        $httpBackend.expectPUT('route/1.0/tags/123', {
          id: 123,
          name: 'FLT',
          description: 'For core fleet, auto tagged when submitting route thru zonal routing',
        });
        Tag.updateOne({
          id: 123,
          name: 'FLT',
          description: 'For core fleet, auto tagged when submitting route thru zonal routing',
        });
        $httpBackend.flush();
      }));

      it('should return resolved promise when succeed', inject((Tag) => {
        let value;
        Tag.updateOne({
          id: 123,
          name: 'FLT',
          description: 'For core fleet, auto tagged when submitting route thru zonal routing',
        }).then((data) => {
          value = data;
        });
        $httpBackend.flush();
        $rootScope.$apply();
        expect(value).toEqual({
          totalItems: 1,
          tag: {
            id: 123,
            name: 'FLT',
            description: 'For core fleet, auto tagged when submitting route thru zonal routing',
          },
        });
      }));

      it('should reject resolved promise when fail', inject((Tag) => {
        requestHandler.respond(401, '');
        let failed = false;
        Tag.updateOne({
          id: 123,
          name: 'FLT',
          description: 'For core fleet, auto tagged when submitting route thru zonal routing',
        }).catch(() => {
          failed = true;
        });
        $httpBackend.flush();
        $rootScope.$apply();
        expect(failed).toBe(true);
      }));
    });

    describe('Tag.deleteOne', () => {
      let $httpBackend;
      let requestHandler;
      let $rootScope;

      beforeEach(inject(($injector, _$rootScope_) => {
        $httpBackend = $injector.get('$httpBackend');
        requestHandler = $httpBackend.whenDELETE(/^route\/1.0\/tags\/\d+$/).respond({
          nvVersion: '1.0',
          data: {
            totalItems: 1,
            tag: {
              id: 123,
              name: 'FLT',
              description: 'For core fleet, auto tagged when submitting route thru zonal routing',
            },
          },
        });
        $rootScope = _$rootScope_;
      }));

      afterEach(() => {
        $httpBackend.verifyNoOutstandingExpectation();
        $httpBackend.verifyNoOutstandingRequest();
      });

      it('should call the right endpoint', inject((Tag) => {
        $httpBackend.expectDELETE('route/1.0/tags/123');
        Tag.deleteOne({
          id: 123,
          name: 'FLT',
          description: 'For core fleet, auto tagged when submitting route thru zonal routing',
        });
        $httpBackend.flush();
      }));

      it('should return resolved promise when succeed', inject((Tag) => {
        let value;
        Tag.deleteOne({
          id: 123,
          name: 'FLT',
          description: 'For core fleet, auto tagged when submitting route thru zonal routing',
        }).then((data) => {
          value = data;
        });
        $httpBackend.flush();
        $rootScope.$apply();
        expect(value).toEqual({
          totalItems: 1,
          tag: {
            id: 123,
            name: 'FLT',
            description: 'For core fleet, auto tagged when submitting route thru zonal routing',
          },
        });
      }));

      it('should reject resolved promise when fail', inject((Tag) => {
        requestHandler.respond(401, '');
        let failed = false;
        Tag.deleteOne({
          id: 123,
          name: 'FLT',
          description: 'For core fleet, auto tagged when submitting route thru zonal routing',
        }).catch(() => {
          failed = true;
        });
        $httpBackend.flush();
        $rootScope.$apply();
        expect(failed).toBe(true);
      }));
    });

    describe('Tag.getAddFields', () => {
      it('should return the fields needed', inject((Tag) => {
        expect(Tag.getAddFields()).toEqual(jasmine.objectContaining({
          name: jasmine.any(Object),
          description: jasmine.any(Object),
        }));
      }));
    });

    describe('Tag.getEditFields', () => {
      it('should return the fields needed', inject((Tag) => {
        expect(Tag.getEditFields()).toEqual(jasmine.objectContaining({
          id: jasmine.any(Object),
          name: jasmine.any(Object),
          description: jasmine.any(Object),
        }));
      }));
    });

    describe('Tag.getTagId', () => {
      it('should return the tag id', inject((Tag) => {
        const tags = [{
          id: 4,
          name: 'FLT',
          description: 'For core fleet, auto tagged when submitting route thru zonal routing',
        }, {
          id: 8,
          name: 'SDD',
          description: 'Sameday',
        }];

        expect(Tag.getTagId('SDD', tags)).toBe(8);
        expect(Tag.getTagId('FLT', tags)).toBe(4);
        expect(Tag.getTagId('ZZZ', tags)).toBe(0);
      }));
    });

    describe('Tag.toOptions', () => {
      it('should return the formatted options', inject((Tag) => {
        const tagsAllResponse = {
          totalItems: 1,
          tags: [{
            id: 123,
            name: 'FLT',
            description: 'For core fleet, auto tagged when submitting route thru zonal routing',
          }],
        };

        expect(Tag.toOptions(tagsAllResponse)).toEqual([{ displayName: 'FLT', value: 123 }]);
      }));
    });
  });
}());
