(function test() {
  describe('timewindow.model', () => {
    const module = angular.mock.module;
    const inject = angular.mock.inject;

    beforeEach(module('nvCommons.services'));
    beforeEach(module('nvCommons.models'));

    describe('Timewindow.toOptions', () => {
      it('should return the formatted options', inject((Timewindow) => {
        expect(Timewindow.toOptions([Timewindow.TYPE.NIGHTSLOT])).toEqual([{ displayName: 'Night Slot (6PM - 10PM)', value: -3 }]);
      }));
    });
  });
}());
