(function test() {
  describe('waypoint.model', () => {
    const module = angular.mock.module;
    const inject = angular.mock.inject;

    beforeEach(module('nvCommons.services'));
    beforeEach(module('nvCommons.models'));

    describe('Waypoint.cleanup', () => {
      let $httpBackend;
      let requestHandler;
      let $rootScope;
      let nvDateTimeUtils;

      beforeEach(inject(($injector, _$rootScope_, _nvDateTimeUtils_) => {
        $httpBackend = $injector.get('$httpBackend');
        requestHandler = $httpBackend.whenGET(/^core\/tools\/cleanwaypoints\?.+$/).respond([{
          id: 111,
          address1: 'ABC',
          address2: 'ABC',
          postcode: 'ABC',
          city: 'ABC',
          country: 'ABC',
          latitude: 1.234,
          longitude: 1.234,
        }]);
        $rootScope = _$rootScope_;
        nvDateTimeUtils = _nvDateTimeUtils_;
      }));

      afterEach(() => {
        $httpBackend.verifyNoOutstandingExpectation();
        $httpBackend.verifyNoOutstandingRequest();
      });

      it('should call the right endpoint', inject((Waypoint, nvDateTimeUtils, nvTimezone) => {
        const from = new Date(2015, 8, 15, 0, 0, 0);
        const to = new Date(2015, 8, 15, 0, 0, 0);

        const fromStr = nvDateTimeUtils.displayDateTime(nvDateTimeUtils.toSOD(from, nvTimezone.getOperatorTimezone()), 'utc')
        const toStr = nvDateTimeUtils.displayDateTime(nvDateTimeUtils.toEOD(to, nvTimezone.getOperatorTimezone()), 'utc')

        $httpBackend.expectGET(`core/tools/cleanwaypoints?from=${fromStr}&to=${toStr}`);
        Waypoint.cleanup(from, to);
        $httpBackend.flush();
      }));

      it('should return resolved promise when succeed', inject((Waypoint) => {
        let result;
        const from = new Date(2015, 8, 15, 0, 0, 0);
        const to = new Date(2015, 8, 15, 0, 0, 0);
        Waypoint.cleanup(from, to).then((value) => {
          result = value;
        });
        $httpBackend.flush();
        $rootScope.$apply();
        expect(result).toEqual([{
          id: 111,
          address1: 'ABC',
          address2: 'ABC',
          postcode: 'ABC',
          city: 'ABC',
          country: 'ABC',
          latitude: 1.234,
          longitude: 1.234,
        }]);
      }));

      it('should reject resolved promise when fail', inject((Waypoint) => {
        requestHandler.respond(401, '');
        let failed = false;
        const from = new Date(2015, 8, 15, 0, 0, 0);
        const to = new Date(2015, 8, 15, 0, 0, 0);
        Waypoint.cleanup(from, to).catch(() => {
          failed = true;
        });
        $httpBackend.flush();
        $rootScope.$apply();
        expect(failed).toBe(true);
      }));
    });
  });
}());
