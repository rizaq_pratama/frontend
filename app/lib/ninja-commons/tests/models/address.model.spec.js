(function test() {
  describe('Address.model', () => {
    const module = angular.mock.module;
    const inject = angular.mock.inject;

    beforeEach(module('nvCommons.services'));
    beforeEach(module('nvCommons.models'));

    let $rootScope;
    let Address;

    beforeEach(inject((_$rootScope_, _Address_) => {
      $rootScope = _$rootScope_;
      Address = _Address_;
    }));

    describe('Address.uploadCSV()', () => {
      let $httpBackend;
      let requestHandler;
      let $http;

      beforeEach(inject(($injector, _$http_) => {
        $http = _$http_;
        $httpBackend = $injector.get('$httpBackend');
        requestHandler = $httpBackend.whenPOST('addressing/waypoints/csv').respond([{}]);
        $http.upload = (url, data) => $http.post(url, data);
        spyOn($http, 'upload').and.callThrough();
      }));

      afterEach(() => {
        $httpBackend.verifyNoOutstandingExpectation();
        $httpBackend.verifyNoOutstandingRequest();
      });

      it('should call the right endpoint', () => {
        Address.uploadCSV({ id: 1 });
        expect($http.upload).toHaveBeenCalledWith('addressing/waypoints/csv', { id: 1 });
        $httpBackend.flush();
      });

      it('should return resolved promise when succeed', () => {
        let result;
        Address.uploadCSV([{ id: 1 }]).then((value) => {
          result = value;
        });
        $httpBackend.flush();
        $rootScope.$apply();
        expect(result).toEqual(jasmine.any(Array));
        expect(result[0]).toEqual({});
      });

      it('should reject resolved promise when fail', () => {
        requestHandler.respond(401, '');
        let failed = false;
        Address.uploadCSV({ id: 1 }).catch(() => {
          failed = true;
        });
        $httpBackend.flush();
        $rootScope.$apply();
        expect(failed).toBe(true);
      });

      it('should reject promise when no file selected', () => {
        let failed = false;
        Address.uploadCSV(null).catch(() => {
          failed = true;
        });
        $rootScope.$apply();
        expect(failed).toBe(true);
      });
    });

    describe('Address.updateWaypoints', () => {
      let $httpBackend;
      let requestHandler;

      beforeEach(inject((_$httpBackend_) => {
        $httpBackend = _$httpBackend_;
        requestHandler = $httpBackend.whenPUT('addressing/waypoints').respond([{}]);
      }));

      afterEach(() => {
        $httpBackend.verifyNoOutstandingExpectation();
        $httpBackend.verifyNoOutstandingRequest();
      });

      it('should call the right api endpoint', () => {
        $httpBackend.expectPUT('addressing/waypoints', { waypoints: [{ a: 1 }, { b: 1 }] });
        Address.updateWaypoints([{ a: 1 }, { b: 1 }]);
        $httpBackend.flush();
      });

      it('should return resolved promise when succeed', () => {
        let result;
        Address.updateWaypoints([{ a: 1 }, { b: 1 }]).then((value) => {
          result = value;
        });
        $httpBackend.flush();
        $rootScope.$apply();
        expect(result).toEqual([{}]);
      });

      it('should reject resolved promise when fail', () => {
        requestHandler.respond(401, '');
        let failed = false;
        Address.updateWaypoints([{ a: 1 }, { b: 1 }]).catch(() => {
          failed = true;
        });
        $httpBackend.flush();
        $rootScope.$apply();
        expect(failed).toBe(true);
      });
    });
  });
}());
