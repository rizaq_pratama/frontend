(function test() {
  describe('hub.model', () => {
    const module = angular.mock.module;
    const inject = angular.mock.inject;
    let Hub;
    let $rootScope;

    beforeEach(module('nvCommons.services'));
    beforeEach(module('nvCommons.models'));

    beforeEach(inject((_Hub_, _$rootScope_) => {
      Hub = _Hub_;
      $rootScope = _$rootScope_;
    }));

    beforeEach(inject((nvExtendUtils) => {
      spyOn(nvExtendUtils, 'addLatLng').and.callFake((objs) => {
        if (_.isArray(objs)) {
          return _.map(objs, obj => _.assign(obj, { _latlng: `${obj.latitude}, ${obj.longitude}` }));
        }
        return _.assign(objs, { _latlng: `${objs.latitude}, ${objs.longitude}` });
      });
    }));

    describe('Hub.read()', () => {
      let $httpBackend;
      let requestHandler;

      beforeEach(inject(($injector) => {
        $httpBackend = $injector.get('$httpBackend');
        requestHandler = $httpBackend.whenGET(/^core\/hubs\?.*$/).respond([{
          latitude: 12.32,
          longitude: 23.31,
          name: 'Test Hub',
        }]);
      }));

      afterEach(() => {
        $httpBackend.verifyNoOutstandingExpectation();
        $httpBackend.verifyNoOutstandingRequest();
      });

      it('should call the right endpoint with active_only default true when no argument passed', () => {
        $httpBackend.expectGET('core/hubs?active_only=true');
        Hub.read();
        $httpBackend.flush();
      });

      it('should call the right endpoin with active_only param filled as in passed arguments', () => {
        $httpBackend.expectGET('core/hubs?active_only=false');
        Hub.read({ active_only: false });
        $httpBackend.flush();
      });

      it('should return resolved promise when succeed', () => {
        let result;
        Hub.read().then((value) => {
          result = value;
        });
        $httpBackend.flush();
        $rootScope.$apply();
        expect(result).toEqual([{
          latitude: 12.32,
          longitude: 23.31,
          name: 'Test Hub',
          _latlng: '12.32, 23.31',
        }]);
      });

      it('should reject resolved promise when fail', () => {
        let failed = false;
        requestHandler.respond(401, '');
        Hub.read().catch(() => {
          failed = true;
        });
        $httpBackend.flush();
        $rootScope.$apply();
        expect(failed).toBe(true);
      });
    });

    describe('Hub.create', () => {
      let $httpBackend;
      let requestHandler;

      beforeEach(inject(($injector) => {
        $httpBackend = $injector.get('$httpBackend');
        requestHandler = $httpBackend.whenPOST('core/hubs').respond({
          id: 123,
          name: 'test hub',
          latitude: 11.11,
          longitude: 12.34,
        });
      }));

      afterEach(() => {
        $httpBackend.verifyNoOutstandingExpectation();
        $httpBackend.verifyNoOutstandingRequest();
      });

      it('should call the right endpoint', () => {
        $httpBackend.expectPOST('core/hubs', {
          name: 'test hub',
          latitude: 11.11,
          longitude: 12.34,
        });
        Hub.create({
          name: 'test hub',
          latitude: 11.11,
          longitude: 12.34,
        });
        $httpBackend.flush();
      });

      it('should return resolved promise when succeed', () => {
        let value;
        Hub.create({
          name: 'test hub',
          latitude: 11.11,
          longitude: 12.34,
        }).then((data) => {
          value = data;
        });
        $httpBackend.flush();
        $rootScope.$apply();
        expect(value).toEqual({
          id: 123,
          name: 'test hub',
          latitude: 11.11,
          longitude: 12.34,
          _latlng: '11.11, 12.34',
        });
      });

      it('should reject resolved promise when fail', () => {
        requestHandler.respond(401, '');
        let failed = false;
        Hub.create({
          name: 'test hub',
          latitude: 11.11,
          longitude: 12.34,
        }).catch(() => {
          failed = true;
        });
        $httpBackend.flush();
        $rootScope.$apply();
        expect(failed).toBe(true);
      });
    });

    describe('Hub.update', () => {
      let $httpBackend;
      let requestHandler;

      beforeEach(inject(($injector) => {
        $httpBackend = $injector.get('$httpBackend');
        requestHandler = $httpBackend.whenPUT(/^core\/hubs\/\d+$/).respond({
          id: 123,
          name: 'test hub',
          latitude: 11.11,
          longitude: 12.34,
        });
      }));

      afterEach(() => {
        $httpBackend.verifyNoOutstandingExpectation();
        $httpBackend.verifyNoOutstandingRequest();
      });

      it('should call the right endpoint', () => {
        $httpBackend.expectPUT('core/hubs/5', {
          name: 'test hub',
          latitude: 11.11,
          longitude: 12.34,
        });
        Hub.update({
          id: 5,
          name: 'test hub',
          latitude: 11.11,
          longitude: 12.34,
        });
        $httpBackend.flush();
      });

      it('should return resolved promise when succeed', () => {
        let value;
        Hub.update({
          id: 5,
          name: 'test hub',
          latitude: 11.11,
          longitude: 12.34,
        }).then((data) => {
          value = data;
        });
        $httpBackend.flush();
        $rootScope.$apply();
        expect(value).toEqual({
          id: 123,
          name: 'test hub',
          latitude: 11.11,
          longitude: 12.34,
          _latlng: '11.11, 12.34',
        });
      });

      it('should reject resolved promise when fail', () => {
        requestHandler.respond(401, '');
        let failed = false;
        Hub.update({
          id: 5,
          name: 'test hub',
          latitude: 11.11,
          longitude: 12.34,
        }).catch(() => {
          failed = true;
        });
        $httpBackend.flush();
        $rootScope.$apply();
        expect(failed).toBe(true);
      });
    });

    describe('Hub.delete', () => {
      let $httpBackend;
      let requestHandler;

      beforeEach(inject(($injector) => {
        $httpBackend = $injector.get('$httpBackend');
        requestHandler = $httpBackend.whenDELETE(/^core\/hubs\/\d+$/).respond({
          id: 123,
          name: 'test hub',
          latitude: 11.11,
          longitude: 12.34,
        });
      }));

      afterEach(() => {
        $httpBackend.verifyNoOutstandingExpectation();
        $httpBackend.verifyNoOutstandingRequest();
      });

      it('should call the right endpoint', () => {
        $httpBackend.expectDELETE('core/hubs/5');
        Hub.delete({
          id: 5,
          name: 'test hub',
          latitude: 11.11,
          longitude: 12.34,
        });
        $httpBackend.flush();
      });

      it('should return resolved promise when succeed', () => {
        let value;
        Hub.delete({
          id: 5,
          name: 'test hub',
          latitude: 11.11,
          longitude: 12.34,
        }).then((data) => {
          value = data;
        });
        $httpBackend.flush();
        $rootScope.$apply();
        expect(value).toEqual({
          id: 123,
          name: 'test hub',
          latitude: 11.11,
          longitude: 12.34,
        });
      });

      it('should reject resolved promise when fail', () => {
        requestHandler.respond(401, '');
        let failed = false;
        Hub.delete({
          id: 5,
          name: 'test hub',
          latitude: 11.11,
          longitude: 12.34,
        }).catch(() => {
          failed = true;
        });
        $httpBackend.flush();
        $rootScope.$apply();
        expect(failed).toBe(true);
      });
    });

    describe('Hub.toOptions', () => {
      it('should return the fields needed', () => {
        const hubs = [
          { id: 123,
            name: 'test hub 1',
            latitude: 11.11,
            longitude: 12.34,
          },
          { id: 234,
            name: 'test hub 2',
            latitude: 11.11,
            longitude: 12.34,
          },
          { id: 345,
            name: 'test hub 3',
            latitude: 11.11,
            longitude: 12.34,
          },
        ];
        expect(Hub.toOptions(hubs)).toEqual([
          { displayName: 'test hub 1', value: 123 },
          { displayName: 'test hub 2', value: 234 },
          { displayName: 'test hub 3', value: 345 },
        ]);
      });
    });

    describe('Hub.getAddFields', () => {
      it('should return the fields needed', () => {
        expect(Hub.getAddFields()).toEqual(jasmine.objectContaining({
          name: jasmine.any(Object),
          longitude: jasmine.any(Object),
          latitude: jasmine.any(Object),
        }));
      });
    });

    describe('Hub.getEditFields', () => {
      it('should return the fields needed', () => {
        expect(Hub.getEditFields()).toEqual(jasmine.objectContaining({
          id: jasmine.any(Object),
          name: jasmine.any(Object),
          longitude: jasmine.any(Object),
          latitude: jasmine.any(Object),
        }));
      });
    });
  });
}());
