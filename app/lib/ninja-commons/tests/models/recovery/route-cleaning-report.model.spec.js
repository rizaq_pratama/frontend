(function test() {
  describe('route-cleaning-report.model', () => {
    const module = angular.mock.module;
    const inject = angular.mock.inject;

    beforeEach(module('pascalprecht.translate'));
    beforeEach(module('nvCommons.services'));
    beforeEach(module('nvCommons.models'));

    let RouteCleaningReport;
    let Excel;
    let $q;
    let $rootScope;
    let downloadSuccess = true;

    beforeEach(inject((_RouteCleaningReport_, _Excel_, _$q_, _$rootScope_) => {
      RouteCleaningReport = _RouteCleaningReport_;
      Excel = _Excel_;
      $q = _$q_;
      $rootScope = _$rootScope_;

      spyOn(Excel, 'download').and.callFake(() => {
        if (downloadSuccess) {
          return $q.resolve();
        }
        return $q.reject();
      });
    }));

    describe('RouteCleaningReport.getJSON', () => {
      let $httpBackend;
      let requestHandler;

      beforeEach(inject((_$httpBackend_) => {
        $httpBackend = _$httpBackend_;
        requestHandler = $httpBackend.whenGET(/^core\/reports\/getroutecleaningreportV3\?.+$/)
          .respond({
            data: {
              reservations:
              [{ date: '2016-10-03', exception: 'Unrouted reservation', shipperName: 'Lazada (CEF electronics)', driverName: 'N/A' },
               { date: '2016-10-03', exception: 'Unrouted reservation', shipperName: 'Javy Group Pte Ltd', driverName: 'N/A' },
               { date: '2016-10-03', exception: 'Unrouted reservation', shipperName: 'Cluster-Cluster', driverName: 'N/A' },
               { date: '2016-10-03', exception: 'Unrouted reservation', shipperName: 'Anchanto', driverName: 'N/A' },
               { date: '2016-10-03', exception: 'Unrouted reservation', shipperName: 'Lazada (Nicedeal)', driverName: 'N/A' },
               { date: '2016-10-03', exception: 'Unrouted reservation', shipperName: 'Lazada (XIAOMEI PTE LTD)', driverName: 'N/A' },
              ],
              cod:
              [{ date: '2016-10-03', codInbounded: 0, routeId: 0, driverName: 'COD Breakdown - Total COD (by ServiceEndTime) | Total Inbounded', codExpected: 66.2 }],
              parcels:
              [{ date: '2016-10-03', lastScanHubId: 3, exception: 'Non inbounded failed delivery', routeId: 52620, lastSeen: '2016-10-03 10:11:02.0', driverName: 'OPS - Ninja Sim', shipperName: 'Camera Rental Centre', lastScanType: 'INBOUND (SORTING_HUB)', trackingId: 'ZNV204725177SG-8705' }],
            },
            success: true,
            errors: [],
            e: {},
          });
      }));

      afterEach(() => {
        $httpBackend.verifyNoOutstandingExpectation();
        $httpBackend.verifyNoOutstandingRequest();
      });

      it('should call the right endpoint', () => {
        $httpBackend.expectGET('core/reports/getroutecleaningreportV3?date=2015-09-14&format=json');
        const date = new Date('2015-09-14');
        RouteCleaningReport.getJSON(date);
        $httpBackend.flush();
      });

      it('should return resolved promise when succeed and processed the payload', () => {
        let result;
        const date = new Date('2015-09-14');
        RouteCleaningReport.getJSON(date).then((value) => {
          result = value;
        });
        $httpBackend.flush();
        $rootScope.$apply();
        expect(result).toEqual(jasmine.objectContaining({
          reservations: jasmine.any(Array),
          cod: jasmine.any(Array),
          parcels: jasmine.any(Array),
        }));
        expect(result.parcels[0]).toEqual(jasmine.objectContaining({
          _momentLastSeen: jasmine.any(moment),
        }));
      });

      it('should reject resolved promise when fail', () => {
        requestHandler.respond(401, '');
        let failed = false;
        const date = new Date('2015-09-14');
        RouteCleaningReport.getJSON(date).catch(() => {
          failed = true;
        });
        $httpBackend.flush();
        $rootScope.$apply();
        expect(failed).toBe(true);
      });
    });

    describe('RouteCleaningReport.getExcel', () => {
      it('should call the right endpoint', () => {
        const date = new Date('2015-09-14');
        RouteCleaningReport.getExcel(date);
        expect(Excel.download).toHaveBeenCalledWith('core/reports/getroutecleaningreportV3?date=2015-09-14', 'route-cleaning-report.xls');
      });

      it('should return resolved promise when succeed', () => {
        let resolved = false;
        const date = new Date('2015-09-14');
        RouteCleaningReport.getExcel(date).then(() => {
          resolved = true;
        });
        $rootScope.$apply();
        expect(resolved).toBe(true);
      });

      it('should reject resolved promise when fail', () => {
        downloadSuccess = false;
        let failed = false;
        const date = new Date('2015-09-14');
        RouteCleaningReport.getExcel(date).catch(() => {
          failed = true;
        });
        $rootScope.$apply();
        expect(failed).toBe(true);
      });
    });
  });
}());
