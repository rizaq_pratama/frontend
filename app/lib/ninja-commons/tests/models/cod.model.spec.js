(function test() {
  describe('cod.model', () => {
    const module = angular.mock.module;
    const inject = angular.mock.inject;

    beforeEach(module('nvCommons.services'));
    beforeEach(module('nvCommons.models'));

    describe('COD.getAll', () => {
      let $httpBackend;
      let requestHandler;
      let $rootScope;

      beforeEach(inject(($injector, _$rootScope_) => {
        $httpBackend = $injector.get('$httpBackend');
        requestHandler = $httpBackend.whenGET(/^core\/codinbounds\?.*$/).respond([{
          id: 123,
          routeId: 11,
          amountCollected: 11.11,
          receiptNo: 'ABC',
          inboundedBy: 'aa',
        }]);
        $rootScope = _$rootScope_;
      }));

      afterEach(() => {
        $httpBackend.verifyNoOutstandingExpectation();
        $httpBackend.verifyNoOutstandingRequest();
      });

      it('should call the right endpoint', inject((COD, nvDateTimeUtils, nvTimezone) => {
        const from = new Date(2015, 8, 15, 0, 0, 0);
        const to = new Date(2015, 8, 15, 0, 0, 0);
        const fromStr = nvDateTimeUtils.displayDateTime(nvDateTimeUtils.toSOD(from, nvTimezone.getOperatorTimezone()), 'utc')
        const toStr = nvDateTimeUtils.displayDateTime(nvDateTimeUtils.toEOD(to, nvTimezone.getOperatorTimezone()), 'utc')

        $httpBackend.expectGET(`core/codinbounds?start=${fromStr}&end=${toStr}`);
        COD.getAll(from, to);
        $httpBackend.flush();
      }));

      it('should return resolved promise when succeed', inject((COD) => {
        let result;
        const from = new Date(2015, 8, 15, 0, 0, 0);
        const to = new Date(2015, 8, 15, 0, 0, 0);
        COD.getAll(from, to).then((value) => {
          result = value;
        });
        $httpBackend.flush();
        $rootScope.$apply();
        expect(result).toEqual([{
          id: 123,
          routeId: 11,
          amountCollected: 11.11,
          receiptNo: 'ABC',
          inboundedBy: 'aa',
        }]);
      }));

      it('should reject resolved promise when fail', inject((COD) => {
        requestHandler.respond(401, '');
        let failed = false;
        const from = new Date(2015, 8, 15, 0, 0, 0);
        const to = new Date(2015, 8, 15, 0, 0, 0);
        COD.getAll(from, to).catch(() => {
          failed = true;
        });
        $httpBackend.flush();
        $rootScope.$apply();
        expect(failed).toBe(true);
      }));
    });

    describe('COD.getById', () => {
      let $httpBackend;
      let requestHandler;
      let $rootScope;

      beforeEach(inject(($injector, _$rootScope_) => {
        $httpBackend = $injector.get('$httpBackend');
        requestHandler = $httpBackend.whenGET(/^core\/codinbounds\/\d+$/).respond({
          id: 123,
          routeId: 11,
          amountCollected: 11.11,
          receiptNo: 'ABC',
          inboundedBy: 'aa',
        });
        $rootScope = _$rootScope_;
      }));

      afterEach(() => {
        $httpBackend.verifyNoOutstandingExpectation();
        $httpBackend.verifyNoOutstandingRequest();
      });

      it('should call the right endpoint', inject((COD) => {
        $httpBackend.expectGET('core/codinbounds/5');
        COD.getById(5);
        $httpBackend.flush();
      }));

      it('should return resolved promise when succeed', inject((COD) => {
        let result;
        COD.getById(5).then((value) => {
          result = value;
        });
        $httpBackend.flush();
        $rootScope.$apply();
        expect(result).toEqual({
          id: 123,
          routeId: 11,
          amountCollected: 11.11,
          receiptNo: 'ABC',
          inboundedBy: 'aa',
        });
      }));

      it('should reject resolved promise when fail', inject((COD) => {
        requestHandler.respond(401, '');
        let failed = false;
        COD.getById(5).catch(() => {
          failed = true;
        });
        $httpBackend.flush();
        $rootScope.$apply();
        expect(failed).toBe(true);
      }));
    });

    describe('COD.create', () => {
      let $httpBackend;
      let requestHandler;
      let $rootScope;

      beforeEach(inject(($injector, _$rootScope_) => {
        $httpBackend = $injector.get('$httpBackend');
        requestHandler = $httpBackend.whenPOST('core/codinbounds').respond({
          id: 123,
          routeId: 11,
          amountCollected: 11.11,
          receiptNo: 'ABC',
          inboundedBy: 'aa',
        });
        $rootScope = _$rootScope_;
      }));

      afterEach(() => {
        $httpBackend.verifyNoOutstandingExpectation();
        $httpBackend.verifyNoOutstandingRequest();
      });

      it('should call the right endpoint', inject((COD) => {
        $httpBackend.expectPOST('core/codinbounds', {
          routeId: 11,
          amountCollected: 11.11,
          receiptNo: 'ABC',
        });
        COD.create({
          routeId: 11,
          amountCollected: 11.11,
          receiptNo: 'ABC',
        });
        $httpBackend.flush();
      }));

      it('should return resolved promise when succeed', inject((COD) => {
        let value;
        COD.create({
          routeId: 11,
          amountCollected: 11.11,
          receiptNo: 'ABC',
        }).then((data) => {
          value = data;
        });
        $httpBackend.flush();
        $rootScope.$apply();
        expect(value).toEqual({
          id: 123,
          routeId: 11,
          amountCollected: 11.11,
          receiptNo: 'ABC',
          inboundedBy: 'aa',
        });
      }));

      it('should reject resolved promise when fail', inject((COD) => {
        requestHandler.respond(401, '');
        let failed = false;
        COD.create({
          routeId: 11,
          amountCollected: 11.11,
          receiptNo: 'ABC',
        }).catch(() => {
          failed = true;
        });
        $httpBackend.flush();
        $rootScope.$apply();
        expect(failed).toBe(true);
      }));
    });

    describe('COD.update', () => {
      let $httpBackend;
      let requestHandler;
      let $rootScope;

      beforeEach(inject(($injector, _$rootScope_) => {
        $httpBackend = $injector.get('$httpBackend');
        requestHandler = $httpBackend.whenPUT(/^core\/codinbounds\/\d+$/).respond({
          id: 123,
          routeId: 11,
          amountCollected: 11.11,
          receiptNo: 'ABC',
          inboundedBy: 'aa',
        });
        $rootScope = _$rootScope_;
      }));

      afterEach(() => {
        $httpBackend.verifyNoOutstandingExpectation();
        $httpBackend.verifyNoOutstandingRequest();
      });

      it('should call the right endpoint', inject((COD) => {
        $httpBackend.expectPUT('core/codinbounds/5', {
          routeId: 11,
          amountCollected: 11.11,
          receiptNo: 'ABC',
        });
        COD.update({
          id: 5,
          routeId: 11,
          amountCollected: 11.11,
          receiptNo: 'ABC',
        });
        $httpBackend.flush();
      }));

      it('should return resolved promise when succeed', inject((COD) => {
        let value;
        COD.update({
          id: 5,
          routeId: 11,
          amountCollected: 11.11,
          receiptNo: 'ABC',
        }).then((data) => {
          value = data;
        });
        $httpBackend.flush();
        $rootScope.$apply();
        expect(value).toEqual({
          id: 123,
          routeId: 11,
          amountCollected: 11.11,
          receiptNo: 'ABC',
          inboundedBy: 'aa',
        });
      }));

      it('should reject resolved promise when fail', inject((COD) => {
        requestHandler.respond(401, '');
        let failed = false;
        COD.update({
          id: 5,
          routeId: 11,
          amountCollected: 11.11,
          receiptNo: 'ABC',
        }).catch(() => {
          failed = true;
        });
        $httpBackend.flush();
        $rootScope.$apply();
        expect(failed).toBe(true);
      }));
    });

    describe('COD.delete', () => {
      let $httpBackend;
      let requestHandler;
      let $rootScope;

      beforeEach(inject(($injector, _$rootScope_) => {
        $httpBackend = $injector.get('$httpBackend');
        requestHandler = $httpBackend.whenDELETE(/^core\/codinbounds\/\d+$/).respond({
          id: 123,
          routeId: 11,
          amountCollected: 11.11,
          receiptNo: 'ABC',
          inboundedBy: 'aa',
        });
        $rootScope = _$rootScope_;
      }));

      afterEach(() => {
        $httpBackend.verifyNoOutstandingExpectation();
        $httpBackend.verifyNoOutstandingRequest();
      });

      it('should call the right endpoint', inject((COD) => {
        $httpBackend.expectDELETE('core/codinbounds/5');
        COD.delete({
          id: 5,
          routeId: 11,
          amountCollected: 11.11,
          receiptNo: 'ABC',
        });
        $httpBackend.flush();
      }));

      it('should return resolved promise when succeed', inject((COD) => {
        let value;
        COD.delete({
          id: 5,
          routeId: 11,
          amountCollected: 11.11,
          receiptNo: 'ABC',
        }).then((data) => {
          value = data;
        });
        $httpBackend.flush();
        $rootScope.$apply();
        expect(value).toEqual({
          id: 123,
          routeId: 11,
          amountCollected: 11.11,
          receiptNo: 'ABC',
          inboundedBy: 'aa',
        });
      }));

      it('should reject resolved promise when fail', inject((COD) => {
        requestHandler.respond(401, '');
        let failed = false;
        COD.delete({
          id: 5,
          routeId: 11,
          amountCollected: 11.11,
          receiptNo: 'ABC',
        }).catch(() => {
          failed = true;
        });
        $httpBackend.flush();
        $rootScope.$apply();
        expect(failed).toBe(true);
      }));
    });

    describe('COD.getAddFields', () => {
      it('should return the fields needed', inject((COD) => {
        expect(COD.getAddFields()).toEqual(jasmine.objectContaining({
          routeId: jasmine.any(Object),
          amountCollected: jasmine.any(Object),
          receiptNo: jasmine.any(Object),
        }));
      }));
    });

    describe('COD.getEditFields', () => {
      it('should return the fields needed', inject((COD) => {
        expect(COD.getEditFields()).toEqual(jasmine.objectContaining({
          id: jasmine.any(Object),
          routeId: jasmine.any(Object),
          amountCollected: jasmine.any(Object),
          receiptNo: jasmine.any(Object),
        }));
      }));
    });
  });
}());
