(function test() {
  describe('shipper.model', () => {
    const module = angular.mock.module;
    const inject = angular.mock.inject;

    beforeEach(() => {
      module('ui.router', ($provide) => {
        $provide.value('$state', {
          go: jasmine.createSpy(),
        });
      });
      module('nvCommons.services');
      module('nvCommons.models');
    });

    describe('Shippers.elasticRead', () => {
      const processedShipperResult = [{
        id: 37,
        legacy_id: 37,
        global_id: 2613,
        name: 'Ninjavan',
        email: 'support@ninjavan.sg',
        industry_id: 15,
        liaison_email: 'Ourselves',
        contact: '98753063',
        sales_person: 'CW',
        active: false,
      }];

      const exampleResult = {
        details: [{
          id: 2613,
          legacy_id: 37,
          name: 'Ninjavan',
          email: 'support@ninjavan.sg',
          industry_id: 15,
          liaison_email: 'Ourselves',
          contact: '98753063',
          sales_person: 'CW',
          active: false,
        }],
        requested_from: 0,
        requested_size: 100,
        total_hits: 1,
        total_size: 1,
      };

      const exampleProcessedResult = {
        details: processedShipperResult,
        requested_from: 0,
        requested_size: 100,
        total_hits: 1,
        total_size: 1,
      };

      let $httpBackend;
      let requestHandler;
      let $rootScope;

      beforeEach(inject(($injector, _$rootScope_) => {
        $httpBackend = $injector.get('$httpBackend');
        requestHandler = $httpBackend.whenPOST('shipper-search/shippers/list').respond(exampleResult);

        $rootScope = _$rootScope_;
      }));

      afterEach(() => {
        $httpBackend.verifyNoOutstandingExpectation();
        $httpBackend.verifyNoOutstandingRequest();
      });

      it('should call the right endpoint', inject((Shippers) => {
        $httpBackend.expectPOST('shipper-search/shippers/list');
        Shippers.elasticRead(false);
        $httpBackend.flush();
      }));

      it('should return resolved promise when succeed', inject((Shippers) => {
        let result;
        let resultBatch;

        Shippers.elasticRead(false).then((value) => {
          result = value;
        });
        $httpBackend.flush();

        Shippers.elasticReadAll().then((value) => {
          resultBatch = value;
        });
        $httpBackend.flush();

        $rootScope.$apply();
        expect(result).toEqual(exampleProcessedResult);
        expect(resultBatch).toEqual(processedShipperResult);
      }));

      it('should reject resolved promise when fail', inject((Shippers) => {
        requestHandler.respond(401, '');
        let failed = false;
        let failedBatch = false;

        Shippers.elasticRead(false).catch(() => {
          failed = true;
        });
        $httpBackend.flush();

        Shippers.elasticReadAll().catch(() => {
          failedBatch = true;
        });
        $httpBackend.flush();

        $rootScope.$apply();
        expect(failed).toBe(true);
        expect(failedBatch).toBe(true);
      }));
    });

    describe('Shippers.elasticCompactSearch', () => {
      const processedShipperResult = [{
        id: 37,
        legacy_id: 37,
        global_id: 2613,
        name: 'Ninjavan',
        short_name: 'NV',
      }];

      const exampleResult = {
        details: [{
          id: 2613,
          legacy_id: 37,
          name: 'Ninjavan',
          short_name: 'NV',
        }],
        requested_from: 0,
        requested_size: 100,
        total_hits: 1,
        total_size: 1,
      };

      const exampleProcessedResult = {
        details: processedShipperResult,
        requested_from: 0,
        requested_size: 100,
        total_hits: 1,
        total_size: 1,
      };

      let $httpBackend;
      let requestHandler;
      let $rootScope;

      beforeEach(inject(($injector, _$rootScope_) => {
        $httpBackend = $injector.get('$httpBackend');
        requestHandler = $httpBackend.whenPOST('shipper-search/shippers/search').respond(exampleResult);

        $rootScope = _$rootScope_;
      }));

      afterEach(() => {
        $httpBackend.verifyNoOutstandingExpectation();
        $httpBackend.verifyNoOutstandingRequest();
      });

      it('should call the right endpoint', inject((Shippers) => {
        $httpBackend.expectPOST('shipper-search/shippers/search');
        Shippers.elasticCompactSearch(false);
        $httpBackend.flush();
      }));

      it('should return resolved promise when succeed', inject((Shippers) => {
        let result;

        Shippers.elasticCompactSearch(false).then((value) => {
          result = value;
        });
        $httpBackend.flush();

        $rootScope.$apply();
        expect(result).toEqual(exampleProcessedResult);
      }));

      it('should reject resolved promise when fail', inject((Shippers) => {
        requestHandler.respond(401, '');
        let failed = false;

        Shippers.elasticCompactSearch(false).catch(() => {
          failed = true;
        });
        $httpBackend.flush();

        $rootScope.$apply();
        expect(failed).toBe(true);
      }));
    });

    describe('Shippers.filterSearch', () => {
      const processedShipperResult = [{
        id: 37,
        legacy_id: 37,
        global_id: 2613,
        name: 'Ninjavan',
        short_name: 'NV',
        displayName: '37-Ninjavan',
      }];

      const exampleResult = {
        details: [{
          id: 2613,
          legacy_id: 37,
          name: 'Ninjavan',
          short_name: 'NV',
        }],
        requested_from: 0,
        requested_size: 100,
        total_hits: 1,
        total_size: 1,
      };

      let $httpBackend;
      let requestHandler;
      let $rootScope;

      beforeEach(inject(($injector, _$rootScope_) => {
        $httpBackend = $injector.get('$httpBackend');
        requestHandler = $httpBackend.whenPOST('shipper-search/shippers/search').respond(exampleResult);

        $rootScope = _$rootScope_;
      }));

      afterEach(() => {
        $httpBackend.verifyNoOutstandingExpectation();
        $httpBackend.verifyNoOutstandingRequest();
      });

      it('should return filtered shipper data', inject((Shippers) => {
        let result;

        Shippers.filterSearch('NINJA').then((value) => {
          result = value;
        });
        $httpBackend.flush();

        $rootScope.$apply();
        expect(result).toEqual(processedShipperResult);
      }));
    });

    describe('Shippers.read', () => {
      let $httpBackend;
      let requestHandler;
      let requestHandlerBatch;
      let $rootScope;

      beforeEach(inject(($injector, _$rootScope_) => {
        $httpBackend = $injector.get('$httpBackend');
        requestHandler = $httpBackend.whenGET('shipper/2.0/shippers?start=0&limit=100').respond([
          {
            id: 36,
            name: 'Ninjavan',
            short_name: 'Ninjavan',
            active: false,
            email: 'support@ninjavan.sg',
            contact: '98753063',
            prefix: 'NVSG',
            archived: false,
            sales_person: 'CW',
            liaison_name: 'Ourselves',
            liaison_address: '',
            billing_address: '',
            industry_id: 15,
            shipper_pricing_rule_id: 0,
            shipper_pricing_template_id: 1,
            misc_settings: {},
            order_create_settings: {
              version: 'v2',
              services_available: [
                '3DAY',
              ],
              is_pre_paid: false,
              allow_prefixless: false,
              allow_cod_service: false,
              allow_staged_orders: true,
            },
            reservation_settings: {
              days: [
                2,
                3,
                4,
                5,
                6,
                7,
              ],
              auto_reservation_settings: {
                enabled: false,
              },
            },
            shopify_settings: {
              is_shipping_code_filter_enabled: false,
              shipping_codes: [],
            },
            standing_instructions: {
              location: 'PP',
              instructions: 'NINJAVAN',
            },
            returns_settings: {},
            print_settings: {},
            qoo10_settings: {},
            magento_settings: {},
            event_subscriptions: [],
            sub_shippers: [],
          },
        ]);

        requestHandlerBatch = $httpBackend.whenGET('shipper/2.0/shippers?start=0&limit=100').respond([
          {
            id: 36,
            name: 'Ninjavan',
            short_name: 'Ninjavan',
            active: false,
            email: 'support@ninjavan.sg',
            contact: '98753063',
            prefix: 'NVSG',
            archived: false,
            sales_person: 'CW',
            liaison_name: 'Ourselves',
            liaison_address: '',
            billing_address: '',
            industry_id: 15,
            shipper_pricing_rule_id: 0,
            shipper_pricing_template_id: 1,
            misc_settings: {},
            order_create_settings: {
              version: 'v2',
              services_available: [
                '3DAY',
              ],
              is_pre_paid: false,
              allow_prefixless: false,
              allow_cod_service: false,
              allow_staged_orders: true,
            },
            reservation_settings: {
              days: [
                2,
                3,
                4,
                5,
                6,
                7,
              ],
              auto_reservation_settings: {
                enabled: false,
              },
            },
            shopify_settings: {
              is_shipping_code_filter_enabled: false,
              shipping_codes: [],
            },
            standing_instructions: {
              location: 'PP',
              instructions: 'NINJAVAN',
            },
            returns_settings: {},
            print_settings: {},
            qoo10_settings: {},
            magento_settings: {},
            event_subscriptions: [],
            sub_shippers: [],
          },
        ]);

        $rootScope = _$rootScope_;
      }));

      afterEach(() => {
        $httpBackend.verifyNoOutstandingExpectation();
        $httpBackend.verifyNoOutstandingRequest();
      });

      it('should call the right endpoint', inject((Shippers) => {
        $httpBackend.expectGET('shipper/2.0/shippers?start=0&limit=100');
        Shippers.read(false);
        $httpBackend.flush();
      }));

      it('should return resolved promise when succeed', inject((Shippers) => {
        let result;
        let resultBatch;
        const exampleResult = [
          {
            id: 36,
            name: 'Ninjavan',
            short_name: 'Ninjavan',
            active: false,
            email: 'support@ninjavan.sg',
            contact: '98753063',
            prefix: 'NVSG',
            archived: false,
            sales_person: 'CW',
            liaison_name: 'Ourselves',
            liaison_address: '',
            billing_address: '',
            industry_id: 15,
            shipper_pricing_rule_id: 0,
            shipper_pricing_template_id: 1,
            misc_settings: {},
            order_create_settings: {
              version: 'v2',
              services_available: [
                '3DAY',
              ],
              is_pre_paid: false,
              allow_prefixless: false,
              allow_cod_service: false,
              allow_staged_orders: true,
            },
            reservation_settings: {
              days: [
                2,
                3,
                4,
                5,
                6,
                7,
              ],
              auto_reservation_settings: {
                enabled: false,
              },
            },
            shopify_settings: {
              is_shipping_code_filter_enabled: false,
              shipping_codes: [],
            },
            standing_instructions: {
              location: 'PP',
              instructions: 'NINJAVAN',
            },
            returns_settings: {},
            print_settings: {},
            qoo10_settings: {},
            magento_settings: {},
            event_subscriptions: [],
            sub_shippers: [],
          },
        ];
        Shippers.read(false).then((value) => {
          result = value;
        });
        $httpBackend.flush();

        Shippers.read().then((value) => {
          resultBatch = value;
        });
        $httpBackend.flush();

        $rootScope.$apply();
        expect(result).toEqual(exampleResult);
        expect(resultBatch).toEqual(exampleResult);
      }));

      it('should reject resolved promise when fail', inject((Shippers) => {
        requestHandler.respond(401, '');
        requestHandlerBatch.respond(401, '');
        let failed = false;
        let failedBatch = false;

        Shippers.read(false).catch(() => {
          failed = true;
        });
        $httpBackend.flush();

        Shippers.read().catch(() => {
          failedBatch = true;
        });
        $httpBackend.flush();

        $rootScope.$apply();
        expect(failed).toBe(true);
        expect(failedBatch).toBe(true);
      }));
    });

    describe('Shippers.deleteOne', () => {
      let $httpBackend;
      let requestHandler;
      let $rootScope;

      beforeEach(inject(($injector, _$rootScope_) => {
        $httpBackend = $injector.get('$httpBackend');
        requestHandler = $httpBackend.whenDELETE(/^shipper\/shippers\/\d+$/).respond({
          id: 123,
          name: 'Shipper 1',
        });
        $rootScope = _$rootScope_;
      }));

      afterEach(() => {
        $httpBackend.verifyNoOutstandingExpectation();
        $httpBackend.verifyNoOutstandingRequest();
      });

      it('should call the right endpoint', inject((Shippers) => {
        $httpBackend.expectDELETE('shipper/shippers/123');
        Shippers.deleteOne({
          id: 123,
          name: 'Shipper 1',
        });
        $httpBackend.flush();
      }));

      it('should return resolved promise when succeed', inject((Shippers) => {
        let value;
        Shippers.deleteOne({
          id: 123,
          name: 'Shipper 1',
        }).then((data) => {
          value = data;
        });
        $httpBackend.flush();
        $rootScope.$apply();
        expect(value).toEqual({
          id: 123,
          name: 'Shipper 1',
        });
      }));

      it('should reject resolved promise when fail', inject((Shippers) => {
        requestHandler.respond(401, '');
        let failed = false;
        Shippers.deleteOne({
          id: 123,
          name: 'Shipper 1',
        }).catch(() => {
          failed = true;
        });
        $httpBackend.flush();
        $rootScope.$apply();
        expect(failed).toBe(true);
      }));
    });
  });
}());
