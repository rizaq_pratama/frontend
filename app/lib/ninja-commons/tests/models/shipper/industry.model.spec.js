(function test() {
  describe('industry.model', () => {
    const module = angular.mock.module;
    const inject = angular.mock.inject;

    beforeEach(module('nvCommons.services'));
    beforeEach(module('nvCommons.models'));

    describe('Industry.read', () => {
      let $httpBackend;
      let requestHandler;
      let $rootScope;

      beforeEach(inject(($injector, _$rootScope_) => {
        $httpBackend = $injector.get('$httpBackend');
        requestHandler = $httpBackend.whenGET('core/industries').respond([
          {
            "id": 1,
            "createdAt": "2015-04-09T05:18:38Z",
            "updatedAt": null,
            "deletedAt": null,
            "name": "Automobile Accessories"
          }
        ]);
        $rootScope = _$rootScope_;
      }));

      afterEach(() => {
        $httpBackend.verifyNoOutstandingExpectation();
        $httpBackend.verifyNoOutstandingRequest();
      });

      it('should call the right endpoint', inject((Industry) => {
        $httpBackend.expectGET('core/industries');
        Industry.read();
        $httpBackend.flush();
      }));

      it('should return resolved promise when succeed', inject((Industry) => {
        let result;
        Industry.read().then((value) => {
          result = value;
        });
        $httpBackend.flush();
        $rootScope.$apply();
        expect(result).toEqual([
          {
            "id": 1,
            "createdAt": "2015-04-09T05:18:38Z",
            "updatedAt": null,
            "deletedAt": null,
            "name": "Automobile Accessories"
          }
        ]);
      }));

      it('should reject resolved promise when fail', inject((Industry) => {
        requestHandler.respond(401, '');
        let failed = false;
        Industry.read().catch(() => {
          failed = true;
        });
        $httpBackend.flush();
        $rootScope.$apply();
        expect(failed).toBe(true);
      }));
    });
  });
}());
