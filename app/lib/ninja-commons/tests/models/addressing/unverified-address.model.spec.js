(function test() {
  describe('UnverifiedAddress.model', () => {
    const module = angular.mock.module;
    const inject = angular.mock.inject;

    beforeEach(module('pascalprecht.translate'));
    beforeEach(module('nvCommons.directives'));
    beforeEach(module('nvCommons.services'));
    beforeEach(module('nvCommons.models'));

    let $rootScope;
    let UnverifiedAddress;

    beforeEach(inject((_$rootScope_, _UnverifiedAddress_) => {
      $rootScope = _$rootScope_;
      UnverifiedAddress = _UnverifiedAddress_;
    }));

    describe('UnverifiedAddress.preAddressVerification()', () => {
      let $httpBackend;
      let requestHandler;

      beforeEach(inject(($injector) => {
        $httpBackend = $injector.get('$httpBackend');
        UnverifiedAddress.preAddressVerification(1, [10001]);
        requestHandler = $httpBackend.whenPOST('core/pre-address-verification/assign/zones/1').respond({
          country: 'sg',
        });
      }));

      afterEach(() => {
        $httpBackend.verifyNoOutstandingExpectation();
        $httpBackend.verifyNoOutstandingRequest();
      });

      it('should call the right endpoint', () => {
        $httpBackend.expectPOST('core/pre-address-verification/assign/zones/1', { waypoint_ids: [10001] });
        UnverifiedAddress.preAddressVerification(1, [10001]);
        $httpBackend.flush();
      });

      it('should return resolved promise when succeed', () => {
        let result;
        UnverifiedAddress.preAddressVerification(1, [10001]).then((value) => {
          result = value;
        });
        $httpBackend.flush();
        $rootScope.$apply();
        expect(result).toEqual({
          country: 'sg',
        });
      });

      it('should reject resolved promise when fail', () => {
        let failed = false;
        requestHandler.respond(401, '');
        UnverifiedAddress.preAddressVerification(1, [10001]).catch(() => {
          failed = true;
        });
        $httpBackend.flush();
        $rootScope.$apply();
        expect(failed).toBe(true);
      });
    });

    describe('UnverifiedAddress.search()', () => {
      let $httpBackend;
      let requestHandler;

      beforeEach(inject(($injector) => {
        $httpBackend = $injector.get('$httpBackend');
        UnverifiedAddress.search({ country: 'sg' });
        requestHandler = $httpBackend.whenPOST('core/pre-address-verification/search').respond({
          country: 'sg',
        });
      }));

      afterEach(() => {
        $httpBackend.verifyNoOutstandingExpectation();
        $httpBackend.verifyNoOutstandingRequest();
      });

      it('should call the right endpoint', () => {
        $httpBackend.expectPOST('core/pre-address-verification/search', { country: 'sg' });
        UnverifiedAddress.search({ country: 'sg' });
        $httpBackend.flush();
      });

      it('should return resolved promise when succeed', () => {
        let result;
        UnverifiedAddress.search({ country: 'sg' }).then((value) => {
          result = value;
        });
        $httpBackend.flush();
        $rootScope.$apply();
        expect(result).toEqual({
          country: 'sg',
        });
      });

      it('should reject resolved promise when fail', () => {
        let failed = false;
        requestHandler.respond(401, '');
        UnverifiedAddress.search({ country: 'sg' }).catch(() => {
          failed = true;
        });
        $httpBackend.flush();
        $rootScope.$apply();
        expect(failed).toBe(true);
      });
    });
  });
}());
