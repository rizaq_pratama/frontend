(function test() {
  describe('Addressing.model', () => {
    const module = angular.mock.module;
    const inject = angular.mock.inject;

    beforeEach(module('nvCommons.services'));
    beforeEach(module('nvCommons.models'));

    let $rootScope;
    let Addressing;

    beforeEach(inject((_$rootScope_, _Addressing_) => {
      $rootScope = _$rootScope_;
      Addressing = _Addressing_;
    }));

    describe('Addressing.createAddress()', () => {
      let $httpBackend;
      let requestHandler;

      beforeEach(inject(($injector) => {
        $httpBackend = $injector.get('$httpBackend');
        Addressing.createAddress({ country: 'sg' });
        requestHandler = $httpBackend.whenPOST('addressing/addresses').respond({
          country: 'sg',
        });
      }));

      afterEach(() => {
        $httpBackend.verifyNoOutstandingExpectation();
        $httpBackend.verifyNoOutstandingRequest();
      });

      it('should call the right endpoint', () => {
        $httpBackend.expectPOST('addressing/addresses', { country: 'sg' });
        Addressing.createAddress({ country: 'sg' });
        $httpBackend.flush();
      });

      it('should return resolved promise when succeed', () => {
        let result;
        Addressing.createAddress({ country: 'sg' }).then((value) => {
          result = value;
        });
        $httpBackend.flush();
        $rootScope.$apply();
        expect(result).toEqual({
          country: 'sg',
        });
      });

      it('should reject resolved promise when fail', () => {
        let failed = false;
        requestHandler.respond(401, '');
        Addressing.createAddress({ country: 'sg' }).catch(() => {
          failed = true;
        });
        $httpBackend.flush();
        $rootScope.$apply();
        expect(failed).toBe(true);
      });
    });

    describe('Addressing.updateAddress', () => {
      let $httpBackend;
      let requestHandler;

      beforeEach(inject((_$httpBackend_) => {
        $httpBackend = _$httpBackend_;
        requestHandler = $httpBackend.whenPUT('addressing/addresses/1').respond([{}]);
      }));

      afterEach(() => {
        $httpBackend.verifyNoOutstandingExpectation();
        $httpBackend.verifyNoOutstandingRequest();
      });

      it('should call the right api endpoint', () => {
        $httpBackend.expectPUT('addressing/addresses/1', { a: 1 });
        Addressing.updateAddress({ a: 1 }, 1);
        $httpBackend.flush();
      });

      it('should return resolved promise when succeed', () => {
        let result;
        Addressing.updateAddress({ a: 1 }, 1).then((value) => {
          result = value;
        });
        $httpBackend.flush();
        $rootScope.$apply();
        expect(result).toEqual([{}]);
      });

      it('should reject resolved promise when fail', () => {
        requestHandler.respond(401, '');
        let failed = false;
        Addressing.updateAddress({ a: 1 }, 1).catch(() => {
          failed = true;
        });
        $httpBackend.flush();
        $rootScope.$apply();
        expect(failed).toBe(true);
      });
    });

    describe('Addressing.deleteAddress', () => {
      let $httpBackend;
      let requestHandler;

      beforeEach(inject((_$httpBackend_) => {
        $httpBackend = _$httpBackend_;
        requestHandler = $httpBackend.whenDELETE('addressing/addresses/sg/1').respond([{}]);
      }));

      afterEach(() => {
        $httpBackend.verifyNoOutstandingExpectation();
        $httpBackend.verifyNoOutstandingRequest();
      });

      it('should call the right api endpoint', () => {
        $httpBackend.expectDELETE('addressing/addresses/sg/1');
        Addressing.deleteAddress('sg', 1);
        $httpBackend.flush();
      });

      it('should return resolved promise when succeed', () => {
        let result;
        Addressing.deleteAddress('sg', 1).then((value) => {
          result = value;
        });
        $httpBackend.flush();
        $rootScope.$apply();
        expect(result).toEqual([{}]);
      });

      it('should reject resolved promise when fail', () => {
        requestHandler.respond(401, '');
        let failed = false;
        Addressing.deleteAddress('sg', 1).catch(() => {
          failed = true;
        });
        $httpBackend.flush();
        $rootScope.$apply();
        expect(failed).toBe(true);
      });
    });

    describe('Addressing.searchAddress', () => {
      let $httpBackend;
      let requestHandler;

      beforeEach(inject((_$httpBackend_) => {
        $httpBackend = _$httpBackend_;
        requestHandler = $httpBackend.whenGET('addressing/2.0/search/test&country=sg').respond([{}]);
      }));

      afterEach(() => {
        $httpBackend.verifyNoOutstandingExpectation();
        $httpBackend.verifyNoOutstandingRequest();
      });

      it('should call the right api endpoint', () => {
        $httpBackend.expectGET('addressing/2.0/search/test&country=sg');
        Addressing.searchAddress('sg', 'test');
        $httpBackend.flush();
      });

      it('should return resolved promise when succeed', () => {
        let result;
        Addressing.searchAddress('sg', 'test').then((value) => {
          result = value;
        });
        $httpBackend.flush();
        $rootScope.$apply();
        expect(result).toEqual([{}]);
      });

      it('should reject resolved promise when fail', () => {
        requestHandler.respond(401, '');
        let failed = false;
        Addressing.searchAddress('sg', 'test').catch(() => {
          failed = true;
        });
        $httpBackend.flush();
        $rootScope.$apply();
        expect(failed).toBe(true);
      });
    });

    describe('Addressing.filterSearch', () => {
      let $httpBackend;
      let requestHandler;

      beforeEach(inject((_$httpBackend_) => {
        $httpBackend = _$httpBackend_;
        requestHandler = $httpBackend.whenGET('addressing/2.0/filter-search?query=test&country=sg').respond([{}]);
      }));

      afterEach(() => {
        $httpBackend.verifyNoOutstandingExpectation();
        $httpBackend.verifyNoOutstandingRequest();
      });

      it('should call the right api endpoint', () => {
        $httpBackend.expectGET('addressing/2.0/filter-search?query=test&country=sg');
        Addressing.filterSearch('sg', 'test');
        $httpBackend.flush();
      });

      it('should return resolved promise when succeed', () => {
        let result;
        Addressing.filterSearch('sg', 'test').then((value) => {
          result = value;
        });
        $httpBackend.flush();
        $rootScope.$apply();
        expect(result).toEqual([{}]);
      });

      it('should reject resolved promise when fail', () => {
        requestHandler.respond(401, '');
        let failed = false;
        Addressing.filterSearch('sg', 'test').catch(() => {
          failed = true;
        });
        $httpBackend.flush();
        $rootScope.$apply();
        expect(failed).toBe(true);
      });
    });
  });
}());
