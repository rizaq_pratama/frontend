(function test() {
  describe('third-party-order.model', () => {
    const module = angular.mock.module;
    const inject = angular.mock.inject;
    let ThirdPartyOrder;
    let Order;
    let $rootScope;

    beforeEach(module('pascalprecht.translate'));
    beforeEach(module('nvCommons.services'));
    beforeEach(module('nvCommons.models'));

    beforeEach(inject((_ThirdPartyOrder_, _Order_, _$rootScope_) => {
      ThirdPartyOrder = _ThirdPartyOrder_;
      Order = _Order_;
      $rootScope = _$rootScope_;
      $rootScope.user = {
        timezone: 'Asia/Singapore',
      };
    }));

    describe('ThirdPartyOrder.read()', () => {
      let $httpBackend;
      let requestHandler;

      beforeEach(inject(($injector) => {
        $httpBackend = $injector.get('$httpBackend');
        requestHandler = $httpBackend.whenGET(/^core\/thirdpartyorders$/).respond([{
          id: 1,
          thirdPartyOrders: [
            { id: 3, thirdPartyTrackingId: 'asdf' }
          ],
        }]);
      }));

      afterEach(() => {
        $httpBackend.verifyNoOutstandingExpectation();
        $httpBackend.verifyNoOutstandingRequest();
      });

      it('should call the right endpoint', () => {
        $httpBackend.expectGET('core/thirdpartyorders');
        ThirdPartyOrder.read();
        $httpBackend.flush();
      });

      it('should return resolved promise when succeed', () => {
        let result;
        ThirdPartyOrder.read().then((value) => {
          result = value;
        });
        $httpBackend.flush();
        $rootScope.$apply();
        expect(result).toEqual(jasmine.any(Array))
        expect(result[0]).toEqual(
          jasmine.objectContaining({
            id: 1,
            thirdPartyOrder: jasmine.objectContaining({
              id: 3, 
              thirdPartyTrackingId: 'asdf',
              _momentOnBoardedDateTime: jasmine.any(moment),
              daySinceTransferred: jasmine.any(Number),
            })
          })
        );
      });

      it('should reject resolved promise when fail', () => {
        let failed = false;
        requestHandler.respond(401, '');
        ThirdPartyOrder.read().catch(() => {
          failed = true;
        });
        $httpBackend.flush();
        $rootScope.$apply();
        expect(failed).toBe(true);
      });
    });

    describe('ThirdPartyOrder.setThirdPartyOptions()', () => {
      it('should set shippers', () => {
        ThirdPartyOrder.setThirdPartyOptions([{id: 4, name: 'shipper 4'}, {id: 5, name: 'shipper 5'}]);
        expect(ThirdPartyOrder.FIELDS.thirdPartyOrder.thirdPartyShipper.id.options).toEqual([
          {displayName: 'shipper 4', value: 4},
          {displayName: 'shipper 5', value: 5},
        ]);
        expect(ThirdPartyOrder.FIELDS.thirdPartyOrder.thirdPartyShipper.id.optionsMap).toEqual({
          4: {displayName: 'shipper 4', value: 4},
          5: {displayName: 'shipper 5', value: 5},
        });
      });
    });

    describe('ThirdPartyOrder.uploadCSV()', () => {
      let $httpBackend;
      let requestHandler;
      let $http;

      beforeEach(inject(($injector, _$http_) => {
        $http = _$http_;
        $httpBackend = $injector.get('$httpBackend');
        ThirdPartyOrder.setThirdPartyOptions([{id: 4, name: 'shipper 4'}, {id: 5, name: 'shipper 5'}]);
        requestHandler = $httpBackend.whenPOST(/^core\/thirdpartyorders$/).respond([{
          orderId: 1,
          orderStatus: 'Pending',
          status: 'Success',
          trackingId: 'NINJA_TRACKING_ID',
          thirdPartyOrderId: 3,
          thirdPartyTrackingId: 'THIRD_PARTY_TRACKING_ID',
          onBoardedDateTime: new Date(),
          thirdPartyShipperId: 3,
        }]);
        $http.upload = (url, data) => $http.post(url, data);
        spyOn($http, 'upload').and.callThrough();
      }));

      afterEach(() => {
        $httpBackend.verifyNoOutstandingExpectation();
        $httpBackend.verifyNoOutstandingRequest();
      });

      it('should call the right endpoint', () => {
        ThirdPartyOrder.uploadCSV({ id: 1, trackingId: 'test' });
        expect($http.upload).toHaveBeenCalledWith('core/thirdpartyorders', { id: 1, trackingId: 'test' });
        $httpBackend.flush();
      });

      it('should return resolved promise when succeed', () => {
        let result;
        ThirdPartyOrder.uploadCSV({ id: 1, trackingId: 'test' }).then((value) => {
          result = value;
        });
        $httpBackend.flush();
        $rootScope.$apply();
        expect(result).toEqual(jasmine.any(Array))
        expect(result[0]).toEqual(
          {
            id: 1,
            status: 'Pending',
            trackingId: 'NINJA_TRACKING_ID',
            thirdPartyOrder: {
              id: 3,
              thirdPartyTrackingId: 'THIRD_PARTY_TRACKING_ID',
              onBoardedDateTime: jasmine.any(Date),
              _momentOnBoardedDateTime: jasmine.any(moment),
              daySinceTransferred: jasmine.any(Number),
              thirdPartyShipper: {
                id: 3,
                name: undefined,
              },
            },
            resultStatus: 'Success',
          }
        );
      });

      it('should reject resolved promise when fail', () => {
        let failed = false;
        let value = null;
        requestHandler.respond(401, '');
        ThirdPartyOrder.uploadCSV({ id: 1, trackingId: 'test' }).then((data) => {
          value = data;
        }, () => {
          failed = true;
        });
        $httpBackend.flush();
        $rootScope.$apply();
        expect(failed).toBe(false);
        expect(value).toBeUndefined();
      });
    });

    describe('ThirdPartyOrder.uploadSingle()', () => {
      let $httpBackend;
      let requestHandler;

      beforeEach(inject(($injector) => {
        $httpBackend = $injector.get('$httpBackend');
        ThirdPartyOrder.setThirdPartyOptions([{id: 4, name: 'shipper 4'}, {id: 5, name: 'shipper 5'}]);
        requestHandler = $httpBackend.whenPOST(/^core\/thirdpartyorders$/).respond([{
          orderId: 1,
          orderStatus: 'Pending',
          status: 'Success',
          trackingId: 'NINJA_TRACKING_ID',
          thirdPartyOrderId: 3,
          thirdPartyTrackingId: 'THIRD_PARTY_TRACKING_ID',
          onBoardedDateTime: new Date(),
          thirdPartyShipperId: 5,
        }]);
      }));

      afterEach(() => {
        $httpBackend.verifyNoOutstandingExpectation();
        $httpBackend.verifyNoOutstandingRequest();
      });

      it('should call the right endpoint', () => {
        $httpBackend.expectPOST('core/thirdpartyorders', {id: 1, trackingId: 'test'});
        ThirdPartyOrder.uploadSingle({ id: 1, trackingId: 'test' });
        $httpBackend.flush();
      });

      it('should return resolved promise when succeed', () => {
        let result;
        ThirdPartyOrder.uploadSingle({ id: 1, trackingId: 'test' }).then((value) => {
          result = value;
        });
        $httpBackend.flush();
        $rootScope.$apply();
        expect(result).toEqual(jasmine.any(Array))
        expect(result[0]).toEqual(
          {
            id: 1,
            status: 'Pending',
            trackingId: 'NINJA_TRACKING_ID',
            thirdPartyOrder: {
              id: 3,
              thirdPartyTrackingId: 'THIRD_PARTY_TRACKING_ID',
              onBoardedDateTime: jasmine.any(Date),
              _momentOnBoardedDateTime: jasmine.any(moment),
              daySinceTransferred: jasmine.any(Number),
              thirdPartyShipper: {
                id: 5,
                name: 'shipper 5',
              },
            },
            resultStatus: 'Success',
          }
        );
      });

      it('should reject resolved promise when fail', () => {
        let failed = false;
        requestHandler.respond(401, '');
        ThirdPartyOrder.uploadSingle({ id: 1, trackingId: 'test' }).catch(() => {
          failed = true;
        });
        $httpBackend.flush();
        $rootScope.$apply();
        expect(failed).toBe(true);
      });
    });

    describe('ThirdPartyOrder.update()', () => {
      let $httpBackend;
      let requestHandler;

      beforeEach(inject(($injector) => {
        $httpBackend = $injector.get('$httpBackend');
        ThirdPartyOrder.setThirdPartyOptions([{id: 4, name: 'shipper 4'}, {id: 5, name: 'shipper 5'}]);
        requestHandler = $httpBackend.whenPUT(/^core\/thirdpartyorders$/).respond([{
          orderId: 1,
          orderStatus: 'Pending',
          status: 'Success',
          trackingId: 'NINJA_TRACKING_ID',
          thirdPartyOrderId: 3,
          thirdPartyTrackingId: 'THIRD_PARTY_TRACKING_ID',
          onBoardedDateTime: new Date(),
          thirdPartyShipperId: 5,
        }]);
      }));

      afterEach(() => {
        $httpBackend.verifyNoOutstandingExpectation();
        $httpBackend.verifyNoOutstandingRequest();
      });

      it('should call the right endpoint', () => {
        $httpBackend.expectPUT('core/thirdpartyorders', {id: 1, trackingId: 'test'});
        ThirdPartyOrder.update({ id: 1, trackingId: 'test' });
        $httpBackend.flush();
      });

      it('should return resolved promise when succeed', () => {
        let result;
        ThirdPartyOrder.update({ id: 1, trackingId: 'test' }).then((value) => {
          result = value;
        });
        $httpBackend.flush();
        $rootScope.$apply();
        expect(result).toEqual(jasmine.any(Array))
        expect(result[0]).toEqual(
          {
            id: 1,
            status: 'Pending',
            trackingId: 'NINJA_TRACKING_ID',
            thirdPartyOrder: {
              id: 3,
              thirdPartyTrackingId: 'THIRD_PARTY_TRACKING_ID',
              onBoardedDateTime: jasmine.any(Date),
              _momentOnBoardedDateTime: jasmine.any(moment),
              daySinceTransferred: jasmine.any(Number),
              thirdPartyShipper: {
                id: 5,
                name: 'shipper 5',
              },
            },
            resultStatus: 'Success',
          }
        );
      });

      it('should reject resolved promise when fail', () => {
        let failed = false;
        requestHandler.respond(401, '');
        ThirdPartyOrder.update({ id: 1, trackingId: 'test' }).catch(() => {
          failed = true;
        });
        $httpBackend.flush();
        $rootScope.$apply();
        expect(failed).toBe(true);
      });
    });

    describe('ThirdPartyOrder.untag()', () => {
      let $httpBackend;
      let requestHandler;

      beforeEach(inject(($injector) => {
        $httpBackend = $injector.get('$httpBackend');
        ThirdPartyOrder.setThirdPartyOptions([{id: 4, name: 'shipper 4'}, {id: 5, name: 'shipper 5'}]);
        requestHandler = $httpBackend.whenDELETE(/^core\/thirdpartyorders$/).respond([{
          orderId: 1,
          orderStatus: 'Pending',
          status: 'Success',
          trackingId: 'NINJA_TRACKING_ID',
          thirdPartyOrderId: 3,
          thirdPartyTrackingId: 'THIRD_PARTY_TRACKING_ID',
          onBoardedDateTime: new Date(),
          thirdPartyShipperId: 5,
        }]);
      }));

      afterEach(() => {
        $httpBackend.verifyNoOutstandingExpectation();
        $httpBackend.verifyNoOutstandingRequest();
      });

      it('should call the right endpoint', () => {
        $httpBackend.expect('DELETE', 
          'core/thirdpartyorders', 
          {id: 1, trackingId: 'test'}, 
          {'Content-Type':'application/json; charset=UTF-8', 'Accept':'application/json, text/plain, */*'});
        ThirdPartyOrder.untag({ id: 1, trackingId: 'test' });
        $httpBackend.flush();
      });

      it('should return resolved promise when succeed', () => {
        let result;
        ThirdPartyOrder.untag({ id: 1, trackingId: 'test' }).then((value) => {
          result = value;
        });
        $httpBackend.flush();
        $rootScope.$apply();
        expect(result).toEqual(jasmine.any(Array))
        expect(result[0]).toEqual(
          {
            id: 1,
            status: 'Pending',
            trackingId: 'NINJA_TRACKING_ID',
            thirdPartyOrder: {
              id: 3,
              thirdPartyTrackingId: 'THIRD_PARTY_TRACKING_ID',
              onBoardedDateTime: jasmine.any(Date),
              _momentOnBoardedDateTime: jasmine.any(moment),
              daySinceTransferred: jasmine.any(Number),
              thirdPartyShipper: {
                id: 5,
                name: 'shipper 5',
              },
            },
            resultStatus: 'Success',
          }
        );
      });

      it('should reject resolved promise when fail', () => {
        let failed = false;
        requestHandler.respond(401, '');
        ThirdPartyOrder.untag({ id: 1, trackingId: 'test' }).catch(() => {
          failed = true;
        });
        $httpBackend.flush();
        $rootScope.$apply();
        expect(failed).toBe(true);
      });
    });

    describe('ThirdPartyOrder.getAddFields()', () => {
      it('should return the fields needed', () => {
        expect(ThirdPartyOrder.getAddFields()).toEqual(jasmine.objectContaining({
          trackingId: jasmine.any(Object),
          thirdPartyOrder: jasmine.any(Object),
        }));
      });
    });

    describe('ThirdPartyOrder.getEditFields()', () => {
      it('should return the fields needed', () => {
        expect(ThirdPartyOrder.getEditFields()).toEqual(jasmine.objectContaining({
          id: jasmine.any(Object),
          trackingId: jasmine.any(Object),
          thirdPartyOrder: jasmine.any(Object),
        }));
      });
    });
  });
}());
