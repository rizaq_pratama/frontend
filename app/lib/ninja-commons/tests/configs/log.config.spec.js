(function() {
    'use strict';

    describe('log.config', function() {

        var module = angular.mock.module;
        var inject = angular.mock.inject;

        beforeEach(module('nvCommons.configs'));
        beforeEach(module('nvCommons.services'));

        describe('called nvToast', function(){
            var $log, nvToast;

            beforeEach(inject(function(_nvToast_){
                nvToast = _nvToast_;

                spyOn(nvToast, 'error');
                spyOn(nvToast, 'warning');
                spyOn(nvToast, 'info');
                spyOn(nvToast, 'success');
            }));

            beforeEach(inject(function(_$log_){
                $log = _$log_;
                $log.reset();
            }));

            describe('$log.log', function(){
                it('should call nvToast.success for log', function(){
                    $log.log('message');
                    expect(nvToast.success).toHaveBeenCalledWith('message', []);
                });

                it('should handle variable length argument', function(){
                    $log.log('message', 'ninjavan');
                    expect(nvToast.success).toHaveBeenCalledWith('message', ['ninjavan']);
                    $log.log('message', 'ninjavan', 'marcella');
                    expect(nvToast.success).toHaveBeenCalledWith('message', ['ninjavan', 'marcella']);
                    $log.log('message', 'ninjavan', 'marcella', 'operatorv2');
                    expect(nvToast.success).toHaveBeenCalledWith('message', ['ninjavan', 'marcella', 'operatorv2']);
                });

                it('should handle error as argument', function(){
                    // in PhantomJS, need to throw error in order to stack
                    try{
                        throw new Error('error2');
                    }catch(error2){
                        $log.log(error2);
                        expect(nvToast.success).toHaveBeenCalledWith(error2.message, error2.stack.split('\n'));
                    }
                });
            });

            describe('$log.info', function(){
                it('should call nvToast.info for info', function(){
                    $log.info('message');
                    expect(nvToast.info).toHaveBeenCalledWith('message', []);
                });

                it('should handle variable length argument', function(){
                    $log.info('message', 'ninjavan');
                    expect(nvToast.info).toHaveBeenCalledWith('message', ['ninjavan']);
                    $log.info('message', 'ninjavan', 'marcella');
                    expect(nvToast.info).toHaveBeenCalledWith('message', ['ninjavan', 'marcella']);
                    $log.info('message', 'ninjavan', 'marcella', 'operatorv2');
                    expect(nvToast.info).toHaveBeenCalledWith('message', ['ninjavan', 'marcella', 'operatorv2']);
                });

                it('should handle error as argument', function(){
                    // in PhantomJS, need to throw error in order to stack
                    try{
                        throw new Error('error2');
                    }catch(error2){
                        $log.info(error2);
                        expect(nvToast.info).toHaveBeenCalledWith(error2.message, error2.stack.split('\n'));
                    }
                });
            });

            describe('$log.warn', function(){
                it('should call nvToast.warning for warning', function(){
                    $log.warn('message');
                    expect(nvToast.warning).toHaveBeenCalledWith('message', []);
                });

                it('should handle variable length argument', function(){
                    $log.warn('message', 'ninjavan');
                    expect(nvToast.warning).toHaveBeenCalledWith('message', ['ninjavan']);
                    $log.warn('message', 'ninjavan', 'marcella');
                    expect(nvToast.warning).toHaveBeenCalledWith('message', ['ninjavan', 'marcella']);
                    $log.warn('message', 'ninjavan', 'marcella', 'operatorv2');
                    expect(nvToast.warning).toHaveBeenCalledWith('message', ['ninjavan', 'marcella', 'operatorv2']);
                });

                it('should handle error as argument', function(){
                    // in PhantomJS, need to throw error in order to stack
                    try{
                        throw new Error('error2');
                    }catch(error2){
                        $log.warn(error2);
                        expect(nvToast.warning).toHaveBeenCalledWith(error2.message, error2.stack.split('\n'));
                    }
                });
            });

            describe('$log.error', function(){
                it('should call nvToast.error for error', function(){
                    $log.error('message');
                    expect(nvToast.error).toHaveBeenCalledWith('message', []);
                });

                it('should handle variable length argument', function(){
                    $log.error('message', 'ninjavan');
                    expect(nvToast.error).toHaveBeenCalledWith('message', ['ninjavan']);
                    $log.error('message', 'ninjavan', 'marcella');
                    expect(nvToast.error).toHaveBeenCalledWith('message', ['ninjavan', 'marcella']);
                    $log.error('message', 'ninjavan', 'marcella', 'operatorv2');
                    expect(nvToast.error).toHaveBeenCalledWith('message', ['ninjavan', 'marcella', 'operatorv2']);
                });

                it('should handle error as argument', function(){
                    // in PhantomJS, need to throw error in order to stack
                    try{
                        throw new Error('error2');
                    }catch(error2){
                        $log.error(error2);
                        expect(nvToast.error).toHaveBeenCalledWith(error2.message, error2.stack.split('\n'));
                    }
                });
            });
        });
    });

})();