(function test() {
  describe('currency.config', () => {
    const module = angular.mock.module;
    const inject = angular.mock.inject;

    let currency = '$';
    const code = 'SGD';

    beforeEach(module('nvCommons.configs'));
    beforeEach(module('nvCommons.services', ($provide) => {
      $provide.value('nvCurrency', { getSymbol: () => currency, getCode: () => code });
    }));

    it('uses nvCurrency for currency when currencySymbol is not specified', inject(($filter) => {
      const currencyFilter = $filter('currency');
      expect(currencyFilter(50)).toEqual('$50.00');
      currency = 'NinjaVan';
      expect(currencyFilter(50)).toEqual('NinjaVan50.00');
    }));

    it('uses the specified currency', inject(($filter, nvCurrency) => {
      const currencyFilter = $filter('currency');
      expect(nvCurrency.getSymbol()).not.toEqual('yo');
      expect(currencyFilter(50, 'yo')).toEqual('yo50.00');
    }));
  });
}());
