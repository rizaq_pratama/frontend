(function test() {
  describe('dom-manipulation.service', () => {
    const module = angular.mock.module;
    const inject = angular.mock.inject;

    let nvDOMManipulation;

    beforeEach(module('nvCommons.services'));
    beforeEach(inject((_nvDOMManipulation_) => {
      nvDOMManipulation = _nvDOMManipulation_;
    }));

    describe('nvDOMManipulation.moveClassesToFirstChild', () => {
      let element;

      beforeEach(() => {
        element = angular.element('<div class="a b ng-isolate-scope"><first class="c"></first><second class="d"></second><third></thrid></div>');
      });

      it('should move to first child only', () => {
        nvDOMManipulation.moveClassesToFirstChild(element[0]);
        expect(element.hasClass('ng-isolate-scope')).toBe(true);
        expect(element.hasClass('a')).toBe(false);
        expect(element.hasClass('b')).toBe(false);
        expect(element.find('first').hasClass('a')).toBe(true);
        expect(element.find('first').hasClass('b')).toBe(true);
        expect(element.find('first').hasClass('c')).toBe(true);
        expect(element.find('first').hasClass('ng-isolate-scope')).toBe(false);
        expect(element.find('second').hasClass('a')).toBe(false);
        expect(element.find('second').hasClass('b')).toBe(false);
        expect(element.find('second').hasClass('c')).toBe(false);
        expect(element.find('third').hasClass('a')).toBe(false);
      });
      it('should move to all child', () => {
        nvDOMManipulation.moveClassesToFirstChild(element[0], true);
        expect(element.hasClass('ng-isolate-scope')).toBe(true);
        expect(element.hasClass('a')).toBe(false);
        expect(element.hasClass('b')).toBe(false);
        expect(element.find('first').hasClass('a')).toBe(true);
        expect(element.find('first').hasClass('b')).toBe(true);
        expect(element.find('first').hasClass('c')).toBe(true);
        expect(element.find('first').hasClass('ng-isolate-scope')).toBe(false);
        expect(element.find('second').hasClass('a')).toBe(true);
        expect(element.find('second').hasClass('b')).toBe(true);
        expect(element.find('second').hasClass('d')).toBe(true);
        expect(element.find('second').hasClass('ng-isolate-scope')).toBe(false);
        expect(element.find('third').hasClass('a')).toBe(true);
        expect(element.find('third').hasClass('b')).toBe(true);
        expect(element.find('third').hasClass('ng-isolate-scope')).toBe(false);
      });
    });

    describe('nvDOMManipulation.removeAttrFromElement', () => {
      let element;

      beforeEach(() => {
        element = angular.element('<div ng-click="doSomething()" model="something"></div>');
      });

      it('should remove attribute', () => {
        expect(element.attr('ng-click')).toEqual('doSomething()');
        nvDOMManipulation.removeAttrFromElement(element[0], 'ng-click');
        expect(element.attr('ng-click')).toBeUndefined();
      });

      it('should not throw', () => {
        expect(mightThrow).not.toThrow();
        expect(element.attr('ng-click')).not.toBeUndefined();
        expect(element.attr('model')).not.toBeUndefined();

        function mightThrow() {
          nvDOMManipulation.removeAttrFromElement(element[0], 'lala');
        }
      });
    });

    describe('nvDOMManipulation.reloadFavicon', () => {
      let $document;
      let nvEnv;
      let env;
      let head;

      beforeEach(inject((_$document_, _nvEnv_) => {
        $document = _$document_;
        nvEnv = _nvEnv_;
        env = nv.config.env;
        head = $document[0].head.innerHTML;
        $document[0].head.innerHTML = `<link rel="icon" href="assets/ico/dev/favicon-dev.002.png" sizes="32x32">
                                            <link rel="shortcut icon" href="assets/ico/dev/favicon-dev.002.png" sizes="32x32">
                                            <link rel="apple-touch-icon-precomposed" href="assets/ico/dev/favicon-dev.002.png" sizes="32x32">
                                            <link rel="something else" href="assets/ico/dev/favicon-dev.002.png" sizes="32x32">`;
      }));

      afterEach(() => {
        nv.config.env = env;
        $document[0].head.innerHTML = head;
      });

      it('should replace icon domain', () => {
        nv.config.env = 'prod';
        nvDOMManipulation.reloadFavicon('sg');
        expect(angular.element($document[0].head.querySelectorAll('[rel="icon"]')).attr('href')).toEqual('assets/ico/sg/favicon-dev.002.png');
        expect(angular.element($document[0].head.querySelectorAll('[rel="shortcut icon"]')).attr('href')).toEqual('assets/ico/sg/favicon-dev.002.png');
        expect(angular.element($document[0].head.querySelectorAll('[rel="apple-touch-icon-precomposed"]')).attr('href')).toEqual('assets/ico/sg/favicon-dev.002.png');
        expect(angular.element($document[0].head.querySelectorAll('[rel="something else"]')).attr('href')).toEqual('assets/ico/dev/favicon-dev.002.png');
      });

      it('should not replace icon domain', () => {
        nv.config.env = 'dev';
        nvDOMManipulation.reloadFavicon('vn');
        expect(angular.element($document[0].head.querySelectorAll('[rel="icon"]')).attr('href')).toEqual('assets/ico/dev/favicon-dev.002.png');
        expect(angular.element($document[0].head.querySelectorAll('[rel="shortcut icon"]')).attr('href')).toEqual('assets/ico/dev/favicon-dev.002.png');
        expect(angular.element($document[0].head.querySelectorAll('[rel="apple-touch-icon-precomposed"]')).attr('href')).toEqual('assets/ico/dev/favicon-dev.002.png');
        expect(angular.element($document[0].head.querySelectorAll('[rel="something else"]')).attr('href')).toEqual('assets/ico/dev/favicon-dev.002.png');
      });

      it('should not replace icon domain', () => {
        nv.config.env = 'saas';
        nvDOMManipulation.reloadFavicon('id');
        expect(angular.element($document[0].head.querySelectorAll('[rel="icon"]')).attr('href')).toEqual('assets/ico/dev/favicon-dev.002.png');
        expect(angular.element($document[0].head.querySelectorAll('[rel="shortcut icon"]')).attr('href')).toEqual('assets/ico/dev/favicon-dev.002.png');
        expect(angular.element($document[0].head.querySelectorAll('[rel="apple-touch-icon-precomposed"]')).attr('href')).toEqual('assets/ico/dev/favicon-dev.002.png');
        expect(angular.element($document[0].head.querySelectorAll('[rel="something else"]')).attr('href')).toEqual('assets/ico/dev/favicon-dev.002.png');
      });
    });
  });
}());
