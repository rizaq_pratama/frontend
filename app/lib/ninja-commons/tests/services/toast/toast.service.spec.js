(function test() {
  describe('toast.service', () => {
    const module = angular.mock.module;
    const inject = angular.mock.inject;

    beforeEach(module('nvCommons.services'));

    describe('handles when window.toastr is not available', () => {
      let toast;
      beforeEach(inject(($window) => {
        toast = $window.toastr;
        $window.toastr = undefined;
        spyOn(console, 'error');
      }));

      afterEach(inject(($window) => {
        $window.toastr = toast;
      }));

      it('should show error in console', () => {
        expect(injectNvToast).toThrow();
        expect(console.error).toHaveBeenCalledWith('toast.service.js', 'toastr is not defined, have you included the lib files?');

        function injectNvToast() {
          inject((nvToast) => {});
        }
      });
    });

    describe('use toastr correctly', () => {
      beforeEach(inject(($window) => {
        spyOn($window.toastr, 'clear');
        spyOn($window.toastr, 'remove');
        spyOn($window.toastr, 'error');
        spyOn($window.toastr, 'info');
        spyOn($window.toastr, 'success');
        spyOn($window.toastr, 'warning');
      }));

      it('should set the correct default toastr options', inject(($window, nvToast) => {
        expect($window.toastr.options.timeOut).toBe(5000);
        expect($window.toastr.options.extendedTimeOut).toBe(5000);
        expect($window.toastr.options.progressBar).toBe(true);
      }));

      it('should call toastr.clear', inject(($window, nvToast) => {
        nvToast.clear();
        expect($window.toastr.clear).toHaveBeenCalled();
      }));

      it('should call toastr.remove', inject(($window, nvToast) => {
        nvToast.remove();
        expect($window.toastr.remove).toHaveBeenCalled();
      }));

      it('should call toastr.error', inject(($window, nvToast) => {
        nvToast.error();
        expect($window.toastr.error).toHaveBeenCalled();
      }));

      it('should call toastr.info', inject(($window, nvToast) => {
        nvToast.info();
        expect($window.toastr.info).toHaveBeenCalled();
      }));

      it('should call toastr.success', inject(($window, nvToast) => {
        nvToast.success();
        expect($window.toastr.success).toHaveBeenCalled();
      }));

      it('should call toastr.warning', inject(($window, nvToast) => {
        nvToast.warning();
        expect($window.toastr.warning).toHaveBeenCalled();
      }));
    });

    describe('correctly interpret parameters', () => {
      beforeEach(inject(($window, nvToast) => {
        spyOn($window.toastr, 'error');
        spyOn($window.toastr, 'info');
        spyOn($window.toastr, 'success');
        spyOn($window.toastr, 'warning');
      }));

      it('should be able to called with just the title', inject(($window, nvToast) => {
        const expectedTitle = {
          error: '<i class="material-icons">error</i>' +
                           '<div class="toast-right">' +
                             '<div class="toast-top">' +
                               '<div class="">TOAST_TITLE</div>' +
                               '<button class="nv-button md-button p dark flat dense md-nvYellow-theme"><i class="material-icons">close</i></button>' +
                             '</div>' +
                           '</div>',
          info: '<i class="material-icons">info</i>' +
                           '<div class="toast-right">' +
                             '<div class="toast-top">' +
                               '<div class="">TOAST_TITLE</div>' +
                               '<button class="nv-button md-button p dark flat dense md-nvBlue-theme"><i class="material-icons">close</i></button>' +
                             '</div>' +
                           '</div>',
          success: '<i class="material-icons">done</i>' +
                          '<div class="toast-right">' +
                            '<div class="toast-top">' +
                              '<div class="">TOAST_TITLE</div>' +
                              '<button class="nv-button md-button p dark flat dense md-nvGreen-theme"><i class="material-icons">close</i></button>' +
                            '</div>' +
                          '</div>',
          warning: '<i class="material-icons">warning</i>' +
                          '<div class="toast-right">' +
                            '<div class="toast-top">' +
                              '<div class="">TOAST_TITLE</div>' +
                              '<button class="nv-button md-button p dark flat dense md-nvRed-theme"><i class="material-icons">close</i></button>' +
                            '</div>' +
                          '</div>',
        };

        nvToast.error('TOAST_TITLE');
        expect($window.toastr.error).toHaveBeenCalledWith(expectedTitle.error, '', {});

        nvToast.info('TOAST_TITLE');
        expect($window.toastr.info).toHaveBeenCalledWith(expectedTitle.info, '', {});

        nvToast.success('TOAST_TITLE');
        expect($window.toastr.success).toHaveBeenCalledWith(expectedTitle.success, '', {});

        nvToast.warning('TOAST_TITLE');
        expect($window.toastr.warning).toHaveBeenCalledWith(expectedTitle.warning, '', {});
      }));

      it('should be able to called with the title and message', inject(($window, nvToast) => {
        const expectedTitle = {
          error: '<i class="material-icons">error</i>' +
                           '<div class="toast-right">' +
                             '<div class="toast-top">' +
                               '<div class="">TOAST_TITLE</div>' +
                               '<button class="nv-button md-button p dark flat dense md-nvYellow-theme"><i class="material-icons">close</i></button>' +
                             '</div>' +
                            '<div class="toast-bottom">TOAST_MESSAGE</div>' +
                           '</div>',
          info: '<i class="material-icons">info</i>' +
                           '<div class="toast-right">' +
                             '<div class="toast-top">' +
                               '<div class="">TOAST_TITLE</div>' +
                               '<button class="nv-button md-button p dark flat dense md-nvBlue-theme"><i class="material-icons">close</i></button>' +
                             '</div>' +
                            '<div class="toast-bottom">TOAST_MESSAGE</div>' +
                           '</div>',
          success: '<i class="material-icons">done</i>' +
                          '<div class="toast-right">' +
                            '<div class="toast-top">' +
                              '<div class="">TOAST_TITLE</div>' +
                              '<button class="nv-button md-button p dark flat dense md-nvGreen-theme"><i class="material-icons">close</i></button>' +
                            '</div>' +
                            '<div class="toast-bottom">TOAST_MESSAGE</div>' +
                          '</div>',
          warning: '<i class="material-icons">warning</i>' +
                          '<div class="toast-right">' +
                            '<div class="toast-top">' +
                              '<div class="">TOAST_TITLE</div>' +
                              '<button class="nv-button md-button p dark flat dense md-nvRed-theme"><i class="material-icons">close</i></button>' +
                            '</div>' +
                            '<div class="toast-bottom">TOAST_MESSAGE</div>' +
                          '</div>',
        };

        nvToast.error('TOAST_TITLE', 'TOAST_MESSAGE');
        expect($window.toastr.error).toHaveBeenCalledWith(expectedTitle.error, '', {});

        nvToast.info('TOAST_TITLE', 'TOAST_MESSAGE');
        expect($window.toastr.info).toHaveBeenCalledWith(expectedTitle.info, '', {});

        nvToast.success('TOAST_TITLE', 'TOAST_MESSAGE');
        expect($window.toastr.success).toHaveBeenCalledWith(expectedTitle.success, '', {});

        nvToast.warning('TOAST_TITLE', 'TOAST_MESSAGE');
        expect($window.toastr.warning).toHaveBeenCalledWith(expectedTitle.warning, '', {});
      }));

      it('should be able to called with the title and message array', inject(($window, nvToast) => {
        const expectedTitle = {
          error: '<i class="material-icons">error</i>' +
                           '<div class="toast-right">' +
                             '<div class="toast-top">' +
                               '<div class="">TOAST_TITLE</div>' +
                               '<button class="nv-button md-button p dark flat dense md-nvYellow-theme"><i class="material-icons">close</i></button>' +
                             '</div>' +
                            '<div class="toast-bottom">TOAST_MESSAGE_1<br />TOAST_MESSAGE_2<br />TOAST MESSAGE 3</div>' +
                           '</div>',
          info: '<i class="material-icons">info</i>' +
                           '<div class="toast-right">' +
                             '<div class="toast-top">' +
                               '<div class="">TOAST_TITLE</div>' +
                               '<button class="nv-button md-button p dark flat dense md-nvBlue-theme"><i class="material-icons">close</i></button>' +
                             '</div>' +
                            '<div class="toast-bottom">TOAST_MESSAGE_1<br />TOAST_MESSAGE_2<br />TOAST MESSAGE 3</div>' +
                           '</div>',
          success: '<i class="material-icons">done</i>' +
                          '<div class="toast-right">' +
                            '<div class="toast-top">' +
                              '<div class="">TOAST_TITLE</div>' +
                              '<button class="nv-button md-button p dark flat dense md-nvGreen-theme"><i class="material-icons">close</i></button>' +
                            '</div>' +
                            '<div class="toast-bottom">TOAST_MESSAGE_1<br />TOAST_MESSAGE_2<br />TOAST MESSAGE 3</div>' +
                          '</div>',
          warning: '<i class="material-icons">warning</i>' +
                          '<div class="toast-right">' +
                            '<div class="toast-top">' +
                              '<div class="">TOAST_TITLE</div>' +
                              '<button class="nv-button md-button p dark flat dense md-nvRed-theme"><i class="material-icons">close</i></button>' +
                            '</div>' +
                            '<div class="toast-bottom">TOAST_MESSAGE_1<br />TOAST_MESSAGE_2<br />TOAST MESSAGE 3</div>' +
                          '</div>',
        };

        nvToast.error('TOAST_TITLE', ['TOAST_MESSAGE_1', 'TOAST_MESSAGE_2', 'TOAST MESSAGE 3']);
        expect($window.toastr.error).toHaveBeenCalledWith(expectedTitle.error, '', {});

        nvToast.info('TOAST_TITLE', ['TOAST_MESSAGE_1', 'TOAST_MESSAGE_2', 'TOAST MESSAGE 3']);
        expect($window.toastr.info).toHaveBeenCalledWith(expectedTitle.info, '', {});

        nvToast.success('TOAST_TITLE', ['TOAST_MESSAGE_1', 'TOAST_MESSAGE_2', 'TOAST MESSAGE 3']);
        expect($window.toastr.success).toHaveBeenCalledWith(expectedTitle.success, '', {});

        nvToast.warning('TOAST_TITLE', ['TOAST_MESSAGE_1', 'TOAST_MESSAGE_2', 'TOAST MESSAGE 3']);
        expect($window.toastr.warning).toHaveBeenCalledWith(expectedTitle.warning, '', {});
      }));

      it('should be able to called with the title and options', inject(($window, nvToast) => {
        const expectedTitle = {
          error: '<i class="material-icons">error</i>' +
                           '<div class="toast-right">' +
                             '<div class="toast-top">' +
                               '<div class="">TOAST_TITLE</div>' +
                               '<button class="dense raised dark md-button nv-button md-nvYellow-theme">BUTTON TITLE</button>' +
                               '<button class="nv-button md-button p dark flat dense md-nvYellow-theme"><i class="material-icons">close</i></button>' +
                             '</div>' +
                           '</div>',
          info: '<i class="material-icons">info</i>' +
                           '<div class="toast-right">' +
                             '<div class="toast-top">' +
                               '<div class="">TOAST_TITLE</div>' +
                               '<button class="dense raised dark md-button nv-button md-nvBlue-theme">BUTTON TITLE</button>' +
                               '<button class="nv-button md-button p dark flat dense md-nvBlue-theme"><i class="material-icons">close</i></button>' +
                             '</div>' +
                           '</div>',
          success: '<i class="material-icons">done</i>' +
                          '<div class="toast-right">' +
                            '<div class="toast-top">' +
                              '<div class="">TOAST_TITLE</div>' +
                              '<button class="dense raised dark md-button nv-button md-nvGreen-theme">BUTTON TITLE</button>' +
                              '<button class="nv-button md-button p dark flat dense md-nvGreen-theme"><i class="material-icons">close</i></button>' +
                            '</div>' +
                          '</div>',
          warning: '<i class="material-icons">warning</i>' +
                          '<div class="toast-right">' +
                            '<div class="toast-top">' +
                              '<div class="">TOAST_TITLE</div>' +
                              '<button class="dense raised dark md-button nv-button md-nvRed-theme">BUTTON TITLE</button>' +
                              '<button class="nv-button md-button p dark flat dense md-nvRed-theme"><i class="material-icons">close</i></button>' +
                            '</div>' +
                          '</div>',
        };

        nvToast.error('TOAST_TITLE', { buttonTitle: 'BUTTON TITLE' });
        expect($window.toastr.error).toHaveBeenCalledWith(expectedTitle.error, '', { buttonTitle: 'BUTTON TITLE' });

        nvToast.info('TOAST_TITLE', { buttonTitle: 'BUTTON TITLE' });
        expect($window.toastr.info).toHaveBeenCalledWith(expectedTitle.info, '', { buttonTitle: 'BUTTON TITLE' });

        nvToast.success('TOAST_TITLE', { buttonTitle: 'BUTTON TITLE' });
        expect($window.toastr.success).toHaveBeenCalledWith(expectedTitle.success, '', { buttonTitle: 'BUTTON TITLE' });

        nvToast.warning('TOAST_TITLE', { buttonTitle: 'BUTTON TITLE' });
        expect($window.toastr.warning).toHaveBeenCalledWith(expectedTitle.warning, '', { buttonTitle: 'BUTTON TITLE' });
      }));

      it('should be able to called with the title, message and options', inject(($window, nvToast) => {
        const expectedTitle = {
          error: '<i class="material-icons">error</i>' +
                           '<div class="toast-right">' +
                             '<div class="toast-top">' +
                               '<div class="">TOAST_TITLE</div>' +
                               '<button class="dense raised dark md-button nv-button md-nvYellow-theme">BUTTON TITLE</button>' +
                               '<button class="nv-button md-button p dark flat dense md-nvYellow-theme"><i class="material-icons">close</i></button>' +
                             '</div>' +
                             '<div class="toast-bottom">TOAST_MESSAGE_1<br />TOAST_MESSAGE_2<br />TOAST MESSAGE 3</div>' +
                           '</div>',
          info: '<i class="material-icons">info</i>' +
                           '<div class="toast-right">' +
                             '<div class="toast-top">' +
                               '<div class="">TOAST_TITLE</div>' +
                               '<button class="dense raised dark md-button nv-button md-nvBlue-theme">BUTTON TITLE</button>' +
                               '<button class="nv-button md-button p dark flat dense md-nvBlue-theme"><i class="material-icons">close</i></button>' +
                             '</div>' +
                             '<div class="toast-bottom">TOAST_MESSAGE_1<br />TOAST_MESSAGE_2<br />TOAST MESSAGE 3</div>' +
                           '</div>',
          success: '<i class="material-icons">done</i>' +
                          '<div class="toast-right">' +
                            '<div class="toast-top">' +
                              '<div class="">TOAST_TITLE</div>' +
                              '<button class="dense raised dark md-button nv-button md-nvGreen-theme">BUTTON TITLE</button>' +
                              '<button class="nv-button md-button p dark flat dense md-nvGreen-theme"><i class="material-icons">close</i></button>' +
                            '</div>' +
                            '<div class="toast-bottom">TOAST_MESSAGE_1<br />TOAST_MESSAGE_2<br />TOAST MESSAGE 3</div>' +
                          '</div>',
          warning: '<i class="material-icons">warning</i>' +
                          '<div class="toast-right">' +
                            '<div class="toast-top">' +
                              '<div class="">TOAST_TITLE</div>' +
                              '<button class="dense raised dark md-button nv-button md-nvRed-theme">BUTTON TITLE</button>' +
                              '<button class="nv-button md-button p dark flat dense md-nvRed-theme"><i class="material-icons">close</i></button>' +
                            '</div>' +
                            '<div class="toast-bottom">TOAST_MESSAGE_1<br />TOAST_MESSAGE_2<br />TOAST MESSAGE 3</div>' +
                          '</div>',
        };

        nvToast.error('TOAST_TITLE', ['TOAST_MESSAGE_1', 'TOAST_MESSAGE_2', 'TOAST MESSAGE 3'], { buttonTitle: 'BUTTON TITLE' });
        expect($window.toastr.error).toHaveBeenCalledWith(expectedTitle.error, '', { buttonTitle: 'BUTTON TITLE' });

        nvToast.info('TOAST_TITLE', ['TOAST_MESSAGE_1', 'TOAST_MESSAGE_2', 'TOAST MESSAGE 3'], { buttonTitle: 'BUTTON TITLE' });
        expect($window.toastr.info).toHaveBeenCalledWith(expectedTitle.info, '', { buttonTitle: 'BUTTON TITLE' });

        nvToast.success('TOAST_TITLE', ['TOAST_MESSAGE_1', 'TOAST_MESSAGE_2', 'TOAST MESSAGE 3'], { buttonTitle: 'BUTTON TITLE' });
        expect($window.toastr.success).toHaveBeenCalledWith(expectedTitle.success, '', { buttonTitle: 'BUTTON TITLE' });

        nvToast.warning('TOAST_TITLE', ['TOAST_MESSAGE_1', 'TOAST_MESSAGE_2', 'TOAST MESSAGE 3'], { buttonTitle: 'BUTTON TITLE' });
        expect($window.toastr.warning).toHaveBeenCalledWith(expectedTitle.warning, '', { buttonTitle: 'BUTTON TITLE' });
      }));
    });

    describe('correctly behaves', () => {
      const toast = {
        buttonCallback: (toast, event) => {},
      };
      let toastElement;
      let buttons;

      beforeEach(inject(($window, nvToast) => {
        spyOn(toast, 'buttonCallback');
        spyOn($window.toastr, 'clear');

        toastElement = nvToast.success('title', 'message', { buttonTitle: 'button', buttonCallback: toast.buttonCallback });
        buttons = toastElement.find('button');
      }));

      it('should called toast.clear when close button is clicked', inject(($window) => {
        expect(buttons).toHaveLength(2);
        expect($window.toastr.clear).not.toHaveBeenCalled();

        const closeButton = buttons.eq(1);
        closeButton.click();

        expect($window.toastr.clear).toHaveBeenCalled();
      }));

      it('should called buttonCallback when button is clicked', () => {
        expect(buttons).toHaveLength(2);
        expect(toast.buttonCallback).not.toHaveBeenCalled();

        const button = buttons.eq(0);
        button.click();

        expect(toast.buttonCallback).toHaveBeenCalled();
        expect(toast.buttonCallback).toHaveBeenCalledWith(toastElement, jasmine.any(Object));
      });
    });

    describe('correctly displays', () => {
      let toast;

      beforeEach(inject(($window, nvToast) => {
        toast = nvToast.success('title', ['message'], { buttonTitle: 'button', icon: 'check_circle' });
      }));

      it('should display properly', () => {
        expect(toast).toHaveClass('toast-success');
        expect(toast).not.toBeEmpty();
        const divs = toast.children();
        expect(divs).toHaveLength(2);
        // make sure content[0] is icon, content[1] is the toast div
        const content = divs.eq(1).children();
        expect(content).toHaveLength(2);
        expect(content.eq(0)).toEqual('i');
        expect(content.eq(0)).toContainText('check_circle');
        expect(content.eq(1)).toEqual('div.toast-right');
        // make sure that toastRight[0] is div.toast-top, toastRight[1] is div.toast-bottom
        const toastRight = content.eq(1).children();
        expect(toastRight.eq(0)).toEqual('div.toast-top');
        expect(toastRight.eq(1)).toEqual('div.toast-bottom');
        // toast-top have title, button, and close button
        const toastTop = toastRight.eq(0).children();
        expect(toastTop.eq(0)).toEqual('div');
        expect(toastTop.eq(0)).toContainText('title');
        expect(toastTop.eq(1)).toEqual('button');
        expect(toastTop.eq(1)).toHaveClass('md-nvGreen-theme');
        expect(toastTop.eq(1)).toHaveClass('nv-button');
        expect(toastTop.eq(1)).toHaveClass('md-button');
        expect(toastTop.eq(1)).toHaveClass('dense');
        expect(toastTop.eq(1)).toHaveClass('raised');
        expect(toastTop.eq(1)).toHaveClass('dark');
        expect(toastTop.eq(1)).toContainText('button');
        expect(toastTop.eq(2)).toEqual('button');
        expect(toastTop.eq(2)).toHaveClass('nv-button');
        expect(toastTop.eq(2)).toHaveClass('md-button');
        expect(toastTop.eq(2)).toHaveClass('p');
        expect(toastTop.eq(2)).toHaveClass('dark');
        expect(toastTop.eq(2)).toHaveClass('flat');
        expect(toastTop.eq(2)).toHaveClass('dense');
        expect(toastTop.eq(2)).toHaveClass('md-nvGreen-theme');
        // toast-bottom is message
        const toastBottom = toastRight.eq(1);
        expect(toastBottom).toContainText('message');
      });
    });
  });
}());
