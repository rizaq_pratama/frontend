(function test() {
  describe('table.service', () => {
    const module = angular.mock.module;
    const inject = angular.mock.inject;

    beforeEach(module('nvCommons.services'));

    describe('nvTable.createTable', () => {
      let table;

      const FIELDS = {
        id: { displayName: 'User ID', type: 'number' },
        firstName: { displayName: 'First Name', type: 'text' },
        lastName: { displayName: 'Last Name', type: 'text' },
        vehicles: {
          _array: true,
          _values: [],
          vehicleType: { displayName: 'Vehicle Type', options: [] },
          vehicleNo: { displayName: 'License Number' },
          ownVehicle: { displayName: 'Own Vehicle' },
        },
        tags: {
          _deep: true,
          resupply: { displayName: 'Able to Bid Routes (RF)?' },
        },
      };
      const DATA = [{ id: 1, firstName: 'aaa', agree: false, tags: { resupply: 'vcx' } },
                    { id: 2, firstName: 'bbb', agree: false, tags: { resupply: 'qew' } },
                    { id: 3, firstName: 'ccc', agree: true, tags: { resupply: 'fds' } },
                    { id: 4, firstName: 'ddd', agree: false, tags: { resupply: 'ore' } },
                    { id: 5, firstName: 'abe', agree: true, tags: { resupply: 'lee' } },
                    { id: 6, firstName: 'bbe', agree: true, tags: { resupply: 'tes' } },
                    { id: 7, firstName: 'cbf', agree: false, tags: { resupply: 'lww' } },
                    { id: 8, firstName: 'dbf', agree: false, tags: { resupply: 'aoa' } }];


      beforeEach(inject((nvTable) => {
        table = nvTable.createTable(FIELDS);
      }));

      describe('basic', () => {
        it('would not throw if no fields passed in', inject((nvTable) => {
          // this test is to improve coverage, cover edge case
          expect(createTableMightThrow).not.toThrow();

          function createTableMightThrow() {
            return nvTable.createTable();
          }
        }));

        it('should able to take in .model.js fields and provide table header display name', () => {
          expect(table.getTableHeaderDisplayName('id')).toEqual('User ID');
          expect(table.getTableHeaderDisplayName('firstName')).toEqual('First Name');
          expect(table.getTableHeaderDisplayName('vehicles')).toEqual('');
          expect(table.getTableHeaderDisplayName('vehicles.vehicleNo')).toEqual('License Number');
          expect(table.getTableHeaderDisplayName('vehicles.ownVehicle')).toEqual('Own Vehicle');
          expect(table.getTableHeaderDisplayName('tags.resupply')).toEqual('Able to Bid Routes (RF)?');
          expect(table.getTableHeaderDisplayName('vehicles.nonexist')).toEqual('');
          expect(table.getTableHeaderDisplayName('nonexist.deep.level')).toEqual('');
        });

        it('should able to add more columns with display name and custom sort function', () => {
          expect(table.sortFunctions.test).toBeUndefined();
          table.addColumn('test', { displayName: 'Test Column' });
          expect(table.getTableHeaderDisplayName('test')).toEqual('Test Column');
          expect(table.sortFunctions.test).toEqual(jasmine.any(Function));

          expect(table.sortFunctions['custom.deep']).toBeUndefined();
          table.addColumn('custom.deep', { displayName: 'Custom Deep', sortFunction: sortFunction });
          expect(table.getTableHeaderDisplayName('custom.deep')).toEqual('Custom Deep');
          expect(table.sortFunctions['custom.deep']).toEqual(sortFunction);

          function sortFunction() {}
        });
      });

      describe('merge and remove', () => {
        it('should able to set data', () => {
          expect(table.getTableData()).toEqual([]);
          expect(table.getLength()).toBe(0);
          expect(table.getUnfilteredLength()).toBe(0);

          table.setData(DATA);

          expect(table.getTableData()).toEqual(DATA);
          expect(table.getLength()).toBe(DATA.length);
          expect(table.getUnfilteredLength()).toBe(DATA.length);
        });

        it('should able to set data as promise', inject(($timeout) => {
          const dataPromise = $timeout(() => DATA);

          expect(table.getTableData()).toEqual([]);

          table.setDataPromise(dataPromise);
          expect(table.getTableData()).toEqual([]);

          $timeout.flush();
          expect(table.getTableData()).toEqual(DATA);
        }));

        it('should able to merge in new data', () => {
          table.setData(angular.copy(DATA));
          expect(table.getTableData()).toEqual(DATA);

          table.mergeIn({ id: 9, firstName: 'vfv', agree: false, tags: { resupply: 'eew' } }, 'id');

          expect(table.getTableData()).toEqual([{ id: 1, firstName: 'aaa', agree: false, tags: { resupply: 'vcx' } },
                                                { id: 2, firstName: 'bbb', agree: false, tags: { resupply: 'qew' } },
                                                { id: 3, firstName: 'ccc', agree: true, tags: { resupply: 'fds' } },
                                                { id: 4, firstName: 'ddd', agree: false, tags: { resupply: 'ore' } },
                                                { id: 5, firstName: 'abe', agree: true, tags: { resupply: 'lee' } },
                                                { id: 6, firstName: 'bbe', agree: true, tags: { resupply: 'tes' } },
                                                { id: 7, firstName: 'cbf', agree: false, tags: { resupply: 'lww' } },
                                                { id: 8, firstName: 'dbf', agree: false, tags: { resupply: 'aoa' } },
                                                { id: 9, firstName: 'vfv', agree: false, tags: { resupply: 'eew' } }]);

          table.mergeIn({ id: 3, firstName: 'kkk', agree: true, tags: { resupply: 'qwe' } }, 'id');

          expect(table.getTableData()).toEqual([{ id: 1, firstName: 'aaa', agree: false, tags: { resupply: 'vcx' } },
                                                { id: 2, firstName: 'bbb', agree: false, tags: { resupply: 'qew' } },
                                                { id: 3, firstName: 'kkk', agree: true, tags: { resupply: 'qwe' } },
                                                { id: 4, firstName: 'ddd', agree: false, tags: { resupply: 'ore' } },
                                                { id: 5, firstName: 'abe', agree: true, tags: { resupply: 'lee' } },
                                                { id: 6, firstName: 'bbe', agree: true, tags: { resupply: 'tes' } },
                                                { id: 7, firstName: 'cbf', agree: false, tags: { resupply: 'lww' } },
                                                { id: 8, firstName: 'dbf', agree: false, tags: { resupply: 'aoa' } },
                                                { id: 9, firstName: 'vfv', agree: false, tags: { resupply: 'eew' } }]);
        });

        it('should able to merge in while maintain filter and sort new data', () => {
          table.setData(angular.copy(DATA));
          table.addFilter(a => (a.id % 2 !== 0)).sort('firstName');

          expect(table.getTableData()).toEqual([{ id: 1, firstName: 'aaa', agree: false, tags: { resupply: 'vcx' } },
                                                { id: 5, firstName: 'abe', agree: true, tags: { resupply: 'lee' } },
                                                { id: 7, firstName: 'cbf', agree: false, tags: { resupply: 'lww' } },
                                                { id: 3, firstName: 'ccc', agree: true, tags: { resupply: 'fds' } }]);

          table.mergeIn({ id: 9, firstName: 'bbb', agree: false, tags: { resupply: 'eew' } }, 'id');

          expect(table.getTableData()).toEqual([{ id: 1, firstName: 'aaa', agree: false, tags: { resupply: 'vcx' } },
                                                { id: 5, firstName: 'abe', agree: true, tags: { resupply: 'lee' } },
                                                { id: 9, firstName: 'bbb', agree: false, tags: { resupply: 'eew' } },
                                                { id: 7, firstName: 'cbf', agree: false, tags: { resupply: 'lww' } },
                                                { id: 3, firstName: 'ccc', agree: true, tags: { resupply: 'fds' } }]);

          table.mergeIn({ id: 7, firstName: 'ddd', agree: false, tags: { resupply: 'eew' } }, 'id');

          expect(table.getTableData()).toEqual([{ id: 1, firstName: 'aaa', agree: false, tags: { resupply: 'vcx' } },
                                                { id: 5, firstName: 'abe', agree: true, tags: { resupply: 'lee' } },
                                                { id: 9, firstName: 'bbb', agree: false, tags: { resupply: 'eew' } },
                                                { id: 3, firstName: 'ccc', agree: true, tags: { resupply: 'fds' } },
                                                { id: 7, firstName: 'ddd', agree: false, tags: { resupply: 'eew' } }]);

          table.mergeIn({ id: 4, firstName: 'eee', agree: false, tags: { resupply: 'ecw' } }, 'id');

          expect(table.getTableData()).toEqual([{ id: 1, firstName: 'aaa', agree: false, tags: { resupply: 'vcx' } },
                                                { id: 5, firstName: 'abe', agree: true, tags: { resupply: 'lee' } },
                                                { id: 9, firstName: 'bbb', agree: false, tags: { resupply: 'eew' } },
                                                { id: 3, firstName: 'ccc', agree: true, tags: { resupply: 'fds' } },
                                                { id: 7, firstName: 'ddd', agree: false, tags: { resupply: 'eew' } }]);
        });

        it('should able to merge in new data promise', inject(($timeout) => {
          table.setData(angular.copy(DATA));
          expect(table.getTableData()).toEqual(DATA);

          const dataPromise1 = $timeout(() => ({ id: 9, firstName: 'vfv', agree: false, tags: { resupply: 'eew' } }));
          table.mergeInPromise(dataPromise1, 'id');

          $timeout.flush();
          expect(table.getTableData()).toEqual([{ id: 1, firstName: 'aaa', agree: false, tags: { resupply: 'vcx' } },
                                                { id: 2, firstName: 'bbb', agree: false, tags: { resupply: 'qew' } },
                                                { id: 3, firstName: 'ccc', agree: true, tags: { resupply: 'fds' } },
                                                { id: 4, firstName: 'ddd', agree: false, tags: { resupply: 'ore' } },
                                                { id: 5, firstName: 'abe', agree: true, tags: { resupply: 'lee' } },
                                                { id: 6, firstName: 'bbe', agree: true, tags: { resupply: 'tes' } },
                                                { id: 7, firstName: 'cbf', agree: false, tags: { resupply: 'lww' } },
                                                { id: 8, firstName: 'dbf', agree: false, tags: { resupply: 'aoa' } },
                                                { id: 9, firstName: 'vfv', agree: false, tags: { resupply: 'eew' } }]);

          const dataPromise2 = $timeout(() => ({ id: 3, firstName: 'kkk', agree: true, tags: { resupply: 'qwe' } }));
          table.mergeInPromise(dataPromise2, 'id');

          $timeout.flush();
          expect(table.getTableData()).toEqual([{ id: 1, firstName: 'aaa', agree: false, tags: { resupply: 'vcx' } },
                                                { id: 2, firstName: 'bbb', agree: false, tags: { resupply: 'qew' } },
                                                { id: 3, firstName: 'kkk', agree: true, tags: { resupply: 'qwe' } },
                                                { id: 4, firstName: 'ddd', agree: false, tags: { resupply: 'ore' } },
                                                { id: 5, firstName: 'abe', agree: true, tags: { resupply: 'lee' } },
                                                { id: 6, firstName: 'bbe', agree: true, tags: { resupply: 'tes' } },
                                                { id: 7, firstName: 'cbf', agree: false, tags: { resupply: 'lww' } },
                                                { id: 8, firstName: 'dbf', agree: false, tags: { resupply: 'aoa' } },
                                                { id: 9, firstName: 'vfv', agree: false, tags: { resupply: 'eew' } }]);
        }));

        it('should able to merge in promise while maintain filter and sort new data', inject(($timeout) => {
          table.setData(angular.copy(DATA));
          table.addFilter(a => (a.id % 2 !== 0)).sort('firstName');

          expect(table.getTableData()).toEqual([{ id: 1, firstName: 'aaa', agree: false, tags: { resupply: 'vcx' } },
                                                { id: 5, firstName: 'abe', agree: true, tags: { resupply: 'lee' } },
                                                { id: 7, firstName: 'cbf', agree: false, tags: { resupply: 'lww' } },
                                                { id: 3, firstName: 'ccc', agree: true, tags: { resupply: 'fds' } }]);

          const dataPromise1 = $timeout(() => ({ id: 9, firstName: 'bbb', agree: false, tags: { resupply: 'eew' } }));
          table.mergeInPromise(dataPromise1, 'id');
          $timeout.flush();

          expect(table.getTableData()).toEqual([{ id: 1, firstName: 'aaa', agree: false, tags: { resupply: 'vcx' } },
                                                { id: 5, firstName: 'abe', agree: true, tags: { resupply: 'lee' } },
                                                { id: 9, firstName: 'bbb', agree: false, tags: { resupply: 'eew' } },
                                                { id: 7, firstName: 'cbf', agree: false, tags: { resupply: 'lww' } },
                                                { id: 3, firstName: 'ccc', agree: true, tags: { resupply: 'fds' } }]);

          const dataPromise2 = $timeout(() => ({ id: 7, firstName: 'ddd', agree: false, tags: { resupply: 'eew' } }));
          table.mergeInPromise(dataPromise2, 'id');
          $timeout.flush();

          expect(table.getTableData()).toEqual([{ id: 1, firstName: 'aaa', agree: false, tags: { resupply: 'vcx' } },
                                                { id: 5, firstName: 'abe', agree: true, tags: { resupply: 'lee' } },
                                                { id: 9, firstName: 'bbb', agree: false, tags: { resupply: 'eew' } },
                                                { id: 3, firstName: 'ccc', agree: true, tags: { resupply: 'fds' } },
                                                { id: 7, firstName: 'ddd', agree: false, tags: { resupply: 'eew' } }]);

          const dataPromise3 = $timeout(() => ({ id: 4, firstName: 'eee', agree: false, tags: { resupply: 'ecw' } }));
          table.mergeInPromise(dataPromise3, 'id');
          $timeout.flush();

          expect(table.getTableData()).toEqual([{ id: 1, firstName: 'aaa', agree: false, tags: { resupply: 'vcx' } },
                                                { id: 5, firstName: 'abe', agree: true, tags: { resupply: 'lee' } },
                                                { id: 9, firstName: 'bbb', agree: false, tags: { resupply: 'eew' } },
                                                { id: 3, firstName: 'ccc', agree: true, tags: { resupply: 'fds' } },
                                                { id: 7, firstName: 'ddd', agree: false, tags: { resupply: 'eew' } }]);
        }));

        it('should able to remove data', () => {
          table.setData(angular.copy(DATA));
          expect(table.getTableData()).toEqual(DATA);

          table.remove({ id: 2, firstName: 'bbb', agree: false, tags: { resupply: 'qew' } }, 'id');

          expect(table.getTableData()).toEqual([{ id: 1, firstName: 'aaa', agree: false, tags: { resupply: 'vcx' } },
                                                { id: 3, firstName: 'ccc', agree: true, tags: { resupply: 'fds' } },
                                                { id: 4, firstName: 'ddd', agree: false, tags: { resupply: 'ore' } },
                                                { id: 5, firstName: 'abe', agree: true, tags: { resupply: 'lee' } },
                                                { id: 6, firstName: 'bbe', agree: true, tags: { resupply: 'tes' } },
                                                { id: 7, firstName: 'cbf', agree: false, tags: { resupply: 'lww' } },
                                                { id: 8, firstName: 'dbf', agree: false, tags: { resupply: 'aoa' } }]);

          table.remove([{ id: 1, firstName: 'aaa', agree: false, tags: { resupply: 'vcx' } },
                        { id: 3, firstName: 'kkk', agree: true, tags: { resupply: 'qwe' } }], 'id');

          expect(table.getTableData()).toEqual([{ id: 4, firstName: 'ddd', agree: false, tags: { resupply: 'ore' } },
                                                { id: 5, firstName: 'abe', agree: true, tags: { resupply: 'lee' } },
                                                { id: 6, firstName: 'bbe', agree: true, tags: { resupply: 'tes' } },
                                                { id: 7, firstName: 'cbf', agree: false, tags: { resupply: 'lww' } },
                                                { id: 8, firstName: 'dbf', agree: false, tags: { resupply: 'aoa' } }]);
        });
      });

      describe('sort', () => {
        it('should maintain the correct sort column and index', () => {
          table.setData(DATA);
          table.sort('id');
          expect(table.currSortIdx).toBe(0);
          expect(table.currSortCol).toEqual('id');

          table.sort('id');
          expect(table.currSortIdx).toBe(1);
          expect(table.currSortCol).toEqual('id');

          table.sort('id');
          expect(table.currSortIdx).toBe(0);
          expect(table.currSortCol).toEqual('id');

          table.sort('firstName');
          expect(table.currSortIdx).toBe(0);
          expect(table.currSortCol).toEqual('firstName');

          table.sort('lastName');
          expect(table.currSortIdx).toBe(0);
          expect(table.currSortCol).toEqual('lastName');

          table.sort('nonexist');
          expect(table.currSortIdx).toBe(0);
          expect(table.currSortCol).toEqual('lastName');

          table.sort('firstName');
          expect(table.currSortIdx).toBe(0);
          expect(table.currSortCol).toEqual('firstName');

          table.sort('firstName');
          expect(table.currSortIdx).toBe(1);
          expect(table.currSortCol).toEqual('firstName');

          table.sort('nonexist.deep');
          expect(table.currSortIdx).toBe(1);
          expect(table.currSortCol).toEqual('firstName');

          table.sort('vehicles.ownVehicle');
          expect(table.currSortIdx).toBe(0);
          expect(table.currSortCol).toEqual('vehicles.ownVehicle');

          table.sort('vehicles.ownVehicle');
          expect(table.currSortIdx).toBe(1);
          expect(table.currSortCol).toEqual('vehicles.ownVehicle');
        });

        it('should give correct active value and icon', () => {
          table.sort('id');
          expect(table.isSortActive('id')).toBe(true);
          expect(table.showSortIcon('id')).toMatch('nv-sort-asc.svg');
          expect(table.isSortActive('firstName')).toBe(false);
          expect(table.showSortIcon('firstName')).toMatch('nv-sort.svg');
          expect(table.isSortActive('tags.resupply')).toBe(false);
          expect(table.showSortIcon('tags.resupply')).toMatch('nv-sort.svg');

          table.sort('id');
          expect(table.isSortActive('id')).toBe(true);
          expect(table.showSortIcon('id')).toMatch('nv-sort-desc.svg');
          expect(table.isSortActive('firstName')).toBe(false);
          expect(table.showSortIcon('firstName')).toMatch('nv-sort.svg');
          expect(table.isSortActive('tags.resupply')).toBe(false);
          expect(table.showSortIcon('tags.resupply')).toMatch('nv-sort.svg');

          table.sort('id');
          expect(table.isSortActive('id')).toBe(true);
          expect(table.showSortIcon('id')).toMatch('nv-sort-asc.svg');

          table.sort('firstName');
          expect(table.isSortActive('id')).toBe(false);
          expect(table.showSortIcon('id')).toMatch('nv-sort.svg');
          expect(table.isSortActive('firstName')).toBe(true);
          expect(table.showSortIcon('firstName')).toMatch('nv-sort-asc.svg');
          expect(table.isSortActive('tags.resupply')).toBe(false);
          expect(table.showSortIcon('tags.resupply')).toMatch('nv-sort.svg');

          table.sort('nonexist');
          expect(table.isSortActive('id')).toBe(false);
          expect(table.showSortIcon('id')).toMatch('nv-sort.svg');
          expect(table.isSortActive('firstName')).toBe(true);
          expect(table.showSortIcon('firstName')).toMatch('nv-sort-asc.svg');
          expect(table.isSortActive('tags.resupply')).toBe(false);
          expect(table.showSortIcon('tags.resupply')).toMatch('nv-sort.svg');
        });

        it('should sort correctly', () => {
          table.setData(DATA);

          table.sort('id');
          expect(table.currSortIdx).toBe(0);
          expect(table.getTableData()).toEqual([{ id: 1, firstName: 'aaa', agree: false, tags: { resupply: 'vcx' } },
                                                   { id: 2, firstName: 'bbb', agree: false, tags: { resupply: 'qew' } },
                                                   { id: 3, firstName: 'ccc', agree: true, tags: { resupply: 'fds' } },
                                                   { id: 4, firstName: 'ddd', agree: false, tags: { resupply: 'ore' } },
                                                   { id: 5, firstName: 'abe', agree: true, tags: { resupply: 'lee' } },
                                                   { id: 6, firstName: 'bbe', agree: true, tags: { resupply: 'tes' } },
                                                   { id: 7, firstName: 'cbf', agree: false, tags: { resupply: 'lww' } },
                                                   { id: 8, firstName: 'dbf', agree: false, tags: { resupply: 'aoa' } }]);
          table.sort('id');
          expect(table.currSortIdx).toBe(1);
          expect(table.getTableData()).toEqual([{ id: 8, firstName: 'dbf', agree: false, tags: { resupply: 'aoa' } },
                                                   { id: 7, firstName: 'cbf', agree: false, tags: { resupply: 'lww' } },
                                                   { id: 6, firstName: 'bbe', agree: true, tags: { resupply: 'tes' } },
                                                   { id: 5, firstName: 'abe', agree: true, tags: { resupply: 'lee' } },
                                                   { id: 4, firstName: 'ddd', agree: false, tags: { resupply: 'ore' } },
                                                   { id: 3, firstName: 'ccc', agree: true, tags: { resupply: 'fds' } },
                                                   { id: 2, firstName: 'bbb', agree: false, tags: { resupply: 'qew' } },
                                                   { id: 1, firstName: 'aaa', agree: false, tags: { resupply: 'vcx' } }]);

          table.sort('tags.resupply');
          expect(table.currSortIdx).toBe(0);
          expect(table.getTableData()).toEqual([{ id: 8, firstName: 'dbf', agree: false, tags: { resupply: 'aoa' } },
                                                   { id: 3, firstName: 'ccc', agree: true, tags: { resupply: 'fds' } },
                                                   { id: 5, firstName: 'abe', agree: true, tags: { resupply: 'lee' } },
                                                   { id: 7, firstName: 'cbf', agree: false, tags: { resupply: 'lww' } },
                                                   { id: 4, firstName: 'ddd', agree: false, tags: { resupply: 'ore' } },
                                                   { id: 2, firstName: 'bbb', agree: false, tags: { resupply: 'qew' } },
                                                   { id: 6, firstName: 'bbe', agree: true, tags: { resupply: 'tes' } },
                                                   { id: 1, firstName: 'aaa', agree: false, tags: { resupply: 'vcx' } },
                                                   ]);

          table.sort('firstName');
          expect(table.currSortIdx).toBe(0);
          expect(table.getTableData()).toEqual([
                                                   { id: 1, firstName: 'aaa', agree: false, tags: { resupply: 'vcx' } },
                                                   { id: 5, firstName: 'abe', agree: true, tags: { resupply: 'lee' } },
                                                   { id: 2, firstName: 'bbb', agree: false, tags: { resupply: 'qew' } },
                                                   { id: 6, firstName: 'bbe', agree: true, tags: { resupply: 'tes' } },
                                                   { id: 7, firstName: 'cbf', agree: false, tags: { resupply: 'lww' } },
                                                   { id: 3, firstName: 'ccc', agree: true, tags: { resupply: 'fds' } },
                                                   { id: 8, firstName: 'dbf', agree: false, tags: { resupply: 'aoa' } },
                                                   { id: 4, firstName: 'ddd', agree: false, tags: { resupply: 'ore' } },
          ]);
        });

        it('should able to set a custom sort', () => {
          table.setData(DATA);
          table.setSortFunction('custom', (data, order) => _.orderBy(data, d => d.tags.resupply.split('').reverse().join(''), order));

          expect(table.getTableData()).toEqual(DATA);

          table.sort('custom');
          expect(table.getTableData()).toEqual([
                     { id: 8, firstName: 'dbf', agree: false, tags: { resupply: 'aoa' } },
                     { id: 5, firstName: 'abe', agree: true, tags: { resupply: 'lee' } },
                     { id: 4, firstName: 'ddd', agree: false, tags: { resupply: 'ore' } },
                     { id: 3, firstName: 'ccc', agree: true, tags: { resupply: 'fds' } },
                     { id: 6, firstName: 'bbe', agree: true, tags: { resupply: 'tes' } },
                     { id: 2, firstName: 'bbb', agree: false, tags: { resupply: 'qew' } },
                     { id: 7, firstName: 'cbf', agree: false, tags: { resupply: 'lww' } },
                     { id: 1, firstName: 'aaa', agree: false, tags: { resupply: 'vcx' } },
          ]);
        });

        it('should able to set a custom sort via custom column', () => {
          table.addColumn('custom.deep', { displayName: 'Custom Deep', sortFunction: sortFunction });
          table.setData(DATA);

          expect(table.getTableData()).toEqual(DATA);

          table.sort('custom.deep');
          expect(table.getTableData()).toEqual([
                     { id: 8, firstName: 'dbf', agree: false, tags: { resupply: 'aoa' } },
                     { id: 5, firstName: 'abe', agree: true, tags: { resupply: 'lee' } },
                     { id: 4, firstName: 'ddd', agree: false, tags: { resupply: 'ore' } },
                     { id: 3, firstName: 'ccc', agree: true, tags: { resupply: 'fds' } },
                     { id: 6, firstName: 'bbe', agree: true, tags: { resupply: 'tes' } },
                     { id: 2, firstName: 'bbb', agree: false, tags: { resupply: 'qew' } },
                     { id: 7, firstName: 'cbf', agree: false, tags: { resupply: 'lww' } },
                     { id: 1, firstName: 'aaa', agree: false, tags: { resupply: 'vcx' } },
          ]);

          function sortFunction(data, order) {
            return _.orderBy(data, d => d.tags.resupply.split('').reverse().join(''), order);
          }
        });
      });

      describe('filter', () => {
        it('should able to add filter and remove filter', () => {
          table.setData(DATA);
          expect(table.getTableData()).toEqual([{ id: 1, firstName: 'aaa', agree: false, tags: { resupply: 'vcx' } },
                                                   { id: 2, firstName: 'bbb', agree: false, tags: { resupply: 'qew' } },
                                                   { id: 3, firstName: 'ccc', agree: true, tags: { resupply: 'fds' } },
                                                   { id: 4, firstName: 'ddd', agree: false, tags: { resupply: 'ore' } },
                                                   { id: 5, firstName: 'abe', agree: true, tags: { resupply: 'lee' } },
                                                   { id: 6, firstName: 'bbe', agree: true, tags: { resupply: 'tes' } },
                                                   { id: 7, firstName: 'cbf', agree: false, tags: { resupply: 'lww' } },
                                                   { id: 8, firstName: 'dbf', agree: false, tags: { resupply: 'aoa' } }]);
          table.addFilter(filterA);
          expect(table.getTableData()).toEqual([{ id: 1, firstName: 'aaa', agree: false, tags: { resupply: 'vcx' } },
                                                   { id: 3, firstName: 'ccc', agree: true, tags: { resupply: 'fds' } },
                                                   { id: 5, firstName: 'abe', agree: true, tags: { resupply: 'lee' } },
                                                   { id: 7, firstName: 'cbf', agree: false, tags: { resupply: 'lww' } }]);
          table.addFilter(filterB);
          expect(table.getTableData()).toEqual([{ id: 3, firstName: 'ccc', agree: true, tags: { resupply: 'fds' } },
                                                   { id: 5, firstName: 'abe', agree: true, tags: { resupply: 'lee' } }]);

          table.removeFilter(filterA);
          expect(table.getTableData()).toEqual([{ id: 3, firstName: 'ccc', agree: true, tags: { resupply: 'fds' } },
                                                   { id: 5, firstName: 'abe', agree: true, tags: { resupply: 'lee' } },
                                                   { id: 6, firstName: 'bbe', agree: true, tags: { resupply: 'tes' } }]);
        });

        it('should able to chain add filter and remove filter', () => {
          table.setData(DATA);
          expect(table.getTableData()).toEqual([{ id: 1, firstName: 'aaa', agree: false, tags: { resupply: 'vcx' } },
                                                   { id: 2, firstName: 'bbb', agree: false, tags: { resupply: 'qew' } },
                                                   { id: 3, firstName: 'ccc', agree: true, tags: { resupply: 'fds' } },
                                                   { id: 4, firstName: 'ddd', agree: false, tags: { resupply: 'ore' } },
                                                   { id: 5, firstName: 'abe', agree: true, tags: { resupply: 'lee' } },
                                                   { id: 6, firstName: 'bbe', agree: true, tags: { resupply: 'tes' } },
                                                   { id: 7, firstName: 'cbf', agree: false, tags: { resupply: 'lww' } },
                                                   { id: 8, firstName: 'dbf', agree: false, tags: { resupply: 'aoa' } }]);
          table
            .addFilter(filterA)
            .addFilter(filterB);

          expect(table.getTableData()).toEqual([{ id: 3, firstName: 'ccc', agree: true, tags: { resupply: 'fds' } },
                                                   { id: 5, firstName: 'abe', agree: true, tags: { resupply: 'lee' } }]);

          table
            .removeFilter(filterB)
            .removeFilter(filterA);

          expect(table.getTableData()).toEqual([{ id: 1, firstName: 'aaa', agree: false, tags: { resupply: 'vcx' } },
                                                   { id: 2, firstName: 'bbb', agree: false, tags: { resupply: 'qew' } },
                                                   { id: 3, firstName: 'ccc', agree: true, tags: { resupply: 'fds' } },
                                                   { id: 4, firstName: 'ddd', agree: false, tags: { resupply: 'ore' } },
                                                   { id: 5, firstName: 'abe', agree: true, tags: { resupply: 'lee' } },
                                                   { id: 6, firstName: 'bbe', agree: true, tags: { resupply: 'tes' } },
                                                   { id: 7, firstName: 'cbf', agree: false, tags: { resupply: 'lww' } },
                                                   { id: 8, firstName: 'dbf', agree: false, tags: { resupply: 'aoa' } }]);
        });

        it('should accept array of arguments for filter', () => {
          table.setData(DATA);
          expect(table.getTableData()).toEqual([{ id: 1, firstName: 'aaa', agree: false, tags: { resupply: 'vcx' } },
                                                   { id: 2, firstName: 'bbb', agree: false, tags: { resupply: 'qew' } },
                                                   { id: 3, firstName: 'ccc', agree: true, tags: { resupply: 'fds' } },
                                                   { id: 4, firstName: 'ddd', agree: false, tags: { resupply: 'ore' } },
                                                   { id: 5, firstName: 'abe', agree: true, tags: { resupply: 'lee' } },
                                                   { id: 6, firstName: 'bbe', agree: true, tags: { resupply: 'tes' } },
                                                   { id: 7, firstName: 'cbf', agree: false, tags: { resupply: 'lww' } },
                                                   { id: 8, firstName: 'dbf', agree: false, tags: { resupply: 'aoa' } }]);
          table.addFilter(filterA, filterB);

          expect(table.getTableData()).toEqual([{ id: 3, firstName: 'ccc', agree: true, tags: { resupply: 'fds' } },
                                                   { id: 5, firstName: 'abe', agree: true, tags: { resupply: 'lee' } }]);

          table.removeFilter(filterB, filterA);

          expect(table.getTableData()).toEqual([{ id: 1, firstName: 'aaa', agree: false, tags: { resupply: 'vcx' } },
                                                   { id: 2, firstName: 'bbb', agree: false, tags: { resupply: 'qew' } },
                                                   { id: 3, firstName: 'ccc', agree: true, tags: { resupply: 'fds' } },
                                                   { id: 4, firstName: 'ddd', agree: false, tags: { resupply: 'ore' } },
                                                   { id: 5, firstName: 'abe', agree: true, tags: { resupply: 'lee' } },
                                                   { id: 6, firstName: 'bbe', agree: true, tags: { resupply: 'tes' } },
                                                   { id: 7, firstName: 'cbf', agree: false, tags: { resupply: 'lww' } },
                                                   { id: 8, firstName: 'dbf', agree: false, tags: { resupply: 'aoa' } }]);
        });

        it('should sort after filter', () => {
          table.setData(DATA);
          expect(table.getTableData()).toEqual([{ id: 1, firstName: 'aaa', agree: false, tags: { resupply: 'vcx' } },
                                                   { id: 2, firstName: 'bbb', agree: false, tags: { resupply: 'qew' } },
                                                   { id: 3, firstName: 'ccc', agree: true, tags: { resupply: 'fds' } },
                                                   { id: 4, firstName: 'ddd', agree: false, tags: { resupply: 'ore' } },
                                                   { id: 5, firstName: 'abe', agree: true, tags: { resupply: 'lee' } },
                                                   { id: 6, firstName: 'bbe', agree: true, tags: { resupply: 'tes' } },
                                                   { id: 7, firstName: 'cbf', agree: false, tags: { resupply: 'lww' } },
                                                   { id: 8, firstName: 'dbf', agree: false, tags: { resupply: 'aoa' } }]);
          table
            .addFilter(filterA)
            .sort('tags.resupply');

          expect(table.getTableData()).toEqual([{ id: 3, firstName: 'ccc', agree: true, tags: { resupply: 'fds' } },
                                                   { id: 5, firstName: 'abe', agree: true, tags: { resupply: 'lee' } },
                                                   { id: 7, firstName: 'cbf', agree: false, tags: { resupply: 'lww' } },
                                                   { id: 1, firstName: 'aaa', agree: false, tags: { resupply: 'vcx' } }]);

          table
            .removeFilter(filterA)
            .addFilter(filterB)
            .sort('firstName');

          expect(table.getTableData()).toEqual([{ id: 5, firstName: 'abe', agree: true, tags: { resupply: 'lee' } },
                                                   { id: 6, firstName: 'bbe', agree: true, tags: { resupply: 'tes' } },
                                                   { id: 3, firstName: 'ccc', agree: true, tags: { resupply: 'fds' } }]);
        });

        it('should filter with search text', () => {
          table.setData(DATA);
          expect(table.getTableData()).toContain({ id: 1, firstName: 'aaa', agree: false, tags: { resupply: 'vcx' } });
          expect(table.getTableData()).toContain({ id: 2, firstName: 'bbb', agree: false, tags: { resupply: 'qew' } });
          expect(table.getTableData()).toContain({ id: 3, firstName: 'ccc', agree: true, tags: { resupply: 'fds' } });
          expect(table.getTableData()).toContain({ id: 4, firstName: 'ddd', agree: false, tags: { resupply: 'ore' } });
          expect(table.getTableData()).toContain({ id: 5, firstName: 'abe', agree: true, tags: { resupply: 'lee' } });
          expect(table.getTableData()).toContain({ id: 6, firstName: 'bbe', agree: true, tags: { resupply: 'tes' } });
          expect(table.getTableData()).toContain({ id: 7, firstName: 'cbf', agree: false, tags: { resupply: 'lww' } });
          expect(table.getTableData()).toContain({ id: 8, firstName: 'dbf', agree: false, tags: { resupply: 'aoa' } });

          let searchText = 'c';
          table.addFilter(item =>
            (item.firstName.indexOf(searchText) > -1)
          );
          expect(table.getTableData()).not.toContain({ id: 1, firstName: 'aaa', agree: false, tags: { resupply: 'vcx' } });
          expect(table.getTableData()).not.toContain({ id: 2, firstName: 'bbb', agree: false, tags: { resupply: 'qew' } });
          expect(table.getTableData()).toContain({ id: 3, firstName: 'ccc', agree: true, tags: { resupply: 'fds' } });
          expect(table.getTableData()).not.toContain({ id: 4, firstName: 'ddd', agree: false, tags: { resupply: 'ore' } });
          expect(table.getTableData()).not.toContain({ id: 5, firstName: 'abe', agree: true, tags: { resupply: 'lee' } });
          expect(table.getTableData()).not.toContain({ id: 6, firstName: 'bbe', agree: true, tags: { resupply: 'tes' } });
          expect(table.getTableData()).toContain({ id: 7, firstName: 'cbf', agree: false, tags: { resupply: 'lww' } });
          expect(table.getTableData()).not.toContain({ id: 8, firstName: 'dbf', agree: false, tags: { resupply: 'aoa' } });

          searchText = 'cb';
          table.refreshData();
          expect(table.getTableData()).not.toContain({ id: 1, firstName: 'aaa', agree: false, tags: { resupply: 'vcx' } });
          expect(table.getTableData()).not.toContain({ id: 2, firstName: 'bbb', agree: false, tags: { resupply: 'qew' } });
          expect(table.getTableData()).not.toContain({ id: 3, firstName: 'ccc', agree: true, tags: { resupply: 'fds' } });
          expect(table.getTableData()).not.toContain({ id: 4, firstName: 'ddd', agree: false, tags: { resupply: 'ore' } });
          expect(table.getTableData()).not.toContain({ id: 5, firstName: 'abe', agree: true, tags: { resupply: 'lee' } });
          expect(table.getTableData()).not.toContain({ id: 6, firstName: 'bbe', agree: true, tags: { resupply: 'tes' } });
          expect(table.getTableData()).toContain({ id: 7, firstName: 'cbf', agree: false, tags: { resupply: 'lww' } });
          expect(table.getTableData()).not.toContain({ id: 8, firstName: 'dbf', agree: false, tags: { resupply: 'aoa' } });
        });

        it('should return boolean from hasFiltered', () => {
          table.setData(DATA);
          expect(table.hasFiltered()).toBe(false);
          table.addFilter(filterA);
          expect(table.hasFiltered()).toBe(true);
          table.removeFilter(filterA);
          expect(table.hasFiltered()).toBe(false);

          let searchText = 'c';
          table.addFilter(item =>
            (searchText === '') || (item.firstName.indexOf(searchText) > -1)
          );
          expect(table.hasFiltered()).toBe(true);

          searchText = '';
          table.refreshData();
          expect(table.hasFiltered()).toBe(false);
        });

        it('should return the correct length', () => {
          table.setData(DATA);
          expect(table.getLength()).toBe(DATA.length);
          expect(table.getUnfilteredLength()).toBe(DATA.length);

          table.addFilter(filterA, filterB);
          expect(table.getLength()).toBe(2);
          expect(table.getUnfilteredLength()).toBe(DATA.length);          
        });

        function filterA(a) {
          return a.id % 2 === 1;
        }
        function filterB(a) {
          return a.agree;
        }
      });

      describe('select', () => {
        beforeEach(() => {
          table.setData(DATA);
        });

        it('should select all', () => {
          expect(table.getSelection()).toEqual([]);

          table.select();
          const selection = table.getSelection();
          expect(selection.length).toBe(8);
          expect(selection).toContain(DATA[0]);
          expect(table.isSelected(DATA[0])).toBe(true);
          expect(selection).toContain(DATA[1]);
          expect(table.isSelected(DATA[1])).toBe(true);
          expect(selection).toContain(DATA[2]);
          expect(table.isSelected(DATA[2])).toBe(true);
          expect(selection).toContain(DATA[3]);
          expect(table.isSelected(DATA[3])).toBe(true);
          expect(selection).toContain(DATA[4]);
          expect(table.isSelected(DATA[4])).toBe(true);
          expect(selection).toContain(DATA[5]);
          expect(table.isSelected(DATA[5])).toBe(true);
          expect(selection).toContain(DATA[6]);
          expect(table.isSelected(DATA[6])).toBe(true);
          expect(selection).toContain(DATA[7]);
          expect(table.isSelected(DATA[7])).toBe(true);
        });

        it('should deselect all', () => {
          table.select();
          expect(table.getSelection().length).toBe(8);
          table.deselect();
          expect(table.getSelection()).toEqual([]);
        });

        it('should select 1 entry', () => {
          table.select(DATA[0]);
          expect(table.getSelection()).toEqual([DATA[0]]);
          expect(table.isSelected(DATA[0])).toBe(true);
        });

        it('should deselect 1 entry', () => {
          table.select();
          expect(table.isSelected(DATA[0])).toBe(true);
          table.deselect(DATA[0]);
          expect(table.getSelection()).not.toContain(DATA[0]);
          expect(table.isSelected(DATA[0])).toBe(false);
        });

        it('should select all currently filtered', () => {
          table.addFilter(filterA);
          table.refreshData();

          table.select();
          const selection = table.getSelection();
          expect(selection.length).toBe(2);
          expect(selection).toContain(DATA[2]);
          expect(selection).toContain(DATA[4]);

          function filterA(a) {
            return a.id % 2 === 1 && a.agree;
          }
        });

        it('should deselect all currently filtered', () => {
          table.select();
          table.addFilter(filterA);
          table.refreshData();
          expect(table.getSelection().length).toBe(8);

          table.deselect();
          const selection = table.getSelection();
          expect(selection.length).toBe(6);
          expect(selection).not.toContain(DATA[2]);
          expect(selection).not.toContain(DATA[4]);

          function filterA(a) {
            return a.id % 2 === 1 && a.agree;
          }
        });

        it('should toggleSelect', () => {
          let isCheckedAll = false;
          spyOn(table, 'isCheckedAll').and.callFake(() => isCheckedAll);
          spyOn(table, 'select').and.callThrough();
          spyOn(table, 'deselect').and.callThrough();
          table.toggleSelect();
          expect(table.select).toHaveBeenCalledWith();

          isCheckedAll = true;
          table.toggleSelect();
          expect(table.deselect).toHaveBeenCalledWith();

          expect(table.isSelected(DATA[0])).toBe(false);
          table.toggleSelect(DATA[0]);
          expect(table.select).toHaveBeenCalledWith(DATA[0]);

          expect(table.isSelected(DATA[0])).toBe(true);
          table.toggleSelect(DATA[0]);
          expect(table.deselect).toHaveBeenCalledWith(DATA[0]);
        });

        it('should return isCheckedAll', () => {
          expect(table.isCheckedAll()).toBe(false);
          table.select();
          expect(table.isCheckedAll()).toBe(true);
          table.addFilter(filterA);
          table.refreshData();
          expect(table.isCheckedAll()).toBe(true);
          table.deselect(DATA[1]);
          expect(table.isCheckedAll()).toBe(true);
          table.deselect(DATA[2]);
          expect(table.isCheckedAll()).toBe(false);
          table.deselect();
          expect(table.isCheckedAll()).toBe(false);
          table.select(DATA[2]);
          table.select(DATA[4]);
          expect(table.isCheckedAll()).toBe(false);
          table.addFilter(filterB);
          table.refreshData();
          expect(table.isCheckedAll()).toBe(true);

          function filterA(a) {
            return a.id % 2 === 1;
          }
          function filterB(a) {
            return a.agree;
          }
        });

        it('should return isSelectionIndeterminate', () => {
          let isCheckedAll = false;
          spyOn(table, 'isCheckedAll').and.callFake(() => isCheckedAll);

          expect(table.getSelection()).toEqual([]);
          expect(table.isSelectionIndeterminate()).toBe(false);

          table.select(DATA[0]);
          expect(table.getSelection().length).toBeGreaterThan(0);
          expect(table.isSelectionIndeterminate()).toBe(true);

          isCheckedAll = true;
          expect(table.isSelectionIndeterminate()).toBe(false);
        });
      });
    });
  });
}());
