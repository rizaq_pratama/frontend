(function test() {
  describe('listener.service', () => {
    const module = angular.mock.module;
    const inject = angular.mock.inject;

    beforeEach(module('nvCommons.services'));

    describe('Listener.create', () => {
      let listener;
      const callback = {
        fn: () => {},
        gn: () => {},
        hn: () => {},
      };

      beforeEach(inject((Listener) => {
        listener = Listener.create();
        spyOn(callback, 'fn');
        spyOn(callback, 'gn');
        spyOn(callback, 'hn');
      }));

      it('should be able to registered by listener.on', () => {
        expect(listener.cb.length).toBe(0);
        const off = listener.on(callback.fn);
        expect(listener.cb.length).toBe(1);
        expect(listener.cb[0]).toBe(callback.fn);
        expect(off).toEqual(jasmine.any(Function));
      });

      it('should be able to deregistered by listener.off', () => {
        listener.on(callback.fn);
        expect(listener.cb.length).toBe(1);
        listener.off(callback.fn);
        expect(listener.cb.length).toBe(0);
      });

      it('should return deregister function', () => {
        spyOn(listener, 'off').and.callThrough();
        const fn = listener.on(callback.fn);
        expect(listener.cb.length).toBe(1);
        fn();
        expect(listener.cb.length).toBe(0);
        expect(listener.off).toHaveBeenCalledWith(callback.fn);
      });

      it('should notify callback function on listener.notify', inject(($timeout) => {
        listener.on(callback.fn);
        expect(callback.fn).not.toHaveBeenCalled();
        listener.notify('a');
        expect(callback.fn).not.toHaveBeenCalled();
        $timeout.flush();
        expect(callback.fn).toHaveBeenCalledWith('a');
        listener.notify('b');
        $timeout.flush();
        expect(callback.fn).toHaveBeenCalledWith('b');
      }));

      it('should notify callback function with multiple arguments on listener.notify', inject(($timeout) => {
        listener.on(callback.fn);
        expect(callback.fn).not.toHaveBeenCalled();
        listener.notify('a', 'b', 'c', 'd');
        expect(callback.fn).not.toHaveBeenCalled();
        $timeout.flush();
        expect(callback.fn).toHaveBeenCalledWith('a', 'b', 'c', 'd');
      }));

      it('should notify callback function when argument changed listener.notifyChange', inject(($timeout) => {
        listener.on(callback.fn);
        expect(callback.fn).not.toHaveBeenCalled();
        listener.notifyChange('a');
        $timeout.flush();
        expect(callback.fn).toHaveBeenCalledWith('a');
        listener.notifyChange('a');
        $timeout.flush();
        expect(callback.fn.calls.count()).toEqual(1);
        listener.notifyChange('b');
        $timeout.flush();
        expect(callback.fn).toHaveBeenCalledWith('b');
      }));

      it('should notify callback function with multiple arguments on listener.notifyChange', inject(($timeout) => {
        listener.on(callback.fn);
        expect(callback.fn).not.toHaveBeenCalled();
        listener.notifyChange('a', 'b', 'c', 'd');
        expect(callback.fn).not.toHaveBeenCalled();
        $timeout.flush();
        expect(callback.fn).toHaveBeenCalledWith('a', 'b', 'c', 'd');
        listener.notifyChange('a', 'b', 'c', 'd');
        $timeout.flush();
        expect(callback.fn.calls.count()).toEqual(1);
        listener.notifyChange('b', 'c', 'd', 'e', 'f');
        $timeout.flush();
        expect(callback.fn).toHaveBeenCalledWith('b', 'c', 'd', 'e', 'f');
      }));

      it('should handle multiple callback function', inject(($timeout) => {
        listener.on(callback.fn);
        listener.on(callback.gn);
        listener.on(callback.hn);
        listener.notify('a');
        $timeout.flush();
        expect(listener.cb.length).toBe(3);
        expect(callback.fn).toHaveBeenCalledWith('a');
        expect(callback.gn).toHaveBeenCalledWith('a');
        expect(callback.hn).toHaveBeenCalledWith('a');
        listener.off(callback.gn);
        expect(listener.cb.length).toBe(2);
        listener.notify('b');
        $timeout.flush();
        expect(callback.fn).toHaveBeenCalledWith('b');
        expect(callback.gn).not.toHaveBeenCalledWith('b');
        expect(callback.hn).toHaveBeenCalledWith('b');
      }));

      it('should only register once when called listener.on multiple times', inject(($timeout) => {
        listener.on(callback.fn);
        listener.on(callback.fn);
        listener.on(callback.fn);
        expect(listener.cb.length).toBe(1);
        listener.notify('a');
        $timeout.flush();
        expect(callback.fn.calls.count()).toBe(1);
      }));

      it('should handle gracefully when the listener.off called multiple times with the same callback funciton', () => {
        listener.on(callback.fn);
        expect(listener.cb.length).toBe(1);
        expect(listenerOff).not.toThrow();
        expect(listenerOff).not.toThrow();
        expect(listener.cb.length).toBe(0);

        function listenerOff() {
          listener.off(callback.fn);
        }
      });

      it('should handle gracefully when non-function is passed in', inject(() => {
        let off = listener.on('a');
        expect(listener.cb.length).toEqual(0);
        expect(off).toEqual(jasmine.any(Function));
        expect(off).not.toThrow();

        expect(notifyMightThrow).not.toThrow();
        expect(offMightThrow).not.toThrow();

        function notifyMightThrow() {
          listener.notify('a');
        }
        function offMightThrow() {
          listener.off('a');
        }
      }));
    });
  });
}());
