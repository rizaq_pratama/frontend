(function test(){
    'use strict';
    describe('sound.service', () => {
        var module = angular.mock.module;
        var inject = angular.mock.inject;
        beforeEach(module('nvCommons.services'));

        it('should return name of sound file', inject(function(nvSoundService){
            expect(nvSoundService.getRackSoundFile('a')).toEqual('resources/sounds/en-A.mp3');
            expect(nvSoundService.getRackSoundFile('a','id')).toEqual('resources/sounds/id-A.mp3');
        }));
    });
})();