(function service() {
  describe('auth.service', () => {
    const module = angular.mock.module;
    const inject = angular.mock.inject;

    describe('$watch', () => {
      beforeEach(module('nvCommons.services'));
      beforeEach(inject(($cookies, _nvAuth_) => { // _nvAuth_ needed here to set up the $watch
        const cookies = {
          ninja_access_token: 'acc355-t0k3n',
          user: '{"serviceId":"sg,my,id"}',
        };
        spyOn($cookies, 'get').and.callFake(key =>
          cookies[key]
        );
      }));

      it('should update the default $http Authorization header', inject(($cookies, $http, $rootScope) => {
        $rootScope.$digest();
        expect($cookies.get).toHaveBeenCalledWith('ninja_access_token');
        expect($http.defaults.headers.common.Authorization).toBe('Bearer acc355-t0k3n');
      }));
    });

    describe('init', () => {
      beforeEach(module('nvCommons.services'));
      beforeEach(inject(($cookies) => {
        const cookies = {
          ninja_access_token: 'acc355-t0k3n',
          user: '{"user":"u53r", "serviceId":"sg,my,id"}',
        };
        spyOn($cookies, 'get').and.callFake(key =>
          cookies[key]
        );
      }));

      it('should set the $rootScope with the user cookie', inject(($cookies, $rootScope, nvAuth) => {
        nvAuth.init();
        expect($cookies.get).toHaveBeenCalledWith('user');
        expect($rootScope.user).toEqual({ user: 'u53r', serviceId: 'sg,my,id' });
      }));
    });

    describe('login', () => {
      beforeEach(module('nvCommons.services'));
      beforeEach(module('nvCommons.services', ($provide) => {
                // mock the $window service for the test
        $provide.value('$window', { location: { origin: 'https://banana.com' } });
      }));
      beforeEach(inject(($location, $cookies) => {
        spyOn($cookies, 'remove');
        spyOn($location, 'host').and.returnValue('api.banana.sg');
      }));
      beforeEach(inject((nvClientStore) => {
        spyOn(nvClientStore.localStorage, 'get').and.callFake(() =>
          'https://banana.com'
        );
      }));

      it('should remove the user and access token cookies', inject(($cookies, $rootScope, nvAuth) => {
        $rootScope.domains = ['sg', 'gpi'];
        nvAuth.login();
        expect($cookies.remove).toHaveBeenCalledWith('user', { path: '/', domain: '.banana.sg' });
        expect($cookies.remove).toHaveBeenCalledWith('ninja_access_token', { path: '/', domain: '.banana.sg' });
      }));

      it('should remove the user from the $rootScope', inject(($rootScope, nvAuth) => {
        nvAuth.login();
        expect($rootScope.user).toBeNull();
      }));

      it('should remove the default $http Authorization header', inject(($http, nvAuth) => {
        nvAuth.login();
        expect($http.defaults.headers.common.Authorization).toBeUndefined();
      }));

      it('should redirect to the auth server with a return url', inject(($window, nvAuth) => {
        nvAuth.login();
        expect($window.location.href).toBe(`${nv.config.servers.authlogin}/signin?nv_redirect=https://banana.com`);
      }));
    });

    describe('logout', () => {
      beforeEach(module('nvCommons.services'));
      beforeEach(inject(($cookies, $location) => {
        spyOn($cookies, 'remove');
        spyOn($location, 'host').and.returnValue('api.banana.sg');
      }));

      it('should remove the access token and user cookies', inject(($cookies, nvAuth) => {
        nvAuth.logout();
        expect($cookies.remove).toHaveBeenCalledWith('ninja_access_token', { path: '/', domain: '.banana.sg' });
        expect($cookies.remove).toHaveBeenCalledWith('user', { path: '/', domain: '.banana.sg' });
      }));

      it('should remove the user from the $rootScope', inject(($rootScope, nvAuth) => {
        nvAuth.logout();
        expect($rootScope.user).toBeNull();
      }));

      it('should remove the default $http Authorization header', inject(($http, nvAuth) => {
        nvAuth.logout();
        expect($http.defaults.headers.common.Authorization).toBeUndefined();
      }));

      it('should invoke the given callback', inject((nvAuth) => {
        const temp = { callback: angular.noop };
        spyOn(temp, 'callback');
        nvAuth.logout(temp.callback);
        expect(temp.callback).toHaveBeenCalled();
      }));
    });

    describe('isLoggedIn', () => {
      beforeEach(module('nvCommons.services'));

      it('should return truthy if there is an existing access token', inject(($cookies, nvAuth) => {
        spyOn($cookies, 'get').and.returnValue({ user: 'u53r' });
        expect(nvAuth.isLoggedIn()).toBeTruthy();
        expect($cookies.get).toHaveBeenCalledWith('user');
      }));

      it('should return falsy if there is no existing access token', inject(($cookies, nvAuth) => {
        spyOn($cookies, 'get');
        expect(nvAuth.isLoggedIn()).toBeFalsy();
        expect($cookies.get).toHaveBeenCalledWith('user');
      }));
    });

    describe('user', () => {
      beforeEach(module('nvCommons.services'));
      beforeEach(inject(($cookies) => {
        const cookies = {
          user: '{"user":"u53r", "serviceId":"sg,my,id"}',
        };
        spyOn($cookies, 'get').and.callFake(key =>
          cookies[key]
        );
      }));

      it('should return the currently logged-in user', inject((nvAuth) => {
        nvAuth.init();
        expect(nvAuth.user()).toEqual({ user: 'u53r', serviceId: 'sg,my,id' });
      }));
    });

    describe('getDomains', () => {
      beforeEach(module('nvCommons.services'));
      beforeEach(inject(($cookies) => {
        const cookies = {
          user: '{"user":"u53r", "serviceId":"sg,my,id,MBS"}',
        };
        spyOn($cookies, 'get').and.callFake(key =>
          cookies[key]
        );
      }));

      it('should return the list of domains based on the available access tokens', inject((nvAuth) => {
        nvAuth.init();
        expect(nvAuth.getDomains()).toEqual(['sg', 'my', 'id', 'mbs']);
      }));
    });
  });
}());
