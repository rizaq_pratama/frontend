(function test() {
  describe('nvDomain.service', () => {
    const module = angular.mock.module;
    const inject = angular.mock.inject;

    beforeEach(() => {
      module('ui.router', ($provide) => {
        $provide.value('$state', {
          go: jasmine.createSpy(),
        });
      });
      module('ngMaterial');
      module('nvCommons.runs');
      module('nvCommons.services');
    });

    it('should return the correct country name', inject((nvDomain) => {
      expect(nvDomain.getCountryName('sg')).toEqual('Singapore');
      expect(nvDomain.getCountryName('my')).toEqual('Malaysia');
      expect(nvDomain.getCountryName('id')).toEqual('Indonesia');
      expect(nvDomain.getCountryName('vn')).toEqual('Vietnam');
      expect(nvDomain.getCountryName('ph')).toEqual('Philippines');
      expect(nvDomain.getCountryName('th')).toEqual('Thailand');
      expect(nvDomain.getCountryName('mm')).toEqual('Myanmar');
    }));

    it('should reload domain', inject((nvDomain, $state) => {
      nvDomain.reloadDomain('sg', {});
      expect($state.go).toHaveBeenCalledWith($state.current, { domain: 'sg' }, { reload: true, inherit: false, notify: true });
    }));

    describe('nvDomain.getDomain', () => {
      beforeEach(inject(($rootScope) => {
        $rootScope.domain = 'sg';
        $rootScope.ninjasaas = true;
        $rootScope.domains = ['sg', 'my', 'mbs'];
      }));
      it('should return domain', inject((nvDomain) => {
        expect(nvDomain.getDomain()).toEqual({
          current: 'sg',
          currentCountry: 'sg',
          systemIdFromUrl: null,
          ninjasaas: true,
          all: ['sg', 'my', 'mbs'],
        });
      }));
    });

    describe('nvDomain.consumeStateParam', () => {
      beforeEach(() => {
        module(($provide) => {
          // mock the nvAuth service for the test
          $provide.value('nvAuth', {
            init: jasmine.createSpy(),
            getDomains: jasmine.createSpy().and.returnValue(['sg', 'my', 'ph', 'th']),
          });
        });
      });
      it('should change domain based on url', inject((nvDomain, $rootScope, nvDOMManipulation) => {
        nvDomain.init();
        spyOn(nvDOMManipulation, 'reloadFavicon');
        $rootScope.domain = 'sg';
        nvDomain.consumeStateParam({ domain: 'ph' });
        expect($rootScope.domain).toEqual('ph');
        expect(nvDomain.getDomain().current).toEqual('ph');
        expect(nvDOMManipulation.reloadFavicon).toHaveBeenCalledWith('ph');
      }));
      it('should reload if domain is not in in nvAuth.getDomains()', inject((nvDomain, $rootScope, $state) => {
        nvDomain.init();
        $rootScope.domain = 'sg';
        nvDomain.consumeStateParam({ domain: 'vn' });
        expect($state.go).toHaveBeenCalledWith($state.current, { domain: 'sg' }, { reload: true, inherit: false, notify: true });
      }));
    });

    describe('nvDomain.init', () => {
      beforeEach(inject((nvAuth) => {
        spyOn(nvAuth, 'getDomains').and.returnValue([]);
      }));

      it('should call $rootScope.logout if there is no domain', inject((nvDomain, $rootScope) => {
        $rootScope.logout = () => {};

        spyOn($rootScope, 'logout');
        nvDomain.init();
        expect($rootScope.logout).toHaveBeenCalled();
      }));
    });
  });
}());
