(function test() {
  describe('utils.service', () => {
    const module = angular.mock.module;
    const inject = angular.mock.inject;

    beforeEach(module('nvCommons.services'));

    // unit test based on example
    describe('nvUtils.defaults', () => {
      it('should do at least it claims to do in example', inject((nvUtils) => {
        const a = { foo: '', bar: 'monkey' };
        nvUtils.defaults(a, { foo: 2 }, { bar: 'banana' }, { baz: 'qux' });
        expect(a).toEqual({ foo: 2, bar: 'monkey', baz: 'qux' });
      }));
    });

    describe('nvUtils.getOrElse', () => {
      it('should do at least it claims to do in example', inject((nvUtils) => {
        expect(nvUtils.getOrElse({}, 'foo.bar', 'baz')).toEqual('baz');
        expect(nvUtils.getOrElse({ foo: { bar: 'qux' } }, 'foo.bar', 'baz')).toEqual('qux');
      }));
    });

    describe('nvUtils.getDeep', () => {
      it('should do at least it claims to do in example', inject((nvUtils) => {
        expect(nvUtils.getDeep({ custom: 'a' }, 'custom.operatingHours')).toBeUndefined();
        expect(nvUtils.getDeep({ custom: { operatingHours: 'qux' } }, 'custom.operatingHours')).toEqual('qux');
      }));
    });

    describe('nvUtils.createSnapshot', () => {
      it('should have snapshot be the same as original', inject((nvUtils) => {
        const original = { custom: 'a', company: { name: 'ninjavan', business: 'logistic' } };
        const snapshot = nvUtils.createSnapshot(original);
        expect(snapshot._snapshot).toEqual(original);
      }));

      it('should not change the snapshot when original has changed', inject((nvUtils) => {
        const original = { custom: 'a', company: { name: 'ninjavan', business: 'logistic' } };
        const snapshot = nvUtils.createSnapshot(original);
        original.custom = 'b';
        original.company.name = 'marcella';
        original.newAttr = 'c';
        expect(snapshot._snapshot).toEqual({ custom: 'a', company: { name: 'ninjavan', business: 'logistic' } });
      }));

      it('should have a revert function that can revert back to snapshot', inject((nvUtils) => {
        const original = { custom: 'a', company: { name: 'ninjavan', business: 'logistic' } };
        const snapshot = nvUtils.createSnapshot(original);
        snapshot.custom = 'b';
        snapshot.company.name = 'marcella';
        snapshot.newAttr = 'c';
        expect(snapshot).not.toEqual(jasmine.objectContaining(original));
        expect(snapshot.revert).toEqual(jasmine.any(Function));
        snapshot.revert();
        expect(snapshot).toEqual(jasmine.objectContaining(original));
      }));
    });

    describe('nvUtils.mapToObjectWith', () => {
      it('should not mutate the array', inject((nvUtils) => {
        const a = getArray();
        nvUtils.mapToObjectWith(a, ['id', 'name']);
        expect(a).toEqual(getArray());
      }));

      it('should accept array in property', inject((nvUtils) => {
        const a = getArray();
        const b = nvUtils.mapToObjectWith(a, ['id', 'name']);
        expect(b).toEqual([{ id: 213, name: 'Lazada (Nicedeal)' },
                                   { id: 214, name: 'Zalora (Nicedeal)' },
                                   { id: 215, name: 'Marcella' },
                                   { id: 216, name: 'LOVEBONITO SINGAPORE PTE LTD' }]);
      }));

      it('should accept list of properties', inject((nvUtils) => {
        const a = getArray();
        const b = nvUtils.mapToObjectWith(a, 'id', 'name');
        expect(b).toEqual([{ id: 213, name: 'Lazada (Nicedeal)' },
                                   { id: 214, name: 'Zalora (Nicedeal)' },
                                   { id: 215, name: 'Marcella' },
                                   { id: 216, name: 'LOVEBONITO SINGAPORE PTE LTD' }]);
      }));
    });

    describe('nvUtils.mapToObjectWithout', () => {
      it('should not mutate the array', inject((nvUtils) => {
        const a = getArray();
        nvUtils.mapToObjectWithout(a, ['id', 'name']);
        expect(a).toEqual(getArray());
      }));

      it('should accept array in property', inject((nvUtils) => {
        const a = getArray();
        const b = nvUtils.mapToObjectWithout(a, ['shortName', 'email', 'companyName', 'billingName', 'contact']);
        expect(b).toEqual([{ id: 213, name: 'Lazada (Nicedeal)' },
                                   { id: 214, name: 'Zalora (Nicedeal)' },
                                   { id: 215, name: 'Marcella' },
                                   { id: 216, name: 'LOVEBONITO SINGAPORE PTE LTD' }]);
      }));

      it('should accept list of properties', inject((nvUtils) => {
        const a = getArray();
        const b = nvUtils.mapToObjectWithout(a, 'shortName', 'email', 'companyName', 'billingName', 'contact');
        expect(b).toEqual([{ id: 213, name: 'Lazada (Nicedeal)' },
                                   { id: 214, name: 'Zalora (Nicedeal)' },
                                   { id: 215, name: 'Marcella' },
                                   { id: 216, name: 'LOVEBONITO SINGAPORE PTE LTD' }]);
      }));
    });

    describe('nvUtils.mergeIn', () => {
      it('should mutate the array', inject((nvUtils) => {
        const a = getArray();
        nvUtils.mergeIn(a, { a: 1 }, 'id', '1');
        expect(a).not.toEqual(getArray());
      }));

      it('should merge in if object present in array', inject((nvUtils) => {
        const a = getArray();
        const b = angular.copy(a);
        const length = a.length;
        nvUtils.mergeIn(a, { ninja: 'van', name: 'b' }, 'id', 213);
        // expect the array maintain the same length
        expect(a.length).toEqual(length);
        // all other object property did not change
        for (let i = 1; i < length; i += 1) {
          expect(a[i]).toEqual(b[i]);
        }
        // property merged in for the existing object
        expect(a[0]).toEqual({ id: 213,
                    name: 'b',
                    ninja: 'van',
                    shortName: 'Lazada',
                    email: 'lazada@nicedeal.sg',
                    companyName: 'Lazada Singapore Pte Ltd',
                    billingName: 'Lazada Singapore Pte Ltd',
                    contact: '67341322',
                  });
      }));

      it('should append if object not present in array', inject((nvUtils) => {
        const a = getArray();
        const b = angular.copy(a);
        const length = a.length;
        nvUtils.mergeIn(a, { ninja: 'van', name: 'b' }, 'id', 217);
        expect(a.length).not.toEqual(length);
        expect(a.length).toEqual(length + 1);
        for (let i = 0; i < length; i += 1) {
          expect(a[i]).toEqual(b[i]);
        }
        expect(a[a.length - 1]).toEqual({ ninja: 'van', name: 'b' });
      }));
    });

    describe('nvUtils.mergeAllIn', () => {
      it('should merge all in, and overwrite if exist', inject((nvUtils) => {
        const a = [{ id: 1, name: 'ninjavan', ninja: 'nnn' },
                   { id: 2, name: 'gautammm', ninja: 'ggg' },
                   { id: 3, name: 'kwanghoc', ninja: 'kkk' },
                   { id: 4, name: 'lihautan', ninja: 'lll' },
                   { id: 5, name: 'limiardi', ninja: 'iii' }];

        const b = [{ id: 3, name: 'clydetan', ninja: 'ccc' },
                   { id: 5, name: 'shaunaun', ninja: 'sss' },
                   { id: 6, name: 'akileshh', ninja: 'aaa' },
                   { id: 7, name: 'rizaqqaz', ninja: 'rrr' },
                   { id: 8, name: 'lucianni', ninja: 'uuu' }];

        const c = nvUtils.mergeAllIn(a, b, 'id');
        expect(c.length).toBe(8);
        expect(c).toContain({ id: 1, name: 'ninjavan', ninja: 'nnn' });
        expect(c).toContain({ id: 2, name: 'gautammm', ninja: 'ggg' });
        expect(c).toContain({ id: 3, name: 'clydetan', ninja: 'ccc' });
        expect(c).toContain({ id: 4, name: 'lihautan', ninja: 'lll' });
        expect(c).toContain({ id: 5, name: 'shaunaun', ninja: 'sss' });
        expect(c).toContain({ id: 6, name: 'akileshh', ninja: 'aaa' });
        expect(c).toContain({ id: 7, name: 'rizaqqaz', ninja: 'rrr' });
        expect(c).toContain({ id: 8, name: 'lucianni', ninja: 'uuu' });
      }));
      it('should work find when array2 is longer than array1', inject((nvUtils) => {
        const a = [{ id: 1, name: 'ninjavan', ninja: 'nnn' },
                   { id: 2, name: 'gautammm', ninja: 'ggg' },
                   { id: 3, name: 'kwanghoc', ninja: 'kkk' }];

        const b = [{ id: 3, name: 'clydetan', ninja: 'ccc' },
                   { id: 5, name: 'shaunaun', ninja: 'sss' },
                   { id: 6, name: 'akileshh', ninja: 'aaa' },
                   { id: 7, name: 'rizaqqaz', ninja: 'rrr' },
                   { id: 8, name: 'lucianni', ninja: 'uuu' }];

        const c = nvUtils.mergeAllIn(a, b, 'id');
        expect(c.length).toBe(7);
        expect(c).toContain({ id: 1, name: 'ninjavan', ninja: 'nnn' });
        expect(c).toContain({ id: 2, name: 'gautammm', ninja: 'ggg' });
        expect(c).toContain({ id: 3, name: 'clydetan', ninja: 'ccc' });
        expect(c).toContain({ id: 5, name: 'shaunaun', ninja: 'sss' });
        expect(c).toContain({ id: 6, name: 'akileshh', ninja: 'aaa' });
        expect(c).toContain({ id: 7, name: 'rizaqqaz', ninja: 'rrr' });
        expect(c).toContain({ id: 8, name: 'lucianni', ninja: 'uuu' });
      }));
    });

    describe('nvUtils.mergeAllIntoFirst', () => {
      it('should merge into 1st array', inject((nvUtils) => {
        const a = [{ id: 1, name: 'ninjavan', ninja: 'nnn', extra: 'a' },
                   { id: 2, name: 'gautammm', ninja: 'ggg' },
                   { id: 3, name: 'kwanghoc', ninja: 'kkk', extra: 'c' },
                   { id: 4, name: 'lihautan', ninja: 'lll' },
                   { id: 5, name: 'limiardi', ninja: 'iii' }];

        const b = [{ id: 3, name: 'clydetan', ninja: 'ccc', extra: 'b' },
                   { id: 5, name: 'shaunaun', ninja: 'sss', extra: 'd', not: 'e' },
                   { id: 6, name: 'akileshh', ninja: 'aaa' },
                   { id: 7, name: 'rizaqqaz', ninja: 'rrr' },
                   { id: 8, name: 'lucianni', ninja: 'uuu' }];

        const c = nvUtils.mergeAllIntoFirst(a, b, 'id');
        expect(c.length).toBe(5);
        expect(c).toContain({ id: 1, name: 'ninjavan', ninja: 'nnn', extra: 'a' });
        expect(c).toContain({ id: 2, name: 'gautammm', ninja: 'ggg' });
        expect(c).toContain({ id: 3, name: 'clydetan', ninja: 'ccc', extra: 'b' });
        expect(c).toContain({ id: 4, name: 'lihautan', ninja: 'lll' });
        expect(c).toContain({ id: 5, name: 'shaunaun', ninja: 'sss', extra: 'd', not: 'e' });
      }));

      it('should merge into 2nd array', inject((nvUtils) => {
        const a = [{ id: 1, name: 'ninjavan', ninja: 'nnn', extra: 'a' },
                   { id: 2, name: 'gautammm', ninja: 'ggg' },
                   { id: 3, name: 'kwanghoc', ninja: 'kkk', extra: 'c' },
                   { id: 4, name: 'lihautan', ninja: 'lll' },
                   { id: 5, name: 'limiardi', ninja: 'iii' }];

        const b = [{ id: 3, name: 'clydetan', ninja: 'ccc', extra: 'b' },
                   { id: 5, name: 'shaunaun', ninja: 'sss', extra: 'd', not: 'e' },
                   { id: 6, name: 'akileshh', ninja: 'aaa' },
                   { id: 7, name: 'rizaqqaz', ninja: 'rrr' },
                   { id: 8, name: 'lucianni', ninja: 'uuu' }];

        const c = nvUtils.mergeAllIntoFirst(a, b, 'id', { overrideFirst: false });
        expect(c.length).toBe(5);
        expect(c).toContain({ id: 1, name: 'ninjavan', ninja: 'nnn', extra: 'a' });
        expect(c).toContain({ id: 2, name: 'gautammm', ninja: 'ggg' });
        expect(c).toContain({ id: 3, name: 'kwanghoc', ninja: 'kkk', extra: 'c' });
        expect(c).toContain({ id: 4, name: 'lihautan', ninja: 'lll' });
        expect(c).toContain({ id: 5, name: 'limiardi', ninja: 'iii', extra: 'd', not: 'e' });
      }));
    });

    function getArray() {
      return [{ id: 213,
                name: 'Lazada (Nicedeal)',
                shortName: 'Lazada',
                email: 'lazada@nicedeal.sg',
                companyName: 'Lazada Singapore Pte Ltd',
                billingName: 'Lazada Singapore Pte Ltd',
                contact: '67341322',
              },
              { id: 214,
                name: 'Zalora (Nicedeal)',
                shortName: 'Zalora',
                email: 'zalora@nicedeal.sg',
                companyName: 'Zalora Singapore Pte Ltd',
                billingName: 'Zalora Singapore Pte Ltd',
                contact: '69832213',
              },
              { id: 215,
                name: 'Marcella',
                shortName: 'Marcella',
                email: 'firas@marcella.com.sg',
                companyName: 'Marcella Holdings Pte. Ltd',
                billingName: 'Marcella Holdings Pte. Ltd',
                contact: '63534216',
              },
              { id: 216,
                name: 'LOVEBONITO SINGAPORE PTE LTD',
                shortName: 'Love, Bonito',
                email: 'jonpoon@lovebonito.com',
                companyName: 'VRV Pte Ltd',
                billingName: 'LOVEBONITO SINGAPORE PTE LTD',
                contact: '98783211',
              }];
    }
  });
}());
