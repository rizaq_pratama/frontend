(function() {
    'use strict';

    describe('translate.service', function() {

        var module = angular.mock.module;
        var inject = angular.mock.inject;
        var nvTranslate, nvClientStore, $translate, $rootScope, $timeout;

        beforeEach(module('pascalprecht.translate', function($provide, $translateProvider){
            // emulation of making translate mock
            var injector = angular.injector(['ng']);
            var $q = injector.get('$q');

            var $translateMock = jasmine.createSpy().and.callFake(function(translationId, interpolateParams, interpolationId, defaultTranslationText, forceLanguage){
                var defer = $q.defer();
                defer.resolve(translationId + interpolationId + defaultTranslationText + forceLanguage + JSON.stringify(interpolateParams));
                return defer.promise;
            });
            $translateMock.instant = jasmine.createSpy().and.callFake(function(translationId, interpolateParams, interpolationId, forceLanguage){
                return translationId + JSON.stringify(interpolateParams) + interpolationId + forceLanguage;
            });
            $translateMock.storageKey = function(){ 
                return ''; 
            };
            $translateMock.storage = function(){
                return { 
                    get: function(){
                        return '';
                    }
                };
            };
            $translateMock.preferredLanguage = jasmine.createSpy().and.returnValue('en');
            $translateMock.use = jasmine.createSpy().and.callFake(function(code){
                // create a mock translate
                var defer = $q.defer();
                if(code === 'fr'){
                    return $translateMock.preferredLanguage();
                } else {
                    defer.resolve({a: 'a', b: 'b'});
                    return defer.promise;
                }
            });

            $provide.value('$translate', $translateMock);
        }));

        beforeEach(module('nvCommons.services'));

        beforeEach(function(){
            spyOn(console, 'error');
        });

        beforeEach(inject(function(_$rootScope_, _$translate_, _$timeout_, _nvTranslate_, _nvClientStore_){
            $translate = _$translate_;
            $rootScope = _$rootScope_;
            $timeout = _$timeout_;
            nvTranslate = _nvTranslate_;
            nvClientStore = _nvClientStore_;
        }));

        describe('init', function(){
            beforeEach(function(){
                spyOn(nvTranslate, 'getLanguageCode').and.returnValue('nv');
            });

            it('should call $translate.use with the value return by nvTranslate.getAvailableLanguages', function(){
                nvTranslate.init();
                var value = nvTranslate.getLanguageCode();
                expect($translate.use).toHaveBeenCalledWith(value);
            });
        });

        describe('getAvailableLanguages', function(){
            it('should return available languages', function(){
                expect(nvTranslate.getAvailableLanguages()).toEqual([
                    { code: 'en', displayName: 'English'    },
                    { code: 'ms', displayName: 'Malay'      },
                    { code: 'id', displayName: 'Indonesian' },
                    { code: 'vn', displayName: 'Vietnam' }
                ]);
            });
        });

        describe('getLanguageCode', function(){
            var user;
            beforeEach(function(){
                user = $rootScope.user;
            });

            afterEach(function(){
                $rootScope.user = user;
            });

            it('should return localstorage first if it is available', function(){
                spyOn(nvClientStore.localStorage, 'get').and.returnValue('nv');
                $rootScope.user = { language: 'vn' };
                expect(nvTranslate.getLanguageCode()).toEqual('nv');
                expect(nvClientStore.localStorage.get).toHaveBeenCalledWith('language');
            });

            it('should return $rootScope.user.language next if it is unavailable', function(){
                spyOn(nvClientStore.localStorage, 'get').and.returnValue(undefined);
                $rootScope.user = { language: 'vn' };
                expect(nvTranslate.getLanguageCode()).toEqual('vn');
                expect(nvClientStore.localStorage.get).toHaveBeenCalledWith('language');
            });

            it('should return \'en\' next if $rootScope.user is unavailable', function(){
                spyOn(nvClientStore.localStorage, 'get').and.returnValue(undefined);
                $rootScope.user = undefined;
                expect(nvTranslate.getLanguageCode()).toEqual('en');
                expect(nvClientStore.localStorage.get).toHaveBeenCalledWith('language');
            });

            it('should return \'en\' next if $rootScope.user.language is unavailable', function(){
                spyOn(nvClientStore.localStorage, 'get').and.returnValue(undefined);
                $rootScope.user = {};
                expect(nvTranslate.getLanguageCode()).toEqual('en');
                expect(nvClientStore.localStorage.get).toHaveBeenCalledWith('language');
            });

            it('should return \'en\' next if $rootScope.user.language is \'\'', function(){
                spyOn(nvClientStore.localStorage, 'get').and.returnValue(undefined);
                $rootScope.user = { language: '' };
                expect(nvTranslate.getLanguageCode()).toEqual('en');
                expect(nvClientStore.localStorage.get).toHaveBeenCalledWith('language');
            });
        });

        describe('translate', function() {
            it('should called $translate', function(){
                nvTranslate.translate('abc');
                expect($translate).toHaveBeenCalledWith('abc');
                nvTranslate.translate('abc', 'def');
                expect($translate).toHaveBeenCalledWith('abc', 'def');
                nvTranslate.translate('abc', 'ninjavan', 'marcella');
                expect($translate).toHaveBeenCalledWith('abc', 'ninjavan', 'marcella');
                nvTranslate.translate('abc', 'ninjavan', 'marcella', 'nv');
                expect($translate).toHaveBeenCalledWith('abc', 'ninjavan', 'marcella', 'nv');
                nvTranslate.translate('abc', {a: 1, b: 2}, 'ninjavan', 'marcella');
                expect($translate).toHaveBeenCalledWith('abc', {a: 1, b:2 }, 'ninjavan', 'marcella');
            });

            it('should return a promise', function(){
                expect(nvTranslate.translate('abc').then).not.toBeUndefined();
                expect(nvTranslate.translate('abc', 'def').then).not.toBeUndefined();
                expect(nvTranslate.translate('abc', 'ninjavan', 'marcella').then).not.toBeUndefined();
                expect(nvTranslate.translate('abc', 'ninjavan', 'marcella', 'nv').then).not.toBeUndefined();
                expect(nvTranslate.translate('abc', {a: 1, b: 2}, 'ninjavan', 'marcella').then).not.toBeUndefined();
            });

            it('should called $translate', function(done){
                $translate('abc')
                .then(function(original){
                    return nvTranslate.translate('abc').then(function(translated){
                        expect(translated).toBe(original);
                    });
                }).then(function(){
                    return $translate('abc', 'def');
                }).then(function(original){
                    return nvTranslate.translate('abc', 'def').then(function(translated){
                        expect(translated).toBe(original);
                    });
                }).then(function(){
                    return $translate('abc', 'ninjavan', 'marcella');    
                }).then(function(original){
                    return nvTranslate.translate('abc', 'ninjavan', 'marcella').then(function(translated){
                        expect(translated).toBe(original);
                    });
                }).then(function(){
                    return $translate('abc', 'ninjavan', 'marcella', 'nv');    
                }).then(function(original){
                    return nvTranslate.translate('abc', 'ninjavan', 'marcella', 'nv').then(function(translated){
                        expect(translated).toBe(original);
                    });
                }).then(function(){
                    return $translate('abc', {a: 1, b: 2}, 'ninjavan', 'marcella');    
                }).then(function(original){
                    return nvTranslate.translate('abc', {a: 1, b: 2}, 'ninjavan', 'marcella').then(function(translated){
                        expect(translated).toBe(original);
                    });
                }).then(done);
            });
        });

        describe('instant', function() {
            it('should called $translate.instant', function(){
                nvTranslate.instant('abc');
                expect($translate.instant).toHaveBeenCalledWith('abc');
                nvTranslate.instant('abc', {a: 1, b: 2});
                expect($translate.instant).toHaveBeenCalledWith('abc', {a: 1, b: 2});
                nvTranslate.instant('abc', {a: 1}, 'ninjavan');
                expect($translate.instant).toHaveBeenCalledWith('abc', {a: 1}, 'ninjavan');
                nvTranslate.instant('abc', {a: 1}, 'ninjavan', 'nv');
                expect($translate.instant).toHaveBeenCalledWith('abc', {a: 1}, 'ninjavan', 'nv');
            });

            it('should return what would have returned from $translate.instant', function(){
                expect(nvTranslate.instant('abc'))
                    .toEqual($translate.instant('abc'));

                expect(nvTranslate.instant('abc', {a: 1, b: 2}))
                    .toEqual($translate.instant('abc', {a: 1, b: 2}));

                expect(nvTranslate.instant('abc', {a: 1}, 'ninjavan'))
                    .toEqual($translate.instant('abc', {a: 1}, 'ninjavan'));

                expect(nvTranslate.instant('abc', {a: 1}, 'ninjavan', 'nv'))
                    .toEqual($translate.instant('abc', {a: 1}, 'ninjavan', 'nv'));
            });
        });

        describe('setLanguageCode', function(){
            it('should call $translate.use', function(){
                nvTranslate.setLanguageCode('en');
                expect($translate.use).toHaveBeenCalledWith('en');
            });

            it('should call nvClientStore.localStorage.set', function(){
                spyOn(nvClientStore.localStorage, 'set').and.returnValue(true);
                nvTranslate.setLanguageCode('fr');
                expect(nvClientStore.localStorage.set).toHaveBeenCalledWith('language', 'fr');
                nvTranslate.setLanguageCode('en');
                expect(nvClientStore.localStorage.set).toHaveBeenCalledWith('language', 'en');
            });

            it('should print error if code is not available', function(){
                nvTranslate.setLanguageCode('en');
                expect(console.error).not.toHaveBeenCalled();
                nvTranslate.setLanguageCode('fr');
                expect(console.error).toHaveBeenCalledWith('fr is not a valid language key');
            });

            it('should return what $translate.use return if code is not available', function(){
                var code = $translate.use('fr');
                expect(nvTranslate.setLanguageCode('fr')).toEqual(code);
            });

            it('should return a promise if code is available', function(){
                var promise = nvTranslate.setLanguageCode('en');
                expect(promise.then).not.toBeUndefined();
            });
        });

        describe('onLanguageChanged', function(){
            var callback = {
                fn: function(){}
            };
            beforeEach(function(){
                spyOn(callback, 'fn');
            });

            it('should notify callback registered', function(done){
                nvTranslate.onLanguageChanged(callback.fn);
                expect(callback.fn).not.toHaveBeenCalled();
                nvTranslate.setLanguageCode('en').then(function(){
                    $timeout.flush();
                    expect(callback.fn).toHaveBeenCalled();
                    done();
                });
                $rootScope.$apply();
            });
        });
    });
})();
