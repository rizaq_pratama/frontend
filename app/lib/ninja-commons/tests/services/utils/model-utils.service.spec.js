(function() {
    'use strict';

    describe('model-utils.service', function() {

        var module = angular.mock.module;
        var inject = angular.mock.inject;

        beforeEach(module('nvCommons.services'));

        // unit test based on example
        describe('nvUtils.fromBoolToYesNoBitString', function(){
            it('should return 10 for true', inject(function(nvModelUtils){
                expect(nvModelUtils.fromBoolToYesNoBitString(true)).toBe('10');
            }));
            it('should return 01 for falsy', inject(function(nvModelUtils){
                expect(nvModelUtils.fromBoolToYesNoBitString(false)).toBe('01');
                expect(nvModelUtils.fromBoolToYesNoBitString(null)).toBe('01');
                expect(nvModelUtils.fromBoolToYesNoBitString(undefined)).toBe('01');
                expect(nvModelUtils.fromBoolToYesNoBitString(0)).toBe('01');
                expect(nvModelUtils.fromBoolToYesNoBitString(NaN)).toBe('01');
                expect(nvModelUtils.fromBoolToYesNoBitString('')).toBe('01');
                expect(nvModelUtils.fromBoolToYesNoBitString("")).toBe('01');
            }));
        });

        describe('nvUtils.fromYesNoBitStringToBool', function(){
            it('should return true for \'10\'', inject(function(nvModelUtils){
                expect(nvModelUtils.fromYesNoBitStringToBool('10')).toBe(true);
            }));

            it('should return false for \'01\'', inject(function(nvModelUtils){
                expect(nvModelUtils.fromYesNoBitStringToBool('01')).toBe(false);
            }));
        });

        describe('nvUtils.fromYesNoBitStringToNumBit', function(){
            it('should return 1 for \'10\'', inject(function(nvModelUtils){
                expect(nvModelUtils.fromYesNoBitStringToNumBit('10')).toBe(1);
            }));

            it('should return 0 for \'01\'', inject(function(nvModelUtils){
                expect(nvModelUtils.fromYesNoBitStringToNumBit('01')).toBe(0);
            }));
        });
    });

})();
