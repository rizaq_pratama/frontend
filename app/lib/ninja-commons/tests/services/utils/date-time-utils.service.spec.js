(function() {
    'use strict';

    describe('date-time-utils.service', function() {

        var module = angular.mock.module;
        var inject = angular.mock.inject;

        var nvDateTimeUtils;

        beforeEach(module('nvCommons.services'));
        beforeEach(inject(function(_nvDateTimeUtils_){
            nvDateTimeUtils = _nvDateTimeUtils_;
            spyOn(console, 'warn');
        }));

        describe('nvDateTimeUtils.compareDate', function(){
            it('should return false when pass in something not date or moment', function() {
                expect(nvDateTimeUtils.compareDate(new Date(), 1)).toBe(false);
                expect(nvDateTimeUtils.compareDate(1, new Date())).toBe(false);
                expect(nvDateTimeUtils.compareDate(1, 'ninjavan')).toBe(false);
                expect(nvDateTimeUtils.compareDate(moment(), 'ninjavan')).toBe(false);
                expect(nvDateTimeUtils.compareDate('ninjavan', moment())).toBe(false);
                expect(nvDateTimeUtils.compareDate('ninjavan', undefined)).toBe(false);
                expect(nvDateTimeUtils.compareDate(null, 'ninjavan')).toBe(false);
            });

            it('should return true when the date are same', function() {
                expect(nvDateTimeUtils.compareDate(new Date(2015,1,5), new Date(2015, 1, 5))).toBe(true);
                expect(nvDateTimeUtils.compareDate(new Date(2016,8,7), moment([2016,8,7]))).toBe(true);
                expect(nvDateTimeUtils.compareDate(moment([2014,3,2]), new Date(2014,3,2))).toBe(true);
                expect(nvDateTimeUtils.compareDate(moment([2011,6,12]), moment([2011,6,12]))).toBe(true);
            });

            it('should return false when the date are not the same', function() {
                expect(nvDateTimeUtils.compareDate(new Date(2015,1,10), new Date(2015, 1, 5))).toBe(false);
                expect(nvDateTimeUtils.compareDate(new Date(2016,3,7), moment([2016,5,7]))).toBe(false);
                expect(nvDateTimeUtils.compareDate(moment([2014,6,19]), new Date(2015,3,2))).toBe(false);
                expect(nvDateTimeUtils.compareDate(moment([2011,6,18]), moment([2011,8,12]))).toBe(false);
            });
        });

        describe('nvDateTimeUtils.FORMAT_*', function(){
            it('should be a constant', function(){
                expect(nvDateTimeUtils.FORMAT).toBe('YYYY-MM-DD HH:mm:ss');
                expect(nvDateTimeUtils.FORMAT_DATE).toBe('YYYY-MM-DD');
                expect(nvDateTimeUtils.FORMAT_TIME).toBe('HH:mm:ss');
                expect(nvDateTimeUtils.FORMAT_INTERNATIONAL_TIME).toBe('HHmm');
                expect(nvDateTimeUtils.FORMAT_ISO).toBe('YYYY-MM-DDTHH:mm:ssZ');
            });
        });

        describe('nvDateTimeUtils.toDate', function(){
            it('should return a date object which is equivalent to nvDateTimeUtils.toMoment(time, timezone).toDate()', function() {
                //number
                checkEquivalent(0);
                //string
                //'YYYY-MM-DD HH:mm:ss', 'YYYY-MM-DDTHH:mm:ssZ', 'HH:mm:ss', 'YYYY-MM-DD'
                checkEquivalent('2016-01-01 00:00:00');
                checkEquivalent('2016-01-01T00:00:00Z');
                checkEquivalent('12:13:14');
                checkEquivalent('2016-12-14');
                //date
                checkEquivalent(new Date());
                //moment
                checkEquivalent(moment());

                function checkEquivalent(date){
                    expect(nvDateTimeUtils.toDate(date)).toEqual(jasmine.any(Date));
                    expect(nvDateTimeUtils.toDate(date)).toEqual(nvDateTimeUtils.toMoment(date).toDate());
                    expect(nvDateTimeUtils.toDate(date, 'Asia/Singapore')).toEqual(jasmine.any(Date));
                    expect(nvDateTimeUtils.toDate(date, 'Asia/Singapore')).toEqual(nvDateTimeUtils.toMoment(date, 'Asia/Singapore').toDate());
                }
            });

            it('should return null when nvDateTimeUtils.toMoment(time, timezone) returns an invalid moment', function(){
                expect(nvDateTimeUtils.toDate(true)).toBeNull();
                expect(nvDateTimeUtils.toDate({ company: 'ninjavan' })).toBeNull();
            });
        });

        describe('nvDateTimeUtils.toMoment', function(){
            it('should take number as milliseconds since Unix Epoch', function(){
                checkUTCTime(nvDateTimeUtils.toMoment(0), 1970, 1-1, 1, 0, 0, 0, 0);
                checkUTCTime(nvDateTimeUtils.toMoment(0, 'Asia/Singapore'), 1970, 1-1, 1, 0, 0, 0, 0);
            });

            it('should convert the date string into date object', function() {
                var localTime = new Date(2016, 1-1, 1, 0, 0, 0, 0);
                var today;

                checkUTCTime(nvDateTimeUtils.toMoment('2016-01-01 00:00:00'), localTime.getUTCFullYear(), 
                                                                          localTime.getUTCMonth(), 
                                                                          localTime.getUTCDate(), 
                                                                          localTime.getUTCHours(), 
                                                                          localTime.getUTCMinutes(), 
                                                                          localTime.getUTCSeconds(), 
                                                                          localTime.getUTCMilliseconds());
                checkUTCTime(nvDateTimeUtils.toMoment('2016-01-01 00:00:00', 'Asia/Singapore'), 2015, 12-1, 31, 16, 0, 0, 0);

                checkUTCTime(nvDateTimeUtils.toMoment('2016-01-01T00:00:00Z'), 2016, 1-1, 1, 0, 0, 0, 0);
                checkUTCTime(nvDateTimeUtils.toMoment('2016-01-01T00:00:00Z', 'Asia/Singapore'), 2016, 1-1, 1, 0, 0, 0, 0);

                today = moment().hour(12).minute(13).second(14).millisecond(0);
                checkUTCTime(nvDateTimeUtils.toMoment('12:13:14'), today.utc().year(), 
                                                               today.utc().month(), 
                                                               today.utc().date(), 
                                                               today.utc().hour(), 
                                                               today.utc().minute(), 
                                                               today.utc().second(), 
                                                               today.utc().millisecond());

                //TODO https://github.com/moment/moment-timezone/issues/390
                // today = moment().tz('Asia/Singapore').hour(12).minute(13).second(14).millisecond(0);
                // checkUTCTime(nvDateTimeUtils.toMoment('12:13:14', 'Asia/Singapore'), today.utc().year(), 
                //                                                                 today.utc().month(), 
                //                                                                 today.utc().date(), 
                //                                                                 today.utc().hour(), 
                //                                                                 today.utc().minute(), 
                //                                                                 today.utc().second(), 
                //                                                                 today.utc().millisecond());

                checkUTCTime(nvDateTimeUtils.toMoment('2016-01-01'), localTime.getUTCFullYear(), 
                                                                 localTime.getUTCMonth(), 
                                                                 localTime.getUTCDate(), 
                                                                 localTime.getUTCHours(), 
                                                                 localTime.getUTCMinutes(), 
                                                                 localTime.getUTCSeconds(), 
                                                                 localTime.getUTCMilliseconds());
                checkUTCTime(nvDateTimeUtils.toMoment('2016-01-01', 'Asia/Singapore'), 2015, 12-1, 31, 16, 0, 0, 0);
            });

            it('should take date object', function(){
                var date = new Date('2016-02-16T01:23:45Z');
                checkUTCTime(nvDateTimeUtils.toMoment(date), 2016, 2-1, 16, 1, 23, 45, 0);
                checkUTCTime(nvDateTimeUtils.toMoment(date, 'Asia/Singapore'), 2016, 2-1, 16, 1, 23, 45, 0);
            });

            it('should take moment object', function(){
                checkUTCTime(nvDateTimeUtils.toMoment(moment.utc([2016, 1, 14, 15, 25, 50, 125])), 2016, 1, 14, 15, 25, 50, 125);
                checkUTCTime(nvDateTimeUtils.toMoment(moment.utc([2016, 1, 14, 15, 25, 50, 125]), 'Asia/Singapore'), 2016, 1, 14, 15, 25, 50, 125);
            });

            it('should return an invalid moment when the given string does not match any allowable format', function(){
                var date = nvDateTimeUtils.toMoment('01-01-2016');
                expect(date.isValid()).toBe(false);
            });

            it('should warn return an invalid moment when the given parameter is neither number, string, date object or moment object', function(){
                var date;

                date = nvDateTimeUtils.toMoment(true);
                expect(date.isValid()).toBe(false);
                expect(console.warn).toHaveBeenCalled();

                date = nvDateTimeUtils.toMoment({ company: 'ninjavan' });
                expect(date.isValid()).toBe(false);
            });

            function checkUTCTime(time, year, month, date, hour, minute, second, millisecond) {
                expect(time).toEqual(jasmine.any(moment));
                expect(time.utc().year()).toEqual(year);
                expect(time.utc().month()).toEqual(month);
                expect(time.utc().date()).toEqual(date);
                expect(time.utc().hour()).toEqual(hour);
                expect(time.utc().minute()).toEqual(minute);
                expect(time.utc().second()).toEqual(second);
                expect(time.utc().millisecond()).toEqual(millisecond);
            }
        });

        describe('nvDateTimeUtils.displayFormat', function(){
            it('should return \'\' if date is not date object nor moment object', function() {
                expect(nvDateTimeUtils.displayFormat(new Date(), 'YYYY-MM-DD HH:mm:ss')).not.toEqual('');
                expect(nvDateTimeUtils.displayFormat(moment(), 'YYYY-MM-DD HH:mm:ss')).not.toEqual('');

                expect(nvDateTimeUtils.displayFormat('2016-01-01', 'YYYY-MM-DD HH:mm:ss')).toEqual('');
                expect(nvDateTimeUtils.displayFormat(0, 'YYYY-MM-DD HH:mm:ss')).toEqual('');
                expect(nvDateTimeUtils.displayFormat(undefined, 'YYYY-MM-DD HH:mm:ss')).toEqual('');
                expect(nvDateTimeUtils.displayFormat(null, 'YYYY-MM-DD HH:mm:ss')).toEqual('');
            });
            it('should return the custom time format for the given date', function() {
                // Singapore Timezone Note: http://www.math.nus.edu.sg/aslaksen/teaching/timezone.html
                var date = new Date('2015-12-31T16:00:00Z');
                expect(nvDateTimeUtils.displayFormat(date, 'YYYY-MM-DD HH:mm:ss')).toEqual(
                    date.getFullYear()+'-'+pad(date.getMonth()+1)+'-'+pad(date.getDate())+' '+pad(date.getHours())+':'+pad(date.getMinutes())+':'+pad(date.getSeconds())
                );
                expect(nvDateTimeUtils.displayFormat(date, 'YYYY-MM-DD HH:mm:ss', 'utc')).toEqual('2015-12-31 16:00:00');
                expect(nvDateTimeUtils.displayFormat(date, 'YYYY-MM-DD HH:mm:ss', 'Asia/Singapore')).toEqual('2016-01-01 00:00:00');

                expect(nvDateTimeUtils.displayFormat(date, 'YYYY-MM-DD')).toEqual(
                    date.getFullYear()+'-'+pad(date.getMonth()+1)+'-'+pad(date.getDate())
                );
                expect(nvDateTimeUtils.displayFormat(date, 'YYYY-MM-DD', 'utc')).toEqual('2015-12-31');
                expect(nvDateTimeUtils.displayFormat(date, 'YYYY-MM-DD', 'Asia/Singapore')).toEqual('2016-01-01');

                expect(nvDateTimeUtils.displayFormat(date, 'HH:mm:ss')).toEqual(
                    pad(date.getHours())+':'+pad(date.getMinutes())+':'+pad(date.getSeconds())
                );
                expect(nvDateTimeUtils.displayFormat(date, 'HH:mm:ss', 'utc')).toEqual('16:00:00');
                expect(nvDateTimeUtils.displayFormat(date, 'HH:mm:ss', 'Asia/Singapore')).toEqual('00:00:00');

                expect(nvDateTimeUtils.displayFormat(date, 'HHmm')).toEqual(
                    pad(date.getHours())+pad(date.getSeconds())
                );
                expect(nvDateTimeUtils.displayFormat(date, 'HHmm', 'utc')).toEqual('1600');
                expect(nvDateTimeUtils.displayFormat(date, 'HHmm', 'Asia/Singapore')).toEqual('0000');

                expect(nvDateTimeUtils.displayFormat(date, 'YYYY-MM-DDTHH:mm:ssZ')).toMatch(
                  date.getFullYear() + '-' +
                  pad(date.getMonth() + 1) + '-' +
                  pad(date.getDate()) + 'T' +
                  pad(date.getHours()) + ':' +
                  pad(date.getMinutes()) + ':' +
                  pad(date.getSeconds()) +
                  getTimezoneRegex(date.getTimezoneOffset())
                );
                expect(nvDateTimeUtils.displayFormat(date, 'YYYY-MM-DDTHH:mm:ssZ', 'utc')).toMatch(/2015-12-31T16:00:00[+-]00:00/);
                expect(nvDateTimeUtils.displayFormat(date, 'YYYY-MM-DDTHH:mm:ssZ', 'Asia/Singapore')).toEqual('2016-01-01T00:00:00+08:00');
            });

            function pad(num){
                return (num < 10) ? '0' + num : num;
            }
            function getTimezoneRegex(offset) {
                const tz = pad(Math.abs(offset) / 60) + ':' + pad(Math.abs(offset) % 60);
                return (offset < 0 ? '[+]' : (offset > 0 ? '[-]' : '[+-]')) + tz;
            }
        });

        describe('nvDateTimeUtils.displayDateTime', function(){
            it('should return the same result as nvDateTimeUtils.displayFormat(\'YYYY-MM-DD HH:mm:ss\', timezone)', function(){
                var date = new Date();
                expect(nvDateTimeUtils.displayDateTime(date)).toEqual(nvDateTimeUtils.displayFormat(date, 'YYYY-MM-DD HH:mm:ss'));
                expect(nvDateTimeUtils.displayDateTime(date, 'Asia/Singapore')).toEqual(nvDateTimeUtils.displayFormat(date, 'YYYY-MM-DD HH:mm:ss', 'Asia/Singapore'));
            });
        });

        describe('nvDateTimeUtils.displayDate', function(){
            it('should return the same result as nvDateTimeUtils.displayFormat(\'YYYY-MM-DD\', timezone)', function(){
                var date = new Date();
                expect(nvDateTimeUtils.displayDate(date)).toEqual(nvDateTimeUtils.displayFormat(date, 'YYYY-MM-DD'));
                expect(nvDateTimeUtils.displayDate(date, 'Asia/Singapore')).toEqual(nvDateTimeUtils.displayFormat(date, 'YYYY-MM-DD', 'Asia/Singapore'));
            });
        });

        describe('nvDateTimeUtils.displayTime', function(){
            it('should return the same result as nvDateTimeUtils.displayFormat(\'HH:mm:ss\', timezone)', function(){
                var date = new Date();
                expect(nvDateTimeUtils.displayTime(date)).toEqual(nvDateTimeUtils.displayFormat(date, 'HH:mm:ss'));
                expect(nvDateTimeUtils.displayTime(date, 'Asia/Singapore')).toEqual(nvDateTimeUtils.displayFormat(date, 'HH:mm:ss', 'Asia/Singapore'));
            });
        });

        describe('nvDateTimeUtils.displayInternationalTime', function(){
            it('should return the same result as nvDateTimeUtils.displayFormat(\'HHmm\', timezone)', function(){
                var date = new Date();
                expect(nvDateTimeUtils.displayInternationalTime(date)).toEqual(nvDateTimeUtils.displayFormat(date, 'HHmm'));
                expect(nvDateTimeUtils.displayInternationalTime(date, 'Asia/Singapore')).toEqual(nvDateTimeUtils.displayFormat(date, 'HHmm', 'Asia/Singapore'));
            });
        });

        describe('nvDateTimeUtils.displayISO', function(){
            it('should return the same result as nvDateTimeUtils.displayFormat(\'YYYY-MM-DDTHH:mm:ssZ\', timezone)', function(){
                var date = new Date();
                expect(nvDateTimeUtils.displayISO(date)).toEqual(nvDateTimeUtils.displayFormat(date, 'YYYY-MM-DDTHH:mm:ssZ'));
                expect(nvDateTimeUtils.displayISO(date, 'Asia/Singapore')).toEqual(nvDateTimeUtils.displayFormat(date, 'YYYY-MM-DDTHH:mm:ssZ', 'Asia/Singapore'));
            });
        });
    });

})();
