(function() {
    'use strict';

    describe('coupled-list-utils.service', function() {

        var module = angular.mock.module;
        var inject = angular.mock.inject;

        describe('transfer', function() {

            beforeEach(module('nvCommons.services'));

            it('should conditionally move items from the src to the dst list', inject(function(nvCoupledListUtils) {
                var list1 = [3, 0, 1, 4, 2];
                var list2 = [];

                nvCoupledListUtils.transfer(list1, list2, function(item) {
                    return item % 2 === 0;
                });

                expect(list1.length).toBe(2);
                expect(list1).not.toContain(0);
                expect(list1).toContain(1);
                expect(list1).not.toContain(2);
                expect(list1).toContain(3);
                expect(list1).not.toContain(4);
                expect(list2.length).toBe(3);
                expect(list2).toContain(0);
                expect(list2).not.toContain(1);
                expect(list2).toContain(2);
                expect(list2).not.toContain(3);
                expect(list2).toContain(4);
            }));

            it('should conditionally move items from the src to the dst list and sort the dst list', inject(function(nvCoupledListUtils) {
                var list1 = ['brown', 'apple', 'bear', 'airplane', 'arc'];
                var list2 = [];

                nvCoupledListUtils.transfer(list1, list2, isFirstCharEqualsA, {sort: sort});

                expect(list1.length).toBe(2);
                expect(list1[0]).toBe('brown');
                expect(list1[1]).toBe('bear');
                expect(list2.length).toBe(3);
                expect(list2[0]).toBe('airplane');
                expect(list2[1]).toBe('apple');
                expect(list2[2]).toBe('arc');

                function isFirstCharEqualsA(item) {
                    return item[0] === 'a';
                }
            }));

        });

        describe('transferAll', function() {

            beforeEach(module('nvCommons.services'));

            it('should move all items from the src to the dst list', inject(function(nvCoupledListUtils) {
                var list1 = [3, 0, 1, 4, 2];
                var list2 = [];

                nvCoupledListUtils.transferAll(list1, list2);

                expect(list1.length).toBe(0);
                expect(list2.length).toBe(5);
                expect(list2).toContain(0);
                expect(list2).toContain(1);
                expect(list2).toContain(2);
                expect(list2).toContain(3);
                expect(list2).toContain(4);
            }));

            it('should move all items from the src to the dst list and sort the dst list', inject(function(nvCoupledListUtils) {
                var list1 = [3, 0, 1, 4, 2];
                var list2 = [];

                nvCoupledListUtils.transferAll(list1, list2, {sort: sort});

                expect(list1.length).toBe(0);
                expect(list2.length).toBe(5);
                expect(list2[0]).toBe(0);
                expect(list2[1]).toBe(1);
                expect(list2[2]).toBe(2);
                expect(list2[3]).toBe(3);
                expect(list2[4]).toBe(4);
            }));

        });

        function sort(a, b) {
            if (a < b) {
                return -1;
            } else if (a > b) {
                return 1;
            } else {
                return 0;
            }
        }

    });

})();
