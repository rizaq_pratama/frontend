 (function test() {
   describe('listener.service', () => {
     const module = angular.mock.module;
     const inject = angular.mock.inject;

     let nvExtendUtils;
     let nvDateTimeUtils;
     const numberFilter = {
       fn: number => (number > 30 ? 'a' : 'b'),
     };

     beforeEach(module('nvCommons.services'));
     beforeEach(module(($provide) => {
       spyOn(numberFilter, 'fn').and.callThrough();
       $provide.value('numberFilter', numberFilter.fn);
     }));

     beforeEach(inject((_nvExtendUtils_, _nvDateTimeUtils_) => {
       nvExtendUtils = _nvExtendUtils_;
       nvDateTimeUtils = _nvDateTimeUtils_;
       spyOn(nvDateTimeUtils, 'toMoment').and.callFake(val => val);
     }));

     describe('nvExtendUtils.addLowercase', () => {
       it('should add lowercase for array of objects', () => {
         const result = nvExtendUtils.addLowercase([{ apple: 'Bear', cat: 'Dog' },
                                  { apple: 'Brown Monkey', cat: 'Donkey' }],
                                  'apple');
         expect(result).toEqual([{ _lowercaseApple: 'bear', apple: 'Bear', cat: 'Dog' },
            { _lowercaseApple: 'brown monkey', apple: 'Brown Monkey', cat: 'Donkey' }]);
       });

       it('should add lowercase for 1 object', () => {
         const result = nvExtendUtils.addLowercase({ apple: 'Bear', cat: 'Dog' }, 'apple');
         expect(result).toEqual({ _lowercaseApple: 'bear', apple: 'Bear', cat: 'Dog' });
       });
     });

     describe('nvExtendUtils.addDisplayAddress', () => {
       it('should add display address for array of objects', () => {
         const result = nvExtendUtils.addDisplayAddress([
         { address1: '30',
          address2: 'Jalan Kilang Barat',
          postcode: '159363',
          country: 'Singapore' },
         { address1: '91',
          address2: 'Cavenagh Rd',
          city: 'Newton',
          postcode: '229629',
          country: 'Singapore' }]);
         expect(result).toEqual([
          { _address: '30 Jalan Kilang Barat 159363 Singapore',
          address1: '30',
          address2: 'Jalan Kilang Barat',
          postcode: '159363',
          country: 'Singapore' },
          { _address: '91 Cavenagh Rd Newton 229629 Singapore',
            address1: '91',
            address2: 'Cavenagh Rd',
            city: 'Newton',
            postcode: '229629',
            country: 'Singapore' }]);
       });

       it('should add display address for 1 object', () => {
         const result = nvExtendUtils.addDisplayAddress(
         { address1: '30',
          address2: 'Jalan Kilang Barat',
          postcode: '159363',
          country: 'Singapore' });
         expect(result).toEqual(
          { _address: '30 Jalan Kilang Barat 159363 Singapore',
          address1: '30',
          address2: 'Jalan Kilang Barat',
          postcode: '159363',
          country: 'Singapore' });
       });
     });

     describe('nvExtendUtils.addValueForBoolean', () => {
       it('should add value for boolean for array of objects', () => {
         const data = [{ apple: true, cat: 'Dog' },
                      { apple: false, cat: 'Donkey' }];
         const result = nvExtendUtils.addValueForBoolean(data, 'apple',
                                  { true: 'Yes', false: 'No' });
         expect(result).toEqual([{ _apple: 'Yes', apple: true, cat: 'Dog' },
                                { _apple: 'No', apple: false, cat: 'Donkey' }]);
       });

       it('should add value for boolean for 1 object', () => {
         const result = nvExtendUtils.addValueForBoolean({ apple: true, cat: 'Dog' },
                                                        'apple',
                                              { true: 'Have', false: 'Have not' });
         expect(result).toEqual({ _apple: 'Have', apple: true, cat: 'Dog' });
       });

       it('should works with undefined value', () => {
         const array = [{ van: 'Ninja', plate: 'NV123' }, { van: 'Marcella' }];
         const result = nvExtendUtils.addValueForBoolean(array, 'plate',
                                        { true: 'Contain', false: 'Not Contain' });
         expect(result).toEqual([{ van: 'Ninja', plate: 'NV123', _plate: 'Contain' },
                                { van: 'Marcella', _plate: 'Not Contain' }]);
       });

       it('should use true false as default', () => {
         const data = [{ apple: true, cat: 'Dog' },
                      { apple: false, cat: 'Donkey' }];
         const result = nvExtendUtils.addValueForBoolean(data, 'apple');
         expect(result).toEqual([{ _apple: true, apple: true, cat: 'Dog' },
                                { _apple: false, apple: false, cat: 'Donkey' }]);
       });
     });

     describe('nvExtendUtils.addFloat', () => {
       it('should add float for array of objects', () => {
         const result = nvExtendUtils.addFloat([{ apple: 32, cat: 'Dog' },
                                  { apple: 3, cat: 'Donkey' }],
                                  'apple', 1);
         expect(numberFilter.fn).toHaveBeenCalledWith(32, 1);
         expect(numberFilter.fn).toHaveBeenCalledWith(3, 1);
         expect(result).toEqual([{ _apple: 'a', apple: 32, cat: 'Dog' },
                                { _apple: 'b', apple: 3, cat: 'Donkey' }]);
       });

       it('should add float for 1 object', () => {
         const result = nvExtendUtils.addFloat({ apple: 32, cat: 'Dog' },
                                  'apple', 1);
         expect(numberFilter.fn).toHaveBeenCalledWith(32, 1);
         expect(result).toEqual({ _apple: 'a', apple: 32, cat: 'Dog' });
       });

       it('should by default use 2 decimal place', () => {
         const result = nvExtendUtils.addFloat({ apple: 32, cat: 'Dog' }, 'apple');
         expect(numberFilter.fn).toHaveBeenCalledWith(32, 2);
         expect(result).toEqual({ _apple: 'a', apple: 32, cat: 'Dog' });
       });
     });

     describe('nvExtendUtils.addLatLng', () => {
       it('should add latlng for array of objects', () => {
         const result = nvExtendUtils.addLatLng([{ latitude: 32.3, longitude: 103.2 },
                                  { apple: 3, cat: 'Donkey' }]);
         expect(result).toEqual([{ latitude: 32.3, longitude: 103.2, _latlng: '(32.3, 103.2)' },
                                  { apple: 3, cat: 'Donkey', _latlng: 'No Lat/Long' }]);
       });

       it('should add latlng for 1 object', () => {
         const result = nvExtendUtils.addLatLng({ latitude: 32.3, longitude: 103.2 });
         expect(result).toEqual({ latitude: 32.3, longitude: 103.2, _latlng: '(32.3, 103.2)' });
       });
     });

     describe('nvExtendUtils.addMoment', () => {
       it('should add moment for array of objects', () => {
         const data = [{ updatedAt: '2016-03-03T14:39:07Z', createdAt: '2016-02-28T14:50:32Z', cat: 'Dog' },
                      { updatedAt: '2016-02-28T14:50:32Z', createdAt: '2014-12-03T14:39:07Z', cat: 'Donkey' }];
         const result = nvExtendUtils.addMoment(data, 'updatedAt');
         expect(nvDateTimeUtils.toMoment).toHaveBeenCalledWith('2016-03-03T14:39:07Z', 'utc');
         expect(nvDateTimeUtils.toMoment).toHaveBeenCalledWith('2016-02-28T14:50:32Z', 'utc');
         expect(result).toEqual([{ _momentUpdatedAt: '2016-03-03T14:39:07Z',
                                  updatedAt: '2016-03-03T14:39:07Z',
                                  createdAt: '2016-02-28T14:50:32Z',
                                  cat: 'Dog' },
                                { _momentUpdatedAt: '2016-02-28T14:50:32Z',
                                  updatedAt: '2016-02-28T14:50:32Z',
                                  createdAt: '2014-12-03T14:39:07Z',
                                  cat: 'Donkey' }]);
       });

       it('should add moment for 1 object', () => {
         const data = { updatedAt: '2016-03-03T14:39:07Z', createdAt: '2016-02-28T14:50:32Z', cat: 'Dog' };
         const result = nvExtendUtils.addMoment(data, 'updatedAt');
         expect(nvDateTimeUtils.toMoment).toHaveBeenCalledWith('2016-03-03T14:39:07Z', 'utc');
         expect(result).toEqual({ _momentUpdatedAt: '2016-03-03T14:39:07Z',
                                  updatedAt: '2016-03-03T14:39:07Z',
                                  createdAt: '2016-02-28T14:50:32Z',
                                  cat: 'Dog' });
       });

       it('should add moment for array of properties', () => {
         const data = { updatedAt: '2016-03-03T14:39:07Z', createdAt: '2016-02-28T14:50:32Z', cat: 'Dog' };
         const result = nvExtendUtils.addMoment(data, ['updatedAt', 'createdAt']);
         expect(nvDateTimeUtils.toMoment).toHaveBeenCalledWith('2016-03-03T14:39:07Z', 'utc');
         expect(nvDateTimeUtils.toMoment).toHaveBeenCalledWith('2016-02-28T14:50:32Z', 'utc');
         expect(result).toEqual({ _momentUpdatedAt: '2016-03-03T14:39:07Z',
                                  updatedAt: '2016-03-03T14:39:07Z',
                                  _momentCreatedAt: '2016-02-28T14:50:32Z',
                                  createdAt: '2016-02-28T14:50:32Z',
                                  cat: 'Dog' });
       });
     });
   });
 }());
