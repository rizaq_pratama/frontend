(function test() {
  describe('highlight.filter', () => {
    const module = angular.mock.module;
    const inject = angular.mock.inject;

    let highlight;
    let $sce;

    beforeEach(module('nvCommons.filters'));

    beforeEach(inject(($filter, _$sce_) => {
      highlight = $filter('nvHighlight');
      $sce = _$sce_;
    }));

    it('should highlight a word', () => {
      const result = highlight('this str contains a str that will be highlighted', ['str']);
      expect($sce.getTrustedHtml(result)).toEqual('this <span class="match">str</span> contains a <span class="match">str</span> that will be highlighted');
    });

    it('should highlight multiple words', () => {
      const result = highlight('she sell seashell on a seashore', ['sea', 'ell']);
      expect($sce.getTrustedHtml(result)).toEqual('she s<span class="match">ell</span> <span class="match">sea</span>sh<span class="match">ell</span> on a <span class="match">sea</span>shore');
    });

    it('should highlight nothing with empty array', () => {
      const result = highlight('she sell seashell on a seashore', []);
      expect($sce.getTrustedHtml(result)).toEqual('she sell seashell on a seashore');
    });

    it('should highlight nothing with empty string', () => {
      const result = highlight('she sell seashell on a seashore', ['']);
      expect($sce.getTrustedHtml(result)).toEqual('she sell seashell on a seashore');
    });

    it('should work on empty string', () => {
      const result = highlight('', ['sea', 'ell']);
      expect($sce.getTrustedHtml(result)).toEqual('');
    });

    it('should work on empty string with empty array', () => {
      const result = highlight('', []);
      expect($sce.getTrustedHtml(result)).toEqual('');
    });

    it('should return string when the given \'string\' is not a string', () => {
      const result = highlight(1, ['']);
      expect($sce.getTrustedHtml(result)).toEqual('1');
    });

    it('should stringify when given object is not a string', () => {
      const result = highlight(12.3, ['a', 'b']);
      expect($sce.getTrustedHtml(result)).toEqual('12.3');
    });

    it('should stringify the given object before matching', () => {
      const result = highlight(12.3, ['a', '2']);
      expect($sce.getTrustedHtml(result)).toEqual('1<span class="match">2</span>.3');
    });
  });
}());
