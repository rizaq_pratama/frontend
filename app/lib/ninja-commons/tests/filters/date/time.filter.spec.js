(function test() {
  describe('time.filter', () => {
    const module = angular.mock.module;
    const inject = angular.mock.inject;
    let nvDateTimeUtils;
    let nvTimeFilter;

    beforeEach(module('nvCommons.filters'));
    beforeEach(module('nvCommons.services'));
    beforeEach(module('pascalprecht.translate'));

    beforeEach(inject((nvTimezone, nvTranslate) => {
      spyOn(nvTimezone, 'getOperatorTimezone').and.returnValue('Asia/Singapore');
      spyOn(nvTranslate, 'instant').and.callFake((key) => 
        (key === 'filter.time.invalid-date') ? 'Invalid Date' : key
      );
    }));

    beforeEach(inject((_nvDateTimeUtils_) => {
      nvDateTimeUtils = _nvDateTimeUtils_;
      spyOn(nvDateTimeUtils, 'displayDateTime').and.returnValue('displayDateTimeStr');
      spyOn(nvDateTimeUtils, 'displayDate').and.returnValue('displayDateStr');
      spyOn(nvDateTimeUtils, 'displayTime').and.returnValue('displayTimeStr');
      spyOn(nvDateTimeUtils, 'displayInternationalTime').and.returnValue('displayInternationalTimeStr');
      spyOn(nvDateTimeUtils, 'displayISO').and.returnValue('displayISOStr');
      spyOn(nvDateTimeUtils, 'displayFormat').and.returnValue('displayFormatStr');
    }));

    beforeEach(() => {
      jasmine.addCustomEqualityTester((obj1, obj2) => {
        if(moment.isMoment(obj1) && moment.isMoment(obj2)) {
          return obj1.isSame(obj2);
        }
      });
    });

    beforeEach(inject(($filter) => {
      nvTimeFilter = $filter('nvTime');
    }));

    it('should call nvDateTimeUtils.displayXXX', () => {
      const date = new moment();
      expect(nvTimeFilter(date, 'utc', 'datetime')).toEqual('displayDateTimeStr');
      expect(nvDateTimeUtils.displayDateTime).toHaveBeenCalledWith(date, 'utc');

      expect(nvTimeFilter(date, 'utc', 'date')).toEqual('displayDateStr');
      expect(nvDateTimeUtils.displayDate).toHaveBeenCalledWith(date, 'utc');

      expect(nvTimeFilter(date, 'utc', 'time')).toEqual('displayTimeStr');
      expect(nvDateTimeUtils.displayTime).toHaveBeenCalledWith(date, 'utc');

      expect(nvTimeFilter(date, 'utc', 'international')).toEqual('displayInternationalTimeStr');
      expect(nvDateTimeUtils.displayInternationalTime).toHaveBeenCalledWith(date, 'utc');

      expect(nvTimeFilter(date, 'utc', 'iso')).toEqual('displayISOStr');
      expect(nvDateTimeUtils.displayISO).toHaveBeenCalledWith(date, 'utc');

      expect(nvTimeFilter(date, 'utc', 'custom format')).toEqual('displayFormatStr');
      expect(nvDateTimeUtils.displayFormat).toHaveBeenCalledWith(date, 'custom format', 'utc');
    });

    it('should handle "operator" as timezone', () => {
      const date = moment();
      nvTimeFilter(date, 'operator', 'datetime');
      expect(nvDateTimeUtils.displayDateTime).toHaveBeenCalledWith(date, 'Asia/Singapore');
    });

    it('should handle invalid invalid date', () => {
      expect(nvTimeFilter('invalid', 'utc', 'datetime')).toEqual('Invalid Date');
    });

    it('should default to "utc" and format "date"', () => {
      const date = moment();
      nvTimeFilter(date);
      expect(nvDateTimeUtils.displayDate).toHaveBeenCalledWith(date, 'utc');
    });
  });
}());
