(function filter() {
  angular
    .module('nvCommons.filters')
    .filter('nvHighlight', highlight);

  highlight.$inject = ['$sce'];

  function highlight($sce) {
    // http://stackoverflow.com/questions/15519713/highlighting-a-filtered-result-in-angularjs
    // well treat all text as string
    return function nvHighlight(str = '', termsToHighlight) {
      // Sort terms by length
      const sortedTerms = _(termsToHighlight)
                            .filter(term => term.length > 0)
                            .sort((a, b) => (b.length - a.length))
                            .value();

      const text = String(str);
      if (sortedTerms.length === 0) {
        return $sce.trustAsHtml(text);
      }
      // Regex to simultaneously replace terms
      const regex = new RegExp(`(${sortedTerms.join('|')})`, 'gi');
      return $sce.trustAsHtml(text.replace(regex, '<span class="match">$&</span>'));
    };
  }
}());
