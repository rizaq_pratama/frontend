(function filter() {
  angular
    .module('nvCommons.filters')
    .filter('nvTime', TimeFilter);

  TimeFilter.$inject = ['nvDateTimeUtils', 'nvTranslate', 'nvTimezone'];
  function TimeFilter(nvDateTimeUtils, nvTranslate, nvTimezone) {
    return function (input, timezone = 'utc', format = 'date') {
      const time = moment(input);
      const fn = getFn(format);
      const tz = (timezone === 'operator') ? nvTimezone.getOperatorTimezone() : timezone;
      return time.isValid() ? fn(time, tz) : nvTranslate.instant('filter.time.invalid-date');
    };

    function getFn(format) {
      switch(format) {
        case 'datetime':
          return nvDateTimeUtils.displayDateTime;
        case 'date':
          return nvDateTimeUtils.displayDate;
        case 'time':
          return nvDateTimeUtils.displayTime;
        case '12-hour-time':
          return nvDateTimeUtils.display12HourTime;
        case 'international':
          return nvDateTimeUtils.displayInternationalTime;
        case 'iso':
          return nvDateTimeUtils.displayISO;
        default:
          return _.partial(nvDateTimeUtils.displayFormat, _, format, _);
      }
    }
  }
}());
