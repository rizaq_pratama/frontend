(function service() {
  angular
    .module('nvCommons.runs')
    .run(monkeyPatchHttpService);

  monkeyPatchHttpService.$inject = ['$http'];

  function monkeyPatchHttpService($http) {
    /**
     * $http.delete2
     * same API as $http.post
     * @return Promise
     */
    $http.delete2 = function upload(url, data = {}, config = {}) {
      const deleteConfig = _.defaultsDeep({}, config, {
        method: 'DELETE',
        url: url,
        data: data,
        headers: { 'Content-Type': 'application/json; charset=UTF-8' },
      });

      return $http(deleteConfig);
    };
  }
}());
