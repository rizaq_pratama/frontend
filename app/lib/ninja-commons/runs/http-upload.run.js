(function service() {
    // upload file in AngularJS
    // https://uncorkedstudios.com/blog/multipartformdata-file-upload-with-angularjs

  angular
        .module('nvCommons.runs')
        .run(monkeyPatchHttpService);

  monkeyPatchHttpService.$inject = ['$http'];

  function monkeyPatchHttpService($http) {
        /**
         * Upload file using POST request
         * @param  String               url   request url
         * @param  File or Array<File>  files  file(s) to be uploaded
         * @param  Object               data   post request body
         * @param  Promise              cancelPromise a promise that if resolved will cancel current request 
         * @return Promise
         */
    $http.upload = function upload(url, files, data, cancelPromise) {
      const fd = prepareForm(files, data);

      return $http.post(url, fd, {
        transformRequest: angular.identity,
        headers: { 'Content-Type': undefined },
        timeout: cancelPromise,
      });
    };

    $http.uploadWithPut = function uploadWithPut(url, files, data) {
      const fd = prepareForm(files, data);

      return $http.put(url, fd, {
        transformRequest: angular.identity,
        headers: { 'Content-Type': undefined },
      });
    };
  }

    /**
    * @param  File or Array<File>  files  file(s) to be uploaded
    * @param  Object               data   request body
    * @return formData
    */
  function prepareForm(files, data) {
    data = angular.isDefined(data) ? data : {};

    const formData = new FormData();
    if (angular.isArray(files)) {
      _.each(files, (file) => {
        formData.append('file[]', file);
      });
    } else {
      formData.append('file', files);
    }
    _.each(data, (value, key) => {
      formData.append(key, value);
    });

    return formData;
  }
}());
