;(function() {
    'use strict';

    angular
        .module('nvCommons.directives')
        .directive('nvWindowResize', windowResize);

    windowResize.$inject = ['$window'];

    function windowResize($window) {

        return function (scope, element, attr) {

            var w = angular.element($window);
            scope.$watch(function () {
                return {
                    'h': window.innerHeight,
                    'w': window.innerWidth
                };
            }, function (newValue, oldValue) {
                /* jshint unused: false */
                scope.$windowHeight = newValue.h;
                scope.$windowWidth = newValue.w;
                scope.$eval(attr.nvWindowResize);

//                scope.resizeWithOffset = function (offsetH) {
//                    return {
//                        'height': (newValue.h - offsetH) + 'px'
//                    };
//                };

            }, true);

            w.bind('resize', function () {
                scope.$apply();
            });
        };
    }

})();