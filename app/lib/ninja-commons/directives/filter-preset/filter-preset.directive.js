(function directive() {
  angular
    .module('nvCommons.directives')
    .directive('nvFilterPreset', nvFilterPresetDirective);

  nvFilterPresetDirective.$inject = [];

  /**
   *
   * nvFilterPresetDirective (typically used together with nvFilterBoxContainer)
   * @param {array}         filterPresets          List of available filter presets with id and name properties
   *
   * @param {integer}       selectedPreset         The id of the selected preset
   *
   * @param {array}         selectedFilters        List of selectedFilters from nv-filter-box-container
   *
   * @param {array}         possibleFilters        List of possibleFilters from nv-filter-box-container
   *
   * @param {callback}      onSave                 An API call promise for creating a new preset
   *                         @param body
   *
   * @param {callback}      onDelete               An API call promise for deleting a preset
   *                         @param presetId        id to be deleted
   *
   * @param {callback}      onUpdate(optional)     An API call promise for updating a preset
   *                         @param presetId        id to be updated
   *                         @param body
   *
   * @param {callback}      onCheckName(optional)  An API call promise for checking name availability
   *                         @param name            name to be checked
   *
   * @param {string}        filterPath(optional)   Property path to get the stored filter object
   *
   * @param {callback}      onLoad(optional)       A callback for converting preset to your own filter format
   *                         @param preset          selectedPreset object
   */
  function nvFilterPresetDirective() {
    return {
      restrict: 'E',
      templateUrl: 'lib/ninja-commons/directives/filter-preset/filter-preset.directive.html',
      replace: true,
      scope: {
        filterPresets: '=',
        selectedPreset: '=',
        selectedFilters: '=',
        possibleFilters: '=',
        onSave: '&',
        onDelete: '&',
        onUpdate: '&?',
        onCheckName: '&?',
        filterPath: '@?',
        onLoad: '&?',
      },
      controller: nvFilterPresetController,
      controllerAs: 'ctrl',
      bindToController: true,
    };
  }

  nvFilterPresetController.$inject = [
    '$scope',
    '$timeout',
    'nvDialog',
    'nvToast',
    'nvTranslate',
    'nvBackendFilterUtils',
    'nvEditModelDialog',
  ];

  function nvFilterPresetController(
    scope,
    $timeout,
    nvDialog,
    nvToast,
    nvTranslate,
    nvBackendFilterUtils,
    nvEditModelDialog
  ) {
    const ctrl = this;

    let selectedFilterDewatcher = null;
    const filterPresetsMap = {};

    ctrl.loadFilterPreset = loadFilterPreset;
    ctrl.savePreset = savePreset;
    ctrl.deletePreset = deletePreset;
    ctrl.updatePreset = updatePreset;

    initializePresets();

    function initializePresets() {
      ctrl.presetOptions = toOptions(ctrl.filterPresets);
      _.forEach(ctrl.filterPresets, (preset) => {
        filterPresetsMap[preset.id] = preset;
      });
    }

    function loadFilterPreset() {
      $timeout(() => {
        const presetId = ctrl.selectedPreset;
        if (presetId == null) {
          return;
        }

        let preset = filterPresetsMap[presetId];
        if (ctrl.filterPath) {
          const pathFn = _.property(ctrl.filterPath);
          preset = pathFn(preset);
        }

        if (ctrl.onLoad) {
          ctrl.onLoad({ preset: preset });
        } else {
          nvBackendFilterUtils.convertPresetToFilter(
            ctrl.possibleFilters,
            ctrl.selectedFilters, preset
          );
        }

        if (!ctrl.onUpdate) {
          if (selectedFilterDewatcher) {
            selectedFilterDewatcher();
            selectedFilterDewatcher = null;
          }

          watchSelectedFilters();
        }
      });
    }

    function savePreset($event) {
      const fields = {
        name: null,
        isNameAvailable: ctrl.onCheckName ? null : true,
      };

      const templateUrl = ctrl.onCheckName ?
        'lib/ninja-commons/directives/filter-preset/dialogs/save-preset.dialog.html' :
        'lib/ninja-commons/directives/filter-preset/dialogs/save-preset-no-check-name.dialog.html';

      nvDialog.showSingle($event, {
        type: 'nv-dialog',
        theme: 'nvGreen',
        templateUrl: templateUrl,
        cssClass: 'save-preset',
        scope: _.assign(nvEditModelDialog.scopeTemplate({ onSave: onSave }), {
          fields: fields,
          selectedFilters: ctrl.selectedFilters,
          getCompactDisplay: nvBackendFilterUtils.toString,
          checkNameAvailability: checkName,
        }),
      }).then(success);

      function onSave() {
        const options = nvBackendFilterUtils.getAddPresetFields(fields.name, ctrl.selectedFilters);
        return ctrl.onSave({ body: options });
      }

      function checkName(name) {
        if (name == null) {
          fields.isNameAvailable = null;
          return;
        }

        return ctrl.onCheckName({ name: name })
          .then(isAvailable => fields.isNameAvailable = isAvailable);
      }

      function success(response) {
        const template = response;
        ctrl.filterPresets.push(template);
        filterPresetsMap[template.id] = template;

        ctrl.presetOptions.unshift({
          value: template.id,
          displayName: `${template.id}-${template.name}`,
        });
        ctrl.selectedPreset = template.id;

        nvToast.success(
          nvTranslate.instant('commons.preset.x-preset-created', { x: 1 }),
          nvTranslate.instant('commons.name-x', { x: template.name }),
          { icon: 'add' }
        );
      }
    }

    function deletePreset($event) {
      const fields = { selectedPreset: null };
      nvDialog.showSingle($event, {
        type: 'nv-dialog',
        theme: 'nvRed',
        templateUrl: 'lib/ninja-commons/directives/filter-preset/dialogs/delete-preset.dialog.html',
        cssClass: 'delete-preset',
        scope: _.assign(nvEditModelDialog.scopeTemplate({ onSave: onDelete }), {
          fields: fields,
          filterPresetOptions: ctrl.presetOptions,
          filterPresetsMap: filterPresetsMap,
        }),
      }).then(success);

      function onDelete() {
        return ctrl.onDelete({ id: fields.selectedPreset });
      }

      function success(response) {
        const template = response;
        _.remove(ctrl.presetOptions, filter => filter.value === template.id);

        nvToast.warning(
          nvTranslate.instant('commons.preset.x-preset-deleted', { x: 1 }),
          nvTranslate.instant('commons.id-x', { x: template.id }),
          { icon: 'delete' }
        );
      }
    }

    function updatePreset() {
      const presetId = ctrl.selectedPreset;

      const data = nvBackendFilterUtils.getAddPresetFields(
        filterPresetsMap[presetId].name,
        ctrl.selectedFilters
      );
      // @params: id, body
      ctrl.onUpdate({ id: presetId, body: data }).then(success);

      function success(response) {
        const template = response;
        filterPresetsMap[template.id] = template;

        const updatedIdx = _.sortedIndexBy(ctrl.presetOptions,
          { value: template.id },
          o => -o.value
        );

        ctrl.presetOptions[updatedIdx] = {
          value: template.id,
          displayName: `${template.id}-${template.name}`,
        };

        nvToast.success(
          nvTranslate.instant('commons.preset.x-preset-updated', { x: 1 }),
          nvTranslate.instant('commons.name-x', { x: template.name }),
          { icon: 'update' }
        );
      }
    }

    function watchSelectedFilters() {
      selectedFilterDewatcher = scope.$watchCollection(() => ctrl.selectedFilters, onChangeFilter);

      function onChangeFilter(newValue, oldValue) {
        if (newValue !== oldValue) {
          ctrl.presetOptions.value = null;
          selectedFilterDewatcher();
        }
      }
    }

    function toOptions(data) {
      return _(data).map((template) => {
        return {
          value: template.id,
          displayName: `${template.id}-${template.name}`,
        };
      }).orderBy('value', 'desc').value();
    }
  }
}());
