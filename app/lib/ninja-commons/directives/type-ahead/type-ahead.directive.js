;(function() {
    'use strict';

    angular
        .module('nvCommons.directives')
        .directive('nvTypeAhead', typeAhead);
    
    typeAhead.$inject = ['$timeout'];  
      
    function typeAhead($timeout) {
        return {
            restrict: 'AEC',
            templateUrl : 'lib/ninja-commons/directives/type-ahead/type-ahead.directive.html',
            scope: {
                items: '<',
                placeholder: '@',
                title: '@',
                model: '=',
                onSelect: '&'
            },
            link : link
        };
        
        function link (scope) {
            scope.handleSelection = function(selectedItem) {
                scope.model = selectedItem;
                scope.current = 0;
                scope.selected = true;
                $timeout(function() {
                    scope.onSelect();
                    }, 200);
            };
            
            scope.current = 0;
            scope.selected = true; // hides the list initially
            scope.isCurrent = function(index) {
                return scope.current === index;
            };
            scope.setCurrent = function(index) {
                scope.current = index;
            };
        }
    }
        
})();