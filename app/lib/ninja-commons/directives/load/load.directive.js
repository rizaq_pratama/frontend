(function directive() {
  angular
    .module('nvCommons.directives')
    .directive('nvLoad', load);

  load.$inject = ['$log', '$q', 'nvUtils', 'nvGa', '$state'];

  function load($log, $q, nvUtils, nvGa, $state) {
    let loaderId = 1;

    return {
      restrict: 'A',
      templateUrl: 'lib/ninja-commons/directives/load/load.directive.html',
      transclude: true,
      replace: false,
      scope: {
        loader: '=nvLoad',
        message: '@loadingMessage',
        onLoad: '&',
      },
      link: link,
    };

    function link(scope) {
      // we assign an empty object to nvLoad at the start,
      // so that it overrides any parent nvLoad scopes
      scope.nvLoad = {};

      scope.$watch('loader', (loader) => {
        if (!loader) {
          // the loader is undefined
          loadPromise(loader);
        } else if (loader._nvLoaderId && scope['nvLoadStatus_' + loader._nvLoaderId]) {
          // the loading has already been handled by another nvLoad, so we just inherit the state
          scope.nvLoad = scope['mkLoadStatus_' + loader._nvLoaderId];
        } else {
          // the loading is being done for the first time
          loader._nvLoaderId = loaderId++;
          scope.nvLoad = scope['mkLoadStatus_' + loader._nvLoaderId] = {};
          loadPromise(loader);
        }
      });

      scope.reload = () => {
        if (scope.loader) {
          loadPromise(scope.loader);
        }
      };

      function loadPromise(loader) {
        // normalise the loader to a promise
        const promise = normalizeLoader(loader);

        // store start time in milis to be used when page load success or failed
        scope.startTime = moment().valueOf();

        if (promise && _.isFunction(promise.then)) {
          // if we have a promise and it is a real promise then we wait
          waitForPromise(promise, loader);
        } else {
          // otherwise return immediately
          isLoaded(promise);
        }
      }

      function normalizeLoader(loader) {
        // todo - use $q.when
        if (!loader) { // passed nothing
          return null;
        } else if (loader.promise) { // passed a deferred
          return loader.promise;
        } else if (_.isFunction(loader)) { // passed a function that returns a promise
          return loader();
        } else if (_.isFunction(loader.then)) { // passed a promise
          return loader;
        }
        $log.error('load.directive.js', 'unknown or invalid promise object passed to directive');
        return undefined;
      }

        // wait for the promise and set the states accordingly
      function waitForPromise(promise) {
        isLoading();
        promise.then(isLoaded, isError);
      }

      function isLoaded(result) {
        report('success');
        scope.nvLoad.status = 'loaded';
        scope.onLoad({ result: result });
        return $q.resolve(result);
      }

      function isLoading() {
        scope.nvLoad.status = 'loading';
      }

      function isError(error) {
        report('error');
        scope.nvLoad.status = 'error';
        scope.errorMessageMain = nvUtils.getOrElse(error, 'data.errorName', 'Oops, something went wrong.');
        scope.errorMessageSub = nvUtils.getOrElse(error, 'data.errorMessage', 'Please check your internet connection, or try again later.');
        return $q.reject(error);
      }

      function report(result) {
        const loadingDuration = scope.startTime - moment().valueOf();
        nvGa.timing('pageLoad', result, loadingDuration, $state.current.name);
        nvGa.event('pageLoad', result, $state.current.name);
      }
    }
  }
}());
