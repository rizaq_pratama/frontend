(function() {
    'use strict';

    angular
        .module('nvCommons.directives')
        .directive('nvAutofocus', autoFocus);

    autoFocus.$inject = ['$timeout'];

    function autoFocus($timeout) {

        return {
            restrict: 'A',
            link : function($scope, $element) {
                $timeout(function() {
                    $element[0].focus();
                });
            }
        };
    }

})();
