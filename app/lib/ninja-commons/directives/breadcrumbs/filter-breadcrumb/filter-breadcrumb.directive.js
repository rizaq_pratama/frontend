;(function() {
    'use strict';

    angular
        .module('nvCommons.directives')
        .directive('nvFilterBreadcrumb', filterBreadcrumb);

    function filterBreadcrumb() {

        return {
            restrict: 'E',
            templateUrl: 'lib/ninja-commons/directives/breadcrumbs/filter-breadcrumb/filter-breadcrumb.directive.html',
            replace: true,
            scope: {
                title: '@',
                subtitle: '@',
                onClick: '&'
            }
        };

    }

})();
