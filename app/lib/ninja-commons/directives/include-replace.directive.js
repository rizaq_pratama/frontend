;(function() {
    'use strict';

    angular
        .module('nvCommons.directives')
        .directive('nvIncludeReplace', includeReplace);

    includeReplace.$inject = [];

    function includeReplace() {

        return {
            require: 'ngInclude',
            restrict: 'A', /* optional */
            link: function (scope, el, attrs) {
                /* jshint unused: false */
                el.replaceWith(el.children());
            }
        };
    }

})();