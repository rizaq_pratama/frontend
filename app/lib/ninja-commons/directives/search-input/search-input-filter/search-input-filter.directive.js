(function directive() {
  /**
   *
   *  Search Inputs - Filter
   *
   *  @param {placeholder}  string    placeholder text
   *  @param {searchText}   model     search text
   *  @param {debounce}     integer   debounce milliseconds for the search input
   *  @param {type}         string    'bar' or 'text'
   *  @param {disabled}     boolean   whether to disable the search input
   *
   */

  angular
    .module('nvCommons.directives')
    .directive('nvSearchInputFilter', nvSearchInputFilter);

  nvSearchInputFilter.$inject = [];

  function nvSearchInputFilter() {
    return {
      restrict: 'E',
      templateUrl: 'lib/ninja-commons/directives/search-input/search-input-filter/search-input-filter.directive.html',
      scope: {
        placeholder: '@',
        searchText: '=',
        debounce: '=?',
        type: '@',
        disabled: '=?',
        onChange: '&?',
        tabIndex: '@?',
        icon: '@?',
        hideIcon: '=?',
      },
      link: link,
    };

    function link(scope, elem) {
      if (scope.type !== 'bar' && scope.type !== 'text') {
        scope.type = 'bar';
      }

      _.defaults(scope, {
        debounce: 500,
        icon: 'filter_list',
        hideIcon: false,
      });

      scope.clear = () => {
        scope.searchText = '';
        scope.notifyChange();
      };
      scope.notifyChange = () => scope.onChange({ $searchText: scope.searchText });

      const input = elem.find('input');
      elem.find('md-icon').eq(0).on('click', () => input.focus());
    }
  }
}());
