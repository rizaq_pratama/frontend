(function directive() {
  /**
   *
   *  Search Inputs - Filter
   *
   *  @param {string}   placeholder    placeholder text
   *  @param {string}   delegateHandle DelegateHandle for nvAutocomplete.Data,
   *                                   @see nvAutocomplete.directive
   *  @param {model}    searchText     search text
   *  @param {string}   icon           material icon name for the icon in the right button,
   *                                   default to 'arrow_forward'
   *  @param {string}   tooltip        tooltip string for the right button
   *  @param {string}   sortBy         Key to sort by for the autocomplete suggestions,
   *                                   @see nvAutocomplete.directive
   *  @param {string}   displayBy      Key for the object to display  for the autocomplete
   *                                   suggestions, @see nvAutocomplete.directive
   *  @param {boolean}  disabled       whether to disable the search input
   *  @param {string}   theme          theme name, ie. 'light' or 'dark', default to 'light'
   *  @param {function} onClick        Callback function when the button is clicked.
   *
   */
  angular
    .module('nvCommons.directives')
    .directive('nvSearchInputProgressive', nvSearchInputProgressive);

  nvSearchInputProgressive.$inject = ['nvAutocomplete.Data'];

  function nvSearchInputProgressive(nvAutocompleteData) {
    return {
      restrict: 'E',
      templateUrl: 'lib/ninja-commons/directives/search-input/search-input-progressive/search-input-progressive.directive.html',
      scope: {
        placeholder: '@',
        delegateHandle: '@',
        searchText: '=',
        icon: '@',
        tooltip: '@',
        sortBy: '@?',
        displayBy: '@?',
        disabled: '=?',
        theme: '@?',
        onClick: '&',
      },
      link: link,
    };

    function link(scope, elem) {
      scope.icon = angular.isDefined(scope.icon) ? scope.icon : 'arrow_forward';
      scope.searchText = angular.isDefined(scope.searchText) ? scope.searchText : '';
      scope.theme = angular.isDefined(scope.theme) ? scope.theme : 'light';
      scope.tooltip = angular.isDefined(scope.tooltip) ? scope.tooltip : 'Go';
      scope.data = nvAutocompleteData.getByHandle(scope.delegateHandle);
      elem.addClass(scope.theme);
    }
  }
}());
