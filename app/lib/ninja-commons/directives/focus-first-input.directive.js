(function() {
    'use strict';

    angular
        .module('nvCommons.directives')
        .directive('nvFocusFirstInput', focusFirstInput);

    focusFirstInput.$inject = ['$timeout'];

    function focusFirstInput($timeout) {

        return {
            restrict: 'A',
            link: link
        };

        function link(scope, iElement) {
            $timeout(function() {
                var firstInput = iElement[0].querySelector('input');
                if (firstInput) {
                    firstInput.focus();
                }
            });
        }

    }

})();
