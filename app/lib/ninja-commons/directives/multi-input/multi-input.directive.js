(function directive() {
  angular
        .module('nvCommons.directives')
        .directive('nvMultiInput', nvMultiInput);

  nvMultiInput.$inject = ['nvDateTimeUtils', 'nvAutocomplete.Data', 'nvTranslate', '$timeout'];

  function nvMultiInput(nvDateTimeUtils, nvAutocompleteData, nvTranslate, $timeout) {
    return {
      restrict: 'E',
      templateUrl: 'lib/ninja-commons/directives/multi-input/multi-input.directive.html',
      replace: true,
      scope: {
        multiselectParam: '=',
        onChange: '&?',
      },
      link: link,
    };

    function link($scope) {
      const PARAM = $scope.multiselectParam;
      init();

      function init() {
        // extras
        $scope.addText = _.get(PARAM, 'extra.addText', nvTranslate.instant('commons.add'));
        $scope.itemType = _.get(PARAM, 'extra.itemType', nvTranslate.instant('commons.item'));
        $scope.maxItem = _.get(PARAM, 'extra.maxItem', 5);

        // function
        $scope.addElement = addElement;
        $scope.removeElement = removeElement;
        $scope.onSelect = onSelect;
        $scope.isAddDisabled = isAddDisabled;
        $scope.isOptionsEmpty = isOptionsEmpty;
        $scope.isAddHidden = isAddHidden;
        $scope.onBlur = onBlur;
        $scope.onChange = angular.isDefined($scope.onChange) ? $scope.onChange : _.noop;
        _.defaults(PARAM, { itemMatchFn: _.eq });

        $scope.elements = [];
        addElement();
      }

      function generateNewElement() {
        const handlerName = generateHandlerName();
        const handler = nvAutocompleteData.getByHandle(handlerName);
        handler.setPossibleOptions(PARAM.options);

        return {
          handler: handler,
          name: handlerName,
          text: '',
          selectedItem: null,
          isFirst: false,
          isSelected: false,
        };
      }

      function generateHandlerName() {
        const uuid = new UUID(4);
        return uuid.format('std');
      }

      function addElement() {
        $scope.elements.push(generateNewElement());
        notifyChanges();
      }

      function removeElement(element) {
        _.remove($scope.elements, el => el.name === element.name);
        _.remove(PARAM.selected, item => PARAM.itemMatchFn(item, element.selectedItem));
        notifyChanges();
      }

      function notifyChanges() {
        _.forEach($scope.elements, (el, idx) => {
          el.isFirst = idx === 0;
        });
        const viewItems = _.cloneDeep($scope.elements);
        const items = _.cloneDeep(PARAM.selected);
        $scope.onChange({ $viewItems: viewItems, $items: items });
      }

      function onSelect(element, item) {
        element.isSelected = true;
        PARAM.selected.push(item);
        notifyChanges();
      }

      function isAddDisabled() {
        const lastEl = _.last($scope.elements);
        const lastElText = _.get(lastEl, 'text');
        if ((_.trim(lastElText) === '') || !_.get(lastEl, 'selectedItem')) {
          return true;
        }
        return false;
      }

      function isOptionsEmpty() {
        return _.size($scope.elements) === 0;
      }

      function isAddHidden() {
        if (_.size($scope.elements) >= $scope.maxItem) {
          return true;
        }
        return false;
      }

      function onBlur() {
        $timeout(cleanUp);
        function cleanUp() {
          const validInput = _.filter($scope.elements, el => el.selectedItem !== null);
          if (_.size(validInput) !== _.size(PARAM.selected)) {
            _.each(PARAM.selected, (el) => {
              const isFound = !!_.find(validInput, e => e.selectedItem === el);
              if (!isFound) {
                _.remove(PARAM.selected, v => v === el);
              }
            });
          }
          let invalidInput = [];
          if (_.size($scope.elements) > 1) {
            invalidInput = _.filter($scope.elements, el =>
              el.selectedItem === null && el.isSelected
            );
          }
          if (_.size(invalidInput) > 0) {
            _.remove($scope.elements, el => _.head(invalidInput).name === el.name);
          }
          notifyChanges();
        }
      }
    }
  }
}());
