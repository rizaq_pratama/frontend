(function tagDirective() {
  angular
    .module('nvCommons.directives')
    .directive('nvTag', tag);

  tag.$inject = ['nvUtils'];

  function tag(nvUtils) {
    return {
      restrict: 'E',
      templateUrl: 'lib/ninja-commons/directives/tag/tag.directive.html',
      scope: {
        mdTheme: '@?',
        nvClass: '@?',
        name: '@',
        onClick: '&?',
      },
      compile: compile,
    };

    function compile(elem, attr) {
      if (attr.mdTheme) {
        elem.find('span').addClass(nvUtils.toThemeClass(attr.mdTheme));
      }

      if (attr.nvClass) {
        elem.find('span').addClass(attr.nvClass);
      }
      return link;
    }

    function link(scope) {
      scope.onClick = scope.onClick ? scope.onClick : angular.noop;
    }
  }
}());
