(function() {
    'use strict';

    angular
        .module('nvCommons.directives')
        .directive('nvAutocompleteListWithCheckbox', directive);

    directive.$inject = ['nvCoupledListUtils'];

    function directive(nvCoupledListUtils) {

        return {
            restrict: 'E',
            templateUrl: 'lib/ninja-commons/directives/autocomplete-list/autocomplete-list-with-checkbox/autocomplete-list-with-checkbox.directive.html',
            scope: {
                searchText: '=',
                possibleOptions: '=', // array of objects like {id: ..., displayName: ..., lowercaseName: ..., _selected: true/false}
                selectedOptions: '=', // array of objects like {id: ..., displayName: ..., lowercaseName: ..., _selected: true/false}
                onAppendOne: '&',
                onSelectAll: '&',
                onUnSelectAll: '&',
                placeholder: '@'
            },
            link: link
        };

        function link(scope) {

            scope.searchText = scope.searchText || '';
            scope.possibleOptions.sort(sortFn);
            scope.selectedOptions.sort(sortFn);

            scope.querySearch = function(searchText) {
                var queryText = (searchText || '').trim().toLowerCase();

                /* jshint -W014 */
                return !queryText
                    ? scope.possibleOptions
                    : _.filter(scope.possibleOptions, filterFn(queryText));
                /* jshint +W014 */
            };

            scope.onSelectedItemChange = function(item) {
                if (!item) { return; }

                nvCoupledListUtils.transfer(
                    scope.possibleOptions,
                    scope.selectedOptions,
                    matchFn(item), {sort: sortFn}
                );

                scope.onAppendOne({item: item});
            };

            scope.onClickSelectAll = function() {
                _.forEach(scope.selectedOptions, function(option) {
                    option._selected = true;
                });
            };

            scope.onClickUnSelectAll = function() {
                _.forEach(scope.selectedOptions, function(option) {
                    option._selected = false;
                });
            };

            function filterFn(queryText) {
                return function(option) {
                    return option.lowercaseName.indexOf(queryText) >= 0;
                };
            }

            function matchFn(item) {
                return function(fromItem) {
                    return fromItem.id === item.id;
                };
            }

            function sortFn(a, b) {
                if (a.displayName < b.displayName) { return -1; }
                if (a.displayName > b.displayName) { return  1; }
                return 0;
            }

        }

    }

})();
