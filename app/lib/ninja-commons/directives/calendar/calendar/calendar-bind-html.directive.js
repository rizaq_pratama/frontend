(function() {
    'use strict';

    angular
        .module('nvCommons.directives')
        .directive('nvCalendarBindHtml', bindHTML);

    bindHTML.$inject = ['nvCalendar.Data', '$templateRequest', '$compile', '$q'];

    // Bind html specified in template_url, specifically for Calendar Directives,
    // calendar data from nvCalender.Data will be as $calendar object within the template scope.

    function bindHTML(CalendarData, $templateRequest, $compile, $q) {

        return {
            restrict: 'E',
            templateUrl: 'lib/ninja-commons/directives/calendar/calendar/calendar-bind-html.directive.html',
            replace: true,
            scope: {
                date: '=',
                isActive: '=?'
            },
            link: function ($scope, $element, $attrs) {
                var calendarData = CalendarData.getByHandle($attrs.delegateHandle);
                var calendar = $scope.$parent.$parent.$parent;

                var children = $element.children();
                var dateLabel = angular.element(children[0]);
                var loader = angular.element(children[1]);
                var container = angular.element(children[2]);
                var lastClass = '';
                var loading = false;

                hideAll();

                if(angular.isDefined($attrs.templateUrl) && $attrs.templateUrl !== ''){
                    $templateRequest($attrs.templateUrl)
                    .then(function(data){
                        return insertElement(data);
                    });
                }else{
                    loading = false;
                }

                // id is the date of this directive,
                // when date changed, load new data
                $scope.$watch('date', function(val){
                    var date = moment(val);
                    dateLabel.text(date.format(calendar.dayFormat));
                    _.defer(getContent, date);
                });

                var offRefresh = calendarData.onRefresh(refresh);

                // do not allow click when is loading
                $element.bind('click', function(e){
                    if(loading){
                        e.stopImmediatePropagation();
                    }
                });

                $scope.$on('$destroy', function(){
                    offRefresh();
                });

                /////////////////////////
                
                function refresh(){
                   var date = moment($scope.date);
                    dateLabel.text(date.format(calendar.dayFormat));
                    _.defer(getContent, date); 
                }

                function getContent(date) {
                    var content = calendarData.getDayContent(date);

                    if(angular.isUndefined(content)){
                        return;
                    }

                    if(isPromise(content)) {
                        showLoading();
                    }
                    //clean up
                    $element.removeClass(lastClass);

                    $q.when(content)
                    .then(function(data){
                        //set scope
                        $scope.$calendar = data;
                        if($attrs.noCache === "false") {
                            calendarData.setDayContent(date, data);
                        }
                        //set theme
                        if(angular.isDefined(data._theme)){
                            $element.addClass(data._theme);
                            lastClass = data._theme;
                        }
                    })
                    .finally(function(){
                        showContent();
                    });
                }

                function insertElement(element) {
                    var compiled = $compile(element)($scope);
                    container.append(compiled);
                    showContent();
                }

                function isPromise(obj){
                    return angular.isObject(obj) && (angular.isFunction(obj.then) || angular.isFunction(obj.success));
                }

                function hideAll(){
                    loading = true;
                    loader.css('display', 'none');
                    container.css('display', 'none');
                }

                function showLoading(){
                    loading = true;
                    loader.css('display', 'inherit');
                    container.css('display', 'none');
                }

                function showContent(){
                    loading = false;
                    loader.css('display', 'none');
                    container.css('display', 'inherit');
                }
            }
        };
    }
})();
