(function() {
    'use strict';

    /**
     *  Based on: https://angular-material-calendar.bradb.net/
     *  Docs: https://github.com/jsmodules/angular-material-calendar *
     *  
     *  Different:
     *  * nvCalendarData.getByHandle(handle).setDataContent(content) instead of MaterialCalendarData.setDayContent(content)
     *    - handle is the `delegateHandle` attribute of the directive
     *  * `template` and `templateUrl` attribute is no longer being used
     */

    angular
        .module('nvCommons.directives')
        .factory('nvCalendar.Calendar', CalendarService);

    CalendarService.$inject = ['DelegateHandle'];

    function CalendarService (DelegateHandle) {
        return DelegateHandle.create(Calendar, 'calendar');
    }

    Calendar.$inject = ['Listener'];
    function Calendar(Listener) {

        var daysInMonth = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];

        var calendar = this;
        calendar.weeks = [];
        calendar.listeners = Listener.create();
        
        //public methods
        calendar.next = next;
        calendar.prev = prev;
        calendar.gotoMonth = gotoMonth;
        calendar.gotoYear = gotoYear;
        calendar.gotoMonthYear = gotoMonthYear;
        calendar.init = init;
        calendar.getFirstDayOfTheMonth = getFirstDayOfTheMonth;
        calendar.getLastDayOfTheMonth = getLastDayOfTheMonth;
        calendar.getFirstDayInCalendar = getFirstDayInCalendar;
        calendar.getLastDayInCalendar = getLastDayInCalendar;
        calendar.getCurrentMonth = getCurrentMonth;
        calendar.getCurrentYear = getCurrentYear;
        calendar.onChangeMonth = calendar.listeners.on;
        
        // public variables
        var now = moment();            
        calendar.year = now.year();
        calendar.month = now.month();
        calendar.firstInCalendar = null;
        calendar.lastInCalendar = null;
        calendar.firstInMonth = null;
        calendar.lastInMonth = null;
        calendar.weekStartsOn = 1;  //standardize to Monday 
        calendar.startDateOfMonth = 1;

        calendar.init(now.year(), now.month());
        
        ////////////////////////////

        function getFirstDayOfTheMonth(){
            return angular.copy(calendar.firstInMonth);
        }

        function getLastDayOfTheMonth(){
            return angular.copy(calendar.lastInMonth);
        }

        function getFirstDayInCalendar(){
            return angular.copy(calendar.firstInCalendar);
        }

        function getLastDayInCalendar(){
            return angular.copy(calendar.lastInCalendar);
        }

        function getCurrentMonth(){
            return calendar.month;
        }

        function getCurrentYear(){
            return calendar.year;
        }

        function next() {
            if (calendar.start.month() < 11) {
                calendar.init(calendar.start.year(), calendar.start.month() + 1);
                return;
            }
            calendar.init(calendar.start.year() + 1, 0);
        }

        function prev() {
            if (calendar.month) {
                calendar.init(calendar.start.year(), calendar.start.month() - 1);
                return;
            }
            calendar.init(calendar.start.year() - 1, 11);
        }

        function gotoMonth(month) {
            calendar.init(calendar.start.year(), month);
        }

        function gotoYear(year) {
            calendar.init(year, calendar.start.month());
        }

        function gotoMonthYear(month, year) {
            calendar.init(year, month);
        }

        function notifyChangeListener(){
            var data = {
                year: calendar.year,
                month: calendar.month,
            };
            calendar.listeners.notifyChange(data);
        }

        // Month should be the javascript indexed month, 0 is January, etc.
        function init(year, month) {
            var now = moment();
            calendar.year = angular.isDefined(year) ? year : now.year();
            calendar.month = angular.isDefined(month) ? month : now.month();

            var monthLength = daysInMonth[calendar.month];
            // Figure out if is a leap year.
            if (calendar.month === 1) {
                if ((calendar.year % 4 === 0 && calendar.year % 100 !== 0) || calendar.year % 400 === 0) {
                    monthLength = 29;
                }
            }

            calendar.start = moment([calendar.year, calendar.month, calendar.startDateOfMonth]);
            var date = angular.copy(calendar.start);
            if ( date.date() === 1) {
                while ( date.day() !== calendar.weekStartsOn) {
                    date.subtract(1, 'days');
                    monthLength++;
                }
            }
            // Last day of calendar month.
            monthLength = Math.ceil(monthLength / 7) * 7;

            calendar.firstInCalendar = angular.copy(date).startOf('days');
            calendar.lastInCalendar = angular.copy(date).add(monthLength - 1, 'days').endOf('days');
            calendar.firstInMonth = angular.copy(calendar.start).startOf('days');
            calendar.lastInMonth = angular.copy(calendar.start).endOf('month');

            //generate weeks
            calendar.weeks = [];
            for (var i = 0; i < monthLength; ++i) {

                // Let's start a new week.
                if (i % 7 === 0) {
                    calendar.weeks.push([]);
                }

                // Add copy of the date. If not a copy,
                // it will get updated shortly.
                calendar.weeks[calendar.weeks.length - 1].push(angular.copy(date));

                // Increment it.
                date.add(1, 'days');
            }
            notifyChangeListener();
        }
    }
})();