(function() {
    'use strict';

    /**
     *  Based on: https://angular-material-calendar.bradb.net/
     *  Docs: https://github.com/jsmodules/angular-material-calendar *
     *  
     *  Different:
     *  * nvCalendarData.getByHandle(handle).setDataContent(content) instead of MaterialCalendarData.setDayContent(content)
     *    - handle is the `delegateHandle` attribute of the directive
     *  * `template` and `templateUrl` attribute is no longer being used
     *  * `setDayContent` attribute is no longer being used. Use nvCalendarData.getByHandle(handle).setDataContentFn(content) instead.
     *  * `onPrevMonth`, `onNextMonth` no longer being used, since can change month through the dropdown, use `onChangeMonth` instead.
     *  * `tooltip`, `noOfDays`, `weekStartsOn` and `startDateOfMonth` removed to standardize all calendars
     *  * templateUrl sets the template of day grid of the calendar
     */

    angular
        .module('nvCommons.directives')
        .directive('nvCalendar', nvCalendar);

    nvCalendar.$inject = ['nvCalendar.Calendar', 'nvCalendar.Data', 'nvDateTimeUtils'];

    function nvCalendar(Calendar, CalendarData, nvDateTimeUtils) {

        return {
            restrict: 'E',
            templateUrl: 'lib/ninja-commons/directives/calendar/calendar/calendar.directive.html',
            scope: {
                ngModel: '=?',
                onDayClick: '=?',
                onChangeMonth: '=?',
                timezone: '=?',
                dayFormat: '=?',
                dayLabelFormat: '=?',
                clearDataCacheOnLoad: '=?',
                disableFutureSelection: '=?',
                disableSelection: '=?',
                templateUrl: '@?',
            },
            link: function ($scope, $element, $attrs) {
                //data service
                $scope.delegateHandle = angular.isDefined($attrs.delegateHandle) ? $attrs.delegateHandle : CalendarData.generateHandle();
                $scope.dataService = CalendarData.getByHandle($scope.delegateHandle);
                $scope.calendar = Calendar.getByHandle($scope.delegateHandle);
                var offMonthChange = $scope.calendar.onChangeMonth(onMonthChange);

                //default month, year
                var date = new Date();
                $scope.calendar.month = parseInt($attrs.startMonth || date.getMonth());
                $scope.calendar.year = parseInt($attrs.startYear || date.getFullYear());

                // Set the defaults here.
                $scope.monthList = _.times(12, _.partial(_.add, 0));
                $scope.yearList = _.times(5, _.partial(_.add, $scope.calendar.year - 2));
                $scope.timezone = $scope.timezone || null;
                $scope.noCache = $scope.clearDataCacheOnLoad || false;
                $scope.dayLabelFormat = $scope.dayLabelFormat || 'ddd';
                $scope.dayFormat = $scope.dayFormat || 'D';
                $scope.dayIdFormat = 'Y-MM-DD';
                $scope.disableFutureSelection = $scope.disableFutureSelection || false;
                $scope.disableSelection = $scope.disableSelection || false;
                $scope.onDayClick = $scope.onDayClick || angular.noop;
                $scope.onChangeMonth = $scope.onChangeMonth || angular.noop;

                //watchers
                $scope.$on('$destroy', function(){
                    //remove listener, prevent leaking
                    offMonthChange();
                    CalendarData.removeHandle($scope.delegateHandle);
                    Calendar.removeHandle($scope.delegateHandle);
                });

                // methods
                $scope.hasEvents = hasEvents;
                $scope.sameMonth = sameMonth;
                $scope.isDisabled = isDisabled;
                $scope.isActive = isActive;
                $scope.prev = prev;
                $scope.next = next;
                $scope.handleDayClick = handleDayClick;

                //bootstrap
                bootstrap();

                /////////////////////////////////

                function dateFind(arr, date) {
                    return _.findIndex(arr, _.partial(nvDateTimeUtils.compareDate, date));
                }

                function sameMonth(date) {
                    return date.year() === $scope.calendar.year &&
                        date.month() === $scope.calendar.month;
                }

                function isDisabled(date) {
                    if ($scope.disableSelection) { return true; }
                    if ($scope.disableFutureSelection && date > new Date()) { return true; }
                    if ($scope.dataService.getDayDisabled(date)) { return true; }
                    return false;
                }

                function isActive (date) {
                    var match;
                    if (!angular.isArray($scope.ngModel)) {
                        if ($scope.ngModel && nvDateTimeUtils.compareDate($scope.ngModel, date)) {
                            match = true;
                        }
                    } else {
                        match = dateFind($scope.ngModel, date) > -1;
                    }
                    return match;
                }

                function hasEvents (date) {
                    var data = $scope.dataService.getDayContent(date);
                    return (data && data.length > 0);
                }

                function prev() {
                    $scope.calendar.prev();
                }

                function next() {
                    $scope.calendar.next();
                }

                function handleDayClick(date) {

                    if($scope.disableFutureSelection && date > new Date()) {
                        return;
                    }

                    if($scope.disableSelection) {
                        return;
                    }

                    if($scope.isDisabled(date)){
                        return;
                    }

                    if(!$scope.sameMonth(date)){
                        $scope.calendar.gotoMonthYear(date.month(), date.year());
                        return;
                    }

                    var val = $scope.onDayClick(angular.copy(date));
                    // return false to prevent event
                    if(val === false){
                        return;
                    }

                    if (angular.isArray($scope.ngModel)) {
                        var idx = dateFind($scope.ngModel, date);
                        if (idx > -1) {
                            $scope.ngModel.splice(idx, 1);
                        } else {
                            $scope.ngModel.push(date);
                        }
                    } else {
                        if (angular.equals($scope.ngModel, date)) {
                            $scope.ngModel = null;
                        } else {
                            $scope.ngModel = date;
                        }
                    }
                }

                function onMonthChange(data){
                    $scope.yearList = _.times(5, _.partial(_.add, data.year - 2));
                    $scope.onChangeMonth(data);
                }

                function init() {
                    $scope.calendar.init($scope.calendar.year, $scope.calendar.month);
                }

                function bootstrap() {
                    init();
                }
            }
        };
    }
})();