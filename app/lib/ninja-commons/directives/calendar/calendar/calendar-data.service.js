(function() {
    'use strict';

    /**
     *  Based on: https://angular-material-calendar.bradb.net/
     *  Docs: https://github.com/jsmodules/angular-material-calendar *
     *  
     *  Different:
     *  * nvCalendarData.getByHandle(handle).setDataContent(content) instead of MaterialCalendarData.setDayContent(content)
     *    - handle is the `delegateHandle` attribute of the directive
     *  * `template` and `templateUrl` attribute is no longer being used
     */

    angular
        .module('nvCommons.directives')
        .factory('nvCalendar.Data', CalendarDataService);

    CalendarDataService.$inject = ['DelegateHandle'];
    
    function CalendarDataService(DelegateHandle){      
        return DelegateHandle.create(CalendarData, 'calendar-data-');
    }
    
    CalendarData.$inject = ['$q', 'Listener'];

    function CalendarData($q, Listener) {
        /*jshint validthis:true */
        var calendar = this;
        calendar._data = {};
        calendar._dataFn = null;
        calendar._disabled = {};
        calendar._disabledFn = null;
        calendar._refreshListener = Listener.create();

        calendar.setDayContent    = setDayContent;
        calendar.getDayContent    = getDayContent;
        calendar.clearDayContent  = clearDayContent;
        calendar.setDayDisabled   = setDayDisabled;
        calendar.getDayDisabled   = getDayDisabled;
        calendar.clearDayDisabled = clearDayDisabled;
        calendar.clearAll         = clearAll;
        calendar.refresh          = calendar._refreshListener.notify;
        calendar.onRefresh        = calendar._refreshListener.on;
        
        ///////////////////////////////////

        function setDayContent(dateOrFn, content) {
            setFnOrVal(setDayContentFn, setDayContentVal, dateOrFn, content);
        }

        function setDayContentVal(date, content) {
            setDataVal(calendar._data, date, content);
        }

        function setDayContentFn(fn) {
            calendar._dataFn = fn;
        }

        /**
         *
         * @param {MomentDate} date momentdate object for selected date
         * @returns {*}
         */
        function getDayContent(date) {
            return getFromCacheOrFn(calendar._data, calendar._dataFn, date);
        }

        function clearDayContent() {
            calendar._data = {};
        }
        
        function getDayKey(date) {
            return date.format('D-M-Y');
        }

        function setDayDisabled(dateOrFn, disabled) {
            setFnOrVal(setDayDisabledFn, setDayDisabledVal, dateOrFn, disabled);
        }

        function setDayDisabledVal(date, disabled) {
            setDataVal(calendar._disabled, date, disabled);
        }

        function setDayDisabledFn(fn) {
            calendar._disabledFn = fn;
        }

        function getDayDisabled(date) {
            return getFromCacheOrFn(calendar._disabled, calendar._disabledFn, date);
        }

        function clearDayDisabled() {
            calendar._disabled = {};
        }

        function clearAll() {
            clearDayContent();
            clearDayDisabled();
        }

        function setFnOrVal(fnfn, valfn, dateOrFn, val) {
            if(angular.isFunction(dateOrFn)) {
                fnfn(dateOrFn);
            }else{
                valfn(dateOrFn, val);
            }
        }

        function setDataVal(cache, date, val) {
            if(angular.isDefined(val)){
                $q.when(val)
                .then(function(value) {
                    cache[getDayKey(date)] = value;
                });
            }else{
                cache[getDayKey(date)] = '';
            }
        }

        function getFromCacheOrFn(cache, fn, date) {
            if (_.has(cache, getDayKey(date))) {
                return cache[getDayKey(date)];
            }else if(!_.isNull(fn)) {
                return fn(date);
            }else{
                return undefined;
            }
        }
    }
})();