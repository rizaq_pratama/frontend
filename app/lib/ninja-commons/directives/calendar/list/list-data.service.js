(function() {
    'use strict';

    angular
        .module('nvCommons.directives')
        .factory('nvCalendarList.Data', CalendarListDataService);

    CalendarListDataService.$inject = ['DelegateHandle'];
    function CalendarListDataService(DelegateHandle) {
        return DelegateHandle.create(CalendarListData, 'calendar-list');
    }

    CalendarListData.$inject = ['nvDateTimeUtils', 'Listener'];
    function CalendarListData(nvDateTimeUtils, Listener) {
        /*jshint validthis:true */
        var list = this;
        list.listener = Listener.create();
        list.dates = [];
        list._original = [];

        list.add = add;
        list.addAll = addAll;
        list.remove = remove;
        list.removeAll = removeAll;
        list.getDates = getDates;
        list.setDates = setDates;
        list.getOriginal = getOriginal;
        list.getChanges = getChanges;
        list.onChange = list.listener.on;
        list.isChanged = isChanged;
        list.isNew = isNew;
        list.notifyCB = notifyCB;

        ///////////////////////

        function add(date) {
            list.dates.push(date);
            list.notifyCB();
        }
        
        function addAll(dates) {
            list.dates = _.concat(list.dates, dates);
            list.notifyCB();
        }

        function remove(date) {
            _.pull(list.dates, date);
            list.notifyCB();
        }

        function removeAll(dates){
            if(angular.isDefined(dates)){
                _.pullAll(list.dates, dates);
            }else{
                list.dates = [];
            }
            list.notifyCB();
        }

        function getDates() {
            return list.dates;
        }

        function setDates(dates) {
            list.dates = angular.isDefined(dates) ? ( angular.isArray(dates) ? dates : [dates] ) : [];
            list._original = _.clone(list.dates);
        }

        function getOriginal() {
            return list._original;
        }

        function getChanges() {
            return {
                added: differenceBy(list.dates, list._original, nvDateTimeUtils.compareDate),
                removed: differenceBy(list._original, list.dates, nvDateTimeUtils.compareDate)
            };
        }

        function isNew(date){
            return _.find(list._original, _.partial(nvDateTimeUtils.compareDate, date)) === undefined;
        }

        function onChange(cb){
            if(_.indexOf(list._cb, cb) > -1){
                return _.pull(list._cb, cb);
            }
            list._cb.push(cb);
        }

        function isChanged(){
            var changed = list.getChanges();
            return changed.added.length === 0 && changed.removed.length === 0;
        }

        function notifyCB(){
            var changed = list.getChanges();
            list.listener.notify(changed);
        }

        function differenceBy(d1, d2){
            return _.filter(d1, function(date){
                return angular.isUndefined(_.find(d2, _.partial(nvDateTimeUtils.compareDate, date)));
            });
        }
    }

})();
