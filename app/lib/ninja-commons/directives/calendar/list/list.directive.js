(function() {
    'use strict';

    angular
        .module('nvCommons.directives')
        .directive('nvCalendarList', calendarList);

    calendarList.$inject = ['$parse', 'nvCalendar.Data', 'nvCalendarList.Data'];
    function calendarList($parse, nvCalendarData, nvCalendarListData) {

        var idx = 0;

        return {
            restrict: 'E',
            templateUrl: 'lib/ninja-commons/directives/calendar/list/list.directive.html',
            scope: {
                label: '@',
                ngModel: '=',
                disabled: '=',
            },
            link: link
        };

        function link(scope, elem, attrs) {
            //handle and data
            scope.delegateHandle = angular.isDefined(attrs.delegateHandle) ? attrs.delegateHandle : 'calendar-list-' + idx++;

            //watch any changes in the ngModel
            scope.$watch('ngModel', function(ngModel){
                var model = transformIntoDateArray(ngModel, attrs.ngModelProp);
                scope.calendarListData.setDates(model);
            });

            //data services
            scope.calendarData = nvCalendarData.getByHandle(scope.delegateHandle);
            scope.calendarListData = nvCalendarListData.getByHandle(scope.delegateHandle);

            //set up property
            scope.year = moment().year();
            generateYearList();
            //Update the year list based on year selected
            scope.$watch('year', function(year){
                if(angular.isDefined(year)) { 
                    generateYearList();
                }
            });

            //watch changes made in calendar
            scope.$watch('calendarListData.dates.length', function(){
                scope.calendarListData.notifyCB();
            });

            scope.$on('$destroy', function(){
                //clean up
                nvCalendarData.removeHandle(scope.delegateHandle);
                nvCalendarListData.removeHandle(scope.delegateHandle);
            });

            //UI functions
            scope.removeDate = removeDate;

            function generateYearList(){
               scope.yearList = _.times(5, _.partial(_.add, scope.year - 2)); 
            }
            function removeDate(date){
                scope.calendarListData.remove(date);
            }
            function transformIntoDateArray(array, prop){
                var transform = angular.isDefined(prop) ? _.property(prop) : _.identity;
                return _(array)
                        .map(transform)
                        .map(function(date){
                            return moment(date); 
                        })
                        .value();
            }
        }
    }

})();
