(function directive() {
  angular
        .module('nvCommons.directives')
        .directive('nvHint', hint);

    /**
     *
     * @param {text}      text         text inside the hint element
     * @param {icon}      icon         icon
     * @param {type}      type         type , possible values:: ['info', 'warning', 'error']
     *
     */


  function hint() {
    return {
      restrict: 'E',
      templateUrl: 'lib/ninja-commons/directives/hint/hint.directive.html',
      replace: true,
      scope: {
        text: '@',
        icon: '@?',
        type: '@?',
      },
      link: link,
    };

    function link(scope) {
      scope.type = scope.type || 'info';
      scope.icon = scope.icon || 'info';
    }
  }
}());
