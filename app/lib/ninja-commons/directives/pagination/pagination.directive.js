(function () {
    'use strict';

    angular
        .module('nvCommons.directives')
        .directive('nvPagination', pagination);

    pagination.$inject = ['$timeout'];

    function pagination($timeout) {

        return {
            restrict: 'E',
            template: nvPaginationTemplate,
            replace: true,
            scope: {
                collection: '=',
                onRefreshCollection: '=',
                onFetchPagePromise: '=',
                onGetItem: '=',
                onClickItem: '=',
                postClickItem: '='
            },
            link: link
        };

        function nvPaginationTemplate($element) {
            /* md-virtual-repeat doesn't support ng-transclude.
             the implementation of this html template is similar to the one in http://goo.gl/UB0GjY.
             Using this, we are able to wrap container as the column in nv-pagination
             */
            return '<div class="nv-pagination">' +
                '<md-virtual-repeat-container ng-style="listStyle">' +
                '<div md-virtual-repeat="item in collection" md-on-demand class="collection-container">' +
                $element[0].innerHTML +
                '</div>' +
                '</md-virtual-repeat-container>' +
                '</div>';
        }

        function link(scope, element) {

            ////////////////////////////////////////////////////////////////

            /* needed for md-on-demand in md-virtual-repeat */
            var Collection = function () {
                this.loadedPages = {};
                this.loadedItems = [];
                this.numItems = 0;
                this.numFilteredItems = 0;
                this.itemsPerPage = 10;
                this.fetchNumItems_();
            };

            // Required.
            Collection.prototype.getItemAtIndex = function (index) {
                var pageNumber = Math.floor(index / this.itemsPerPage) + 1;
                var page = this.loadedPages[pageNumber];
                if (page) {
                    return page[index % this.itemsPerPage];
                } else if (page !== null) {
                    this.fetchPage_(pageNumber);
                }
            };

            // Required.
            Collection.prototype.getLength = function () {
                return this.numFilteredItems;
            };

            Collection.prototype.fetchPage_ = function(pageNumber) {
                this.loadedPages[pageNumber] = null;

                var options = {
                    page: pageNumber,
                    itemsPerPage: this.itemsPerPage,
                    all: false
                };

                scope.onFetchPagePromise(options).then(angular.bind(this, function(result) {
                    this.numItems = result.count;
                    this.numFilteredItems = result.count;
                    var col = scope.onGetItem(result);
                    this.loadedPages[pageNumber] = col;
                    this.loadedItems = this.loadedItems.concat(col);
                }));
            };

            Collection.prototype.fetchNumItems_ = function () {
                this.fetchPage_(1);
            };

            scope.collection = new Collection();
            scope.onRefreshCollection = refreshCollection;

            /* md-virtual-container does not allow dynamic height
             * https://github.com/angular/material/issues/4314     */
            scope.listStyle = {
                height: (window.innerHeight - getOccupiedHeight()) + 'px'
            };

            window.addEventListener('resize', onResize);

            ////////////////////////////////////////////////////////////////

            function onResize() {
                $timeout(function() {
                    scope.listStyle.height = (window.innerHeight - getOccupiedHeight()) + 'px';
                });
            }

            function getOccupiedHeight() {
                var totalHeight = 0;

                _.forEach(element[0].parentNode.children, function(el){
                    if(!angular.element(el).hasClass("nv-pagination")) {
                        totalHeight = totalHeight + el.offsetHeight;
                    }
                });
                return totalHeight;
            }

            function refreshCollection() {
                scope.collection = new Collection();
            }

            ////////////////////////////////////////////////////////////////
        }
    }

})();