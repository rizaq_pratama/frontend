(function tagDirective() {
  angular
    .module('nvCommons.directives')
    .directive('nvAddressFinder', addressFinder);

  addressFinder.$inject = [
    'nvAddress', '$rootScope', 'nvTable', 'nvMaps', '$timeout',
    'nvDomain',
  ];

  function addressFinder(
    nvAddress, $rootScope, nvTable, nvMaps, $timeout,
    nvDomain
  ) {
    addressFinderController.$inject = ['$scope'];

    return {
      restrict: 'E',
      templateUrl: 'lib/ninja-commons/directives/address-finder/address-finder.directive.html',
      scope: {
        onSearchModeChanged: '&?',
        callback: '&',
      },
      controller: addressFinderController,
      controllerAs: 'ctrl',
    };

    function addressFinderController(scope) {
      // variables
      const ctrl = this;
      ctrl.searchMode = '10';
      ctrl.byName = {
        form: {},
        state: 'idle',
        value: null,
        isDisabled: searchTermIsDisabled,
        saveLocation: searchTermSaveLocation,
        tableParam: nvTable.createTable(),
        searchAddress: searchAddress,
      };

      ctrl.byCoordinates = {
        form: {},
        state: 'idle',
        latitude: null,
        longitude: null,
        latLngOnChange: latLngOnChange,
        isDisabled: latLngIsDisabled,
        saveLocation: latLngSaveLocation,
        map: null,
        marker: null,
      };

      ctrl.byName.tableParam.addColumn('_score', { displayName: 'commons.jaro-score' });
      ctrl.byName.tableParam.addColumn('_fullAddress', { displayName: 'commons.address' });

      // functions
      ctrl.onSearchModeChanged = onSearchModeChanged;

      // functions details
      function onSearchModeChanged() {
        if (ctrl.searchMode === '01') { // by coordinates
          $timeout(() => {
            ctrl.byCoordinates.map = nvMaps.initialize('address-finder-map');

            const options = {
              latitude: 0,
              longitude: 0,
              draggable: false,
            };
            const iconColorConfig = {
              size: 'M',
              markerColorCode: 'fe0000',
            };

            ctrl.byCoordinates.marker = nvMaps.addMarker(ctrl.byCoordinates.map, _.merge(
              options, nvMaps.getIconData(iconColorConfig)
            ));
          });
        }

        if (scope.onSearchModeChanged) {
          scope.onSearchModeChanged({ searchMode: ctrl.searchMode });
        }
      }

      function searchTermIsDisabled() {
        return !ctrl.byName.value;
      }

      function latLngOnChange() {
        $timeout(() => {
          if (!latLngIsDisabled()) {
            const options = {
              latitude: ctrl.byCoordinates.latitude,
              longitude: ctrl.byCoordinates.longitude,
            };

            nvMaps.markerSetLatLng(ctrl.byCoordinates.marker, options);
            nvMaps.flyToLocation(ctrl.byCoordinates.map, options);
          }
        });
      }

      function latLngIsDisabled() {
        return _.isNil(ctrl.byCoordinates.latitude) || _.isNil(ctrl.byCoordinates.longitude) ||
          isNaN(ctrl.byCoordinates.latitude) || isNaN(ctrl.byCoordinates.longitude) ||
          ctrl.byCoordinates.latitude === '' || ctrl.byCoordinates.longitude === '';
      }

      function searchAddress() {
        const domain = nvDomain.getDomain();
        nvAddress.lookup(ctrl.byName.value, domain.currentCountry).then(success);

        function success(response) {
          ctrl.byName.tableParam.setData(extendAddresses(response));
          ctrl.byName.tableParam.refreshData();
        }
      }

      function searchTermSaveLocation(address) {
        scope.callback({
          address: {
            fullAddress: nvAddress.format(address, $rootScope.domain),
            address1: nvAddress.address1(address),
            address2: nvAddress.address2(address),
            city: nvAddress.city(address),
            country: nvAddress.country(address),
            postcode: address.postcode || '',
            latitude: address.latitude,
            longitude: address.longitude,
          },
        });
      }

      function latLngSaveLocation() {
        nvMaps.reverseGeocoding(
          ctrl.byCoordinates.latitude, ctrl.byCoordinates.longitude
        ).then(success);

        function success(response) {
          if (_.size(response)) {
            const address = response[0];

            scope.callback({
              address: {
                fullAddress: nvAddress.extract(address),
                address1: address.address1,
                address2: address.address2,
                city: '',
                country: '',
                postcode: '',
                latitude: address.latitude,
                longitude: address.longitude,
              },
            });
          } else {
            scope.callback();
          }
        }
      }

      function extendAddresses(addresses) {
        return _.map(addresses, address => (
          setCustomAddressData(address)
        ));

        function setCustomAddressData(address) {
          address._score = setScore(address.jaro_winkler_score);
          address._fullAddress = nvAddress.format(address, $rootScope.domain);
          return address;
        }

        function setScore(score) {
          return Math.ceil(score) === score ? score.toFixed(1) : score.toFixed(2).substr(1);
        }
      }
    }
  }
}());
