(function directive() {
  angular
    .module('nvCommons.directives')
    .directive('nvSelectableImage', nvSelectableImage);

  nvSelectableImage.$inject = ['$timeout'];
  /**
   * refer to:
   * https://www.npmjs.com/package/fancybox
   * https://fancyapps.com/fancybox/3/docs/#options
   */
  function nvSelectableImage($timeout) {
    let idx = 0;

    return {
      restrict: 'E',
      templateUrl: 'lib/ninja-commons/directives/selectable-image/selectable-image.directive.html',
      scope: {
        source: '@',
        title: '@?',
        options: '=?',
        id: '=?',
        active: '=?',
        thumbnail: '@?',
        showcheckbox: '=?',
        showdelete: '=?',
        ondelete: '&?',
      },
      link: link,
    };

    function link(scope) {
      scope.title = angular.isDefined(scope.title) ? scope.title : 'info unavailable';
      scope.thumbnail = angular.isDefined(scope.thumbnail) ? scope.thumbnail : scope.source;
      scope.idx = idx += 1;
      scope.id = angular.isDefined(scope.id) ? scope.id : scope.idx;
      scope.showcheckbox = angular.isDefined(scope.showcheckbox) ? scope.showcheckbox : true;
      scope.showdelete = angular.isDefined(scope.showdelete) ? scope.showdelete : false;
      scope.active = angular.isDefined(scope.active) ? scope.active : false;

      const options = _.defaults(scope.options, {
        protect: true,
        title: scope.title,
        loop: true,
        animationEffect: 'fade',
        type: 'image',
      });

      scope.onClick = () => {
        $.fancybox.open($('.nv-image-wrapper'), options);
        const elements = $('.nv-image-wrapper');
        const theElement = $(`.nv-image-wrapper[data-image-id='${scope.id}']`);
        if (elements && theElement) {
          const index = elements.index(theElement);
          $timeout(() => $.fancybox.jumpto(index), 500);
        } else {
          $timeout(() => $.fancybox.jumpto(scope.id), 500);
        }
      };
    }
  }
}());
