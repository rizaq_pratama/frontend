;(function() {
    'use strict';

    angular
        .module('nvCommons.directives')
        .directive('nvButtonWorking', buttonWorking);

    function buttonWorking() {

        return {
            restrict: 'E',
            templateUrl: 'lib/ninja-commons/directives/forms/buttons/button-working/button-working.directive.html',
            scope: {
                nameDefault: '@',
                nameWorking: '@',
                nameSuccess: '@',
                nameFailure: '@',
                state: '=', // can be either 'default', 'working', 'success', 'failure'
                onClick: '&',
                isDisabled: '='
            },
            link: link
        };

        function link(scope) {
            scope._isDisabled = isDisabled;

            function isDisabled() {
                return scope.isDisabled || scope.state === 'working';
            }
        }

    }

})();
