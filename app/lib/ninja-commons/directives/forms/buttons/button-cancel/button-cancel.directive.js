;(function() {
    'use strict';

    angular
        .module('nvCommons.directives')
        .directive('nvButtonCancel', buttonCancel);

    function buttonCancel() {

        return {
            restrict: 'E',
            templateUrl: 'lib/ninja-commons/directives/forms/buttons/button-cancel/button-cancel.directive.html',
            scope: {
                form: '=',
                onClick: '&',
                isDisabled: '&?'
            },
            link: link
        };

        function link(scope) {
            scope._isDisabled = isDisabled;

            function isDisabled() {
                return scope.isDisabled() || (scope.form && !scope.form.$dirty);
            }
        }

    }

})();
