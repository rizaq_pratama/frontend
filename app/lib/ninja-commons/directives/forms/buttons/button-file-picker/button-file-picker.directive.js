(function directive() {
  angular
    .module('nvCommons.directives')
    .constant('nvButtonFilePickerType', buttonFilePickerType())
    .directive('nvButtonFilePicker', buttonFilePicker);

  /**
   * Common file types for the {@code accept=""} string.
   */
  function buttonFilePickerType() {
    const self = {
      ANY: '*',
      AUDIO: '.mp3',
      CSV: '.csv',
      IMAGE: '.jpg,.jpeg,.png,.gif,.webp,.JPEG,.JPG,.PNG,.GIF',
      JS: '.js',
      JSON: '.json',
      GEOJSON: '.geojson',
      PDF: '.pdf',
      EXCEL: '.xls,.xlsx',
      EXCEL_AND_CSV: '.xls,.xlsx,.csv',

      /**
       * Use to specify more than one value,
       * eg. nvButtonFilePickerType.join(nvButtonFilePickerType.AUDIO, nvButtonFilePickerType.IMAGE)
       * @returns {string} The concatenation of the types.
       */
      join: (...args) => Array.prototype.slice.call(args).join(','),

      /**
       * Use to create a RegExp for the {@code accept=""} string.
       * @param str A comma-separated list of file types.
       * @returns {RegExp} The file type regex to match.
       */
      regex: (str) => {
        return (str === self.ANY || _.isEmpty(str))
          ? new RegExp()
          : new RegExp(_.map(str.split(','), regexpTemplate).join('|'));

        function regexpTemplate(extension) {
          return `(\\${extension})$`;
        }
      },
    };
    return self;
  }

  buttonFilePicker.$inject = ['nvButtonFilePickerType', '$timeout', '$parse'];

  /**
   * A button that wraps a file input.
   * Attributes:
   *   label - label of the button
   *   disabled - disable the button
   *   accept - file type the file picker accepts, eg: `nvButtonFilePickerType.IMAGE`
   *   multiple - true/false, allow the file picker to pick multiple files
   *   onFileSelect - callback fn when files are selected, one per file, eg. `doSomething($file)`
   *   onFileReject - callback fn when files are rejected, because the file extension
   *                  does not match the accept regex, eg. `doSomething($file)`
   */
  function buttonFilePicker(nvButtonFilePickerType, $timeout, $parse) {
    let i = 0; // need a unique id for selecting the input correctly

    return {
      restrict: 'E',
      templateUrl: 'lib/ninja-commons/directives/forms/buttons/button-file-picker/button-file-picker.directive.html',
      scope: {
        label: '@',
        disabled: '=?',
        accept: '@?',
        multiple: '@?',
        icon: '@?',
        name: '@?',
      },
      link: link,
    };

    function link(scope, elem, attr) {
      scope.id = i += 1;

      _(elem[0].classList)
        .difference(['ng-isolate-scope'])
        .forEach(moveClassToChild);

      if (angular.isDefined(scope.icon)) {
        elem[0].children[0].classList.add('nv-button-with-icon');
      }

      const elFileInput = angular.element(elem[0].querySelector('input'));

      scope.accept = angular.isDefined(scope.accept) ? scope.accept : nvButtonFilePickerType.ANY;
      scope.disabled = angular.isDefined(scope.disabled) ? scope.disabled : false;
      scope.multiple = angular.isDefined(scope.multiple) ? (scope.multiple === 'true') : true;
      scope.onFileSelectCB = angular.isDefined(attr.onFileSelect)
        ? wrapFileChange(attr.onFileSelect)
        : angular.noop;
      scope.onFileRejectCB = angular.isDefined(attr.onFileReject)
        ? wrapFileChange(attr.onFileReject)
        : angular.noop;
      scope.acceptRegex = nvButtonFilePickerType.regex(scope.accept);
      scope.name = angular.isDefined(scope.name) ? scope.name : `file-${scope.id}`;
      scope.openDialog = openDialog;
      scope.onFileChange = onFileChange;

      elFileInput.bind('change', scope.onFileChange);
      if (scope.multiple) {
        elFileInput.attr('multiple', 'multiple');
      }

      // //////////////////////////////

      function openDialog(event) {
        if (event) {
          $timeout(() => {
            event.preventDefault();
            event.stopPropagation();
            const eFileInput = event.target.children[2];
            if (eFileInput !== undefined) {
              eFileInput.click();
            }
          });
        }
      }

      function onFileChange(event) {
        const files = event.files || event.target.files;
        if (files.length <= 0) {
          return;
        }

        _.each(files, fileCB);
        if (event.target) {
          event.target.value = null;
        }
      }

      function wrapFileChange(fileChange) {
        const parsedFileChange = $parse(fileChange);
        return file => parsedFileChange(_.defaults({ $file: file }, scope.$parent));
      }

      function fileCB(file) {
        if (scope.acceptRegex.test(file.name)) {
          scope.onFileSelectCB(file);
        } else {
          scope.onFileRejectCB(file);
        }
      }

      function moveClassToChild(clazz) {
        elem[0].classList.remove(clazz);
        elem[0].children[0].classList.add(clazz);
      }
    }
  }
}());
