(function directive() {
  angular
    .module('nvCommons.directives')
    .directive('nvButtonGroup', buttonGroup);

  function buttonGroup() {
    const ATTRIBUTE_NAME = 'titleForModelIdx';
    let btnGroupIdx = 0;

    return {
      restrict: 'E',
      require: '?ngModel',
      templateUrl: 'lib/ninja-commons/directives/forms/buttons/button-group/button-group.directive.html',
      scope: {
        model: '=ngModel', // e.g. '111000' where 'title-for-model-idx-0' is the title for the left-most index
        label: '@',
        form: '=?',
        required: '=?',
      },
      compile: compile,
    };

    function compile(tElement, tAttrs) {
      let index = 0;
      let title = tAttrs[ATTRIBUTE_NAME + index];
      while (!_.isUndefined(title)) {
        tElement[0].children[1].innerHTML +=
          `${'<md-button' +
          ' type="button"' +
          ' class="button-group-button"' +
          ' aria-label="'}${title}"` +
          ` ng-click="onClick(${index})"` +
          ` ng-class="{\'active\': +model[${index}]}">${
          title
          }</md-button>`;
        title = tAttrs[ATTRIBUTE_NAME + (++index)];
      }
      return linkWrapper(index);
    }

    function linkWrapper(count) {
      return function link(scope, elem, attrs, ngModel) {
        if (!angular.isDefined(scope.model)) {
          scope.model = _.repeat('0', count); // default all to false if undefined
        }

        scope.selected = getSelectedNames(scope.model, attrs);

        scope.onClick = (index) => {
          const str = scope.model;
          scope.model = str.substr(0, index) + ((+str[index] + 1) % 2) + str.substr(index + 1);

          scope.selected = getSelectedNames(scope.model, attrs);

          ngModel.$setDirty();
          if (scope.required) {
            ngModel.$setValidity('require', _.indexOf(scope.model, '1') > -1);
          }
        };

        ngModel.$name = `button-group-${btnGroupIdx++}`;
        if (angular.isDefined(scope.form)) {
          scope.form.$addControl(ngModel);
        }
        scope.$on('$destroy', () => {
          if(scope.form) {
            scope.form.$removeControl(ngModel);
          }
        });

        function getSelectedNames(bits, attribute) {
          return _.reduce(bits, (arr, val, idx) =>
            (+val ? arr.concat([attribute[ATTRIBUTE_NAME + idx]]) : arr),
          []).join(', ');
        }
      };
    }
  }
}());
