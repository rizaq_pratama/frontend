;(function() {
    'use strict';

    /**
     * model: Integer{0,1,2,3} timewindow id
     * startTime and endTime [moment Object]
     * 	- startTime and endTime is one-way, changing model will set startTime and endTime,
     * 	  but changing startTime/endTime will not change the model.
     */

    angular
        .module('nvCommons.directives')
        .directive('nvButtonTimeslot', buttonTimeslot);

    function buttonTimeslot() {

        return {
            restrict: 'E',
            templateUrl: 'lib/ninja-commons/directives/forms/buttons/button-timeslot/button-timeslot.directive.html',
            scope: {
                form: '=?',
                model: '=?',
                startTime: '=?',
                endTime: '=?',
                showSelected: '=?',
            },
            link: link
        };

        function link(scope) {
        	scope.model = parseInt(scope.model);
        	if(isNaN(scope.model)) {
        		scope.model = 0;
        	}
        	scope.defaultSelection = modelToSelection(scope.model);
          scope.timeslot = scope.defaultSelection;
          scope.showSelected = angular.isDefined(scope.showSelected) ? scope.showSelected : true;

          //It won't update the startTime and endTime when the model is set the first time, 
          //update only after when model is changed through the ui
          var firstTime = true;
        	scope.$watch('model', function(model){
        		scope.timeslot = modelToSelection(model);
            if(firstTime){
              firstTime = false;
            }else{
          		if(angular.isDefined(scope.startTime)){
  							scope.startTime = timeslotToStartTime(scope.model, scope.startTime);
  						}
  						if(angular.isDefined(scope.endTime)){
  							scope.endTime = timeslotToEndTime(scope.model, scope.endTime);
          		}
            }
        	});

        	scope.$watch('timeslot', function(timeslot){
        		scope.model = selectionToModel(timeslot);
        	});

        	///////////////////////////

          function modelToSelection(model){
          	return _.times(4, function(i){ return i === model ? '1' : '0'; }).join('');
          }
          function selectionToModel(selection){
          	return _.indexOf(selection, '1');
          }
          function timeslotToStartTime(timeslot, fromMoment){
            fromMoment = angular.isUndefined(fromMoment) ? moment() : angular.copy(fromMoment);
          	switch(timeslot){
          		case 0:
          			return fromMoment.hour(9).minute(0).second(0);
          		case 1:
          			return fromMoment.hour(12).minute(0).second(0);
          		case 2:
          			return fromMoment.hour(15).minute(0).second(0);
          		case 3:
          			return fromMoment.hour(18).minute(0).second(0);
          	}
          }
          function timeslotToEndTime(timeslot, fromMoment){
            fromMoment = angular.isUndefined(fromMoment) ? moment() : angular.copy(fromMoment);
          	switch(timeslot){
          		case 0:
          			return fromMoment.hour(12).minute(0).second(0);
          		case 1:
          			return fromMoment.hour(15).minute(0).second(0);
          		case 2:
          			return fromMoment.hour(18).minute(0).second(0);
          		case 3:
          			return fromMoment.hour(22).minute(0).second(0);
          	}
          }
        }

    }

})();
