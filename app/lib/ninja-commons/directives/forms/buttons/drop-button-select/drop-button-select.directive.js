(function directive() {
  angular.module('nvCommons.directives')
    .directive('nvDropButtonSelect', nvDropButtonDirective);

  function nvDropButtonDirective() {
    return {
      restrict: 'E',
      templateUrl: 'lib/ninja-commons/directives/forms/buttons/drop-button-select/drop-button-select.directive.html',
      scope: {
        model: '=',
      },
      link: link,
    };

    function link(scope) {
      scope.onClick = onClick;

      _.each(scope.model, (m,i) => {
        if(m.selected) {
          scope.selected = i;
      }});

      scope.selected = scope.selected || 0;
      scope.model[scope.selected].selected = true;

      scope.$on('$destroy', () => {
        _.each(scope.model, m => delete m.selected)
      });

      function onClick(index) {
        if(index === scope.selected) return;
        scope.model[scope.selected].selected = false;
        scope.model[index].selected = true;
        scope.selected = index;
      }
    }
  }
}());