(function directive() {
  angular
    .module('nvCommons.directives')
    .directive('nvButtonSave', buttonSave);

  buttonSave.$inject = ['nvDOMManipulation'];
  function buttonSave(nvDOMManipulation) {
    return {
      restrict: 'E',
      templateUrl: 'lib/ninja-commons/directives/forms/buttons/button-save/button-save.directive.html',
      scope: {
        name: '@',
        icon: '@?',
        form: '=',
        state: '=', // can be either 'clean', 'saving'
        isDisabled: '&?',
        mdTheme: '@?',
        initDirty: '@?',
      },
      link: link,
    };

    function link(scope, iElement) {
      nvDOMManipulation.moveClassesToFirstChild(iElement[0]);

      scope.name = scope.name || 'Save';
      scope._isDisabled = angular.isDefined(scope.isDisabled) ?
        customIsDisabled : defaultIsDisabled;
      scope.mdTheme = scope.mdTheme || 'nvGreen';
      scope.initDirty = scope.initDirty || false;

      if (angular.isDefined(scope.icon)) {
        iElement[0].children[0].classList.add('nv-button-with-icon');
      }

      if (scope.initDirty) {
        scope.form.$setDirty();
      }

      function defaultIsDisabled() {
        return (scope.form && (!scope.form.$dirty || scope.form.$invalid)) || scope.state === 'saving';
      }

      function customIsDisabled() {
        return scope.isDisabled() || defaultIsDisabled();
      }
    }
  }
}());
