(function directive() {
  angular
    .module('nvCommons.directives')
    .directive('nvButtonGroupRadio', buttonGroupRadio);

  function buttonGroupRadio() {
    const ATTRIBUTE_NAME = 'titleForModelIdx';
    const ATTRIBUTE_NAME_ALL = 'titleForAll';
    let btnGroupIdx = 0;

    return {
      restrict: 'E',
      templateUrl: 'lib/ninja-commons/directives/forms/buttons/button-group-radio/button-group-radio.directive.html',
      require: '?ngModel',
      scope: {
        model: '=ngModel', // e.g. '10' where 'title-for-model-idx-0' is the title for the left-most index
        label: '@',
        form: '=?',
        required: '=?',
      },
      compile: compile,
    };

    function compile(tElement, tAttrs) {
      let index = 0;
      let title = tAttrs[ATTRIBUTE_NAME + index];
      while (!_.isUndefined(title)) {
        tElement[0].children[1].innerHTML +=
                    `${'<md-button' +
                    ' type="button"' +
                    ' class="btn button-group-radio-button"' +
                    ' aria-label="'}${title}"` +
                    ` ng-click="onClick(${index})"` +
                    ` ng-class="{\'active\': isActive(${index})}">${
                    title
                    }</md-button>`;
        title = tAttrs[ATTRIBUTE_NAME + (++index)];
      }

      const titleForAll = tAttrs[ATTRIBUTE_NAME_ALL];
      if (!_.isUndefined(titleForAll)) {
        tElement[0].children[1].innerHTML +=
                    `${'<md-button' +
                    ' type="button"' +
                    ' class="btn button-group-radio-button"' +
                    ' aria-label="'}${titleForAll}"` +
                    ' ng-click="onClickAll()"' +
                    ` ng-class="{\'active\': all}">${
                    titleForAll
                    }</md-button>`;
      }

      return linkWrapper(index);
    }

    function linkWrapper(count) {
      return function link(scope, elem, attr, ngModel) {
        if (!angular.isDefined(scope.model)) {
          scope.model = _.repeat('0', count); // default all to false if undefined
        }

        scope.all = _.every(scope.model, i => (+i));

        scope.onClick = (index) => {
          const str = _.repeat('0', scope.model.length);
          if (!scope.all && scope.model[index] === '1') {
            scope.model = str;
          } else {
            scope.model = `${str.substr(0, index)}1${str.substr(index + 1)}`;
          }

          ngModel.$setDirty();
          if (scope.required) {
            ngModel.$setValidity('require', _.indexOf(scope.model, '1') > -1);
          }
          scope.all = false;
        };

        scope.onClickAll = () => {
          if (scope.all) {
            scope.model = _.repeat('0', scope.model.length);
            scope.all = false;
          } else {
            scope.model = _.repeat('1', scope.model.length);
            scope.all = true;
          }

          ngModel.$setDirty();
          if (scope.required) {
            ngModel.$setValidity('require', _.indexOf(scope.model, '1') > -1);
          }
        };

        scope.isActive = index => (scope.all ? false : +scope.model[index]);

        ngModel.$name = `button-group-${btnGroupIdx++}`;
        if (angular.isDefined(scope.form)) {
          scope.form.$addControl(ngModel);
        }
        scope.$on('$destroy', () => {
          if(scope.form) {
            scope.form.$removeControl(ngModel);
          }
        });
      };
    }
  }
}());
