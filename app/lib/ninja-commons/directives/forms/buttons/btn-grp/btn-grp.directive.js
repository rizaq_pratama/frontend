(function directive() {
  angular
        .module('nvCommons.directives')
        .directive('nvBtnGrp', btnGrp);

  function btnGrp() {
    const ATTRIBUTE_NAME = 'labelForModelIdx';

    return {
      restrict: 'E',
      templateUrl: 'lib/ninja-commons/directives/forms/buttons/btn-grp/btn-grp.directive.html',
      replace: true,
      scope: {
        model: '=',            // e.g. '10' where 'label-for-model-idx-0' is the label for the left-most index
        selectMultiple: '&',   // e.g. true or false
        selectNone: '&',       // e.g. true or false
        defaultSelection: '&', // e.g. '10', '01', '11', '00' (default if not specified)
        form: '=?',
        showSelected: '=?',
        onchange: '&?',
        disabled: '&?',
      },
      compile: compile,
    };

    function compile(tElement, tAttrs) {
      let index = 0;
      let label = tAttrs[ATTRIBUTE_NAME + index];
      const theme = tAttrs.mdTheme || 'nvBlue';
      const classNames = tAttrs.class || '';
      const disabled = tAttrs.disabled || false;

      if (theme && !disabled) {
        tElement.children()[0].setAttribute('md-theme', theme);
      }

      if (classNames) {
        tElement.children()[0].className += ` ${classNames}`;
      }

      while (!_.isUndefined(label)) {
        tElement.children()[0].innerHTML +=
                    `${'<md-button' +
                    ' type="button"' +
                    ` ng-disabled="${disabled}"` +
                    ' md-theme="'}${theme}"` +
                    ` class="nv-button ${classNames}"` +
                    ` aria-label="${label}"` +
                    ` ng-click="::onClick(${index})"` +
                    ` ng-class="{\'raised\': +model[${index}], \'ghost\': !+model[${index}]}">${
                    label
                    }</md-button>`;
        label = tAttrs[ATTRIBUTE_NAME + (++index)];
      }

      return linkWrapper(index);
    }

    function linkWrapper(count) {
      return function link(scope, elem, attr) {
        if (!angular.isDefined(scope.model)) {
          scope.model = angular.isDefined(scope.defaultSelection())
                        ? scope.defaultSelection() // use the default selection if defined
                        : _.repeat('0', count);    // default all to false if undefined
        }
        scope.showSelected = angular.isDefined(scope.showSelected) ? scope.showSelected : false;
        scope.selected = getSelectedNames(scope.model, attr);

        scope.onClick = function onClick(index) {
          scope.model = scope.selectMultiple()
                        ? getModelForMultipleSelection(scope.model, index)
                        : getModelForSingularSelection(scope.model, index);

          scope.selected = getSelectedNames(scope.model, attr);

          if (angular.isDefined(scope.form)) {
            scope.form.$setDirty();
          }
        };
        scope.$watch('model', (val, oldVal) => {
          if (oldVal && angular.isFunction(scope.onchange)) {
            scope.onchange({ index: val });
          }
        });

        // make child buttons at least the same width
        // set their min-width property to be the max width amongst themselves.
        const children = _.map($(elem).children(), (child) => { return $(child); });
        const maxWidth = _.max(_.map(children, (child) => { return child.outerWidth(); }));
        _.forEach(children, (child) => { child.css('min-width', `${maxWidth}px`); });

        function getModelForMultipleSelection(model, idx) {
          const befIdx = model.substr(0, idx);
          const aftIdx = model.substr(idx + 1);
          const tModel = befIdx + (+model[idx] + 1) % 2 + aftIdx;

          return !scope.selectNone() && +tModel === 0 ? `${befIdx}1${aftIdx}` : tModel;
        }

        function getModelForSingularSelection(model, idx) {
          const tModel = _.repeat('0', model.length);
          const befIdx = tModel.substr(0, idx);
          const aftIdx = tModel.substr(idx + 1);

          return befIdx + (scope.selectNone() ? (+model[idx] + 1) % 2 : '1') + aftIdx;
        }

        function getSelectedNames(bits, theAttr) {
          return _.reduce(bits, (arr, val, idx) => {
            return +val ? arr.concat([theAttr[ATTRIBUTE_NAME + idx]]) : arr;
          }, []).join(', ');
        }
      };
    }
  }
}());
