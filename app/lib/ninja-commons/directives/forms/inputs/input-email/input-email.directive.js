(function directive() {
  angular
        .module('nvCommons.directives')
        .directive('nvInputEmail', inputEmail);

    /**
     *
     * @param {string}      label        the label of the input
     * @param {model}       model        the model for the input
     * @param {form}        form         the form object
     * @param {boolean=}    disabled     if true, disable the input, default false
     * @param {boolean=}    required     if true, make input required, default false
     * @param {string}      helpText     additional help message text under the input
     * @param {integer}     readOnly     if true, the input is not modifiable, default false
     * @param {string}      confirmText  Active confirmation text
     * @param {expression}  confirm      Expression evaluates to the condition whether to show
     *                                   the confirmation text below the input and a tick beside
     *                                   the input, default to false
     *
     */

  inputEmail.$inject = ['$filter'];

  function inputEmail($filter) {
    let idx = 0;
    return {
      restrict: 'E',
      templateUrl: 'lib/ninja-commons/directives/forms/inputs/input-email/input-email.directive.html',
      replace: true,
      scope: {
        name: '@',
        label: '@',
        model: '=',
        form: '=?',
        disabled: '=?',
        required: '=?',
        helpText: '@?',
        readonly: '=?',
        confirmText: '@?',
        confirm: '&?',
        onChange: '&?',
      },
      link: link,
    };

    function link(scope) {
      scope.id = scope.name || (`${$filter('nvNaturalCase2HyphenCase')(scope.label)}-${idx += 1}`);
      scope.disabled = angular.isDefined(scope.disabled) ? scope.disabled : false;
      scope.required = angular.isDefined(scope.required) ? scope.required : false;
      scope.readonly = angular.isDefined(scope.readonly) ? scope.readonly : false;
      scope.onChange = scope.onChange || angular.noop();
      scope.confirm = angular.isDefined(scope.confirm) ?
        scope.confirm : function confirm() { return false; };
    }
  }
}());
