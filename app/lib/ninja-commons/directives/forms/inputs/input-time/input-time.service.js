;(function() {
    'use strict';

    angular
        .module('nvCommons.directives')
        .factory('nvInputTimeService', inputTimeService);

    inputTimeService.$inject = [];

    function inputTimeService() {
    		return {

    				/**
    				 * Accept string and returns moment duration
    				 * eg:
    				 * 	parse('-2m3s') -> negative moment.duration of 2 minutes 3 seconds
    				 *
    				 * Accept unit: y (years), M (months), w (weeks), d (days), h (hours), m (minutes), s (seconds), ms (milliseconds)
    				 * Do not accept decimal.
    				 */

    				parse: parse,
    		};

    		function parse(str){
    				var regex = new RegExp(/(\d+)([yMwdhms]+)/g);
    				var duration = moment.duration();
    				var match = regex.exec(str);
    				while(match !== null){
    						var amount = parseInt(match[1]) || 0;
    						var unit = match[2];
    						duration.add(amount, unit);
    						match = regex.exec(str);
    				}
    				if(angular.isString(str) && str.length > 0 && str.charAt(0) === '-'){
    						duration = moment.duration().subtract(duration);
    				}
    				return duration;
    		}
    }
})();