;(function() {
    'use strict';

    /**
     * nv-input-time is a time input.
     * Pass in only Javascript Date object or Moment object to ng-model.
     *
     * Some validation added to this input:
     * - requireLater, requireLaterOffset :
     *     - require the input time to be later than a date with a offset
     *     - sets form.inputControl.$error.later
     *
     * - requireBefore, requireBeforeOffset :
     *     - require the input time to be before than a date with a offset
     *     - sets form.inputControl.$error.before
     *
     * - offset is passed in as a duration string: 1h30m or -45m
     */

    angular
        .module('nvCommons.directives')
        .directive('nvInputTime', inputTime);

    inputTime.$inject = ['nvInputTimeService'];

    function inputTime(nvInputTimeService) {

        var idx = 0;

        return {
            restrict: 'E',
            templateUrl: 'lib/ninja-commons/directives/forms/inputs/input-time/input-time.directive.html',
            require: '?ngModel',
            scope: {
                label: '@',
                ngModel: '=',
                form: '=?',
                disabled: '=?',
                required: '=?',
                helpText: '@?',
                requireLater: '=?',
                requireBefore: '=?',
                requireLaterOffset: '@?',
                requireBeforeOffset: '@?',
            },
            link: link
        };

        function link(scope, elem, attr, ngModel) {
            if(angular.isUndefined(scope.ngModel)){
                scope.ngModel = moment();
            }
            if(!moment.isMoment(scope.ngModel) && !angular.isDate(scope.ngModel)){
                throw new Error("the 'ngModel' in the nv-input-time must be either a moment Object or a Date Object");
            }
            displayTime(scope.ngModel);

            scope.displayedHourChanged = displayedHourChanged;
            scope.displayedMinuteChanged = displayedMinuteChanged;
            scope.toggleAMPM = toggleAMPM;
            scope.displayAfterBeforeTime = displayAfterBeforeTime;

            ngModel.$name = angular.isDefined(attr.name) ? attr.name : 'input-time-' + (idx++);
            scope.id = ngModel.$name;
            if(angular.isDefined(scope.form)){
                scope.form.$addControl(ngModel);
            }
            scope.$on('$destroy', function() {
                if(angular.isDefined(scope.form)){
                    scope.form.$removeControl(ngModel);
                }
            });

            scope.$watch('ngModel', function(){
                displayTime(scope.ngModel);
            });

            scope.$watch(function(){
                return angular.isDefined(scope.requireLater) ? scope.requireLater.toString() : '';
            }, _setValidity);

            scope.$watch(function(){
                return angular.isDefined(scope.requireBefore) ? scope.requireBefore.toString() : '';
            }, _setValidity);

            var digitRegex = /^[0-9]*$/;

            ////////////////////////////////////
            /// Element Event Binding

            var inputContainer = elem.children().eq(0);
            var inputs = inputContainer.children();
            var hourInput = inputs.eq(0);
            var minuteInput = inputs.eq(2);

            hourInput.bind('mousewheel wheel', getMouseWheelEventCB(incrementHour));
            hourInput.on('keydown', getKeyboardEventCB(incrementHour));

            minuteInput.bind('mousewheel wheel', getMouseWheelEventCB(incrementMinute));
            minuteInput.on('keydown', getKeyboardEventCB(incrementMinute));

            ////////////////////////////////////

            function displayTime(time){
                const t = moment(time);
                scope.hour = t.hour();
                scope.displayedHour = displayHour(scope.hour);
                scope.minute = t.minute();
                scope.displayedMinute = displayMinute(scope.minute);
                scope.ampm = t.hour() >= 12 ? 'PM' : 'AM';
            }

            function displayMinute(minute){
                return (minute < 10) ? ('0' + minute) : minute;
            }

            function displayHour(hour){
                return (hour > 12) ? (hour - 12) : hour;
            }

            function displayedHourChanged(hour){
                if(hour !== ''){
                    hour = parseInt(hour);
                    if(digitRegex.test(hour) && !isNaN(hour) && hour >= 0 && hour < 13){
                        scope.hour = hour;
                        _setHour(calcHour());
                    }
                    scope.displayedHour = displayHour(scope.hour);
                }else{
                    _setValidity();
                }
            }

            function displayedMinuteChanged(minute){
                if(minute !== ''){
                    minute = parseInt(minute);
                    if(digitRegex.test(minute) && !isNaN(minute) && minute >= 0 && minute < 60){
                        scope.minute = minute;
                        _setMinute(minute);
                        scope.displayedMinute = displayMinute(scope.minute);
                    }
                    scope.displayedMinute = displayMinute(scope.minute);
                }else{
                    _setValidity();
                }
            }

            function toggleAMPM() {
                scope.ampm = scope.ampm === 'AM' ? 'PM' : 'AM';
                _setHour(calcHour());
            }

            function calcHour(){
                if(scope.ampm === 'PM'){
                    return scope.hour < 12 ? scope.hour + 12 : scope.hour;
                }else{
                    return scope.hour % 12;
                }
            }

            function incrementHour(delta){
                var hour = (parseInt(scope.hour) || 0) + delta;
                hour = (hour > 12) ? hour - 12 : ((hour <= 0) ? hour + 12 : hour);
                scope.hour = hour;
                scope.displayedHour = displayHour(scope.hour);
                _setHour(calcHour());
            }

            function incrementMinute(delta){
                var minute = (parseInt(scope.minute) || 0) + delta;
                minute = (minute >= 60) ? minute - 60 : ((minute < 0) ? minute + 60 : minute);
                scope.minute = minute;
                scope.displayedMinute = displayMinute(scope.minute);
                _setMinute(minute);
            }

            function getMouseWheelEventCB(fn){
                return function(e) {
                    fn(isScrollingUp(e) ? 1 : -1);
                    scope.$apply();
                    e.preventDefault();
                };
            }

            function getKeyboardEventCB(fn){
                return function(event){
                    //38 = up, 40 = down
                    if(event.keyCode === 38 || event.keyCode === 40){
                        fn((event.keyCode === 38) ? 1 : -1);
                        scope.$apply();
                        event.preventDefault();
                    }
                };
            }

            function isScrollingUp(e) {
                e = e.originalEvent || e;
                //pick correct delta variable depending on event
                var delta = (e.wheelDelta) ? e.wheelDelta : -e.deltaY;
                return (e.detail || delta > 0);
            }


            function _setHour(hour){
                if(angular.isDate(scope.ngModel)){
                    scope.ngModel.setHours(hour);
                }else{
                    scope.ngModel.hour(hour);
                }
                _setValidity();
            }

            function _setMinute(minute){
                if(angular.isDate(scope.ngModel)){
                    scope.ngModel.setMinutes(minute);
                }else{
                    scope.ngModel.minute(minute);
                }
                _setValidity();
            }

            function _setValidity(){
                if(angular.isDefined(scope.form)){
                    ngModel.$setDirty();
                    if(scope.required){
                        ngModel.$setValidity('required', scope.displayedHour !== '' && scope.displayedMinute !== '');
                    }
                    if(angular.isDefined(scope.requireLater)){
                        var laterOffsetDuration = nvInputTimeService.parse(scope.requireLaterOffset);
                        var laterCompareTime = moment(angular.copy(scope.requireLater));
                        var laterModelTime = angular.copy(laterCompareTime);
                        laterCompareTime.add(laterOffsetDuration.hours(), 'h').add(laterOffsetDuration.minutes(), 'm').second(0).millisecond(0);
                        laterModelTime.hour(calcHour()).minute(scope.minute).second(0).millisecond(0);
                        ngModel.$setValidity('later', laterModelTime.isSameOrAfter(laterCompareTime));
                    }
                    if(angular.isDefined(scope.requireBefore)){
                        var beforeOffsetDuration = nvInputTimeService.parse(scope.requireBeforeOffset);
                        var beforeCompareTime = moment(angular.copy(scope.requireBefore));
                        var beforeModelTime = angular.copy(beforeCompareTime);
                        beforeCompareTime.add(beforeOffsetDuration.hours(), 'h').add(beforeOffsetDuration.minutes(), 'm').second(0).millisecond(0);
                        beforeModelTime.hour(calcHour()).minute(scope.minute).second(0).millisecond(0);
                        ngModel.$setValidity('before', beforeModelTime.isSameOrBefore(beforeCompareTime));
                    }
                }
            }
            function displayAfterBeforeTime(time, amount){
                var duration = nvInputTimeService.parse(amount);
                var compareTime = moment(angular.copy(time));
                return compareTime.add(duration).format('hh:mm A');
            }
        }

    }

})();
