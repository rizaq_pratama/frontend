;(function() {
    'use strict';

    angular
        .module('nvCommons.directives')
        .directive('nvInputTextarea', inputTextarea);

    inputTextarea.$inject = ['$filter'];

    function inputTextarea($filter) {

        return {
            restrict: 'E',
            templateUrl: 'lib/ninja-commons/directives/forms/inputs/input-textarea/input-textarea.directive.html',
            replace: true,
            scope: {
                label: '@',
                model: '=',
                form: '=?',
                disabled: '=?',
                required: '=?',
                helpText: '@?',
                readOnly: '=?',
                onChange: '&?',
                name: '@?',
            },
            link: link
        };

        function link(scope) {
            scope.id = scope.name || $filter('nvNaturalCase2HyphenCase')(scope.label);
            scope.disabled = angular.isDefined(scope.disabled) ? scope.disabled : false;
            scope.required = angular.isDefined(scope.required) ? scope.required : false;
            scope.readOnly = angular.isDefined(scope.readOnly) ? scope.readOnly : false;
            scope.onChange = scope.onChange || angular.noop();

        }

    }

})();
