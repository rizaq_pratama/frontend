(function directive() {
  angular
    .module('nvCommons.directives')
    .directive('nvInputDecimal', inputDecimal);

    /**
     *
     * @param {string}      name         the name of the input
     * @param {string}      label        the label of the input
     * @param {model}       model        the model for the input
     * @param {form}        form         the form object
     * @param {extra}       extra        configuration for decimal input
     *                                   {decimal: 2, minValue: 0.00, maxValue: 999.99}
     *
     */

  inputDecimal.$inject = ['$filter'];

  const STATE = {
    RESET: -1,
    IDLE: 0,
    STABLE: 1,
    CORRECTING: 2,
    SCALE10: 3,
  };

  function inputDecimal($filter) {
    let idx = 0;

    return {
      restrict: 'E',
      templateUrl: 'lib/ninja-commons/directives/forms/inputs/input-decimal/input-decimal.directive.html',
      replace: true,
      scope: {
        name: '@',
        label: '@',
        model: '=',
        form: '=?',
        extra: '=?',
      },
      link: link,
    };

    function link(scope, element) {
      scope.id = scope.name || (`${$filter('nvNaturalCase2HyphenCase')(scope.label)}-${idx += 1}`);
      scope.extra = _.defaults(scope.extra, { decimal: 2, minValue: 0.00, maxValue: 999.99 });
      if (scope.extra.decimal < 0) {
        scope.extra.decimal = 0;
      }
      scope.model = scope.model || scope.extra.minValue;
      // +1 to for decimal separator character
      scope.maxLength = calculateMaxLength();
      scope.minLength = 0;
      scope.onClick = onClick;
      scope.onBlur = onBlur;

      const decimal = scope.extra.decimal;
      const divider = Math.pow(10, decimal);

      scope.dummyModel = scope.model.toFixed(decimal);

      scope.state = STATE.STABLE;

      element.on('keydown', onlyNumbers);

      scope.$watch('dummyModel', processInput);
      scope.$watch('model', invalidate);

      function processInput(val) {
        // *val is a string*
        let current = val;
        let dec = scope.model;

        // undifined when textbox is filled more than its maxLength
        if (current === undefined || dec > scope.extra.maxValue) {
          dec = scope.extra.maxValue;
          current = dec.toFixed(decimal);
          scope.state = STATE.STABLE;
        }

        if (scope.state === STATE.RESET) {
          if (decimal > 0) {
            dec = parseFloat(current) / divider;
            scope.state = STATE.CORRECTING;
          } else {
            // for integer val no further calculation necessary
            dec = parseFloat(current);
            scope.state = STATE.IDLE;
          }
        } else if (scope.state === STATE.IDLE) {
          if (decimal > 0) {
            dec = parseFloat(current) * 10;
            scope.state = STATE.CORRECTING;
          } else {
            // for integer val no further calculation necessary
            dec = parseFloat(current);
            scope.state = STATE.IDLE;
          }
        } else if (scope.state === STATE.CORRECTING) {
          scope.state = STATE.IDLE;
        }

        scope.model = Math.round(dec * divider) / divider;
        scope.dummyModel = dec.toFixed(decimal);
      }

      function onlyNumbers($event) {
        const keys = _(10)
          .times(String)
          .concat('\t')
          .map(c => c.charCodeAt(0));

        if (!_.find(keys, key => (key === $event.keyCode))) {
          $event.preventDefault();
        }
      }

      function onClick($event) {
        $event.target.select();
        scope.state = STATE.RESET;
      }

      function onBlur(val) {
        let current = val;
        let dec = scope.model;

        // null when textbox is empty
        if (current === null || dec < scope.extra.minValue) {
          dec = scope.extra.minValue;
          current = dec.toFixed(decimal);

          scope.model = Math.round(dec * divider) / divider;
          scope.dummyModel = dec.toFixed(decimal);
        }
        scope.state = STATE.STABLE;
      }

      function invalidate() {
        if (scope.state === STATE.STABLE) {
          scope.dummyModel = scope.model.toFixed(decimal);
        }
      }

      function calculateMaxLength() {
        const decimalLength = scope.extra.decimal;
        const maxValue = scope.extra.maxValue;

        const logResult = Math.log10(maxValue);
        const ceilingResult = Math.ceil(logResult);

        let length = ceilingResult;
        if (logResult === ceilingResult) {
          // for case max = 1000 : log(1000) = 3, but we need one extra digit
          // for text field max length validation
          length += 1;
        }

        // +1 for '.'
        if (decimalLength > 0) {
          return decimalLength + length + 1;
        }
        return length;
      }
    }
  }
}());
