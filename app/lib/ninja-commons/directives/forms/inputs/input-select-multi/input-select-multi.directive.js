(function InputSelectMultiDirective() {
    /**
     * a variant of nv-input-select that using angular material multiple select design
     */
  angular
        .module('nvCommons.directives')
        .directive('nvInputSelectMulti', inputSelectMulti);

  inputSelectMulti.$inject = ['$filter'];

  function inputSelectMulti($filter) {
    let idx = 0;

    return {
      restrict: 'E',
      templateUrl: 'lib/ninja-commons/directives/forms/inputs/input-select-multi/input-select-multi.directive.html',
      replace: true,
      scope: {
        label: '@',
        model: '=',
        modelOptions: '=?',
        onChange: '&?',
        options: '=',
        form: '=?',
        multiple: '=?',
        disabled: '=?',
        required: '=?',
        helpText: '@?',
        setDefault: '@?',
        mdContainerClass: '@?',
      },
      link: link,
    };

    function link(scope) {
      idx += 1;
      scope.id = `${$filter('nvNaturalCase2HyphenCase')(scope.label)}-${idx}`;

      // false by default
      if (angular.isUndefined(scope.multiple)) {
        scope.multiple = false;
      }

      // enabled by default
      if (angular.isUndefined(scope.disabled)) {
        scope.disabled = false;
      }

      // not required by default
      if (angular.isUndefined(scope.required)) {
        scope.required = false;
      }

      // set default value to true by default
      if (angular.isUndefined(scope.setDefault)) {
        scope.setDefault = true;
      }

    }
  }
}());
