(function directive() {
  angular
    .module('nvCommons.directives')
    .directive('nvInputEmails', inputEmail);

    const EMAIL_REGEXP = /^(?=.{1,254}$)(?=.{1,64}@)[-!#$%&'*+\/0-9=?A-Z^_`a-z{|}~]+(\.[-!#$%&'*+\/0-9=?A-Z^_`a-z{|}~]+)*@[A-Za-z0-9]([A-Za-z0-9-]{0,61}[A-Za-z0-9])?(\.[A-Za-z0-9]([A-Za-z0-9-]{0,61}[A-Za-z0-9])?)*$/;

    /**
     *
     * @param {string}      label        the label of the input
     * @param {model}       model        the model for the input
     * @param {form}        form         the form object
     * @param {boolean=}    disabled     if true, disable the input, default false
     * @param {boolean=}    required     if true, make input required, default false
     * @param {string}      helpText     additional help message text under the input
     * @param {string}      confirmText  Active confirmation text
     * @param {expression}  confirm      Expression evaluates to the condition whether to show the confirmation 
     *                                   text below the input and a tick beside the input, default to false
     *
     */

  inputEmail.$inject = ['$filter', '$mdConstant'];

  function inputEmail($filter, $mdConstant) {
    return {
      restrict: 'E',
      templateUrl: 'lib/ninja-commons/directives/forms/inputs/input-emails/input-emails.directive.html',
      replace: true,
      require: '?ngModel',
      scope: {
        label: '@',
        model: '=ngModel',
        form: '=?',
        disabled: '=?',
        required: '=?',
        helpText: '@?',
        confirmText: '@?',
        confirm: '&?',
      },
      link: link,
    };

    function link(scope, elem, attr, ngModel) {
      scope.disabled = scope.disabled || false;
      scope.required = scope.required || false;
      scope.confirm = scope.confirm || (() => false);
      scope.model = scope.model || [];
      scope.keys = [$mdConstant.KEY_CODE.ENTER, $mdConstant.KEY_CODE.COMMA];
      scope.checkIsEmail = checkIsEmail;

      //ng model
      ngModel.$name = attr.name || $filter('nvNaturalCase2HyphenCase')(scope.label);
      if(angular.isDefined(scope.form)){
        scope.form.$addControl(ngModel);
      }
      scope.$on('$destroy', function() {
        if(angular.isDefined(scope.form)){
          scope.form.$removeControl(ngModel);
        }
      });
      scope.$watch('model.length', (value) => {
        ngModel.$setDirty();
        if (scope.required) {
          ngModel.$setValidity('required', value > 0);
        }
      });
      function checkIsEmail(value) {
        return (EMAIL_REGEXP.test(value)) ? value : null;
      }

    }
  }
}());
