;(function() {
    'use strict';

    angular
        .module('nvCommons.directives')
        .directive('nvInputTel', inputTel);

    /**
     * 
     * @param {string}      label        the label of the input
     * @param {model}       model        the model for the input
     * @param {form}        form         the form object
     * @param {boolean=}    disabled     if true, disable the input, default false
     * @param {boolean=}    required     if true, make input required, default false
     * @param {string}      helpText     additional help message text under the input 
     * @param {integer}     readOnly     if true, the input is not modifiable, default false
     * @param {string}      confirmText  Active confirmation text
     * @param {expression}  confirm      Expression evaluates to the condition whether to show the confirmation text below the input and a tick beside the input, default to false
     * 
     */

    inputTel.$inject = ['$filter'];

    function inputTel($filter) {

        return {
            restrict: 'E',
            templateUrl: 'lib/ninja-commons/directives/forms/inputs/input-tel/input-tel.directive.html',
            replace: true,
            scope: {
                label: '@',
                model: '=',
                form: '=?',
                disabled: '=?',
                required: '=?',
                helpText: '@?',
                readonly: '=?',
                confirmText: '@?',
                confirm: '&?',
            },
            link: link
        };

        function link(scope) {
            scope.id = $filter('nvNaturalCase2HyphenCase')(scope.label);
            scope.disabled = angular.isDefined(scope.disabled) ? scope.disabled : false;
            scope.required = angular.isDefined(scope.required) ? scope.required : false;
            scope.readonly = angular.isDefined(scope.readonly) ? scope.readonly : false;
            scope.confirm = angular.isDefined(scope.confirm) ? scope.confirm : function(){ return false; };
        }

    }

})();
