(function directive() {
  angular
        .module('nvCommons.directives')
        .directive('nvInputNumber', inputNumber);

  /**
   *
   * @param {string}      name         the name of the input
   * @param {string}      label        the label of the input
   * @param {model}       model        the model for the input
   * @param {integer}     step         step value for the input
   * @param {form}        form         the form object
   * @param {string}      currency     currency to be show in front of the text input
   * @param {boolean=}    disabled     if true, disable the input, default false
   * @param {boolean=}    required     if true, make input required, default false
   * @param {string}      helpText     additional help message text under the input
   * @param {integer}     readOnly     if true, the input is not modifiable, default false
   * @param {integer}     min          minimum allowable value for the input
   * @param {integer}     max          maximum allowable value for the input
   * @param {string}      confirmText  Active confirmation text
   * @param {expression}  confirm      Expression evaluates to the condition whether to
   *                                   show the confirmation text below the input and a
   *                                   tick beside the input, default to false
   * @param {array}      errorMsgs     array object to hold error message to
   *                                   show using ng-message,
   *                                   the object will have following structure
   *                                   [{key: 'maxlength',msg: 'commons.too-long' }]
   *
   */

  inputNumber.$inject = ['$filter'];

  function inputNumber($filter) {
    let idx = 0;

    return {
      restrict: 'E',
      templateUrl: 'lib/ninja-commons/directives/forms/inputs/input-number/input-number.directive.html',
      replace: true,
      scope: {
        name: '@',
        label: '@',
        model: '=',
        step: '&?',
        form: '=?',
        currency: '@?',
        disabled: '=?',
        required: '=?',
        helpText: '@?',
        readonly: '=?',
        min: '@?',
        max: '@?',
        confirmText: '@?',
        confirm: '&?',
        onChange: '&?',
        errorMsgs: '=?',
        onKeypress: '&?',
      },
      link: link,
    };

    function link(scope) {
      scope.id = scope.name || `${$filter('nvNaturalCase2HyphenCase')(scope.label)}-${(idx += 1)}`;
      scope.disabled = angular.isDefined(scope.disabled) ? scope.disabled : false;
      scope.required = angular.isDefined(scope.required) ? scope.required : false;
      scope.readonly = angular.isDefined(scope.readonly) ? scope.readonly : false;
      scope.confirm = angular.isDefined(scope.confirm) ? scope.confirm : function confirmFn() {
        return false;
      };
      scope.onChange = scope.onChange || angular.noop();
      scope.errorMsgs = scope.errorMsgs || [];
      scope.onKeypress = scope.onKeypress || angular.noop();
    }
  }
}());
