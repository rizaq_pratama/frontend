;(function() {
    'use strict';

    angular
        .module('nvCommons.directives')
        .directive('nvInputAddress', inputAddress);

    inputAddress.$inject = ['$filter'];

    function inputAddress($filter) {

        return {
            restrict: 'E',
            templateUrl: 'lib/ninja-commons/directives/forms/inputs/input-address/input-address.directive.html',
            replace: true,
            scope: {
                label: '@',
                model: '=',
                form: '=?',
                disabled: '=?',
                required: '=?',
                helpText: '@?',
                openDialogCallback: '&?'
            },
            link: link
        };

        function link(scope) {
            scope.id = $filter('nvNaturalCase2HyphenCase')(scope.label);
            scope.disabled = angular.isDefined(scope.disabled) ? scope.disabled : false;
            scope.required = angular.isDefined(scope.required) ? scope.required : false;
            scope.openDialogCallback = angular.isDefined(scope.openDialogCallback) ? scope.openDialogCallback : openDialogCallback;

            function openDialogCallback() {
                console.log('default callback');
            }
        }

    }

})();
