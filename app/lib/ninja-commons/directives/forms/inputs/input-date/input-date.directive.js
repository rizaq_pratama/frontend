(function directive() {
  angular
        .module('nvCommons.directives')
        .directive('nvInputDate', inputDate);

  inputDate.$inject = ['$filter'];

  function inputDate($filter) {
    return {
      restrict: 'E',
      templateUrl: 'lib/ninja-commons/directives/forms/inputs/input-date/input-date.directive.html',
      replace: true,
      scope: {
        label: '@',
        model: '=',
        form: '=?',
        disabled: '=?',
        required: '=?',
        minDate: '=?',
        maxDate: '=?',
        showAlert: '=?',
      },
      link: link,
    };

    function link(scope) {
      scope.id = $filter('nvNaturalCase2HyphenCase')(scope.label);
      scope.disabled = angular.isDefined(scope.disabled) ? scope.disabled : false;
      scope.required = scope.required || false;
      scope.showAlert = scope.showAlert || false;
    }
  }
}());
