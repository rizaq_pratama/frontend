(function directive() {
  angular
        .module('nvCommons.directives')
        .directive('nvInputText', inputText);

    /**
     *
     * @param {string}      name         the name of the input
     * @param {string}      label        the label of the input
     * @param {string}      placeholder  the placeholder of the input
     * @param {model}       model        the model for the input
     * @param {form}        form         the form object
     * @param {boolean=}    disabled     if true, disable the input, default false
     * @param {boolean=}    required     if true, make input required, default false
     * @param {string}      helpText     additional help message text under the input
     * @param {integer}     readOnly     if true, the input is not modifiable, default false
     * @param {integer}     maxLength    maximum length of text allowable for the input
     * @param {integer}     minLength    minimum length of text allowable for the input
     * @param {string}      confirmText  Active confirmation text
     * @param {function}    onKeypress   Function to execute upon keypress, $event and model are available for the called function 
     * @param {expression}  confirm      Expression evaluates to the condition whether to show
     *                                   the confirmation text below the input and a tick beside
     *                                   the input, default to false
     * @param {array}      errorMsgs     array object to hold error message to
     *                                   show using ng-message,
     *                                   the object will have following structure
     *                                   [{key: 'maxlength',msg: 'commons.too-long' }]
     */

  inputText.$inject = ['$filter'];

  function inputText($filter) {
    let idx = 0;
    inputTextController.$inject = ['$scope'];

    return {
      restrict: 'E',
      templateUrl: 'lib/ninja-commons/directives/forms/inputs/input-text/input-text.directive.html',
      replace: true,
      scope: {
        name: '@',
        label: '@',
        placeholder: '@?',
        model: '=',
        form: '=?',
        disabled: '=?',
        required: '=?',
        helpText: '@?',
        readonly: '=?',
        maxlength: '=?',
        minlength: '=?',
        confirmText: '@?',
        confirm: '&?',
        onChange: '&?',
        modelOptions: '=?',
        errorMsgs: '=?',
        onKeypress: '&?',
      },
      link: link,
      controller: inputTextController,
    };

    function link(scope, element) {
      scope.id = scope.name || (`${$filter('nvNaturalCase2HyphenCase')(scope.label)}-${idx += 1}`);
      scope.disabled = scope.disabled || false;
      scope.required = scope.required || false;
      scope.readonly = scope.readonly || false;
      scope.maxlength = scope.maxlength || -1;
      scope.minlength = scope.minlength || 0;
      scope.confirm = scope.confirm || (() => false);
      scope.onChange = scope.onChange || angular.noop();
      scope.errorMsgs = scope.errorMsgs || [];
      scope.placeholder = scope.placeholder || '';
      scope.onKeypress = scope.onKeypress || angular.noop();

      // to add/remove disabled attributes in input element
      scope.$watch('disabled', (val) => {
        if (val != null) {
          const inputEl = getInputEl(element.children());
          if (inputEl && (val === true)) {
            inputEl.setAttribute('disabled', 'true');
          }
          if (inputEl && (val === false)) {
            inputEl.removeAttribute('disabled');
          }
        }
      });
    }

    function inputTextController(scope) {
      scope.modelOptions = scope.modelOptions || {};
    }

    function getInputEl(children) {
      let inputEl = null;
      for (let i = 0; i < children.length; i++) {
        const el = children[i];
        if (el.nodeName.toLowerCase() === 'input') {
          inputEl = el;
        }
      }

      return inputEl;
    }
  }
}());
