(function directive() {
  angular
    .module('nvCommons.directives')
    .directive('nvInputPassword', inputPassword);

  /**
   *
   * @param {string}      label        the label of the input
   * @param {model}       model        the model for the input
   * @param {form}        form         the form object
   * @param {boolean=}    disabled     if true, disable the input, default false
   * @param {boolean=}    required     if true, make input required, default false
   * @param {string}      helpText     additional help message text under the input 
   *
   */
  inputPassword.$inject = ['$filter'];

  function inputPassword($filter) {
    return {
      restrict: 'E',
      templateUrl: 'lib/ninja-commons/directives/forms/inputs/input-password/input-password.directive.html',
      replace: true,
      scope: {
        label: '@',
        model: '=',
        form: '=?',
        disabled: '=?',
        required: '=?',
        helpText: '@?',
        nvMinlength: '@?',
      },
      link: link,
    };

    function link(scope) {
      scope.showPassword = false;
      scope.id = $filter('nvNaturalCase2HyphenCase')(scope.label);
      scope.disabled = angular.isDefined(scope.disabled) ? scope.disabled : false;
      scope.required = angular.isDefined(scope.required) ? scope.required : false;
      scope.nvMinlength = angular.isDefined(scope.nvMinlength) ? scope.nvMinlength : '0';
    }
  }
}());
