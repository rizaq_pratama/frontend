;(function() {
    'use strict';

    angular
        .module('nvCommons.directives')
        .directive('nvInputCheckbox', inputCheckbox);

    function inputCheckbox() {

        return {
            restrict: 'E',
            templateUrl: 'lib/ninja-commons/directives/forms/inputs/input-checkbox/input-checkbox.directive.html',
            replace: true,
            scope: {
                label: '@',
                model: '=',
                form: '=?',
                disabled: '=?',
                onChange: '&?'
            },
            link: link
        };

        function link(scope, iElement) {
            scope.ariaLabel = scope.label || '-';
            scope.disabled = angular.isDefined(scope.disabled) ? scope.disabled : false;
            scope.onChange = angular.isDefined(scope.onChange) ? scope.onChange : null;

            _(iElement[0].classList)
                .intersection(['md-hue-1', 'md-hue-2', 'md-hue-3'])
                .forEach(moveClassToChild);

            function moveClassToChild(clazz) {
                iElement[0].classList.remove(clazz);
                iElement[0].children[0].classList.add(clazz);
            }
        }

    }

})();
