;(function() {
    'use strict';

    angular
        .module('nvCommons.directives')
        .directive('nvInputSwitch', inputSwitch);

    inputSwitch.$inject = ['$filter'];

    function inputSwitch($filter) {

        var idx = 0;

        return {
            restrict: 'E',
            templateUrl: 'lib/ninja-commons/directives/forms/inputs/input-switch/input-switch.directive.html',
            replace: true,
            scope: {
                label: '@',
                model: '=',
                onChange: '&',
                form: '=?',
                disabled: '=?',
                switchEnabledText: '@',
                switchDisabledText: '@'
            },
            link: link
        };

        function link(scope) {
            scope.id = $filter('nvNaturalCase2HyphenCase')(scope.label) + '-' + idx++;
            scope.disabled = angular.isDefined(scope.disabled) ? scope.disabled : false;
            scope.model = angular.isDefined(scope.model) ? scope.model : false;

            scope.text = function() {
                return scope.model ? scope.switchEnabledText : scope.switchDisabledText;
            };
        }

    }

})();
