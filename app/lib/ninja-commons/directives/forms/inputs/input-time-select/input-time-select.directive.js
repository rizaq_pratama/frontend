(function directive() {
  angular
        .module('nvCommons.directives')
        .directive('nvInputTimeSelect', inputTimeSelect);

    /**
     *
     * @param {string}      name         the name of the input
     * @param {model}       model        the model for the input
     * @param {form}        form         the form object
     * @param {boolean=}    disabled     if true, disable the input, default false
     */


  function inputTimeSelect() {
    timeSelectController.$inject = ['$scope', 'nvTimezone'];

    return {
      restrict: 'E',
      templateUrl: 'lib/ninja-commons/directives/forms/inputs/input-time-select/input-time-select.directive.html',
      replace: true,
      scope: {
        name: '@',
        model: '=',
        form: '=?',
        disabled: '=?',
        onChange: '&?',
        minuteValues: '=?',
        hourValues: '=?',
        minimumValue: '=?',
        maximumValue: '=?',
        label: '@?',
      },
      link: link,
      controller: timeSelectController,
    };

    function link(scope) {
      scope.minuteValues = [
        {
          displayName: '00',
          value: 0,
        },
        {
          displayName: '15',
          value: 15,
        },
        {
          displayName: '30',
          value: 30,
        },
        {
          displayName: '45',
          value: 45,
        },
      ];
      scope.hourValues = _.times(24,(value)=> (
        {
          displayName : _.padStart(value,2,'0'),
          value: value,
        }
      ));

      if (angular.isUndefined(scope.model)) {
        scope.model = moment();
      }
      scope.hour = scope.model ? scope.model.hour() : null;
      scope.minute = scope.model ? scope.model.minute() : null;
      scope.id = scope.name;
      scope.disabled = scope.disabled || false;
      scope.onChange = angular.isFunction(scope.onChange) ? scope.onChange : angular.noop();
      scope.label = scope.label || null;
    }

    function timeSelectController(scope, nvTimezone) {
      scope.modelOptions = scope.modelOptions || {};
      scope.$watch('hour', (val) => {
        if ((val || val === 0) && !scope.model) {
          scope.model = moment().tz(nvTimezone.getOperatorTimezone()).second(0).millisecond(0);
        }
        if (scope.model) {
          scope.model.hour(val);
        }
        if (angular.isFunction(scope.onChange)) {
          scope.onChange();
        }
      });

      scope.$watch('minute', (val) => {
        if ((val || val === 0) && !scope.model) {
          scope.model = moment().tz(nvTimezone.getOperatorTimezone()).second(0).millisecond(0);
        }
        if (scope.model) {
          scope.model.minute(val);
        }
        if (angular.isFunction(scope.onChange)) {
          scope.onChange();
        }
      });
    }
  }
}());
