(function customFieldGenDirective() {
  angular
    .module('nvCommons.directives')
    .directive('nvCustomFieldGen', customFieldGen);

  customFieldGen.$inject = ['$filter', '$compile'];

  function customFieldGen($filter, $compile) {
    // the different html input type templates
    const text = '<nv-input-text class="md-block" name="$name" label="$displayName" model="field.value" form="$form"' +
      ' disabled="$disabled" required="$required" name="$name" help-text="$helpText"></nv-input-text>';
    const tel = '<nv-input-tel class="md-block" name="$name" label="$displayName" model="field.value" form="$form" disabled="$disabled" required="$required" help-text="$helpText"></nv-input-tel>';
    const email = '<nv-input-email class="md-block" name="$name" label="$displayName" model="field.value" form="$form" disabled="$disabled" required="$required" help-text="$helpText"></nv-input-email>';
    const number = '<nv-input-number class="md-block" name="$name" label="$displayName" model="field.value" form="$form" disabled="$disabled" required="$required" help-text="$helpText"></nv-input-number>';
    const checkbox = '<nv-input-checkbox class="md-block" name="$name" label="$displayName" model="field.value" form="$form" disabled="$disabled"></nv-input-checkbox>';
    const password = '<nv-input-password class="md-block" name="$name" label="$displayName" model="field.value" form="$form" disabled="$disabled" required="$required" help-text="$helpText"></nv-input-password>';
    const select = '<nv-input-select set-default="::false" class="md-block" name="$name" label="$displayName" model="field.value" model-options="{}" options="field.options" form="$form" disabled="$disabled" required="$required" help-text="$helpText"></nv-input-select>';
    const address = '<nv-input-address class="md-block" name="$name" label="$displayName" model="field.value" form="$form" disabled="$disabled" required="$required" help-text="$helpText" open-dialog-callback="$openDialogCallback"></nv-input-address>';
    const date = '<label for="$idx">$displayName</label>' +
      '<md-datepicker ng-model="field.value"  ng-disabled="$disabled" md-placeholder="Enter date"></md-datepicker>';

    return {
      restrict: 'E',
      templateUrl: 'lib/ninja-commons/directives/forms/inputs/custom-field-gen/custom-field-gen.directive.html',
      replace: true,
      scope: {
        field: '=',
        form: '@?',
      },
      link: link,

    };

    function link(scope, ele) {
      scope.form = scope.form || 'modelForm'; 
      scope.field.name_local = $filter('nvNaturalCase2CamelCase')(scope.field.name);
      scope.field.id_local = $filter('nvNaturalCase2HyphenCase')(scope.field.name);
      scope.field.disabled = scope.field.disabled || false;
      scope.field.readonly = scope.field.readonly || false;
      scope.field.required = false;
      scope.field.openDialogCallback = openDialogCallback;

      function openDialogCallback() {
//
      }

      function generateField(scope, elem) {
        let templ = '';
        let field = scope.field;
        // get all the input html elements to inject into the dialog
        if (field.hidden) {
          return;
        }
        switch (field.type) {
          case 'text':
            templ = getInputHtml(text, scope);
            break;
          case 'tel':
            templ = getInputHtml(tel, scope);
            break;
          case 'email':
            templ = getInputHtml(email, scope);
            break;
          case 'number':
            field.value = Number(field.value);
            templ = getInputHtml(number, scope);
            break;
          case 'date': {
            // date checking
            const timestamp = Date.parse(field.value);
            if (!isNaN(timestamp)) {
              field.value = new Date(timestamp);
            }
            templ = getInputHtml(date, scope);
            break;
          }
          case 'checkbox':
            templ = getInputHtml(checkbox, scope);
            break;
          case 'password':
            templ = getInputHtml(password, scope);
            break;
          case 'select':
            // checking if option value is null or undefined or an empty array
            if (field.options === undefined || field.options === null
                || field.options.length === 0) {
              field.options = [{}];
            }
            // check for options value
            field.options = _.map(field.options, (option) => {
              if (!option.displayName) {
                option.displayName = option.display_name;                
              }
              return option;
            });

            // for select type, if disabled is true, and field.value is nulll,
            if (field.disabled && !field.value) {
              field.value = '-';
            }

            templ = getInputHtml(select, scope);
            break;
          case 'address':
            templ = getInputHtml(address, scope);
            break;
          default:
            break;
        }

        // compile the input html elements with the current scope
        // and insert them into the correct position in the dialog
        const element = $compile(templ)(scope);
        elem.append(element);
      }

      function getInputHtml(template, scope) {
        return template
          .replace('$displayName', _.startCase(_.lowerCase(scope.field.name)))
          .replace(/\$idx/g, scope.field.name_local)
          .replace('$form', scope.form)
          .replace('$name', scope.field.name_local)
          .replace('$disabled', (!!scope.field.disabled).toString())
          .replace('$readonly', (!!scope.field.readonly).toString())
          .replace('$required', (!!scope.field.required).toString())
          .replace('$openDialogCallback', scope.field.openDialogCallback)
          .replace('$helpText', '');
         
      }

      generateField(scope, ele);
    }
  }
}());
