(function directive() {
  angular
        .module('nvCommons.directives')
        .directive('nvInputSelect', inputSelect);

  inputSelect.$inject = ['$filter', '$timeout'];

  function inputSelect($filter, $timeout) {
    let idx = 0;

    return {
      restrict: 'E',
      templateUrl: 'lib/ninja-commons/directives/forms/inputs/input-select/input-select.directive.html',
      replace: true,
      scope: {
        name: '@?',
        label: '@',
        model: '=',
        modelOptions: '=?',
        onClose: '&?',
        options: '=',
        form: '=?',
        searchable: '=?',
        disabled: '=?',
        required: '=?',
        helpText: '@?',
        setDefault: '@?',
        mdContainerClass: '@?',
        onChange: '&?',
        placeholder: '@?',
        showLabel: '=?',
      },
      compile: compile,
    };

    function compile(elem, attr) {
      if (attr.multiple && attr.multiple === 'true') {
        elem.find('md-select').attr('multiple', 'true');
      }

      return link;
    }

    function link(scope, element) {
      let input = null;

      scope.id = scope.name || `${$filter('nvNaturalCase2HyphenCase')(scope.label)}-${idx += 1}`;
      scope.searchTerm = '';

      if (angular.isUndefined(scope.searchable)) {
        scope.searchable = false;
      }

      if (scope.searchable) {
        $timeout(() => {
          input = element.find('input');
          input.on('keydown', (ev) => {
            ev.stopPropagation();
          });
        });
      }

      // enabled by default
      if (angular.isUndefined(scope.disabled)) {
        scope.disabled = false;
      }

      // not required by default
      if (angular.isUndefined(scope.required)) {
        scope.required = false;
      }

      // set default value to true by default
      if (angular.isUndefined(scope.setDefault)) {
        scope.setDefault = true;
      }

      // select the first option by default
      if (angular.isUndefined(scope.model) && scope.setDefault === true) {
        scope.model = scope.options[0].value;
      }

      if (angular.isUndefined(scope.showLabel)) {
        scope.showLabel = true;
      }

      scope.onChange = scope.onChange || angular.noop();
      scope.placeholder = scope.placeholder || scope.label;
      scope.onSelectionOpen = onSelectionOpen;
      scope.onSelectionClose = onSelectionClose;

      function onSelectionOpen() {
        if (input) {
          $timeout(() => {
            input.focus();
          }, 500);
        }
      }

      function onSelectionClose() {
        scope.searchTerm = '';

        if (!_.isUndefined(scope.onClose)) {
          scope.onClose();
        }
      }
    }
  }
}());
