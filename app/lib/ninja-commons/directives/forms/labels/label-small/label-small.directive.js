(function() {
    'use strict';

    angular
        .module('nvCommons.directives')
        .directive('nvLabelSmall', labelSmall);

    function labelSmall() {

        return {
            restrict: 'E',
            templateUrl: 'lib/ninja-commons/directives/forms/labels/label-small/label-small.directive.html',
            replace: true,
            scope: {
                label: '@'
            }
        };

    }

})();
