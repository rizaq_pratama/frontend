(function directive() {
  angular
    .module('nvCommons.directives')
    .directive('nvIconButton', iconButton);

  /**
   * nvIconButton - Icon Button
   *
   * @param {string|resId} name             For aria-label and tooltip, can be string or tranlation resId
   * @param {string}       icon             Material Icon font name: @see https://design.google.com/icons/
   * @param {boolean}      disabled         Optional. To disable the button
   * @param {function}     onClick          Callback for onClick
   * @param {string}       ngChildClass     Works like ng-class, Useful when passing in class with condition
   * @param {number}       tooltipDelay     delay for tooltip, in milliseconds
   * @param {string}       tooltipDirection direction of the tooltip
   *
   */

  iconButton.$inject = ['nvDOMManipulation', 'nvGa'];

  function iconButton(nvDOMManipulation, nvGa) {
    return {
      restrict: 'E',
      templateUrl: 'lib/ninja-commons/directives/buttons/icon-button/icon-button.directive.html',
      scope: {
        name: '@',
        icon: '@',
        iconSet: '@',
        disabled: '=?',
        onClick: '&',
        ngChildClass: '=?', // useful when passing in class with condition
        tooltipDelay: '@?',
        tooltipDirection: '@?',
      },
      compile: compile,
    };

    function compile(elem, attr) {
      if (attr.mdThemeWatch) {
        elem.find('md-button').attr('md-theme-watch', attr.mdThemeWatch);
      }
      if (attr.tooltip === 'false') {
        elem.find('md-tooltip').remove();
      }
      return link;
    }

    function link(scope, element) {
      nvDOMManipulation.moveClassesToFirstChild(element[0]);
      if (angular.isUndefined(scope.tooltipDelay)) {
        scope.tooltipDelay = 400;
      }

      scope.onClickWrapper = ($event) => {
        scope.onClick($event);
        nvGa.event('button', 'click', scope.name);
      };
    }
  }
}());

