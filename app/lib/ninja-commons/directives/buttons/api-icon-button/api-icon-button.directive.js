(function directive() {
  angular
    .module('nvCommons.directives')
    .directive('nvApiIconButton', apiIconButton);

  /**
   * nvApiIconButton - Icon Button with Loading Spinner
   *
   * @param {string|resId} name             For aria-label and tooltip, can be string or tranlation resId
   * @param {string}       icon             Material Icon font name: @see https://design.google.com/icons/
   * @param {string}       state            Required. Either 'idle', 'waiting' or 'failed'. Will show md-progress when state is in 'waiting'
   * @param {boolean}      disabled         Optional. To disable the button
   * @param {function}     onClick          Callback for onClick
   * @param {form}         form             Optional. Form Controller. Button will be disabled when form is invalid.
   * @param {number}       tooltipDelay     delay for tooltip, in milliseconds
   * @param {string}       tooltipDirection direction of the tooltip
   * @param {string}       buttonId         id of the button
   * 
   */

  apiIconButton.$inject = ['nvDOMManipulation', 'nvGa'];

  function apiIconButton(nvDOMManipulation, nvGa) {
    let idx = 0;

    return {
      restrict: 'E',
      templateUrl: 'lib/ninja-commons/directives/buttons/api-icon-button/api-icon-button.directive.html',
      scope: {
        name: '@',
        icon: '@',
        state: '=',
        disabled: '=?',
        onClick: '&',
        form: '=?',
        tooltipDelay: '@?',
        tooltipDirection: '&?',
        buttonId: '@?',
      },
      link: link,
    };

    function link(scope, elem) {
      nvDOMManipulation.moveClassesToFirstChild(elem[0]);

      if (angular.isUndefined(scope.buttonId)) {
        scope.id = `button-api-icon_${idx++}`;
      } else {
        scope.id = scope.buttonId;
        nvDOMManipulation.removeAttrFromElement(elem[0], 'button-id');
      }

      scope.isDisabled = isDisabled;
      if (angular.isUndefined(scope.disabled)) {
        scope.disabled = false;
      }

      if (angular.isUndefined(scope.tooltipDelay)) {
        scope.tooltipDelay = 400;
      }

      if (angular.isUndefined(scope.tooltipDirection)) {
        scope.tooltipDirection = 'bottom';
      }

      scope.onClickWrapper = ($event) => {
        scope.onClick($event);
        nvGa.event('button', 'click-api', scope.name);
      };

      function isDisabled() {
        return formError() || scope.disabled || (scope.state === 'waiting');

        function formError() {
          return (scope.form && (scope.form.$invalid));
        }
      }
    }
  }
}());

