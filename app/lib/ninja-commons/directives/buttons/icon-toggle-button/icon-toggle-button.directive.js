(function() {
    'use strict';

    angular
        .module('nvCommons.directives')
        .directive('nvIconToggleButton', iconToggleButton);

    /**
     * nvIconToggleButton - Toggleable Icon Button, change the icon based on state (true or false)
     *
     * @param {boolean} state true or false
     * @param {string} iconActive Material Icon font name when state is true: @see https://design.google.com/icons/
     * @param {string} iconInactive Material Icon font name when state is false: @see https://design.google.com/icons/
     * @param {string} textActive Tooltip text when state is true
     * @param {string} textInactive Tooltip text when state is false
     * @param {function} onClick onClick callback function, local variables available: $event, $state
     */

    iconToggleButton.$inject = ['nvDOMManipulation'];

    function iconToggleButton(nvDOMManipulation) {

        return {
            restrict: 'E',
            templateUrl: 'lib/ninja-commons/directives/buttons/icon-toggle-button/icon-toggle-button.directive.html',
            scope: {
                state: '=',
                onClick: '&?',
                iconActive: '@',
                iconInactive: '@',
                textActive: '@',
                textInactive: '@',
            },
            link: link
        };

        function link(scope, element) {
            scope.onClick = angular.isDefined(scope.onClick) ? scope.onClick : angular.noop;

            nvDOMManipulation.moveClassesToFirstChild(element[0]);
            scope._onClick = function($event) {
                scope.state = !scope.state;
                scope.onClick({$event: $event, state: scope.state});
            };

        }

    }

})();
