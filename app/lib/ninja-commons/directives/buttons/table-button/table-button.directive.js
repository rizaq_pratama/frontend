;(function() {
    'use strict';

    angular
        .module('nvCommons.directives')
        .directive('nvTableButton', tableButton);

    function tableButton() {

        return {
            restrict: 'E',
            templateUrl: 'lib/ninja-commons/directives/buttons/table-button/table-button.directive.html',
            scope: {
                name: '@',
                icon: '@?',
                onClick: '&',
                disabled : '=?'
            },
            link: link
        };

        function link(scope, iElement) {
            _(iElement[0].classList)
                .difference(['ng-isolate-scope'])
                .forEach(moveClassToChild);

            if (angular.isDefined(scope.icon)) {
                iElement[0].children[0].classList.add('nv-button-with-icon');
            }

            function moveClassToChild(clazz) {
                iElement[0].classList.remove(clazz);
                iElement[0].children[0].classList.add(clazz);
            }
        }

    }

})();
