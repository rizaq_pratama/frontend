(function() {
    'use strict';

    angular
        .module('nvCommons.directives')
        .directive('nvCreateButton', createButton);

    /**
     * nvCreateButton
     *
     * Just a wrapper of nvIconTextButton with predefined style
     *
     * @param {string}    name      label of the button, eg: 'Create Shipper'
     * @param {function&} onClick   on click function
     * @param {boolean}   disabled  if true, disable the button
     * 
     */

    createButton.$inject = [];

    function createButton() {

        return {
            restrict: 'E',
            template: function(elem, attr){
                return ['<nv-icon-text-button ',
                            'icon="add" ',
                            'name="', attr.name, '" ',
                            'on-click="', attr.onClick, '" ', 
                            'md-theme="nvGreen" ', 
                            'class="raised" ',
                            'disabled="', (attr.disabled || 'false'), '">',
                           '</nv-icon-text-button>'].join('');
            },
        };
    }
})();
