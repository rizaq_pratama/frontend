(function() {
    'use strict';

    angular
        .module('nvCommons.directives')
        .directive('nvToggleButton', toggleButton);

    /**
     * nvToggleButton - Toggleable Icon Button, change the icon based on state (true or false)
     *
     * @param {boolean} state true or false
     * @param {function} onClick onClick callback function, local variables available: $event, $state
     * @param {string} textActive Tooltip text when state is true
     * @param {string} textInactive Tooltip text when state is false
     * 
     */

    toggleButton.$inject = ['nvDOMManipulation'];

    function toggleButton(nvDOMManipulation) {

        return {
            restrict: 'E',
            templateUrl: 'lib/ninja-commons/directives/buttons/toggle-button/toggle-button.directive.html',
            scope: {
                state: '=',
                onClick: '&?',
                textActive: '@',
                textInactive: '@'
            },
            link: link
        };

        function link(scope, element) {
            scope.onClick = angular.isDefined(scope.onClick) ? scope.onClick : angular.noop;
            
            nvDOMManipulation.moveClassesToFirstChild(element[0]);
            scope._onClick = function($event) {
                scope.state = !scope.state;
                scope.onClick({$event: $event, state: scope.state});
            };
        }

    }

})();
