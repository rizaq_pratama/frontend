(function directive() {
  angular
        .module('nvCommons.directives')
        .directive('nvDownloadCsvButton', downloadCsvButton);

  /**
   * Download CSV Button
   *
   * @param {object} data
   * @param {string} name button name
   * @param {string} csvHeader
   * @param {string} csvColumnOrder [description]
   * @param {string} filename
   * @param {boolean} disable If true, disable the button
   * @param {boolean} dense If true, show only icon
   * @param {boolean} have icon if true, default to true
   * @param {boolean} quoteStrings, default to enabled
   */

  downloadCsvButton.$inject = ['nvUtils', 'nvGa'];

  function downloadCsvButton(nvUtils, nvGa) {
    return {
      restrict: 'E',
      templateUrl: 'lib/ninja-commons/directives/buttons/download-csv-button/download-csv-button.directive.html',
      scope: {
        data: '=',
        name: '@?',
        csvHeader: '@',
        csvColumnOrder: '@',
        filename: '@',
        disabled: '=?',
        dense: '@?',
        haveIcon: '=?',
      },
      compile: compile,
    };

    function compile(elem, attr) {
      if (_.isUndefined(attr.quoteStrings) && attr.quoteStrings !== 'false') {
        elem.find('div').attr('quote-strings', 'true');
      }

      return link;
    }

    function link(scope) {
      scope.getCSVData = getCSVData;
      scope.parse = parse;
      scope.isDisabled = isDisabled;
      scope.state = 'idle';
      scope.dense = angular.isDefined(scope.dense) ? scope.dense : false;
      scope.name = angular.isDefined(scope.name) ? scope.name : 'commons.download-csv';
      scope.haveIcon = angular.isDefined(scope.haveIcon) ? scope.haveIcon : true;

      if (angular.isUndefined(scope.disabled)) {
        scope.disabled = false;
      }

      function getCSVData() {
        let dataAsArray = [];
        nvGa.event('button', 'click-csv', scope.name);
        scope.state = 'waiting';
        const data = (_.isFunction(scope.data)) ? scope.data() : scope.data;

        if (_.isPlainObject(data)) {
          _.each(data, (e) => { dataAsArray.push(e); });
        } else {
          dataAsArray = data;
        }

        const groupByWithDeepProps = _.groupBy(parse(scope.csvColumnOrder), key =>
          _.includes(key, '.')
        );

        const result = _.merge(
          _.map(dataAsArray, theData => _.pick(theData, groupByWithDeepProps.false)),
          _.map(dataAsArray, theData =>
            _.reduce(groupByWithDeepProps.true, (theResult, key) => {
              theResult[key] = nvUtils.getDeep(theData, key);
              return theResult;
            }, {})
          )
        );
        scope.state = 'idle';

        return result;
      }

      function parse(str) {
        return JSON.parse(str.replace(/['"]/g, '"'));
      }

      function isDisabled() {
        return scope.disabled || scope.state !== 'idle';
      }
    }
  }
}());
