(function directive() {
  angular
    .module('nvCommons.directives')
    .directive('nvIconTextButton', iconTextButton);

  /**
   * nvIconTextButton - Icon Text Button
   *
   * @param {string|resId} name             For aria-label and tooltip, can be string or tranlation resId
   * @param {string}       icon             Material Icon font name: @see https://design.google.com/icons/
   * @param {string}       iconPosition     Position of the icon, either 'left' or 'right'. default 'left'.
   * @param {string|resId} text             Text for the button, if undefined, will use name as the button text
   * @param {boolean}      disabled         Optional. To disable the button
   * @param {function}     onClick          Callback for onClick
   * @param {form}         form             Optional. Form Controller. Button will be disabled when form is invalid.
   * @param {number}       tooltipDelay     delay for tooltip, in milliseconds
   * @param {string}       tooltipDirection direction of the tooltip
   * @param {string}       buttonId         id of the button
   * @param {boolean}      showToolTip      true if show tool tip (default : true)
   *
   */

  iconTextButton.$inject = ['nvDOMManipulation', 'nvGa'];

  function iconTextButton(nvDOMManipulation, nvGa) {
    let idx = 0;

    return {
      restrict: 'E',
      transclude: true,
      templateUrl: 'lib/ninja-commons/directives/buttons/icon-text-button/icon-text-button.directive.html',
      scope: {
        name: '@',
        icon: '@?',
        iconPosition: '@?',
        text: '@?',
        disabled: '=?',
        onClick: '&',
        form: '=?',
        tooltipDelay: '@?',
        tooltipDirection: '&?',
        buttonId: '@?',
        ngClazz: '=?',
        showToolTip: '=?',
      },
      compile: compile,
    };

    function compile(elem, attr) {
      if (attr.mdThemeWatch) {
        elem.find('md-button').attr('md-theme-watch', attr.mdThemeWatch);
      }
      return link;
    }

    function link(scope, elem) {
      nvDOMManipulation.moveClassesToFirstChild(elem[0]);
      
      if (angular.isUndefined(scope.buttonId)) {
        scope.id = `button-api-text_${idx++}`;
      } else {
        scope.id = scope.buttonId;
        nvDOMManipulation.removeAttrFromElement(elem[0], 'button-id');
      }
      
      scope.name = scope.name || 'commons.save';
      scope.showToolTip = angular.isDefined(scope.showToolTip) ? scope.showToolTip : getDefaultShowTooltip();
      scope.isDisabled = isDisabled;
      
      if (angular.isUndefined(scope.disabled)) {
        scope.disabled = false;
      }

      scope.onClickWrapper = ($event) => {
        scope.onClick($event);
        nvGa.event('button', 'click', scope.name);
      };

      function isDisabled() {
        return formError() || scope.disabled;

        function formError() {
          return (scope.form && (scope.form.$invalid));
        }
      }

      function getDefaultShowTooltip() {
        return !(!scope.name || scope.text === undefined);
      }
    }
  }
}());

