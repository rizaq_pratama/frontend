(function directive() {
  angular
        .module('nvCommons.directives')
        .directive('nvDragAndDrop', dragAndDrop);

  dragAndDrop.$inject = ['$timeout'];
  function dragAndDrop($timeout) {
    return {
      restrict: 'E',
      transclude: true,
      templateUrl: 'lib/ninja-commons/directives/drag-and-drop/drag-and-drop.directive.html',
      scope: {
        id: '@?',
        list: '=',
        enableWatchList: '=?',
        disableSpliceOnMoved: '=?',
        onDrop: '&?',
        allowedTypes: '=?',
        disableIf: '&?',
        dropzone: '=?',
        multiSelect: '=?',
        buttonTheme: '@?',
        buttonClass: '@?',
        buttonNgClass: '=?',
        onClose: '&?',
      },
      compile: compile,
    };

    function compile(elem, attr) {
      if (attr.templateWithCloseBtn === 'true') {
        $(elem.children()[0].children[0]).addClass('template-with-close-btn');
        elem.children()[0].children[0].innerHTML = `<div class="template-with-close-btn-holder">
          <a href="javascript:;" class="close-btn" ng-click="onClose({item: item})"><i class="material-icons">close</i></a>
          <div ng-bind-html="item.label"></div>
        </div>`;
      } else {
        elem.children()[0].children[0].innerHTML = `<button class="item-label nv-button md-button raised dense nv-text-no-select"
            ng-class="::item.ngClazz"
            ng-disabled="itemDisableIf(item)"
            ng-bind-html="item.label | translate">
        </button>`;
      }

      return link;
    }

    function link(scope) {
      scope.disableSpliceOnMoved = scope.disableSpliceOnMoved || false;
      scope.onDrop = angular.isDefined(scope.onDrop) ? scope.onDrop : null;
      scope.allowedTypes = angular.isDefined(scope.allowedTypes) ? scope.allowedTypes : '';
      scope.disableIf = angular.isDefined(scope.disableIf) ? scope.disableIf : null;
      scope.multiSelect = angular.isDefined(scope.multiSelect) ? scope.multiSelect : false;
      scope.buttonTheme = angular.isDefined(scope.buttonTheme) ? scope.buttonTheme : 'nvBlue';
      scope.buttonClass = angular.isDefined(scope.buttonClass) ? scope.buttonClass : 'nv-primary';
      scope.enableWatchList = !_.isUndefined(scope.enableWatchList) ? scope.enableWatchList : false;
      scope.onClose = scope.onClose || _.noop();

      scope.$watch('list', (newValue, oldValue) => {
        if (!scope.enableWatchList || _.isEqual(newValue, oldValue)) {
          return;
        }

        processList(newValue);
      });
      processList(scope.list);

      scope.onItemDrop = function onItemDrop(item, dndType, index, event) {
        if (scope.onDrop === null) {
          if (scope.multiSelect) {
            _.forEach(item, (row) => {
              row.dndSelected = false;
            });

            scope.list = scope.list.concat(item);
            return true;
          }

          return item;
        }

        $timeout(() => {
          scope.$eval(scope.onDrop)({
            item: item, // if multiSelect = true, then this will be array of items
            dndType: dndType,
            index: index,
            event: event,
            target: this,
          });
        });
        return true;
      };

      scope.onItemMoved = function onItemMoved(index) {
        if (scope.disableSpliceOnMoved) {
          return;
        }

        if (scope.multiSelect) {
          scope.list = _.filter(scope.list, item => !item.dndSelected);
        } else {
          scope.list.splice(index, 1);
        }
      };

      scope.itemDisableIf = function itemDisableIf(item) {
        if (scope.disableIf === null) { return false; }

        return scope.$eval(scope.disableIf)(item);
      };

      scope.onItemSelected = function onItemSelected(item) {
        if (scope.multiSelect) {
          item.dndSelected = !item.dndSelected;
        }
      };

      scope.getSelectedItems = function getSelectedItems(item) {
        if (scope.multiSelect) {
          item.dndSelected = true;
          return _.filter(scope.list, theItem => theItem.dndSelected);
        }

        return item;
      };

      scope.onDragstart = function onDragstart(event) {
        if (event.dataTransfer.setDragImage &&
          scope.multiSelect && _.size(scope.getSelectedItems({})) > 1) {
          const i = document.createElement('i');
          i.append('content_copy');
          i.setAttribute('class', 'material-icons');
          document.body.appendChild(i);
          event.dataTransfer.setDragImage(i, 0, 0);
        }
      };

      function processList(list) {
        _.forEach(list, (item) => {
          const ngClazz = {};

          if (angular.isDefined(scope.buttonClass)) {
            const buttonClasses = scope.buttonClass.split(' ');
            _.forEach(buttonClasses, (buttonClass) => {
              if (buttonClass) {
                ngClazz[buttonClass] = true;
              }
            });
          }

          if (angular.isDefined(scope.buttonNgClass)) {
            _.forEach(scope.buttonNgClass, (fn, key) => {
              ngClazz[key] = fn(item);
            });
          }

          _.defaultsDeep(item, {
            dndType: '',
            dndSelected: false,
            buttonTheme: scope.buttonTheme,
            ngClazz: ngClazz,
          });
        });
      }
    }
  }
}());
