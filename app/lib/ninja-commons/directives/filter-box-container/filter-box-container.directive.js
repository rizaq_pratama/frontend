(function directive() {
  angular
    .module('nvCommons.directives')
    .directive('nvFilterBoxContainer', nvFilterBoxContainerDirective);

  nvFilterBoxContainerDirective.$inject = ['nvAutocomplete.Data'];

  function nvFilterBoxContainerDirective(nvAutocompleteData) {
    nvFilterBoxContainerController.$inject = ['$scope'];

    return {
      restrict: 'E',
      templateUrl: 'lib/ninja-commons/directives/filter-box-container/filter-box-container.directive.html',
      replace: true,
      scope: {
        selectedFilters: '=',
        possibleFilters: '=',
        searchBy: '@?',
        onChange: '&?',
        form: '=?',
      },
      controller: nvFilterBoxContainerController,
    };

    function nvFilterBoxContainerController(scope) {
      scope.searchBy = scope.searchBy || 'filterName';
      const searchByFn = _.property(scope.searchBy);
      scope.searchText = '';
      scope.autocompleteHandle = nvAutocompleteData.generateHandle();
      scope.onChange = scope.onChange || angular.noop;

      scope.dataService = nvAutocompleteData.getByHandle(scope.autocompleteHandle);
      scope.dataService.setSortBy(null);
      scope.dataService.setDisplayBy(scope.searchBy);
      scope.dataService.setPossibleOptions(scope.possibleFilters);
      scope.dataService.setSelectedOptions(scope.selectedFilters);
      scope.dataService.onChange(onChange);

      scope.onClose = (filter) => {
        if (!filter.isMandatory) {
          scope.dataService.remove(filter);
        }
      };
      scope.onClearAll = () => {
        const selectedFilters = scope.dataService.getSelectedOptions();
        const selectedFiltersMap = _.keyBy(selectedFilters, 'key');
        _.each(selectedFiltersMap, (filter) => {
          if (!filter.isMandatory) {
            scope.dataService.remove(filter);
          }
        });
      };

      scope.$on('$destroy', () => {
        nvAutocompleteData.removeHandle(scope.autocompleteHandle);
      });

      function onChange(changes) {
        scope.possibleFilters = changes.possibleOptions;
        scope.selectedFilters = changes.selectedOptions;

        scope.onChange(changes);
      }
    }
  }
}());