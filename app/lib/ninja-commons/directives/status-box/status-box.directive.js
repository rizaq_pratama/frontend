;(function() {
    'use strict';

    angular
        .module('nvCommons.directives')
        .directive('nvStatusBox', statusBox);

    function statusBox() {

        return {
            restrict: 'E',
            templateUrl: 'lib/ninja-commons/directives/status-box/status-box.directive.html',
            scope: {
                iconClass: '@',
                description: '@',
                count: '=',
                isSelected: '=',
                onClick: '&',
                tooltip: '@?'
            },
            link: link
        };

        function link() {

        }

    }

})();
