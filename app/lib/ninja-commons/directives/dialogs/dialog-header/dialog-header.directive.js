(function directive() {
  angular
    .module('nvCommons.directives')
    .directive('nvDialogHeader', dialogHeader);

  function dialogHeader() {
    return {
      restrict: 'E',
      templateUrl: 'lib/ninja-commons/directives/dialogs/dialog-header/dialog-header.directive.html',
      replace: true,
      scope: {
        title: '@',
        onCancel: '&',
        cancelText: '@?',
      },
      link: link,
    };

    function link(scope) {
      scope.cancelText = scope.cancelText || 'commons.discard-changes';
    }
  }
}());
