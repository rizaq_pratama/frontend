(function directive() {
  angular
    .module('nvCommons.directives')
    .directive('nvAutocomplete', AutocompleteDirective);

  /**
   *
   * nvAutocomplete Directive
   *
   * @param {string}      placeholder              Placeholder for autocomplete
   * @param {string}      delegateHandle           DelegateHandle for nvAutocomplete.Data
   * @param {string}      itemTypes                Used in md-not-found, ie: "no {{::itemTypes}} matching {{searchText}} were found."
   * @param {string}      icon material            Icon name at the left of the nvAutocomplete, default to "search"
   * @param {expression}  selectedItem             A model to bind selected item by default
   * @param {expression}  searchText               A model to bind search query text to
   * @param {boolean}     multiple                 Specify this nvAutocomplete allow multiple selections, this will change the UX behaviour of the nvAutocomplete, default to false
   * @param {string}      sortBy                   Key to sort by for the autocomplete suggestions
   * @param {string}      sortOrder                Default for ascending order, 'asc' for ascending order and 'desc' for descending order
   * @param {string}      displayBy                Key for the object to display  for the autocomplete suggestions, NOTE: Substring is matched with this property!!!!
   * @param {expression}  onSelect                 An expression to be run each time a new item is selected, eg: "doSomething($item)"
   * @param {expression}  onRemove                 An expression to be run each time the selected item is removed, eg: "doSomething($item)"
   * @param {expression}  onChange                 An expression to be run each time a new item is selected or the selected item is removed, eg: "doSomething($possibleOptions, $selectedOptions)"
   * @param {integer}     maxNumberResult          A value to limit the total number of results returned from the suggestion, this is to ensure a smooth ui
   * @param {string}      noFoundItemMessage       A string to show to replace the default md-not-found, can use string interpolation such as "Cant find {{ctrl.searchText}}"
   * @param {boolean}     hideNotFound             If true, don't show md-not-found
   * @param {boolean}     disabled                 An expression to disable the nvAutocomplete
   * @param {boolean}     showSuggestionWhenEmpty  An expression to allow nvAutocomplete to show suggestions when search query text is empty, default to false.
   * @param {boolean}     clearOnBlur              If true, when the input losing focus, clear search query text if nothing is selected, default to true
   * @param {string}      mdMenuClass              The string specified will be applied as class to the dropdown menu for styling, default to ''
   * @param {string}      theme                    Theme name, 'light' or 'dark', default to 'light'
   * @param {boolean}     hideDropdownIcon         If true, don't show the dropdown icon on the right, default false
   * @param {boolean}     hideSearchIcon           If true, hide the icon on the left, default false
   * @param {expression}  selectedOptions          An expression of list of selected options for the autocomplete
   * @param {expression}  possibleOptions          An expression of list of possible options for the autocomplete
   * @param {form}        form                     Will set the form to dirty when data changed
   *
   */

  /* TODO: Fix this: when clearing searchText and showSuggestionWhenEmpty = true, the selected item is missing from the possible options,
   * Problem: data.filter is getting called before onSelectedItemChange
   */
  AutocompleteDirective.$inject = ['nvAutocomplete.Data', '$timeout'];

  function AutocompleteDirective(nvAutocompleteData, $timeout) {
    return {
      restrict: 'E',
      templateUrl: 'lib/ninja-commons/directives/autocomplete/autocomplete.directive.html',
      scope: {
        placeholder: '@',
        delegateHandle: '@',
        itemTypes: '@',
        icon: '@?',
        searchText: '=?',
        persistSearchText: '=?',
        multiple: '=?',
        sortBy: '@?',
        sortOrder: '@?',
        displayBy: '@?',
        onSelect: '&?',
        onRemove: '&?',
        onChange: '&?',
        maxNumberResult: '=?',
        noFoundItemMessage: '@?',
        hideNotFound: '=?',
        disabled: '=?',
        minTextLength: '@?',
        showSuggestionWhenEmpty: '=?',
        clearOnBlur: '=?',
        mdMenuClass: '@?',
        theme: '@?',
        hideDropdownIcon: '=?',
        hideSearchIcon: '=?',
        selectedItem: '=?',
        selectedOptions: '=?',
        possibleOptions: '=?',
        onBlurCallback: '&?',
        form: '=?',
      },
      compile: compile,
    };

    function compile(elem, attr) {
      if (attr.delay) {
        elem.find('md-autocomplete').attr('md-delay', attr.delay);
      }

      if (attr.required && attr.required === 'true') {
        elem.find('md-autocomplete').attr('required', "true");
      }

      return link;
    }

    function link(scope, elem) {
      scope.icon = angular.isDefined(scope.icon) ? scope.icon : 'search';
      scope.data = nvAutocompleteData.getByHandle(scope.delegateHandle);
      scope.multiple = angular.isDefined(scope.multiple) ? scope.multiple : false;
      scope.searchText = angular.isDefined(scope.searchText) ? scope.searchText : '';
      scope.persistSearchText = angular.isDefined(scope.persistSearchText) ? scope.persistSearchText : false;
      scope.clearOnBlur = angular.isDefined(scope.clearOnBlur) ? scope.clearOnBlur : true;
      scope.mdMenuClass = angular.isDefined(scope.mdMenuClass) ? scope.mdMenuClass : '';
      scope.theme = angular.isDefined(scope.theme) ? scope.theme : 'light';
      scope.hideDropdownIcon = angular.isDefined(scope.hideDropdownIcon) ? scope.hideDropdownIcon : false;
      scope.hideSearchIcon = angular.isDefined(scope.hideSearchIcon) ? scope.hideSearchIcon : false;
      scope.focus = false;
      scope.menuHidden = false;
      scope.minTextLength = angular.isDefined(scope.minTextLength) ? +scope.minTextLength : 1;

      scope.selectedItem = angular.isDefined(scope.selectedItem) ? scope.selectedItem : null;
      scope.onSelectedItemChange = onSelectedItemChange;
      scope.toggleMenu = toggleMenu;
      scope.onDropdownClick = onDropdownClick;
      scope.onBlurCallback = angular.isDefined(scope.onBlurCallback) ? scope.onBlurCallback : _.noop;
      scope.filter = filter;

      let searchText = scope.searchText;

      // initialize data service
      if (angular.isDefined(scope.sortBy)) {
        angular.isDefined(scope.sortOrder) ?
          scope.data.setSortBy(scope.sortBy, scope.sortOrder) :
          scope.data.setSortBy(scope.sortBy, 'asc');
      }
      if (angular.isDefined(scope.displayBy)) {
        scope.data.setDisplayBy(scope.displayBy);
      }

      if (angular.isDefined(scope.maxNumberResult)) {
        scope.data.setMaxResult(scope.maxNumberResult);
      }

      if (angular.isDefined(scope.showSuggestionWhenEmpty)) {
        scope.minTextLength = scope.showSuggestionWhenEmpty ? 0 : 1;
      }

      if (angular.isDefined(scope.selectedOptions)) {
        scope.data.setSelectedOptions(scope.selectedOptions);
      }

      if (angular.isDefined(scope.possibleOptions)) {
        scope.data.setPossibleOptions(scope.possibleOptions);
      }

      const offSelect = angular.isDefined(scope.onSelect) ? scope.data.onSelect(wrapFn(scope.onSelect)) : angular.noop;
      const offRemove = angular.isDefined(scope.onRemove) ? scope.data.onRemove(wrapFn(scope.onRemove)) : angular.noop;
      const offChange = angular.isDefined(scope.onChange) ? scope.data.onChange(wrapFn(scope.onChange)) : angular.noop;

      // clean up listeners
      scope.$on('$destroy', () => {
        offSelect();
        offRemove();
        offChange();
        nvAutocompleteData.removeHandle(scope.delegateHandle);
      });

      const originalMinTextLength = scope.minTextLength;

      // hackish way to get mdAutocomplete menu state by accessing to the mdAutocompleteCtrl
      // Currently can't find any public api to access the autocomplete menu show/hidden state and modify the autocomplete
      // Inspired from: http://stackoverflow.com/a/31859445
      const mdAutocomplete = angular.element(elem.find('md-autocomplete'));
      const mdAutocompleteCtrl = mdAutocomplete.controller('mdAutocomplete');
      scope.$mdAutocompleteCtrl = mdAutocompleteCtrl.scope.$mdAutocompleteCtrl;
      // to hide no found
      if (angular.isDefined(scope.hideNotFound) && scope.hideNotFound) {
        scope.$mdAutocompleteCtrl.hasNotFound = false;
      }
      // watch whether autocomplete suggestion menu is hidden
      scope.$watch('$mdAutocompleteCtrl.hidden', (hidden) => {
        scope.menuHidden = hidden;
        if (hidden) {
          scope.minTextLength = originalMinTextLength;
          onBlur();
        }
      });

      // input on blur and on focus event listener
      // $timeout is used because the input element only available on next tick
      $timeout(setupFocusBlurListeners);

      // ////////////////////

      function onSelectedItemChange(item) {
        if (angular.isDefined(item)) {
          if (scope.multiple) {
            if (scope.persistSearchText) {
              scope.searchText = searchText;
            } else {
              scope.searchText = '';
            }
          } else {
            scope.selectedItem = item;
          }
          scope.data.select(item);

          if (_.size(scope.data.getPossibleOptions()) <= 0) {
            $timeout(() => {
              scope.$mdAutocompleteCtrl.listLeave();
            });
          }
        } else if (!scope.multiple) {
          scope.data.removeAll();
        }
        if (scope.form) {
          scope.form.$setDirty();
        }
      }
      function wrapFn(fn) {
        // calling function with local: $item, $possibleOptions, $selectedOptions
        return function wrap(item) {
          fn({ $item: item, $possibleOptions: item.possibleOptions, $selectedOptions: item.selectedOptions });
        };
      }

      function setupFocusBlurListeners() {
        const input = elem.find('input');
        input.on('blur', onInputBlur);
        input.on('focus', onInputFocus);
      }

      function onInputBlur() {
        $timeout(() => {
          scope.focus = false;
          onBlur();
        });
      }

      function onInputFocus() {
        $timeout(() => {
          scope.focus = true;
        });
      }

      function onBlur() {
        // must be no more on focus and suggestion menu is hidden, only check whether to clear the text
        if (!scope.focus && scope.menuHidden && scope.clearOnBlur) {
          $timeout(clearText);
        }
        scope.onBlurCallback();

        function clearText() {
          if (scope.selectedItem === null) {
            scope.searchText = '';
          }
        }
      }

      // toggle menu with the right dropdown icon
      function toggleMenu(e) {
        e.preventDefault();
        if (scope.menuHidden) {
          scope.minTextLength = 0;
          scope.menuHidden = false;
          $timeout(scope.$mdAutocompleteCtrl.focus);
        } else {
          $timeout(() => {
            const input = elem.find('input');
            input.blur();
            scope.$mdAutocompleteCtrl.blur();
          });
        }
      }

      // drop down onclick
      function onDropdownClick(e) {
        if (scope.menuHidden) {
          e.preventDefault();
        }
      }

      function filter(theSearchText) {
        searchText = theSearchText;
        return scope.data.filter(theSearchText);
      }
    }
  }
}());
