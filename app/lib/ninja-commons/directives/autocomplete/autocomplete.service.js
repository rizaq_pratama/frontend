(function service() {
  angular
        .module('nvCommons.directives')
        .factory('nvAutocomplete.Data', AutocompleteDataService);

  AutocompleteDataService.$inject = ['DelegateHandle'];
  function AutocompleteDataService(DelegateHandle) {
    return DelegateHandle.create(AutocompleteData, 'nv-autocomplete-');
  }

  AutocompleteData.$inject = ['nvCoupledListUtils', 'Listener', '$q'];
  function AutocompleteData(nvCoupledListUtils, Listener, $q) {
    const autocomplete = this;

    // private variables
    autocomplete._possibleOptions = [];
    autocomplete._selectedOptions = [];
    autocomplete._possibleOptionsPromise = null;
    autocomplete._callPromiseOnChange = true;
    autocomplete._matchFn = _.eq;
    autocomplete._sortBy = 'displayName';
    autocomplete._sortOrder = 1;
    autocomplete._sortByFn = _.property(autocomplete._sortBy);
    autocomplete._displayBy = 'displayName';
    autocomplete._displayByFn = _.property(autocomplete._displayBy);
    autocomplete._filterFn = undefined;
    autocomplete._selectListener = Listener.create();
    autocomplete._removeListener = Listener.create();
    autocomplete._changeListener = Listener.create();

    // Maximum number of results return from filter,
    // More than that it's going to be laggy, we can search through it,
    // why bother scroll among 100 items
    autocomplete._maxResult = 100;

    // public methods
    autocomplete.getPossibleOptions = getPossibleOptions;
    autocomplete.setPossibleOptions = setPossibleOptions;
    autocomplete.getSelectedOptions = getSelectedOptions;
    autocomplete.setSelectedOptions = setSelectedOptions;
    autocomplete.getCallPromiseOnChange = getCallPromiseOnChange;
    autocomplete.setCallPromiseOnChange = setCallPromiseOnChange;
    autocomplete.getDisplay = getDisplay;
    autocomplete.setMatchFn = setMatchFn;
    autocomplete.setSortBy = setSortBy;
    autocomplete.setDisplayBy = setDisplayBy;
    autocomplete.setFilterFn = setFilterFn;
    autocomplete.setMaxResult = setMaxResult;
    autocomplete.select = select;
    autocomplete.selectAll = selectAll;
    autocomplete.remove = remove;
    autocomplete.removeAll = removeAll;
    autocomplete.filter = filter;
    autocomplete.onSelect = autocomplete._selectListener.on;
    autocomplete.onRemove = autocomplete._removeListener.on;
    autocomplete.onChange = autocomplete._changeListener.on;

    // ///////////////////////////

    function getPossibleOptions() {
      return autocomplete._possibleOptions;
    }

    function setPossibleOptions(optionsOrPromise) {
      let promise;
      if (_.isFunction(optionsOrPromise) && !autocomplete._callPromiseOnChange) {
        promise = $q.when(optionsOrPromise())
          .then((options) => {
            autocomplete._possibleOptions = sort(options);
            autocomplete._possibleOptionsPromise = null;
            notifyChange();
            return getPossibleOptions();
          });
      } else if (_.isFunction(optionsOrPromise)) {
        promise = optionsOrPromise;
      } else {
        promise = $q.when(optionsOrPromise)
          .then((options) => {
            autocomplete._possibleOptions = sort(options);
            autocomplete._possibleOptionsPromise = null;
            notifyChange();
            return getPossibleOptions();
          });
      }

      autocomplete._possibleOptionsPromise = promise;
      return promise;
    }

    function getSelectedOptions() {
      return autocomplete._selectedOptions;
    }

    function setSelectedOptions(options) {
      autocomplete._selectedOptions = sort(options);
      notifyChange();
    }

    function getCallPromiseOnChange() {
      return autocomplete._callPromiseOnChange;
    }

    function setCallPromiseOnChange(bool) {
      autocomplete._callPromiseOnChange = bool;
    }

    function getDisplay(item) {
      return autocomplete._displayByFn(item);
    }

    function setMatchFn(fn) {
      autocomplete._matchFn = fn;
    }

    function setSortBy(sortBy, sortOrder) {
      autocomplete._sortBy = sortBy;
      autocomplete._sortOrder = sortOrder === 'desc' ? -1 : 1;
      autocomplete._sortByFn = _.property(autocomplete._sortBy);
      sort(autocomplete._possibleOptions);
    }

    function setDisplayBy(displayBy) {
      autocomplete._displayBy = displayBy;
      autocomplete._displayByFn = _.property(autocomplete._displayBy);
    }

    function setFilterFn(fn) {
      autocomplete._filterFn = fn;
    }

    function setMaxResult(max) {
      autocomplete._maxResult = max;
    }

    function select(item) {
      if (_.isFunction(autocomplete._possibleOptionsPromise)) {
        autocomplete._possibleOptions.push(item);
      }

      nvCoupledListUtils.transfer(
                autocomplete._possibleOptions,
                autocomplete._selectedOptions,
                matchFn(item), getSortOption()
            );
      autocomplete._selectListener.notify(item);
      notifyChange();
    }

    function selectAll() {
      const selected = _.clone(autocomplete._possibleOptions); // shallow clone
      nvCoupledListUtils.transferAll(
                autocomplete._possibleOptions,
                autocomplete._selectedOptions,
                getSortOption()
            );
      _.each(selected, autocomplete._selectListener.notify);
      notifyChange();
    }

    function remove(item) {
      nvCoupledListUtils.transfer(
                autocomplete._selectedOptions,
                autocomplete._possibleOptions,
                matchFn(item), getSortOption()
            );
      autocomplete._removeListener.notify(item);
      notifyChange();
    }

    function removeAll() {
      const removed = _.clone(autocomplete._selectedOptions); // shallow clone
      nvCoupledListUtils.transferAll(
                autocomplete._selectedOptions,
                autocomplete._possibleOptions,
                getSortOption()
            );
      _.each(removed, autocomplete._selectListener.notify);
      notifyChange();
    }

    function filter(text) {
      if (_.isFunction(autocomplete._possibleOptionsPromise)) {
        return autocomplete._possibleOptionsPromise(text)
          .then(trim);
      }

      if (_.isEmpty(text)) {
        return _.isNull(autocomplete._possibleOptionsPromise) ?
                            trim(autocomplete._possibleOptions) :
                            autocomplete._possibleOptionsPromise.then(trim);
      }

      function filterPossibleOptions(theText) {
        const filterFn = angular.isDefined(autocomplete._filterFn) ?
                                autocomplete._filterFn(theText) :
                                defaultFilterFn(theText);
        return trim(_.filter(autocomplete._possibleOptions, filterFn));
      }

      return _.isNull(autocomplete._possibleOptionsPromise) ?
                        filterPossibleOptions(text) :
                        autocomplete._possibleOptionsPromise.then(
                          _.partial(filterPossibleOptions, text)
                        );
    }

    function trim(array) {
      return _.take(array, autocomplete._maxResult);
    }

    function notifyChange() {
      autocomplete._changeListener.notify({
        possibleOptions: autocomplete._possibleOptions,
        selectedOptions: autocomplete._selectedOptions,
      });
    }

    function sort(arr) {
      return autocomplete._sortBy ? arr.sort(sortFn) : arr;
    }

    function getSortOption() {
      return autocomplete._sortBy ? { sort: sortFn } : {};
    }

    function sortFn(a, b) {
      const aa = autocomplete._sortByFn(a);
      const bb = autocomplete._sortByFn(b);
      if (_.lt(aa, bb)) { return -1 * autocomplete._sortOrder; }
      if (_.gt(aa, bb)) { return 1 * autocomplete._sortOrder; }
      return 0;
    }

    function matchFn(item) {
      return function fn(fromItem) {
        return autocomplete._matchFn(item, fromItem);
      };
    }

    function defaultFilterFn(text) {
      // escape text for regex
      const regex = new RegExp(text.trim().replace(/[\-\[\]\/\{\}\(\)\*\+\?\.\\\^\$\|]/g, '\\$&'), 'i');
      return function fn(item) {
        return regex.test(autocomplete._displayByFn(item));
      };
    }
  }
}());
