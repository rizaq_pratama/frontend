(function() {
    'use strict';

    angular
    .module('nvCommons.directives')
    .directive('nvFileDrop', fileDrop);

    fileDrop.$inject = ['$parse', 'nvButtonFilePickerType'];

    /**
     * File Drop Zone
     * Attributes:
     *   label - label text shown when file hover above the drop zone
     *   disabled - disable the drop zone
     *   disabledLabel - label text shown when file hover above the disabled drop zone
     *   accept - file type the drop zone accepts, eg: `nvButtonFilePickerType.IMAGE`
     *   onFileSelect - callback function when files are selected, one file for one callback, eg: `doSomething($file)`
     *   onFileReject - callback function when files are rejected, because of its file extension does not match the accept regex, eg: `doSomething($file)`
     */

    function fileDrop($parse, nvButtonFilePickerType) {

      return {
          restrict: 'E',
          templateUrl: 'lib/ninja-commons/directives/file-drop/file-drop.directive.html',
          replace: true,
          scope: {
          	label: '@?',
          	disabled: '=?',
            disabledLabel: '@?',
          	accept: '@?',
          },
          link: link
      };

      function link(scope, elem, attr) {
      	scope.label = angular.isDefined(scope.label) ? scope.label : 'Drop it here';
      	scope.disabled = angular.isDefined(scope.disabled) ? scope.disabled : false;
        scope.disabledLabel = angular.isDefined(scope.disabledLabel) ? scope.disabledLabel : 'File drop disabled';
      	scope.accept = angular.isDefined(scope.accept) ? scope.accept : nvButtonFilePickerType.ANY;
      	scope.isDrag = false;
				scope.onFileSelectCB = angular.isDefined(attr.onFileSelect) ? wrapFileChange(attr.onFileSelect) : angular.noop;
				scope.onFileRejectCB = angular.isDefined(attr.onFileReject) ? wrapFileChange(attr.onFileReject) : angular.noop;
				scope.acceptRegex = nvButtonFilePickerType.regex(scope.accept);

      	var elDragView = angular.element(elem);

      	elDragView.bind("dragover", function(e){
    			e.stopPropagation();
      		e.preventDefault();
    			elDragView.addClass('nv-file-drop-drag-hover');
      	});

      	elDragView.bind("dragleave", function(e){
    			e.stopPropagation();
      		e.preventDefault();
    			elDragView.removeClass('nv-file-drop-drag-hover');
      	});

      	elDragView.bind("drop", function(e){
      		e.stopPropagation();
      		e.preventDefault();
      		elDragView.removeClass('nv-file-drop-drag-hover');

          if(scope.disabled){
            return;
          }

      		e = (angular.isObject(e.originalEvent)) ? e.originalEvent : e;

      		var files = e.target.files || e.dataTransfer.files;
      		_.each(files, fileCB);
      	});

				function wrapFileChange(onFileChange) {
					var parsedFileChange = $parse(onFileChange);
						return function(file){
							return parsedFileChange(_.defaults({$file: file}, scope.$parent));
					};
				}

				function fileCB(file){
					if(scope.acceptRegex.test(file.name)){
						scope.onFileSelectCB(file);
					}else{
						scope.onFileRejectCB(file);
					}
				}
      }
    }
})();
