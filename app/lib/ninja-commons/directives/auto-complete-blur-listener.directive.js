(function() {
    'use strict';

    angular
        .module('nvCommons.directives')
        .directive('nvAutoCompleteBlurListener', nvAutoCompleteBlurListener);
    
    nvAutoCompleteBlurListener.$inject = ['$timeout'];
    
    /**
     * directive to set nvAutoCompleteBlurListener attributes into input field in autocomplete
     * as describe in https://github.com/angular/material/issues/3906
     * timeout 0 purpose : http://stackoverflow.com/questions/779379/why-is-settimeoutfn-0-sometimes-useful
     * md autocomplete doesn't have its onBlur listener
     * usage :
     * <md-autocomplete
            (...)
            md-floating-label="Some text"
            nv-auto-complete-blur-listener="yourCallback()">
            (...)
        </md-autocomplete>
     */
    function nvAutoCompleteBlurListener ($timeout) {
        return {
            restrict: 'A',
            link: link
        };

    function link (scope, element, attributes){
        $timeout(onBlur, 0);

        function onBlur(){
            angular.element(element[0].querySelector("input.md-input")).bind("blur", function(){
                $timeout(function() {
                    scope.$eval(attributes.nvAutoCompleteBlurListener);
                }, 100);
            });
        }
    }
}

})();