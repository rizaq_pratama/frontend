(function directive() {
  angular.module('nvCommons.directives')
    .directive('nvFilterNumberBox', nvFilterNumberBoxDirective);

  nvFilterNumberBoxDirective.$inject = ['nvFilterNumberBoxService'];

  function nvFilterNumberBoxDirective(nvFilterNumberBoxService) {
    return {
      restrict: 'E',
      templateUrl: 'lib/ninja-commons/directives/filter-box/filter-number-box/filter-number-box.directive.html',
      scope: {
        mainTitle: '@',
        model: '=',
        numberExtra: '=?',
        onClose: '&?',
        onChange: '&?',
      },
      link: link,
    };

    function link(scope) {
      scope = _.defaults(scope, {onClose: angular.noop, onChange: angular.noop});

      scope.typeOptions = nvFilterNumberBoxService.FILTER_TYPE_OPTIONS;
      scope.operatorOptions = nvFilterNumberBoxService.FILTER_OPERATOR_OPTIONS;
    }
  }
}());
