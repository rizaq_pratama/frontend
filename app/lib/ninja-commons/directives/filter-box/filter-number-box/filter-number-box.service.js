(function service() {
  angular
    .module('nvCommons.directives')
    .factory('nvFilterNumberBoxService', nvFilterNumberBoxService);

  nvFilterNumberBoxService.$inject = [];

  const FILTER_TYPE_OPTIONS = [
    { value: { code: 0, name: 'range' }, displayName: 'Range' },
    { value: { code: 1, name: 'value' }, displayName: 'Value' },
  ];

  const FILTER_OPERATOR_OPTIONS = [
    { value: { code: 0, name: '==' }, displayName: '==' },
    { value: { code: 1, name: '<=' }, displayName: '<=' },
    { value: { code: 2, name: '>=' }, displayName: '>=' },
    { value: { code: 3, name: '<' }, displayName: '<' },
    { value: { code: 4, name: '>' }, displayName: '>' },
  ];

  const FILTER_TYPES = {
    RANGE: FILTER_TYPE_OPTIONS[0],
    VALUE: FILTER_TYPE_OPTIONS[1],
  };

  const FILTER_OPERATORS = {
    EQ: FILTER_OPERATOR_OPTIONS[0],
    ELT: FILTER_OPERATOR_OPTIONS[1],
    EGT: FILTER_OPERATOR_OPTIONS[2],
    LT: FILTER_OPERATOR_OPTIONS[3],
    GT: FILTER_OPERATOR_OPTIONS[4],
  };

  function nvFilterNumberBoxService() {
    return {
      FILTER_TYPE_OPTIONS: FILTER_TYPE_OPTIONS,
      FILTER_OPERATOR_OPTIONS: FILTER_OPERATOR_OPTIONS,
      FILTER_TYPES: FILTER_TYPES,
      FILTER_OPERATORS: FILTER_OPERATORS,
      isPresetValid: isPresetValid,
    };

    function isPresetValid(preset) {
      let isValid = false;

      if (!preset.type) {
        return false;
      }

      const type = _.find(
        FILTER_TYPES,
        t => {
          if (preset.type) {
            return t.value.name.toUpperCase() === preset.type.toUpperCase()
          } else {
            return false;
          }
        });

      if (!type) {
        return false;
      }

      if (type.value.name === FILTER_TYPES.RANGE.value.name) {
        isValid = (!isNaN(preset.from) && !isNaN(preset.to));
      } else if (type.value.name === FILTER_TYPES.VALUE.value.name) {
        const operator = _.find(
          FILTER_OPERATORS,
          op => (op.value.name === preset.operator)
        );
        if (!operator) {
          isValid = false;
        } else {
          isValid = !isNaN(preset.operand);
        }
      }
      return isValid;
    }
  }
}());
