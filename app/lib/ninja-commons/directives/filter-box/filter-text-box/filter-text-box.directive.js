(function directive() {
  angular.module('nvCommons.directives')
    .directive('nvFilterTextBox', nvFilterTextBoxDirective);

  nvFilterTextBoxDirective.$inject = [];

  function nvFilterTextBoxDirective() {
    return {
      restrict: 'E',
      templateUrl: 'lib/ninja-commons/directives/filter-box/filter-text-box/filter-text-box.directive.html',
      scope: {
        mainTitle: '@',
        placeholder: '@?',
        model: '=',
        onClose: '&?',
        isMandatory: '=?',
      },
      link: link,
    };

    function link(scope) {
      scope.placeholder = scope.placeholder || '';
      scope.isMandatory = scope.isMandatory ? scope.isMandatory : false;
    }
  }
}());
