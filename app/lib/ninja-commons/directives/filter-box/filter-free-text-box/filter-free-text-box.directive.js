(function directive() {
  angular.module('nvCommons.directives')
        .directive('nvFilterFreeTextBox', nvFilterFreeTextBox);

  nvFilterFreeTextBox.$inject = [];

  function nvFilterFreeTextBox() {
    return {
      restrict: 'E',
      templateUrl: 'lib/ninja-commons/directives/filter-box/filter-free-text-box/filter-free-text-box.directive.html',
      scope: {
        mainTitle: '@',
        model: '=',
        onClose: '&?',
        onChange: '&?',
        isMandatory: '=?',
      },
      link: link,
    };

    function link(pScope) {
      const scope = _.defaults(pScope, { onClose: angular.noop, model: [], isMandatory: false });
      scope.search = '';
      scope.counter = 0;
      scope.showAll = true;
      scope.onKeypress = function onKeypress($event) {
        if ($event.which === 13) {
          if (scope.search && (scope.search.length > 0)) {
            scope.model.push({
              id: scope.counter,
              displayName: scope.search,
            });
            scope.counter += 1;
            scope.search = '';
          }
        }
      };

      scope.onRemove = function onRemove(id) {
        return _.remove(scope.model, item => item.id === id);
      };

      scope.onClearAll = function onClearAll() {
        scope.model.length = 0;
      };

      scope.toggleState = function toggleState() {
        scope.showAll = !scope.showAll;
      };
    }
  }
}());
