(function directive() {
  angular.module('nvCommons.directives')
    .directive('nvFilterTimeBox', nvFilterTimeBoxDirective);

  nvFilterTimeBoxDirective.$inject = [];

  const HOUR_OPTIONS = [
    { value: { code: 0, name: 0 }, displayName: '00' },
    { value: { code: 1, name: 1 }, displayName: '01' },
    { value: { code: 2, name: 2 }, displayName: '02' },
    { value: { code: 3, name: 3 }, displayName: '03' },
    { value: { code: 4, name: 4 }, displayName: '04' },
    { value: { code: 5, name: 5 }, displayName: '05' },
    { value: { code: 6, name: 6 }, displayName: '06' },
    { value: { code: 7, name: 7 }, displayName: '07' },
    { value: { code: 8, name: 8 }, displayName: '08' },
    { value: { code: 9, name: 9 }, displayName: '09' },
    { value: { code: 10, name: 10 }, displayName: '10' },
    { value: { code: 11, name: 11 }, displayName: '11' },
    { value: { code: 12, name: 12 }, displayName: '12' },
    { value: { code: 13, name: 13 }, displayName: '13' },
    { value: { code: 14, name: 14 }, displayName: '14' },
    { value: { code: 15, name: 15 }, displayName: '15' },
    { value: { code: 16, name: 16 }, displayName: '16' },
    { value: { code: 17, name: 17 }, displayName: '17' },
    { value: { code: 18, name: 18 }, displayName: '18' },
    { value: { code: 19, name: 19 }, displayName: '19' },
    { value: { code: 20, name: 20 }, displayName: '20' },
    { value: { code: 21, name: 21 }, displayName: '21' },
    { value: { code: 22, name: 22 }, displayName: '22' },
    { value: { code: 23, name: 23 }, displayName: '23' },
  ];

  const MINUTE_OPTIONS = [
    { value: { code: 0, name: 0 }, displayName: '00' },
    { value: { code: 1, name: 30 }, displayName: '30' },
  ];

  function nvFilterTimeBoxDirective() {
    nvFilterTimeBoxDirectiveController.$inject = ['$scope', 'nvDateTimeUtils'];

    return {
      restrict: 'E',
      templateUrl: 'lib/ninja-commons/directives/filter-box/filter-time-box/filter-time-box.directive.html',
      scope: {
        mainTitle: '@',
        fromModel: '=',
        toModel: '=',
        onClose: '&?',
        onChange: '&?',
      },
      controller: nvFilterTimeBoxDirectiveController,
    };

    function nvFilterTimeBoxDirectiveController(scope, nvDateTimeUtils) {
      scope = _.defaults(scope, { onClose: angular.noop, onChange: angular.noop, container: {} });
      scope.hourOptions = HOUR_OPTIONS;
      scope.minuteOptions = MINUTE_OPTIONS;
      scope.onTimeChange = onTimeChange;

      init();

      // watch the nv-select model
      scope.$watch('container.fromHour', (val) => {
        if (val) {
          scope.fromModel.hour(val.name);
          validateInput();
        }
      });

      scope.$watch('container.fromMinute', (val) => {
        if (val) {
          scope.fromModel.minute(val.name);
          validateInput();
        }
      });

      scope.$watch('container.toHour', (val) => {
        if (val) {
          scope.toModel.hour(val.name);
          validateInput();
        }
      });

      scope.$watch('container.toMinute', (val) => {
        if (val) {
          scope.toModel.minute(val.name);
          validateInput();
        }
      });

      // call back on date change
      function onTimeChange() {
        onCalendarChange();
        validateInput();
        scope.onChange();
      }

      function onCalendarChange() {
        disableCalendarWatcher();
        const fromMoment = nvDateTimeUtils.toMoment(scope.container.fromDate);
        const toMoment = nvDateTimeUtils.toMoment(scope.container.toDate);

        scope.fromModel.year(fromMoment.year());
        scope.fromModel.month(fromMoment.month());
        scope.fromModel.date(fromMoment.date());

        scope.toModel.year(toMoment.year());
        scope.toModel.month(toMoment.month());
        scope.toModel.date(toMoment.date());
        enableCalendarWatcher();
      }

      function getMinuteFromDate(moment) {
        if (moment) {
          return (moment.minute() >= 30) ? 30 : 0;
        }
        return 0;
      }

      function init() {
        disableCalendarWatcher();
        // reinite state of every input
        scope.container.fromDate = scope.fromModel.toDate();

        // init fromX
        scope.container.fromHour = (_.find(HOUR_OPTIONS, h =>
          h.value.name === scope.fromModel.hour()) || HOUR_OPTIONS[0]).value;

        scope.container.fromMinute = (_.find(MINUTE_OPTIONS, m =>
          m.value.name === getMinuteFromDate(scope.fromModel)) || MINUTE_OPTIONS[0]).value;

        scope.fromModel.second(0);

        // init toX
        scope.container.toDate = scope.toModel.toDate();

        scope.container.toHour = (_.find(HOUR_OPTIONS, h =>
          h.value.name === scope.toModel.hour()) || HOUR_OPTIONS[0]).value;

        scope.container.toMinute = (_.find(MINUTE_OPTIONS,
          m => m.value.name === getMinuteFromDate(scope.toModel)) || MINUTE_OPTIONS[0]).value;
        scope.toModel.second(0);

        enableCalendarWatcher();
      }

      function validateInput() {
        const from = scope.fromModel.unix();
        const to = scope.toModel.unix();

        if (from > to) {
          const temp = scope.fromModel;
          scope.fromModel = scope.toModel;
          scope.toModel = temp;
          init();
        }
      }

      function enableCalendarWatcher() {
        // watch fromModel and toModel to capture change from outside directive
        scope.fromCalendarWatcher = scope.$watch('fromModel', (val, oldVal) => { if (val !== oldVal) { init(); } });
        scope.toCalendarWatcher = scope.$watch('toModel', (val, oldVal) => { if (val !== oldVal) { init(); } });
      }

      function disableCalendarWatcher() {
        if (scope.fromCalendarWatcher && scope.toCalendarWatcher) {
          scope.fromCalendarWatcher();
          scope.toCalendarWatcher();
        }
      }
    }
  }
}());
