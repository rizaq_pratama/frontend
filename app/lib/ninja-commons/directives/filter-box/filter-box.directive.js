(function directive() {
  angular
    .module('nvCommons.directives')
    .directive('nvFilterBox', nvFilterBoxDirective);

  nvFilterBoxDirective.$inject = ['nvAutocomplete.Data'];

  function nvFilterBoxDirective(nvAutocompleteData) {
    nvFilterBoxDirectiveController.$inject = ['$scope'];

    return {
      restrict: 'E',
      templateUrl: 'lib/ninja-commons/directives/filter-box/filter-box.directive.html',
      scope: {
        searchText: '=',
        persistSearchText: '=?',
        possibleOptions: '=', // array of objects like {id: ..., displayName: ..., lowercaseName: ...}
        selectedOptions: '=', // array of objects like {id: ..., displayName: ..., lowercaseName: ...}
        searchBy: '@?',
        sortBy: '@?',
        sortOrder: '@?',
        onChange: '&',
        mainTitle: '@',
        placeholder: '@',
        itemTypes: '@',
        showSelected: '=?',
        buttonContainerPosition: '@?',
        autocompleteHandle: '@?delegateHandle',
        noFoundItemMessage: '@?',
        theme: '@?',
        showSuggestionWhenEmpty: '=?',
        includeModel: '=?',
        onClose: '&?',
        orientation: '@?',
        hideClose: '@?',
        isMandatory: '=?',
      },
      controller: nvFilterBoxDirectiveController,
    };

    function nvFilterBoxDirectiveController(scope) {
      scope.autocompleteHandle = scope.autocompleteHandle || nvAutocompleteData.generateHandle();
      scope.dataService = nvAutocompleteData.getByHandle(scope.autocompleteHandle);
      scope.searchText = scope.searchText || '';
      scope.persistSearchText = scope.persistSearchText || false;
      scope.buttonContainerPosition = scope.buttonContainerPosition || 'bottom'; // 'bottom' or 'top' only
      scope.onChange = scope.onChange || angular.noop;
      scope.searchBy = scope.searchBy || 'displayName';
      scope.sortBy = scope.sortBy || scope.searchBy;
      scope.sortOrder = scope.sortOrder || 'asc';
      scope.hideClose = scope.hideClose || false;
      scope.displayByFn = _.property(scope.searchBy);
      scope.isMandatory = scope.isMandatory ? scope.isMandatory : false;
      scope.state = {
        showSelected: _.isUndefined(scope.showSelected) ? true : scope.showSelected,
      };

      scope.dataService.onChange(onChange);
      scope.dataService.setSortBy(scope.sortBy, scope.sortOrder);
      scope.dataService.setDisplayBy(scope.searchBy);
      scope.dataService.setCallPromiseOnChange(false);
      scope.dataService.setPossibleOptions(scope.possibleOptions);
      scope.dataService.setSelectedOptions(scope.selectedOptions);
      scope.dataService.setMaxResult(_.size(scope.possibleOptions) + _.size(scope.selectedOptions));

      scope.onRemoveItem = (item) => {
        scope.dataService.remove(item);
      };

      scope.onClearAll = () => {
        scope.dataService.removeAll();
      };

      scope.$on('$destroy', () => {
        nvAutocompleteData.removeHandle(scope.autocompleteHandle);
      });

      function onChange(changes) {
        scope.selectedOptions = changes.selectedOptions;
        scope.possibleOptions = changes.possibleOptions;
        scope.onChange(changes);
      }
    }
  }
}());
