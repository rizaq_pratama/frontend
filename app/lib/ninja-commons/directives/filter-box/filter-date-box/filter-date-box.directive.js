(function directive() {
  angular.module('nvCommons.directives')
    .directive('nvFilterDateBox', nvFilterDateBoxDirective);

  nvFilterDateBoxDirective.$inject = [];

  function nvFilterDateBoxDirective() {
    return {
      restrict: 'E',
      templateUrl: 'lib/ninja-commons/directives/filter-box/filter-date-box/filter-date-box.directive.html',
      scope: {
        mainTitle: '@',
        fromModel: '=',
        toModel: '=',
        onClose: '&?',
        onChange: '&?',
        isRange: '=?',
        maxDateRange: '@?',
        isMandatory: '=?',
        form: '=?',
        minDate: '=?',
      },
      link: link,
    };

    function link(scope) {
      scope = _.defaults(scope, {
        onClose: angular.noop,
        onChange: angular.noop,
        isRange: true,
        isMandatory: false,
        currentDate: new Date(),
        maxDateRange: 0,
        minDate: null,
      });

      setMaxDateModel();

      scope.onDateChanged = onDateChanged;

      // functions
      function onDateChanged() {
        setMaxDateModel();
        validateDateRange();
        scope.onChange();
      }

      function validateDateRange() {
        const maxDateRange = _.toInteger(scope.maxDateRange);
        if (scope.isRange && maxDateRange > 0) {
          const bool = moment(scope.toModel).diff(scope.fromModel, 'days') < maxDateRange;

          if (_.size(scope.form) > 0) {
            if (!bool) {
              scope.form.toDateField.$setValidity('maxdate', false);
            } else {
              scope.form.toDateField.$setValidity('maxdate', true);
            }
          }

          return bool;
        }

        return true;
      }

      function setMaxDateModel() {
        const maxDateRange = _.toInteger(scope.maxDateRange);
        if (scope.isRange && maxDateRange > 0) {
          scope.toMaxDateModel = moment(scope.fromModel).add(maxDateRange - 1, 'day').toDate();
        }
      }
    }
  }
}());
