(function directive() {
  angular.module('nvCommons.directives')
    .directive('nvFilterRangeBox', nvFilterRangeBoxDirective);

  nvFilterRangeBoxDirective.$inject = [];

  function nvFilterRangeBoxDirective() {
    return {
      restrict: 'E',
      templateUrl: 'lib/ninja-commons/directives/filter-box/filter-range-box/filter-range-box.directive.html',
      scope: {
        mainTitle: '@',
        fromModel: '=',
        toModel: '=',
        onClose: '&?',
        unit: '@?',
      },
      link: link,
    };

    function link(scope) {
      scope = _.defaults(scope, { 
        onClose: angular.noop, 
        fromModel : 0, 
        toModel: 1.0,
        unit: '',
       });
    }
  }
}());
