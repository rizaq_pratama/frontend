(function directive() {
  angular.module('nvCommons.directives')
        .directive('nvFilterBooleanBox', nvFilterBooleanBoxDirective);

  nvFilterBooleanBoxDirective.$inject = ['$compile'];

  function nvFilterBooleanBoxDirective($compile) {
    return {
      restrict: 'E',
      scope: {
        mainTitle: '@',
        booleanModel: '=',
        onClose: '&?',
        trueLabel: '@?',
        falseLabel: '@?',
        filter: '=?',
        isMandatory: '=?',
      },
      link: link,
    };

    function link(scope, ele) {
      scope = _.defaults(scope, { onClose: angular.noop });
      scope.trueLabel = scope.trueLabel !== '' ? scope.trueLabel : 'Show';
      scope.falseLabel = scope.falseLabel !== '' ? scope.falseLabel : 'Hide';
      scope.isMandatory = scope.isMandatory ? scope.isMandatory : false;
      const html =
            `<div layout="row" class="header-container" layout-align="start center">
	        <div class="main-title">
		        <p class="medium nv-text-ellipsis">{{ mainTitle | translate }}</p>
	        </div>

	        <nv-btn-grp
		        class="boolean-container flex"
		        model="booleanModel"
		        label-for-model-idx-0="${scope.trueLabel}"
		        label-for-model-idx-1="${scope.falseLabel}"
		        show-selected="false"
		        select-multiple="false"
		        select-none="false">
	        </nv-btn-grp>

	        <div class="primary-button-container" flex layout="row" layout-align="end center"
	             ng-if="!isMandatory">
		        <nv-icon-button name="commons.remove-filter" class="flat dense" icon="close" on-click="::onClose()">
		        </nv-icon-button>
	        </div>
            </div>`;
      const el = $compile(html)(scope);
      ele.append(el);
    }
  }
}());
