(function directive() {
  angular.module('nvCommons.directives')
    .directive('nvFilterAutocomplete', nvFilterAutocompleteDirective);

  nvFilterAutocompleteDirective.$inject = ['nvAutocomplete.Data'];

  function nvFilterAutocompleteDirective(nvAutocompleteData) {
    controller.$inject = ['$scope'];

    return {
      restrict: 'E',
      templateUrl: 'lib/ninja-commons/directives/filter-box/filter-autocomplete/filter-autocomplete.directive.html',
      scope: {
        mainTitle: '@',
        itemTypes: '@',
        selectedOptions: '=',
        callback: '=',
        displayBy: '@?',
        sortBy: '@?',
        onChange: '&',
        onClose: '&?',
        autocompleteHandle: '@?delegateHandle',
        noFoundItemMessage: '@?',
        theme: '@?',
        minTextLength: '@?',
        hideClose: '@?',
        isMandatory: '=?',
      },
      controller: controller,
    };

    function controller(scope) {
      scope.autocompleteHandle = scope.autocompleteHandle || nvAutocompleteData.generateHandle();
      scope.dataService = nvAutocompleteData.getByHandle(scope.autocompleteHandle);
      scope.searchText = scope.searchText || '';
      scope.onChange = scope.onChange || angular.noop;
      scope.displayBy = scope.displayBy || 'displayName';
      scope.sortBy = scope.sortBy || scope.displayBy;
      scope.minTextLength = scope.minTextLength || 3;
      scope.hideClose = scope.hideClose || false;
      scope.isMandatory = scope.isMandatory ? scope.isMandatory : false;
      scope.displayByFn = _.property(scope.displayBy);
      scope.state = {
        showSelected: true,
      };

      scope.dataService.onChange(onChange);
      scope.dataService.setSortBy(scope.sortBy);
      scope.dataService.setDisplayBy(scope.displayBy);
      scope.dataService.setPossibleOptions(scope.callback);
      scope.dataService.setSelectedOptions(scope.selectedOptions);

      scope.onRemoveItem = (item) => {
        scope.dataService.remove(item);
      };

      scope.onClearAll = () => {
        scope.dataService.removeAll();
      };

      scope.$on('$destroy', () => {
        nvAutocompleteData.removeHandle(scope.autocompleteHandle);
      });

      function onChange(changes) {
        scope.selectedOptions = changes.selectedOptions;
        scope.possibleOptions = changes.possibleOptions;
        scope.onChange(changes);
      }
    }
  }
}());
