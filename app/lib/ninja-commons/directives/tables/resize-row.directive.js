(function() {
    'use strict';

    angular
        .module('nvCommons.directives')
        .directive('nvResizeRow', resizeRow);

    resizeRow.$inject = ['$window', '$timeout'];

    function resizeRow($window, $timeout) {

        return {
            restrict: 'A',
            link: link
        };

        function link(scope, elem, attrs) {
            var w = angular.element($window),
                e = elem[0];
            var rows = e.querySelector('tbody').children;
            var clazz = '.' + attrs.nvResizeRow;

            w.bind('resize', onResize); // listen to the window resize event
            scope.$on('$destroy', function() {
                w.unbind('resize', onResize); // clean up
            });

            scope.$watch(function() {
                return e.offsetWidth + e.offsetHeight;
            }, function() {
                _.forEach(rows, function(row) {
                    var columns = row.querySelectorAll(clazz),
                        htValue = row.offsetHeight + 'px';
                    _.forEach(columns, function(column) {
                        column.style.height = htValue;
                    });
                });
            });

            function onResize() {
                $timeout(angular.noop); // force a $digest loop so that the $watch will be triggered
            }
        }

    }

})();
