(function directive() {
  angular
    .module('nvCommons.directives')
    .directive('nvTable', nvTableDirective);

  /**
   * <nv-table> directive
   *
   * @param {obj} param
   *        table param created through nvTable service
   *
   * @param {number} distance
   *        The distance from the bottom that the scroll
   *        must reach to trigger the on-infinite-scroll expression.
   *        Default 0.1
   *
   * @param {expression} onInfiniteScroll
   *        What to call when the scroller reaches the bottom.
   *        Must return a promise. returns a rejected promise to
   *        indicate that the table has reach its end.
   *        Default $q.reject
   *
   * @param {boolean} selectable
   *        true or false, specify whether to show a checkbox at the rightmost of
   *        the table.
   * 
   * @param {array} additionalMenu
   *        array of object that contain the additional menu that will be showed at the table
   *        {
   *          label: 'theLabel',
   *          onClick: callBackFn
   *        }
   *
   * ----------------------------------------------------------------------------
   *
   * <nv-table-row> directive
   *
   * @param {string} use
   *        Variable name of the object model bound to each row.
   *
   * @param {number} rowHeight
   *        Specify a fixed height for each row, use in conjunction with
   *        @include nv-table-row-height(height). This will fix the virtual-repeat
   *        dynamic height issue, if you know the row height ahead of time. It will
   *        also disable the auto-expanding and shrinking of the <nv-action>.
   *
   * @param {boolean} useVirtualRepeat
   *        Default true, choose whether need to use md-virtual-repeat-container
   *        directive or not
   *
   * ----------------------------------------------------------------------------
   *
   * <nv-index> directive
   *
   * Auto-increment index column, will always appear at the leftmost of the table.
   *
   * ----------------------------------------------------------------------------
   *
   * <nv-td> directive
   *
   * By default, nv-td will show a filter input box, and is sortable
   *
   * @param {string} column Attribute name of the column, use for sorting, and header display name
   * @param {boolean} sortable Specify whether the column is sortable, default true
   * @param {boolean} filter Specify whether the column should show a filter box, default true
   *
   * -------------------------------------------------------------------------------
   *
   * <nv-action> directive
   *
   * By default, nv-action will not show a filter input box, and is not sortable
   *
   * @param {string} column Attribute name of the column, use for sorting, and header display name
   * @param {boolean} sortable Specify whether the column is sortable, default false
   * @param {boolean} filter Specify whether the column should show a filter box, default false
   *
   *
   *
   */

  nvTableDirective.$inject = ['$filter', '$timeout', '$q', 'nvTable'];

  function nvTableDirective($filter, $timeout, $q, nvTable) {
    const nvCamelCase2HyphenCase = $filter('nvCamelCase2HyphenCase');
    const INFINITE_IDLE = 0;
    const INFINITE_LOADING = 1;
    const INFINITE_ENDED = 2;

    return {
      restrict: 'E',
      scope: {
        table: '=param',
        distance: '=?',
        onInfiniteScroll: '&?',
        controllerAs: '=',
        additionalMenu: '=?',
      },
      template: template,
      link: link,
    };

    function template(elem, attr) {
      const tableRow = elem.find('nv-table-row');
      const isSelectable = attr.selectable === 'true';
      const showIndex = tableRow.find('nv-index').length === 1;
      const tableData = tableRow.find('nv-td');
      const tableAction = tableRow.find('nv-action');
      const alias = tableRow.attr('use');
      const rowHeight = tableRow.attr('row-height');
      const useVirtualRepeat = tableRow.attr('use-virtual-repeat') !== 'false';
      const hasFilter = _.some(tableData, hasFilterFn) ||
        _.some(tableAction, _.partial(hasFilterFn, _, true));

      return `<div class="table-with-locks-container ${isSelectable ? 'with-checkbox' : ''} ${hasFilter ? 'has-filter' : ''} ${useVirtualRepeat ? '' : 'no-virtual-repeat'}">
                <div class="table-head-container ${hasFilter ? 'has-filter' : ''}">
                  <table class="table-head nv-text-no-select">
                    <thead>
                      <tr>
                      ${showIndex ? `<th class="column-index">
                        {{ 'commons.number-shortform' | translate }}
                        </th>` :
                      ''}
                      ${getTableHeaderHTML(tableData)}
                      ${getTableHeaderHTML(tableAction, true)}
                      ${isSelectable ? `<th class="column-checkbox" ng-class="{ 'show-selected': selection.isShownOnlySelected }">
                        <md-menu md-position-mode="target-right target">
                          <md-button
                            class="nv-button ghost dense custom"
                            ng-class="{ 'show-selected': selection.isShownOnlySelected, 'has-selected': table.getSelectionCount() > 0 }"
                            aria-label="{{'directive.table.selection' | translate}}"
                            layout="row"
                            layout-align="center center"
                            ng-click="::$mdOpenMenu($event)">
                            <i class="material-icons">{{selection.isShownOnlySelected ? 'check_box': 'arrow_drop_down'}}</i>
                          </md-button>
                          <md-menu-content>
                            <md-menu-item>
                              <md-button 
                                class="nv-button custom" 
                                aria-label="{{'directive.table.select-all-shown' | translate }}"
                                ng-click="selection.selectAllShown()">
                                <span flex="1" translate="directive.table.select-all-shown"></span>
                              </md-button>
                            </md-menu-item>
                            <md-menu-item>
                              <md-button 
                                class="nv-button custom" 
                                aria-label="{{'directive.table.deselect-all-shown' | translate }}"
                                ng-click="selection.deselectAllShown()">
                                <span flex="1" translate="directive.table.deselect-all-shown"></span>
                              </md-button>
                            </md-menu-item>
                            <md-menu-item>
                              <md-button 
                                class="nv-button custom" 
                                aria-label="{{'directive.table.clear-selection' | translate }}"
                                ng-click="selection.clearSelection()">
                                <span flex="1" translate="directive.table.clear-selection"></span>
                              </md-button>
                            </md-menu-item>
                            <md-menu-item ng-repeat="menu in additionalMenu">
                              <md-button 
                                class="nv-button custom" 
                                aria-label="{{menu.label | translate}}"
                                ng-click="menu.onClick()">
                                <span flex="1" >{{menu.label | translate}}</span>
                              </md-button>
                            </md-menu-item>
                            <md-menu-item
                              class="custom"
                              ng-class="{ 'has-selected': table.getSelectionCount() > 0 }">
                              <md-button 
                                class="nv-button custom"
                                aria-label="{{'directive.table.show-only-selected' | translate }}"
                                ng-click="selection.toggleShowOnlySelected()">
                                <i class="material-icons">{{selection.isShownOnlySelected ? 'check_box': 'check_box_outline_blank'}}</i>
                                <span flex="1" translate="directive.table.show-only-selected"></span>
                              </md-button>
                            </md-menu-item>
                          </md-menu-content>
                        </md-menu>
                      </th>` : ''}
                      </tr>
                    </thead>
                  </table>
                  <span class="table-checkbox-column-divider column-divider"></span>
                  ${getColumnShadowDividerHTML()}
                  ${getColumnDividerHTML()}
                </div>
                ${useVirtualRepeat ? '<md-virtual-repeat-container>' : '<div class="no-virtual-repeat-container">'}
                  <table class="table-body" ng-class="{ 'noStripe': !table.isStripped }">
                    <tbody>
                      <tr
                        class="{{ table.highlight(${alias}).class }}"
                        ${useVirtualRepeat ? 'md-virtual-repeat' : 'ng-repeat'}="${alias} in getTableData()"
                        ${rowHeight ? `md-item-size="${rowHeight}"` : ''}
                        ng-class="{ even: ($index % 2 === 0), odd: ($index % 2 !== 0), 'is-selected': (table.isSelected(${alias})),
                          'highlight': (table.highlight(${alias}).highlighted),'highlight-new': ${alias}._isNew,
                          'last-row': (!selection.isShownOnlySelected && $index === table.getLength()),
                          'last-row-no-result': table.getLength() === 0 }">
                        <td class="table-row-divider"></td>
                        ${showIndex ? '<td class="column-index">{{ $index + 1}}</td>' : ''}
                        ${getTableDataHTML(tableData)}
                        ${getTableDataHTML(tableAction, true)}
                        ${isSelectable ?
                          `<td class="column-checkbox"
                            ${!rowHeight ? `nv-table-action-td="${alias}"` : ''}
                            >
                            <md-checkbox class="check-box md-primary"
                              aria-label="Select"
                              ng-click="table.toggleSelect(${alias})"
                              ng-checked="table.isSelected(${alias})"
                              md-theme="nvBlue">
                            </md-checkbox>
                          </td>` :
                           ''}
                        <td class="end-of-result" ng-if="!selection.isShownOnlySelected && $index === table.getLength()">
                          ${getLoadingState()}
                          ${getEndStateFilter()}
                          ${getEndState()}
                          ${getBlankState()}
                        </td>
                      </tr>
                    </tbody>
                  </table>
                  <span class="table-checkbox-column-divider column-divider"></span>
                  ${getColumnShadowDividerHTML()}
                  ${getColumnDividerHTML()}
                ${useVirtualRepeat ? '</md-virtual-repeat-container>' : '</div>'}
                <div class="table-horizontal-scroller">
                  <div class="table-horizontal-scroller-content"></div>
                </div>
                <div class="table-horizontal-scroller-end"></div>
                <div class="table-vertical-scroller-end"></div>
                <div class="table-vertical-scroller">
                  <div class="table-vertical-scroller-content"></div>
                </div>
              </div>`;

      function getTableHeaderHTML(data, action = false) {
        return _(data).map(_.partial(mapToHeaderHTML, action)).join('');
      }
      function getTableDataHTML(data, action = false) {
        return _(data).map(_.partial(mapToDataHTML, action)).join('');
      }
      function getColumnShadowDividerHTML() {
        return ((tableAction.length) > 0 || isSelectable) ? '<span class="table-column-shadow-divider column-divider"></span>' : '';
      }
      function getColumnDividerHTML() {
        return '<span class="table-column-divider column-divider"></span>'.repeat(tableAction.length);
      }
      function mapToHeaderHTML(isAction, element, index) {
        const e = angular.element(element);
        const columnName = e.attr('column');
        const showIf = _.isUndefined(e.attr('show-if')) ? true : e.attr('show-if');
        const ngClick = `ng-click="::table.sort('${columnName}')"`;
        const sortable = (!isAction && (e.attr('sortable') !== 'false')) || (e.attr('sortable') === 'true');
        const filterable = hasFilterFn(element, isAction);

        return `<th ng-show="${showIf}"  
              class="${getClassName(columnName)} ${isAction ? 'column-locked-right' : ''}"
              column="${columnName}"
              ${filterable ? `nv-table-filter="${columnName}"` : ''}
              ng-class="{'active': table.isSortActive('${columnName}')}"
              ${sortable ? ngClick : ''}>
            ${sortable ? `<md-icon md-svg-src="{{table.showSortIcon('${columnName}')}}" aria-label="sort"></md-icon>` : ''}
              <span>{{ (table.getTableHeaderDisplayName('${columnName}')) | translate }}</span>
            ${filterable ?
            `<nv-search-input-filter
              search-text="filter.${columnName}"
              placeholder="Find..."
              on-change="refreshData()"
              ng-click="$event.stopPropagation()"
              tab-index="${index + 10}"
              type="bar">
            </nv-search-input-filter>` : ''}
          </th>`;
      }
      function mapToDataHTML(isAction, element) {
        const e = angular.element(element);
        const columnName = e.attr('column');
        const showIf = _.isUndefined(e.attr('show-if')) ? true : e.attr('show-if');
        const classes = e.attr('class');
        const ngClasses = e.attr('ng-class');
        const nested = e.children().length > 0;
        const filterable = hasFilterFn(element, isAction);

        let enableHighlightText = !isAction && nested && filterable;
        if (!_.isUndefined(e.attr('highlight-text'))) {
          // override highlight text functionality if value is defined
          enableHighlightText = e.attr('highlight-text') === 'true';
        }

        if (enableHighlightText) {
          e.find('*')
          .attr('nv-table-highlight', `filter.${columnName}`);
        }
        return `<td 
                  ng-show="${showIf}"  
                  class="${getClassName(columnName)} ${isAction ? 'column-locked-right' : ''} ${classes || ''}"
                  ${ngClasses ? `ng-class="${ngClasses}"` : ''}
                  ${isAction && !rowHeight ? `nv-table-action-td="${alias}"` : ''}
                  ${!isAction && !nested ? `nv-table-highlight="filter.${columnName}"` : ''}>${e.html()}</td>`;
      }
      function getLoadingState() {
        return `<div class="load-more-data" ng-if="infiniteScrollState !== ${INFINITE_ENDED}">
                  <md-progress-linear md-mode="indeterminate"></md-progress-linear>
                  <h5>{{'directive.table.load-more-data' | translate}}</h5>
                </div>`;
      }
      function getEndStateFilter() {
        return `<div class="end-of-filter" ng-if="infiniteScrollState === ${INFINITE_ENDED} && table.getLength() > 0 && table.hasFiltered()">
                  <h5>{{'directive.table.end-of-filter' | translate}}</h5>
                  <p ng-if="table.showEndTableDescription">{{'directive.table.end-of-filter-desc' | translate}}</p>
                </div>`;
      }
      function getEndState() {
        return `<div class="end-of-table" ng-if="infiniteScrollState === ${INFINITE_ENDED} && table.getLength() > 0 && !table.hasFiltered()">
                  <h5>{{'directive.table.end-of-table' | translate}}</h5>
                  <p ng-if="table.showEndTableDescription">{{'directive.table.end-of-table-desc' | translate}}</p>
                </div>`;
      }
      function getBlankState() {
        return `<div class="no-result" ng-if="infiniteScrollState === ${INFINITE_ENDED} && table.getLength() === 0">
                  <h5>{{'directive.table.no-result' | translate}}</h5>
                  <p ng-if="table.showEndTableDescription">{{'directive.table.no-result-desc-1' | translate}}<br>{{'directive.table.no-result-desc-2' | translate}}</p>
                </div>`;
      }
      function getClassName(columnName) {
        return nvCamelCase2HyphenCase(columnName).replace(/\./g, '-');
      }
      function hasFilterFn(element, isAction) {
        const e = angular.element(element);
        return (!isAction && e.attr('filter') !== 'false') ||
          (!!isAction && e.attr('filter') === 'true');
      }
    }

    function link(scope, elem, attr) {
      const table = $(elem);

      const horizontalScroller = table.find('.table-horizontal-scroller');
      const horizontalScrollerContent = table.find('.table-horizontal-scroller-content');
      const tableHead = table.find('.table-head');
      const tableBody = table.find('.table-body');

      const HEAD = 1;
      const BODY = 2;
      const HSCROLLBAR = 3;
      let horizontalScrollingElem = 0;
      let horizontalTimeout = null;

      scope.filter = {};
      initFilterInput(table.find('th[nv-table-filter]'));

      scope.getTableData = getTableData;
      scope.refreshData = refreshData;

      // infinite scroll
      scope.distance = scope.distance || 0.02;
      scope.onInfiniteScroll = scope.onInfiniteScroll || $q.reject;
      scope.infiniteScrollState = INFINITE_IDLE;
      scope[attr.controllerAs] = scope.controllerAs;

      // selection
      scope.selection = {
        selectAllShown: scope.table.select,
        deselectAllShown: scope.table.deselect,
        clearSelection: scope.table.clearSelect,
        toggleShowOnlySelected: toggleShowOnlySelected,
        isShownOnlySelected: false,
      };

      // synchronize scrolling
      $timeout(() => {
        initLastRowExpanding();
        syncHorizontalScroll();
        syncVerticalScroll();
      }, 500, false); // no need trigger unnecessary digest cycle

      function getTableData() {
        if (scope.selection.isShownOnlySelected) {
          return scope.table.getSelection();
        }
        return _.concat(scope.table.getTableData(), ['end']);
      }

      function syncHorizontalScroll() {
        // make the content within horizontal scroller the same width as tablehead
        const tableRowWidth = tableHead.find('thead').width();
        horizontalScrollerContent.width(tableRowWidth);
        // Listen to scoll events, and sync them: table-head, table-body, horizontal-scroller
        tableHead.on('scroll', hScrollFunction(HEAD, tableHead, $.merge($(tableBody), horizontalScroller)));
        tableBody.on('scroll', hScrollFunction(BODY, tableBody, $.merge($(tableHead), horizontalScroller)));
        horizontalScroller.on('scroll', hScrollFunction(HSCROLLBAR, horizontalScroller, $.merge($(tableHead), tableBody)));
        function hScrollFunction(elementName, thisElement, otherElement) {
          return () => {
            hScroll(elementName, thisElement, otherElement);
          };
        }
      }

      function hScroll(elementName, thisElement, otherElement) {
        if (horizontalScrollingElem > 0 && horizontalScrollingElem !== elementName) {
          return;
        }
        otherElement.scrollLeft(thisElement.scrollLeft());
        $timeout.cancel(horizontalTimeout);
        horizontalTimeout = $timeout(stopHorizontal, 250, false);
        horizontalScrollingElem = elementName;
        // check if horizontal scroll reach the end
        if (thisElement.scrollLeft() + thisElement.innerWidth() >= thisElement[0].scrollWidth) {
          table.addClass('scroll-x-end');
        } else {
          table.removeClass('scroll-x-end');
        }

        function stopHorizontal() {
          horizontalScrollingElem = 0;
        }
      }

      function syncVerticalScroll() {
        const verticalScroller = table.find('.table-vertical-scroller');
        const verticalScrollerContent = table.find('.table-vertical-scroller-content');
        const virtualScroller = table.find('.md-virtual-repeat-scroller');
        const virtualScrollResizer = table.find('.md-virtual-repeat-sizer');
        const virtualScrollOffseter = table.find('.md-virtual-repeat-offsetter');

        const VIRTUAL = 1;
        const VSCROLLBAR = 2;
        let verticalScrollingElem = 0;
        let verticalTimeout = null;
        // make the content within vertical scroller the same height as virtualScrollResizer
        scope.$watch('table.tableData.length', () => {
          $timeout(() => {
            verticalScrollerContent.height(virtualScrollResizer.height());
            checkScrollTop();
          }, 100, false);
        });
        // Listen to scoll events, and sync them: virtual-scroll, vertical-scroller
        verticalScroller.on('scroll', vScrollFunction(VSCROLLBAR, verticalScroller, virtualScroller));
        virtualScroller.on('scroll', vScrollFunction(VIRTUAL, virtualScroller, verticalScroller));
        function stopVertical() {
          verticalScrollingElem = 0;
        }
        function vScrollFunction(elementName, thisElement, otherElement) {
          return () => {
            if (verticalScrollingElem > 0 && verticalScrollingElem !== elementName) {
              return;
            }
            const scrollTop = thisElement.scrollTop();
            otherElement.scrollTop(scrollTop);
            $timeout.cancel(verticalTimeout);
            verticalTimeout = $timeout(stopVertical, 250, false);
            verticalScrollingElem = elementName;
            // check the scroll top pass the infinite-scroll load mark
            checkScrollTop();
          };
        }

        function checkScrollTop() {
          if (scope.selection.isShownOnlySelected) {
            return;
          }
          const lastRowTop = (offSetterTransformY() - virtualScroller.scrollTop() - 140)
                               + virtualScrollOffseter.height();
          const scrollerHeight = virtualScroller.height();
          const scrollTop = virtualScroller.scrollTop() + virtualScroller.height();
          const height = virtualScrollResizer.height();
          if (scope.infiniteScrollState === INFINITE_IDLE &&
              lastRowTop <= scrollerHeight &&
              scrollTop >= (height * (1 - scope.distance)) - 140) {
            scope.infiniteScrollState = INFINITE_LOADING;
            scope.onInfiniteScroll()
            .then(() => {
              scope.infiniteScrollState = INFINITE_IDLE;
              // if it is out of focus, don't keep loading
              // * when document is out of foucs, it might not render the elements
              // causing the height to increase, and cause the table to load more
              if (document.hasFocus()) {
                $timeout(checkScrollTop);
              } else {
                // run once and remove event handler
                $(window).one('focus', () => {
                  $timeout(checkScrollTop);
                });
              }
            }, () => {
              scope.infiniteScrollState = INFINITE_ENDED;
            });
          }
        }
        function offSetterTransformY() {
          if (virtualScrollOffseter.length <= 0) {
            return 0;
          }

          const match = virtualScrollOffseter.css('transform').match(/^matrix\([0-9, ]*, ([0-9]+)\)$/);
          return (match && parseInt(match[1], 10)) || 0;
        }
      }

      function refreshData() {
        $timeout(() => {
          scope.table.refreshData();

          // after refresh table data, have to sync horizontal scroll
          $timeout(() => {
            hScroll(HEAD, tableHead, $.merge($(tableBody), horizontalScroller));
          });
        });
      }

      function initFilterInput(headers) {
        const prefilter = scope.table.getPrefilter();
        _.each(headers, (header) => {
          const headerElement = angular.element(header);
          const propertyName = headerElement.attr('nv-table-filter');
          _.set(scope.filter, propertyName, prefilter[propertyName] || '');
          const strFn = (() => _.get(scope.filter, propertyName, ''));
          scope.table.addColumnFilter(propertyName,
            nvTable.getBasicFilterFunction(propertyName, strFn),
            strFn);
        });
      }

      function toggleShowOnlySelected() {
        scope.selection.isShownOnlySelected = !scope.selection.isShownOnlySelected;
      }

      function initLastRowExpanding() {
        // create css tag
        const styleTag = $('<style>');
        table.append(styleTag);
        const styleSheet = styleTag[0].sheet;
        // calculate width
        const lastColumn = table.find('th:not(.column-locked-right):not(.column-checkbox)').last();
        const lastColumnName = lastColumn.attr('column');
        const lastColumnWidth = lastColumn.outerWidth();
        const tbodyWidth = table.find('thead').width();
        const tableSpace = table.find('table.table-body');
        resize();
        $(window).resize(() => {
          $timeout(resize, 500, false);
        });
        // resize last column function
        function resize() {
          const totalSpace = tableSpace.width();
          if (totalSpace > tbodyWidth) {
            const newWidth = lastColumnWidth + (totalSpace - tbodyWidth);
            if (styleSheet.rules.length > 0) {
              styleSheet.deleteRule(0);
            }
            styleSheet.insertRule(`td.${lastColumnName}, th.${lastColumnName} { 
                width: ${newWidth}px !important; 
                max-width: ${newWidth}px !important;
              }`, 0);
            table.addClass('scroll-x-end');
          } else {
            table.removeClass('scroll-x-end');
          }
        }
      }
    }
  }
}());
