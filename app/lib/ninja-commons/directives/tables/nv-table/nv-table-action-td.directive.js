(function directive() {
  /**
   * This directive is to make the action <td> of the <nv-table>
   * to have the same height as the other <td> in the same <tr>.
   * This is because we absolute positioned the locked-cells on the right,
   * and when the <td> on the left grows in height to accommodate the content,
   * the locked-cells td did not grow in height together.
   * 
   * So a watcher is set to watch the data change, and set the height of this <td>
   * to be the same as the parent <tr> height.
   */

  angular
    .module('nvCommons.directives')
    .directive('nvTableActionTd', nvTableActionTd);

  nvTableActionTd.$inject = ['$timeout'];

  function nvTableActionTd($timeout) {
    return {
      restrict: 'A',
      link: link,
      scope: {
        data: '=nvTableActionTd',
      },
    };

    function link(scope, elem) {
      const element = $(elem);
      const parent = element.parent();
      scope.$watch('data', () => {
        // no need trigger unnecessary digest cycle
        $timeout(() => setRowHeight(element, parent), 0, false);
      });
    }

    function setRowHeight(element, parent) {
      if(element.outerHeight() < parent.outerHeight()) {
        element.outerHeight(parent.outerHeight());        
      }
      if(element.outerHeight() > parent.outerHeight()) {
        parent.outerHeight(element.outerHeight());
      }
    }
  }
}());
