(function directive() {
  angular
    .module('nvCommons.directives')
    .directive('nvTableHighlight', nvTableHighlight);

  nvTableHighlight.$inject = ['$interpolate', '$parse'];

  function nvTableHighlight($interpolate, $parse) {
    return {
      terminal: true,
      restrict: 'A',
      compile: compile,
    };

    function compile(elem, attr) {
      const unsafeTextExpr = $interpolate(elem.html());
      const parsedTerm = $parse(attr.nvTableHighlight);

      return function link(scope, element) {
        let text = null;
        let regex = null;
        let prevTerm = null;

        const debouncedRehighlight = _.debounce(rehighlight, 50);

        const watcher = scope.$watch($scope => ({
          term: parsedTerm($scope),
          unsafeText: unsafeTextExpr($scope).trim(),
        }),
          hide,
          true);

        function hide(state) {
          element.text(state.unsafeText);
          debouncedRehighlight(state);
        }

        function rehighlight(state) {
          if (!_.isEmpty(state.term)) {
            text = state.unsafeText;
            if (regex === null || state.term !== prevTerm) {
              regex = new RegExp(sanitize(state.term), 'i');
            }
            element.html(text.replace(regex, '<span class="highlight">$&</span>'));
          }
          prevTerm = state.term;
        }

        function sanitize(term) {
          return term && term.replace(/[\\\^\$\*\+\?\.\(\)\|\{}\[\]]/g, '\\$&');
        }

        element.on('$destroy', watcher);
      };
    }
  }
}());
