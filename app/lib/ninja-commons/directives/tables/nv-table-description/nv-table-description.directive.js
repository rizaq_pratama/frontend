(function directive() {

  angular
    .module('nvCommons.directives')
    .directive('nvTableDescription', nvTableDescription);

  nvTableDescription.$inject = [];

  function nvTableDescription() {
    return {
      restrict: 'E',
      scope: {
        param: '=tableParam',
        filters: '=filters',
        titleDisplay: '@?',
        descriptionDisplay: '@?',
      },
      templateUrl: 'lib/ninja-commons/directives/tables/nv-table-description/nv-table-description.directive.html',
      link: link
    };

    function link(scope) {
      scope.titleDisplay = scope.titleDisplay || 'title';
      scope.descriptionDisplay = scope.descriptionDisplay || 'description';

      scope.getTitle = _.property(scope.titleDisplay);
      scope.getDescription = _.property(scope.descriptionDisplay);
    }
  }
}());
