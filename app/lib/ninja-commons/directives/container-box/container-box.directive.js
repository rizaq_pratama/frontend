(function() {
    'use strict';

    angular
        .module('nvCommons.directives')
        .directive('nvContainerBox', containerBox);

    function containerBox() {

        return {
            restrict: 'E',
            templateUrl: 'lib/ninja-commons/directives/container-box/container-box.directive.html',
            transclude: true,
            replace: false,
            scope: {
                label: '@'
            }
        };

    }

})();
