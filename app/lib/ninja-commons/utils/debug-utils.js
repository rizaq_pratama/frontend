(function utils(window, document, angular) {
  /**
   * This file defines nv-namespace debug methods on the window object.
   * This is useful for running debug commands / utility methods from the console.
   */
  const nvu = window.nvu = {};
  nvu.debug = {

    /**
     * Retrieves an angular service.
     * @param serviceName - The name of the service to retrieve.
     * @returns {*} The retrieved angular service.
     */
    getService: function getService(serviceName) {
      return angular.element(document.body).injector().get(serviceName);
    },

    /**
     * Temporarily changes the language.
     * Changes are reverted when the page is refreshed.
     * @param language - The language code to change to, e.g. 'en', 'id', etc.
     */
    changeLanguage: function changeLanguage(language) {
      nvu.debug.getService('$timeout')(() => {
        nvu.debug.getService('$translate').use(language);
      });
    },

  };
}(window, document, angular));
