(function operatorMessageModel() {
  angular
    .module('nvCommons.models')
    .factory('OperatorMessage', OperatorMessage);

  OperatorMessage.$inject = ['$http', 'nvRequestUtils'];

  function OperatorMessage($http, nvRequestUtils) {
    return {
      searchAll: (opt) => {
        const options = _.defaults(opt || {}, {
          all: true,
          page: 0,
          itemsPerPage: 0,
        });

        const params = [
          options.all ? 'all=true' : '',
          `page=${options.page}`,
          `itemsPerPage=${options.itemsPerPage}`,
        ];

        return $http
          .get(`driver/1.0/drivers/all/messages?${_.compact(params).join('&')}`)
          .then(nvRequestUtils.nvSuccess, nvRequestUtils.nvFailure);
      },

      searchReceipts: id =>
        $http
          .get(`driver/1.0/drivers/${id}/receipts?all=true`)
          .then(nvRequestUtils.nvSuccess, nvRequestUtils.nvFailure),
    };
  }
}());
