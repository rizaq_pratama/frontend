(function model() {
  angular
  .module('nvCommons.models')
  .factory('Warehousing', Warehousing);

  Warehousing.$inject = ['$http', 'nvRequestUtils'];

  function Warehousing($http, nvRequestUtils) {
    const fields = {
      id: { displayName: 'commons.id' },
      title: { displayName: 'container.warehousing.title' },
      sortation_factors: { displayName: 'container.warehousing.sortation-factors' },
      override_factors: { displayName: 'container.warehousing.override-factors' },
      active: { displayName: 'container.warehousing.active' },
    };

    const zoneChunkFields = {
      arms: { displayName: 'container.warehousing.arms' },
      routes: { displayName: 'commons.routes' },
      zones: { displayName: 'commons.zones' },
      sizes: { displayName: 'commons.sizes' },
      descriptions: { displayName: 'commons.descriptions' },
      request: { displayName: 'container.warehousing.requests' },
    };

    const beltOptions = [
      { displayName: 'container.warehousing.upper', value: 'NV_S_2' },
      { displayName: 'container.warehousing.lower', value: 'NV_S_1' },
    ];

    return {
      FIELDS: fields,
      ZONE_CHUNK_FIELDS: zoneChunkFields,
      BELT_OPTIONS: beltOptions,

      uploadToGreyOrange: (ip, data) => $http.post(`http://${ip}/api/v1/client/reference`, data, { headers: { timezone: undefined } }),

      getConfig: (ip, belt) =>
        $http.get(`http://${ip}/api/v2/installations/${belt}/config`, { headers: { timezone: undefined } })
          .then(nvRequestUtils.success, nvRequestUtils.nvFailure),

      updateConfig: (ip, belt, id, data) => $http.put(`http://${ip}/api/v2/installations/${belt}/config/${id}`, data, { headers: { timezone: undefined } }),

      updateRouteMapping: (ip, belt, id, data) => $http.put(`http://${ip}/api/v2/installations/${belt}/config/${id}/groupmapping`, data, { headers: { timezone: undefined } }),

      getInboundData: () =>
        $http.get('core/sorter/inbound/reference')
          .then(nvRequestUtils.success, nvRequestUtils.nvFailure),

      getParcelSortData: () =>
        $http.get('core/sorter/parcel-sort/reference')
          .then(nvRequestUtils.success, nvRequestUtils.nvFailure),

      getDimmingOffset: () =>
        $http.get('core/warehouse/dimming-offset')
          .then(nvRequestUtils.success, nvRequestUtils.nvFailure),

      sweep: payload => $http.post('core/warehousesweeps', payload)
        .then(nvRequestUtils.success, nvRequestUtils.nvFailure),

    };
  }
}());
