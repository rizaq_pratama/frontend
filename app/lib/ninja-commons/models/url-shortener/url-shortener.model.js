(function model() {
  angular
        .module('nvCommons.models')
        .factory('UrlShortener', UrlShortener);

  UrlShortener.$inject = ['$http', 'nvRequestUtils'];

  function UrlShortener($http, nvRequestUtils) {
   
    return {
        /**
         *  bulk short function
         *  payload : 
         *  
         *      [
         *          {
         *              "longUrl": "https://google.com/"
         *          },
         *          {
         *              "longUrl": "https://www.ninjavan.co"
         *          }
         *      ]
         *  }
         * 
         */
        bulkShort: function bulkShort(payload){
            return $http.post('url-shortener', payload)
                .then(nvRequestUtils.success, nvRequestUtils.nvFailure);
        }
    };
    
  }
}());
