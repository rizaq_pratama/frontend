(function driverContactTypeModel() {
  angular
    .module('nvCommons.models')
    .factory('DriverContactType', DriverContactType);

  DriverContactType.$inject = ['$http', 'nvModelUtils', 'nvRequestUtils'];

  function DriverContactType($http, nvModelUtils, nvRequestUtils) {
    const fields = {
      id: { displayName: 'ID' },
      name: { displayName: 'Name' },
      createdAt: { displayName: 'Created At' },
      updatedAt: { displayName: 'Updated At' },
      deletedAt: { displayName: 'Deleted At' },
    };

    return {
      searchAll: (options) => {
        const params = _.defaults(options || {}, {
          refresh: false, // set true to reload Hazelcast map
        });

        return $http.get(`driver/1.0/driver-contact-types${(params.refresh ? '?refresh=true' : '')}`)
          .then(nvRequestUtils.nvSuccess, nvRequestUtils.nvFailure);
      },

      createOne: data =>
        $http.post('driver/1.0/driver-contact-types', data)
          .then(nvRequestUtils.nvSuccess, nvRequestUtils.nvFailure),

      updateOne: (options) => {
        const data = _.omit(options, 'contactType.id');

        return $http.put(`driver/1.0/driver-contact-types/${options.contactType.id}`, data)
          .then(nvRequestUtils.nvSuccess, nvRequestUtils.nvFailure);
      },

      deleteOne: id =>
        $http.delete(`driver/1.0/driver-contact-types/${id}`)
          .then(nvRequestUtils.nvSuccess, nvRequestUtils.nvFailure),

      toOptions: data =>
        _(data.contactTypes)
          .map(contactType => ({
            displayName: contactType.name,
            value: contactType.name,
          }))
          .sortBy('displayName')
          .value(),

      getAddFields: () =>
        nvModelUtils.pickFields(fields, ['name']),

      getEditFields: model =>
        nvModelUtils.pickFields(fields, ['id', 'name'], { setValues: true, model: model }),
    };
  }
}());
