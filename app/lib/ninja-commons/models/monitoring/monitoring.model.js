(function model() {
  angular.module('nvCommons.models')
    .factory('Monitoring', Monitoring);

  Monitoring.$inject = ['$http', 'nvRequestUtils', 'nvDateTimeUtils', 'nvTimezone'];

  function Monitoring($http, nvRequestUtils, nvDateTimeUtils, nvTimezone) {
    let migratedBaseUrl = 'core';
    if (nv.config.env === 'prod') {
      migratedBaseUrl = 'core-kafka';
    }
    return {
      read: (options) => {
        const currentTime = moment().toDate();
        _.defaults(options, {
          from_datetime: nvDateTimeUtils.displayDateTime(
            nvDateTimeUtils.toSOD(currentTime, nvTimezone.getOperatorTimezone()), 'utc'),
          to_datetime: nvDateTimeUtils.displayDateTime(
            nvDateTimeUtils.toEOD(currentTime, nvTimezone.getOperatorTimezone()), 'utc'),
        });

        return $http.post(`${migratedBaseUrl}/monitoring/outbound`, options)
          .then(nvRequestUtils.success, nvRequestUtils.nvFailure);
      },
      readSingle: id => $http
        .get(`${migratedBaseUrl}/monitoring/outbound/${id}`)
        .then(nvRequestUtils.success, nvRequestUtils.nvFailure),
      putFlag: routeId =>
        $http.post(`core/monitoring/routes/${routeId}/flag`, {})
          .then(nvRequestUtils.success, nvRequestUtils.nvFailure),
      comment: (routeId, comment) =>
        $http.post('core/monitoring/routes/outbound-comments', { id: routeId, outbound_comments: comment })
          .then(nvRequestUtils.success, nvRequestUtils.nvFailure),
      fetchNonInbounded: date => $http
        .get(`${migratedBaseUrl}/monitoring/noninbounded?date=${date}`)
        .then(nvRequestUtils.success, nvRequestUtils.nvFailure),
      fetchFailedOrder: params =>
        $http.post('core/monitoring/noninbounded/search', params)
          .then(nvRequestUtils.success, nvRequestUtils.nvFailure),
    };
  }
}());
