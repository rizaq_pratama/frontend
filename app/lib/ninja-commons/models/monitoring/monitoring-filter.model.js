(function model() {
  angular
    .module('nvCommons.models')
    .factory('MonitoringFilter', MonitoringFilter);

  MonitoringFilter.$inject = ['nvBackendFilterUtils', 'nvTimezone', 'nvDateTimeUtils',
    'Hub', 'Zone'];

  function MonitoringFilter(nvBackendFilterUtils, nvTimezone, nvDateTimeUtils,
    Hub, Zone) {
    return {
      init: init,
    };

    function init() {
      const filters = [
        {
          type: nvBackendFilterUtils.FILTER_DATE,
          fromModel: moment().toDate(),
          toModel: moment().toDate(),
          showOnInit: true,
          isMandatory: true,
          transformFn: transformDate,
          mainTitle: 'Date',
          maxDateRange: 7,
          key: {
            from: 'from_datetime',
            to: 'to_datetime',
          },
          presetKey: {
            from: 'from_datetime',
            to: 'to_datetime',
          },
        },
        {
          type: nvBackendFilterUtils.FILTER_OPTIONS,
          possibleOptions: getZones(),
          selectedOptions: [],
          searchText: {},
          showOnInit: true,
          mainTitle: 'Zone Select',
          key: 'zone_ids',
        },
        {
          type: nvBackendFilterUtils.FILTER_OPTIONS,
          possibleOptions: getHubs(),
          selectedOptions: [],
          searchText: {},
          showOnInit: true,
          mainTitle: 'Hub Select',
          key: 'hub_ids',
        },
      ];

      return filters;
    }

    function transformDate(date, type) {
      if (type === 'from') {
        const startDate = nvDateTimeUtils.toSOD(date, nvTimezone.getOperatorTimezone());
        return nvDateTimeUtils.displayDateTime(startDate, 'utc');
      }
      // type === 'to'
      const endDate = nvDateTimeUtils.toEOD(date, nvTimezone.getOperatorTimezone());
      return nvDateTimeUtils.displayDateTime(endDate, 'utc');
    }

    function getHubs() {
      return Hub.read({ active_only: false }).then(hubs => _(hubs)
        .map(hub => _.defaults(hub, { displayName: hub.name }))
        .value());
    }

    function getZones() {
      return Zone.read().then(data => Zone.toFilterOptionsWithIdName(data));
    }
  }
}());

