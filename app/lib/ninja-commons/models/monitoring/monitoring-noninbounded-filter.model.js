(function model() {
  angular
    .module('nvCommons.models')
    .factory('NonInboundedFilter', NonInboundedFilter);

  NonInboundedFilter.$inject = ['nvBackendFilterUtils', 'nvTimezone', 'nvDateTimeUtils',
    'Shippers'];

  function NonInboundedFilter(nvBackendFilterUtils, nvTimezone, nvDateTimeUtils,
    Shippers) {
    return {
      init: init,
      getSelectedOptions: getSelectedOptions,
    };

    function init() {
      const filters = [
        {
          type: nvBackendFilterUtils.FILTER_DATE,
          fromModel: moment().toDate(),
          showOnInit: true,
          isMandatory: true,
          isRange: false,
          transformFn: transformDate,
          mainTitle: 'Date',
          name: 'date',
          key: {
            from: 'date',
          },
        },
        {
          type: nvBackendFilterUtils.FILTER_AUTOCOMPLETE,
          callback: getShippers,
          backendKey: 'legacy_ids',
          selectedOptions: [],
          showOnInit: true,
          mainTitle: 'Shipper Select',
          name: 'shippers',
          key: 'shipper_ids',
        },
        {
          type: nvBackendFilterUtils.FILTER_OPTIONS,
          possibleOptions: getMasterShippers(),
          selectedOptions: [],
          searchText: {},
          showOnInit: true,
          mainTitle: 'Master Shipper Select',
          name: 'masterShippers',
          key: 'master_shipper_ids',
        },
      ];

      return filters;
    }

    function getSelectedOptions(filters, filterName) {
      const filter = _.find(filters, ['name', filterName]);
      return (filter && filter.selectedOptions) || [];
    }

    function transformDate(date) {
      const startDate = nvDateTimeUtils.toEOD(date, nvTimezone.getOperatorTimezone());
      return nvDateTimeUtils.displayDateTime(startDate, 'utc');
    }

    function getShippers(text) {
      return Shippers.filterSearch(
        text,
        _.isUndefined(this.getSelectedOptions) ? [] : this.getSelectedOptions()
      );
    }

    function getMasterShippers() {
      return Shippers.readAll({ is_marketplace: true }).then(response => _(response)
          .map(shipper => _.defaults(shipper, { displayName: `${shipper.id}-${shipper.name}` }))
          .value());
    }
  }
}());

