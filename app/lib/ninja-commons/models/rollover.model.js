(function model() {
  angular
  .module('nvCommons.models')
  .factory('Rollover', Rollover);

  Rollover.$inject = ['$http', 'nvRequestUtils', 'nvTranslate'];

  function Rollover($http, nvRequestUtils, nvTranslate) {
    const fields = {
      uuid: { displayName: 'commons.model.uuid' },
      status: { displayName: 'commons.model.status' },
    };

    const rolloverStatus = [
      'container.rollover.in-progress',
      'container.rollover.success',
      'container.rollover.error',
    ];

    return {
      rollover: (date) => 
        $http.get(`core/warehouse/rollover?${nvRequestUtils.queryParam({ routeDate: date, read_only: false })}`).then(
          nvRequestUtils.success,
          nvRequestUtils.nvFailure
        ).then((data) => ({ uuid: data, status: getStatus(0) })),

      dryRun: (date) => 
        $http.get(`core/warehouse/rollover?${nvRequestUtils.queryParam({ routeDate: date })}`).then(
          nvRequestUtils.success,
          nvRequestUtils.nvFailure
        ).then((data) => ({ uuid: data, status: getStatus(0) })),

      poll: (uuid) => 
        $http.get(`core/warehouse/rollover/peek/${uuid}`).then(
          nvRequestUtils.success,
          nvRequestUtils.nvFailure
        ).then((data) => ({ uuid: uuid, status: getStatus(data) })),

      pollAll: () =>
        $http.get(`core/warehouse/rollover/peekall`).then(
          nvRequestUtils.success,
          nvRequestUtils.nvFailure
        ).then(extend),

      FIELDS: fields,
    };

    function extend(data) { 
      return _.map(data, (value, key) => ({
        uuid: key,
        status: getStatus(value)
      }));
    }

    function getStatus(status) {
      return nvTranslate.instant(rolloverStatus[status]);
    }
  }
}());
