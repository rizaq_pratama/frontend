(function model() {
  angular
    .module('nvCommons.models')
    .factory('CODReport', CODReport);

  CODReport.$inject = [
    '$http',
    'nvRequestUtils',
    'nvExtendUtils',
    'nvDateTimeUtils',
    'nvTimezone',
  ];

  function CODReport(
    $http, 
    nvRequestUtils, 
    nvExtendUtils, 
    nvDateTimeUtils,
    nvTimezone
  ) {
    const fields = {
      id: { displayName: 'Id' },
      tracking_id: { displayName: 'Tracking Id' },
      goods_amount: { displayName: 'Cash On Delivery Amount' },
      shipping_amount: { displayName: 'Shipping Amount' },
      collected_sum: { displayName: 'Collected Sum' },
      collection_at: { displayName: 'Collected At' },
      collected: { displayName: 'Collected' },
      granular_status: { displayName: 'Granular Status' },
      shipper_name: { displayName: 'Shipper Name' },
      route_id: { displayName: 'Route Id' },
      vehicle: { displayName: 'Vehicle' },
      driver_name: { displayName: 'Driver Name' },
    };

    return {

      getCOD: (date) => {
        const data = {
          startDate: nvDateTimeUtils.displayDateTime(nvDateTimeUtils.toSOD(date, nvTimezone.getOperatorTimezone()), 'utc'),
          endDate: nvDateTimeUtils.displayDateTime(nvDateTimeUtils.toEOD(date, nvTimezone.getOperatorTimezone()), 'utc'),
        };
        return $http.get(`core/cod?${nvRequestUtils.queryParam(data)}`).then(
          nvRequestUtils.success,
          nvRequestUtils.nvFailure
        ).then(mapCOD);
      },

      getDriverCOD: (date) => {
        const data = {
          startDate: nvDateTimeUtils.displayDateTime(nvDateTimeUtils.toSOD(date, nvTimezone.getOperatorTimezone()), 'utc'),
          endDate: nvDateTimeUtils.displayDateTime(nvDateTimeUtils.toEOD(date, nvTimezone.getOperatorTimezone()), 'utc'),
        };
        return $http.get(`core/cod/driver?${nvRequestUtils.queryParam(data)}`).then(
          nvRequestUtils.success,
          nvRequestUtils.nvFailure
        ).then(mapCOD);
      },

      FIELDS: fields,

      COD_TABLE_COLUMNS: ['tracking_id', 'granular_status', 'shipper_name', 'goods_amount', 'shipping_amount', 'collected_sum', 'collection_at', 'collected'],

      DRIVER_COD_TABLE_COLUMNS: ['tracking_id', 'granular_status', 'shipper_name', 'goods_amount', 'shipping_amount', 'collected_sum', 'collection_at', 'collected', 'driver_name', 'route_id'],

    };

    function mapCOD(cods) {
      return _.map(cods, (cod) => {
        cod.collected = (cod.goods_amount <= cod.collected_sum) ? 'Yes' : 'No';
        cod.collection_at = (cod.collection_at === 'DD') ? 'Delivery' : 'Pickup';
        return cod;
      });
    }
  }
}());
