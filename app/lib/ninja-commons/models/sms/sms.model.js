(function model() {
  angular
        .module('nvCommons.models')
        .factory('Sms', Sms);

  Sms.$inject = ['$q', 'nvRequestUtils', 'nvTranslate', '$http'];

  function Sms($q, nvRequestUtils, nvTranslate, $http) {
    const fields = {
      route: { displayName: nvTranslate.instant('container.sms.channel') },
      sentTime: { displayName: nvTranslate.instant('container.sms.sent-date-time') },
      toNumber: { displayName: nvTranslate.instant('container.sms.contact-number') },
      message: { displayName: nvTranslate.instant('container.sms.content') },
    };

    return {
      fields: fields,
      uploadCsv: file =>
        $http.upload('core/messaging/orders/csv', file)
          .then($q.resolve, nvRequestUtils.nvFailure),

      sendSms: payload =>
        $http.post('core/messaging/orders/sms', payload)
          .then(nvRequestUtils.success, nvRequestUtils.nvFailure),

      history: trackingId =>
        $http.get(`core/messaging/orders/sms/history?tracking_id=${trackingId}`)
          .then(nvRequestUtils.success, nvRequestUtils.nvFailure),
    };
  }
}());
