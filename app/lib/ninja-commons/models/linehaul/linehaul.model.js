(function model() {
  angular
        .module('nvCommons.models')
        .factory('Linehaul', Linehaul);

  Linehaul.$inject = ['$http', 'nvRequestUtils'];

  function Linehaul($http, nvRequestUtils) {
    const days = ['MONDAY', 'THURSDAY', 'WEDNESDAY', 'TUESDAY', 'FRIDAY', 'SATURDAY', 'SUNDAY'];
    const frequencyOptions = [

      {
        value: 'NONE',
        displayName: 'None',
      },
      {
        value: 'WEEKLY',
        displayName: 'Weekly',
      },
      {
        value: 'FORTNIGHTLY',
        displayName: 'Fortnightly',
      },
      {
        value: 'MONTHLY',
        displayName: 'Monthly',
      },
    ];

    const daysOptions = [
      {
        value: 'MONDAY',
        displayName: 'Monday',
      },
      {
        value: 'TUESDAY',
        displayName: 'Tuesday',
      },
      {
        value: 'WEDNESDAY',
        displayName: 'Wednesday',
      },
      {
        value: 'THURSDAY',
        displayName: 'Thursday',
      },
      {
        value: 'FRIDAY',
        displayName: 'Friday',
      },
      {
        value: 'SATURDAY',
        displayName: 'Saturday',
      },
      {
        value: 'SUNDAY',
        displayName: 'Sunday',
      },
    ];
    const fields = {
      id: { displayName: 'Linehaul ID' },
      name: { displayName: 'Name' },
      start_hub: { displayName: 'Start Hub' },
      transit_hub: { displayName: 'Transit Hub' },
      end_hub: { displayName: 'End Hub' },
      frequency: { displayName: 'Frequency' },
      days_of_week: { displayName: 'Days of Week' },
      start_date: { displayName: 'Start Date' },
      end_date: { displayName: 'End Date' },
      comments: { displayName: 'Comments' },
    };

    return {
      getDays: function getDays() {
        return days;
      },
      TABLE_FIELDS: fields,
      getFrequencyOptions: function getFrequencyOptions() {
        return frequencyOptions;
      },
      getDayOptions: function getDayOptions() {
        return daysOptions;
      },
      createLinehaul: function createLinehaul(payload) {
        return $http.post('hub/linehauls', payload)
          .then(nvRequestUtils.success, nvRequestUtils.nvFailure);
      },
      getAllLinehaul: function getAllLinehaul() {
        return $http.get('hub/linehauls')
          .then(nvRequestUtils.success, nvRequestUtils.nvFailure);
      },
      getLinehaul: function getLinehaul(linehaulId) {
        return $http.get(`hub/linehauls/${linehaulId}`)
          .then(nvRequestUtils.success, nvRequestUtils.nvFailure);
      },
      deleteLinehaul: function deleteLinehaul(linehaulId) {
        return $http.delete(`hub/linehauls/${linehaulId}`)
          .then(nvRequestUtils.success, nvRequestUtils.nvFailure);
      },
      updateLinehaul: function updateLinehaul(linehaulId, payload) {
        return $http.put(`hub/linehauls/${linehaulId}`, payload)
          .then(nvRequestUtils.success, nvRequestUtils.nvFailure);
      },
      updateLinehaulLeg: function updateLinehaulLeg(linehaulId, legs) {
        return $http.put(`hub/linehauls/${linehaulId}/legs`, legs)
          .then(nvRequestUtils.success, nvRequestUtils.nvFailure);
      },
      linehaulsSchedule: function linehaulsSchedule(startDate, endDate) {
                // check if date
        let startDateString = startDate;
        let endDateString = endDate;
        if (startDate instanceof Date) {
          startDateString = moment(startDate).format('YYYY-MM-DD');
        }
        if (endDate instanceof Date) {
          endDateString = moment(endDate).format('YYYY-MM-DD');
        }
        return $http.get(`hub/linehauls/scheduled-dates?start=${startDateString}&end=${endDateString}`)
          .then(nvRequestUtils.success, nvRequestUtils.nvFailure);
      },
      linehaulSchedule: function linehaulSchedule(linehaulId, startDate, endDate) {
        let startDateString = startDate;
        let endDateString = endDate;
        if (startDate instanceof Date) {
          startDateString = moment(startDate).format('YYYY-MM-DD');
        }
        if (endDate instanceof Date) {
          endDateString = moment(endDate).format('YYYY-MM-DD');
        }
        return $http.get(`hub/linehauls/${linehaulId}/scheduled-dates?start=${startDateString}&end=${endDateString}`)
          .then(nvRequestUtils.success, nvRequestUtils.nvFailure);
      },
      getAllLinehaulPage: function getAllLinehaulPage(page, pageSize) {
        return $http.get(`hub/linehauls?page=${page}&page_size=${pageSize}`)
          .then(nvRequestUtils.success, nvRequestUtils.nvFailure);
      },
      searchPage: function searchPage(params, page, pageSize) {
        return $http.post(`hub/linehauls/search?page_size=${pageSize}&page=${page}`, params)
          .then(nvRequestUtils.success, nvRequestUtils.nvFailure);
      },
      mapToFrequencyRequest: function mapToFrequencyRequest(frequency) {
        return frequency.value;
      },
      mapToHubRequest: function mapToHubRequest(hub) {
        return {
          country: hub.country,
          id: hub.id,
        };
      },
      mapToDayOfWeekRequest: function mapToDayOfWeekRequest(dow) {
        return dow.value;
      },
    };
  }
}());
