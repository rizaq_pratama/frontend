(function filterTemplate() {
  angular.module('nvCommons.models')
        .factory('LinehaulFilterTemplate', LinehaulFilterTemplate);

  LinehaulFilterTemplate.$inject = ['$http', 'nvRequestUtils', 'nvModelUtils'];

  function LinehaulFilterTemplate($http, nvRequestUtils) {
    const LINEHAUL = 'LINEHAUL';
    return {
      LINEHAUL: LINEHAUL,
      create: function create(payload) {
        const template = {
          template: {
            name: payload.name,
            payload: _.omit(payload, 'name'),
            entity: LINEHAUL,
          },
        };
        return $http.post('hub/templates', template)
                    .then(nvRequestUtils.success, nvRequestUtils.nvFailure);
      },
      update: function update(id, payload) {
        const template = {
          template: {
            name: payload.name,
            payload: _.omit(payload, 'name'),
            entity: LINEHAUL,
          },
        };
        return $http.put(`hub/templates/${id}`, template)
                    .then(nvRequestUtils.success, nvRequestUtils.nvFailure);
      },
      getFilter: function getFilter() {
        return $http.get(`hub/templates?entity=${LINEHAUL}`)
                    .then(nvRequestUtils.success, nvRequestUtils.nvFailure);
      },
      getFilterById: function getFilterById(id) {
        return $http.get(`hub/templates/${id}`)
                    .then(nvRequestUtils.success, nvRequestUtils.nvFailure);
      },
      deleteFilter: function deleteFilter(id) {
        return $http.delete(`hub/templates/${id}`)
                    .then(nvRequestUtils.success, nvRequestUtils.nvFailure);
      },
      toOptions: function toOptions(data) {
        return _.map(data.templates, template => ({
          displayName: `${template.id}-${template.name}`,
          value: template,
        }));
      },
    };
  }
}());
