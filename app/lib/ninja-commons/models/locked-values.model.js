(function Overwatch() {
  angular
    .module('nvCommons.models')
    .factory('LockedValues', LockedValues);

  LockedValues.$inject = ['$http', 'nvRequestUtils'];

  function LockedValues($http, nvRequestUtils) {
    const COLUMN_KEY = {
      INBOUND_WEIGHT_TOLERANCE: 'inbound_weight_tolerance',
      INBOUND_MAX_WEIGHT: 'inbound_max_weight',
    };

    return {
      COLUMN_KEY: COLUMN_KEY,

      read: () => $http.get('core/get_locked_values')
        .then(nvRequestUtils.success, nvRequestUtils.nvFailure),
      toggle: key => $http.put('core/parameter_toggle', { key: key })
        .then(nvRequestUtils.success, nvRequestUtils.nvFailure),
      update: data => $http.put('core/parameter', data)
        .then(nvRequestUtils.success, nvRequestUtils.nvFailure),
    };
  }
}());
