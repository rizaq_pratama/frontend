(function model() {
  angular
        .module('nvCommons.models')
        .factory('Shipping', Shipping);

  Shipping.$inject = [
    '$http', 'nvDateTimeUtils', 'nvTimezone', 'nvTranslate', '$filter',
    'nvRequestUtils', 'PDF', '$q',
  ];

  function Shipping(
    $http, nvDateTimeUtils, nvTimezone, nvTranslate, $filter,
    nvRequestUtils, PDF, $q
  ) {
    const fields = {
      id: { displayName: nvTranslate.instant('container.shipment-management.shipment-id') },
      created_at: { displayName: nvTranslate.instant('container.shipment-management.creation-date-time') },
      transit_at: { displayName: nvTranslate.instant('container.shipment-management.transit-date-time') },
      status: { displayName: nvTranslate.instant('container.shipment-management.status') },
      orig_hub_name: { displayName: nvTranslate.instant('container.shipment-management.start-hub') },
      curr_hub_name: { displayName: nvTranslate.instant('container.shipment-management.last-inbound-hub') },
      pending_hub_name: { displayName: nvTranslate.instant('container.shipment-management.pending-hub-title') },
      dest_hub_name: { displayName: nvTranslate.instant('container.shipment-management.end-hub-title') },
      arrival_datetime: { displayName: nvTranslate.instant('container.shipment-management.eta-date-time') },
      completed_at: { displayName: nvTranslate.instant('container.shipment-management.completion-date-time') },
      orders_count: { displayName: nvTranslate.instant('container.shipment-management.total-parcels') },
      comments: { displayName: nvTranslate.instant('container.shipment-management.comments') },
      shipment_type: { displayName: nvTranslate.instant('container.shipment-management.shipment-type') },
      mawb: { displayName: nvTranslate.instant('container.shipment-management.mawb') },
    };
    const scanningFields = {
      trackingId: {
        displayName: nvTranslate.instant('container.shipment-scanning.tracking-id'),
      },
      destination: {
        displayName: nvTranslate.instant('container.shipment-scanning.destination'),
      },
      addressee: {
        displayName: nvTranslate.instant('container.shipment-scanning.addressee'),
      },
      rackSector: {
        displayName: nvTranslate.instant('container.shipment-scanning.rack-sector'),
      },
      deliverBy: {
        displayName: nvTranslate.instant('container.shipment-scanning.deliver-by'),
      },
    };
    const ITEM_PER_PAGE = 10;
    const PAGE = 1;
    const DEFAULT_ITEM_PER_PAGE = 300;

    const statuses = [
      {
        id: 1,
        displayName: 'Pending',
        lowercaseName: 'pending',
      },
      {
        id: 2,
        displayName: 'Transit',
        lowercaseName: 'transit',
      },
      {
        id: 3,
        displayName: 'Completed',
        lowercaseName: 'completed',
      },
      {
        id: 4,
        displayName: 'Cancelled',
        lowercaseName: 'cancelled',
      },
    ];

    const DETAILS_FIELDS = {
      MISSING: {
        tracking_id: { displayName: nvTranslate.instant('commons.tracking-id') },
        granular_status: { displayName: nvTranslate.instant('container.shipment-management.granular-status') },
        last_hub: { displayName: nvTranslate.instant('container.shipment-management.last-hub') },
        created_at: { displayName: nvTranslate.instant('commons.created-at') },
      },
      EXTRA: {
        tracking_id: { displayName: nvTranslate.instant('commons.tracking-id') },
        last_hub: { displayName: nvTranslate.instant('container.shipment-management.last-hub') },
        last_scan: { displayName: nvTranslate.instant('container.shipment-management.last-scan') },
      },
      REMOVED: {
        tracking_id: { displayName: nvTranslate.instant('commons.tracking-id') },
        granular_status: { displayName: nvTranslate.instant('container.shipment-management.granular-status') },
        last_hub: { displayName: nvTranslate.instant('container.shipment-management.last-hub') },
        created_at: { displayName: nvTranslate.instant('commons.created-at') },
      },
      ORDERS: {
        tracking_id: { displayName: nvTranslate.instant('commons.tracking-id') },
        granular_status: { displayName: nvTranslate.instant('container.shipment-management.granular-status') },
        last_hub: { displayName: nvTranslate.instant('container.shipment-management.last-hub') },
        created_at: { displayName: nvTranslate.instant('container.shipment-management.last-scan') },
      },
      SCANS: {
        source: { displayName: nvTranslate.instant('container.shipment-management.source') },
        user_id: { displayName: nvTranslate.instant('container.shipment-management.user_id') },
        result: { displayName: nvTranslate.instant('container.shipment-management.result') },
        hub: { displayName: nvTranslate.instant('commons.hub') },
        created_at: { displayName: nvTranslate.instant('commons.created-at') },
      },
    };
    const self = {
      DETAILS_FIELDS: DETAILS_FIELDS,
      getStatuses: function getStatuses() {
        return statuses;
      },
      getHubs: function getHubs(country) {
        if (country) {
          return $http.get(`hub/hubs?country=${country}`)
            .then(nvRequestUtils.success, nvRequestUtils.nvFailure);
        }
        return $http.get('hub/hubs')
          .then(nvRequestUtils.success, nvRequestUtils.nvFailure);
      },
      get: function get(id) {
        return $http.get(`hub/shipments/${id}`)
          .then(nvRequestUtils.success, nvRequestUtils.nvFailure);
      },
      getDetail: function getDetail(id) {
        return $http.get(`hub/shipments/${id}?include_scans=true`)
          .then(nvRequestUtils.success, nvRequestUtils.nvFailure);
      },
      getAll: function getAll() {
        return $http.get('hub/shipments')
          .then(nvRequestUtils.success, nvRequestUtils.nvFailure);
      },
      getAllPage: function getAllPage(itemsPerPage, page) {
        const nItemsPerPage = itemsPerPage || ITEM_PER_PAGE;
        const nPage = page || PAGE;
        return $http.get(`hub/shipments?items_per_page=${nItemsPerPage}&page=${nPage}`)
          .then(nvRequestUtils.success, nvRequestUtils.nvFailure);
      },
      create: function create(shipping) {
        if (shipping.shipment && shipping.shipment.end_date) {
          shipping.shipment.end_date = nvDateTimeUtils.displayDate(shipping.shipment.end_date);
        } else {
          shipping.shipment.end_date = null;
        }
        return $http.post('hub/shipments', shipping)
          .then(nvRequestUtils.success, nvRequestUtils.nvFailure);
      },
      update: function update(shipping, id) {
        return $http.put(`hub/shipments/${id}`, shipping)
          .then(nvRequestUtils.success, nvRequestUtils.nvFailure);
      },
      delete: function deleteShipment(id) {
        return $http.delete(`hub/shipments/${id}`)
          .then(nvRequestUtils.success, nvRequestUtils.nvFailure);
      },
      updateOrder: function updateOrder(order, id) {
        return $http.put(`hub/shipments/${id}/orders`, order)
          .then(nvRequestUtils.success, nvRequestUtils.nvFailure);
      },
      scan: function scan(hubId, shipmentId, scanValue) {
        const payload = {
          scans: [{
            hub_id: hubId,
            shipment_id: shipmentId,
            scan: scanValue,
          }],
        };
        return $http.post('hub/shipments/scan', payload)
          .then(nvRequestUtils.success, returnReject);
      },
      hubInboundScan: function hubInboundScan(payload) {
        return $http.post('hub/1.1/shipments/scans/hub-inbound', payload)
          .then(nvRequestUtils.success, returnReject);
      },
      vanInboundScan: function vanInboundScan(payload) {
        return $http.post('hub/1.1/shipments/scans/van-inbound', payload)
          .then(nvRequestUtils.success, returnReject);
      },
      getShipmentByHub: function getShipmentByHub(hubId) {
        return $http.get(`hub/hubs/${hubId}/shipments-in-progress`)
          .then(nvRequestUtils.success, nvRequestUtils.nvFailure);
      },
      search: function search(filter) {
        let payload = {
          from_creation_date: filter.fromCreationDate ? moment(filter.fromCreationDate).format('YYYY-MM-DD') : null,
          to_creation_date: filter.toCreationDate ? moment(filter.toCreationDate).format('YYYY-MM-DD') : null,
          shipment_status: mapToStatusRequest(filter.shipmentStatus),
          orig_hub: mapToHubRequest(filter.originHubs),
          curr_hub: mapToHubRequest(filter.currentHubs),
          pending_hub: mapToHubRequest(filter.pendingHubs),
          dest_hub: mapToHubRequest(filter.destinationHubs),
        };
        payload = _.omitBy(payload, _.isNil);
        return $http.post('hub/shipments/search', payload)
          .then(nvRequestUtils.success, nvRequestUtils.nvFailure);
      },
      searchByPageV1: (payload, itemsPerPage, page) =>
        $http.post(`hub/1.1/shipments/search?items_per_page=${itemsPerPage}&page=${page}`, payload)
          .then(nvRequestUtils.success, nvRequestUtils.nvFailure),
      /**
       * Quick Search method
       * @param {String[]} types - array of shiment type
       * @param {{id: Number, system_id: String}[]} origHubIds - array of origin hub
       * @returns {Object} shipments
       */
      quickSearch: function quickSearch(types, origHubIds) {
        const today = moment.tz(new Date(), nvTimezone.getOperatorTimezone());
        const thirtyDaysBefore = moment().subtract(30, 'days');

        const payload = {
          shipment_status: ['PENDING'],
          orig_hub: origHubIds,
          created: {
            from: nvDateTimeUtils.displayFormat(nvDateTimeUtils.toSOD(thirtyDaysBefore),
              nvDateTimeUtils.FORMAT_ISO_TZ, 'utc'),
            to: nvDateTimeUtils.displayFormat(nvDateTimeUtils.toEOD(today),
              nvDateTimeUtils.FORMAT_ISO_TZ, 'utc'),
          },
        };
        const shipmentType = _.reject(types, _.isEmpty);
        if (shipmentType && shipmentType.length > 0) {
          payload.shipment_type = shipmentType;
        }
        return self.searchByPageV1(_.omitBy(payload, _.isNil), DEFAULT_ITEM_PER_PAGE, PAGE);
      },
      searchPage: function searchPage(filter, itemsPerPage, page) {
        const nItemsPerPage = itemsPerPage || ITEM_PER_PAGE;
        const nPage = page || PAGE;
        return $http.post(`hub/shipments/search?items_per_page=${nItemsPerPage}&page=${nPage}`, filter)
          .then(nvRequestUtils.success, nvRequestUtils.nvFailure);
      },
      searchPagev1: function searchPagev1(filter, itemsPerPage, page) {
        const nItemsPerPage = itemsPerPage || ITEM_PER_PAGE;
        const nPage = page || PAGE;
        return self.searchByPageV1(filter, nItemsPerPage, nPage);
      },
      shipmentScans: function shipmentScans(shipmentId) {
        return $http.get(`hub/shipments/${shipmentId}/scans`)
          .then(nvRequestUtils.success, nvRequestUtils.nvFailure);
      },
      orderScan: function orderScan(payload) {
        return $http.post('hub/shipments/orders/scans/add-to-shipment', payload)
          .then(nvRequestUtils.success, returnReject);
      },
      shipmentAwb: function shipmentAwb(shipmentId) {
        return PDF.view(`hub/shipments/${shipmentId}/shipment-awb`);
      },
      reports: function reports(startDate, endDate) {
        const lStartDate = startDate ? moment(startDate).format('YYYY-MM-DD') : null;
        const lEndDate = endDate ? moment(endDate).format('YYYY-MM-DD') : null;
        return $http.get(`hub/reports/shipment?start=${lStartDate}&end=${lEndDate}`)
          .then(nvRequestUtils.success, nvRequestUtils.nvFailure);
      },
      reportsPdf: function reportsPdf(startDate, endDate) {
        const lStartDate = startDate ? moment(startDate).format('YYYY-MM-DD') : null;
        const lEndDate = endDate ? moment(endDate).format('YYYY-MM-DD') : null;
        return PDF.view(`hub/reports/shipment?start=${lStartDate}&end=${lEndDate}&format=pdf`);
      },
      updateShipment: payload =>
        $http.put('hub/shipments/orders/transactions', payload)
          .then(nvRequestUtils.success, nvRequestUtils.nvFailure),
      updateShipmentDates: function updateShipmentDate(shipments, dateString) {
        const endDates = _.map(shipments, o => ({
          shipment_id: o.shipmentId,
          end_date: dateString,
        }));
        const payload = {
          end_dates: endDates,
        };
        return self.updateShipment(payload);
      },
      updateShipmentDate: function updateShipmentDate(shipment, dateString) {
        const endDate = {
          shipment_id: shipment.id,
          end_date: dateString,
        };
        const payload = {
          end_dates: [endDate],
        };
        return self.updateShipment(payload);
      },

      getShipmentTypes: function getShipmentTypes() {
        return $http.get('hub/shipment-types')
          .then(response =>
            $q.resolve(remapShipmentTypes(_.get(response, 'data'))), nvRequestUtils.nvFailure
        );

        function remapShipmentTypes(result) {
          return _.map(_.get(result, 'shipment_types'), type => ({
            value: type,
            displayName: $filter('nvSnakeCase2NaturalCase')(type),
          }));
        }
      },
      getShipmentReports: function getShipmentReports(shipmentIds) {
        const payload = {
          shipment_ids: shipmentIds,
        };
        return $http.post('hub/reports/shipments', payload)
          .then(nvRequestUtils.success, nvRequestUtils.nvFailure);
      },
      toOptions: datas =>
        _(datas)
          .map(data => ({ displayName: `${data.id} ${data.orig_hub_name}-${data.dest_hub_name}`, value: data.id, id: data.id }))
          .sortBy('value')
          .value(),

      /**
       * inbound a parcel in shipment's destination hub
       * @param {Object} payload payload object
       * @param {Object} payload.scan
       * @param {String} payload.scan.scan_value scan value
       * @param {String} payload.scan.hub_country Hub country code e.g ID, SG
       * @param {Number} payload.scan.hub_id Inbounding hub id
       * @param {Number} payload.scan.shipment_id Id of shipment
       * @param {Number} payload.scan.order_id order id
       * @param {String} payload.scan.result result from inbounding (rack sector)
       */
      parcelHubInbound: payload => (
        $http.post('hub/shipments/orders/scans/hub-inbound', payload)
        .then(nvRequestUtils.success, nvRequestUtils.nvFailure)
      ),

      TABLE_FIELDS: fields,
      mapToHubRequest: mapToHubRequest,
      mapToStatusRequest: mapToStatusRequest,
      getScanningFields: scanningFields,
      mapToShipmentTypeRequest: mapToShipmentTypeRequest,

    };

    return self;

    function mapToHubRequest(hub) {
      return {
        system_id: hub.country,
        id: hub.value,
      };
    }

    function mapToStatusRequest(status) {
      return status.displayName;
    }

    function mapToShipmentTypeRequest(shipmentType) {
      return _.snakeCase(shipmentType.displayName).toUpperCase();
    }

    function returnReject(err) {
      return $q.reject(err);
    }
  }
}());
