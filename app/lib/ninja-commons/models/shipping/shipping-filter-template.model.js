(function filterTemplate() {
  angular.module('nvCommons.models')
        .factory('ShippingFilterTemplate', ShippingFilterTemplate);

  ShippingFilterTemplate.$inject = ['$http', 'nvRequestUtils', 'nvModelUtils'];

  function ShippingFilterTemplate($http, nvRequestUtils) {
    const SHIPMENT = 'SHIPMENT';
    return {
      SHIPMENT: SHIPMENT,
      create: function create(payload) {
        const template = {
          template: {
            name: payload.name,
            payload: _.omit(payload, 'name'),
            entity: SHIPMENT,
          },
        };

        return $http.post('hub/templates', template)
                    .then(nvRequestUtils.success, nvRequestUtils.nvFailure);
      },
      update: function update(id, payload) {
        const template = {
          template: {
            name: payload.name,
            payload: _.omit(payload, 'name'),
            entity: SHIPMENT,
          },
        };
        return $http.put(`hub/templates/${id}`, template)
                    .then(nvRequestUtils.success, nvRequestUtils.nvFailure);
      },
      getFilter: function getFilter() {
        return $http.get(`hub/templates?entity=${SHIPMENT}`)
                    .then(nvRequestUtils.success, nvRequestUtils.nvFailure);
      },
      getFilterById: function getFilterById(id) {
        return $http.get(`hub/templates/${id}`)
                    .then(nvRequestUtils.success, nvRequestUtils.nvFailure);
      },
      deleteFilter: function deleteFilter(id) {
        return $http.delete(`hub/templates/${id}`)
                    .then(nvRequestUtils.success, nvRequestUtils.nvFailure);
      },
      toOptions: function toOptions(data) {
        return _.map(data.templates, template => ({
          displayName: `${template.id}-${template.name}`,
          value: template,
        }));
      },
    };
  }
}());
