(function model() {
  angular.module('nvCommons.models')
    .factory('Transaction', Transaction);

  Transaction.$inject = ['$http', 'nvRequestUtils'];

  function Transaction($http, nvRequestUtils) {
    const fields = {
      id: { translateKey: 'container.transactions.txn-id', displayName: 'Txn Id' },
      orderId: { translateKey: 'container.transactions.order-id', displayName: 'Order Id' },
      trackingId: { translateKey: 'container.transactions.tracking-id', displayName: 'Tracking Id' },
      type: {
        translateKey: 'container.transactions.txn-type',
        displayName: 'Txn Type',
        values: [
          { id: 0, name: 'PP' },
          { id: 1, name: 'DD' },
        ],
      },
      fromName: { translateKey: 'container.transactions.shipper', displayName: 'Shipper' },
      orderType: {
        translateKey: 'container.transactions.order-type',
        displayName: 'Order Type',
        values: [
          { id: 0, name: 'Normal' },
          { id: 1, name: 'Return' },
          { id: 2, name: 'C2C' },
        ],
      },
      address: { translateKey: 'container.transactions.txn-address', displayName: 'Txn Address' },
      routeId: { translateKey: 'commons.model.route-id', displayName: 'commons.model.route-id' },
      granularStatus: {
        translateKey: 'container.transactions.granular-status',
        displayName: 'Granular Status',
        values: [
          { id: 0, name: 'Staging' },
          { id: 1, name: 'Pending Pickup' },
          { id: 2, name: 'Pending Pickup at Distribution Point' },
          { id: 3, name: 'Van en-route to pickup' },
          { id: 4, name: 'En-route to Sorting Hub' },
          { id: 5, name: 'Pickup fail' },
          { id: 6, name: 'Arrived at Sorting Hub' },
          { id: 7, name: 'On Vehicle for Delivery' },
          { id: 8, name: 'Completed' },
          { id: 9, name: 'Pending Reschedule' },
          { id: 10, name: 'Transferred to 3PL' },
          { id: 11, name: 'Returned to Sender' },
          { id: 12, name: 'Cancelled' },
          { id: 13, name: 'Arrived at Distribution Point' },
          { id: 14, name: 'Arrived at Origin Hub' },
          { id: 15, name: 'Cross Border Transit' },
          { id: 16, name: 'Customs Cleared' },
          { id: 17, name: 'Customs Held' },
          { id: 18, name: 'Returned to Warehouse' },
          { id: 19, name: 'Removed' },
          { id: 20, name: 'On Hold' },
        ],
      },
      endDateTime: { translateKey: 'container.transactions.end-date-time', displayName: 'End Date Time' },
      status: {
        displayName: 'commons.status',
        values: [
          { id: 0, name: 'Staging' },
          { id: 1, name: 'Pending' },
          { id: 2, name: 'Success' },
          { id: 3, name: 'Fail' },
          { id: 4, name: 'Missed Pickup' },
          { id: 5, name: 'Cancelled' },
          { id: 6, name: 'Forced Success' },
          { id: 7, name: 'Forced Cancelled' },
        ],
      },
      distributionPointId: { displayName: 'commons.distribution-point-id-abbr' },
      name: { displayName: 'commons.name' },
      contact: { displayName: 'commons.contact' },
      email: { displayName: 'commons.email' },
      priorityLevel: { displayName: 'commons.model.priority-level' },
      dnr: { displayName: 'commons.model.dnr' },
    };

    const DNR = {
      SET_ASIDE_INCOMPLETE_ORDER_DETAILS: -3,
      SET_ASIDE_NINJA_COLLECT: -2,
      CANCEL: -1,
      NORMAL: 0,
      PRIORITY_OR_TIMESENSITIVE: 1,
      RESCHEDULING: 2,
      TEMP_HALT: 3,
      USING_3PL: 4,
      STAGING: 5,
      FORCED_SUCCESS: 6,
      FORCED_CANCELLATION: 7,
      SET_ASIDE: 8,
      SAME_DAY: 9,
      SET_ASIDE_GROUP_ONE: 10,
      SET_ASIDE_GROUP_TWO: 11,
      SET_ASIDE_GROUP_THREE: 12,
      SET_ASIDE_GROUP_FOUR: 13,
      SET_ASIDE_GROUP_FIVE: 14,
      SET_ASIDE_GROUP_SIX: 15,
      SET_ASIDE_GROUP_SEVEN: 16,
      SET_ASIDE_GROUP_EIGHT: 17,
      SET_ASIDE_GROUP_NINE: 18,
      SET_ASIDE_GROUP_TEN: 19,
      SET_ASIDE_GROUP_ELEVEN: 20,
      SET_ASIDE_GROUP_TWELVE: 21,
    };

    const TYPE = {
      PICKUP: 'PP',
      DELIVERY: 'DD',
    };

    const TYPE_NAME = {
      PICKUP: 'PICKUP',
      DELIVERY: 'DELIVERY',
    };

    const STATUS = {
      STAGING: 'Staging',
      PENDING: 'Pending',
      SUCCESS: 'Success',
      FAIL: 'Fail',
      CANCELLED: 'Cancelled',
    };

    const self = {

      merge: function merge(transactionIds) {
        return $http.post('core/waypoints/merge', transactionIds).then(
          nvRequestUtils.success,
          nvRequestUtils.nvFailure
        );
      },

      unmerge: function unmerge(data) {
        return $http.put('core/transactions/unmerge', data).then(
          nvRequestUtils.success,
          nvRequestUtils.nvFailure
        );
      },

      searchIds: function searchIds(options) {
        return $http.post('core/route-group-templates/transactionsIds', options).then(
          nvRequestUtils.success,
          nvRequestUtils.nvFailure
        );
      },

      search: function search(options) {
        return $http.post('core/route-group-templates/transactions', options).then(
          nvRequestUtils.success,
          nvRequestUtils.nvFailure
        );
      },

      searchByIds: function searchByIds(ids) {
        return $http
          .post('core/route-groups/transactions', { transactionIds: ids })
          .then(nvRequestUtils.success, nvRequestUtils.nvFailure);
      },

      searchWithRouteIdsOnlyByIds: function searchWithRouteIdsOnlyByIds(ids) {
        return $http
          .post('core/route-groups/transactions-with-route-ids', { transactionIds: ids })
          .then(nvRequestUtils.success, nvRequestUtils.nvFailure);
      },

      update: (orderId, transactionId, data) => $http
        .put(`core/orders/${orderId}/transactions/${transactionId}`, data)
        .then(nvRequestUtils.success, nvRequestUtils.nvFailure),

      isPending: txn => txn.status.toLowerCase() === 'pending',
      isFailed: txn => txn.status.toLowerCase() === 'fail',
      isPickup: txn => txn.type === 'PICKUP' || txn.type === 'PP',
      isDelivery: txn => txn.type === 'DELIVERY' || txn.type === 'DD',

      getFirstPendingNTTransaction: (transactions) => {
        // Get first pending non transit transaction
        const sortedTransactions = _.sortBy(transactions, 'seqNo');

        return _.find(sortedTransactions, txn =>
          !txn.transit && self.isPending(txn)
        ) || null;
      },

      getLastNTTransaction: (transactions) => {
        // Get last non transit transaction
        const sortedTransactions = _.sortBy(transactions, 'seqNo');

        return _.findLast(sortedTransactions, txn =>
            !txn.transit
          ) || null;
      },

      getLastPPNT: (transactions) => {
        // Get last pickup non transit transaction
        const sortedTransactions = _.sortBy(transactions, 'seqNo');

        return _.findLast(sortedTransactions, txn =>
          !txn.transit && self.isPickup(txn)
        ) || null;
      },

      getLastDDNT: (transactions) => {
        // Get last delivery non transit transaction
        const sortedTransactions = _.sortBy(transactions, 'seqNo');

        return _.findLast(sortedTransactions, txn =>
          !txn.transit && self.isDelivery(txn)
        ) || null;
      },

      getTransactionTypeOptions: function getTransactionTypeOptions() {
        return [
          { displayName: 'commons.model.pickup', value: TYPE.PICKUP },
          { displayName: 'commons.delivery', value: TYPE.DELIVERY },
        ];
      },

      getTypeEnum: function getTypeEnum(value) {
        let typeEnum = null;
        _.forEach(TYPE, (typeValue, key) => {
          if (typeValue === value) {
            typeEnum = key;
            return false;
          }

          return _.noop();
        });

        return typeEnum;
      },

      fields: fields,
      DNR: DNR,
      TYPE: TYPE,
      TYPE_NAME: TYPE_NAME,
      STATUS: STATUS,
    };

    return self;
  }
}());
