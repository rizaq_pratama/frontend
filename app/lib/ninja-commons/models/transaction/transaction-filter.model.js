(function model() {
  angular.module('nvCommons.models')
    .factory('TransactionFilter', TransactionFilter);

  TransactionFilter.$inject = [
    'nvCoupledListUtils',
    'nvBackendFilterUtils',
    'nvDateTimeUtils',
    'nvTimezone',
    'Transaction',
    'Zone',
    'Shippers',
    'RouteGroup',
    'Reservation',
    'DistributionPoint',
    'nvFilterNumberBoxService',
    'Order',
    'Waypoint',
    '$q',
  ];

  function TransactionFilter(
    nvCoupledListUtils,
    nvBackendFilterUtils,
    nvDateTimeUtils,
    nvTimezone,
    Transaction,
    Zone,
    Shippers,
    RouteGroup,
    Reservation,
    DistributionPoint,
    nvFilterNumberBoxService,
    Order,
    Waypoint,
    $q
  ) {
    return {
      init: init,
      getRouteGroups: getRouteGroups,
      getSelectedOptions: getSelectedOptions,
    };

    function init(result = {}) {
      const generalFilters = [
        {
          type: nvBackendFilterUtils.FILTER_DATE,
          showOnInit: true,
          fromModel: moment().toDate(),
          toModel: moment().toDate(),
          transformFn: transformDate,
          mainTitle: 'Start Datetime',
          key: {
            from: 'startTimeFrom',
            to: 'startTimeTo',
          },
          presetKey: {
            from: 'startTimeFrom',
            to: 'startTimeTo',
          },
        },
        {
          type: nvBackendFilterUtils.FILTER_DATE,
          showOnInit: true,
          fromModel: moment().toDate(),
          toModel: moment().toDate(),
          transformFn: transformDate,
          mainTitle: 'End Datetime',
          key: {
            from: 'endTimeFrom',
            to: 'endTimeTo',
          },
          presetKey: {
            from: 'endTimeFrom',
            to: 'endTimeTo',
          },
        },
        {
          type: nvBackendFilterUtils.FILTER_TIME,
          showOnInit: true,
          fromModel: moment().tz(nvTimezone.getOperatorTimezone()),
          toModel: moment().tz(nvTimezone.getOperatorTimezone()),
          transformFn: transformTime,
          mainTitle: 'Creation Time',
          key: {
            from: 'orderCreateTimeFrom',
            to: 'orderCreateTimeTo',
          },
          presetKey: {
            from: 'orderCreateTimeFrom',
            to: 'orderCreateTimeTo',
          },
        },
        {
          type: nvBackendFilterUtils.FILTER_AUTOCOMPLETE,
          showOnInit: true,
          callback: getShippers,
          backendKey: 'legacy_ids',
          selectedOptions: [],
          mainTitle: 'Shipper',
          key: 'shipperIds',
          presetKey: 'shipperIds',
        },
        {
          type: nvBackendFilterUtils.FILTER_OPTIONS,
          showOnInit: false,
          possibleOptions: getMasterShippers,
          transformFn: transformMasterShippers,
          selectedOptions: [],
          mainTitle: 'Master Shipper',
          key: 'shipperIds',
          presetKey: 'masterShipperIds',
        },
        {
          type: nvBackendFilterUtils.FILTER_OPTIONS,
          showOnInit: true,
          possibleOptions: !_.isUndefined(result.distributionPoints) ?
            getDistributionPoints(result.distributionPoints) : getDistributionPoints,
          selectedOptions: [],
          mainTitle: 'DP Order',
          key: 'distributionPointIds',
          presetKey: 'distributionPointIds',
        },
        {
          type: nvBackendFilterUtils.FILTER_OPTIONS,
          showOnInit: true,
          possibleOptions: !_.isUndefined(result.routeGroups) ?
            getRouteGroups(result.routeGroups) : getRouteGroups,
          transformFn: transformRouteGroup,
          selectedOptions: [],
          mainTitle: 'Route Grouping',
          key: 'ids',
          presetKey: 'routeGroupIds',
        },
        {
          type: nvBackendFilterUtils.FILTER_BOOLEAN,
          showOnInit: true,
          booleanModel: '01',
          mainTitle: 'Routed',
          key: 'isRouted',
          presetKey: 'isRouted',
        },
      ];

      const txnFilters = [
        _.defaults(
          {
            type: nvBackendFilterUtils.FILTER_OPTIONS,
            searchText: {},
            mainTitle: 'Granular Order Status',
            searchBy: 'name',
            key: 'orderGranularStatusIds',
            presetKey: 'orderGranularStatusIds',
          }, getGranularStatus()),
        {
          type: nvBackendFilterUtils.FILTER_OPTIONS,
          possibleOptions: getZones,
          selectedOptions: [],
          searchText: {},
          mainTitle: 'Zone',
          searchBy: 'name',
          key: 'zoneIds',
          presetKey: 'zoneIds',
        },
        {
          type: nvBackendFilterUtils.FILTER_OPTIONS,
          possibleOptions: getOrderTypes(),
          selectedOptions: [],
          searchText: {},
          mainTitle: 'Order Type',
          searchBy: 'name',
          key: 'orderTypeIds',
          presetKey: 'orderTypeIds',
        },
        {
          type: nvBackendFilterUtils.FILTER_OPTIONS,
          possibleOptions: getTransactionTypes(),
          selectedOptions: [],
          searchText: {},
          mainTitle: 'PP/DD Leg',
          searchBy: 'name',
          key: 'typeIds',
          presetKey: 'typeIds',
        },
        {
          type: nvBackendFilterUtils.FILTER_OPTIONS,
          possibleOptions: getTransactionStatus(),
          selectedOptions: [],
          searchText: {},
          mainTitle: 'Transaction Status',
          searchBy: 'name',
          key: 'statusIds',
          presetKey: 'statusIds',
        },
        {
          type: nvBackendFilterUtils.FILTER_BOOLEAN,
          booleanModel: '01',
          mainTitle: 'RTS',
          key: 'isRTS',
          presetKey: 'isRTS',
        },
        {
          type: nvBackendFilterUtils.FILTER_NUMBER,
          mainTitle: 'Weight',
          options: {
            type: nvFilterNumberBoxService.FILTER_TYPES.RANGE.value,
            from: 0.00,
            to: 999.99,
            unit: 'kg',
            operator: nvFilterNumberBoxService.FILTER_OPERATORS.GT.value,
            operand: 0.00,
            extra: {
              decimal: 2,
              minValue: 0.00,
              maxValue: 999.99,
            },
          },
          key: 'weight',
          presetKey: 'weight',
          transformFn: transformWeight,
        },
        {
          type: nvBackendFilterUtils.FILTER_OPTIONS,
          possibleOptions: getParcelSize(),
          selectedOptions: [],
          searchText: {},
          mainTitle: 'Parcel Size',
          key: 'orderSizeIds',
          presetKey: 'orderSizeIds',
        },
        {
          type: nvBackendFilterUtils.FILTER_OPTIONS,
          possibleOptions: getTimeslots(),
          selectedOptions: [],
          searchText: {},
          mainTitle: 'Timeslots',
          key: 'timeslotIds',
          presetKey: 'timeslotIds',
        },
        {
          type: nvBackendFilterUtils.FILTER_OPTIONS,
          possibleOptions: getDeliveryType(),
          selectedOptions: [],
          searchText: {},
          mainTitle: 'Delivery Type',
          key: 'deliveryTypeIds',
          presetKey: 'deliveryTypeIds',
        },
        {
          type: nvBackendFilterUtils.FILTER_NUMBER,
          mainTitle: 'Priority Level',
          options: {
            type: nvFilterNumberBoxService.FILTER_TYPES.RANGE.value,
            from: 0,
            to: 999,
            operator: nvFilterNumberBoxService.FILTER_OPERATORS.GT.value,
            operand: 0,
            extra: {
              decimal: 0,
              minValue: 0,
              maxValue: 999,
            },
          },
          key: 'priorityLevel',
          presetKey: 'priorityLevel',
          transformFn: transformPriorityLevel,
        },
        {
          type: nvBackendFilterUtils.FILTER_OPTIONS,
          possibleOptions: getDnrs(),
          selectedOptions: [],
          searchText: {},
          mainTitle: 'DNR Group',
          key: 'dnrIds',
          presetKey: 'dnrIds',
        },
        {
          type: nvBackendFilterUtils.FILTER_OPTIONS,
          possibleOptions: getBulkyTypes(),
          selectedOptions: [],
          searchText: {},
          mainTitle: 'Bulky Types',
          key: 'bulkyTypes',
          presetKey: 'bulkyTypes',
        },
      ];

      const rxnFilters = [
        {
          type: nvBackendFilterUtils.FILTER_OPTIONS,
          possibleOptions: getPickUpSize(),
          selectedOptions: [],
          searchText: {},
          mainTitle: 'Pick Up Size',
          key: 'pickupSize',
          presetKey: 'approxVolumeValues',
        },
        _.defaults({
          type: nvBackendFilterUtils.FILTER_OPTIONS,
          searchText: {},
          mainTitle: 'Reservation Type',
          key: 'reservationTypeValue',
          presetKey: 'reservationTypeIds',
        }, getReservationType()),
        {
          type: nvBackendFilterUtils.FILTER_OPTIONS,
          possibleOptions: getReservationStatus(),
          selectedOptions: [],
          searchText: {},
          mainTitle: 'Reservation Status',
          key: 'statusValue',
          presetKey: 'reservationStatusIds',
        },
      ];

      return {
        generalFilters: generalFilters,
        txnFilters: txnFilters,
        rxnFilters: rxnFilters,
      };
    }

    function transformDate(date, type) {
      if (type === 'from') {
        const startDate = nvDateTimeUtils.toSOD(date, nvTimezone.getOperatorTimezone());
        return nvDateTimeUtils.displayISO(startDate, 'utc');
      }
      if (type === 'to') {
        const endDate = nvDateTimeUtils.toEOD(date, nvTimezone.getOperatorTimezone());
        return nvDateTimeUtils.displayISO(endDate, 'utc');
      }

      return _.noop();
    }

    function transformTime(date) {
      return nvDateTimeUtils.displayISO(date, 'utc');
    }

    function getShippers(text) {
      return Shippers.filterSearch(
        text,
        _.isUndefined(this.getSelectedOptions) ? [] : this.getSelectedOptions()
      );
    }

    function getMasterShippers() {
      return Shippers.readAll({ is_marketplace: true }).then(response =>
        _(response)
          .map(shipper => _.defaults(shipper, { displayName: `${shipper.id}-${shipper.name}` }))
          .value()
      );
    }

    function getDistributionPoints(distributionPoints = null) {
      if (_.size(distributionPoints) > 0) {
        return processDistributionPoints(distributionPoints);
      }

      return DistributionPoint.listDPs({ is_active: true, is_public: true }).then(res =>
        processDistributionPoints(res)
      );

      function processDistributionPoints(theDistributionPoints) {
        return _(theDistributionPoints)
          .map(dp => _.defaults(dp, { displayName: `${dp.id}-${dp.name}` }))
          .concat({ id: -1, global_id: -1, dpms_id: -1, displayName: 'Not DP' })
          .value();
      }
    }

    function getRouteGroups(routeGroups = null) {
      if (_.size(routeGroups) > 0) {
        return $q.when(processRouteGroups(routeGroups));
      }

      return RouteGroup.read().then(res =>
        processRouteGroups(res.routeGroups)
      );

      function processRouteGroups(theRouteGroups) {
        return _(theRouteGroups)
          .map(rg => _.defaults(rg, { displayName: `${rg.id}-${rg.name}` }))
          .concat({ id: -1, displayName: 'Not in RG', transactionIds: [-1], reservationIds: [-1] })
          .value();
      }
    }

    function getSelectedOptions(filters, presetKey) {
      const filter = _.find(filters, ['presetKey', presetKey]);
      return (filter && filter.selectedOptions) || [];
    }

    function getZones() {
      return Zone.read();
    }

    function getPickUpSize() {
      return _.map(Reservation.volumeOptions, vol => ({ id: vol, displayName: vol }));
    }

    function getOrderTypes() {
      return _.cloneDeep(Transaction.fields.orderType.values);
    }

    function getTransactionTypes() {
      return _.cloneDeep(Transaction.fields.type.values);
    }

    function getTransactionStatus() {
      return _.cloneDeep(Transaction.fields.status.values);
    }

    function getParcelSize() {
      return _.cloneDeep(Order.PARCEL_SIZE);
    }

    function getTimeslots() {
      return _.cloneDeep(Waypoint.TIMEWINDOW_ID);
    }

    function getDeliveryType() {
      return _.cloneDeep(Order.DELIVERY_TYPE);
    }

    function getBulkyTypes() {
      return _.cloneDeep(Order.BULKY_TYPES);
    }

    function getDnrs() {
      return _.map(Transaction.DNR, (code, key) => ({
        id: code,
        displayName: `${code} - ${_.startCase(key)}`,
      }));
    }

    function getGranularStatus() {
      const granularStatus = _.cloneDeep(Transaction.fields.granularStatus.values);

      // Completed, Returned to Sender, Cancelled
      const excludeGranularStatusIds = [8, 11, 12];
      const defaultGranularStatus = [];

      nvCoupledListUtils.transfer(granularStatus, defaultGranularStatus, status =>
        _.findIndex(excludeGranularStatusIds, id => id === status.id) === -1);

      return {
        selectedOptions: defaultGranularStatus,
        possibleOptions: granularStatus,
      };
    }

    function getReservationType() {
      const options = Reservation.typeOptions;
      return {
        selectedOptions: [options[0]],
        possibleOptions: _.filter(options, t => t.id !== 0),
      };
    }

    function getReservationStatus() {
      return _.cloneDeep(Reservation.statusOptions);
    }

    function transformMasterShippers(masterShipper) {
      return masterShipper.id;
    }

    function transformRouteGroup(routeGroup) {
      return routeGroup;
    }

    function transformWeight(options) {
      // initial condition: options is valid
      if (options.type.code === nvFilterNumberBoxService.FILTER_TYPES.RANGE.value.code) {
        return { type: 'range', from: options.from, to: options.to };
      } else if (options.type.code === nvFilterNumberBoxService.FILTER_TYPES.VALUE.value.code) {
        return { type: 'value', operator: options.operator.name, operand: options.operand };
      }
      return null;
    }

    function transformPriorityLevel(options) {
      // initial condition: options is valid
      if (options.type.code === nvFilterNumberBoxService.FILTER_TYPES.RANGE.value.code) {
        return { type: 'range', from: options.from, to: options.to };
      } else if (options.type.code === nvFilterNumberBoxService.FILTER_TYPES.VALUE.value.code) {
        return { type: 'value', operator: options.operator.name, operand: options.operand };
      }
      return null;
    }
  }
}());
