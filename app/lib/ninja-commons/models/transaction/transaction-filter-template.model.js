(function model() {
  angular
    .module('nvCommons.models')
    .factory('TransactionFilterTemplate', TransactionFilterTemplate);

  TransactionFilterTemplate.$inject = ['$http', 'nvRequestUtils'];

  function TransactionFilterTemplate($http, nvRequestUtils) {
    return {

      create: data => $http
        .post('core/transaction-filter-templates', data)
        .then(nvRequestUtils.success, nvRequestUtils.nvFailure),

      read: () => $http
        .get('core/transaction-filter-templates')
        .then(nvRequestUtils.success, nvRequestUtils.nvFailure),

      delete: id => $http
        .delete(`core/transaction-filter-templates/${id}`)
        .then(nvRequestUtils.success, nvRequestUtils.nvFailure),

      update: (id, data) => $http
        .put(`core/transaction-filter-templates/${id}`, data)
        .then(nvRequestUtils.success, nvRequestUtils.nvFailure),

      checkNameAvailability: name => $http
        .get(`core/transaction-filter-templates/${name}/availability`)
        .then(nvRequestUtils.success, nvRequestUtils.nvFailure),

      toOptions: data => _(data)
        .map(template => ({ displayName: template.name, value: template.id }))
        .orderBy('value', 'desc')
        .value(),

    };
  }
}());
