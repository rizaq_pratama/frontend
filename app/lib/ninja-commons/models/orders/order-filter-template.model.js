(function model() {
  angular
    .module('nvCommons.models')
    .factory('OrderFilterTemplate', OrderFilterTemplate);

  OrderFilterTemplate.$inject = ['$http', 'nvRequestUtils'];

  function OrderFilterTemplate($http, nvRequestUtils) {
    return {

      create: data => $http
        .post('core/order-filter-templates', data)
        .then(nvRequestUtils.success, nvRequestUtils.nvFailure),

      read: () => $http
        .get('core/order-filter-templates')
        .then(nvRequestUtils.success, nvRequestUtils.nvFailure),

      delete: id => $http
        .delete(`core/order-filter-templates/${id}`)
        .then(nvRequestUtils.success, nvRequestUtils.nvFailure),

      update: (id, data) => $http
        .put(`core/order-filter-templates/${id}`, data)
        .then(nvRequestUtils.success, nvRequestUtils.nvFailure),

      checkNameAvailability: name => $http
        .get(`core/order-filter-templates/availability?name=${name}`)
        .then(nvRequestUtils.success, nvRequestUtils.nvFailure),

      toOptions: data => _(data)
        .map(template => ({ displayName: template.name, value: template.id }))
        .orderBy('value', 'desc')
        .value(),

    };
  }
}());
