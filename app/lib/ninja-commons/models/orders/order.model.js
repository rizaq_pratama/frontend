(function model() {
  angular
    .module('nvCommons.models')
    .factory('Order', Orders);

  Orders.$inject = [
    'nvRequestUtils', 'nvDateTimeUtils', 'nvTimezone', 'Transaction', '$http',
    '$q', 'PDF',
  ];

  function Orders(
    nvRequestUtils, nvDateTimeUtils, nvTimezone, Transaction, $http,
    $q, PDF
  ) {
    const fields = {
      id: { displayName: 'ID' },
      trackingId: { displayName: 'commons.tracking-id' },
      fromName: { displayName: 'commons.from-name' },
      fromContact: { displayName: 'commons.from-contact' },
      fromAddress1: { displayName: 'commons.from-address1' },
      fromAddress2: { displayName: 'commons.from-address2' },
      fromPostcode: { displayName: 'commons.from-postcode' },
      toName: { displayName: 'commons.to-name' },
      toContact: { displayName: 'commons.to-contact' },
      toAddress1: { displayName: 'commons.to-address1' },
      toAddress2: { displayName: 'commons.to-address2' },
      toPostcode: { displayName: 'commons.to-postcode' },
      granularStatus: { displayName: 'commons.model.granular-status' },
      createdAt: { displayName: 'Created On' },
      updatedAt: { displayName: 'Updated On' },
      deletedAt: { displayName: 'Deleted On' },
    };
    const STATUS = {
      STAGING: 'Staging',
      PENDING: 'Pending',
      TRANSIT: 'Transit',
      COMPLETED: 'Completed',
      PICKUP_FAIL: 'Pickup fail',
      DELIVERY_FAIL: 'Delivery fail',
      CANCELLED: 'Cancelled',
      ON_HOLD: 'On Hold',
    };
    const GRANULAR_STATUS = {
      STAGING: 'Staging',
      PENDING_PICKUP: 'Pending Pickup',
      PENDING_PICKUP_AT_DISTRIBUTION_POINT: 'Pending Pickup at Distribution Point',
      VAN_ENROUTE_TO_PICKUP: 'Van en-route to pickup',
      ENROUTE_TO_SORTING_HUB: 'En-route to Sorting Hub',
      PICKUP_FAIL: 'Pickup fail',
      ARRIVED_AT_SORTING_HUB: 'Arrived at Sorting Hub',
      ON_VEHICLE_FOR_DELIVERY: 'On Vehicle for Delivery',
      COMPLETED: 'Completed',
      PENDING_RESCHEDULE: 'Pending Reschedule',
      TRANSFERRED_TO_3PL: 'Transferred to 3PL',
      RETURNED_TO_SENDER: 'Returned to Sender',
      CANCELLED: 'Cancelled',
      ARRIVED_AT_DISTRIBUTION_POINT: 'Arrived at Distribution Point',
      ARRIVED_AT_ORIGIN_HUB: 'Arrived at Origin Hub',
      CROSS_BORDER_IN_TRANSIT: 'Cross Border Transit',
      CROSS_BORDER_CUSTOMS_CLEARED: 'Customs Cleared',
      CROSS_BORDER_CUSTOMS_HELD: 'Customs Held',
      RETURNED_TO_WAREHOUSE: 'Returned to Warehouse',
      REMOVED: 'Removed',
      ON_HOLD: 'On Hold',
    };
    const TYPE = {
      NORMAL: 'Normal',
      RETURN: 'Return',
      C2C: 'C2C',
    };

    const PORTATION = {
      IMPORT: 'Import',
      EXPORT: 'Export',
    };

    const CHARGE_TO = {
      RECIPIENT: 'RECIPIENT',
      SENDER: 'SENDER',
    };

    const SOURCE_ID = {
      SINGLE: 0,
      MULTI: 1,
      CSV: 2,
      API: 3,
      SHOPIFY: 4,
      NCO: 5,
      HYPERLOCAL: 6,
    };

    const DELIVERY_TYPE = [
      { id: 1, displayName: '(1) 1 Day - Anytime' },
      { id: 2, displayName: '(2) 3 Days - Anytime' },
      { id: 3, displayName: '(3) 7 Days Anytime' },
      { id: 4, displayName: '(4) Sameday' },
      { id: 5, displayName: '(5) 1 Day Timeslot' },
      { id: 6, displayName: '(6) International' },
      { id: 7, displayName: '(7) 1 Day - Day/Night' },
      { id: 8, displayName: '(8) 3 Days - Day/Night' },
      { id: 9, displayName: '(9) 3 Days - Timeslot' },
      { id: 10, displayName: '(10) Return 1 Day - Timeslot' },
      { id: 11, displayName: '(11) Return 1 Day - Day/Night' },
      { id: 12, displayName: '(12) Return 1 Day - Anytime' },
      { id: 13, displayName: '(13) C2C 1 Day - Anytime' },
      { id: 14, displayName: '(14) C2C 1 Day - Timeslot' },
      { id: 15, displayName: '(15) C2C 1 Day - Day/Night' },
      { id: 16, displayName: '(16) C2C 3 Days - Anytime' },
      { id: 17, displayName: '(17) C2C 3 Days - Day/Night' },
      { id: 18, displayName: '(18) C2C 3 Days - Timeslot' },
      { id: 19, displayName: '(19) C2C Sameday' },
      { id: 20, displayName: '(20) Normal 2 Days Anytime' },
      { id: 21, displayName: '(21) Normal 2 Days Day/Night' },
      { id: 22, displayName: '(22) Normal 2 Days Timeslot' },
      { id: 23, displayName: '(23) Return 2 Days Anytime' },
      { id: 24, displayName: '(24) Return 2 Days Day/Night' },
      { id: 25, displayName: '(25) Return 2 Days Timeslot' },
      { id: 26, displayName: '(26) C2C 2 Days Anytime' },
      { id: 27, displayName: '(27) C2C 2 Days Day/Night' },
      { id: 28, displayName: '(28) C2C 2 Days Timeslot' },
      { id: 29, displayName: '(29) Return 3 Days Anytime' },
      { id: 30, displayName: '(30) Return 3 Days Day/Night' },
      { id: 31, displayName: '(31) Return 3 Days Timeslot' },
    ];

    const PARCEL_SIZE = [
      {
        id: 0,
        displayName: 'Small',
        displayNameShort: 'commons.model.size-small-short',
        capacity: 1,
        itemSize: 0,
        enumValue: 'SMALL',
      },
      {
        id: 1,
        displayName: 'Medium',
        displayNameShort: 'commons.model.size-medium-short',
        capacity: 10,
        itemSize: 1,
        enumValue: 'MEDIUM',
      },
      {
        id: 2,
        displayName: 'Large',
        displayNameShort: 'commons.model.size-large-short',
        capacity: 20,
        itemSize: 5,
        enumValue: 'LARGE',
      },
      {
        id: 3,
        displayName: 'Extra Large',
        displayNameShort: 'commons.model.size-extra-large-short',
        capacity: 30,
        itemSize: 5,
        enumValue: 'EXTRALARGE',
      },
      {
        id: 4,
        displayName: 'Bulky (XXL)',
        displayNameShort: 'commons.model.size-bulky-short',
        capacity: 40,
        itemSize: 5,
        enumValue: 'XXLARGE',
      },
    ];

    const BULKY_TYPES = [
      { displayName: 'Regular', id: 'REGULAR' },
      { displayName: 'Regular+', id: 'REGULARPLUS' },
      { displayName: 'Premium', id: 'PREMIUM' },
    ];

    const ELASTIC_SEARCH_MATCH_TYPE = {
      EXACT: 'exact',
      FULL_TEXT: 'full_text', // wildcard search
    };

    const self = {
      isInternational: function isInternational(order) {
        return order.deliveryType === 'INTERNATIONAL';
      },
      isDeliverySameDay: function isDeliverySameDay(order) {
        return order.deliveryType === 'DELIVERY_SAME_DAY';
      },
      isC2CSameDay: function isC2CSameDay(order) {
        return order.deliveryType === 'C2C_SAME_DAY';
      },
      getStatusEnum: function getStatusEnum(statusValue) {
        let statusEnum = null;
        _.forEach(STATUS, (value, key) => {
          if (_.toLower(value) === _.toLower(statusValue)) {
            statusEnum = key;
          }
        });

        return statusEnum;
      },
      isStatusEnum: function isStatusEnum(order, statusValue) {
        return order && order.status === self.getStatusEnum(statusValue);
      },
      getGranularStatusEnum: function getGranularStatusEnum(granularStatusValue) {
        let granularStatusEnum = null;
        _.forEach(GRANULAR_STATUS, (value, key) => {
          if (_.toLower(value) === _.toLower(granularStatusValue)) {
            granularStatusEnum = key;
          }
        });

        return granularStatusEnum;
      },
      isGranularStatusEnum: function isGranularStatusEnum(order, granularStatusValue) {
        return order && order.granularStatus === self.getGranularStatusEnum(granularStatusValue);
      },
      isGranularStatusesEnum: function isGranularStatusesEnum(order, granularStatusesValue) {
        let same = false;
        _.forEach(granularStatusesValue, (gran) => {
          same = same ||
            (order && order.granularStatus === self.getGranularStatusEnum(gran));
        });
        return same;
      },
      isTypeEnum: function isTypeEnum(order, typeValue) {
        return order && order.type === getTypeEnum(typeValue);

        function getTypeEnum(theTypeValue) {
          let typeEnum = null;
          _.forEach(TYPE, (value, key) => {
            if (_.toLower(value) === _.toLower(theTypeValue)) {
              typeEnum = key;
            }
          });

          return typeEnum;
        }
      },
      getParcelSizeByEnum: function getParcelSizeByEnum(enumValue) {
        let parcelSize = null;
        _.forEach(PARCEL_SIZE, (ps) => {
          if (ps.enumValue === enumValue) {
            parcelSize = ps;
            return false;
          }

          return undefined;
        });

        return parcelSize;
      },
      isUpdateOrderSuccess: (response) => {
        const status = _.get(response, 'status');
        return status && status.toLowerCase() === 'success';
      },
      isCorrectStatusToRTS: (order, ddnt) => {
        let forceReschedule = false;
        if (ddnt && ddnt.routeId && Transaction.isPending(ddnt)) {
          forceReschedule = true;
        }

        return (self.isStatusEnum(order, STATUS.TRANSIT) &&
          self.isGranularStatusEnum(order, GRANULAR_STATUS.ON_VEHICLE_FOR_DELIVERY)
          && forceReschedule) ||
          self.isStatusEnum(order, STATUS.DELIVERY_FAIL) ||
          self.isGranularStatusEnum(order, GRANULAR_STATUS.ARRIVED_AT_SORTING_HUB) ||
          self.isGranularStatusEnum(order, GRANULAR_STATUS.ARRIVED_AT_ORIGIN_HUB) ||
          self.isGranularStatusEnum(order, GRANULAR_STATUS.TRANSFERRED_TO_3PL) ||
          self.isGranularStatusEnum(order, GRANULAR_STATUS.ENROUTE_TO_SORTING_HUB);
      },
      search: function search(options) {
        const params = _.defaults(options, {
          noOfRecords: 100,
          page: 1,
          shipperIds: [],
          statusIds: [],
          granularStatusIds: [],
          oneMonthSearch: true,
          createTimeFrom: null, // eg: "2016-08-19T02:53:44+00:00"
          createTimeTo: null, // eg: "2016-08-19T02:53:44+00:00"
          keyword: null, // eg: "d9230"
          withTransactions: false,
        });

        return $http.post('core/orders/search', params)
          .then(nvRequestUtils.success, nvRequestUtils.nvFailure);
      },
      elasticSearch: function elasticSearch(data, urlParams = {}) {
        const theData = _.defaults(data, {
          search_field: null,
          search_filters: [],
          search_range: null,
        });

        return $http.post(`order-search/search?${nvRequestUtils.queryParam(urlParams)}`, theData)
          .then(nvRequestUtils.success, nvRequestUtils.nvFailure);
      },
      searchSummary: function searchSummary(options) {
        const params = _.defaults(options, {
          noOfRecords: 100,
          page: 1,
          shipperIds: [],
          statusIds: [],
          granularStatusIds: [],
          oneMonthSearch: true,
          createTimeFrom: null,
          createTimeTo: null,
          keyword: null,
          withTransactions: false,
        });
        return $http.post('core/2.1/orders/search', params)
          .then(nvRequestUtils.success, nvRequestUtils.nvFailure);
      },
      searchByCsv: function searchByCsv(data) {
        return $http.upload('core/orders/search/bulk', data)
          .then(nvRequestUtils.success, nvRequestUtils.nvFailure);
      },
      get: function get(id) {
        return $http.get(`core/orders/${id}`)
          .then(nvRequestUtils.success, nvRequestUtils.nvFailure);
      },
      update: function update(id, data) {
        return $http.put(`core/orders/${id}`, data)
          .then(nvRequestUtils.success, nvRequestUtils.nvFailure);
      },
      updateV2: (id, data) => $http
        .patch(`core/2.0/orders/${id}`, data)
        .then(nvRequestUtils.success, nvRequestUtils.nvFailure),
      addToRouteV2: (id, payload, ignoreError = false) => $http
        .put(`core/2.0/orders/${id}/routes`, payload)
        .then(nvRequestUtils.success, (resp) => {
          if (!ignoreError) {
            return nvRequestUtils.nvFailure(resp);
          }
          return $q.reject(_.get(resp, 'data'));
        }),
      removeFromRouteV2: (id, payload, ignoreError = false) => $http({
        method: 'DELETE',
        url: `core/2.0/orders/${id}/routes`,
        data: payload,
        headers: { 'Content-Type': 'application/json' },
      }).then(nvRequestUtils.success, (resp) => {
        if (!ignoreError) {
          return nvRequestUtils.nvFailure(resp);
        }
        return $q.reject(_.get(resp, 'data'));
      }),

      updatePickupdetails: function updatePickupdetails(id, data) {
        return $http.put(`core/orders/${id}/pickup`, data)
          .then(nvRequestUtils.success, nvRequestUtils.nvFailure);
      },
      updateDeliverydetails: function updateDeliverydetails(id, data) {
        return $http.put(`core/orders/${id}/delivery`, data)
          .then(nvRequestUtils.success, nvRequestUtils.nvFailure);
      },
      resume: function resume(data) {
        return $http.post('core/orders/resume', data)
          .then(nvRequestUtils.success, nvRequestUtils.nvFailure);
      },
      cancel: function cancel(orderId, data = {}) {
        return $http.put(`core/2.0/orders/${orderId}/cancel`, data)
          .then(nvRequestUtils.success, nvRequestUtils.nvFailure);
      },
      deleteOrder: function deleteOrder(orderId) {
        return $http.delete(`core/orders/${orderId}`)
          .then(nvRequestUtils.success, nvRequestUtils.nvFailure);
      },
      reverify: function reverify(data) {
        _.forEach(data, (row) => {
          _.defaults(row, {
            source: 'ORDER_REVERIFY',
          });
        });

        return $http.post('core/orders/verify', data)
          .then(nvRequestUtils.success, nvRequestUtils.nvFailure);
      },
      getPODs: function getPODs(orderId) {
        return $http.get(`core/orders/${orderId}/pods`)
          .then(nvRequestUtils.success, nvRequestUtils.nvFailure);
      },
      downloadAWB: function downloadAWB(params, fileName) {
        return PDF.download(`core/dashboard/awb?${nvRequestUtils.queryParam(params)}`,
          fileName);
      },
      getTrackingId: function getTrackingId(trackingId) {
        return $http.get(`core/orders/search?scan=${trackingId}`)
          .then(nvRequestUtils.success, errorHandler);
      },
      getTrackingIdV2: function getTrackingIdV2(trackingId) {
        return $http.get(`core/v2/orders/search?scan=${trackingId}`)
          .then(nvRequestUtils.success, errorHandler);
      },
      getTrackingIdListV2: function getTrackingIdListV2(trackingId) {
        return $http.get(`core/v2/orders/searchlist?scan=${trackingId}`)
          .then(nvRequestUtils.success, errorHandler);
      },
      getTrackingIds: function getTrackingIds(trackingIds) {
        const payload = {
          orderIds: trackingIds,
        };

        return $http.post('core/ordersbyids', payload)
          .then(nvRequestUtils.success, errorHandler);
      },
      getByScan: function getByScan(scan) {
        return $http.get(`core/orders?scan=${scan}`)
          .then(nvRequestUtils.success, errorHandler);
      },
      changeDeliveryByCSV: function changeDeliveryByCSV(csvFile) {
        if (csvFile.length === 0) {
          return $q.reject('No file selected');
        }

        return $http.upload('core/orders/changedeliverydetails', csvFile[0]).then(
          nvRequestUtils.success,
          nvRequestUtils.nvFailure
        );
      },
      createByCSV: function createByCSV(csvFiles) {
        return $http.upload('core/third_party_orders', csvFiles).then(
          nvRequestUtils.success,
          nvRequestUtils.nvFailure
        );
      },
      createCombineByCSV: function createCombineByCSV(csvFiles) {
        return $http.upload('shipper-dashboard/orders-csv', csvFiles[0]).then(
          nvRequestUtils.success,
          excluding400
        );
        // Don't show error toast for 400, we handle it separately
        function excluding400(response) {
          if (response.status === 400) {
            // TODO make it to go to sad path by $q.reject.
            return $q.resolve(response.data.data.message);
          }
          return nvRequestUtils.nvFailure(response);
        }
      },
      v4CreateBatch: function v4CreateBatch() {
        return $http.post('order-create/internal/4.1/batch').then(
          nvRequestUtils.success,
          nvRequestUtils.nvFailure
        );
      },
      v4SendOrderToBatch: function v4SendOrderToBatch(batchId, shipperId, data) {
        return $http.post(`order-create/internal/4.1/batch/${batchId}/shippers/${shipperId}/orders`, data).then(
          nvRequestUtils.success,
          nvRequestUtils.nvFailure
        );
      },
      v4GetBatchStatus: function v4GetBatchStatus(batchId) {
        return $http.get(`order-create/internal/4.1/batch/${batchId}`).then(
          nvRequestUtils.success,
          nvRequestUtils.nvFailure
        );
      },
      v4GetSuccessfulOrdersFromBatch: function v4GetSuccessfulOrdersFromBatch(batchId, options) {
        const params = _.defaults(options, {
          page: 1,
          pageSize: 100,
        });

        return $http.get(`order-create/internal/4.1/batch/${batchId}/orders?${nvRequestUtils.queryParam(params)}`).then(
          nvRequestUtils.success,
          nvRequestUtils.nvFailure
        );
      },
      v4GetErrorResponseFromOrder: function v4GetErrorResponseFromOrder(batchId, asyncUUID) {
        return $http.get(`order-create/internal/4.1/batch/${batchId}/orders/${asyncUUID}/error`).then(
          nvRequestUtils.success,
          nvRequestUtils.nvFailure
        );
      },
      forceSuccess: function forceSuccess(orderId, options) {
        return $http.put(`core/orders/${orderId}/forcesuccess?${nvRequestUtils.queryParam(options)}`).then(
          nvRequestUtils.success,
          nvRequestUtils.nvFailure
        );
      },
      getAgedParcelManagementData: function getAgedParcelManagementData(params) {
        return $http.get(`core/orders/agedParcel?${nvRequestUtils.queryParam(params)}`).then(
          nvRequestUtils.success,
          nvRequestUtils.nvFailure
        );
      },
      getFailedPickupManagementData: function getFailedPickupManagementData() {
        return $http.get('core/orders/fpm').then(
          nvRequestUtils.success,
          nvRequestUtils.nvFailure
        );
      },
      reschedulePickupFailures: function reschedulePickupFailures(data) {
        return $http.post('core/reschedulepickupfailures', data).then(
          nvRequestUtils.success,
          nvRequestUtils.nvFailure
        );
      },
      getFailedDeliveryManagementData: function getFailedDeliveryManagementData() {
        return $http.get('core/orders/fdm').then(
          nvRequestUtils.success,
          nvRequestUtils.nvFailure
        );
      },
      rescheduleDeliveryFailures: function rescheduleDeliveryFailures(data) {
        return $http.put('core/orders/rescheduledeliveries', data).then(
          nvRequestUtils.success,
          nvRequestUtils.nvFailure
        );
      },
      reschedule: function reschedule(orderId, data) {
        return $http.post(`core/orders/${orderId}/reschedule`, data).then(
          nvRequestUtils.success,
          nvRequestUtils.nvFailure
        );
      },
      returnToSender: function returnToSender(orderId, data) {
        return $http.put(`core/orders/${orderId}/rts`, data).then(
          nvRequestUtils.success,
          nvRequestUtils.nvFailure
        );
      },
      deleteCod: function deleteCod(orderId) {
        return $http.delete(`core/2.0/orders/${orderId}/cod`).then(
          nvRequestUtils.success,
          nvRequestUtils.nvFailure
        );
      },
      createOrUpdateCod: function createOrUpdateCod(orderId, data) {
        return $http.put(`core/2.0/orders/${orderId}/cod`, data).then(
          nvRequestUtils.success,
          nvRequestUtils.nvFailure
        );
      },
      deleteCop: function deleteCop(orderId) {
        return $http.delete(`core/2.0/orders/${orderId}/cop`).then(
          nvRequestUtils.success,
          nvRequestUtils.nvFailure
        );
      },
      createOrUpdateCop: function createOrUpdateCop(orderId, data) {
        return $http.put(`core/2.0/orders/${orderId}/cop`, data).then(
          nvRequestUtils.success,
          nvRequestUtils.nvFailure
        );
      },
      updateStamp: function updateStamp(orderId, data) {
        return $http.put(`core/orders/${orderId}/stamp`, data)
          .then(nvRequestUtils.success, nvRequestUtils.nvFailure);
      },

      scheduleRecalculate: data =>
        $http.put('core/orders/schedule-recalculate', data).then(
          nvRequestUtils.success,
          nvRequestUtils.nvFailure
        ),

      batchRecalculate: payload => $http
        .put('core/orders/batch-recalculate', payload)
        .then(nvRequestUtils.success, nvRequestUtils.nvFailure),

      getDeliveryType: function getDeliveryType() {
        return $http.get('core/deliverytypes').then(
          nvRequestUtils.success,
          nvRequestUtils.nvFailure
        );
      },

      updateDeliveryTypes: function updateDeliveryTypes(id, data) {
        return $http.put(`core/orders/${id}/deliverytypes`, data).then(
          nvRequestUtils.success,
          nvRequestUtils.nvFailure
        );
      },
      updateMultiParcels: function updateMultiParcels(id, data) {
        return $http.put(`core/1.0/mulit-parcel-orders/${id}`, data).then(
          nvRequestUtils.success,
          nvRequestUtils.nvFailure
        );
      },

      /**
       * startDate and endDate in YYYY-MM-DD format
       */
      triggerBulkyUpdate: function triggerBulkyUpdate(startDate, endDate) {
        const startDateStr = nvDateTimeUtils.displayDate(
          startDate, nvTimezone.getOperatorTimezone()
        );
        const endDateStr = nvDateTimeUtils.displayDate(
          endDate, nvTimezone.getOperatorTimezone()
        );
        return $http.get(`core/bulky/orders/timeslots/bulk-update?start_date=${startDateStr}&end_date=${endDateStr}`)
          .then(nvRequestUtils.success, nvRequestUtils.nvFailure);
      },

      uploadAmazonOrderStatus: function uploadAmazonOrderStatus(file) {
        return $http.upload('core/amazon/upload-order-status?upload=true', file)
          .then(nvRequestUtils.success, nvRequestUtils.nvFailure);
      },
      uploadSingtelOrderStatus: function uploadSingtelOrderStatus(data) {
        return $http.post('core/core/singtel/edi/device-serial/status', data)
          .then(nvRequestUtils.success, nvRequestUtils.nvFailure);
      },

      getBulkOrderDetails: bulkId => $http.get(`core/bulkorders/${bulkId}`)
        .then(nvRequestUtils.success, nvRequestUtils.nvFailure),

      getBulkOrderCsvData: bulkId => $http.get(`core/bulkorders/${bulkId}/data`)
        .then(nvRequestUtils.success, nvRequestUtils.nvFailure),

      deleteBulkOrders: bulkId => $http.delete(`core/bulkorders/${bulkId}`)
        .then(nvRequestUtils.success, nvRequestUtils.nvFailure),

      getBatchOrderDetails: batchId => $http.get(`core/orders/bybatchid/${batchId}`)
        .then(nvRequestUtils.success, nvRequestUtils.nvFailure),

      deleteBatchOrders: batchId => $http.delete(`core/orders/bybatchid/${batchId}`)
        .then(nvRequestUtils.success, nvRequestUtils.nvFailure),

      getDnrs: () => $http
        .get('core/dnrs')
        .then(nvRequestUtils.success, nvRequestUtils.nvFailure),

      getSetAside: () => $http
        .get('core/set-aside')
        .then(nvRequestUtils.success, nvRequestUtils.nvFailure),

      lazadaCancelledBy3pl: payload => $http
        .post('core/tools/upload-rejected-parcel-list', payload)
        .then(nvRequestUtils.success, nvRequestUtils.nvFailure),

      getOrdersByTrackingIds: trackingIds => $http
        .post('core/orders/dpms/searchbyscans', { scans: trackingIds })
        .then(nvRequestUtils.success, errorHandler),

      getSetAsideCurrentCounter: () => $http.get('core/set-aside/counter/current')
        .then(nvRequestUtils.success, nvRequestUtils.nvFailure),

      pingCoreApi: () => $http
        .get('core/')
        .then(nvRequestUtils.success, nvRequestUtils.failure),

      removeFromWarehouse: function removeFromWarehouse(orderId) {
        const payload = {
          order_ids: [orderId],
        };
        return $http.post('core/orders/remove-from-warehouse', payload)
          .then(nvRequestUtils.success, nvRequestUtils.nvFailure);
      },

      downloadPod: payload => $http
        .post('core/orders/pods/generation', payload)
        .then(nvRequestUtils.success, nvRequestUtils.nvFailure),

      generatePackTrackingId: payload => $http
        .post('order-service/ninja-pack/tracking-id', payload)
        .then(nvRequestUtils.success, nvRequestUtils.nvFailure),

      /**
       * Search waypoints by ids and store it to cache
       * @param  {Array<number>} ids - waypoint ids
       * @return {Promise} Promise with array of waypoint objects
      */
      searchByWaypointIds: ids => $http
        .post('core/3.0/orders/parcels/bywaypointids', ids)
        .then(nvRequestUtils.success, nvRequestUtils.nvFailure),

      getOrderPhotos: id => $http
        .get(`core/orders/${id}/photos`)
        .then(nvRequestUtils.success, nvRequestUtils.nvFailure),

      searchByWaypointIdsInBatch: (ids, maxInChunk) => {
        const deferred = $q.defer();
        const MAX_CALL_IN_CHUNK = maxInChunk || 1000;
        const chunkIds = _.chunk(ids, MAX_CALL_IN_CHUNK);
        const promises = [];
        _.forEach(chunkIds, (theIds) => {
          promises.push(
            self.searchByWaypointIds(theIds)
          );
        });

        $q.all(promises).then(success, failure);
        return deferred.promise;

        function success(responses) {
          let waypoints = [];
          _.forEach(responses, (response) => {
            waypoints = _.concat(waypoints, response);
          });
          return deferred.resolve(waypoints);
        }
        function failure(response) {
          deferred.reject(response);
        }
      },

      /**
       * payload
       * order_id: @type {Number}
       * tags: @type {String[]}
       */
      putTags: (orderId, payload) =>
        $http.post(`core/orders/${orderId}/tags`, payload)
          .then(nvRequestUtils.success, nvRequestUtils.nvFailure),

      getTags: orderId =>
        $http.get(`core/orders/${orderId}/tags`)
          .then(nvRequestUtils.success, nvRequestUtils.nvFailure),

      deleteTags: orderId =>
        $http.get(`core/orders/${orderId}/tags`)
          .then(nvRequestUtils.success, nvRequestUtils.nvFailure),

      /**
       * payload:
       * weight: @type {Number}
       */
      updateWeight: (orderId, payload) =>
        $http.post(`core/orders/${orderId}/pricing-weight`, payload)
          .then(nvRequestUtils.success, nvRequestUtils.nvFailure),

      FIELDS: fields,
      STATUS: STATUS,
      TYPE: TYPE,
      PORTATION: PORTATION,
      CHARGE_TO: CHARGE_TO,
      SOURCE_ID: SOURCE_ID,
      GRANULAR_STATUS: GRANULAR_STATUS,
      DELIVERY_TYPE: DELIVERY_TYPE,
      PARCEL_SIZE: PARCEL_SIZE,
      BULKY_TYPES: BULKY_TYPES,
      ELASTIC_SEARCH_MATCH_TYPE: ELASTIC_SEARCH_MATCH_TYPE,
    };

    return self;

    function errorHandler(err) {
      return $q.reject(err);
    }
  }
}());
