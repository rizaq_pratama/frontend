(function model() {
  angular.module('nvCommons.models')
    .factory('OrderFilter', OrderFilter);

  OrderFilter.$inject = [
    'nvCoupledListUtils',
    'nvBackendFilterUtils',
    'nvDateTimeUtils',
    'nvTimezone',
    'Shippers',
    'Order',
    'nvTranslate',
  ];

  function OrderFilter(
    nvCoupledListUtils,
    nvBackendFilterUtils,
    nvDateTimeUtils,
    nvTimezone,
    Shippers,
    Order,
    nvTranslate
  ) {
    return {
      init: init,
    };

    async function init() {
      const generalFilters = [
        {
          type: nvBackendFilterUtils.FILTER_TIME,
          showOnInit: false,
          fromModel: moment().startOf('day').tz(nvTimezone.getOperatorTimezone()),
          toModel: moment().tz(nvTimezone.getOperatorTimezone()),
          transformFn: transformTime,
          mainTitle: nvTranslate.instant('commons.creation-time'),
          key: {
            from: 'createTimeFrom',
            to: 'createTimeTo',
          },
          presetKey: {
            from: 'createTimeFrom',
            to: 'createTimeTo',
          },
          columnName: 'created_at',
        },
        {
          type: nvBackendFilterUtils.FILTER_AUTOCOMPLETE,
          showOnInit: false,
          callback: getShippers,
          backendKey: 'legacy_ids',
          selectedOptions: [],
          mainTitle: nvTranslate.instant('commons.shipper'),
          key: 'shipperIds',
          presetKey: 'shipperIds',
          columnName: 'shipper_id',
        },
        {
          type: nvBackendFilterUtils.FILTER_OPTIONS,
          possibleOptions: await getMasterShippers(),
          showOnInit: false,
          transformFn: transformMasterShippers,
          selectedOptions: [],
          mainTitle: nvTranslate.instant('commons.master-shipper'),
          columnName: 'master_shippers',
          key: 'shipper_ids',
        },
        _.defaults({
          type: nvBackendFilterUtils.FILTER_OPTIONS,
          showOnInit: true,
          searchText: {},
          mainTitle: nvTranslate.instant('commons.status'),
          searchBy: 'name',
          key: 'statusIds',
          presetKey: 'statusIds',
          columnName: 'status',
          transformFn: getStatusIndex,
          comparator: compareStatusByIndex,
        }, getStatus()),
        _.defaults({
          type: nvBackendFilterUtils.FILTER_OPTIONS,
          showOnInit: true,
          searchText: {},
          mainTitle: nvTranslate.instant('commons.model.granular-status'),
          searchBy: 'name',
          key: 'granularStatusIds',
          presetKey: 'granularStatusIds',
          columnName: 'granular_status',
          transformFn: getGranularStatusIndex,
          comparator: compareGranularStatusByIndex,
        }, getGranularStatus()),
      ];

      return {
        generalFilters: generalFilters,
      };
    }

    function transformTime(date) {
      return nvDateTimeUtils.displayISO(date, 'utc');
    }

    function getShippers(text) {
      return Shippers.filterSearch(
        text,
        _.isUndefined(this.getSelectedOptions) ? [] : this.getSelectedOptions()
      );
    }

    function getMasterShippers() {
      return Shippers.readAll({ is_marketplace: true }).then(response =>
        _(response)
          .map(shipper => _.defaults(shipper, { displayName: `${shipper.id}-${shipper.name}` }))
          .value()
      );
    }

    function transformMasterShippers(masterShipper) {
      return masterShipper.id;
    }

    function getStatus() {
      const statuses = _.map(Order.STATUS, value =>
        ({ id: value, name: value })
      );

      const selectedStatusIds = [Order.STATUS.PENDING]; // Pending
      const defaultStatuses = [];

      nvCoupledListUtils.transfer(statuses, defaultStatuses, status =>
      _.findIndex(selectedStatusIds, id => id === status.id) >= 0);

      return {
        selectedOptions: defaultStatuses,
        possibleOptions: statuses,
      };
    }

    function getGranularStatus() {
      const granularStatuses = _.map(Order.GRANULAR_STATUS, value =>
        ({ id: value, name: value })
      );

      const selectedGranularStatusIds = [Order.GRANULAR_STATUS.PENDING_PICKUP]; // Pending Pickup
      const defaultGranularStatus = [];

      nvCoupledListUtils.transfer(granularStatuses, defaultGranularStatus, status =>
      _.findIndex(selectedGranularStatusIds, id => id === status.id) >= 0);

      return {
        selectedOptions: defaultGranularStatus,
        possibleOptions: granularStatuses,
      };
    }

    function getStatusIndex(option) {
      const statuses = _.values(Order.STATUS);
      return _.indexOf(statuses, option.id);
    }

    function getGranularStatusIndex(option) {
      const granularStatuses = _.values(Order.GRANULAR_STATUS);
      return _.indexOf(granularStatuses, option.id);
    }

    function compareStatusByIndex(option, index) {
      const statuses = _.values(Order.STATUS);
      return option.id === statuses[index];
    }

    function compareGranularStatusByIndex(option, index) {
      const granularStatuses = _.values(Order.GRANULAR_STATUS);
      return option.id === granularStatuses[index];
    }
  }
}());
