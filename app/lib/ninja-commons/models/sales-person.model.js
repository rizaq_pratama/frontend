(function model() {
  angular
    .module('nvCommons.models')
    .factory('SalesPerson', SalesPerson);

  SalesPerson.$inject = ['$http', 'nvRequestUtils'];

  function SalesPerson($http, nvRequestUtils) {
    return {
      readAll: () => $http
        .get('core/1.0/sales')
        .then(nvRequestUtils.success, nvRequestUtils.nvFailure),
      read: id => $http
        .get(`core/1.0/sales/${id}`)
        .then(nvRequestUtils.success, nvRequestUtils.nvFailure),
      create: data => $http
        .post('core/1.0/sales', data)
        .then(nvRequestUtils.success, nvRequestUtils.nvFailure),
      update: (id, data) => $http
        .patch(`core/1.0/sales/${id}`, data)
        .then(nvRequestUtils.success, nvRequestUtils.nvFailure),
    };
  }
}());
