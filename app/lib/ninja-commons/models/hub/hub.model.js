(function hubModel() {
  angular
    .module('nvCommons.models')
    .factory('Hub', Hub);

  Hub.$inject = ['$http', 'nvRequestUtils', 'nvModelUtils', 'nvExtendUtils', '$q'];

  function Hub($http, nvRequestUtils, nvModelUtils, nvExtendUtils, $q) {
    const fields = {
      id: { displayName: 'container.hub-list.hub-id' },
      name: { displayName: 'container.hub-list.hub-name' },
      short_name: { displayName: 'container.hub-list.display-name' },
      city: { displayName: 'container.hub-list.city' },
      country: { displayName: 'container.hub-list.country' },
      latitude: { displayName: 'container.hub-list.latitude' },
      longitude: { displayName: 'container.hub-list.longitude' },
      createdAt: { displayName: 'container.hub-list.created-on' },
      updatedAt: { displayName: 'container.hub-list.updated-on' },
      deletedAt: { displayName: 'container.hub-list.deleted-on' },
      active: { displayName: 'commons.active' },
      facility_type: { displayName: 'container.hub-list.facility-type' },
    };

    const hubGroupFields = {
      id: { displayName: 'container.hub-group-management.id' },
      name: { displayName: 'container.hub-group-management.hub-group-name' },
      hubs: { displayName: 'container.hub-group-management.hubs' },
    };

    const hubFacilityTypes = [
      { displayName: 'container.hub-list.hub-facility-crossdock', value: 'CROSSDOCK' },
      { displayName: 'container.hub-list.hub-facility-station', value: 'STATION' },
      { displayName: 'container.hub-list.hub-facility-dp', value: 'DP' },
      { displayName: 'container.hub-list.hub-facility-recovery', value: 'RECOVERY' },
      { displayName: 'container.hub-list.hub-facility-others', value: 'OTHERS' },
    ];
    let HUB_MAP = {};

    return {
      create: options =>
        $http.post('core/hubs', options).then(
          nvRequestUtils.success,
          nvRequestUtils.nvFailure
        ).then(nvExtendUtils.addLatLng),

      /**
       * @param options.active_only boolean
       */
      read: (options = {}) => {
        _.defaults(options, { active_only: true });
        const deferred = $q.defer();

        $http.get(`core/hubs?${nvRequestUtils.queryParam(options)}`)
          .then(success, failure);

        return deferred.promise;

        function success(response) {
          const datas = response.data;
          const RESULTS = nvExtendUtils.addLatLng(datas);
          // replace all value inside map
          HUB_MAP = _.keyBy(RESULTS, 'id');
          deferred.resolve(RESULTS);
        }

        function failure(response) {
          deferred.reject(response.data.data || response.data.error);
        }
      },

      searchByIds: (ids) => {
        const RESULTS = [];
        const idsToSearch = [];

        _.forEach(_.uniq(_.compact(_.castArray(ids))), (id) => {
          if (HUB_MAP[id]) {
            RESULTS.push(HUB_MAP[id]);
          } else {
            idsToSearch.push(id);
          }
        });

        if (_.size(idsToSearch) <= 0) {
          return $q.when(RESULTS);
        }

        const deferred = $q.defer();
        $http.get('core/hubs')
          .then(success, failure);
        return deferred.promise;

        function success(response) {
          const hubs = nvExtendUtils.addLatLng(response.data);
          const notFoundIds = [];

          _.forEach(idsToSearch, (id) => {
            const theHub = _.find(hubs, { id: id });
            if (theHub) {
              HUB_MAP[id] = theHub;
              RESULTS.push(theHub);
            } else {
              notFoundIds.push(id);
            }
            deferred.resolve(RESULTS);
          });
        }

        function failure(response) {
          deferred.reject(response.data.data || response.data.error);
        }
      },

      update: (options) => {
        const data = _.omit(options, 'id');
        return $http.put(`core/hubs/${options.id}`, data).then(
                  nvRequestUtils.success,
                  nvRequestUtils.nvFailure
              ).then(nvExtendUtils.addLatLng);
      },

      delete: options =>
        $http.delete(`core/hubs/${options.id}`).then(
          nvRequestUtils.success,
          nvRequestUtils.nvFailure
        ),

      refresh: () =>
        $http.get('hub/hubs/reload').then(
          nvRequestUtils.success,
          nvRequestUtils.nvFailure
        ),

      getRouteGroupOfHub: hub =>
        $http.get(`hub/1.0/hubs/${hub.id}/route-groups`).then(
          nvRequestUtils.success,
          nvRequestUtils.nvFailure
        ),

      getRouteGroupOfHubs: () =>
        $http.get('hub/1.0/hubs/route-groups').then(
          nvRequestUtils.success,
          nvRequestUtils.nvFailure
        ),

      addRouteGroupsToHub: (hubId, routeGroupIds) =>
        $http.put(`hub/1.0/hubs/${hubId}/route-groups`,
          {
            route_group_ids: _.castArray(routeGroupIds),
          }).then(
          nvRequestUtils.success,
          nvRequestUtils.nvFailure
        ),

      removeRouteGroupsFromHub: (hubId, routeGroupIds) =>
        $http.delete2(`hub/1.0/hubs/${hubId}/route-groups`,
          {
            route_group_ids: _.castArray(routeGroupIds),
          }).then(
          nvRequestUtils.success,
          nvRequestUtils.nvFailure
        ),
      /**
      * Get hub group by hub group id
      * @param {Number} hubGroupId - id of hub group
      */
      getHubGroup: hubGroupId => (
        $http.get(`core/hub-groups/${hubGroupId}`)
        .then(nvRequestUtils.success, nvRequestUtils.nvFailure)
      ),
      /**
       * Get all hub group
       */
      getHubGroups: () => (
        $http.get('core/hub-groups')
          .then(nvRequestUtils.success, nvRequestUtils.nvFailure)
      ),
      /**
       * create hub group
       * @param {Object} payload - payload object
       * @param {String} payload.name - hub group name
       * @param {Number[]} payload.hub_ids - hub ids
       */
      createHubGroup: payload => (
        $http.post('core/hub-groups', payload)
          .then(nvRequestUtils.success, nvRequestUtils.nvFailure)
      ),

      /**
       * update hub group
       * @param {Object} payload - payload object
       * @param {String} payload.name - hub group name
       * @param {Number[]} payload.hub_ids - hub ids
       */
      updateHubGroup: (payload, hubId) => (
        $http.put(`core/hub-groups/${hubId}`, payload)
          .then(nvRequestUtils.success, nvRequestUtils.nvFailure)
      ),

      /**
       * Delete hub group by hub group id
       * @param {Number} hubGroupId - Hub group id to delete
       */
      deleteHubGroup: hubGroupId => (
        $http.delete(`core/hub-groups/${hubGroupId}`)
          .then(nvRequestUtils.success, nvRequestUtils.nvFailure)
      ),

      toOptions: data =>
        _(data)
          .map(hub => ({ displayName: hub.name, value: hub.id }))
          .sortBy('value')
          .value(),

      toOptionsWithId: data =>
        _(data)
          .map(hub => ({ displayName: `${hub.id} - ${hub.name}`, value: hub.id }))
          .sortBy('value')
          .value(),
      toOptionsWithObjectValue: data =>
          _(data)
            .map(hub => ({ displayName: hub.name, value: hub }))
            .sortBy('value')
            .value(),

      getFacilityTypeName: facilityType =>
        _.get(_.find(hubFacilityTypes, item => item.value === facilityType), 'displayName', 'UNKNOWN'),
      getAddFields: () =>
        nvModelUtils.pickFields(fields, ['name', 'short_name', 'city', 'country', 'longitude', 'latitude']),

      getEditFields: model =>
        nvModelUtils.pickFields(fields, [
          'id', 'name', 'short_name', 'city', 'country', 'latitude', 'longitude', 'active',
        ], { setValues: true, model: model }),
      HUB_GROUP_FIELDS: hubGroupFields,
      HUB_FACILITY_TYPES: hubFacilityTypes,
    };
  }
}());
