(function Overwatch() {
  angular
    .module('nvCommons.models')
    .factory('Outbound', Outbound);

  Outbound.$inject = ['$http', 'nvRequestUtils'];

  function Outbound($http, nvRequestUtils) {
    return {
      getLimit: () => $http.get('core/outbound/percentage/limit')
        .then(nvRequestUtils.success, nvRequestUtils.nvFailure),
      updateLimit: limit => $http.post('core/outbound/percentage', { percentage: limit })
        .then(nvRequestUtils.success, nvRequestUtils.nvFailure),
      confirmScans: (routeId, trackingId, hubId) => {
        const payload = [{
          excludedScans: [],
          missingScans: [],
          routeId: routeId,
          successScans: [trackingId.trim().toUpperCase()],
          hubId: hubId,
        }];
        return $http.post('core/outbound/confirmScans', payload)
          .then(nvRequestUtils.success, nvRequestUtils.nvFailure);
      },
    };
  }
}());
