(function Overwatch() {
  angular
    .module('nvCommons.models')
    .factory('Inbound', Inbound);

  Inbound.$inject = ['$http', 'nvRequestUtils', '$q'];

  function Inbound($http, nvRequestUtils, $q) {
    const TYPE = {
      VAN_FROM_SHIPPER: 1,
      SORTING_HUB: 2,
      TRUCK: 3,
      VAN_FROM_NINJAVAN: 4,
      SORTING_DISTRIBUTION_POINT_HUB: 5,
    };

    const STATUS = {
      ORDER_COMPLETED: 'Order Completed',
      ORDER_NOT_FOUND: 'Order not found',
      ORDER_RETURNED_TO_SENDER: 'Order Returned To Sender',
      ORDER_CANCELLED: 'Order Cancelled',
      ORDER_TRANSFERRED_TO_3PL: 'Order Transferred to 3PL',
      PENDING_DELIVERY: 'Pending Delivery',
      SUCCESSFUL_INBOUND: 'Inbound Success',
      ON_HOLD: 'On Hold',
    };

    function searchByRouteIds (ids, scanTypes) {
      const input = {
        route_ids: ids,
      };

      if (!_.isUndefined(scanTypes)) {
        input.scan_types = scanTypes;
      }

      const data = _.defaults(input, {
        scan_types: [
          'TRUCK',
          'VAN_FROM_NINJAVAN',
        ],
      });
      return $http.post('core/routes/inbound-scans/latest', data);
    }

    return {
      TYPE: TYPE,
      STATUS: STATUS,

      getTypeEnum: function getTypeEnum(value) {
        let typeEnum = null;
        _.forEach(TYPE, (typeValue, key) => {
          if (typeValue === value) {
            typeEnum = key;
            return false;
          }

          return _.noop();
        });

        return typeEnum;
      },

      getStatusEnum: function getStatusEnum(value) {
        let statusEnum = null;
        _.forEach(STATUS, (statusValue, key) => {
          if (statusValue === value) {
            statusEnum = key;
            return false;
          }

          return _.noop();
        });

        return statusEnum;
      },
      /**
       * handling for weight tolerance
       * use the new calculation for threshold
       */
      calculateWeightDiff: (inboundScanData) => {
        const result = {};
        if (inboundScanData.weight_difference) {
          const diffData = inboundScanData.weight_difference;
          if (diffData.inbound_weight_difference_threshold != null && diffData.old_weight != null ) {
            const diffBy = Math.abs(((diffData.new_weight * 10) - (diffData.old_weight * 10)) / 10);
            if (diffBy > diffData.inbound_weight_difference_threshold) {
              result.weightDiff = true;
              result.weightDiffData = {
                higher: diffData.new_weight >= diffData.old_weight,
                diffBy: diffBy,
              };
            }
          }
        }
        return result;
      },

      getLimit: () => $http.get('core/inbounds/limit')
        .then(nvRequestUtils.success, nvRequestUtils.nvFailure),
      updateLimit: limit => $http.post('core/inbounds/limit', { limit: limit })
        .then(nvRequestUtils.success, nvRequestUtils.nvFailure),
      counter: () => $http.post('core/inbounds/counter', {})
        .then(nvRequestUtils.success, nvRequestUtils.nvFailure),
      clearSetAside: () => $http.post('core/inbounding/setaside', {})
        .then(nvRequestUtils.success, nvRequestUtils.nvFailure),
      inbound: (payload) => {
        return $http.post('core/inbounds', payload)
          .then(nvRequestUtils.success, nvRequestUtils.nvFailure);
      },
      scanInboundCmi: (payload) => {        
        return $http.post('core/scans/inbounds', _.assign(payload, { to_reschedule: true }))
          .then(nvRequestUtils.success, nvRequestUtils.nvFailure);
      },
      scanInbound: (payload) => {
        return $http.post('core/scans/inbounds', payload)
          .then(nvRequestUtils.success);
      },
      getRouteFetchingList: (params) => {
        return $http.get(`core/inbounds/getRouteFetchingList?${nvRequestUtils.queryParam(params)}`)
          .then(nvRequestUtils.success, nvRequestUtils.nvFailure);
      },
      getSummaryByRouteId: (routeId) => {
        return $http.get(`core/scans/inbounds/route/${routeId}`)
          .then(nvRequestUtils.success, nvRequestUtils.nvFailure);
      },
      vanInbound: (routeId, payloads) => (
        $http.post(`core/routes/${routeId}/van-inbounds`, payloads)
          .then(nvRequestUtils.success, nvRequestUtils.nvFailure)
        ),
      inboundCods: (routeId, data) =>
        $http.post(`core/inbounding/routes/${routeId}/cods`, data)
          .then(nvRequestUtils.success, nvRequestUtils.nvFailure),
      searchByRouteIdsInBatch: (ids, maxInChunk) => {
        const deferred = $q.defer();
        const MAX_CALL_IN_CHUNK = maxInChunk || 1000;
        const chunkIds = _.chunk(ids, MAX_CALL_IN_CHUNK);
        const promises = _.map(chunkIds, id => (searchByRouteIds(id)));

        $q.all(promises).then(success, failure);

        return deferred.promise;

        function success(responses) {
          let inboundScans = [];
          _.forEach(responses, (response) => {
            inboundScans = _.concat(inboundScans, response.data);
          });

          return deferred.resolve(inboundScans);
        }

        function failure(response) {
          deferred.reject(response);
        }
      },
    };
  }
}());
