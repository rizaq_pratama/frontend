(function(){
    'use strict';

    angular
        .module('nvCommons.models')
        .factory('InboundMonitoring', InboundMonitoring);

    InboundMonitoring.$inject = ['$q','$http', 'nvRequestUtils'];

    function InboundMonitoring($q, $http, nvRequestUtils){

        return {
            //api
            fetch : function (hubId, stringDate) {
                var url = 'core/inbounds/pickup/' + hubId + "/" + stringDate;
                return $http.get(url).then(
                    nvRequestUtils.success,
                    nvRequestUtils.nvFailure
                );
            },

            loadHub : function () {
                var url = "core/hubs";
                return $http.get(url).then(
                    nvRequestUtils.success,
                    nvRequestUtils.nvFailure
                );
            },

            loadReservation : function (shipperId, startDate, endDate) {
                var url = "core/dashboard/shippers/" + shipperId + "/reservations";

                //start_date and end_date in format "YYYY-MM-DD HH:mm:ss" 
                var options = {
                    params : {
                        start_date: startDate,
                        end_date: endDate,
                        include_route: true
                    }
                };

                return $http.get(url, options).then(
                    nvRequestUtils.success,
                    nvRequestUtils.nvFailure
                );
            },
        };
    }

})();