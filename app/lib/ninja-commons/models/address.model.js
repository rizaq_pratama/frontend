(function AddressModel() {
  angular
        .module('nvCommons.models')
        .factory('Address', Address);

  Address.$inject = ['$http', 'nvRequestUtils', 'nvExtendUtils', '$q', 'nvDomain'];

  const DP_ZONE_MAP = {};

  function Address($http, nvRequestUtils, nvExtendUtils, $q, nvDomain) {
    const addressTypes = [
      {
        value: 'A',
        displayName: 'Apartment',
      },
      {
        value: 'C',
        displayName: 'Condominium',
      },
      {
        value: 'H',
        displayName: 'HBD',
      },
      {
        value: 'K',
        displayName: 'Block',
      },
      {
        value: 'S',
        displayName: 'Standard',
      },
      {
        value: 'U',
        displayName: 'Unknown',
      },
    ];

    const countries = [
      {
        value: 'sg',
        displayName: 'Singapore',
      },
      {
        value: 'my',
        displayName: 'Malaysia',
      },
      {
        value: 'id',
        displayName: 'Indonesia',
      },
      {
        value: 'vn',
        displayName: 'Vietnam',
      },
      {
        value: 'ph',
        displayName: 'Philippines',
      },
      {
        value: 'th',
        displayName: 'Thailand',
      },
    ];

    const self = {
      addressTypes: addressTypes,
      countries: countries,
      readByShipperHyperlocal: function readByShipperHyperlocal(shipper) {
        return $http.get(`core/shippers/${shipper.id}/addresses?withChildren=true`).then(
                    success,
                    nvRequestUtils.nvFailure
                );
      },
      /**
       * Upload a csv file for address verification bulk
       * @param {File} csvFile - uploaded file
       */
      uploadCSV: function uploadCSV(csvFile) {
        if(!csvFile) {
          return $q.reject();
        }

        return $http.upload('addressing/waypoints/csv', csvFile)
          .then(nvRequestUtils.success, nvRequestUtils.nvFailure);
      },
      updateWaypoints: function updateWaypoints(waypoints) {
        const payload = {
          waypoints: waypoints,
        };
        return $http.put('addressing/waypoints', payload)
          .then(nvRequestUtils.success, nvRequestUtils.nvFailure);
      },

      preAvAssign: function preAvAssign(zoneId, waypoints) {
        const data = {
          waypoint_ids: _.castArray(waypoints),
        };
        return $http.post(`core/pre-address-verification/assign/zones/${zoneId}`, data)
          .then(nvRequestUtils.success, nvRequestUtils.nvFailure);
      },

      readMilkrunSettingsCount: samsIds =>
        $http.post('shipper/2.0/shippers/addresses/milkrun-count', samsIds)
          .then(nvRequestUtils.success, nvRequestUtils.nvFailure),

      readMilkrunSettings: (filter, samsIds) => {
        const param = _.defaults(filter, { start: 0, limit: 100 });
        return $http.post(`shipper/2.0/shippers/addresses/milkrun?${nvRequestUtils.queryParam(param)}`, samsIds)
          .then(nvRequestUtils.nvSuccess, nvRequestUtils.nvFailure);
      },

      readMilkrunSettingsByIds: function readMilkrunSettingsByIds(samsIds) {
        const deferred = $q.defer();
        getAllMilkrunSettings(deferred, samsIds);
        return deferred.promise;
      },
      /**
       * Verify a jaro score
       * @param {Object[]} data -  parameters
       * @param {number} data[].waypointId - id of the waypoint
       * @param {number} data[].addressId - id of the linked address
       * @param {number} data[].orderJaroScoreId - jaro score
       * @param {number} data[].latitude - updated latitude of the waypoint
       * @param {number} data[].longitude - updated longitude of the waypoint
       * @param {boolean} data[].sameday - sameday flag
       */
      verifyJaroScore: function verifyJaroScore(data) {
        return $http.put('core/orderjaroscores/verify', data)
          .then(nvRequestUtils.success, nvRequestUtils.nvFailure);
      },
      /**
       * Fetch addresses that needs to be reverified
       */
      getReverifiedAddresses: function getReverifiedAddresses() {
        return $http.get('core/orderjaroscoresv2?reverified=true')
          .then(nvRequestUtils.success, nvRequestUtils.nvFailure);
      },
      /**
       * Fetch unverified address based on route group id
       */
      getAddressesByRouteGroup: function getAddressesByRouteGroup(routeGroupId) {
        return $http.get(`core/orderjaroscoresv2?routeGroupId=${routeGroupId}`)
          .then(nvRequestUtils.success, nvRequestUtils.nvFailure);
      },

      /**
       * Create an address update event
       * @param {Object} data - address event object
       * @param {number} data.id - Waypoint id
       * @param {String} data.address1 - address string
       * @param {number} data.latitude - Address latitude
       * @param {number} data.longitude - Address longitude
       */
      createAddressEvent: function createAddressEvent(data) {
        const payload = {
          event: 'WAYPOINT_VERIFY_ADDRESS',
          waypoint: data,
        };
        return $http.post('addressing/1.0/events', payload)
          .then(nvRequestUtils.success, nvRequestUtils.nvFailure);
      },
      /**
       * Initialize address verification process
       * @param {Object} param - parameter of the initialization
       * @param {boolean} param.inbounded_only - flag for filtering inbounded only addresses
       * @param {boolean} param.is_sameday - flag for filtering sameday orders
       * @param {boolean} param.is_priority - flag for filtering priority orders
       * @param {Date} param.date - date filter
       */
      initializeAddressVerification: function initializeAddressVerification(param) {
        const newParam = _.defaults(param, {
          score_start: 0.0,
          score_end: 0.8,
        });
        return $http.post(`core/orderjaroscoresv2/initialize?${nvRequestUtils.queryParam(newParam)}`)
          .then(nvRequestUtils.success, nvRequestUtils.nvFailure);
      },
      /**
       * Fetch address verification
       * @param {Object} param - parameter for the fetch
       * @param {number} param.size - number of the fetched addresses
       * @param {number} param.zone_id - zone id of the addrresses
       */
      fetchAddressVerification: function fetchAddressVerification(param) {
        return $http.get(`core/orderjaroscoresv2/fetch?${nvRequestUtils.queryParam(param)}`)
          .then(nvRequestUtils.success, nvRequestUtils.nvFailure);
      },

      getUnverifiedAddressStats: function getUnverifiedAddressStats() {
        return $http.get('core/orderjaroscoresv2/stats')
          .then(nvRequestUtils.success, nvRequestUtils.nvFailure);
      },

      fetchAddressVerificationByrouteGroup: routeGroupId => (
        $http.get(`core/orderjaroscoresv2?routeGroupId=${routeGroupId}`)
        .then(nvRequestUtils.success, nvRequestUtils.nvFailure)
      ),

      archiveOrderJaroScore: ojsId => (
        $http.put(`core/orderjaroscoresv2/archive/${ojsId}`)
          .then(nvRequestUtils.success, nvRequestUtils.nvFailure)
      ),
      searchAddress: (query) => {
        const filter = {
          query: query,
          country: _.toLower(nvDomain.getDomain().currentCountry),
        };
        return $http.get(`addressing/2.0/filter_search?${nvRequestUtils.queryParam(filter)}`)
          .then(nvRequestUtils.success, nvRequestUtils.nvFailure);
      },

      /**
       *  address for shipper is different than address from addressing api
       *  here is the format for shipper address
       * {
             "id":386,
             "createdAt":"2014-12-05T11:51:09Z",
             "updatedAt":"2014-12-05T11:51:09Z",
             "deletedAt":null,
             "name":"Lazada (CEF electronics)",
             "contact":"62970676",
             "email":"johnyao@cefideas.com",
             "country":"SG",
             "city":"SG",
             "postcode":"208787",
             "address1":"10 JALAN BESAR",
             "address2":"SIM LIM TOWER 09-05",
             "latitude":null,
             "longitude":null
        }
       *
       */
      addressingApiToShipperAddress: function addressingApiToShipperAddress(apiResponse) {
        if (apiResponse) {
          return {
            country: apiResponse.country,
            city: apiResponse.city,
            address1: apiResponse.street,
            address2: apiResponse.building,
            postcode: apiResponse.postcode,
            latitude: apiResponse.latitude,
            longitude: apiResponse.longitude,
          };
        }
        return null;
      },
      dpZones: function dpZones(country = 'sg', useCache = true) {
        if (useCache && _.size(DP_ZONE_MAP[country]) > 0) {
          return $q.when(DP_ZONE_MAP[country]);
        }
        const deferred = $q.defer();
        getAllDpZones(deferred, country);
        return deferred.promise;
      },


      /**
       * Shipper address legacy
       */
      allShipperAddress: function allShipperAddress(options, loopingParams) {
        let theLoopingParams = loopingParams || null;

        if (theLoopingParams === null) {
          theLoopingParams = {
            deferred: $q.defer(),
            addresses: [],
            start: 0,
            limit: 100,
          };
        }

        self.getAddressesCount(options).success((response) => {
          const loopNumber = Math.ceil(response / theLoopingParams.limit);
          const promises = [];
          for (let i = 0; i < loopNumber; i++) {
            promises.push(self.getAddresses(_.defaultsDeep({
              start: (i * theLoopingParams.limit),
              limit: theLoopingParams.limit,
            }, options)));
          }

          $q.all(promises).then((responses) => {
            _.forEach(responses, (childResponse) => {
              theLoopingParams.addresses =
                 _.concat(theLoopingParams.addresses, childResponse.data.data);
            });
            theLoopingParams.deferred.resolve(theLoopingParams.addresses);
          }, () => {
            theLoopingParams.deferred.reject('Fail to get address');
          });
        }).error(() => {
          theLoopingParams.deferred.reject('Fail to get address');
        });

        return theLoopingParams.deferred.promise;
      },
      getAddressesCount: function getAddressesCount(options) {
        const params = _.defaults(options, {});
        return $http({
          method: 'GET',
          url: 'shipper/legacy/shippers/addresses-count',
          params: params,
        });
      },
      getAddresses: function getAddresses(options) {
        const params = _.defaults(options, {});
        return $http({
          method: 'GET',
          url: 'shipper/legacy/shippers/addresses',
          params: params,
        });
      },
      updateShipperAddress: function updateShipperAddress(data) {
        return $http.put('shipper/legacy/shippers/addresses', _.castArray(data))
        .then(nvRequestUtils.success, nvRequestUtils.nvFailure);
      },
    };

    return self;

    function success(response) {
      return nvRequestUtils.success(response).then(nvExtendUtils.addDisplayAddress);
    }

    function getAllMilkrunSettings(defer, samsIds) {
      const callLimit = 100;
      let milkrunSettings = [];
      self.readMilkrunSettingsCount(samsIds).then((response) => {
        const totalCount = response;

        const loopNumber = Math.ceil(totalCount / callLimit);
        const promises = [];
        for (let i = 0; i < loopNumber; i++) {
          promises.push(self.readMilkrunSettings({
            start: i * callLimit,
            limit: callLimit,
          }, samsIds));
        }
        $q.all(promises).then((responses) => {
          _.forEach(responses, (childResponse) => {
            milkrunSettings = _.concat(milkrunSettings, childResponse);
          });
          milkrunSettings = _.sortBy(milkrunSettings, ['id']);
          defer.resolve(milkrunSettings);
        }, () => defer.reject(-1));
        // do call
      }, () => defer.reject(-1));
    }

    function getAllDpZones(deferred, country) {
      $http.get(`addressing/dp-zones?country=${country}`).then(onSuccess, onFailure);

      function onSuccess(response) {
        const data = response.data;
        DP_ZONE_MAP[country] = data;
        deferred.resolve(data);
      }

      function onFailure() {
        deferred.reject(-1);
      }
    }
  }
}());
