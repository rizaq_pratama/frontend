(function model() {
  angular
    .module('nvCommons.models')
    .factory('OpsRoute', OpsRoute);

  OpsRoute.$inject = ['$http', 'nvRequestUtils', 'nvModelUtils'];

  function OpsRoute($http, nvRequestUtils, nvModelUtils) {
    const FIELDS = {
      id: { displayName: 'commons.id' /* Id */ },
      routeId: { displayName: 'commons.model.route-id' /* Route Id */ },
      country: { displayName: 'commons.model.country' /* Country */ },
      typeId: { displayName: 'commons.model.type-id' /* Type ID */ },
      type: {
        _deep: true,
        id: { displayName: 'commons.id' /* Id */ },
        name: { displayName: 'commons.name' /* Name */ },
        createdAt: { displayName: 'commons.model.created-at' /* Created On */ },
        deletedAt: { displayName: 'commons.model.deleted-at' /* Deleted On */ },
        updatedAt: { displayName: 'commons.model.updated-at' /* Updated On */ },
      },
      createdAt: { displayName: 'commons.model.created-at' /* Created On */ },
      deletedAt: { displayName: 'commons.model.deleted-at' /* Deleted On */ },
      updatedAt: { displayName: 'commons.model.updated-at' /* Updated On */ },
    };

    return {

      getAll: () =>
        $http.get('ticketing/opsroutes').then(
          nvRequestUtils.success,
          nvRequestUtils.nvFailure
        ),

      update: route =>
        $http.put(`ticketing/opsroute/${route.typeId}`,
          { routeId: String(route.routeId) }).then(
          nvRequestUtils.success,
          nvRequestUtils.nvFailure
        ),

      FIELDS: FIELDS,

      getEditFields: field =>
        nvModelUtils.pickFields(FIELDS, [
          'id', 'routeId',
        ], { setValues: true, model: field }),
    };
  }
}());
