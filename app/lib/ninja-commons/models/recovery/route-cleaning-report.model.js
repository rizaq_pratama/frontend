(function model() {
  angular
    .module('nvCommons.models')
    .factory('RouteCleaningReport', RouteCleaningReport);

  RouteCleaningReport.$inject = ['$http', 'nvRequestUtils', 'nvExtendUtils', 'nvDateTimeUtils', 'Excel'];

  function RouteCleaningReport($http, nvRequestUtils, nvExtendUtils, nvDateTimeUtils, Excel) {
    const FIELDS = {
      reservations: {
        _array: true,
        _values: [],
        date: { displayName: 'Date' },
        exception: { displayName: 'Exception' },
        shipperName: { displayName: 'Shipper Name' },
        driverName: { displayName: 'Driver Name' },
      },
      cod: {
        _array: true,
        _values: [],
        date: { displayName: 'Date' },
        codInbounded: { displayName: 'COD Inbounded' },
        codExpected: { displayName: 'COD Expected' },
        routeId: { displayName: 'Route Id' },
        driverName: { displayName: 'Driver Name' },
      },
      parcels: {
        _array: true,
        _values: [],
        date: { displayName: 'Date' },
        lastScanHubId: { displayName: 'Last Scan Hub ID' },
        exception: { displayName: 'Exception' },
        routeId: { displayName: 'Route ID' },
        lastSeen: { displayName: 'Last Seen' },
        driverName: { displayName: 'Driver Name' },
        shipperName: { displayName: 'Shipper Name' },
        lastScanType: { displayName: 'Last Scan Type' },
        trackingId: { displayName: 'Tracking Id' },
      },
    };

    return {

      getJSON: (date) => {
        const params = {
          date: nvDateTimeUtils.displayDate(date),
          format: 'json',
        };

        return $http.get(`core/reports/getroutecleaningreportV3?${nvRequestUtils.queryParam(params)}`).then(
                    nvRequestUtils.nvSuccess,
                    nvRequestUtils.nvFailure
                ).then(addMomentToParcels);

        function addMomentToParcels(data) {
          data.parcels = nvExtendUtils.addMoment(data.parcels, ['lastSeen']);
          return data;
        }
      },

      getExcel: (date) => {
        const params = {
          date: nvDateTimeUtils.displayDate(date),
        };
        return Excel.download(`core/reports/getroutecleaningreportV3?${nvRequestUtils.queryParam(params)}`, 'route-cleaning-report.xls');
      },

      FIELDS: FIELDS,
    };
  }
}());
