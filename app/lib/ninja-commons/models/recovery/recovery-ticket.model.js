(function model() {
  angular
        .module('nvCommons.models')
        .factory('Ticketing', Ticketing);

  Ticketing.$inject = ['$http', 'nvModelUtils', 'Order', 'nvRequestUtils', '$q'];

  function Ticketing($http, nvModelUtils, Order, nvRequestUtils, $q) {
    const ticketTypeOptions = [
            { displayName: 'MISSING', value: '1' },
            { displayName: 'DAMAGE', value: '2' },
    ];

    const statuses = {
      PENDING: 'PENDING',
      IN_PROGRESS: 'IN PROGRESS',
      RESOLVED: 'RESOLVED',
      ON_HOLD: 'ON HOLD',
      PENDING_SHIPPER: 'PENDING SHIPPER',
      CANCELLED: 'CANCELLED',
    };

    const fields = {
      id: { displayName: 'container.recovery-tickets.id', fieldName: 'id' },
      status: { displayName: 'container.recovery-tickets.status', fieldName: 'status' },
      ticketType: {
        displayName: 'container.recovery-tickets.ticket-type',
        fieldName: 'ticketType',
        fieldId: 17,
        options: ticketTypeOptions,
      },
      subTicketType: {
        displayName: 'container.recovery-tickets.ticket-sub-type',
        fieldName: 'subTicketType',
        fieldId: 17,
        options: [{}],
      },
      entrySource: {
        displayName: 'container.recovery-tickets.entry-source',
        fieldName: 'entrySource',
        fieldId: 13,
        options: [{}],
      },
      outcome: {
        displayName: 'container.recovery-tickets.outcome',
        fieldName: 'outcome',
      },
      customFields: {
        _deep: true,
      },
      trackingId: {
        displayName: 'container.recovery-tickets.tracking-id',
        fieldId: 2,
        fieldName: 'trackingId',
      },
      shipper: {
        displayName: 'container.recovery-tickets.shipper',
        fieldName: 'shipper',
      },
      lastScanType: {
        displayName: 'container.recovery-tickets.last-scan-type',
        fieldName: 'lastScanType',
      },
      lastScan: {
        displayName: 'container.recovery-tickets.last-scan',
        fieldName: 'lastScan',
      },
      investigatingParty: {
        displayName: 'container.recovery-tickets.investigating-dept',
        fieldId: 15,
        options: [{}],
        fieldName: 'investigatingParty',
      },
      investigatingHub: {
        displayName: 'container.recovery-tickets.investigating-hub',
        fieldId: 67,
        options: [{}],
        fieldName: 'INVESTIGATING HUB ID',
      },
      comments: {
        displayName: 'container.recovery-tickets.comments',
        fieldId: 26,
        fieldName: 'comments',
        value: null,
      },
      daysDue: {
        displayName: 'container.recovery-tickets.days-due',
        fieldName: 'daysDue',
      },
      _createdAt: {
        displayName: 'container.recovery-tickets.created',
        fieldName: '_createdAt',
      },
      ticketTypeSubType: {
        displayName: 'container.recovery-tickets.ticket-type-sub-type',
        fieldName: 'ticketTypeSubType',
      },
      assigneeName: {
        displayName: 'container.recovery-tickets.assignee',
        fieldName: 'assigneeName',
      },
      SCAN_COMMENTS: {
        displayName: 'container.recovery-tickets.comments',
        fieldName: 'SCAN_COMMENTS',
        fieldId: null,
      },
      hubName: {
        displayName: 'container.recovery-tickets.hub-name',
        fieldName: 'hubName',
        fieldId: null,
      },
      orderStatus: {
        displayName: 'container.recovery-tickets.order-status',
        fieldName: 'orderStatus',
      },
      custZendeskId: {
        displayName: 'container.recovery-tickets.cust-zendesk-id',
        fieldName: 'custZendeskId',
        fieldId: 34,
      },
      shipperZendeskId: {
        displayName: 'container.recovery-tickets.shipper-zendesk-id',
        fieldName: 'shipperZendeskId',
        fieldId: 36,
      },
      ticketNotes: {
        displayName: 'container.recovery-tickets.ticket-notes',
        fieldName: 'ticketNotes',
        fieldId: 32,
      },
      ticketCreator: {
        displayName: 'container.recovery-tickets.ticket-creator',
        fieldName: 'ticketCreator',
      },
      orderGranularStatus: {
        displayName: 'container.recovery-tickets.order-granular-status',
        fieldName: 'orderGranularStatus',
      },
    };

    const self = {
      statuses: statuses,

      getTableFields: function getTableFields() {
        return nvModelUtils.pickFields(fields, [
          'daysDue', '_createdAt', 'id', 'ticketTypeSubType',
          'status', 'assigneeName', 'investigatingParty', 'investigatingHub',
          'trackingId', 'shipper', 'hubName', 'orderStatus',
          'ticketCreator', 'orderGranularStatus',
        ]);
      },

      /**
       * Proxy function to get hubs data. Calling core/hubs via ticket service
       */
      getHubs: function getHubs() {
        return $http.get('ticketing/hubs')
          .then(nvRequestUtils.success, nvRequestUtils.nvFailure);
      },

      createOne: function createOne(data) {
        return $http.post('ticketing/ticket', data)
          .then(nvRequestUtils.success, nvRequestUtils.nvFailure);
      },

      createBulk: function createBulk(tickets) {
        const payload = {
          tickets: tickets,
        };
        return $http.post('ticketing/ticket/bulkcreate', payload)
          .then(nvRequestUtils.success, nvRequestUtils.nvFailure);
      },

      getAddField: function getAddField() {
        return nvModelUtils.pickFields(fields, [
          'ticketType',
          'subTicketType',
          'entrySource',
          'trackingId',
          'investigatingParty',
          'investigatingHub',
          'comments',
          'shipperZendeskId',
          'custZendeskId',
          'ticketNotes',
        ], { setDefaultValues: true });
      },

      getAddFieldScanning: function getAddFieldScanning() {
        return nvModelUtils.pickFields(fields, [
          'ticketType',
          'subTicketType',
          'entrySource',
          'trackingId',
          'investigatingParty',
          'investigatingHub',
          'SCAN_COMMENTS',
        ], { setDefaultValues: true });
      },

      search: function search(
        dateStart,
        dateEnd,
        page,
        pageSize,
        payloadObject
      ) {
        const param = {};

        if (dateStart) {
          const momentDateStart = moment(dateStart);
          param.start = momentDateStart.isValid() ? momentDateStart.format('YYYY-MM-DD') : '';
        }

        if (dateEnd) {
          const momentDateEnd = moment(dateEnd);
          param.end = momentDateEnd.isValid() ? momentDateEnd.format('YYYY-MM-DD') : '';
        }
        param.page = page || 1;
        param.page_size = pageSize || 30;

        return $http.put('ticketing/tickets/search', payloadObject, { params: param })
          .then(nvRequestUtils.success, nvRequestUtils.nvFailure);
      },

      /**
       * Search ticket
       * {@link https://confluence.ninjavan.co/display/NVE/Ticketing+API#TicketingAPI-Searchfortickets}
       * @param {Number} page - The page number of the search results set
       * @param {Number} pageSize - The number of results to be displayed in each page
       * @param {Object} params
       * @param {Number[]} params.ticket_statuses
       * @param {Number[]} params.investigating_parties
       * @param {Number[]} params.entry_sources
       * @param {String[]} params.tracking_ids
       * @param {Number[]} params.hub_ids,
       * @param {Number[]} params.user_ids
       * @param {Number[]} params.investigating_hub_ids
       * @param {Number[]} params.ticket_sub_types
       * @param {Number[]} params.shipper_ids
       * @param {Date} params.start
       * @param {Date} params.end
       * @returns {{tickets: Object[], count: Number}} tickets
       */
      searchTicket: function searchTicket(params, page, pageSize) {
        const newParams = {
          ticket_statuses: [],
          investigating_parties: [],
          entry_sources: [],
          ticket_types: [],
        };
        const dateStart = params.start;
        const dateEnd = params.end;
        const paramsOmitted = _.omit(params, ['start', 'end', 'resolvedTicket', 'showAll']);
        _.merge(newParams, paramsOmitted);

        return self.search(dateStart, dateEnd,
            page, pageSize, newParams);
      },

      searchTicketByTrackingId: (trackingId) => {
        const newParams = {
          ticket_statuses: [],
          investigating_parties: [],
          entry_sources: [],
          ticket_types: [],
          tracking_ids: [trackingId],
        };
        return self.search(null, null,
          1, 30, newParams);
      },

      getCustomField: function getCustomField(ticketTypeId) {
        return $http.get(`ticketing/types/${ticketTypeId}`)
          .then(nvRequestUtils.success, nvRequestUtils.nvFailure);
      },

      getTicketTypes: function getTicketTypes() {
        return $http.get('ticketing/types')
          .then(nvRequestUtils.success, nvRequestUtils.nvFailure);
      },

      getGroup: function getGroup() {
        return $http.get('ticketing/groups')
          .then(nvRequestUtils.success, nvRequestUtils.nvFailure);
      },

      getEntrySource: function getEntrySource() {
        return $http.get('ticketing/entrysources')
          .then(nvRequestUtils.success, nvRequestUtils.nvFailure);
      },

      updateOne: function updateOne(data, id) {
        return $http.put(`ticketing/ticket/${id}`, data)
          .then(nvRequestUtils.success, nvRequestUtils.nvFailure);
      },

      deleteOne: function deleteOne(id) {
        return $http.delete(`ticketing/ticket/${id}`)
          .then(nvRequestUtils.success, nvRequestUtils.nvFailure);
      },

      getTicket: function getTicket(id) {
        return $http.get(`ticketing/tickets/${id}`)
          .then(nvRequestUtils.success, nvRequestUtils.nvFailure);
      },

      getTicketUser: function getTicketUser() {
        return $http.get('ticketing/users')
          .then(nvRequestUtils.success, nvRequestUtils.nvFailure);
      },

      /**
       * Get a specific sub type based on id. for retrieving all available sub type
       * @see getAllSubtypes
       * @param {Number} id - sub type id
       * @returns {Object} subtype - check the doc {@link https://confluence.ninjavan.co/display/NVE/Ticketing+API#TicketingAPI-Getsub-typebyID}
       */
      getSubtype: function getSubtype(id) {
        return $http.get(`ticketing/subtype/${id}`)
          .then(nvRequestUtils.success, nvRequestUtils.nvFailure);
      },

      /**
       * Get all sub types
       * @function
       * @returns {{id: Number, name: string, acronym: string}[]} subTypeArray -
       * All sub types in array
       */
      getAllSubtypes: function getAllSubtypes() {
        return $http.get('ticketing/sub-types')
          .then(nvRequestUtils.success, nvRequestUtils.nvFailure);
      },

      /**
       * Get all possible ticket status
       * @function
       * @returns {{id: Number, createdAt: String, updatedAt: String, deletedAt:String, name: String}[]} statusArray -
       * array of status object. sample:
       * [
        {
          "id":1,
          "createdAt":"2016-06-13T16:37:48Z",
          "updatedAt":null,
          "deletedAt":null,
          "name":"PENDING"
        }
        ]
       */
      getTicketStatus: function getTicketStatus() {
        return $http.get('ticketing/statuses')
          .then(nvRequestUtils.success, nvRequestUtils.nvFailure);
      },

      /**
       * Get ticket by tracking id
       * @param {Object} payload - Request object, example :
       * {
       *  "tracking_id": ['NVSGHAHAHA01', 'NVSGLALA01]
       * }
       */
      getTicketByTrackingIdV2: function getTicketByTrackingIdV2(trackingIds) {
        const payload = {
          tracking_ids: trackingIds,
        };
        return $http.post('ticketing/2.0/tickets-verify', payload)
          .then(nvRequestUtils.success, nvRequestUtils.nvFailure);
      },
      validateBeforeCreate: function validateBeforeCreate(trackingId) {
        return Order.getTrackingIdV2(trackingId)
            .then(afterCheck, onNotFound);

        function afterCheck(result) {
          return result;
        }

        function onNotFound() {
          return $q.reject('Tracking invalid');
        }
      },
      getTicketFilter: function getTicketFilter() {
        return $http.get('ticketing/ticket-filters')
          .then(nvRequestUtils.success, nvRequestUtils.nvFailure);
      },
      createTicketFilter: function createTicketFilter(ticketFilter) {
        const preset = {
          name: ticketFilter.name,
          payload: angular.toJson(_.omit(ticketFilter, 'name')),
        };
        return $http.post('ticketing/ticket-filters', preset)
          .then(nvRequestUtils.success, nvRequestUtils.nvFailure);
      },
      updateTicketFilter: function updateTicketFilter(ticketFilter, id) {
        const preset = {
          name: ticketFilter.name,
          payload: angular.toJson(_.omit(ticketFilter, 'name')),
          id: id,
        };
        return $http.put(`ticketing/ticket-filters/${id}`, preset)
          .then(nvRequestUtils.success, nvRequestUtils.nvFailure);
      },
      deleteTicketFilter: function deleteTicketFilter(id) {
        return $http.delete(`ticketing/ticket-filters/${id}`)
          .then(nvRequestUtils.success, nvRequestUtils.nvFailure);
      },

      getOrderOutcomeByTypeId: function getOrderOutcomeByTypeId(typeId) {
        return $http.get(`ticketing/orderoutcomes?type_id=${typeId}`)
          .then(nvRequestUtils.success, nvRequestUtils.nvFailure);
      },

      getOrderOutcomeBySubTypeId: function getOrderOutcomeBySubTypeId(subTypeId) {
        return $http.get(`ticketing/orderoutcomes?subtype_id=${subTypeId}`)
          .then(nvRequestUtils.success, nvRequestUtils.nvFailure);
      },

      bulkUpdate: function bulkUpdate(payload) {
        return $http.put('ticketing/tickets/massupdate', _.castArray(payload))
          .then(nvRequestUtils.success, nvRequestUtils.nvFailure);
      },

      createFollowUp: function createFollowUp(ticketId, payload) {
        return $http.post(`ticketing/tickets/${ticketId}/create-followup`, payload)
          .then(nvRequestUtils.success, nvRequestUtils.nvFailure);
      },

      createByCsv: function createCsv(file) {
        return $http.upload('ticketing/2.0/tickets/bulk-create', file)
          .then(nvRequestUtils.success, nvRequestUtils.nvFailure);
      },

      createByCsvAsync: function createByCsvAsync(file) {
        return $http.upload('ticketing/2.0/tickets/bulk-upload', file)
          .then(nvRequestUtils.success, nvRequestUtils.nvFailure);
      },

      getCsvJobStatus: jobId =>
        ($http.get(`ticketing/2.0/tickets/job-status?jobId=${jobId}`).then(nvRequestUtils.success, nvRequestUtils.nvFailure)),

      uploadImage: function uploadImage(file, ticketId) {
        return $http.upload(`ticketing/2.0/tickets/${ticketId}/photos`, file)
          .then(nvRequestUtils.success, nvRequestUtils.nvFailure);
      },
      deleteImage: function deleteImage(ticketId, photoId) {
        return $http.delete(`ticketing/2.0/tickets/${ticketId}/photos/${photoId}`)
          .then(nvRequestUtils.success, nvRequestUtils.nvFailure);
      },
      listImages: function listImages(ticketId) {
        return $http.get(`ticketing/2.0/tickets/${ticketId}/photos`)
          .then(nvRequestUtils.success, nvRequestUtils.failure);
      },
      validateTrackingIds: function validateTrackingIds(trackingIds) {
        const payload = {
          tracking_ids: _.castArray(trackingIds),
        };
        return $http.post('core/tickets/search', payload)
          .then(nvRequestUtils.success, nvRequestUtils.nvFailure);
      },

      getAcronyms: function getAcronyms() {
        return $http.get('ticketing/acronyms')
          .then(nvRequestUtils.success, nvRequestUtils.nvFailure);
      },
      autoResolve: function autoResolve(ticketId, hubId, outcome = 'FOUND - INBOUND') {
        const payload = {
          order_outcome: outcome,
          hub_id: hubId,
        };
        return $http.post(`ticketing/2.0/tickets/${ticketId}/resolve`, payload)
          .then(nvRequestUtils.success, nvRequestUtils.nvFailure);
      },

      updateTicket: function updateTicket(ticketId, payload) {
        return $http.put(`ticketing/2.0/tickets/${ticketId}`, payload)
          .then(nvRequestUtils.success, nvRequestUtils.nvFailure);
      },
      addUser: function addUser(email, firstName, lastName) {
        const payload = {
          email: email,
          first_name: firstName,
          last_name: lastName,
        };
        return $http.post('ticketing/users', payload)
          .then(nvRequestUtils.success, nvRequestUtils.nvFailure);
      },
      hasActiveTickets: function hasActiveTickets(tickets) {
        let status = false;
        _.forEach(tickets, (ticket) => {
          status = !_.includes([statuses.RESOLVED, statuses.CANCELLED], ticket.status);
          if (status) {
            return false; // break
          }

          return _.noop();
        });
        return status;
      },
      toOptions: datas =>
        _(datas)
          .map(data => ({ displayName: data.name, value: data.id, id: data.id }))
          .sortBy('value')
          .value(),

      quickSearchTicket: function quickSearchTicket(trackingId) {
        return $http.get(`ticketing/2.0/tickets?tracking_id=${trackingId}`)
          .then(nvRequestUtils.success, nvRequestUtils.nvFailure);
      },

    };

    return self;
  }
}());
