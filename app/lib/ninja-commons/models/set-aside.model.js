(function model() {
  angular
    .module('nvCommons.models')
    .factory('SetAside', SetAside);

  SetAside.$inject = ['$http', 'nvRequestUtils'];

  function SetAside($http, nvRequestUtils) {
    return {
      readTransactions: dnrId => $http
        .get(`core/set-aside/${dnrId}/transactions`)
        .then(nvRequestUtils.success, nvRequestUtils.nvFailure),
      readSetAside: () => $http
        .get('core/set-aside')
        .then(nvRequestUtils.success, nvRequestUtils.nvFailure),
      enable: payload => $http
        .put('core/set-aside/enable', payload)
        .then(nvRequestUtils.success, nvRequestUtils.nvFailure),
      disable: () => $http
        .put('core/set-aside/disable')
        .then(nvRequestUtils.success, nvRequestUtils.nvFailure),
      resetSetAside: dnrId => $http
        .post(`core/set-aside/${dnrId}/transactions`, {})
        .then(nvRequestUtils.success, nvRequestUtils.nvFailure),
      resetSetAsideCounter: dnrIds => $http
        .post('core/set-aside/counter/reset', _.assign({}, { dnrIds: dnrIds }))
        .then(nvRequestUtils.success, nvRequestUtils.nvFailure),
      recalc: dnrIds => $http
        .post('core/set-aside/counter/recal', _.assign({}, { dnrIds: dnrIds }))
        .then(nvRequestUtils.success, nvRequestUtils.nvFailure),
      limit: limit => $http
        .post('core/set-aside/counter/limit', _.assign({}, { limit: limit }))
        .then(nvRequestUtils.success, nvRequestUtils.nvFailure),
      getCounter: () => $http
        .get('core/set-aside/counter')
        .then(nvRequestUtils.success, nvRequestUtils.nvFailure),
    };
  }
}());
