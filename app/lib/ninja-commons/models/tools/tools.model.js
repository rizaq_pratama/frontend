(function ToolsModel() {
  angular
    .module('nvCommons.models')
    .factory('Tools', Tools);

  Tools.$inject = ['$http', 'nvRequestUtils'];

  function Tools($http, nvRequestUtils) {
    return {
      sendWebhooks: payload => $http
        .post('core/tools/webhooks', payload)
        .then(nvRequestUtils.success, nvRequestUtils.nvFailure),
    };
  }
}());
