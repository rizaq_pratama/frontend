(function model() {
  angular
    .module('nvCommons.models')
    .factory('DWH', DWH);

  DWH.$inject = ['$http', 'nvRequestUtils'];

  function DWH($http, nvRequestUtils) {

    return {

      /**
      * {
      "start_date": "2018-08-15T16:00:00Z",
      "end_date": "2018-10-09T15:59:59Z",
      "timezone": "Asia/Singapore",
      "shipper_ids": [],
      "email_addresses": ["gautam@ninjavan.co"],
      "consolidated_options": ["SHIPPER", "PARENT", "AGGREGATED", "ALL"]
      }
       */
      reportsBilling: function reportsBilling(data) {
        return $http.post('dwh/1.0/reports/billing', data)
          .then(nvRequestUtils.success, nvRequestUtils.nvFailure);
      },

    };
  }
}());
