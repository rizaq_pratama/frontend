(function model() {
  angular
    .module('nvCommons.models')
    .factory('PriorityLevel', PriorityLevel);

  PriorityLevel.$inject = ['$http', 'nvRequestUtils'];

  function PriorityLevel($http, nvRequestUtils) {
    return {
      search: payload =>
        $http.post('core/priority-levels/search', payload).then(nvRequestUtils.success, nvRequestUtils.nvFailure),
      update: payload =>
        $http.put('core/priority-levels', payload).then(nvRequestUtils.success, nvRequestUtils.nvFailure),
      bulkUpdate: payload =>
        $http.put('core/priority-levels/bulk', payload).then(nvRequestUtils.success, nvRequestUtils.nvFailure),
    };
  }
}());
