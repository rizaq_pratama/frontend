(function PaymentProcessingModel() {
  angular
    .module('nvCommons.models')
    .factory('PaymentProcessing', model);

  model.$inject = ['nvTranslate', '$http', 'nvRequestUtils'];
  function model(nvTranslate, $http, nvRequestUtils) {
    // STATUSES NAME
    const COMPLETED = 'BANK_TRANSFER_COMPLETED';
    const IN_PROGRESS = 'BANK_TRANSFER_IN_PROGRESS';
    const FAILED = 'BANK_TRANSFER_FAILED';
    const CANCELLED = 'CANCELLED';
    const PENDING = 'PENDING';
    const REJECTED = 'REJECTED';

    // EVENTS NAME
    const EVENT_IN_PROGREESS = 'BANK_TRANSFER_IN_PROGRESS';
    const EVENT_COMPLETED = 'BANK_TRANSFER_COMPLETED';
    const EVENT_FAILED = 'BANK_TRANSFER_FAILED';
    const EVENT_WITHDRAWAL_INITIATED = 'WITHDRAWAL_REQUEST_INITIATED';
    const EVENT_WITHDRAWAL_CANCELLED = 'WITHDRAWAL_REQUEST_CANCELLED';
    const EVENT_WITHDRAWAL_REJECTED = 'WITHDRAWAL_REQUEST_REJECTED';
    const EVENT_NO_ACTION = 'NO_ACTION';

    const field = {
      created_at: { displayName: nvTranslate.instant('container.payment-processing.request-date') },
      formattedStatus: { displayName: nvTranslate.instant('container.payment-processing.status') },
      id: { displayName: nvTranslate.instant('container.payment-processing.trans-req-id') },
      ref: { displayName: nvTranslate.instant('container.payment-processing.withdrawal-ref') },
      contact_no: { displayName: nvTranslate.instant('container.payment-processing.phone-number') },
      seller_user_id: { displayName: nvTranslate.instant('container.payment-processing.nco-user-id') },
      account_holder_name: { displayName: nvTranslate.instant('container.payment-processing.acc-holder') },
      bank_account_no: { displayName: nvTranslate.instant('container.payment-processing.bank-account-no') },
      bank_name: { displayName: nvTranslate.instant('container.payment-processing.bank-name') },
      amount: { displayName: nvTranslate.instant('container.payment-processing.req-amount') },
      currency: { displayName: nvTranslate.instant('container.payment-processing.currency') },
      comments: { displayName: nvTranslate.instant('container.payment-processing.comments') },
    };

    const statuses = [
      {
        displayName: 'container.payment-processing.pending',
        value: PENDING,
      },
      {
        displayName: 'container.payment-processing.in-progress',
        value: IN_PROGRESS,
      },
      {
        displayName: 'container.payment-processing.failed',
        value: FAILED,
      },
      {
        displayName: 'container.payment-processing.completed',
        value: COMPLETED,
      },
      {
        displayName: 'container.payment-processing.cancelled',
        value: CANCELLED,
      },
      {
        displayName: 'container.payment-processing.rejected',
        value: REJECTED,
      },
    ];

    const countries = [
      {
        displayName: 'Singapore',
        value: 'sg',
      },
      {
        displayName: 'Indonesia',
        value: 'id',
      },
      {
        displayName: 'Malaysia',
        value: 'my',
      },
      {
        displayName: 'Thailand',
        value: 'th',
      },
      {
        displayName: 'Vietnam',
        value: 'vn',
      },
      {
        displayName: 'Philippines',
        value: 'ph',
      },
    ];

    const self = {
      PENDING: PENDING,
      IN_PROGRESS: IN_PROGRESS,
      FAILED: FAILED,
      CANCELLED: CANCELLED,
      COMPLETED: COMPLETED,
      REJECTED: REJECTED,
      EVENT_IN_PROGREESS: EVENT_IN_PROGREESS,
      EVENT_COMPLETED: EVENT_COMPLETED,
      EVENT_FAILED: EVENT_FAILED,
      EVENT_WITHDRAWAL_INITIATED: EVENT_WITHDRAWAL_INITIATED,
      EVENT_WITHDRAWAL_CANCELLED: EVENT_WITHDRAWAL_CANCELLED,
      EVENT_WITHDRAWAL_REJECTED: EVENT_WITHDRAWAL_REJECTED,
      EVENT_NO_ACTION: EVENT_NO_ACTION,
      statuses: statuses,
      tableFields: field,
      countries: countries,
      getPossibleStatus: function getPossibleStatus(currStatus) {
        let possibleStatuses = [];
        switch (currStatus) {
          case PENDING :
            possibleStatuses = _.pick(statuses, [0, 1, 4]);
            break;
          case IN_PROGRESS:
            possibleStatuses = _.pick(statuses, [1, 2, 3, 5]);
            break;
          case FAILED:
            possibleStatuses = _.pick(statuses, [2]);
            break;
          case COMPLETED:
            possibleStatuses = _.pick(statuses, [3]);
            break;
          case CANCELLED:
            possibleStatuses = _.pick(statuses, [4]);
            break;
          case REJECTED:
            possibleStatuses = _.pick(statuses, [5]);
            break;
          default :
            break;
        }
        return possibleStatuses;
      },
      listWithdrawal: function listWithdrawal(page, pageSize) {
        return $http.get(`checkout/1.0/withdrawals?page=${page}&page_size=${pageSize}`)
          .then(nvRequestUtils.success, nvRequestUtils.nvFailure);
      },
      getWithdrawal: function getWithdrawal(trxId) {
        return $http.get(`checkout/1.0/withdrawals/${trxId}`)
          .then(nvRequestUtils.success, nvRequestUtils.nvFailure);
      },
      createWithdrawal: function createWithdrawal(payload) {
        return $http.post('checkout/1.0/withdrawals', payload)
          .then(nvRequestUtils.success, nvRequestUtils.nvFailure);
      },
      withdrawalEvent: function withdrawalEvent(trxId, event) {
        return $http.post(`checkout/1.0/withdrawals/${trxId}/transactions`, event)
          .then(nvRequestUtils.success, nvRequestUtils.nvFailure);
      },
      withdrawalEventByRef: function withdrawalEventByRef(event) {
        return $http.post('checkout/1.0/withdrawals/transactions', event)
          .then(nvRequestUtils.success, nvRequestUtils.nvFailure);
      },
      doFailTransaction: function doFailTransaction(ref, comment) {
        const eventPayload = {
          withdrawal_ref: ref,
          event: FAILED,
          comments: comment,
        };
        return self.withdrawalEventByRef(_.castArray(eventPayload));
      },
      doInProgressTransaction: function doInProgressTransaction(ref, comment) {
        const eventPayload = {
          withdrawal_ref: ref,
          event: IN_PROGRESS,
          comments: comment,
        };
        return self.withdrawalEventByRef(_.castArray(eventPayload));
      },
      doCancelTransaction: function doCancelTransaction(ref, comment) {
        const eventPayload = {
          withdrawal_ref: ref,
          event: CANCELLED,
          comments: comment,
        };
        return self.withdrawalEventByRef(_.castArray(eventPayload));
      },
      doCompleteTransaction: function doCompleteTransaction(ref, comment) {
        const eventPayload = {
          withdrawal_ref: ref,
          event: COMPLETED,
          comments: comment,
        };
        return self.withdrawalEventByRef(_.castArray(eventPayload));
      },
      doRejectTransaction: function doRejectTransaction(ref, comment) {
        const eventPayload = {
          withdrawal_ref: ref,
          event: REJECTED,
          comments: comment,
        };
        return self.withdrawalEventByRef(_.castArray(eventPayload));
      },
      doBulkUpdateTransaction: function doBulkUpdateTransaction(payload) {
        return self.withdrawalEventByRef(payload);
      },
      search: function search(param) {
        return $http.post('checkout/1.0/withdrawals/search', param)
          .then(nvRequestUtils.success, nvRequestUtils.nvFailure);
      },
      updateWithdrawal: function updateWithdrawal(id, payload) {
        return $http.put(`checkout/1.0/withdrawals/${id}`, payload)
          .then(nvRequestUtils.success, nvRequestUtils.nvFailure);
      },
      statusToEvent: function statusToEvent(status) {
        switch (status) {
          case COMPLETED:
            return EVENT_COMPLETED;
          case IN_PROGRESS:
            return EVENT_IN_PROGREESS;
          case FAILED:
            return EVENT_FAILED;
          case CANCELLED:
            return EVENT_WITHDRAWAL_CANCELLED;
          case PENDING:
            return EVENT_WITHDRAWAL_INITIATED;
          case REJECTED:
            return EVENT_WITHDRAWAL_REJECTED;
          default:
            return EVENT_NO_ACTION;
        }
      },
    };

    return self;
  }
}());
