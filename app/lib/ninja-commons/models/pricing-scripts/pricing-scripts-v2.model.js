(function model() {
  angular
    .module('nvCommons.models')
    .factory('PricingScriptsV2', PricingScripts);

  PricingScripts.$inject = ['$q', '$http', 'nvRequestUtils'];

  function PricingScripts($q, $http, nvRequestUtils) {
    const DELIVERY_TYPE_OPTIONS = [
      { displayName: 'Standard', value: 'STANDARD' },
      { displayName: 'Express', value: 'EXPRESS' },
      { displayName: 'Next Day', value: 'NEXT_DAY' },
      { displayName: 'Same Day', value: 'SAME_DAY' },
    ];

    const ORDER_TYPE_OPTIONS = [
      { displayName: 'Normal', value: 'NORMAL' },
      { displayName: 'Return', value: 'RETURN' },
      { displayName: 'C2C', value: 'C2C' },
    ];

    const SIZE_OPTIONS = [
      { displayName: 'S', value: 'S' },
      { displayName: 'M', value: 'M' },
      { displayName: 'L', value: 'L' },
      { displayName: 'XL', value: 'XL' },
      { displayName: 'XXL', value: 'XXL' },
    ];

    const TIMESLOT_TYPE_OPTIONS = [
      { displayName: 'None', value: 'NONE' },
      { displayName: 'Day Night', value: 'DAY_NIGHT' },
      { displayName: 'Time Slot', value: 'TIMESLOT' },
    ];

    const MEASUREMENT_OPTIONS = [
      { displayName: 'Weight', value: { id: 0, name: 'WEIGHT' } },
      { displayName: 'Size', value: { id: 1, name: 'SIZE' } },
    ];

    const SCRIPT_TYPE = {
      NORMAL: 'normal',
      TIME_INBOUNDED: 'time_inbounded',
    };

    // dont change identation below
    const defaultSource = `function calculate(deliveryType, orderType, timeslotType, size, weight,
  fromZone, toZone, codValue, insuredValue) {
  var price = 0.0;\n
  var result = {};
  result.delivery_fee = price;
  result.cod_fee = 0.0;
  result.insurance_fee = 0.0;
  result.handling_fee = 0.0;
  return result;\n}`;

    const FIELDS = {
      no: { displayName: 'No' },
      id: { displayName: 'ID' },
      name: { displayName: 'container.pricing-scripts.script-name' },
      linkedShippers: { displayName: 'container.pricing-scripts.header-linked-shippers' },
      lastModified: { displayName: 'container.pricing-scripts.header-last-modified' },
      description: { displayName: 'commons.description' },
      draftActions: { displayName: 'commons.actions-abbr' },
    };

    const LEGENDS = [
      { variable: 'deliveryType', description: 'container.pricing-scripts.description-delivery-type', value: 'STANDARD EXPRESS NEXT_DAY SAME_DAY' },
      { variable: 'orderType', description: 'container.pricing-scripts.description-order-type', value: 'NORMAL RETURN C2C' },
      { variable: 'timeslotType', description: 'container.pricing-scripts.description-time-slot-type', value: 'NONE DAY_NIGHT TIMESLOT' },
      { variable: 'size', description: 'container.pricing-scripts.description-parcel-size', value: 'S M L XL XXL' },
      { variable: 'weight', description: 'container.pricing-scripts.description-parcel-weight', value: 'container.pricing-scripts.legends.any-number' },
      { variable: 'insuredValue', description: 'container.pricing-scripts.description-insured-value', value: 'container.pricing-scripts.legends.any-number' },
      { variable: 'codValue', description: 'container.pricing-scripts.description-cod-value', value: 'container.pricing-scripts.legends.any-number' },
      { variable: 'fromZone', description: 'container.pricing-scripts.description-origin-zone', value: 'commons.zone-object' },
      { variable: 'toZone', description: 'container.pricing-scripts.description-destination-zone', value: 'commons.zone-object' },
    ];

    return {
      FIELDS: FIELDS,
      DELIVERY_TYPE_OPTIONS: DELIVERY_TYPE_OPTIONS,
      ORDER_TYPE_OPTIONS: ORDER_TYPE_OPTIONS,
      TIMESLOT_TYPE_OPTIONS: TIMESLOT_TYPE_OPTIONS,
      MEASUREMENT_OPTIONS: MEASUREMENT_OPTIONS,
      SIZE_OPTIONS: SIZE_OPTIONS,
      DEFAULT_SCRIPT: defaultSource,
      LEGENDS: LEGENDS,
      SCRIPT_TYPE: SCRIPT_TYPE,

      readAllDrafts: function readAllDrafts() {
        return $http.get('script-engine/2.0/scripts?status=Draft').then(
          nvRequestUtils.success,
          nvRequestUtils.nvFailure
        );
      },

      readAllActive: function readAllActive() {
        return $http.get('script-engine/2.0/scripts?status=Active').then(
          nvRequestUtils.success,
          nvRequestUtils.nvFailure
        );
      },

      readAllChildren: function readAllChildren(parentId) {
        return $http.get(`script-engine/2.0/scripts?parent_id=${parentId}`).then(
          nvRequestUtils.success,
          nvRequestUtils.nvFailure
        );
      },

      read: function read(id) {
        const url = `script-engine/2.0/scripts/${id}`;
        return $http.get(url).then(
            pricingTemplateOk,
            nvRequestUtils.nvFailure
        );
      },
      update: function update(id, payload) {
        const url = `script-engine/2.0/scripts/${id}`;
        return $http.put(url, payload).then(
            nvRequestUtils.success,
            nvRequestUtils.nvFailure
        );
      },
      release: function release(id) {
        const url = `script-engine/2.0/scripts/${id}/release`;
        return $http.post(url, {}).then(
          nvRequestUtils.success,
          nvRequestUtils.nvFailure
        );
      },
      checkSyntax: function checkSyntax(payload) {
        const url = 'script-engine/2.0/scripts/validate';
        if (payload.source) {
          payload.source = payload.source;
        }
        return $http.post(url, payload).then(
          nvRequestUtils.success,
          nvRequestUtils.nvFailure
        );
      },
      create: function create(payload) {
        const url = 'script-engine/2.0/scripts';
        if (payload.source) {
          payload.source = payload.source;
        }
        return $http.post(url, payload).then(
            nvRequestUtils.success,
            nvRequestUtils.nvFailure
        );
      },
      delete: function deleteOne(id) {
        return $http.delete(`script-engine/2.0/scripts/${id}`).then(
            nvRequestUtils.success,
            nvRequestUtils.nvFailure
        );
      },

      getLinkedShippers: function getLinkedShippers(id) {
        const url = `script-engine/2.0/scripts/${id}/shippers`;
        return $http.get(url).then(
          pricingTemplateOk,
          nvRequestUtils.nvFailure
        );
      },

      linkShippers: function linkShippers(scriptId, shipperIds, force) {
        let url = `script-engine/2.0/scripts/${scriptId}/shippers`;
        if (typeof force !== 'undefined' && force !== null) {
          url += `?force=${force}`;
        }
        return $http.post(url, shipperIds).then(
          nvRequestUtils.success,
          nvRequestUtils.nvFailure
        );
      },

      test: function test(payload) {
        const url = 'script-engine/2.0/scripts/test';
        return $http.post(url, payload).then(
              nvRequestUtils.success,
              nvRequestUtils.nvFailure
          );
      },

      verify: function verify(payload) {
        const url = 'script-engine/2.0/scripts/verify';
        return $http.post(url, payload).then(
              nvRequestUtils.success,
              nvRequestUtils.nvFailure
          );
      },

      queryZone: function queryZone(zoneName, country) {
        const url = `addressing/pricing-zones/search?address=${zoneName}&country=${country}`;
        return $http.get(url).then(
              nvRequestUtils.success,
              nvRequestUtils.nvFailure
          );
      },

      getAllParameters: function getAllParameters() {
        const url = 'script-engine/2.0/parameters/list';
        return $http.post(url).then(
          nvRequestUtils.success,
          nvRequestUtils.nvFailure
        );
      },

      getZoneBase: function getZoneBase(country) {
        const url = `addressing/pricing-zones?country=${country}`;
        return $http.get(url).then(
              nvRequestUtils.success,
              nvRequestUtils.nvFailure
          );
      },

      getBillingZones: function getBillingZones() {
        const url = 'addressing/billing-zones'; // country is taken from http headers
        return $http.get(url).then(
              nvRequestUtils.success,
              nvRequestUtils.nvFailure
          );
      },

      getScript: shipperId => $http
        .get(`script-engine/2.0/shippers/${shipperId}/scripts`)
        .then(nvRequestUtils.success, $q.resolve), // make it error tollerant
    };

    function pricingTemplateOk(response) {
      const data = response.data;
      return $q.resolve(data);
    }
  }
}());
