(function model() {
  angular
    .module('nvCommons.models')
    .factory('Dash', Dash);

  Dash.$inject = ['$http', '$q', 'nvRequestUtils', '$window', '$rootScope', 'nvURL'];

  function Dash($http, $q, nvRequestUtils, $window, $rootScope, nvURL) {
    const self = {
      /**
       * login to ninja dash with a token generated by
       * @function createNinjaDashToken
       */
      loginDash: function login(token, shipperGlobalId) {
        return $window.open(
          `${nvURL.buildNinjaDashUrl(nv.config.env)}/login?token=${token}&shipper_id=${shipperGlobalId}`
        );
      },
      /**
       * retrieve shipper that has account in ninja dash
       * @response
       * [
       *  {
       *    shipper_id: 1, 
       *  },
       *  {
       *    shipper_id: 10
       *  }
       * ]
       */
      getNinjaDashShippers: function getNinjaDashShippers() {
        return $http.get('dash/1.0/shippers')
          .then(nvRequestUtils.success, nvRequestUtils.nvFailure);
      },
      /**
       * request a login token for ninja dash based on shipper global id
       */
      createNinjaDashToken: function createNinjaDashToken(shipperGlobalId) {
        return $http.get(`dash/1.0/users/access-token?shipper_id=${shipperGlobalId}`)
          .then(nvRequestUtils.success, nvRequestUtils.nvFailure);
      },

      checkDashEligibility: globalId =>
        $http.post(`dash/1.0/shippers/verify?shipper_id=${globalId}`)
          .then(nvRequestUtils.success, error => $q.reject(error)),
    };

    return self;
  }
}());
