(function factory() {
  angular
    .module('nvCommons.models')
    .factory('RouteManifest', RouteManifest);

  RouteManifest.$inject = ['$http', 'nvRequestUtils'];

  function RouteManifest($http, nvRequestUtils) {
    const fields = {
      sequence: { displayName: 'container.route-manifest.sequence' },
      id: { displayName: 'container.route-manifest.id' },
      countD: { displayName: 'container.route-manifest.count-d' },
      countP: { displayName: 'container.route-manifest.count-p' },
      status: { displayName: 'container.route-manifest.status' },
      priority: { displayName: 'container.route-manifest.priority' },
      address: { displayName: 'container.route-manifest.address' },
      trackingIds: { displayName: 'container.route-manifest.tracking-ids' },
      timeWindow: { displayName: 'container.route-manifest.time-window' },
      serviceEnd: { displayName: 'container.route-manifest.service-end' },
      distance: { displayName: 'container.route-manifest.distance' },
      reason: { displayName: 'container.route-manifest.reason' },
      addressee: { displayName: 'container.route-manifest.addressee' },
      contact: { displayName: 'container.route-manifest.contact' },
      cod: { displayName: 'container.route-manifest.cod' },
      comments: { displayName: 'container.route-manifest.comments' },
    };

    return {

      fields: fields,

      manifest: function manifest(routeId) {
        return $http.get(`route/2.0/routes/${routeId}/manifest`).then(
          nvRequestUtils.nvSuccess,
          nvRequestUtils.nvFailure
        );
      },
    };
  }
}());
