(function ReservationPresetModel() {
  angular
    .module('nvCommons.models')
    .factory('ReservationPreset', ReservationPreset);

  ReservationPreset.$inject = ['$http', 'nvRequestUtils'];

  function ReservationPreset($http, nvRequestUtils) {
    const FIELDS = {
      id: { displayName: 'commons.id' },
      name: { displayName: 'commons.name' },
      driver_id: { displayName: 'commons.driver' },
      hub_id: { displayName: 'commons.hub' },
    };
    const STATUS = {
      PENDING: 'PENDING',
      SUCCESS: 'SUCCESS',
    };

    return {
      FIELDS: FIELDS,
      STATUS: STATUS,

      getPendingTasks: () => $http.get('route/1.0/milkrun-pending-tasks')
        .then(nvRequestUtils.nvSuccess, nvRequestUtils.nvFailure),

      getGroups: filter => $http.get(`route/1.0/milkrun-groups?${nvRequestUtils.queryParam(filter)}`)
        .then(nvRequestUtils.nvSuccess, nvRequestUtils.nvFailure),

      createGroup: data => $http.post('route/1.0/milkrun-groups', data)
        .then(nvRequestUtils.success, nvRequestUtils.nvFailure),

      updateGroups: data => $http.put('route/1.0/milkrun-groups', data)
        .then(nvRequestUtils.nvSuccess, nvRequestUtils.nvFailure),

      deleteGroup: groupId => $http.delete(`route/1.0/milkrun-groups/${groupId}`)
        .then(nvRequestUtils.nvSuccess, nvRequestUtils.nvFailure),

      linkAddressToGroup: (groupId, pendingTaskId) => $http.post(`route/1.0/milkrun-pending-tasks/${pendingTaskId}/milkrun-groups/${groupId}`)
        .then(nvRequestUtils.success, nvRequestUtils.nvFailure),

      unlinkAddressFromShipper: pendingTaskId => $http.delete(`route/1.0/milkrun-pending-tasks/${pendingTaskId}`)
        .then(nvRequestUtils.success, nvRequestUtils.nvFailure),

      createRoutes: data => $http.post('route/1.0/milkrun-routes', data)
        .then(nvRequestUtils.nvSuccess, nvRequestUtils.nvFailure),

      groupToOptions: result =>
        _(result)
          .map(group => ({ displayName: group.name, value: group.id }))
          .sortBy('value')
          .value(),
    };
  }
}());
