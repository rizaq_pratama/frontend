(function factory() {
  angular
    .module('nvCommons.models')
    .factory('Route', Route);

  Route.$inject = [
    '$http',
    'nvRequestUtils',
    'nvModelUtils',
    'nvDateTimeUtils',
    'nvTimezone',
    'PDF',
    '$q',
  ];

  function Route(
    $http,
    nvRequestUtils,
    nvModelUtils,
    nvDateTimeUtils,
    nvTimezone,
    PDF,
    $q
  ) {
    const fields = {
      id: { displayName: 'commons.model.route-id' },
      date: { displayName: 'commons.model.route-date', defaultValue: moment().toDate() },
      tags: { displayName: 'commons.model.route-tags' },
      zoneId: { displayName: 'commons.zone' },
      routePassword: { displayName: 'commons.model.route-password' },
      hubId: { displayName: 'commons.model.hub' },
      driverId: { displayName: 'commons.model.assigned-driver' },
      vehicleId: { displayName: 'commons.model.vehicle' },
      comments: { displayName: 'commons.comments' },
      status: { displayName: 'commons.model.status' },
    };

    const STATUS = {
      PENDING: 0,
      IN_PROGRESS: 1,
      COMPLETED: 2,
      ARCHIVED: 3,
      ACCEPTED: 4,
      UNRESOLVED: 5,
    };

    const DRIVER_ATTENDANCE = {
      NOT_PRESENT: 0,
      LEFT: 1,
      RETURNED: 2,
    };

    const ROUTES_MAP = {};

    let migratedBaseUrl = 'core';
    if (nv.config.env === 'prod') {
      migratedBaseUrl = 'core-kafka';
    }

    const self = {

      STATUS: STATUS,
      DRIVER_ATTENDANCE: DRIVER_ATTENDANCE,

      getStatusEnum: function getStatusEnum(value) {
        let statusEnum = null;
        _.forEach(STATUS, (statusValue, key) => {
          if (statusValue === value) {
            statusEnum = key;
            return false;
          }

          return _.noop();
        });

        return statusEnum;
      },

      getDriverAttendanceEnum: function getDriverAttendanceEnum(value) {
        let driverAttendanceEnum = null;
        _.forEach(DRIVER_ATTENDANCE, (driverAttendanceValue, key) => {
          if (driverAttendanceValue === value) {
            driverAttendanceEnum = key;
            return false;
          }

          return _.noop();
        });

        return driverAttendanceEnum;
      },

      addByRouteGroup: function addByRouteGroup(options) {
        return $http.post('core/waypoints/routes/add-by-route-group', options).then(
          nvRequestUtils.success,
          nvRequestUtils.nvFailure
        );
      },

      suggestRoute: function suggestRoute(data) {
        return $http
          .post('core/2.0/routes/suggest', data)
          .then(nvRequestUtils.success, nvRequestUtils.nvFailure);
      },

      bulkAddToRoutes: function bulkAddToRoutes(data) {
        return $http
          .post('core/routes/orders/bulk-add', data)
          .then(nvRequestUtils.success, nvRequestUtils.nvFailure);
      },

      createRoutes: function createRoutes(data) {
        // data eg.:
        // [{
        //   "zoneId":16,
        //   "hubId":1,
        //   "date":"2016-07-19 16:00:00",
        //   "waypoints":[5201219,5178695,5193031],
        //   "tags":["SDS"]
        // }]

        return $http.post('core/routes', data).then(
          nvRequestUtils.success,
          $q.reject
        );
      },

      startBacklogRollover: function startBacklogRollover(options) {
        return $http.get(`core/start-backlogrollover?${nvRequestUtils.queryParam(options)}`).then(
          nvRequestUtils.success,
          nvRequestUtils.nvFailure
        );
      },

      updateRoutes: function updateRoutes(data) {
        return $http.put('core/routes', data).then(
          nvRequestUtils.success,
          nvRequestUtils.nvFailure
        );
      },

      /**
       * Search routes by ids and store it into cache
       * @param  {number} ids - route ids
       * @return {Promise} Promise with array of route objects
       */
      searchByIds: (ids) => {
        const results = [];
        const idsToSearch = [];
        _.forEach(_.uniq(_.compact(_.castArray(ids))), (id) => {
          if (ROUTES_MAP[id]) {
            results.push(ROUTES_MAP[id]);
          } else {
            idsToSearch.push(id);
          }
        });

        if (_.size(idsToSearch) <= 0) {
          return $q.when(results);
        }

        const deferred = $q.defer();
        const MAX_CALL_IN_CHUNK = 30;
        const chunkIds = _.chunk(idsToSearch, MAX_CALL_IN_CHUNK);
        const promises = [];
        _.forEach(chunkIds, (theIds) => {
          const params = {
            id: _.join(theIds, ','),
          };

          promises.push(
            self.search(params)
          );
        });
        $q.all(promises).then(success, failure);

        return deferred.promise;

        function success(responses) {
          let routes = [];
          _.forEach(responses, (response) => {
            routes = _.concat(routes, response);
          });

          const notFound = [];

          _.forEach(idsToSearch, (id) => {
            const theRoute = _.find(routes, ['id', id]);

            if (theRoute) {
              ROUTES_MAP[theRoute.id] = theRoute;
              results.push(theRoute);
            } else {
              notFound.push(id);
            }

            return _.noop();
          });

          deferred.resolve(results);
        }

        function failure(response) {
          deferred.reject(response);
        }
      },

      /**
       * Search routes - have to provide at least id / zone_id / hub_id / fr_date / to_date
       * @param {Object} options - filter object
       * @param {string|number[]} [options.id] - list of Long values, e.g. e.g. "1,2,3" or [1, 2, 3]
       * @param {string} [options.fr_date] - in the format YYYY-MM-DD
       * @param {string} [options.to_date] - in the format YYYY-MM-DD
       * @param {string|number[]} [options.zone_id] - list of Long values, e.g. "1,2,3" or [1, 2, 3]
       * @param {string|number[]} [options.hub_id] - list of hub ids, e.g. "1,2,3" or [1, 2, 3]
       * @param {boolean} [options.include_waypoints] - default false
       * @param {string} [options.status] - list of STATUS enum, eg. "ARCHIVED,PENDING"
       * @return {Promise} Promise with array of route objects
       */
      search: (options) => {
        const params = _.defaults(options, {
          include_waypoints: false,
        });

        return $http
          .get(`${migratedBaseUrl}/3.0/routes?${nvRequestUtils.queryParam(params)}`)
          .then(nvRequestUtils.success, nvRequestUtils.nvFailure);
      },

      read: function readRoutes(options) {
        const params = _.defaults(options, { include_waypoints: false });

        let url = 'core/routes';
        if (nv.config.env === 'prod') {
          url = 'core-kafka/routes';
        }

        return $http.get(`${url}?${nvRequestUtils.queryParam(params)}`).then(
          nvRequestUtils.success,
          nvRequestUtils.nvFailure
        );
      },

      delete: function deleteRoutes(routes) {
        return $http({
          method: 'DELETE',
          url: 'core/routes',
          data: routes,
          headers: { 'Content-Type': 'application/json' },
        }).then(
          nvRequestUtils.success,
          nvRequestUtils.nvFailure
        );
      },

      getZoneTransactions: function getZoneTransactions(options) {
        return $http.get(`core/routing/zonal?${nvRequestUtils.queryParam(options)}`).then(
          nvRequestUtils.success,
          nvRequestUtils.nvFailure
        );
      },

      getZoneTransactionsManual: options =>
        $http.get(`core/routing/manual?${nvRequestUtils.queryParam(options)}`).then(
          nvRequestUtils.success,
          nvRequestUtils.nvFailure
        ),

      // data eg.:
      // [{
      //   routeId: 123,
      //   tagIds: [1, 2]
      // }]
      updateTags: function updateTags(data) {
        return $http.put('route/1.0/routes/tags', data).then(
          nvRequestUtils.nvSuccess,
          nvRequestUtils.nvFailure
        );
      },

      // data eg.:
      // [{
      //   routeId: 123,
      //   driverTypeIds: [1, 2]
      // }]
      updateRouteDriverTypes: function updateRouteDriverTypes(data) {
        return $http.put('route/1.0/route-driver-types', data).then(
          nvRequestUtils.nvSuccess,
          nvRequestUtils.nvFailure
        );
      },

      updateRouteDriverAttendance: function updateRouteDriverAttendance(routeId, data) {
        return $http.put(`core/routes/${routeId}/driver-attendance`, data).then(
          nvRequestUtils.success,
          nvRequestUtils.nvFailure
        );
      },

      routeInboundEndSession: function routeInboundEndSession(routeId, data) {
        return $http.put(`core/routes/${routeId}/route-inbound-end-session`, data).then(
          nvRequestUtils.success,
          nvRequestUtils.nvFailure
        );
      },

      downloadManifestPdf: function downloadManifestPdf(ids) {
        return PDF.download(
          `core/manifestpdf?ids=${ids.concat(',')}`,
          'route_printout'
        );
      },

      downloadPasswordPdf: function downloadPasswordPdf(data) {
        return PDF.downloadWithPost('core/routes-password', data, 'routes_password');
      },

      updateRouteDetails: function updateRouteDetails(data) {
        return $http.put('core/routes/details', data).then(
          nvRequestUtils.success,
          nvRequestUtils.nvFailure
        );
      },

      reverifyAddress: routeId => $http
        .post(`core/routes/${routeId}/reverify`)
        .then(nvRequestUtils.success, nvRequestUtils.nvFailure),

      getRouteForId: function getRouteForId(routeId) {
        return $http.get(`core/drivers/v2/1/routes/${routeId}`)
          .then(nvRequestUtils.success, nvRequestUtils.nvFailure);
      },

      startRoute: function startRoute(routeId) {
        return $http.get(`core/drivers/1/routes/${routeId}/resuproutestart`)
          .then(nvRequestUtils.success, nvRequestUtils.nvFailure);
      },

      archiveRoutes: function archiveRoutes(data) {
        return $http.put('core/routes/archive', data).then(
          nvRequestUtils.success,
          nvRequestUtils.nvFailure
        );
      },

      optimize: function optimize(routeId) {
        return $http.get(`core/routes/${routeId}/optimize`).then(
          nvRequestUtils.success,
          nvRequestUtils.nvFailure
        );
      },

      // data eg.:
      // {
      //  routeIds: [123, 124]
      // }
      mergeTransactions: function mergeTransactions(data) {
        return $http.put('core/routes/mergetransactions', data).then(
          nvRequestUtils.success,
          nvRequestUtils.nvFailure
        );
      },

      optimizeSingleRouteSequence: function optimizeSingleRouteSequence(data) {
        return $http.post('vrp/optimizesingleroutesequence', data).then(
          nvRequestUtils.success,
          nvRequestUtils.nvFailure
        );
      },

      autoAssignDrivers: function autoAssignDrivers(data) {
        return $http.post('vrp/1.0/drivers/assign', data).then(
          nvRequestUtils.success,
          nvRequestUtils.nvFailure
        );
      },

      algoCreateRoutes: function algoCreateRoutes(data) {
        return $http.post('vrp/1.0/routes/assign/async', data).then(
          nvRequestUtils.success,
          nvRequestUtils.nvFailure
        );
      },

      algoCreateRoutesBulky: function algoCreateRoutesBulky(data) {
        return $http.post('vrp/1.0/routes/assign-bulky/async', data).then(
          nvRequestUtils.success,
          nvRequestUtils.nvFailure
        );
      },

      algoGetSolutions: function algoGetSolutions(jobId) {
        return $http.get(`vrp/1.0/routes/solutions/${jobId}`).then(
          nvRequestUtils.success,
          nvRequestUtils.nvFailure
        );
      },

      getRouteMonitoring: function getRouteMonitoring(filter) {
        return $http.get(`route/2.0/routes/monitoring?${nvRequestUtils.queryParam(filter)}`).then(
          nvRequestUtils.success,
          nvRequestUtils.nvFailure
        );
      },

      getPagedRouteMonitoring: function getPagedRouteMonitoring(filter) {
        return $http.get(`core/3.0/routes/monitoring?${nvRequestUtils.queryParam(filter)}`).then(
          nvRequestUtils.success,
          nvRequestUtils.nvFailure
        );
      },

      getAddFields: function getAddFields() {
        return nvModelUtils.pickFields(fields, [
          'date', 'tags', 'zoneId', 'hubId', 'driverId', 'vehicleId', 'comments',
        ], { setDefaultValues: true });
      },

      getEditFields: function getEditFields(model) {
        const editFields = nvModelUtils.pickFields(fields, [
          'date', 'tags', 'zoneId', 'hubId', 'driverId', 'vehicleId', 'comments',
        ], { setValues: true, model: model });
        editFields.date.value = moment(
          nvDateTimeUtils.displayDateTime(
            moment(editFields.date.value), nvTimezone.getOperatorTimezone()
          )
        ).toDate();

        return editFields;
      },

      getRouteByScan: trackingId => $http
        .get(`${migratedBaseUrl}/scans/get-routes-by-scan?scan=${trackingId}`)
        .then(nvRequestUtils.success, nvRequestUtils.nvFailure),

      getRouteBreak: function getRouteBreak(routeId) {
        return $http.get(`core/monitoring/routes/${routeId}/clean`)
          .then(nvRequestUtils.success, nvRequestUtils.nvFailure);
      },

      fetchRoute: options =>
        $http
          .get(`${migratedBaseUrl}/2.0/routes?${nvRequestUtils.queryParam(options)}`)
          .then(nvRequestUtils.success, nvRequestUtils.nvFailure),

      getVanInbounded: routeId =>
        $http
          .get(`core/routes/${routeId}/van-inbounds`)
          .then(nvRequestUtils.success, nvRequestUtils.nvFailure),

      FIELDS: fields,

    };

    return self;
  }
}());
