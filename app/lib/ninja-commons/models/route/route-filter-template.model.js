(function() {
  angular.module('nvCommons.models')
    .factory('RouteFilterTemplate', RouteFilterTemplate);

  RouteFilterTemplate.$inject = ['$http', 'nvRequestUtils', 'nvModelUtils'];

  function RouteFilterTemplate($http, nvRequestUtils, nvModelUtils) {
    return {

      create: function createFilterTemplate (data) {
        return $http.post('route/1.0/route-filter-templates', data)
          .then(nvRequestUtils.nvSuccess, nvRequestUtils.nvFailure);
      },

      read: function readFilterTemplate () {
        return $http.get('route/1.0/route-filter-templates')
          .then(nvRequestUtils.nvSuccess, nvRequestUtils.nvFailure);
      },

      delete: function deleteFilterTemplate (id) {
        return $http.delete('route/1.0/route-filter-templates/' + id)
          .then(nvRequestUtils.nvSuccess, nvRequestUtils.nvFailure);
      },

      update: function updateFilterTemplate (id, data) {
        return $http.put('route/1.0/route-filter-templates/' + id, data)
          .then(nvRequestUtils.nvSuccess, nvRequestUtils.nvFailure);
      },

      checkNameAvailability: function checkNameAvailability (name) {
        return $http.get('route/1.0/route-filter-templates/' + name + '/availability')
          .then(nvRequestUtils.nvSuccess, nvRequestUtils.nvFailure);
      },

      toOptions: function (data) {
        return _(data.routeFilterTemplates).map(template => {
          return {
            displayName: template.name,
            value: template.id
          };
        }).orderBy('value', 'desc').value();
      },
    }
  }
})();