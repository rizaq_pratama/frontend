(function model(){
  angular.module('nvCommons.models')
    .factory('RouteFilter', RouteFilter);

  RouteFilter.$inject = [
    'nvBackendFilterUtils',
    'nvDateTimeUtils',
    'nvTimezone',
    'Driver',
    'Hub',
    'Zone',
    'Tag',
    'DriverType',
  ];

  function RouteFilter (
    nvBackendFilterUtils,
    nvDateTimeUtils,
    nvTimezone,
    Driver,
    Hub,
    Zone,
    Tag,
    DriverType
  ) {
    return {
      init: init,
    };

    function init(result = {}) {

      const defaultFilters = [{
        type: nvBackendFilterUtils.FILTER_DATE,
        mainTitle: 'Route Date',
        fromModel: moment().toDate(),
        toModel: moment().toDate(),
        transformFn: transformDate,
        key: { from: 'from', to: 'to' },
        isMandatory: true,
        maxDateRange: 7,
        presetKey: { from: 'startDate', to: 'endDate' },
      }, {
        type: nvBackendFilterUtils.FILTER_OPTIONS,
        mainTitle: 'Hub',
        possibleOptions: !_.isUndefined(result.hubs) ? result.hubs : Hub.read(),
        selectedOptions: [],
        searchBy: 'name',
        searchText: {},
        key: 'hubIds',
        presetKey: 'hubIds',
        isMandatory: true,
      }];

      const possibleFilters = [{
        type: nvBackendFilterUtils.FILTER_BOOLEAN,
        mainTitle: 'Archived Routes',
        booleanModel: '10',
        key: ['archived', 'unarchived'],
        presetKey: 'archived',
      }, {
        type: nvBackendFilterUtils.FILTER_OPTIONS,
        mainTitle: 'Driver Type',
        possibleOptions: !_.isUndefined(result.driverTypes) ? result.driverTypes : getDriverTypes,
        selectedOptions: [],
        searchBy: 'name',
        searchText: {},
        key: 'driverTypeIds',
        presetKey: 'driverTypeIds',
      }, {
        type: nvBackendFilterUtils.FILTER_OPTIONS,
        mainTitle: 'Driver',
        possibleOptions: !_.isUndefined(result.drivers) ? result.drivers : getDrivers,
        selectedOptions: [],
        searchBy: 'firstName',
        searchText: {},
        key: 'driverIds',
        presetKey: 'driverIds',
      }, {
        type: nvBackendFilterUtils.FILTER_OPTIONS,
        mainTitle: 'Tags',
        possibleOptions: !_.isUndefined(result.tags) ? result.tags : getTags,
        selectedOptions: [],
        searchBy: 'name',
        searchText: {},
        key: 'tagIds',
        presetKey: 'tagIds',
      }, {
        type: nvBackendFilterUtils.FILTER_OPTIONS,
        mainTitle: 'Zone',
        possibleOptions: !_.isUndefined(result.zones) ? result.zones : getZones,
        selectedOptions: [],
        searchBy: 'name',
        searchText: {},
        key: 'zoneIds',
        presetKey: 'zoneIds',
      }];

      return {
        default: defaultFilters,
        possible: possibleFilters,
      };
    }

    function getDriverTypes() {
      return DriverType.searchAll().then(response => response.driverTypes);
    }

    function getDrivers() {
      return Driver.searchAll().then(response => response.drivers)
    }

    function getTags() {
      return Tag.all().then(response => response.tags);
    }

    function getZones() {
      return Zone.read();
    }

    function transformDate(date, type) {
      if (type === 'from') {
        const startDate = nvDateTimeUtils.toSOD(date, nvTimezone.getOperatorTimezone());
        return nvDateTimeUtils.displayDateTime(startDate, 'utc');
      }
      if (type === 'to') {
        const endDate = nvDateTimeUtils.toEOD(date, nvTimezone.getOperatorTimezone());
        return nvDateTimeUtils.displayDateTime(endDate, 'utc');
      }
    }
  }
})();