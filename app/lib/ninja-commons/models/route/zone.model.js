(function model() {
  angular
        .module('nvCommons.models')
        .factory('Zone', Zone);

  Zone.$inject = ['$http', '$q', 'nvRequestUtils'];

  function Zone($http, $q, nvRequestUtils) {
    const fields = {
      id: { displayName: 'commons.id', type: 'number' },
      name: { displayName: 'commons.name', type: 'text' },
      short_name: { displayName: 'commons.short-name', type: 'text' },
      hub_id: { displayName: 'commons.hub-id', type: 'number' },
      description: { displayName: 'commons.description', type: 'text' },
      latitude: { displayName: 'commons.latitude', type: 'date' },
      longitude: { displayName: 'commons.longitude', type: 'date' },
    };
    let ZONES_MAP = {};

    function remapIds(response) {
      ZONES_MAP = {};
      const data = response.data;
      _.forEach(data, (zone) => {
        zone.internal_id = zone.id;
        zone.id = zone.legacy_zone_id;

        ZONES_MAP[zone.id] = zone;
      });
      return $q.resolve(data);
    }

    function remapId(response) {
      const zone = response.data;
      zone.internal_id = zone.id;
      zone.id = zone.legacy_zone_id;
      return $q.resolve(zone);
    }

    const self = {
      FIELDS: fields,
      get: function get(id) {
        return $http.get(`addressing/1.0/zones/${id}`).then(remapId, nvRequestUtils.nvFailure);
      },
      read: function read(withPolygon = false) {
        return $http.get(`addressing/1.0/zones?with_polygon=${withPolygon}`).then(
          remapIds,
          nvRequestUtils.nvFailure
        );
      },

      reloadRoutingZonesCache: function reloadRoutingZonesCache() {
        return $http.get('addressing/1.0/zones/reload-cache').then(
          remapIds,
          nvRequestUtils.nvFailure
        );
      },

      createRoutingZone: function createRoutingZone(data) {
        return $http.post('addressing/1.0/zones', data).then(
          remapId,
          nvRequestUtils.nvFailure
        );
      },

      updateRoutingZone: function updateRoutingZone(id, data) {
        return $http.put(`addressing/1.0/zones/${id}`, data).then(
          remapId,
          nvRequestUtils.nvFailure
        );
      },

      deleteRoutingZone: function deleteRoutingZone(id) {
        return $http.delete(`addressing/1.0/zones/${id}`).then(
          nvRequestUtils.success,
          nvRequestUtils.nvFailure
        );
      },

      toOptions: function toOptions(data, sortByDisplayName = true) {
        if (sortByDisplayName) {
          return _(data)
            .map(zone => ({ displayName: _.toUpper(zone.name), value: zone.id }))
            .sortBy('displayName')
            .value();
        }

        return _(data)
                 .map(zone => ({ displayName: zone.name, value: zone.id }))
                 .sortBy('value')
                 .value();
      },

      toFilterOptions: function toFilterOptions(data) {
        return _.map(data, zone => ({
          id: zone.id,
          displayName: zone.name,
          lowercaseName: zone.name.toLowerCase(),
        }));
      },

      toFilterOptionsWithIdName: function toFilterOptions(data) {
        return _.map(data, zone => ({
          id: zone.id,
          displayName: `${zone.id}-${zone.name}`,
          lowercaseName: zone.name.toLowerCase(),
        }));
      },

      searchZoneByCoords: function searchZoneByCoords(payload) {
        return $http.post('addressing/1.0/zones/search', payload)
        .then(
          nvRequestUtils.success,
          nvRequestUtils.nvFailure
        );
      },

      searchByIds: (ids) => {
        const RESULTS = [];
        const idsToSearch = [];

        _.forEach(_.uniq(_.compact(_.castArray(ids))), (id) => {
          if (ZONES_MAP[id]) {
            RESULTS.push(ZONES_MAP[id]);
          } else {
            idsToSearch.push(id);
          }
        });

        if (_.size(idsToSearch) <= 0) {
          return $q.when(RESULTS);
        }

        const deferred = $q.defer();
        self.read().then(success, failure);
        return deferred.promise;

        function success(response) {
          const zones = response;
          const notFoundIds = [];

          _.forEach(idsToSearch, (id) => {
            const theZone = _.find(zones, { id: id });
            if (theZone) {
              ZONES_MAP[id] = theZone;
              RESULTS.push(theZone);
            } else {
              notFoundIds.push(id);
            }
            deferred.resolve(RESULTS);
          });
        }

        function failure(response) {
          deferred.reject(response);
        }
      },

    };

    return self;
  }
}());
