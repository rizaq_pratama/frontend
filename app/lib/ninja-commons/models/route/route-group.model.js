(function RouteGroupModel() {
  angular
    .module('nvCommons.models')
    .factory('RouteGroup', RouteGroup);

  RouteGroup.$inject = ['$http', 'nvRequestUtils', 'nvModelUtils', 'nvUtils'];

  function RouteGroup($http, nvRequestUtils, nvModelUtils, nvUtils) {
    const fields = {
      id: { displayName: 'commons.sn', type: 'number' },
      name: { displayName: 'commons.model.group-name', type: 'text' },
      description: { displayName: 'commons.description', type: 'text' },
      hub: { displayName: 'commons.model.hub', type: 'text', options: [], selectedOptions: [] },
      createdAt: { displayName: 'commons.created-on', type: 'date' },
      updatedAt: { displayName: 'commons.updated-on', type: 'date' },
      deletedAt: { displayName: 'commons.deleted-on', type: 'date' },
    };

    return {
      FIELDS: fields,

      create: (name, description) =>
        $http
          .post('route/1.0/route-groups', { name: name, description: description })
          .then(nvRequestUtils.nvSuccess, nvRequestUtils.nvFailure),

      read: () =>
        $http
          .get('route/1.0/route-groups')
          .then(nvRequestUtils.nvSuccess, nvRequestUtils.nvFailure),

      update: (id, name, description) =>
        $http
          .put(`route/1.0/route-groups/${id}`, { name: name, description: description })
          .then(nvRequestUtils.nvSuccess, nvRequestUtils.nvFailure),

      delete: id =>
        $http
          .delete(`route/1.0/route-groups/${id}`)
          .then(nvRequestUtils.nvSuccess, nvRequestUtils.nvFailure),

      appendReferences: (id, data) =>
        $http
          .post(`route/1.0/route-groups/${id}/references?append=true`, data)
          .then(nvRequestUtils.nvSuccess, nvRequestUtils.nvFailure),

      removeReferences: (id, data) =>
        $http
          .post(`route/1.0/route-groups/${id}/references?remove=true`, data)
          .then(nvRequestUtils.nvSuccess, nvRequestUtils.nvFailure),

      toOptions: (routeGroups, orderBy = 'displayName', sortBy = 'asc') =>
        _.orderBy(_.map(routeGroups, routeGroup => ({
          displayName: routeGroup.name, value: routeGroup.id,
        })), [(routeGroup) => {
          if (orderBy !== 'displayName') {
            return routeGroup[orderBy];
          }

          return routeGroup[orderBy].toLowerCase();
        }], [sortBy]),

      setOptions: (props, options) => {
        nvUtils.getDeep(fields, props).options = options;
      },

      getAddFields: () =>
        nvModelUtils.pickFields(fields, [
          'name', 'description', 'hub',
        ], { setDefaultValues: true }),

      getEditFields: model =>
        nvModelUtils.pickFields(fields, [
          'id', 'name', 'description', 'hub', 'createdAt',
        ], { setValues: true, model: model }),

      optimize: (id, data) =>
        $http.post(`vrp/1.0/route-groups/${id}/optimize`, data).then(
          nvRequestUtils.success,
          nvRequestUtils.nvFailure
        ),
      optimizeAsync: function optimizeAsync(id, data) {
        return $http.post(`vrp/1.0/route-groups/${id}/optimize/async`, data)
          .then(nvRequestUtils.success, nvRequestUtils.nvFailure);
      },
      poolSolution: function poolSolution(jobId) {
        return $http.get(`vrp/1.0/routes/solutions/${jobId}`)
          .then(nvRequestUtils.success, nvRequestUtils.nvFailure);
      },
      saveSuggestedRoute: function saveSuggestedRoute(payload) {
        return $http.put('core/bulky/orders/suggested-timeslots', payload)
          .then(nvRequestUtils.success, nvRequestUtils.nvFailure);
      },

    };
  }
}());
