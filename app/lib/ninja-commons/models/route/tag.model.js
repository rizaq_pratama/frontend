(function tagModel() {
  angular
    .module('nvCommons.models')
    .factory('Tag', Tag);

  Tag.$inject = ['$http', 'nvRequestUtils', 'nvModelUtils'];

  function Tag($http, nvRequestUtils, nvModelUtils) {
    const fields = {
      id:          { displayName: 'commons.id',          type: 'number'                           },
      name:        { displayName: 'commons.tag-name',    type: 'text', minLength: 3, maxLength: 3 },
      description: { displayName: 'commons.description', type: 'text'                             },
      createdAt:   { displayName: 'commons.created-on',  type: 'date'                             },
      updatedAt:   { displayName: 'commons.updated-on',  type: 'date'                             },
      deletedAt:   { displayName: 'commons.deleted-on',  type: 'date'                             },
    };

    const RESERVED_TAG = {
      RESEQUENCED: 'RES',
    };

    return {

      FIELDS: fields,
      RESERVED_TAG: RESERVED_TAG,

      all: () => $http
        .get('route/1.0/tags')
        .then(nvRequestUtils.nvSuccess, nvRequestUtils.nvFailure),

      create: tag => $http
        .post('route/1.0/tags', tag)
        .then(nvRequestUtils.nvSuccess, nvRequestUtils.nvFailure),

      updateOne: tag => $http
        .put(`route/1.0/tags/${tag.id}`, tag)
        .then(nvRequestUtils.nvSuccess, nvRequestUtils.nvFailure),

      deleteOne: tag => $http
        .delete(`route/1.0/tags/${tag.id}`, tag)
        .then(nvRequestUtils.nvSuccess, nvRequestUtils.nvFailure),

      getAddFields: () => nvModelUtils.pickFields(fields, [
        'name',
        'description'
      ], { setDefaultValues: true }),

      getEditFields: model => nvModelUtils.pickFields(fields, [
        'id',
        'name',
        'description'
      ], { setValues: true, model: model }),

      getTagId: (key, tags) => _.result(_.find(tags, ['name', key]), 'id', 0),

      toOptions: data => _(data.tags)
        .map(tag => ({ displayName: tag.name, value: tag.id }))
        .sortBy('displayName')
        .value(),

      setValues: (data, values) => _(data.tags)
        .map(tag => _.assign(tag, { _value: !!values[tag.name] }))
        .sortBy('name')
        .value(),

    };
  }
}());
