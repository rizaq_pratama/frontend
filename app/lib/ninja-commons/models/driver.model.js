(function model() {
  angular
    .module('nvCommons.models')
    .factory('Driver', Driver);

  Driver.$inject = [
    '$http', 'nvModelUtils', 'nvUtils', '$q', 'nvRequestUtils',
  ];

  function Driver(
    $http, nvModelUtils, nvUtils, $q, nvRequestUtils
  ) {
    const fields = {
      uuid: { displayName: 'UUID' },
      id: { displayName: 'commons.id' },
      firstName: { displayName: 'First Name' },
      lastName: { displayName: 'Last Name' },
      licenseNumber: { displayName: 'Driver License Number' },
      driverType: { displayName: 'container.driver-strength.header-type', /* 'Driver Type' */ options: [] },
      driverTypeId: { displayName: 'Driver Type Id' },
      availability: { displayName: 'Availability', defaultValue: false },
      codLimit: { displayName: 'COD Limit', defaultValue: 1000000000 },
      vehicles: {
        _array: true,
        _values: [],
        vehicleType: { displayName: 'container.driver-strength.header-vehicle', /* 'Vehicle Type'*/ options: [] },
        vehicleNo: { displayName: 'License Number' },
        ownVehicle: { displayName: 'container.driver-strength.header-own' /* 'Own Vehicle' */ },
        active: { displayName: 'Active' },
        capacity: { displayName: 'Vehicle Capacity' },
      },
      contacts: {
        _array: true,
        _values: [],
        type: { displayName: 'Contact Type', options: [] },
        details: { displayName: 'Contact' },
      },
      zonePreferences: {
        _array: true,
        _values: [],
        uuid: { displayName: 'UUID' },
        zoneId: { displayName: 'Zone', options: [] },
        zoneUuid: { displayName: 'Zone UUID' },
        latitude: { displayName: 'Seed Latitude' },
        longitude: { displayName: 'Seed Longitude' },
        minWaypoints: { displayName: 'Min' },
        maxWaypoints: { displayName: 'Max' },
        rank: { displayName: 'Rank' },
        cost: { displayName: 'Cost' },
      },
      tags: {
        _deep: true,
        RESUPPLY: { displayName: 'container.driver-strength.allow-bidding' },
      },
      maxBiddableWaypoints: { displayName: 'Maximum Bid Limit' },
      maxOnDemandJobs: { displayName: 'commons.model.maximum-on-demand-waypoints', defaultValue: 0 },
      username: { displayName: 'Username' },
      password: { displayName: 'Password' },
      comments: { displayName: 'commons.comments' /* 'Comments' */ },
      hub: { displayName: 'commons.model.hub', options: [], selectedOptions: [] },
    };

    const FAILURE_REASON_CODE_LIABILITY = {
      EXTERNAL: 'EXTERNAL',
      NINJAVAN: 'NINJAVAN',
    };

    const MAP_KEY = {
      DRIVERS: 'drivers',
      FAILURE_REASONS: 'failure_reasons',
    };
    const MAPS = {
      drivers: {},
      failure_reasons: {},
    }; // map by id

    const self = {
      FAILURE_REASON_CODE_LIABILITY: FAILURE_REASON_CODE_LIABILITY,

      searchOne: uuid =>
        $http.get(`driver/1.1/drivers/${uuid}`)
          .then(nvRequestUtils.nvSuccess, nvRequestUtils.nvFailure)
          .then(toClientFormat),

      searchAll: (options) => {
        const params = _.defaults(options, {
          all: true,
          refresh: false,  // set true to reload Hazelcast map
        });

        return $http.get(`driver/1.1/drivers?${nvRequestUtils.queryParam(params)}`)
          .then(nvRequestUtils.nvSuccess, nvRequestUtils.nvFailure)
          .then(toClientFormat);
      },

      searchOneFromResponse: data =>
        self.searchOne(data.uuid),

      createOne: (data) => {
        const payload = {
          driver: toServerFormat(_.cloneDeep(data)),
        };

        return $http.post('driver/2.0/drivers', payload)
          .then(nvRequestUtils.nvSuccess, nvRequestUtils.nvFailure)
          .then(self.searchOneFromResponse)
          .then(_.partial(mergeSuccessResponse, data));
      },

      updateOne: (uuid, data) => {
        const payload = {
          driver: toServerFormat(_.cloneDeep(data)),
        };

        return $http.put(`driver/2.0/drivers/${uuid}`, payload)
          .then(nvRequestUtils.nvSuccess, nvRequestUtils.nvFailure)
          .then(self.searchOneFromResponse)
          .then(_.partial(mergeSuccessResponse, data));
      },

      deleteOne: uuid =>
        $http.delete(`driver/2.0/drivers/${uuid}`)
          .then(nvRequestUtils.nvSuccess, nvRequestUtils.nvFailure),

      updateAvailability: (uuid, availability) => {
        if (_.isArray(uuid)) {
          return $http.put('driver/1.0/drivers-availability', {
            driverUuids: uuid,
            availability: !!availability,
          }).then(nvRequestUtils.nvSuccess, nvRequestUtils.nvFailure);
        }

        return $http.put(`driver/1.0/drivers/${uuid}/availability`, {
          availability: !!availability,
        }).then(nvRequestUtils.nvSuccess, nvRequestUtils.nvFailure);
      },

      checkUsernameAvailability: username =>
        $http.get(`driver/1.0/driver-usernames/${username}/availability`)
          .then(nvRequestUtils.nvSuccess, nvRequestUtils.nvFailure),

      setOptions: (props, options) => {
        nvUtils.getDeep(fields, props).options = options;
      },

      getDriveryById: function getDriverById(id) {
        return $http.get(`driver/1.2/drivers/${id}`)
          .then(nvRequestUtils.nvSuccess, nvRequestUtils.nvFailure);
      },

      searchByIds: (ids) => {
        const RESULT = [];
        const idsToSearch = [];

        _.forEach(_.uniq(_.compact(_.castArray(ids))), (id) => {
          if (MAPS[MAP_KEY.DRIVERS][id]) {
            RESULT.push(MAPS[MAP_KEY.DRIVERS][id]);
          } else {
            idsToSearch.push(id);
          }
        });

        if (_.size(idsToSearch) <= 0) {
          return $q.when(RESULT);
        }

        const deferred = $q.defer();
        const MAX_CALL_IN_CHUNK = 30;
        const chunkIds = _.chunk(idsToSearch, MAX_CALL_IN_CHUNK);
        const promises = [];
        _.forEach(chunkIds, (theIds) => {
          const params = {
            driverIds: _.join(theIds, ','),
            all: false,
            refresh: false,
            itemsPerPage: _.size(theIds),
          };

          promises.push(
            self.searchAll(params)
          );
        });

        $q.all(promises).then(success, failure);

        return deferred.promise;

        function success(responses) {
          let drivers = [];
          _.forEach(responses, (response) => {
            const clientFormatted = response;
            drivers = _.concat(
              drivers,
              _.get(clientFormatted, 'drivers') || _.castArray(_.get(clientFormatted, 'driver'))
            );
          });

          const notFoundIds = [];
          _.forEach(idsToSearch, (id) => {
            const theDriver = _.find(drivers, { id: id });
            if (theDriver) {
              MAPS[MAP_KEY.DRIVERS][id] = theDriver;
              RESULT.push(theDriver);
            } else {
              notFoundIds.push(id);
            }
          });
          deferred.resolve(RESULT);
        }

        function failure(response) {
          deferred.reject(response);
        }
      },

      searchFailureReasonsByIds: (ids) => {
        const RESULT = [];
        const idsToSearch = [];

        _.forEach(_.uniq(_.compact(_.castArray(ids))), (id) => {
          if (MAPS[MAP_KEY.FAILURE_REASONS][id]) {
            RESULT.push(MAPS[MAP_KEY.FAILURE_REASONS][id]);
          } else {
            idsToSearch.push(id);
          }
        });

        if (_.size(idsToSearch) <= 0) {
          return $q.when(RESULT);
        }

        const deferred = $q.defer();
        const MAX_CALL_IN_CHUNK = 30;
        const chunkIds = _.chunk(idsToSearch, MAX_CALL_IN_CHUNK);
        const promises = [];
        _.forEach(chunkIds, (theIds) => {
          promises.push(
            self.getFailureReasonsByIds(theIds)
          );
        });

        $q.all(promises).then(success, failure);

        return deferred.promise;

        function success(responses) {
          let failureReasons = [];
          _.forEach(responses, (response) => {
            failureReasons = _.concat(failureReasons, _.get(response, 'failureReasons'));
          });

          const notFoundIds = [];
          _.forEach(idsToSearch, (id) => {
            const theReason = _.find(failureReasons, { id: id });
            if (theReason) {
              MAPS[MAP_KEY.FAILURE_REASONS][id] = theReason;
              RESULT.push(theReason);
            } else {
              notFoundIds.push(id);
            }
          });
          deferred.resolve(RESULT);
        }

        function failure(response) {
          deferred.reject(response);
        }
      },

      getOptions: props =>
        nvUtils.getDeep(fields, props).options,

      toOptions: data =>
        _(data.drivers)
            .map(driver => ({
              displayName: _.compact([driver.firstName, driver.lastName]).join(' '),
              value: driver.id,
            }))
            .sortBy('displayName')
            .value(),

      toFilterOptions: (data, { optionsInclude = [] } = {}) =>
        _.map(data.drivers, (driver) => {
          let additionalProperties = {};
          const name = _.compact([driver.firstName, driver.lastName]).join('');

          if (optionsInclude.length > 0) {
            additionalProperties = _.pick(driver, optionsInclude);
          }

          return _.merge(additionalProperties, {
            id: driver.id,
            displayName: name,
            lowercaseName: name.toLowerCase(),
          });
        }),

      getAddFields: () =>
        nvModelUtils.pickFields(fields, [
          'firstName', 'lastName', 'licenseNumber', 'driverType', 'availability', 'codLimit',
          'vehicles', 'contacts', 'zonePreferences', 'tags', 'maxBiddableWaypoints', 'maxOnDemandJobs',
          'username', 'password', 'comments', 'hub',
        ], { setDefaultValues: true }),

      getEditFields: (driver) => {
        const editFields = nvModelUtils.pickFields(fields, [
          'uuid', 'id', 'firstName', 'lastName', 'licenseNumber', 'driverType', 'availability', 'codLimit',
          'vehicles', 'contacts', 'zonePreferences', 'tags', 'maxBiddableWaypoints', 'maxOnDemandJobs',
          'username', 'password', 'comments', 'hub',
        ], { setValues: true, setArrayValues: true, setDefaultValues: true, model: driver });

        editFields.zonePreferences._values = _(editFields.zonePreferences._values)
                    .orderBy('rank')
                    .forEach(mapFn);

        if (driver.hub !== null) {
          editFields.hub.value = driver.hub;
          editFields.hub.selectedOptions.push(editFields.hub.value);
          _.remove(editFields.hub.options, editFields.hub.value);
        }

        return editFields;

        function mapFn(zonePreference, idx) {
          zonePreference.rank = idx + 1;
        }
      },

      isReserveFleetDriver: driver =>
        driver.tags && nvModelUtils.fromYesNoBitStringToBool(driver.tags.RESUPPLY) === true,

      updatePreferences: function updatePreferences(templateId, data) {
        return $http.put(`driver/2.0/driver-templates/${templateId}/preferences`, data).then(
          nvRequestUtils.nvSuccess,
          nvRequestUtils.nvFailure
        );
      },

      searchFailureReasons: (params) => {
        const opt = _.defaults(params, {
          all: true,
          active: true,
          language: 'en',
          sortByOrder: true,
        });

        return $http.get(`driver/1.0/failure-reasons?${nvRequestUtils.queryParam(opt)}`)
          .then(nvRequestUtils.nvSuccess, nvRequestUtils.nvFailure);
      },

      getFailureReasonsByIds: ids =>
        $http.get(`driver/1.1/failure-reasons?ids=${_.join(ids, ',')}`)
          .then(nvRequestUtils.nvSuccess, nvRequestUtils.nvFailure),

      readDriverName: (driver) => {
        if (driver) {
          const str = `${driver.firstName ? driver.firstName : ''} ${driver.lastName ? driver.lastName : ''}`.trim();
          return str.length !== 0 ? str : 'NO NAME';
        }
        return 'NO NAME';
      },

      FIELDS: fields,

    };

    return self;

    function mergeSuccessResponse(oldData, newData) {
      oldData.password = null; // clear the password before returning
      return _.merge(oldData, newData.driver);
    }

    function toClientFormat(data) {
      const drivers = data.driver ? [data.driver] : data.drivers;

      _.forEach(drivers, (driver) => {
        mergeOnVehicles(driver.vehicles, nvModelUtils.fromBoolToYesNoBitString);
        mergeOnTags(driver.tags, nvModelUtils.fromBoolToYesNoBitString);
        driver.hub = _.find(fields.hub.options, { value: driver.hubId }) || null;
      });

      return data;
    }

    function toServerFormat(driver) {
      mergeOnVehicles(driver.vehicles, nvModelUtils.fromYesNoBitStringToBool);
      mergeOnTags(driver.tags, nvModelUtils.fromYesNoBitStringToBool);
      if (driver.hub !== null) {
        driver.hubId = driver.hub.value;
      }
      return driver;
    }

    function mergeOnVehicles(vehicles, callback) {
      _.forEach(vehicles, (vehicle) => {
        _.merge(vehicle, { ownVehicle: callback(vehicle.ownVehicle) });
      });
    }

    function mergeOnTags(tags, callback) {
      tags.RESUPPLY = callback(tags.RESUPPLY);
    }
  }
}());
