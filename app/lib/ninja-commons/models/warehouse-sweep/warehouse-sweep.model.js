(function model() {
  angular
  .module('nvCommons.models')
  .factory('WarehouseSweep', WarehouseSweep);

  WarehouseSweep.$inject = ['nvRequestUtils', '$http', '$q'];

  function WarehouseSweep(nvRequestUtils, $http, $q) {
    let baseUrl = 'core';
    if (nv.config.env === 'prod') {
      baseUrl = 'core-kafka';
    }

    const IS_DEBUG = false;

    function passError(err) {
      return $q.reject(err);
    }


    return {
      warehouseSweep: (trackingId, hubId) => {
        const payload = [
          {
            scan: trackingId,
            hubId: hubId,
          },
        ];
        if (IS_DEBUG) {
          return $q.resolve([{ scan: trackingId, hubId: hubId, id: 789 }]);
        }

        return $http.post('core/warehousesweeps', payload)
          .then(nvRequestUtils.success, passError);
      },
      warehouseSweepForShipment: (trackingId, hubId, shipmentId, data) => {
        const payload = [
          {
            scan: trackingId,
            hubId: hubId,
            shipmentId: shipmentId,
            result: data,
          },
        ];
        if (IS_DEBUG) {
          return $q.resolve([{ scan: trackingId, hubId: hubId, id: 789 }]);
        }

        return $http.post('core/warehousesweepsforshipment', payload)
          .then(nvRequestUtils.success, passError);
      },
      populateMissing: () => ($http.post(`${baseUrl}/inbounds/populatemissing`)
      .then(nvRequestUtils.success, nvRequestUtils.nvFailure)),

      outboundParcel: trackingId =>
        $http.get(`core/outbound/parcel?scan=${trackingId}`)
          .then(nvRequestUtils.success, nvRequestUtils.nvFailure),

      fetch: () => {
        if (IS_DEBUG) {
          return $.getJSON('views/container/parcel-sweeper/json/dest.json', dest => ($q.resolve(dest)));
        }
        return $http.get(`${baseUrl}/outbound/destinationsessionoptimized`)
          .then(nvRequestUtils.success, nvRequestUtils.nvFailure);
      },

      fetchByHub: (hubIds) => {
        const payload = {
          hub_ids: hubIds,
        };
        return $http.post(`${baseUrl}/warehouse/sort/search`, payload)
          .then(nvRequestUtils.success, nvRequestUtils.nvFailure);
      },

      fetchWithShipment: (shipmentId) => {
        if (IS_DEBUG) {
          return $.getJSON('views/container/shipment-inbound/json/dest.json', dest => ($q.resolve(dest)));
        }
        return $http.get(`${baseUrl}/outbound/destinationsessionoptimized/shipments/${shipmentId}`)
          .then(nvRequestUtils.success, nvRequestUtils.nvFailure);
      },
    };
  }
}());
