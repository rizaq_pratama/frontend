(function model() {
  angular
    .module('nvCommons.models')
    .factory('Wallet', Wallet);

  Wallet.$inject = ['$http', 'nvRequestUtils'];

  function Wallet($http, nvRequestUtils) {
    return {
      /**
       * readjust prepaid shipper balance
       * @param {Object} payload - wallet readjustment object
       * @param {string} [payload.external_transaction_ref] - anything unique
       * @param {string} [payload.currency] - in the format YYYY-MM-DD
       * @param {number} [payload.amount] - positive number
       * @param {string} [payload.timestamp] - UTC time, e.g. "2018-10-18T04:45:17Z"
       * @param {number} [payload.shipper_id] - legacy shipper id
       * @param {string} [payload.country] - country code, e.g. "SG"
       * @param {string} [payload.comments] - description of readjustment
       * @param {string} [payload.type] - type of readjustment, "DEBIT or CREDIT"
       * @return {Promise} Promise with response of wallet readjustment
      */
      readjust: payload =>
        $http
          .post('wallet/1.0/transactions/prepaid-account-correction', payload)
          .then(nvRequestUtils.success, nvRequestUtils.nvFailure),
    };
  }
}());
