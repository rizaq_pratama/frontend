(function api() {
  angular
  .module('nvCommons.models')
  .factory('DeliveryTypesModel', DeliveryTypesModel);

  DeliveryTypesModel.$inject = ['$http', 'nvRequestUtils'];

  function DeliveryTypesModel($http, nvRequestUtils) {
    return {
      getTypes: function getTypes() {
        return $http.get('core/deliverytypes')
          .then(nvRequestUtils.success, nvRequestUtils.nvFailure);
      },
    };
  }
}());
