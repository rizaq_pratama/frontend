(function vehicleTypeModel() {
  angular
    .module('nvCommons.models')
    .factory('VehicleType', VehicleType);

  VehicleType.$inject = ['$http', 'nvModelUtils', 'nvRequestUtils'];

  function VehicleType($http, nvModelUtils, nvRequestUtils) {
    const fields = {
      id: { displayName: 'ID' },
      name: { displayName: 'Name' },
      createdAt: { displayName: 'Created At' },
      updatedAt: { displayName: 'Updated At' },
      deletedAt: { displayName: 'Deleted At' },
    };

    return {
      searchAll: (options) => {
        const params = _.defaults(options || {}, {
          refresh: false, // set true to reload Hazelcast map
        });

        return $http.get(`driver/1.0/vehicle-types${(params.refresh ? '?refresh=true' : '')}`)
          .then(nvRequestUtils.nvSuccess, nvRequestUtils.nvFailure);
      },

      createOne: data =>
        $http.post('driver/1.0/vehicle-types', data)
          .then(nvRequestUtils.nvSuccess, nvRequestUtils.nvFailure),

      updateOne: (options) => {
        const data = _.omit(options, 'vehicleType.id');

        return $http.put(`driver/1.0/vehicle-types/${options.vehicleType.id}`, data)
          .then(nvRequestUtils.nvSuccess, nvRequestUtils.nvFailure);
      },

      deleteOne: id =>
        $http.delete(`driver/1.0/vehicle-types/${id}`)
          .then(nvRequestUtils.nvSuccess, nvRequestUtils.nvFailure),

      toOptions: data =>
        _(data.vehicleTypes)
          .map(vehicleType => ({
            displayName: vehicleType.name,
            value: vehicleType.name,
          }))
          .sortBy('displayName')
          .value(),

      getAddFields: () =>
        nvModelUtils.pickFields(fields, ['name']),

      getEditFields: model =>
        nvModelUtils.pickFields(fields, ['id', 'name'], { setValues: true, model: model }),

    };
  }
}());
