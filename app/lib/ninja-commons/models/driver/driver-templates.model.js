(function model() {
  angular
        .module('nvCommons.models')
        .factory('DriverTemplate', DriverTemplateFactory);

  DriverTemplateFactory.$inject = ['$http', 'nvRequestUtils'];

  function DriverTemplateFactory($http, nvRequestUtils) {
    return {
      read: function read() {
        return $http.get('driver/1.0/driver-templates')
            .then(nvRequestUtils.nvSuccess, nvRequestUtils.nvFailure);
      },

      update: function update(id, options) {
        return $http.put(`driver/1.0/driver-templates/${id}`, options)
            .then(nvRequestUtils.nvSuccess, nvRequestUtils.nvFailure);
      },
    };
  }
}());
