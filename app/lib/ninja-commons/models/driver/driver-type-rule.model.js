(function() {
    'use strict';

    angular
        .module('nvCommons.models')
        .factory('DriverTypeRule', DriverTypeRule);

    DriverTypeRule.$inject = ['$http', 'nvRequestUtils', 'nvModelUtils', 'nvToast'];

    function DriverTypeRule($http, nvRequestUtils, nvModelUtils, nvToast) {

        var fields = {
            id:         { displayName: 'ID'               },
            name:       { displayName: 'Name'             },
            conditions: {
                _deep:  true,
                0:      { displayName: 'Delivery Type',     defaultValue: '100'    },
                1:      { displayName: 'Priority Level',    defaultValue: '01'     },
                2:      { displayName: 'Reservation Size',  defaultValue: '100000' },
                3:      { displayName: 'Parcel Size',       defaultValue: '1000'   },
                4:      { displayName: 'Timeslot',          defaultValue: '010000' },
            },
        };

        var CONDITIONS = {
            0: ['Normal Delivery', 'C2C + Return Pick Up', 'Reservation Pick Up'],
            1: ['Priority Only', 'Non-Priority Only'],
            2: ['Less than 3 Parcels', 'Less than 10 Parcels', 'Trolley Required', 'Half Van Load', 'Full Van Load', 'Larger than Van Load'],
            3: ['Small', 'Medium', 'Large', 'Extra Large'],
            4: ['9AM to 6PM', '9AM to 10PM', '9AM to 12PM', '12PM to 3PM', '3PM to 6PM', '6PM to 10PM']
        };

        var self = {

            create: function(options) {
                return $http.post('route/rbe/rules/drivertype', reverseConditionStrings(options)).then(
                    successOne(options.isTryToCreate),
                    nvRequestUtils.nvFailure
                );
            },

            read: function(options) {
                options = options || {};
                var id = options.id ? ('/' + options.id) : '';
                return $http.get('route/rbe/rules/drivertype' + id).then(
                    successAll,
                    nvRequestUtils.nvFailure
                );
            },

            update: function(options) {
                options = options || {};
                var id = options.id ? ('/' + options.id) : '';
                return $http.put('route/rbe/rules/drivertype' + id, reverseConditionStrings(options)).then(
                    successOne(false),
                    tryToCreate(options)
                );
            },

            delete: function(id) {
                return $http.delete('route/rbe/rules/drivertype/' + id).then(
                    successOne(false),
                    function(response) {
                        /* jshint -W014 */
                        // if we deleted a rule that doesn't exist, just ignore the error
                        return response.status === 404
                            ? nvRequestUtils.success(response)
                            : nvRequestUtils.nvFailure(response);
                        /* jshint +W014 */
                    }
                );
            },

            getAddFields: function() {
                return nvModelUtils.pickFields(fields, ['name', 'conditions'], { setDefaultValues: true });
            },

            getEditFields: function(model) {
                return nvModelUtils.pickFields(fields, ['id', 'name', 'conditions'], {setValues: true, model: model});
            },

            addConditionStrings: function(models) {
                return _.map(models, function(model) {
                    model = _.defaults(model, {_conditions: {}});
                    if (!_.isUndefined(model.conditions)) {
                        model._conditions = _.mapValues(model.conditions, function(bits, key) {
                            return toString(bits, CONDITIONS[key]);
                        });
                    }
                    return model;
                });
            }

        };

        return self;

        function successOne(isTryToCreate) {
            return function(response) {
                if (isTryToCreate) {
                    nvToast.success('Rule successfully created!');
                }

                return nvRequestUtils.success({
                    data: reverseConditionStrings(response.data)
                });
            };
        }

        function successAll(response) {
            return nvRequestUtils.success({
                data: _.map(response.data, reverseConditionStrings)
            });
        }

        function tryToCreate(options) {
            return function(response) {
                if (response.data.error.code === 0) {
                    nvToast.warning('Attempting to create rule...'); // rule not found
                    options.isTryToCreate = true;
                    return self.create(options);
                }
                return nvRequestUtils.nvFailure(response);
            };
        }

        // we reverse the conditions to ensure compatibility with the convention that
        // str[0] is the left-most char; on the server, condition[0] is the right-most char
        function reverseConditionStrings(rule) {
            return {
                id: rule.id,
                name: rule.name,
                conditions: _.mapValues(rule.conditions, function(condition) {
                    return condition.split('').reverse().join('');
                })
            };
        }

        function toString(bits, conditions) {
            /* jshint -W014 */
            return _.every(bits, function(bit) { return +bit; })
                ? 'All'
                : _.filter(conditions, function(_, i) { return bits[i] === '1'; }).join(', ');
            /* jshint +W014 */
        }

    }

})();
