(function model() {
  angular
    .module('nvCommons.models')
    .factory('Vehicle', VehicleFactory);

  VehicleFactory.$inject = ['$http', 'nvRequestUtils'];

  function VehicleFactory($http, nvRequestUtils) {
    return {
      read: function read() {
        return $http.get('driver/1.0/vehicles?all=true')
          .then(nvRequestUtils.nvSuccess, nvRequestUtils.nvFailure);
      },

      toOptions: function toOptions(data) {
        return _(data.vehicles)
          .map(vehicle => ({
            displayName: [vehicle.vehicleType, vehicle.vehicleNo].join('; '),
            value: vehicle.id,
          }))
          .sortBy('displayName')
          .value();
      },
    };
  }
}());
