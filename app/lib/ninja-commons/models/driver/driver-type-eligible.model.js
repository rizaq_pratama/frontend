(function model() {
  angular
    .module('nvCommons.models')
    .factory('DriverTypeEligible', DriverTypeEligible);

  DriverTypeEligible.$inject = ['$http', 'nvRequestUtils'];

  function DriverTypeEligible($http, nvRequestUtils) {
    const ConditionSet = function ConditionSet(conditionMap) {
      this.conditionMap = conditionMap; // map of condition -> index in conditions array
      this.conditions = Array
        .apply(null, Array(Object.keys(conditionMap).length))
        .map(Number.prototype.valueOf, 0);
    };

    ConditionSet.prototype.set = function set(property) {
      if (typeof this.conditionMap[property] === 'undefined') { return; }
      this.conditions[this.conditionMap[property]] = 1;
      return this;
    };

    ConditionSet.prototype.unset = function unset(property) {
      if (typeof this.conditionMap[property] === 'undefined') { return; }
      this.conditions[this.conditionMap[property]] = 0;
      return this;
    };

    ConditionSet.prototype.setIdx = function setIdx(idx) {
      if (idx < 0 || idx >= this.conditions.length) { return; }
      this.conditions[idx] = 1;
      return this;
    };

    ConditionSet.prototype.unsetIdx = function unsetIdx(idx) {
      if (idx < 0 || idx >= this.conditions.length) { return; }
      this.conditions[idx] = 0;
      return this;
    };

    ConditionSet.prototype.setAll = function setAll() {
      for (let i = 0; i < this.conditions.length; ++i) {
        this.conditions[i] = 1;
      }
      return this;
    };

    ConditionSet.prototype.unsetAll = function unsetAll() {
      for (let i = 0; i < this.conditions.length; ++i) {
        this.conditions[i] = 0;
      }
      return this;
    };

    ConditionSet.prototype.noneSet = function unsetAll() {
      return +this.conditions.join('') === 0;
    };

    ConditionSet.prototype.get = function get() {
      return this.conditions.join('');
    };

    const DVT = { // Delivery Type
      RESERVATION_PICKUP: 0,
      C2C_AND_RETURN_PICKUP: 1,
      NORMAL_DELIVERY: 2,
    };
    const PRL = { // Priority Level
      NON_PRIORITY: 0,
      PRIORITY: 1,
    };
    const RSZ = { // Reservation Size
      'Larger than Van Load': 0,
      'Full-Van Load': 1,
      'Half-Van Load': 2,
      'Trolley Required': 3,
      'Less than 10 Parcels': 4,
      'Less than 3 Parcels': 5,
    };
    const PSZ = { // Parcel Size
      EXTRALARGE: 0,
      LARGE: 1,
      MEDIUM: 2,
      SMALL: 3,
    };
    const TIS = { // Timeslot
      FROM_1800_TO_2200: 0, // timewindowId =  3
      FROM_1500_TO_1800: 1, // timewindowId =  2
      FROM_1200_TO_1500: 2, // timewindowId =  1
      FROM_0900_TO_1200: 3, // timewindowId =  0
      FROM_0900_TO_2200: 4, // timewindowId = -1
      FROM_0900_TO_1800: 5,  // timewindowId = -2
    };

    return {
      getBulkIdsRemote: function getBulkIdsRemote(waypoints, priorities) {
        return $http
          .post('route/rbe/findbulkidmatches/drivertype', {
            conditions: getBulkConditions(waypoints, priorities),
          })
          .then(nvRequestUtils.success, nvRequestUtils.nvFailure);
      },
      getPriorityIds: function getPriorityIds() {
        return $http
          .post('route/rbe/findidmatches/drivertype', {
            conditions: {1: getPriorityLevelConditions(true)}
          })
          .then(nvRequestUtils.success, nvRequestUtils.nvFailure);
      },
    };

    function getConditions(waypoint, priority) {
      let c;
      const conditions = {};

      c = getDeliveryTypeConditions(waypoint);
      if (!!c) { conditions[0] = c; }

      c = getPriorityLevelConditions(priority);
      if (!!c) { conditions[1] = c; }

      c = getReservationSizeConditions(waypoint);
      if (!!c) { conditions[2] = c; }

      c = getParcelSizeConditions(waypoint);
      if (!!c) { conditions[3] = c; }

      c = getTimeslotConditions(waypoint);
      if (!!c) { conditions[4] = c; }

      return conditions;
    }

    function getBulkConditions(waypoints, priorities) {
      const conditions = [];

      for (let i = 0; i < waypoints.length; ++i) {
        conditions.push(getConditions(waypoints[i], priorities[i]));
      }

      return conditions;
    }

    function getDeliveryTypeConditions(waypoint) {
      const conditions = new ConditionSet(DVT);

      if (waypoint.type === 'RESERVATION') {
        return conditions.set('RESERVATION_PICKUP').get();
      }

      if (!waypoint.transactions || waypoint.transactions.length === 0) {
        return conditions.set('NORMAL_DELIVERY').get();
      }

      const types = [];
      for (let i = 0; i < waypoint.transactions.length; ++i) {
        const transaction = waypoint.transactions[i];
        if (!transaction) {
          types.push('NORMAL_DELIVERY');
          continue;
        }

        const order = transaction.order;
        if (!order) {
          types.push('NORMAL_DELIVERY');
          continue;
        }

        if (transaction.type === 'PICKUP' && !transaction.transit) { // PPNT
          if (order.type === 'C2C' || order.type === 'RETURN') {
            types.push('C2C_AND_RETURN_PICKUP');
            continue;
          }
        }

        types.push('NORMAL_DELIVERY');
      }

      if (types.length === 1) {
        return conditions.set(types[0]).get();
      }

      if (same(types)) {
        return conditions.set(types[0]).get();
      }

      // if there are multiple types, default to C2C_AND_RETURN_PICKUP -- todo?
      return conditions.set('C2C_AND_RETURN_PICKUP').get();

      function same(arr) {
        for (let i = 1; i < arr.length; ++i) {
          if (arr[0] !== arr[i]) { return false; }
        }
        return true;
      }
    }

    function getPriorityLevelConditions(priority) {
      const conditions = new ConditionSet(PRL);
      return priority ? conditions.set('PRIORITY').get() : conditions.set('NON_PRIORITY').get();
    }

    function getReservationSizeConditions(waypoint) {
      if (waypoint.type !== 'RESERVATION') { return null; }
      const conditions = new ConditionSet(RSZ);

      if (!!waypoint.reservations) {
        for (let i = 0; i < waypoint.reservations.length; ++i) {
          conditions.set(waypoint.reservations[i].approx_volume);
        }
      } else if (!!waypoint.reservation) {
        conditions.set(waypoint.reservation.approx_volume);
      }

      return conditions.noneSet() ? null : conditions.get();
    }

    function getParcelSizeConditions(waypoint) {
      if (waypoint.type !== 'TRANSACTION') { return null; }
      if (!waypoint.transactions || waypoint.transactions.length === 0) { return null; }
      const conditions = new ConditionSet(PSZ);

      for (let i = 0; i < waypoint.transactions.length; ++i) {
        const transaction = waypoint.transactions[i];
        if (!transaction) { continue; }

        const order = transaction.order;
        if (!order) { continue; }

        conditions.set(order.parcelSize);
      }

      return conditions.noneSet() ? null : conditions.get();
    }

    function getTimeslotConditions(waypoint) {
      if (isNaN(waypoint.timewindowId)) { return null; }
      const conditions = new ConditionSet(TIS);
      const idx = 3 - waypoint.timewindowId;
      return (idx >= 0 && idx <= 6) ? conditions.setIdx(idx % 6).get() : null;
    }
  }
}());
