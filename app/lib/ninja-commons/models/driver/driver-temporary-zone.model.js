(function model() {
  angular
    .module('nvCommons.models')
    .factory('DriverTemporaryZone', DriverTemporaryZone);

  DriverTemporaryZone.$inject = ['$http', 'nvRequestUtils'];

  function DriverTemporaryZone($http, nvRequestUtils) {
    return {
      read: function read() {
        return $http.get('driver/1.0/drivers/temporary-zones')
          .then(nvRequestUtils.nvSuccess, nvRequestUtils.nvFailure);
      },
      update: function update(data) {
        return $http.put('driver/1.0/drivers/temporary-zones', data)
          .then(nvRequestUtils.nvSuccess, nvRequestUtils.nvFailure);
      },

      delete: function update() {
        return $http.delete('driver/1.0/drivers/temporary-zones')
          .then(nvRequestUtils.nvSuccess, nvRequestUtils.nvFailure);
      },
    };
  }
}());
