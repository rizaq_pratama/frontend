(function model() {
  angular
    .module('nvCommons.models')
    .factory('Events', Events);

  Events.$inject = ['$http', 'nvRequestUtils', 'nvTranslate'];

  function Events($http, nvRequestUtils, nvTranslate) {
    const FIELDS = {
      order_id: { displayName: 'commons.order-id' },
      user_id: { displayName: 'commons.id' },
      user_type: { displayName: 'commons.user-type' },
      user_name: { displayName: 'commons.user-id' },
      name: { displayName: 'commons.event-name' },
      event_type: { displayName: 'commons.event-type' },
    };
    const TAG = {
      SCAN: 'SCAN',
      PICKUP: 'PICKUP',
      ROUTING: 'ROUTING',
      SORT: 'SORT',
      DELIVERY: 'DELIVERY',
      COD: 'COD',
      DP: 'DP',
      MANUAL_UPDATE: 'MANUAL_UPDATE',
      RTS: 'RTS',
      NOTIFICATION: 'NOTIFICATION',
    };

    const CATEGORY = {
      DP: 1,
      ADDRESS: 2,
      DRIVER: 3,
      ORDER: 4,
      ROUTE: 5,
      RESERVATION: 6,
      RESCHEDULE: 7,
      UPDATE: 8,
      NOTIFICATION: 9,
      SCAN: 10,
      MISCELLANEOUS: 11,
    };

    const SUBCATEGORY = {
      DP: {
        TAG_DP: 1,
        HANDOFF_TO_CUSTOMER_DP: 2,
        RECEIVE_FROM_DRIVER_DP: 3,
        HANDOFF_TO_DRIVER_DP: 4,
        RECEIVE_FROM_CUSTOMER_DP: 5,
      },
      ADDRESS: {
        VERIFY_ADDRESS: 1,
      },
      DRIVER: {
        START_ROUTE: 1,
        DELIVERY_SUCCESS: 2,
        DELIVERY_FAILURE: 3,
        ROUTE_INBOUND_SCAN: 4,
      },
      RESERVATION: {
        PICKUP_SUCCESS: 1,
        PICKUP_FAILURE: 2,
      },
      ROUTE: {
        ADD_WAYPOINT_TO_ROUTE: 1,
        PULL_OUT_OF_ROUTE: 2,
      },
      SCAN: {
        UNKNOWN: 0,
        HUB: 1,
        DP_HUB: 2,
        VAN: 3,
        TRUCK: 4,
        SHIPPER: 5,
      },
      MISCELLANEOUS: {
        DEFAULT: 0,
        RESUME: 1,
        CANCEL: 2,
        FORCE_SUCCESS: 3,
      },
      RESCHEDULE: {
        DEFAULT: 0,
        PICKUP: 1,
        DELIVERY: 2,
      },
    };

    const SCAN_TYPE = {
      UNKNOWN: 0,
      INBOUND: 1,
      SWEEP: 2,
    };

    const SCAN_RESULT = {
      UNKNOWN: {
        value: 0,
        name: 'commons.unknown',
      },
      SUCCESS: {
        value: 1,
        name: 'commons.success',
      },
      CMI: {
        value: 2,
        name: 'commons.cmi',
      },
      DUPLICATE: {
        value: 3,
        name: 'commons.duplicate',
      },
      COMPLETED: {
        value: 4,
        name: 'commons.completed',
      },
      NOT_MATCHED: {
        value: 5,
        name: 'commons.not-matched',
      },
    };

    const MESSAGE = {
      'DP.TAG_DP': {
        withValue: {
          languageKey: 'commons.model.events.dp.tag_dp',
          variablesNeed: ['dp_name', 'dp_id'],
        },
        default: {
          languageKey: 'commons.model.events.dp.tag_dp.default',
          variablesNeed: [],
        },
      },
      'DP.HANDOFF_TO_CUSTOMER_DP': {
        withValue: {
          languageKey: 'commons.model.events.dp.handoff_to_customer_dp',
          variablesNeed: ['to_name'],
        },
        default: {
          languageKey: 'commons.model.events.dp.handoff_to_customer_dp.default',
          variablesNeed: [],
        },
      },
      'DP.RECEIVE_FROM_DRIVER_DP': {
        withValue: {
          languageKey: 'commons.model.events.dp.receive_from_driver_dp',
          variablesNeed: ['driver_name', 'driver_id', 'route_id'],
        },
        default: {
          languageKey: 'commons.model.events.dp.receive_from_driver_dp.default',
          variablesNeed: [],
        },
      },
      'DP.HANDOFF_TO_DRIVER_DP': {
        withValue: {
          languageKey: 'commons.model.events.dp.handoff_to_driver_dp',
          variablesNeed: ['driver_name', 'driver_id', 'route_id'],
        },
        default: {
          languageKey: 'commons.model.events.dp.handoff_to_driver_dp.default',
          variablesNeed: [],
        },
      },
      'DP.RECEIVE_FROM_CUSTOMER_DP': {
        withValue: {
          languageKey: 'commons.model.events.dp.receive_from_customer_dp',
          variablesNeed: ['from_name'],
        },
        default: {
          languageKey: 'commons.model.events.dp.receive_from_customer_dp.default',
          variablesNeed: [],
        },
      },
      'ADDRESS.VERIFY_ADDRESS': {
        withValue: {
          languageKey: 'commons.model.events.address.verify_address',
          variablesNeed: ['formatted_address', 'latitude', 'longitude', 'jaro_score'],
        },
        default: {
          languageKey: 'commons.model.events.address.verify_address.default',
          variablesNeed: [],
        },
      },
      'DRIVER.DELIVERY_SUCCESS': {
        withValue: {
          languageKey: 'commons.model.events.driver.delivery_success',
          variablesNeed: ['driver_name', 'driver_id', 'cod'],
        },
        withValueNoCod: {
          languageKey: 'commons.model.events.driver.delivery_success.no-cod',
          variablesNeed: ['driver_name', 'driver_id'],
        },
        withValueNoDriver: {
          languageKey: 'commons.model.events.driver.delivery_success.no-driver',
          variablesNeed: ['cod'],
        },
        default: {
          languageKey: 'commons.model.events.driver.delivery_success.default',
          variablesNeed: [],
        },
      },
      'DRIVER.DELIVERY_FAILURE': {
        withValue: {
          languageKey: 'commons.model.events.driver.delivery_failure',
          variablesNeed: ['failure_reason', 'latitude', 'longitude'],
        },
        withValueNoReason: {
          languageKey: 'commons.model.events.driver.delivery_failure.no-reason',
          variablesNeed: ['latitude', 'longitude'],
        },
        withValueNoLatLong: {
          languageKey: 'commons.model.events.driver.delivery_failure.no-latlng',
          variablesNeed: ['failure_reason'],
        },
        default: {
          languageKey: 'commons.model.events.driver.delivery_failure.default',
          variablesNeed: [],
        },
      },
      'RESERVATION.PICKUP_SUCCESS': {
        withValue: {
          languageKey: 'commons.model.events.reservation.pickup_success',
          variablesNeed: ['driver_name', 'driver_id', 'reservation_id'],
        },
        withValueNoReservationId: {
          languageKey: 'commons.model.events.reservation.pickup_success.no-reservation-id',
          variablesNeed: ['driver_name', 'driver_id'],
        },
        withValueNoDriver: {
          languageKey: 'commons.model.events.reservation.pickup_success.no-driver',
          variablesNeed: ['reservation_id'],
        },
        default: {
          languageKey: 'commons.model.events.reservation.pickup_success.default',
          variablesNeed: [],
        },
      },
      'RESERVATION.PICKUP_FAILURE': {
        withValue: {
          languageKey: 'commons.model.events.reservation.pickup_failure',
          variablesNeed: ['driver_name', 'driver_id', 'reservation_id', 'failure_reason'],
        },
        withValueNoReason: {
          languageKey: 'commons.model.events.reservation.pickup_failure.no-reason',
          variablesNeed: ['driver_name', 'driver_id', 'reservation_id'],
        },
        withValueNoDriver: {
          languageKey: 'commons.model.events.reservation.pickup_failure.no-driver',
          variablesNeed: ['reservation_id', 'failure_reason'],
        },
        default: {
          languageKey: 'commons.model.events.reservation.pickup_failure.default',
          variablesNeed: [],
        },
      },
      'ROUTE.ADD_WAYPOINT_TO_ROUTE': {
        withValue: {
          languageKey: 'commons.model.events.route.add_waypoint_to_route',
          variablesNeed: ['route_id', 'waypoint_id'],
        },
        default: {
          languageKey: 'commons.model.events.route.add_waypoint_to_route.default',
          variablesNeed: [],
        },
      },
      'ROUTE.PULL_OUT_OF_ROUTE': {
        withValue: {
          languageKey: 'commons.model.events.route.pull_out_of_route',
          variablesNeed: ['route_id', 'waypoint_id'],
        },
        default: {
          languageKey: 'commons.model.events.route.pull_out_of_route.default',
          variablesNeed: [],
        },
      },
      'RESCHEDULE.PICKUP': {
        withValue: {
          languageKey: 'commons.model.events.reschedule.pickup',
          variablesNeed: ['_oldDate',  '_newDate',  'old_time_window',  'new_time_window',  'formatted_address'],
        },
        default: {
          languageKey: 'commons.model.events.reschedule.default',
          variablesNeed: [],
        },
      },
      'RESCHEDULE.DELIVERY': {
        withValue: {
          languageKey: 'commons.model.events.reschedule.delivery',
          variablesNeed: ['_oldDate', '_newDate', 'old_time_window', 'new_time_window', 'formatted_address'],
        },
        default: {
          languageKey: 'commons.model.events.reschedule.default',
          variablesNeed: [],
        },
      },
      'SCAN.VAN.INBOUND': {
        withValue: {
          languageKey: 'commons.model.events.scan.scan-result',
          variablesNeed: ['resultName'],
        },
        default: {
          languageKey: 'commons.model.events.scan.van.inbound.default',
          variablesNeed: [],
        },
      },
      'SCAN.TRUCK.INBOUND': {
        withValue: {
          languageKey: 'commons.model.events.scan.scan-result',
          variablesNeed: ['resultName'],
        },
        default: {
          languageKey: 'commons.model.events.scan.truck.inbound.default',
          variablesNeed: [],
        },
      },
      'SCAN.HUB.INBOUND': {
        withValue: {
          languageKey: 'commons.model.events.scan.scan-result',
          variablesNeed: ['resultName'],
        },
        default: {
          languageKey: 'commons.model.events.scan.hub.inbound.default',
          variablesNeed: [],
        },
      },
      'SCAN.HUB.SWEEP': {
        withValue: {
          languageKey: 'commons.model.events.scan.scan-result',
          variablesNeed: ['resultName'],
        },
        default: {
          languageKey: 'commons.model.events.scan.hub.sweep.default',
          variablesNeed: [],
        },
      },
    };

    const self = {
      FIELDS: FIELDS,
      TAG: TAG,

      getOrderEvents: function getOrderEvents(orderId) {
        return $http.get(`events/order/${orderId}`).then(
          nvRequestUtils.success,
          nvRequestUtils.nvFailure
        );
      },
      getOrderEventsV2: function getOrderEventsV2(orderId) {
        return $http.get(`events/1.0/orders/${orderId}/events`).then(
            nvRequestUtils.success,
            nvRequestUtils.nvFailure
        );
      },
      getCategoryEnum: function getCategoryEnum(categoryValue) {
        let categoryEnum = null;
        _.forEach(CATEGORY, (value, key) => {
          if (value === categoryValue) {
            categoryEnum = key;
          }
        });

        return categoryEnum;
      },
      getSubCategoryEnum: function getSubCategoryEnum(categoryEnum, subCategoryValue) {
        let subCategoryEnum = null;
        _.forEach(SUBCATEGORY[categoryEnum], (value, key) => {
          if (value === subCategoryValue) {
            subCategoryEnum = key;
          }
        });

        return subCategoryEnum;
      },
      getScanTypeEnum: function getScanTypeEnum(scanTypeValue) {
        let scanTypeEnum = null;
        _.forEach(SCAN_TYPE, (value, key) => {
          if (value === scanTypeValue) {
            scanTypeEnum = key;
          }
        });

        return scanTypeEnum;
      },
      getScanResultEnum: function getScanResultEnum(scanResultValue) {
        let scanResultEnum = null;
        _.forEach(SCAN_RESULT, (row, key) => {
          if (row.value === scanResultValue) {
            scanResultEnum = key;
          }
        });

        return scanResultEnum;
      },
      getMessage: function getMessage(categoryValue, subCategoryValue, data) {
        let languageKey = null;

        const categoryEnum = self.getCategoryEnum(categoryValue);
        if (categoryEnum && !_.isUndefined(SUBCATEGORY[categoryEnum])) {
          const subCategoryEnum = self.getSubCategoryEnum(categoryEnum, subCategoryValue);

          if (subCategoryEnum) {
            if (categoryEnum === self.getCategoryEnum(CATEGORY.SCAN)) {
              const scanTypeEnum = self.getScanTypeEnum(data.type);
              if (scanTypeEnum) {
                const messageKey = `${categoryEnum}.${subCategoryEnum}.${scanTypeEnum}`;
                languageKey = getLanguageKey(messageKey, data);
              }
            } else {
              const messageKey = `${categoryEnum}.${subCategoryEnum}`;
              languageKey = getLanguageKey(messageKey, data);
            }
          }
        }

        return languageKey;

        function getLanguageKey(theMessageKey, theData) {
          let theLanguageKey = null;

          if (!_.isUndefined(MESSAGE[theMessageKey])) {
            theLanguageKey = MESSAGE[theMessageKey].default;

            // extend data into theData
            const scanResultEnum = self.getScanResultEnum(theData.result);
            if (scanResultEnum) {
              theData.resultName = nvTranslate.instant(SCAN_RESULT[scanResultEnum].name);
            }

            if (theData.old_date) {
              const oldDate = moment(theData.old_date);
              if (oldDate.isValid()) {
                theData._oldDate = oldDate.format("D MMM YYYY");
              }
            }

            if (theData.new_date) {
              const newDate = moment(theData.new_date);
              if (newDate.isValid()) {
                theData._newDate = newDate.format("D MMM YYYY");
              }
            }

            _.forEach(MESSAGE[theMessageKey], (message) => {
              const keyDifference = _.difference(message.variablesNeed, _.keys(theData));
              if (_.size(keyDifference) <= 0) {
                theLanguageKey = message.languageKey;
                return false;
              }

              return undefined;
            });
          }

          return theLanguageKey;
        }
      },
    };

    return self;
  }
}());
