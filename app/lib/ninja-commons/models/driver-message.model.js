(function driverMessageModel() {
  angular
    .module('nvCommons.models')
    .factory('DriverMessage', DriverMessage);

  DriverMessage.$inject = ['$http', 'nvRequestUtils'];

  function DriverMessage($http, nvRequestUtils) {
    return {
      notifyMany: (ids, from, message, priority) => {
        const data = {
          driverIds: ids,
          fromName: from,
          message: message,
          priority: priority,
        };

        return $http.post('driver/2.0/drivers/messages', data)
          .then(nvRequestUtils.nvSuccess, nvRequestUtils.nvFailure);
      },
    };
  }
}());
