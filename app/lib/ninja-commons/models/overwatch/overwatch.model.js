(function Overwatch() {
  angular
    .module('nvCommons.models')
    .factory('Overwatch', model);

  model.$inject = ['$http', 'nvRequestUtils'];

  function model($http, nvRequestUtils) {
    return {
      getETA: (lat, long) =>
        $http
          .get(`overwatch/1.0/estimated-arrival-time?latitude=${lat}&longitude=${long}`)
          .then(nvRequestUtils.success, nvRequestUtils.nvFailure),
    };
  }
}());
