(function dpUserModel() {
  angular
    .module('nvCommons.models')
    .factory('DistributionPointUser', DistributionPointUser);

  DistributionPointUser.$inject = ['$http', 'nvRequestUtils', 'nvModelUtils'];

  function DistributionPointUser($http, nvRequestUtils, nvModelUtils) {
    const fields = {
      id:         { displayName: 'User ID',     type: 'number'  },
      first_name: { displayName: 'First Name',  type: 'text'    },
      last_name:  { displayName: 'Last Name',   type: 'text'    },
      contact_no: { displayName: 'Contact No.', type: 'tel'     },
      email:      { displayName: 'Email',       type: 'email'   },
      username:   { displayName: 'Username',    type: 'text'    },
      password:   { displayName: 'Password',    type: 'password', overrideValue: '' },
      created_at: { displayName: 'Created On',  type: 'date'    },
      updated_at: { displayName: 'Updated On',  type: 'date'    },
      deleted_at: { displayName: 'Deleted On',  type: 'date'    },
    };

    return {

      create: function create(options) {
        const data = _.omit(options, 'partnerId', 'dpId');
        return $http
          .post(`dp/1.0/partners/${options.partnerId}/dps/${options.dpId}/users`, data)
          .then(nvRequestUtils.success, nvRequestUtils.nvFailure);
      },

      search: function search(partnerId, dpId) {
        return $http
          .get(`dp/1.0/partners/${partnerId}/dps/${dpId}/users`)
          .then(nvRequestUtils.success, nvRequestUtils.nvFailure)
          .then(data => data.users);
      },

      update: function update(options) {
        const data = _.omit(options, 'partnerId', 'dpId', 'id', 'username');
        return $http
          .put(`dp/1.0/partners/${options.partnerId}/dps/${options.dpId}/users/${options.id}`, data)
          .then(nvRequestUtils.success, nvRequestUtils.nvFailure);
      },

      getAddFields: function getAddFields() {
        return nvModelUtils.pickFields(fields, [
          'first_name', 'last_name', 'contact_no', 'email', 'username', 'password',
        ]);
      },

      getEditFields: function getEditFields(model) {
        return nvModelUtils.pickFields(fields, [
          'id', 'first_name', 'last_name', 'contact_no', 'email', 'username',
        ], { setValues: true, model: model });
      },

    };
  }
}());
