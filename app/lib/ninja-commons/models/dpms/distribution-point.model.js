(function DistributionPointModel() {
  angular
        .module('nvCommons.models')
        .factory('DistributionPoint', DistributionPoint);

  DistributionPoint.$inject = ['$http', 'nvRequestUtils', 'nvModelUtils', 'nvUtils', 'nvTimezone', 'nvToast',
    '$q', 'nvExtendUtils', 'nvDateTimeUtils'];

  function DistributionPoint($http, nvRequestUtils, nvModelUtils, nvUtils, nvTimezone, nvToast,
    $q, nvExtendUtils, nvDateTimeUtils) {
    const typeOptions = [
      { displayName: 'Ninja Box', value: 'BOX' },
      { displayName: 'Ninja Point', value: 'SHOP' },
    ];

    const serviceOptions = [
      { displayName: 'Lodge In & Pick Up', value: 'BOTH', code: 2 },
      { displayName: 'Lodge In Only', value: 'LODGE_IN', code: 3 },
      { displayName: 'Pick Up Only', value: 'PICK_UP', code: 4 },
    ];

    const driverCollectionModeOptions = [
      { displayName: 'Confirmation Code', value: 'CONFIRMATION_CODE' },
      { displayName: 'Signature', value: 'SIGNATURE' },
    ];

    const displayNameForMPSD = 'Max Parcel Stay Duration (Working Days)';

    const fields = {
      id: { displayName: 'ID', type: 'number' },
      dpms_id: { displayName: 'DPMS ID', type: 'number' },
      type: { displayName: 'Type', type: 'select', options: typeOptions },
      service_type: { displayName: 'Service', type: 'select', options: serviceOptions },
      name: { displayName: 'Name', type: 'text' },
      short_name: { displayName: 'Shortname', type: 'text' },
      contact: { displayName: 'Contact No.', type: 'tel' },
      address_1: { displayName: 'Address Line 1', type: 'text' },
      address_2: { displayName: 'Address Line 2', type: 'text' },
      city: { displayName: 'City', type: 'text' },
      country: { displayName: 'Country', type: 'text' },
      postal_code: { displayName: 'Postcode', type: 'text' },
      latitude: { displayName: 'Latitude', type: 'text' },
      longitude: { displayName: 'Longitude', type: 'text' },
      directions: { displayName: 'Directions', type: 'text' },
      is_active: { displayName: 'Is Active?', type: 'switch', defaultValue: false },
      max_parcel_stay_duration: { displayName: displayNameForMPSD, type: 'number', defaultValue: 2 },
      shipper_id: { displayName: 'Shipper Account', type: 'number' },
      ninja_dp: { displayName: 'Is Ninja Warehouse?', type: 'switch', defaultValue: false },
      hub: { displayName: 'Hub', options: [], selectedOptions: [] },
      createdAt: { displayName: 'Created On', type: 'date' },
      updatedAt: { displayName: 'Updated On', type: 'date' },
      deletedAt: { displayName: 'Deleted On', type: 'date' },
      can_shipper_lodge_in: { displayName: 'Can Shipper Lodge In?', type: 'switch', defaultValue: true },
      can_customer_collect: { displayName: 'Can Customer Collect?', type: 'switch', defaultValue: true },
      is_ninja_warehouse: { displayName: 'Is Ninja Warehouse?', type: 'switch', defaultValue: false },
      hub_id: { displayName: 'Hub', options: [], selectedOptions: [] },
      driver_collection_mode: { displayName: 'Driver Collection Mode', type: 'select', options: driverCollectionModeOptions },
      distance: { displayName: 'Distance', type: 'number' },
      custom: {
        _deep: true,
        operatingHours: { displayName: 'Operating Hours' },
      },
      external_store_id: { displayName: 'External Store ID', type: 'text' },
      is_public: { displayName: 'Public', type: 'switch', defaultValue: false },
      floor_number: { displayName: 'Floor No.', type: 'text' },
      unit_number: { displayName: 'Unit No.', type: 'text' },
      actual_max_capacity: { displayName: 'Max Cap', type: 'number' },
      computed_max_capacity: { displayName: 'Cap Buffer', type: 'number' },
    };

    const DP_DAY = {
      1: 'Monday',
      2: 'Tuesday',
      3: 'Wednesday',
      4: 'Thursday',
      5: 'Friday',
      6: 'Saturday',
      7: 'Sunday',
    };

    return {
      listDPs: function listDPs(options) {
        return $http.get(`dp/1.1/dps?${nvRequestUtils.queryParam(options || {})}`)
          .then(nvRequestUtils.nvSuccess, nvRequestUtils.nvFailure)
          .then((data) => {
            // to support the legacy dpms_id being used by other entities, we map the response:
            // 1. id -> global_id
            // 2. dpms_id -> id
            return _.map(data || [], dp => _.defaults({ global_id: dp.id, id: dp.dpms_id }, dp));
          });
      },

      createV2: (partnerId, payload) => $http.post(`dp/1.0/partner/${partnerId}/dps`, payload)
        .then(nvRequestUtils.success, nvRequestUtils.nvFailure),

      readAllV2: partnerId => $http.get(`dp/1.0/partner/${partnerId}/dps`)
        .then(nvRequestUtils.success, nvRequestUtils.nvFailure),

      updateV2: (partnerId, dpId, payload) => $http.put(`dp/1.0/partner/${partnerId}/dps/${dpId}`, payload)
        .then(nvRequestUtils.success, nvRequestUtils.nvFailure),

      getCapacity: (options) => {
        const opt = options.all
          ? { all: true, include_today: true }
          : _.defaults(options, { latitude: 0, longitude: 0, max_distance: 500 });
        return $http.get(`dp/1.0/available-slots?${nvRequestUtils.queryParam(opt)}`)
          .then(nvRequestUtils.success, nvRequestUtils.nvFailure);
      },

      tagOrder: (payload, options, bulkAction = false) => {
        // avoid display error toast if bulk action
        const opt = _.defaults(options, {
          callback: true,
          mode: 'DROP_OFF',
          failover: true,
        });
        return $http.post(`dp/1.0/reservations?${nvRequestUtils.queryParam(opt)}`, payload)
          .then(nvRequestUtils.success, (response) => {
            if (bulkAction) {
              return $q.reject(response.data);
            }
            return nvRequestUtils.nvFailure(response);
          });
      },

      unTagOrder: (orderId, options, bulkAction = false) => {
        const opt = _.defaults(options, {
          callback: true,
        });
        return $http.delete(`dp/1.0/reservations?order_id=${orderId}&${nvRequestUtils.queryParam(opt)}`)
          .then(nvRequestUtils.success, (response) => {
            if (bulkAction) {
              return $q.reject(response.data);
            }
            return nvRequestUtils.nvFailure(response);
          });
      },

      setOptions: (props, options) => {
        nvUtils.getDeep(fields, props).options = options;
      },

      toOptions: function toOptions(dps) {
        return _(dps)
          .map(dp => ({ displayName: dp.name, value: dp.id }))
          .sortBy('displayName')
          .value();
      },

      getAddFields: function getAddFields() {
        return nvModelUtils.pickFields(fields, [
          'shipper_id', 'hub_id', 'type', 'can_shipper_lodge_in', 'can_customer_collect',
          'driver_collection_mode', 'max_parcel_stay_duration', 'name', 'short_name',
          'address_1', 'address_2', 'city', 'postal_code', 'latitude', 'longitude', 'directions', 'distance',
          'contact', 'is_active', 'is_ninja_warehouse',
          'external_store_id', 'is_public', 'floor_number', 'unit_number', 'actual_max_capacity', 'computed_max_capacity',
        ], { setDefaultValues: true });
      },

      getEditFields: function getEditFields(model) {
        return nvModelUtils.pickFields(fields, [
          'id', 'dpms_id',
          'shipper_id', 'hub_id', 'type', 'can_shipper_lodge_in', 'can_customer_collect',
          'driver_collection_mode', 'max_parcel_stay_duration', 'name', 'short_name',
          'address_1', 'address_2', 'city', 'postal_code', 'latitude', 'longitude', 'directions', 'distance',
          'contact', 'is_active', 'is_ninja_warehouse',
          'custom',
          'external_store_id', 'is_public', 'floor_number', 'unit_number', 'actual_max_capacity', 'computed_max_capacity',
        ], { setValues: true, model: model });
      },

      defaultOpenHour: function defaultOpenHour(timezone = nvTimezone.getOperatorTimezone()) {
        return moment().tz(timezone)
            .hour(8)
            .minute(0)
            .second(0)
            .millisecond(0);
      },

      defaultCloseHour: function defaultCloseHour(timezone = nvTimezone.getOperatorTimezone()) {
        return moment().tz(timezone)
            .hour(21)
            .minute(0)
            .second(0)
            .millisecond(0);
      },

      resolveUpload: function resolveUpload(file) {
        return $http.upload('core/orders/dpms/uploadbycsv', file)
          .then(nvRequestUtils.success, nvRequestUtils.nvFailure);
      },

      resolveAssign: function resolveAssign(payload) {
        return $http.post('core/orders/dpms/resolve', payload)
          .then(nvRequestUtils.success, nvRequestUtils.nvFailure);
      },

      SERVICE_TYPE: serviceOptions,
      extendDps: extendDps,
      buildOptions: extendedDps => _(extendedDps)
        .map(dp => ({ value: dp.id, legacyValue: dp.dpms_id, displayName: `${dp.dpms_id} - ${dp.name}` }))
        .value(),
      buildDateOptions: extendedDps => _(extendedDps)
        .map(dp => ({ id: dp.id, options: slotsToOptions(dp.slots) }))
        .keyBy('id')
        .value(),
    };

    function extendDps(dps) {
      // available flag indicate the dp still have a drop_off capacity
      return _(dps)
        .map(dp => _.assign(dp, {
          opening_hours: extendTimeAndDay(dp.opening_hours),
          slots: extendTimeAndDay(dp.slots) }))
        .sortBy('id')
        .value();

      function extendTimeAndDay(datas) {
        return _.map(
          nvExtendUtils.addMoment(datas, ['end_time', 'start_time', 'date'], nvTimezone.getOperatorTimezone()),
          data => _.assign(data, {
            _dayName: DP_DAY[data.day_of_week],
          })
        );
      }
    }

    function slotsToOptions(slots) {
      /* initial condition:
      dps with at least 1 (max of 3) slot
      available will be listed, while dps
      with no slots will have nearest date
      option listed as WAITING
      */
      return _.every(slots, slot => slot.drop_off <= 0) ? _(slots)
        .take(1)
        .map(slot => ({
          value: slot._momentDate,
          displayName: `${nvDateTimeUtils.displayDate(
            slot._momentDate, nvTimezone.getOperatorTimezone()
          )} (WAITING)`,
        }))
        .value() : _(slots)
        .filter(slot => slot.drop_off > 0)
        .take(3)
        .map(slot => ({
          value: slot._momentDate,
          displayName: nvDateTimeUtils.displayDate(
            slot._momentDate, nvTimezone.getOperatorTimezone()
          ),
        }))
        .value();
    }
  }
}());
