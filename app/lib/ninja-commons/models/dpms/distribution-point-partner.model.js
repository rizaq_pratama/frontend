(function dpPartner() {
  angular
      .module('nvCommons.models')
      .factory('DistributionPointPartner', DistributionPointPartner);

  DistributionPointPartner.$inject = ['$http', 'nvRequestUtils', 'nvModelUtils'];

  function DistributionPointPartner($http, nvRequestUtils, nvModelUtils) {
    const fields = {
      id: { displayName: 'Partner ID', type: 'number' },
      dpms_partner_id: { displayName: 'Partner ID', type: 'number' },
      name: { displayName: 'Partner Name', type: 'text' },
      poc_name: { displayName: 'POC Name', type: 'text' },
      poc_tel: { displayName: 'POC No.', type: 'tel' },
      poc_email: { displayName: 'POC Email', type: 'email' },
      restrictions: { displayName: 'Restrictions', type: 'textarea' },
      created_at: { displayName: 'Created On', type: 'date' },
      updated_at: { displayName: 'Updated On', type: 'date' },
      deleted_at: { displayName: 'Deleted On', type: 'date' },
      send_notifications_to_customer: { displayName: 'Send Notifications', type: 'switch' },
    };

    return {
      createV2: payload => $http.post('dp/1.0/partner', payload)
        .then(nvRequestUtils.success, nvRequestUtils.nvFailure),

      readAllV2: () => $http.get('dp/1.0/partners')
        .then(nvRequestUtils.success, nvRequestUtils.nvFailure),

      updateV2: (id, payload) => $http.put(`dp/1.0/partner/${id}`, payload)
        .then(nvRequestUtils.success, nvRequestUtils.nvFailure),

      getAddFields: () => nvModelUtils.pickFields(fields, [
        'name', 'poc_name', 'poc_tel', 'poc_email', 'restrictions', 'send_notifications_to_customer']),
      getEditFields: model => nvModelUtils.pickFields(fields, [
        'id', 'dpms_partner_id',
        'name', 'poc_name', 'poc_tel', 'poc_email', 'restrictions', 'send_notifications_to_customer'], { setValues: true, model: model }),
    };
  }
}());
