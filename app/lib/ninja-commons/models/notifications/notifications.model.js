(function Overwatch() {
  angular
    .module('nvCommons.models')
    .factory('Notifications', Notifications);

  Notifications.$inject = ['$http', 'nvRequestUtils'];

  function Notifications($http, nvRequestUtils) {
    return {
      getSmsSettings: () => $http.get('core/notifications/sms/settings')
        .then(nvRequestUtils.success, nvRequestUtils.nvFailure),
      updateSmsSettings: options => $http.put('core/notifications/sms/settings', options)
        .then(nvRequestUtils.success, nvRequestUtils.nvFailure),
    };
  }
}());
