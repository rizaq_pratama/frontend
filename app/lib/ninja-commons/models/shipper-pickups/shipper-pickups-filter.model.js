(function model() {
  angular
    .module('nvCommons.models')
    .factory('ShipperPickupsFilter', ShipperPickupsFilter);

  ShipperPickupsFilter.$inject = ['nvBackendFilterUtils', 'nvTimezone', 'nvDateTimeUtils',
    'Reservation', 'Hub', 'Shippers', 'Zone', 'Waypoint'];

  function ShipperPickupsFilter(nvBackendFilterUtils, nvTimezone, nvDateTimeUtils,
    Reservation, Hub, Shippers, Zone, Waypoint) {
    return {
      init: init,
      getSelectedOptions: getSelectedOptions,
    };

    function init() {
      const filters = [
        {
          type: nvBackendFilterUtils.FILTER_DATE,
          fromModel: moment().toDate(),
          toModel: moment().toDate(),
          showOnInit: true,
          isMandatory: true,
          transformFn: transformDate,
          mainTitle: 'Reservation Date',
          maxDateRange: 7,
          key: {
            from: 'from_datetime',
            to: 'to_datetime',
          },
          presetKey: {
            from: 'from_datetime',
            to: 'to_datetime',
          },
        },
        _.defaults({
          type: nvBackendFilterUtils.FILTER_OPTIONS,
          showOnInit: true,
          mainTitle: 'Reservation Types',
          key: 'types',
        }, getReservationTypes()),
        _.defaults({
          type: nvBackendFilterUtils.FILTER_OPTIONS,
          showOnInit: true,
          mainTitle: 'Waypoint Status',
          key: 'waypoint_status',
        }, getWaypointsOptions()),
        {
          type: nvBackendFilterUtils.FILTER_OPTIONS,
          possibleOptions: getHubs(),
          showOnInit: true,
          selectedOptions: [],
          mainTitle: 'Hubs',
          key: 'hub_ids',
        },
        {
          type: nvBackendFilterUtils.FILTER_AUTOCOMPLETE,
          callback: getShippers,
          backendKey: 'legacy_ids',
          showOnInit: true,
          selectedOptions: [],
          mainTitle: 'Shipper',
          key: 'shipper_ids',
        },
        {
          type: nvBackendFilterUtils.FILTER_OPTIONS,
          possibleOptions: getMasterShippers(),
          showOnInit: true,
          transformFn: transformMasterShippers,
          selectedOptions: [],
          mainTitle: 'Master Shipper',
          name: 'masterShippers',
          key: 'shipper_ids',
        },
        {
          type: nvBackendFilterUtils.FILTER_OPTIONS,
          possibleOptions: getZones(),
          showOnInit: true,
          selectedOptions: [],
          mainTitle: 'Zones',
          key: 'zone_ids',
        },
      ];

      return filters;
    }

    function getSelectedOptions(filters, filterName) {
      const filter = _.find(filters, ['name', filterName]);
      return (filter && filter.selectedOptions) || [];
    }

    function transformDate(date, type) {
      if (type === 'from') {
        const startDate = nvDateTimeUtils.toSOD(date, nvTimezone.getOperatorTimezone());
        return nvDateTimeUtils.displayDateTime(startDate, 'utc');
      }
      // type === 'to'
      const endDate = nvDateTimeUtils.toEOD(date, nvTimezone.getOperatorTimezone());
      return nvDateTimeUtils.displayDateTime(endDate, 'utc');
    }

    function transformMasterShippers(masterShipper) {
      return masterShipper.id;
    }

    function getHubs() {
      return Hub.read({ active_only: false }).then(hubs => _(hubs)
        .map(hub => _.defaults(hub, { displayName: hub.name }))
        .value());
    }

    function getShippers(text) {
      return Shippers.filterSearch(
        text,
        _.isUndefined(this.getSelectedOptions) ? [] : this.getSelectedOptions()
      );
    }

    function getMasterShippers() {
      return Shippers.readAll({ is_marketplace: true }).then(response =>
        _(response)
          .map(shipper => _.defaults(shipper, { displayName: `${shipper.id}-${shipper.name}` }))
          .value()
      );
    }

    function getZones() {
      return Zone.read().then(response => Zone.toFilterOptionsWithIdName(response));
    }

    function getReservationTypes() {
      const selectedIds = [0];
      const possibleTypes = angular.copy(Reservation.typeOptions);
      const selectedTypes = _.remove(possibleTypes, type =>
        _.find(selectedIds, id => type.id === id) != null);
      return {
        possibleOptions: possibleTypes,
        selectedOptions: selectedTypes,
      };
    }

    function getWaypointsOptions() {
      const options = Waypoint.statusAsOptions();
      const selectedOptions = _.remove(options, opt =>
        opt.value === Waypoint.UPDATE_STATUS_ACTION.ROUTED
        || opt.value === Waypoint.UPDATE_STATUS_ACTION.PENDING
      );
      return {
        possibleOptions: options,
        selectedOptions: selectedOptions,
      };
    }
  }
}());

