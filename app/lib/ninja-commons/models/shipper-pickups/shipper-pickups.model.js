(function model() {
  angular
    .module('nvCommons.models')
    .factory('ShipperPickups', ShipperPickups);

  ShipperPickups.$inject = ['$http', 'nvRequestUtils'];

  function ShipperPickups($http, nvRequestUtils) {
    let migratedBaseUrl = 'core';
    if (nv.config.env === 'prod') {
      migratedBaseUrl = 'core-kafka';
    }
    return {
      search: options => $http
        .post(`${migratedBaseUrl}/pickups/search`, options)
        .then(nvRequestUtils.success, nvRequestUtils.nvFailure),

      getPickupReservations: reservationId => $http
        .get(`${migratedBaseUrl}/pickups/${reservationId}`)
        .then(nvRequestUtils.success, nvRequestUtils.nvFailure),

      getPod: podId => $http
        .get(`${migratedBaseUrl}/pickups/${podId}/pods`)
        .then(nvRequestUtils.success, nvRequestUtils.nvFailure),

      updatePod: (reservationId, podId, payload) =>
        $http.post(`core/reservations/${reservationId}/pods/${podId}`, payload)
          .then(nvRequestUtils.success, nvRequestUtils.nvFailure),

      updatePodManifest: (reservationId, podId, payload) =>
        $http.put(`core/pickups/${reservationId}/pods/${podId}`, payload)
          .then(nvRequestUtils.success, nvRequestUtils.nvFailure),

      rerouteReservation: (reservationId, payload) =>
        $http.put(`core/2.0/reservations/${reservationId}/route`, payload)
          .then(nvRequestUtils.success, nvRequestUtils.nvFailure),

      suggestRoute: payload =>
        $http.post('core/reservations/route-suggestions', payload)
          .then(nvRequestUtils.success, nvRequestUtils.nvFailure),

      removeRoute: (routeId, wpId, payload) =>
        $http.put(`core/admin/routes/${routeId}/waypoints/${wpId}/pods`, payload)
          .then(nvRequestUtils.success, nvRequestUtils.nvFailure),
    };
  }
}());

