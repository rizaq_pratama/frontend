(function model() {
  angular
    .module('nvCommons.models')
    .factory('Claim', Claim);

  Claim.$inject = ['$http', 'nvRequestUtils'];

  function Claim($http, nvRequestUtils) {

    return {
      create: function create(data) {
        return $http.post('claim/claimed_items', data)
          .then(nvRequestUtils.success, nvRequestUtils.nvFailure);
      },

    };
  }
}());
