 (function AddressingModel() {
   angular.module('nvCommons.models')
        .factory('Addressing', Addressing);

   Addressing.$inject = ['$http', 'nvRequestUtils'];

   function Addressing($http, nvRequestUtils) {
     const countries = [
       {
         key: 'sg',
         name: 'Singapore',
       },
       {
         key: 'my',
         name: 'Malaysia',
       },
       {
         key: 'id',
         name: 'Indonesia',
       },
       {
         key: 'vn',
         name: 'Vietnam',
       },
       {
         key: 'ph',
         name: 'Philippines',
       },
       {
         key: 'th',
         name: 'Thailand',
       },
       {
         key: 'mm',
         name: 'Myanmar',
       },
     ];
     const fields = {
       building_no: {
         displayName: 'Building No',
       },
       building: {
         displayName: 'Building',
       },
       street: {
         displayName: 'Street',
       },
       area: {
         displayName: 'Area',
       },
       community: {
         displayName: 'Community',
       },
       ward: {
         displayName: 'Ward',
       },
       subdistrict: {
         displayName: 'Sub District',
       },
       subdivision: {
         displayName: 'Sub Division',
       },
       district: {
         displayName: 'District',
       },
       city: {
         displayName: 'City',
       },
       state: {
         displayName: 'State',
       },
       province: {
         displayName: 'Province',
       },
       postcode: {
         displayName: 'Post Code',
       },
       country: {
         displayName: 'Country',
       },
       latitude: {
         displayName: 'Latitude',
       },
       longitude: {
         displayName: 'Longitude',
       },
       address_type: {
         displayName: 'Address Type',
       },
       source: {
         displayName: 'Source',
       },
     };
     const fieldName = [
       'building_no', 'building', 'street', 'area',
       'community', 'ward', 'subdistrict', 'subdivision',
       'township', 'district', 'city', 'state',
       'province', 'postcode', 'country', 'latitude',
       'longitude', 'address_type', 'source',
     ];
     return {
       getCountries: function getCountries() {
         return countries;
       },
       createAddress: function createAddress(address) {
         return $http.post('addressing/addresses', address)
           .then(nvRequestUtils.success, nvRequestUtils.nvFailure);
       },
       updateAddress: function updateAddress(address, id) {
         return $http.put(`addressing/addresses/${id}`, address)
           .then(nvRequestUtils.success, nvRequestUtils.nvFailure);
       },
       deleteAddress: function deleteAddress(country, id) {
         return $http.delete(`addressing/addresses/${country}/${id}`)
           .then(nvRequestUtils.success, nvRequestUtils.nvFailure);
       },
       searchAddress: function searchAddress(country, query) {
         return $http.get(`addressing/2.0/search/${query}&country=${country}`)
           .then(nvRequestUtils.success, nvRequestUtils.nvFailure);
       },
       filterSearch: function filterSearch(country, query) {
         return $http.get(`addressing/2.0/filter-search?query=${query}&country=${country}`)
           .then(nvRequestUtils.success, nvRequestUtils.nvFailure);
       },
       getAddressFieldNamebyCountry: function getAddressFieldNamebyCountry(country = 'SG') {
         switch (country) {
           case 'sg':
             return _.pick(fieldName, [0, 1, 2, 10, 13, 14, 15, 16, 17, 18]);
           case 'my':
             return _.pick(fieldName, [0, 1, 2, 3, 10, 11, 13, 14, 15, 16, 17, 18]);
           case 'id':
             return _.pick(fieldName, [0, 1, 2, 4, 9, 10, 12, 13, 14, 15, 16, 17, 18]);
           case 'vn':
             return _.pick(fieldName, [0, 1, 2, 5, 9, 10, 13, 14, 15, 16, 17, 18]);
           case 'th':
             return _.pick(fieldName, [0, 1, 2, 6, 9, 12, 13, 14, 15, 16, 17, 18]);
           case 'ph':
             return _.pick(fieldName, [0, 1, 2, 7, 9, 10, 12, 13, 14, 15, 16, 17, 18]);
           case 'mm':
             return _.pick(fieldName, [0, 1, 2, 8, 9, 11, 13, 14, 15, 16, 17, 18]);
           default:
             return fieldName;
         }
       },
     };
   }
 }());
