 (function UnverifiedAddressModel() {
   angular.module('nvCommons.models')
        .factory('UnverifiedAddress', UnverifiedAddress);

   UnverifiedAddress.$inject = [
     'Order',
     '$http',
     'Zone',
     'Shippers',
     'nvBackendFilterUtils',
     'nvDateTimeUtils',
     'nvTimezone',
     'nvRequestUtils',
   ];

   function UnverifiedAddress(Order, $http, Zone, Shippers,
    nvBackendFilterUtils, nvDateTimeUtils, nvTimezone, nvRequestUtils) {
     return {

       getUnverifiedAddressFilter: function getUnverifiedAddressFilter(result = {}) {
         const filter = [
           {
             type: nvBackendFilterUtils.FILTER_OPTIONS,
             possibleOptions: result.zones || getZones,
             selectedOptions: [],
             searchText: {},
             mainTitle: 'Zone',
             searchBy: 'name',
             key: 'zone_ids',
             presetKey: 'zone_ids',
           },
           {
             type: nvBackendFilterUtils.FILTER_AUTOCOMPLETE,
             callback: getShippers,
             backendKey: 'legacy_ids',
             selectedOptions: [],
             mainTitle: 'Shipper',
             key: 'shipper_ids',
             presetKey: 'shipper_ids',
           },
           {
             type: nvBackendFilterUtils.FILTER_DATE,
             fromModel: moment().toDate(),
             transformFn: transformDate,
             isRange: false,
             mainTitle: 'Due Date',
             key: {
               from: 'due_date',
             },
             presetKey: {
               from: 'due_date',
             },
           },
           {
             type: nvBackendFilterUtils.FILTER_RANGE,
             fromModel: 0,
             mainTitle: 'Jaro Score',
             unit: 'Point',
             key: {
               from: 'jaroscore_from',
               to: 'jaroscore_to',
             },
             presetKey: {
               from: 'jaroscore_from',
               to: 'jaroscore_to',
             },
           },
           {
             type: nvBackendFilterUtils.FILTER_OPTIONS,
             possibleOptions: getMasterShippers,
             selectedOptions: [],
             searchText: {},
             mainTitle: 'Master Shipper',
             key: 'shipper_ids',
             presetKey: 'master_shipper_ids',
           },
           {
             type: nvBackendFilterUtils.FILTER_OPTIONS,
             possibleOptions: getDeliveryType,
             selectedOptions: [],
             searchText: {},
             mainTitle: 'Delivery Type',
             key: 'delivery_types',
             presetKey: 'delivery_types',
           },

         ];
         return filter;
       },

       preAddressVerification: function preAddressVerification(zoneId, waypointIds) {
         const payload = {
           waypoint_ids: waypointIds,
         };
         return $http.post(`core/pre-address-verification/assign/zones/${zoneId}`, payload).then(
           nvRequestUtils.success,
           nvRequestUtils.nvFailure
         );
       },
       search: function search(param) {
         return $http.post('core/pre-address-verification/search', param).then(
           nvRequestUtils.success,
           nvRequestUtils.nvFailure
         );
       },
     };

     function getZones() {
       return Zone.read();
     }

     function getShippers(text) {
       return Shippers.filterSearch(
         text,
         _.isUndefined(this.getSelectedOptions) ? [] : this.getSelectedOptions()
       );
     }

     function getMasterShippers() {
       return Shippers.readAll({ is_marketplace: true }).then(response => (_(response)
                    .map(shipper => _.defaults(shipper, { displayName: `${shipper.id}-${shipper.name}` }))
                    .value()));
     }

     function getDeliveryType() {
       return Order.getDeliveryType().then(types => (_(types)
                        .map(type => _.defaults(type, { displayName: `${type.name}` }))
                        .value()));
     }

     function transformDate(date, type) {
       if (type === 'from') {
         const startDate = nvDateTimeUtils.toSOD(date, nvTimezone.getOperatorTimezone());
         return nvDateTimeUtils.displayDate(startDate, 'utc');
       }
       return null;
     }
   }
 }());