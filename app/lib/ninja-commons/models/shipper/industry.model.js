(function model() {
  angular
    .module('nvCommons.models')
    .factory('Industry', Industry);

  Industry.$inject = ['$http', 'nvRequestUtils'];

  function Industry($http, nvRequestUtils) {
    return {
      read: function read() {
        return $http.get('core/industries').then(
          nvRequestUtils.success,
          nvRequestUtils.nvFailure
        );
      },
    };
  }
}());
