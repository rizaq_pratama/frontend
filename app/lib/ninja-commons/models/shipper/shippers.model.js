(function model() {
  angular
        .module('nvCommons.models')
        .factory('Shippers', Shippers);

  Shippers.$inject = [
    '$http', 'nvRequestUtils', '$q', 'nvURL', '$window',
    '$rootScope', 'nvExtendUtils', 'nvDomain',
  ];

  function Shippers(
    $http, nvRequestUtils, $q, nvURL, $window,
    $rootScope, nvExtendUtils, nvDomain
  ) {
    const fields = {
      id: { displayName: 'commons.id' },
      name: { displayName: 'commons.name' },
      email: { displayName: 'commons.email' },
      liaison_email: { displayName: 'commons.liaison-email' },
      contact: { displayName: 'commons.contact' },
      sales_person: { displayName: 'commons.salesperson' },
    };

    const possibleNamespaces = {
      orderCreate: { namespace: 'order_create' },
      reservation: { namespace: 'reservation' },
      shopify: { namespace: 'shopify' },
      returns: { namespace: 'returns' },
      labelPrinter: { namespace: 'label_printer' },
      qoo10: { namespace: 'qoo10' },
      magento: { namespace: 'magento' },
      distributionPoints: { namespace: 'distribution_points' },
      marketplaceBilling: { namespace: 'marketplace_billing' },
      marketplaceDefault: { namespace: 'marketplace_default' },
      notification: { namespace: 'notification' },
      pickup: { namespace: 'pickup' },
      delivery: { namespace: 'delivery' },
    };

    const SERVICE_TYPE = {
      PARCEL: {
        id: 'Parcel',
        displayName: 'container.shippers.service-type-parcel',
      },
      RETURN: {
        id: 'Return',
        displayName: 'container.shippers.service-type-return',
      },
      MARKETPLACE: {
        id: 'Marketplace',
        displayName: 'container.shippers.service-type-marketplace',
      },
      NINJA_PACK: {
        id: 'Ninja Pack',
        displayName: 'container.shippers.service-type-ninja-pack',
      },
      BULKY: {
        id: 'Bulky',
        displayName: 'container.shippers.service-type-bulky',
      },
      INTERNATIONAL: {
        id: 'International',
        displayName: 'container.shippers.service-type-international',
      },
      MARKETPLACE_INTERNATIONAL: {
        id: 'Marketplace International',
        displayName: 'container.shippers.service-type-marketplace-international',
      },
    };

    const SERVICE_LEVEL = {
      SAMEDAY: {
        id: 'SAMEDAY',
        displayName: 'commons.model.sameday',
        isLegacy: false,
      },
      NEXTDAY: {
        id: 'NEXTDAY',
        displayName: 'commons.model.nextday',
        isLegacy: false,
      },
      EXPRESS: {
        id: 'EXPRESS',
        displayName: 'commons.model.express',
        isLegacy: false,
      },
      STANDARD: {
        id: 'STANDARD',
        displayName: 'commons.model.standard',
        isLegacy: false,
      },
      ONEDAY_SG: {
        id: '1-DAY',
        displayName: 'commons.model.oneday',
        isLegacy: true,
      },
      TWODAY_SG: {
        id: '2-DAY',
        displayName: 'commons.model.twoday',
        isLegacy: true,
      },
      THREEDAY_SG: {
        id: '3-DAY',
        displayName: 'commons.model.threeday',
        isLegacy: true,
      },
    };

    const OC_SETTINGS_SERVICES_AVAILABLE = [
      {
        displayName: '1DAY',
        value: '1DAY',
        serviceLevelId: SERVICE_LEVEL.NEXTDAY.id,
      },
      {
        displayName: '2DAY',
        value: '2DAY',
        serviceLevelId: SERVICE_LEVEL.EXPRESS.id,
      },
      {
        displayName: '3DAY',
        value: '3DAY',
        serviceLevelId: SERVICE_LEVEL.STANDARD.id,
      },
      {
        displayName: 'SAMEDAY',
        value: 'SAMEDAY',
        serviceLevelId: SERVICE_LEVEL.SAMEDAY.id,
      },
      {
        displayName: 'FREIGHT',
        value: 'FREIGHT',
        serviceLevelId: null,
      },
    ];

    const ORDER_CREATE_TYPE = {
      V2: {
        id: 'v2',
        displayName: 'v2',
      },
      V3: {
        id: 'v3',
        displayName: 'v3',
      },
      V4: {
        id: 'v4',
        displayName: 'v4',
      },
    };

    const TRACKING_TYPE = {
      DYNAMIC: {
        id: 'Dynamic',
        displayName: 'commons.model.tracking-type-dynamic',
      },
      PREFIXLESS: {
        id: 'Prefixless',
        displayName: 'commons.model.tracking-type-prefixless',
      },
      FIXED: {
        id: 'Fixed',
        displayName: 'commons.model.tracking-type-fixed',
      },
      NINJA_FIXED: {
        id: 'NinjaFixed',
        displayName: 'commons.model.tracking-type-ninja-fixed',
      },
      LEGACY_DYNAMIC: {
        id: 'LegacyDynamic',
        displayName: 'commons.model.tracking-type-legacy-dynamic',
      },
      MULTI_DYNAMIC: {
        id: 'MultiDynamic',
        displayName: 'commons.model.tracking-type-multi-dynamic',
      },
      MULTI_FIXED: {
        id: 'MultiFixed',
        displayName: 'commons.model.tracking-type-multi-fixed',
      },
    };

    const PICKUP_SERVICE_TYPE = {
      SCHEDULED: {
        id: 'Scheduled',
        displayName: 'commons.model.scheduled',
      },
      ON_DEMAND: {
        id: 'On-Demand',
        displayName: 'commons.on-demand',
      },
    };

    const PICKUP_SERVICE_LEVEL = {
      PREMIUM: {
        id: 'Premium',
        displayName: 'commons.model.premium',
      },
      STANDARD: {
        id: 'Standard',
        displayName: 'commons.model.standard',
      },
    };

    const SHIPPER_CLASSIFICATION = {
      B2C_MARKETPLACE: {
        id: 1,
        displayName: 'B2C Marketplace Platform',
      },
      C2C_MARKETPLACE: {
        id: 2,
        displayName: 'C2C Marketplace Platform',
      },
      ONLINE_STORE: {
        id: 3,
        displayName: 'Brand Online Store',
      },
      SOCIAL_COMMERCE: {
        id: 4,
        displayName: 'Social Commerce',
      },
      TELEVISION_TELEPHONE_SALES: {
        id: 5,
        displayName: 'Television / Telephone Sales',
      },
      BRICK_MORTAR: {
        id: 6,
        displayName: 'Brick & Mortar',
      },
      CROSS_BORDER_FORWARDING: {
        id: 7,
        displayName: 'Cross Border Forwarding / Consolidation',
      },
      B2B_DISTRIBUTION: {
        id: 8,
        displayName: 'B2B Distribution',
      },
      OTHERS: {
        id: 9,
        displayName: 'Others',
      },
    };

    const SHIPPER_ACCOUNT_TYPE = {
      TEST: { id: 0, displayName: 'Test' },
      CROSS_BORDER: { id: 3, displayName: 'Cross Border' },
      OTHERS: { id: 5, displayName: 'Others' },
      B2C: { id: 6, displayName: 'B2C' },
      MARKETPLACE: { id: 7, displayName: 'Marketplace' },
      C2C: { id: 8, displayName: 'C2C' },
      B2B: { id: 9, displayName: 'B2B' },
    };

    const SUPPORTED_PICKUP_COUNTRY = {
      SG: { displayName: 'Singapore', value: 'SG' },
      MY: { displayName: 'Malaysia', value: 'MY' },
      ID: { displayName: 'Indonesia', value: 'ID' },
      PH: { displayName: 'Philipine', value: 'PH' },
      TH: { displayName: 'Thailand', value: 'TH' },
      VN: { displayName: 'Vietnam', value: 'VN' },
      MM: { displayName: 'Myanmar', value: 'MM' },
    };

    const SEARCH_BY = {
      id: 'legacy_ids',
      global_id: 'ids',
    };
    const SHIPPERS_MAP = {
      legacy_ids: {}, // map by legacy id
      ids: {}, // map by global id
    };
    const ADDRESS_MAP = {};

    const self = {
      FIELDS: fields,
      OC_SERVICES: OC_SETTINGS_SERVICES_AVAILABLE,
      SERVICE_TYPE: SERVICE_TYPE,
      SERVICE_LEVEL: SERVICE_LEVEL,
      ORDER_CREATE_TYPE: ORDER_CREATE_TYPE,
      TRACKING_TYPE: TRACKING_TYPE,
      PICKUP_SERVICE_TYPE: PICKUP_SERVICE_TYPE,
      PICKUP_SERVICE_LEVEL: PICKUP_SERVICE_LEVEL,
      SHIPPER_CLASSIFICATION: SHIPPER_CLASSIFICATION,
      SHIPPER_ACCOUNT_TYPE: SHIPPER_ACCOUNT_TYPE,
      SUPPORTED_PICKUP_COUNTRY: SUPPORTED_PICKUP_COUNTRY,
      SEARCH_BY: SEARCH_BY,
      namespaces: possibleNamespaces,

      elasticRead: (options) => {
        const data = _.defaults(options, { from: 0, size: 200000 });
        return $http.post('shipper-search/shippers/list', data)
          .then(response =>
            $q.resolve(elasticRemapAllShipper(response.data)), nvRequestUtils.nvFailure
        );
      },

      elasticReadAll: (options) => {
        const deferred = $q.defer();
        elasticGetAllShipper(deferred, _.defaults(options, {}));
        return deferred.promise;
      },

      elasticCompactSearch: (options) => {
        const data = _.defaults(options, { from: 0, size: 100 });
        return $http.post('shipper-search/shippers/search', data)
          .then(response =>
            $q.resolve(elasticRemapAllShipper(response.data)), nvRequestUtils.nvFailure
        );
      },

      filterSearch: (textOrObject, selectedOptions = [], fieldToCheck = 'id') => {
        let filter = {};
        if (_.isString(textOrObject)) {
          filter.keyword = textOrObject;
        } else if (_.isObject(textOrObject)) {
          filter = textOrObject;
          let size = 100;
          _.forEach(filter, (list) => {
            size = _.size(list);
            return false;
          });

          filter.size = size;
        }

        const data = _.assign({
          from: 0,
          size: 100 + _.size(selectedOptions),
        }, filter);
        return self.elasticCompactSearch(data).then(response =>
          _(response.details)
            .map(shipper => _.defaults(shipper, { displayName: `${shipper.id}-${shipper.name}` }))
            .filter(shipper => !_.includes(
              _.map(selectedOptions, fieldToCheck), shipper[fieldToCheck])
            )
            .value()
        );
      },

      getSearchByEnum: function getSearchByEnum(searchByValue) {
        let searchByEnum = null;
        _.forEach(SEARCH_BY, (value, key) => {
          if (_.toLower(value) === _.toLower(searchByValue)) {
            searchByEnum = key;
          }
        });

        return searchByEnum;
      },

      searchByIds: (ids, searchBy = SEARCH_BY.id) => {
        const resultShippers = [];
        const idsToSearch = [];
        _.forEach(_.uniq(_.compact(_.castArray(ids))), (id) => {
          if (SHIPPERS_MAP[searchBy][id]) {
            resultShippers.push(SHIPPERS_MAP[searchBy][id]);
          } else {
            idsToSearch.push(id);
          }
        });

        if (_.size(idsToSearch) <= 0) {
          return $q.when(resultShippers);
        }

        const deferred = $q.defer();
        const params = {
          size: _.size(idsToSearch),
        };
        params[searchBy] = idsToSearch;
        self.elasticCompactSearch(params)
          .then(success, failure);

        return deferred.promise;

        function success(response) {
          const shippers = _.get(response, 'details');
          const notFound = [];

          _.forEach(idsToSearch, (id) => {
            const theShipper = _.find(shippers, [self.getSearchByEnum(searchBy), id]);

            if (theShipper) {
              SHIPPERS_MAP[SEARCH_BY.id][theShipper.id] = theShipper;
              SHIPPERS_MAP[SEARCH_BY.global_id][theShipper.global_id] = theShipper;
              resultShippers.push(theShipper);
            } else {
              notFound.push(id);
            }

            return _.noop();
          });

          deferred.resolve(resultShippers);
        }

        function failure(response) {
          deferred.reject(response);
        }
      },

      readCount: options =>
        $http.get(`shipper/2.0/shippers-count?${nvRequestUtils.queryParam(options)}`)
          .then(nvRequestUtils.success, nvRequestUtils.nvFailure),

      read: (options) => {
        const param = _.defaults(options, { start: 0, limit: 100 });
        return $http.get(`shipper/2.0/shippers?${nvRequestUtils.queryParam(param)}`)
          .then(response => $q.resolve(remapAllShipper(response.data)), nvRequestUtils.nvFailure);
      },

      readAll: (options) => {
        const deferred = $q.defer();
        getAllShipper(deferred, _.defaults(options, {}));
        return deferred.promise;
      },

      readShipper: id =>
        $http.get(`shipper/2.0/shippers/${id}`)
          .then(response =>
            $q.resolve(remapSingleShipper(response.data)), nvRequestUtils.nvFailure),

      deleteOne: function deleteOne(shipper) {
        return $http.delete(`shipper/shippers/${shipper.id}`).then(
          nvRequestUtils.success,
          nvRequestUtils.nvFailure
        );
      },

      toOptions: function toOptions(data) {
        return _(data)
          .map(shipper => ({
            displayName: `${shipper.id} - ${shipper.name}`,
            value: shipper.id,
          }))
          .sortBy('displayName')
          .value();
      },

      toFilterOptions: function toFilterOptions(shippers) {
        return _.map(shippers, shipper => ({
          id: shipper.id,
          displayName: `${shipper.id} - ${shipper.name}`,
          lowercaseName: shipper.name ? shipper.name.toLowerCase() : '',
        }));
      },

      orderCreateTypeToOptions: function orderCreateTypeToOptions() {
        return _(ORDER_CREATE_TYPE)
          .map(orderCreateType => ({
            displayName: orderCreateType.displayName,
            value: orderCreateType.id,
          }))
          .sortBy('displayName')
          .value();
      },

      trackingTypeToOptions: function trackingTypeToOptions() {
        return _(TRACKING_TYPE)
          .map(trackingType => ({
            displayName: trackingType.displayName,
            value: trackingType.id,
          }))
          .sortBy('displayName')
          .value();
      },

      serviceLevelToOptions: (withLegacy = false) =>
        _(SERVICE_LEVEL)
          .filter(serviceLevel => withLegacy || !serviceLevel.isLegacy)
          .map(serviceLevel => ({
            displayName: serviceLevel.displayName,
            value: serviceLevel.id,
          }))
          .value(),

      pickupServiceTypeToOptions: function pickupServiceTypeToOptions() {
        return _(PICKUP_SERVICE_TYPE)
          .map(serviceType => ({
            displayName: serviceType.displayName,
            value: serviceType.id,
          }))
          .sortBy('displayName')
          .value();
      },

      pickupServiceLevelToOptions: function pickupServiceLevelToOptions() {
        return _(PICKUP_SERVICE_LEVEL)
          .map(serviceLevel => ({
            displayName: serviceLevel.displayName,
            value: serviceLevel.id,
          }))
          .sortBy('displayName')
          .value();
      },

      pickupCountryToOptions: () =>
        _(SUPPORTED_PICKUP_COUNTRY)
          .map(el => ({
            displayName: el.displayName,
            value: el.value,
          }))
          .sortBy('displayName')
          .value(),

      updateShipperV2: (id, shipper) =>
        $http.put(`shipper/2.0/shippers/${id}`, shipper)
          .then(nvRequestUtils.success, nvRequestUtils.nvFailure),

      readShipperLegacy: id =>
        $http.get(`shipper/legacy/shippers/${id}`)
          .then(nvRequestUtils.success, nvRequestUtils.nvFailure),

      readMarketplaces: id =>
        $http.get(`shipper/2.0/shippers/${id}/marketplaces`)
          .then(nvRequestUtils.success, nvRequestUtils.nvFailure),

      readSellers: (id, options) => {
        const deferred = $q.defer();
        getAllSellers(deferred, id, _.defaults(options, {}));
        return deferred.promise;
      },
      readSellersByIds: ids => $q
        .all(_.map(ids, id => self.elasticReadSellers(id)))
        .then(responses => _(responses).flatMap().compact().value()),

      elasticReadSellers: (id) => {
        const deferred = $q.defer();
        elasticGetAllSellers(deferred, id);
        return deferred.promise;
      },

      readSettingsV2: (id, options) =>
        $http.get(`shipper/2.0/shippers/${id}/settings?${nvRequestUtils.queryParam(options)}`)
          .then(nvRequestUtils.success, nvRequestUtils.nvFailure),

      readSpecificSettingV2: (id, namespace, key) => {
        if (key) {
          return $http.get(`shipper/2.0/shippers/${id}/settings/${namespace}/${key}`)
            .then(nvRequestUtils.success, nvRequestUtils.nvFailure);
        }
        return $http.get(`shipper/2.0/shippers/${id}/settings/${namespace}`)
          .then(nvRequestUtils.success, nvRequestUtils.nvFailure);
      },

      updateSettingsV2: (id, options) => {
        const deferred = $q.defer();
        const calls = [];
        _.forIn(options, (val, key) => {
          calls.push(self.updateSpecificSettingV2(id, val, key));
        });
        $q.all(calls).then(() => {
          deferred.resolve(0);
        }, (responses) => {
          deferred.reject(responses);
        });
        return deferred.promise;
      },
      updateSpecificSettingV2: (id, payload, namespace, key) => {
        if (key) {
          return $http.post(`shipper/2.0/shippers/${id}/settings/${namespace}/${key}`, payload)
            .then(nvRequestUtils.success, nvRequestUtils.nvFailure);
        }
        return $http.post(`shipper/2.0/shippers/${id}/settings/${namespace}`, payload)
          .then(nvRequestUtils.success, nvRequestUtils.nvFailure);
      },

      readMultipleSettingsV2: (id, namespaces) => {
        const deferred = $q.defer();
        const calls = _.map(namespaces, namespace => self.readSpecificSettingV2(id, namespace));
        const settings = {};

        $q.all(calls).then((responses) => {
          for (let i = 0; i < responses.length; i++) {
            const field = namespaces[i];
            settings[field] = responses[i];
          }
          deferred.resolve(settings);
        }, () => {
          deferred.reject(-1);
        });

        return deferred.promise;
      },

      readMilkrunSettings: (shipperId, addressId, options = null) => {
        return $http.get(`shipper/2.0/shippers/${shipperId}/addresses/${addressId}/milkrun?${nvRequestUtils.queryParam(options)}`)
          .then(nvRequestUtils.success, nvRequestUtils.nvFailure);
      },

      create: shipper => ($http.post('shipper/2.0/shippers', shipper)
      .then(nvRequestUtils.success, nvRequestUtils.nvFailure)),

      /**
       * create sd account
       * payload is:
       * {
       *    "name": "Test",
       *    "email": "aab2@aaa.com",
       *    "password": "Ninjitsu89",
       *    "shipper_id": 0,
       *    "language_code": "en
       * }
       */
      createSdAccount: shipper => ($http.post('shipperpanel/app/accounts/create', shipper)
      .then(nvRequestUtils.success, $q.reject)),

      /**
       * check for SD account availability.
       * return 200 if available,
       * return 400 if not available
       */
      checkSdAccount: email => ($http.get('shipperpanel/app/accounts/validate_email',
        {
          params: { email: email },
        }))
      .then(nvRequestUtils.success, nvRequestUtils.failure),

      loginToSd: (legacyId, email, issuer = null) => {
        // Header
        const oHeader = { alg: 'RS256', typ: 'JWT' };

        // Payload
        const tNow = moment().unix();
        const tEnd = moment().add(1, 'hour').unix();
        const oPayload = {
          sub: 'Shipper Login',
          nbf: tNow,
          iat: tNow,
          exp: tEnd,
          jti: _.toString(`shipper_${legacyId}`),
          aud: 'SHIPPER_DASHBOARD',
          system_id: $rootScope.domain,
          shipper_id: legacyId,
          email: email,
        };

        if (issuer) {
          oPayload.iss = issuer;
        }

        const sHeader = JSON.stringify(oHeader);
        const sPayload = JSON.stringify(oPayload);

        const privateKey = KEYUTIL.getKey(`-----BEGIN RSA PRIVATE KEY-----
          MIIJKAIBAAKCAgEAwHbFSXlOC8KISimcNKF1RHvgWzD6Ay2LsUCNPhTH67mkEoT+
          k5/4lRuqYeLf+nyU6esM7WBN3S3HNSuRbOknmkTatRvgUJRGC4F7adLJdWPLoqls
          ZVDmRoZ4IOvalSdYy4CjD4w8MZzw5o9ERloQ3S3b5VN987v0pLUncfm3o2CSR46i
          SQtLSot6Mc7rpIr+Ydge34rJIFA6of0JuHeu5SOSPHRZsUr/ItBBYjMnDcYzCdZL
          XAIkkHFBm/fchGr9nX53q//fqG2v0ZA3NBYtOo0qDF+J+oTGw6DQmPrR84s/byL1
          L2Ku4IMEl4zPvXnbhe9ld/iRX0zkeywL7cBNI6lIFg14Tqo7LlrPmk3usWBoBBCD
          od0ReYhyp89T4EjazEeNC/rdWwgnpw5zlqMtKIHUvgp88VPHHifxDoY2XHoopyBH
          T4vwPVTIrGcWO140ywVSaXu7zQtDFE1JEGF7WyrRb6w/tBL6Zg/NyvCGQwn8G0P4
          rE2U93iZsmD28Rwn8KkT6/gKoZTQ0QvIF3G7iAlBlJZWWf0J07dWJA4Mr4UC4LEs
          aGDXBXv5IDapqI3fPZ7DTd/zjiY1JsPzORdu87xMRxfdUitEtIQ1zXAxcjtlhdCF
          OJqKOSlxS3cqFNTts1SqvtoEkxXct0nt+zxb/+dAFiC2o1wj6JtGxvaDZ9MCAwEA
          AQKCAgAAvvUYyc2RohlhIDQoyA9qSoieEdyGMqD9+OATQ4Va1odaQwWxkMcn3MmW
          Q4jWsbDqDy0npl3m44yIGjkgEO9EW8v1M7x0035KASB3UofRWqepY7q10dByqYbB
          qehY96QSddcr8kExeAg+/IMpOqQiF1Fo3B/619mk4cbbXDpn+bHXcSgT3GVYTp4U
          n1audpNx0aKc/y60X1s60X4+JsiYvsCMV28Zjs7aVbBLLyORnBkKirgUDTYTET1N
          23IRWVDOUXUpR36YaoRp7YMpQoS+W1m0XpJsVCNpErB8R0yVAw8l4R7OJjXDTP6x
          j6wmcdh19m2QP3hgxC2GhK8vJrWl0gNRS7YTxnqCmEKk104CF/nwC7dQMNnfB6p/
          3lyXTCJ+w7YFI33Mf6a5A7tictzm7UUHrcKgkqFAIQSrc6azfM/2+2K2qtzmD3A0
          +objc9+W537WliyarrMjMCswHAfpgq+VqSZVqROmRwB3I7qZI5tUBrKdXdNS010H
          Y14gzNHScnoqphJGxXbS4IrEDXNHFRCoeFlufig29OmGfwCpzNJ3TMdo6q+FX47i
          /9rb/Zqi4WPT3elNFsDI90p5s3xF8jQtLf+tF2cH1d0z9wesMTdWab5UxLyFHyPz
          McDQJc2KKEGT8pRHH4N0NttRQ+D+j86Ec1igoWFCW9XH3TTl4QKCAQEA5vv/fa0o
          iTG8H1EOWSB819Z0Rh1lJdkANpp4JZsEWVgSAPKAl0dcvSlAqJeZDoU3gu57oZrb
          ZN9WWBEKOyAxjkJVSxX1BCMrFLgo6hpmoFZjz6hkVPgcwR+hKyn4j7QGIkX1GNvY
          kAe1Qdf40wAE8DdhtvDM9wvlBj9+rnhCpv9lbXsE+op4vh8TiMTLqmlNIPAeLaBe
          x12ouUaZQS/dkMQbxM10A0q+GrBbSTDJro+QGbeaUrE/ucYulJdAJNJoL943TDlo
          6hI+7l3guQpjV6pjwzKOosCiwtMhXRPDnG3Yvk2Nxf9451Wih34qA3zcbb9H8ers
          x3A4A63ej/aqqQKCAQEA1U7M9TaARwxlXIN74c7Pwv5jywufUl7knurMBfu477g/
          G3tzsUFXikRHtBI70chnwHRr8aVHD1XhvvGvTvbIo+J+1X/qE1MyKZ82JnDD2Qn3
          QACdU+0n65Rx0RzMAiKAxu/qt62foeEzK/OpmsPbH1g+kGqUT8rvfJPmsvt9H96z
          PwKaJuYdAW7gay8LDowmZ5JvWK8V1DQdwx4/r3k9Tq+HMjTuONLFSNVPREqp/u5v
          AU74WbAqFeImeH09hcl26QfhQbT0QxcByTTSRqLNfWbYZUQJpdvfT11St07br75h
          2SaPqPVGvRBtI6omsY4QhoaX5SRuAanf3gIHqE0oGwKCAQABOuRQagSmPmKPFCJs
          bRTdHWI/IVbxjadHoyjyolGynBp0cNYoeBPRWg/jFfOVtTXnd5em86kpb1sJWwXf
          7Lr20/maTcKhRAKtuJFXNc7IGc2sRGxMW4njqWR1U2LkeQPzWMYGXN1ZXwEjGaBU
          l1JvaQwXKk8dHa4Zi2sESNWqCAAlRhtvrh8YfBfhiZLAFJF5lG3Etm7qxt/oJK0a
          QMQASlsPgDyW6+X5RGkoCZ95U4Bj8qtat2Dsf1auKeQIjipJ3vgSatfZL0GwiUVy
          FtESnXCskG9cBISZXwB1e252PcuCr+0KWeLt8HHOIOjeXeLansMmUabzJPDXbCyc
          PacJAoIBAQDH2+6D7w3gBAhQ101SdLELtckQ7RHZXtn84GhLTuCnk01l1ZS/x0Zb
          YcCBQvhThd9YXnzpS/+5qbE7dD3a0bWKgGRwbEqI0tHUV72n5M5L5F+K00iV40Yd
          H9hIox+u/F8GhrK4aPCB/3wvuTz3qnz78HWmYIYYAT/EzNtCZgqFuubdqfV/bupM
          dnPFWa2VctFlB7pWMK5WRoKAWC45nnJH8ITPEPMHwMxigqKjjQWcvBC7/B8MAdhc
          6qk7CbWEThChJOX057RZzAu7V0eSMWYyEhO9RjN3+PxR41HmOPfzwYDysoO2PiKK
          cQKhgQKCKGbpdOCLLUf5PQ5peCNumORzAoIBAEE7yOyGX2JUe7dF2tTS9wYH4ceE
          QHM/Sm9qCdO41h2omsdzC9c+TD+T6S2j09FVURYZAY0xwAEVpgG4cx0hyiyWyKZc
          VuzocaDU65m1QgstMCu26O+uRbKAPV6UUea/mwkJgO0p5FY3w1Fw7c9Lmg6IKFmd
          iTdORJrd/uv6mwr4Aqg9hpY45nNFwdbd9pFenAyt9youeT1iURNbmpbmg57HgufM
          qKLW2FUYzghApxVL3YuJnmVjgmgR5rcIzcOhpSI8PK2lCwR0HAKgQnH3flFilWHe
          i4/WiPVLNr4z1txNflUCpi5nR3V70c+Y52vUfoj0Enmp2txcvuFtVJNkC10=
          -----END RSA PRIVATE KEY-----`);

        const sJWT = KJUR.jws.JWS.sign('RS256', sHeader, sPayload, privateKey);

        $window.open(
          `${nvURL.buildSDUrl(nv.config.env, $rootScope.domain)}/app/accounts/login?token=${sJWT}`
        );
      },

      disableSdShipper: legacyId =>
        $http.post('shipperpanel/app/accounts/deactivate', { shipper_id: legacyId })
          .then(data => $q.resolve(data.data), error => $q.reject(error)),

      readSalesPerson: () =>
        $http.get('core/salespersons')
          .then(nvRequestUtils.success, nvRequestUtils.nvFailure),

      addSellers: (marketplaceId, ids) =>
        $http.post(`shipper/2.0/shippers/${marketplaceId}`, ids)
          .then(nvRequestUtils.success, nvRequestUtils.nvFailure),

      reMapSellers: (marketplaceId, ids) =>
        $http.put(`shipper/2.0/shippers/${marketplaceId}`, ids)
          .then(nvRequestUtils.success, nvRequestUtils.nvFailure),

      createAddress: (shipperId, payload, param = {}) =>
        $http.post(`shipper/2.0/shippers/${shipperId}/addresses?${nvRequestUtils.queryParam(param)}`, payload)
          .then(nvRequestUtils.success, nvRequestUtils.nvFailure),

      readAddresses: (shipperId) => {
        const deferred = $q.defer();
        readAllAddress(shipperId, deferred);
        return deferred.promise;
      },

      searchAddressByIds: ids => findAddress(_.uniq(_.compact(_.castArray(ids)))),

      updateAddress: (shipperId, addressId, payload) =>
        $http.patch(`shipper/2.0/shippers/${shipperId}/addresses/${addressId}`, payload)
          .then((data) => {
            const address = data.data;
            ADDRESS_MAP[addressId] = address;
            return $q.when(address);
          }, nvRequestUtils.nvFailure),

      deleteAddress: (shipperId, addressId) =>
        $http.delete(`shipper/2.0/shippers/${shipperId}/addresses/${addressId}`)
          .then(nvRequestUtils.success, nvRequestUtils.nvFailure),
      mapToLocalizedAddress: mapToLocalizedAddress,
      mapToInternalAddress: mapToInternalAddress,
    };

    return self;

    function remapSingleShipper(shipper) {
      return _.assign(shipper, { id: shipper.legacy_id, global_id: shipper.id });
    }

    function remapAllShipper(data) {
      if (data && data.data) {
        data.data = _.map(data.data, shipper => remapSingleShipper(shipper));
      }
      return data;
    }

    function elasticRemapAllShipper(data) {
      if (data && data.details) {
        data.details = _.map(data.details, shipper => remapSingleShipper(shipper));
      }
      return data;
    }

    function elasticGetAllShipper(defer, options) {
      const callLimit = 200000;
      self.elasticRead(_.defaults(options, { size: callLimit })).then((response) => {
        const totalCount = _.get(response, 'total_hits') || 0;
        if (totalCount < callLimit) {
          defer.resolve(response.details);
        } else {
          let shippers = response.details;
          const loopNumber = Math.ceil(totalCount / callLimit) - 1;
          const promises = [];
          for (let i = 0; i < loopNumber; i++) {
            promises.push(self.elasticRead(_.cloneDeep(_.assign(options, {
              from: ((i + 1) * callLimit),
              size: callLimit }
            ))));
          }
          $q.all(promises).then((responses) => {
            _.forEach(responses, (childResponse) => {
              shippers = _.concat(shippers, childResponse.details);
            });
            shippers = _.sortBy(shippers, ['id']);
            defer.resolve(shippers);
          }, () => defer.reject(-1));
          // do call
        }
      }, () => defer.reject(-1));
    }

    function getAllShipper(defer, options) {
      const callLimit = 100;
      let shippers = [];
      self.readCount(options).then((response) => {
        const totalCount = response;

        const loopNumber = Math.ceil(totalCount / callLimit);
        const promises = [];
        for (let i = 0; i < loopNumber; i++) {
          promises.push(self.read(_.assign(options, {
            start: i * callLimit,
            limit: callLimit }
          )));
        }
        $q.all(promises).then((responses) => {
          _.forEach(responses, (childResponse) => {
            shippers = _.concat(shippers, childResponse.data);
          });
          shippers = _.sortBy(shippers, ['id']);
          defer.resolve(shippers);
        }, () => defer.reject(-1));
        // do call
      }, () => defer.reject(-1));
    }

    function getAllSellers(defer, id, options) {
      const callLimit = 100;
      readSellers(id, _.defaults(options, { limit: callLimit })).then((response) => {
        const totalCount = _.get(response, 'paging.total_count') || 0;
        if (totalCount < callLimit) {
          defer.resolve(response.data);
        } else {
          let sellers = response.data;
          const loopNumber = Math.ceil(totalCount / callLimit) - 1;
          const promises = [];
          for (let i = 0; i < loopNumber; i++) {
            promises.push(readSellers(id, _.assign(options, {
              start: ((i + 1) * callLimit),
              limit: callLimit }
            )));
          }
          $q.all(promises).then((responses) => {
            _.forEach(responses, (childResponse) => {
              sellers = _.concat(sellers, childResponse.data);
            });
            sellers = _.sortBy(sellers, ['id']);
            defer.resolve(sellers);
          }, () => defer.reject(-1));
          // do call
        }
      }, () => defer.reject(-1));

      function readSellers(marketplaceId, localOptions) {
        const param = _.defaults(localOptions, { start: 0, limit: 100 });
        return $http
          .get(`shipper/2.0/shippers/${marketplaceId}/sellers?${nvRequestUtils.queryParam(param)}`)
          .then(response => $q.resolve(response.data), nvRequestUtils.nvFailure);
      }
    }

    function elasticGetAllSellers(defer, id) {
      const callLimit = 10000;
      const options = { limit: callLimit };
      elasticReadSellers(id, options).then((response) => {
        const totalCount = _.get(response, 'total_hits') || 0;
        if (totalCount < callLimit) {
          defer.resolve(response.marketplace_sellers);
        } else {
          let sellers = response.marketplace_sellers;
          const loopNumber = Math.ceil(totalCount / callLimit) - 1;
          const promises = [];
          for (let i = 0; i < loopNumber; i++) {
            promises.push(elasticReadSellers(id, _.assign(options, {
              start: ((i + 1) * callLimit),
              limit: callLimit }
            )));
          }
          $q.all(promises).then((responses) => {
            _.forEach(responses, (childResponse) => {
              sellers = _.concat(sellers, childResponse.marketplace_sellers);
            });
            sellers = _.sortBy(sellers, ['id']);
            defer.resolve(sellers);
          }, () => defer.reject(-1));
          // do call
        }
      }, () => defer.reject(-1));

      function elasticReadSellers(marketplaceId, localOptions) {
        const param = _.defaults(localOptions, { start: 0, limit: 10000 });
        return $http
          .get(`shipper-search/shippers/${marketplaceId}/sellers?${nvRequestUtils.queryParam(param)}`)
          .then(response => $q.resolve(response.data), nvRequestUtils.nvFailure);
      }
    }

    function mapToLocalizedAddress(address) {
      const result = {
        id: _.get(address, 'id'),
        name: _.get(address, 'name'),
        contact: _.get(address, 'contact'),
        email: _.get(address, 'email'),
        address1: _.get(address, 'address1'),
        address2: _.get(address, 'address2'),
        country: _.toUpper(_.get(address, 'country', nvDomain.getDomain().currentCountry)),
        latitude: _.toString(_.get(address, 'latitude')),
        longitude: _.toString(_.get(address, 'longitude')),
      };
      switch (result.country) {
        case 'SG':
          _.assign(result, {
            postcode: _.get(address, 'postcode'),
          });
          break;
        case 'MY':
          _.assign(result, {
            area: _.get(address, 'neighbourhood'),
            city: _.get(address, 'locality'),
            state: _.get(address, 'region'),
            postcode: _.get(address, 'postcode'),
          });
          break;
        case 'ID': {
          const neighbourhoods = _(_.get(address, 'neighbourhood')).split(',').map(s => _.trim(s)).value();
          _.assign(result, {
            kelurahan: _.first(neighbourhoods),
            kecamatan: _.last(neighbourhoods),
            city: _.get(address, 'locality'),
            province: _.get(address, 'region'),
            postcode: _.get(address, 'postcode'),
          });
          break;
        }
        case 'VN':
          _.assign(result, {
            ward: _.get(address, 'neighbourhood'),
            district: _.get(address, 'locality'),
            city: _.get(address, 'region'),
          });
          break;
        case 'TH':
          _.assign(result, {
            sub_district: _.get(address, 'neighbourhood'),
            district: _.get(address, 'locality'),
            province: _.get(address, 'region'),
            postcode: _.get(address, 'postcode'),
          });
          break;
        case 'PH':
          _.assign(result, {
            barangay: _.get(address, 'neighbourhood'),
            city: _.get(address, 'locality'),
            province: _.get(address, 'region'),
            postcode: _.get(address, 'postcode'),
          });
          break;
        case 'MM':
          _.assign(result, {
            township: _.get(address, 'neighbourhood'),
            district: _.get(address, 'locality'),
            state: _.get(address, 'region'),
            postcode: _.get(address, 'postcode'),
          });
          break;
        default:
          _.assign(result, {
            postcode: _.get(address, 'postcode'),
          });
          break;
      }

      return result;
    }

    function mapToInternalAddress(address) {
      const result = {
        id: _.get(address, 'id'),
        name: _.get(address, 'name'),
        contact: _.get(address, 'contact'),
        email: _.get(address, 'email'),
        address1: _.get(address, 'address1', ''),
        address2: _.get(address, 'address2', ''),
        country: _.toUpper(_.get(address, 'country', nvDomain.getDomain().currentCountry)),
        latitude: _.toNumber(_.get(address, 'latitude')),
        longitude: _.toNumber(_.get(address, 'longitude')),
      };
      switch (result.country) {
        case 'SG':
          _.assign(result, {
            postcode: _.get(address, 'postcode'),
          });
          break;
        case 'MY':
          _.assign(result, {
            neighbourhood: _.get(address, 'area'),
            locality: _.get(address, 'city'),
            region: _.get(address, 'state'),
            postcode: _.get(address, 'postcode'),
          });
          break;
        case 'ID': {
          const neighbourhood = `${_.get(address, 'kelurahan', '')}, ${_.get(address, 'kecamatan', '')}`;
          _.assign(result, {
            neighbourhood: neighbourhood,
            locality: _.get(address, 'city'),
            region: _.get(address, 'province'),
            postcode: _.get(address, 'postcode'),
          });
          break;
        }
        case 'VN':
          _.assign(result, {
            neighbourhood: _.get(address, 'ward'),
            locality: _.get(address, 'district'),
            region: _.get(address, 'city'),
          });
          break;
        case 'TH':
          _.assign(result, {
            neighbourhood: _.get(address, 'sub_district'),
            locality: _.get(address, 'district'),
            region: _.get(address, 'province'),
            postcode: _.get(address, 'postcode'),
          });
          break;
        case 'PH':
          _.assign(result, {
            neighbourhood: _.get(address, 'barangay'),
            locality: _.get(address, 'city'),
            region: _.get(address, 'province'),
            postcode: _.get(address, 'postcode'),
          });
          break;
        case 'MM':
          _.assign(result, {
            neighbourhood: _.get(address, 'township'),
            locality: _.get(address, 'district'),
            region: _.get(address, 'state'),
            postcode: _.get(address, 'postcode'),
          });
          break;
        default:
          _.assign(result, {
            postcode: _.get(address, 'postcode'),
          });
          break;
      }

      return result;
    }

    function readAllAddress(shipperId, deferred) {
      $http.get(`shipper/2.0/shippers/${shipperId}/addresses-count`).then((response) => {
        const count = 50;
        const loopNumber = Math.ceil(Number(response.data) / count);
        const promiseResults = [];
        for (let i = 0; i < loopNumber; i++) {
          const start = (i * count);
          const param = {
            start: start,
            size: count,
          };
          promiseResults.push($http.get(`shipper/2.0/shippers/${shipperId}/addresses?${nvRequestUtils.queryParam(param)}`));
        }
        $q.all(promiseResults).then(success, () => deferred.reject(-1));
      }, () => deferred.reject(-1));

      function success(responses) {
        let result = [];
        _.forEach(responses, (response) => {
          const addresses = _.map(_.get(response, 'data.data'),
            address => nvExtendUtils.addDisplayAddress(address));
          result = _.concat(result, addresses);
        });
        deferred.resolve(result);
      }
    }

    function findAddress(ids) {
      if (_.size(ids) <= 0) {
        return $q.when([]);
      }

      const RESULT = [];
      const idsToSearch = [];

      _.forEach(ids, (id) => {
        if (ADDRESS_MAP[id]) {
          RESULT.push(ADDRESS_MAP[id]);
        } else {
          idsToSearch.push(id);
        }
      });

      if (_.size(idsToSearch) <= 0) {
        return $q.when(RESULT);
      }
      const deferred = $q.defer();
      getAddresses(idsToSearch)
        .then(success, failure);
      return deferred.promise;

      function getAddresses(addressIds) {
        const CHUNK = 1000;
        const chunkedAddressIds = _.chunk(addressIds, CHUNK);
        const promises = _(chunkedAddressIds)
          .map(theIds => ({ address_ids: theIds }))
          .map(thePayload => $http.post('shipper/legacy/shippers/addresses-search', thePayload))
          .value();
        return $q.all(promises).then(mergeAddressResults, e => $q.reject(e));

        function mergeAddressResults(responses) {
          const result = _.flatMap(responses, response => _.get(response, 'data'));
          return $q.when(result);
        }
      }

      function success(addresses) {
        const notFound = [];

        _.forEach(idsToSearch, (id) => {
          const theAddress = _.find(addresses, { id: id });

          if (theAddress) {
            ADDRESS_MAP[id] = theAddress;
            RESULT.push(theAddress);
          } else {
            notFound.push(id);
          }

          return _.noop();
        });

        deferred.resolve(RESULT);
      }

      function failure(response) {
        deferred.reject(response);
      }
    }
  }
}());
