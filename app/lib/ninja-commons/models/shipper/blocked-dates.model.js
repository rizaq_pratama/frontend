(function model() {
  angular
    .module('nvCommons.models')
    .factory('BlockedDates', BlockedDates);

  BlockedDates.$inject = ['$http', 'nvRequestUtils', 'nvExtendUtils', 'nvModelUtils', '$rootScope', 'nvTimezone'];

  function BlockedDates($http, nvRequestUtils, nvExtendUtils, nvModelUtils, $rootScope, nvTimezone) {
    const fields = {
      id: { displayName: 'ID', type: 'number' },
      date: { displayName: 'Blocked Date', type: 'date' },
      createdAt: { displayName: 'Created On', type: 'date' },
      updatedAt: { displayName: 'Updated On', type: 'date' },
      deletedAt: { displayName: 'Deleted On', type: 'date' },
    };

    return {

      create: options =>
        $http.post('core/calendar/addblockeddates/', options).then(
          nvRequestUtils.success,
          nvRequestUtils.nvFailure
        ),

      read: () =>
        $http.get('core/calendar/blockeddates').then(
          nvRequestUtils.success,
          nvRequestUtils.nvFailure
        ).then(addMoment),

      delete: options =>
        $http.post('core/calendar/deleteblockeddates/', options).then(
          nvRequestUtils.success,
          nvRequestUtils.nvFailure
        ),

      getAddFields: () =>
        nvModelUtils.pickFields(fields, ['date']),
    };

    function addMoment(blockeddates) {
      return nvExtendUtils.addMoment(blockeddates, ['date'], nvTimezone.getOperatorTimezone());
    }
  }
}());
