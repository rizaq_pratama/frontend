(function model() {
  angular
    .module('nvCommons.models')
    .factory('Billings', Billings);

  Billings.$inject = ['$http', 'nvRequestUtils'];

  function Billings($http, nvRequestUtils) {
    return {
      generateSuccessBillings: (options, emails) =>
        $http.post(`core/successbillings/generate_background?${nvRequestUtils.queryParam(options)}`, { email: emails }).then(
          nvRequestUtils.success,
          nvRequestUtils.nvFailure
      ),

      generateSuccessBillingsV2: payload =>
        $http.post('core/2.0/order-billings/generate', payload).then(
          nvRequestUtils.success,
          nvRequestUtils.nvFailure
      ),

      getEmailFields: () => ({ email: { displayName: 'commons.model.email' } }),
    };
  }
}());
