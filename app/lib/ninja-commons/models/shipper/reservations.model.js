(function reservationsModel() {
  angular
        .module('nvCommons.models')
        .factory('Reservation', Reservation);

  Reservation.$inject = [
    '$http',
    '$q',
    'nvRequestUtils',
    'nvExtendUtils',
    'nvModelUtils',
    'nvDateTimeUtils',
    'nvTimezone',
    'nvTranslate',
  ];

  function Reservation(
    $http,
    $q,
    nvRequestUtils,
    nvExtendUtils,
    nvModelUtils,
    nvDateTimeUtils,
    nvTimezone,
    nvTranslate
  ) {
    const fields = {
      id: { displayName: 'ID' },
      activeWaypoint: {
        _deep: true,
        timewindowId: { displayName: 'Active Waypoint', defaultValue: 0 },
        status: { displayName: 'Status' },
        address1: { displayName: 'Address1' },
        address2: { displayName: 'Address2' },
        city: { displayName: 'City' },
        country: { displayName: 'Country' },
        postcode: { displayName: 'Postal Code' } },
      addressId: { displayName: 'Address Id' },
      approxVolume: { displayName: 'Approximate Volume' },
      comments: { displayName: 'Comments', defaultValue: '' },
      readyDatetime: { displayName: 'Ready Time' },
      latestDatetime: { displayName: 'Latest By' },
      shipperId: { displayName: 'Shipper' },
      status: { displayName: 'Status' },
      updatedAt: { displayName: 'Updated On' },
      deletedAt: { displayName: 'Deleted On' },
    };

    const tableField = {
      formattedDate: { displayName: nvTranslate.instant('container.reservations.expected-time') },
      id: { displayName: nvTranslate.instant('container.reservations.reservation-id') },
      dpName: { displayName: nvTranslate.instant('container.reservations.assigned-dp') },
      reservationType: { displayName: nvTranslate.instant('container.reservations.type') },
      approxVolume: { displayName: nvTranslate.instant('container.reservations.approx-vol-label') },
      pickupAddress: { displayName: nvTranslate.instant('container.reservations.pick-up-address') },
      name: { displayName: nvTranslate.instant('container.reservations.shipper-name') },
      contact: { displayName: nvTranslate.instant('container.reservations.shipper-contact') },
    };

    const BOOKING_STATUS = {
      OPEN: 'OPEN',
      CLOSED: 'CLOSED',
      REJECTED: 'REJECTED',
      COMPLETED: 'COMPLETED',
      CANCELLED: 'CANCELLED',
      WAITING: 'WAITING',
    };

    const REJECTED_FIELDS = {
      id: { displayName: 'commons.id' },
      shipper_id: { displayName: 'commons.shipper' },
      reservation_id: { displayName: 'commons.reservation-id' },
      waypoint_id: { displayName: 'commons.waypoint-id' },
      priority_level: { displayName: 'commons.model.priority-level' },
      start: { displayName: 'commons.start-time' },
      end: { displayName: 'commons.end-time' },
      rejection_reason: { displayName: 'commons.model.rejection-reason' },
      driver_id: { displayName: 'commons.driver' },
      route_id: { displayName: 'commons.route' },
      hub_id: { displayName: 'commons.hub' },
      rejected_at: { displayName: 'commons.model.time-rejected' },
      driver_assigned_at: { displayName: 'commons.model.assigned-at' },
      waiting_at: { displayName: 'commons.model.waiting-at' },
      from_address_1: { displayName: 'commons.address1' },
      from_address_2: { displayName: 'commons.address2' },
      from_district: { displayName: 'commons.district' },
      from_province: { displayName: 'commons.province' },
      from_city: { displayName: 'commons.city' },
      from_region: { displayName: 'commons.region' },
      from_state: { displayName: 'commons.state' },
      from_country: { displayName: 'commons.country' },
      from_postal_code: { displayName: 'commons.postcode' },
      from_latitude: { displayName: 'commons.latitude' },
      from_longitude: { displayName: 'commons.longitude' },
      deadline: { displayName: 'commons.deadline' },
    };

    const PICKUP_SERVICE_TYPE = {
      SCHEDULED: 'Scheduled',
      ONDEMAND: 'OnDemand',
    };

    const PICKUP_SERVICE_LEVEL = {
      STANDARD: 'Standard',
      PREMIUM: 'Premium',
    };

    const RESERVATION_TYPE_ENUM = {
      REGULAR: 0,
      ON_DEMAND: 1,
      HYPERLOCAL: 2,
      SHIPPER_CUSTOMER: 3,
      PREMIUM_SCHEDULED: 4,
    };

    const volumeOptions = ['Less than 10 Parcels',
                             'Less than 3 Parcels',
                             'Trolley Required',
                             'Half-Van Load',
                             'Full-Van Load',
                             'Larger than Van Load'];

    const typeOptions = [
      { id: 0, value: 'REGULAR', displayName: 'Normal' },
      { id: 1, value: 'ON_DEMAND', displayName: 'On-Demand' },
      { id: 2, value: 'HYPERLOCAL', displayName: 'Hyperlocal' },
      { id: 3, value: 'SHIPPER_CUSTOMER', displayName: 'Shipper Customer' },
      { id: 4, value: 'PREMIUM_SCHEDULED', displayName: 'Premium Scheduled' },
    ];

    const STATUS = {
      PENDING: 0,
      SUCCESS: 1,
      FAIL: 2,
      REJECT: 3,
      CANCEL: 4,
    };

    const self = {

      TABLE_FIELDS: tableField,
      REJECTED_FIELDS: REJECTED_FIELDS,
      BOOKING_STATUS: BOOKING_STATUS,
      STATUS: STATUS,
      PICKUP_SERVICE_TYPE: PICKUP_SERVICE_TYPE,
      PICKUP_SERVICE_LEVEL: PICKUP_SERVICE_LEVEL,

      read: function read(obj) {
        return $http.get(`reservation/reservations?${nvRequestUtils.queryParam(obj)}`).then(
                    nvRequestUtils.success,
                    nvRequestUtils.nvFailure
                )
                .then(filterOutCancelled)
                .then(addMoment)
                .then(_.partial(nvModelUtils.setDefaultsToObject, fields));
      },

      searchByIds: function searchByIds(ids) {
        return ids && ids.length > 0
          ? self.search({ reservationId: ids, includeRoute: true })
          : $q.resolve([]);
      },

      searchWithRouteIdsOnlyByIds: function searchWithRouteIdsOnlyByIds(ids) {
        if (!ids || ids.length <= 0) {
          return $q.resolve([]);
        }

        return $http
          .post('reservation/reservations-with-routes', { reservationId: ids })
          .then(nvRequestUtils.success, nvRequestUtils.nvFailure);
      },

      /**
       * Search Reservations
       * @param  {Object} options:
       *           reservationId  {Array}     <int>
       *           waypointId     {Array}     <int>
       *           dpId           {Array}     <int>
       *           shipperId      {Array}     <int>
       *           pickupSize     {Array}     <String>
       *           onDemand       {Boolean}   <True/False>
       *           readyStartTime {DateTime}
       *           readyEndTime   {DateTime}
       * @return {Promise}
       */
      search: function search(options) {
        const payload = _.defaultsDeep(options, {
          includeRoute: false,
        });

        if (payload.startDate && payload.endDate) {
          payload.startDate = nvDateTimeUtils.displayDateTime(moment(payload.startDate), 'utc');
          payload.endDate = nvDateTimeUtils.displayDateTime(moment(payload.endDate), 'utc');
        }

        if (options.createdFrom && options.createdTo) {
          options.createdFrom = nvDateTimeUtils.displayDateTime(moment(options.createdFrom), 'utc');
          options.createdTo = nvDateTimeUtils.displayDateTime(moment(options.createdTo), 'utc');
        }

        return $http.post('reservation/reservations/search', payload).then(
          nvRequestUtils.success,
          nvRequestUtils.nvFailure
        ).then(addMoment);
      },

      /**
       * Create Reservations
       * @param  {Number} shipperId [shipper.id]
       * @param  {Number} addressId [address.id]
       * @param  {Array} options   eg: { timewindowId: <int>,
       *                                 readyDatetime: <string>,
       *                                 latestDatetime: <string>,
       *                                 approxVolume: <string>,
       *                                 comments: <string>,
       *                             }
       * @return {Promise}
       */
      create: function create(options) {
        const calls = [];

        _.forEach(options, (rsvn) => {
          // trim to prevent whitespace since it slowing down trim process in angular http call
          rsvn.pickup_instruction = rsvn.pickup_instruction ? rsvn.pickup_instruction.trim() : null;

          calls.push($http.post('reservation/2.0/reservations', rsvn).then(
            nvRequestUtils.success,
            nvRequestUtils.nvFailure
          ));
        });

        return $q.allSettled(calls);
      },

      createHyperlocal: function createHyperlocal(shipperId, options) {
        const param = { shipper_id: shipperId };
        return $http.post(`reservation/reservations?${nvRequestUtils.queryParam(param)}`, options).then(
                    nvRequestUtils.success,
                    nvRequestUtils.nvFailure
                );
      },

      updateHyperlocal: function updateHyperlocal(payload) {
        return $http.put(`reservation/reservations`, payload)
          .then(nvRequestUtils.success,
                nvRequestUtils.nvFailure);
      },

      update: function update(reservation) {
        const param = { address_id: reservation.addressId, shipper_id: reservation.shipperId };
        
        // trim to prevent whitespace since it slowing down trim process in angular http call
        const comments = reservation.comments ? reservation.comments.trim() : null;
        const options = [{
          id: reservation.id,
          timewindowId: reservation.activeWaypoint.timewindowId,
          readyDatetime: displayDateTime(reservation._momentReadyDatetime),
          latestDatetime: displayDateTime(reservation._momentLatestDatetime),
          approxVolume: reservation.approxVolume,
          comments: comments,
          priorityLevel: reservation.priorityLevel,
        }];
        return $http
          .put(`reservation/reservations?${nvRequestUtils.queryParam(param)}`, options)
          .then(nvRequestUtils.success, nvRequestUtils.nvFailure);
        
        function displayDateTime(moment) {
          return nvDateTimeUtils.displayDateTime(moment, 'utc');
        }
      },

      updatePartial: function update(payload) {
        const payloads = _.castArray(payload);
        return $http.put('reservation/reservations', payloads)
          .then(nvRequestUtils.success, nvRequestUtils.nvFailure);
      },

      updateStatus: (reservationId, payload) =>
        $http.post(`reservation/2.0/reservations/${reservationId}/update-status`, payload).then(
          nvRequestUtils.success,
          nvRequestUtils.nvFailure
        ),

      isHyperlocal: function isHyperlocal(reservation) {
        return reservation && reservation.reservationType &&
          reservation.reservationType.toLowerCase() === 'hyperlocal';
      },

      combineDateTimeToString: function combineDateTimeToString(momentDate, momentTime) {
        const m = _.cloneDeep(momentDate).tz(nvTimezone.getOperatorTimezone());
        m
          .hour(momentTime.hour())
          .minute(momentTime.minute())
          .second(0);
        return nvDateTimeUtils.displayFormat(m, null);
      },

      unrouteReservation: (id, payload = {}) =>
        $http
          .put(`core/2.0/reservations/${id}/unroute`, payload)
          .then(nvRequestUtils.success, nvRequestUtils.nvFailure),

      updateV2: (id, payload) =>
        $http.post(`reservation/2.0/reservations/${id}`, payload)
          .then(nvRequestUtils.success, nvRequestUtils.nvFailure),

      getRejectedReservations: filter =>
        $http.get(`overwatch/2.0/bookings?${nvRequestUtils.queryParam(filter)}`)
          .then(nvRequestUtils.nvSuccess, nvRequestUtils.nvFailure),

      assignReservation: (reservationId, driverId, data) =>
        $http.put(`overwatch/1.0/reservations/${reservationId}/drivers/${driverId}?callback=true`, data)
          .then(nvRequestUtils.nvSuccess, nvRequestUtils.nvFailure),

      failReservation: (bookingId, data) =>
        $http.delete2(`overwatch/1.1/bookings/${bookingId}`, data)
          .then(nvRequestUtils.nvSuccess, nvRequestUtils.nvFailure),

      volumeOptions: volumeOptions,
      typeOptions: typeOptions,
      statusOptions: getStatusOptions(),
      getTypeId: getTypeId,
      TYPE: RESERVATION_TYPE_ENUM,
    };

    return self;

    function getStatusOptions() {
      return _.map(STATUS, (value, key) => ({
        id: value,
        displayName: capitalizeFirstLetter(_.toLower(key)),
      }));

      function capitalizeFirstLetter(string) {
        return string.charAt(0).toUpperCase() + string.slice(1);
      }
    }

    function addMoment(reservations) {
      return nvExtendUtils.addMoment(reservations, ['readyDatetime', 'latestDatetime'], nvTimezone.getOperatorTimezone());
    }

    function filterOutCancelled(reservations) {
      return _.filter(reservations, reservation =>
        STATUS[reservation.status] !== STATUS.CANCEL
      );
    }

    function getTypeId(type) {
      const option = _.find(typeOptions, opt => _.toLower(opt.value) === _.toLower(type));
      return _.get(option, 'id');
    }
  }
}());
