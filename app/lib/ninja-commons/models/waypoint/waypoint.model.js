(function model() {
  angular
    .module('nvCommons.models')
    .factory('Waypoint', Waypoint);

  Waypoint.$inject = ['$http', 'nvRequestUtils', 'nvDateTimeUtils', 'nvTimezone', '$q'];

  function Waypoint($http, nvRequestUtils, nvDateTimeUtils, nvTimezone, $q) {
    /* jshint unused: false */
    const fields = {
      id: { displayName: 'ID' },
      address1: { displayName: 'Address1' },
      address2: { displayName: 'Address2' },
      postcode: { displayName: 'Postal Code' },
      city: { displayName: 'City' },
      country: { displayName: 'Country' },
      latitude: { displayName: 'Latitude' },
      longitude: { displayName: 'Longitude' },
    };

    const type = {
      RESERVATION: 'RESERVATION',
      TRANSACTION: 'TRANSACTION',
    };

    const TIMEWINDOW_ID = [
      { id: -3, displayName: 'Night' },
      { id: -2, displayName: 'Day' },
      { id: -1, displayName: 'Anytime' },
      { id: 0, displayName: '9-12pm' },
      { id: 1, displayName: '12-3pm' },
      { id: 2, displayName: '3-6pm' },
      { id: 3, displayName: '6-10pm' },
    ];

    const UPDATE_STATUS_ACTION = {
      SUCCESS: 'success',
      FAIL: 'fail',
      ROUTED: 'routed',
      PENDING: 'pending',
    };

    const WAYPOINTS_MAP = {};

    const self = {
      FIELDS: fields,
      TYPE: type,
      TIMEWINDOW_ID: TIMEWINDOW_ID,
      UPDATE_STATUS_ACTION: UPDATE_STATUS_ACTION,

      isTransaction: function isTransaction(waypoint) {
        if (waypoint.type && waypoint.type.toUpperCase() === 'TRANSACTION') {
          return true;
        }
        return false;
      },
      isReservation: function isReservation(waypoint) {
        if (waypoint.type && waypoint.type.toUpperCase() === 'RESERVATION') {
          return true;
        }
        return false;
      },

      cleanup: (from, to) => {
        const dateRange = {
          from: nvDateTimeUtils.displayDateTime(nvDateTimeUtils.toSOD(from, nvTimezone.getOperatorTimezone()), 'utc'),
          to: nvDateTimeUtils.displayDateTime(nvDateTimeUtils.toEOD(to, nvTimezone.getOperatorTimezone()), 'utc'),
        };
        return $http.get(`core/tools/cleanwaypoints?${nvRequestUtils.queryParam(dateRange)}`).then(
                    nvRequestUtils.success,
                    nvRequestUtils.nvFailure);
      },

      /**
       * this function returns waypoint level data only
       */
      getByIds: (wpIds) => {
        const payload = {
          ids: wpIds,
        };
        return $http.post('core/waypoints/search', payload)
          .then(nvRequestUtils.success, nvRequestUtils.nvFailure);
      },

      /**
       * Search waypoints by ids - v2.0
       * @param  {Array<number>} ids - waypoint ids
       * @return {Promise} Promise with array of waypoint objects
       */
      v2GetByIds: ids =>
        $http.get(`core/2.0/waypoints?ids=${_.join(ids, ',')}`)
          .then(nvRequestUtils.success, nvRequestUtils.nvFailure),

      /**
       * this function returns full address, tracking_id, transaction start/end time, etc
       */
      search: (wpIds) => {
        const payload = {
          ids: wpIds,
        };
        return $http.post('core/1.0/waypoints/search', payload)
          .then(nvRequestUtils.success, nvRequestUtils.nvFailure);
      },

      /**
       * Search waypoints by ids and store it to cache
       * @param  {Array<number>} ids - waypoint ids
       * @return {Promise} Promise with array of waypoint objects
       */
      searchByIds: (ids) => {
        const results = [];
        const idsToSearch = [];
        _.forEach(_.uniq(_.compact(_.castArray(ids))), (id) => {
          if (WAYPOINTS_MAP[id]) {
            results.push(WAYPOINTS_MAP[id]);
          } else {
            idsToSearch.push(id);
          }
        });

        if (_.size(idsToSearch) <= 0) {
          return $q.when(results);
        }

        const deferred = $q.defer();
        const MAX_CALL_IN_CHUNK = 30;
        const chunkIds = _.chunk(idsToSearch, MAX_CALL_IN_CHUNK);
        const promises = [];
        _.forEach(chunkIds, (theIds) => {
          promises.push(
            self.v2GetByIds(theIds)
          );
        });
        $q.all(promises).then(success, failure);

        return deferred.promise;

        function success(responses) {
          let waypoints = [];
          _.forEach(responses, (response) => {
            waypoints = _.concat(waypoints, response);
          });

          const notFound = [];
          _.forEach(idsToSearch, (id) => {
            const theWaypoint = _.find(waypoints, ['id', id]);

            if (theWaypoint) {
              WAYPOINTS_MAP[theWaypoint.id] = theWaypoint;
              results.push(theWaypoint);
            } else {
              notFound.push(id);
            }

            return _.noop();
          });

          deferred.resolve(results);
        }

        function failure(response) {
          deferred.reject(response);
        }
      },

      update: payload =>
        $http.put('core/waypoints', payload)
          .then(nvRequestUtils.success, nvRequestUtils.nvFailure),

      updateStatus: (routeId, waypointId, payload) =>
        $http.put(`core/admin/routes/${routeId}/waypoints/${waypointId}/pods`, payload)
          .then(nvRequestUtils.success, nvRequestUtils.nvFailure),

      getPhotos: wpId => $http
        .get(`core/waypoints/${wpId}/photos`)
        .then(nvRequestUtils.success, nvRequestUtils.nvFailure
      ),

      statusAsOptions: () =>
        _.map(UPDATE_STATUS_ACTION, (val, key) =>
          ({ id: _.startCase(val), value: val, displayName: key })),
    };

    return self;
  }
}());
