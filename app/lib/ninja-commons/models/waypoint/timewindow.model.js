(function model() {
  angular
    .module('nvCommons.models')
    .factory('Timewindow', Timewindow);

  Timewindow.$inject = ['nvDateTimeUtils', 'nvTimezone'];

  function Timewindow(nvDateTimeUtils, nvTimezone) {
    const TYPE = {
      NIGHTSLOT: '-3',
      DAYSLOT: '-2',
      DAYNIGHTSLOT: '-1',
      TIMESLOT: '0-3',
    };
    const TIMEWINDOWS = [
      {
        id: -3,
        name: 'Night Slot (6PM - 10PM)',
        fromTime: moment().hour(18).minute(0).second(0).millisecond(0),
        toTime: moment().hour(22).minute(0).second(0).millisecond(0),
        type: TYPE.NIGHTSLOT,
      },
      {
        id: -2,
        name: 'Day Slot (9AM - 6PM)',
        fromTime: moment().hour(9).minute(0).second(0).millisecond(0),
        toTime: moment().hour(18).minute(0).second(0).millisecond(0),
        type: TYPE.DAYSLOT,
      },
      {
        id: -1,
        name: 'All Day (9AM - 10PM)',
        fromTime: moment().hour(9).minute(0).second(0).millisecond(0),
        toTime: moment().hour(22).minute(0).second(0).millisecond(0),
        type: TYPE.DAYNIGHTSLOT,
      },
      {
        id: 0,
        name: '9AM - 12PM',
        fromTime: moment().hour(9).minute(0).second(0).millisecond(0),
        toTime: moment().hour(12).minute(0).second(0).millisecond(0),
        type: TYPE.TIMESLOT,
      },
      {
        id: 1,
        name: '12PM - 3PM',
        fromTime: moment().hour(12).minute(0).second(0).millisecond(0),
        toTime: moment().hour(15).minute(0).second(0).millisecond(0),
        type: TYPE.TIMESLOT,
      },
      {
        id: 2,
        name: '3PM - 6PM',
        fromTime: moment().hour(15).minute(0).second(0).millisecond(0),
        toTime: moment().hour(18).minute(0).second(0).millisecond(0),
        type: TYPE.TIMESLOT,
      },
      {
        id: 3,
        name: '6PM - 10PM',
        fromTime: moment().hour(18).minute(0).second(0).millisecond(0),
        toTime: moment().hour(22).minute(0).second(0).millisecond(0),
        type: TYPE.TIMESLOT,
      },
    ];

    const self = {
      TYPE: TYPE,
      TIMEWINDOWS: TIMEWINDOWS,

      toOptions: function toOptions(types) {
        let filteredTimeWindows = TIMEWINDOWS;
        if (_.size(types)) {
          filteredTimeWindows = _.filter(TIMEWINDOWS, timewindow =>
            _.indexOf(types, timewindow.type) >= 0
          );
        }

        return _(filteredTimeWindows)
          .map(timewindow => ({ displayName: timewindow.name, value: timewindow.id }))
          .sortBy('value')
          .value();
      },

      getTimewindowById: timewindowId =>
        _.find(TIMEWINDOWS, tw => tw.id === timewindowId) || null,

      getTimeWindowId: (transaction) => {
        const startTime = nvDateTimeUtils.toMoment(
          transaction.startTime, nvTimezone.getOperatorTimezone()
        );
        const endTime = nvDateTimeUtils.toMoment(
          transaction.endTime, nvTimezone.getOperatorTimezone()
        );

        const timewindow = _.find(TIMEWINDOWS, (tw) => {
          const isFromTimeMatch = startTime.hour() === tw.fromTime.hour();
          const isEndTimeMatch = endTime.hour() === tw.toTime.hour();
          return isFromTimeMatch && isEndTimeMatch;
        });
        return _.get(timewindow, 'id');
      },
    };

    return self;
  }
}());
