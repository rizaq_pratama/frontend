(function() {
    'use strict';

    angular
        .module('nvCommons.models')
        .factory('DriverReport', DriverReport);

    DriverReport.$inject = ['$http', 'nvRequestUtils', 'nvDateTimeUtils', 'Excel', '$rootScope', 'nvTimezone'];

    function DriverReport($http, nvRequestUtils, nvDateTimeUtils, Excel, $rootScope, nvTimezone) {

        return {

            generateCSV: generateCSV,

            generateExcelReport: generateExcelReport,

        };

        function generateCSV (fromDate, toDate) {
            var param = { start_date: fromDate.format('Y-MM-DD'),
                          end_date: toDate.format('Y-MM-DD') };
            return Excel.download('core/drivers/salarycsv?' + nvRequestUtils.queryParam(param), 'driversalaries.zip');
        }

        function generateExcelReport (fromDate, toDate) {
            var param = {  startStr: nvDateTimeUtils.displayDateTime(nvDateTimeUtils.toSOD(fromDate, nvTimezone.getOperatorTimezone()), 'utc'),
                           endStr: nvDateTimeUtils.displayDateTime(nvDateTimeUtils.toEOD(toDate, nvTimezone.getOperatorTimezone()), 'utc')
                       };
            var queryParam = nvRequestUtils.queryParam(param);
            var fileName = 'DriverRouteXLReport(' + fromDate + '-' + toDate + ').xls';

            return $http.get('core/reports/newgetdriverroutereportxl?' + queryParam).then(
                success,
                nvRequestUtils.nvFailure
            );

            function success() {
                var param = {  startStr: fromDate.format('Y-MM-DD'),
                               endStr: toDate.format('Y-MM-DD') };
                var queryParam = nvRequestUtils.queryParam(param);
                return Excel.download('core/reports/getdriverroutereportxl?' + queryParam, fileName);
            }
        }

    }

})();
