(function directive() {
  angular
    .module('nvCommons.models')
    .factory('ThirdPartyShipper', ThirdPartyShipper);

  ThirdPartyShipper.$inject = ['$http', 'nvRequestUtils', 'nvModelUtils'];

  function ThirdPartyShipper($http, nvRequestUtils, nvModelUtils) {
    const fields = {
      id: { displayName: 'commons.id' },
      code: { displayName: 'commons.model.code' },
      name: { displayName: 'commons.model.name' },
      url: { displayName: 'commons.model.url' },
    };

    return {
      FIELDS: fields,

      read: () =>
        $http.get('core/thirdpartyshippers')
          .then(nvRequestUtils.success, nvRequestUtils.nvFailure),

      create: data =>
        $http.post('core/thirdpartyshippers', data)
          .then(nvRequestUtils.success, nvRequestUtils.nvFailure),

      update: data =>
        $http.put(`core/thirdpartyshippers/${data.id}`, data)
          .then(nvRequestUtils.success, nvRequestUtils.nvFailure),

      delete: data =>
        $http.delete(`core/thirdpartyshippers/${data.id}`)
          .then(nvRequestUtils.success, nvRequestUtils.nvFailure),

      getAddFields: () =>
        nvModelUtils.pickFields(fields, ['id', 'name', 'code', 'url'],
          { setDefaultValues: true }),

      getEditFields: model =>
        nvModelUtils.pickFields(fields, ['id', 'name', 'code', 'url'],
          { setValues: true, model: model }),
    };
  }
}());
