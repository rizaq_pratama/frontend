
(function directive() {
  angular
    .module('nvCommons.models')
    .factory('ThirdPartyOrder', ThirdPartyOrder);

  ThirdPartyOrder.$inject = ['$http', 'nvRequestUtils', 'nvExtendUtils', 'nvModelUtils', 'nvUtils',
                              'nvDateTimeUtils', 'nvTimezone', '$rootScope', 'Order'];

  function ThirdPartyOrder($http, nvRequestUtils, nvExtendUtils, nvModelUtils, nvUtils,
                            nvDateTimeUtils, nvTimezone, $rootScope, Order) {
    const fields = {
      id: { displayName: 'commons.id' },
      status: { displayName: 'container.third-party-order.order-status' },
      trackingId: { displayName: 'commons.model.tracking-id' },
      thirdPartyOrder: {
        _deep: true,
        id: { displayName: 'commons.id', options: [] },
        _momentOnBoardedDateTime: { displayName: 'commons.model.third-party-onboarding-time' },
        daySinceTransferred: { displayName: 'commons.model.working-day-since-transferred' },
        thirdPartyTrackingId: { displayName: 'commons.model.third-party-tracking-id' },
        thirdPartyShipper: {
          _deep: true,
          id: { displayName: 'commons.id', options: [], optionsMap: {} },
          name: { displayName: 'commons.model.third-party-provider' },
        },
      },
    };

    return {
      FIELDS: fields,

      read: () =>
        $http.get('core/thirdpartyorders')
          .then(nvRequestUtils.success, nvRequestUtils.nvFailure)
          .then(extend),

      uploadCSV: data =>
        $http.upload('core/thirdpartyorders', data)
          .then(nvRequestUtils.success, nvRequestUtils.nvFailure)
          .then(mapServerBeanToLocalData, angular.noop),

      uploadSingle: data =>
        $http.post('core/thirdpartyorders', data)
          .then(nvRequestUtils.success, nvRequestUtils.nvFailure)
          .then(mapServerBeanToLocalData),

      update: data =>
        $http.put('core/thirdpartyorders', data)
          .then(nvRequestUtils.success, nvRequestUtils.nvFailure)
          .then(mapServerBeanToLocalData),

      untag: data => {
        return $http({ method: 'DELETE', url: 'core/thirdpartyorders', data: data, headers: { 'Content-Type': 'application/json; charset=UTF-8' } })
          .then(nvRequestUtils.success, nvRequestUtils.nvFailure)
          .then(mapServerBeanToLocalData);
      },

      setThirdPartyOptions: (options) => {
        fields.thirdPartyOrder.thirdPartyShipper.id.options = _.map(options, shipper => ({
          displayName: shipper.name,
          value: shipper.id }));
        fields.thirdPartyOrder.thirdPartyShipper.id.optionsMap =
          _.keyBy(fields.thirdPartyOrder.thirdPartyShipper.id.options, 'value');
      },

      getAddFields: () =>
        nvModelUtils.pickFields(fields, ['trackingId', 'thirdPartyOrder'],
          { setDefaultValues: true }),

      getEditFields: model =>
        nvModelUtils.pickFields(fields, ['id', 'trackingId', 'thirdPartyOrder'],
          { setValues: true, model: model }),
    };

    function extend(data) {
      return _.map(data, (order) => {
        order.thirdPartyOrder = order.thirdPartyOrders[0];
        return extendOrder(order);
      });
    }

    function extendOrder(order) {
      order.thirdPartyOrder = nvExtendUtils.addMoment(order.thirdPartyOrder, 'onBoardedDateTime', nvTimezone.getOperatorTimezone());
      order.thirdPartyOrder.daySinceTransferred = nvDateTimeUtils.dayBetweenDates(moment(),
                                                   order.thirdPartyOrder._momentOnBoardedDateTime);
      return order;
    }

    function mapServerBeanToLocalData(datas) {
      return _.map(datas, data => extendOrder({
        id: data.orderId,
        status: data.orderStatus,
        trackingId: data.trackingId,
        thirdPartyOrder: {
          id: data.thirdPartyOrderId,
          thirdPartyTrackingId: data.thirdPartyTrackingId,
          onBoardedDateTime: data.onBoardedDateTime,
          thirdPartyShipper: {
            id: data.thirdPartyShipperId,
            name: get3plShipperName(data.thirdPartyShipperId),
          },
        },
        resultStatus: data.status,
      }));

      function get3plShipperName(shipperId) {
        const shipperValue = fields.thirdPartyOrder.thirdPartyShipper.id.optionsMap[shipperId];
        return (shipperValue || {}).displayName;
      }
    }
  }
}());
