(function driverTypeModel() {
  angular
        .module('nvCommons.models')
        .factory('DriverType', DriverType);

  DriverType.$inject = ['$http', 'nvModelUtils', 'nvRequestUtils'];

  function DriverType($http, nvModelUtils, nvRequestUtils) {
    const fields = {
      id: { displayName: 'ID' },
      name: { displayName: 'Name' },
      createdAt: { displayName: 'Created At' },
      updatedAt: { displayName: 'Updated At' },
      deletedAt: { displayName: 'Deleted At' },
    };

    return {

      searchAll: function searchAll(options) {
        const params = _.defaults(options || {}, {
          refresh: false, // set true to reload Hazelcast map
        });

        return $http.get(`driver/1.0/driver-types${(params.refresh ? '?refresh=true' : '')}`)
          .then(nvRequestUtils.nvSuccess, nvRequestUtils.nvFailure);
      },

      createOne: function createOne(data) {
        return $http.post('driver/2.0/driver-types', { driverType: data })
          .then(nvRequestUtils.nvSuccess, nvRequestUtils.nvFailure)
          .then(_.partial(mergeSuccessResponse, data));
      },

      updateOne: function updateOne(id, data) {
        return $http.put(`driver/2.0/driver-types/${id}`, { driverType: data })
          .then(nvRequestUtils.nvSuccess, nvRequestUtils.nvFailure)
          .then(_.partial(mergeSuccessResponse, data));
      },

      deleteOne: function deleteOne(id) {
        return $http.delete(`driver/2.0/driver-types/${id}`)
          .then(nvRequestUtils.nvSuccess, nvRequestUtils.nvFailure);
      },

      toOptions: function toOptions(data, valueField = 'name') {
        return _(data.driverTypes || data)
          .map(driverType => ({
            displayName: driverType.name,
            value: driverType[valueField],
            _sortBy: driverType.id }))
          .sortBy('_sortBy')
          .value();
      },

      toFilterOptions: function toFilterOptions(data) {
        return _.map(data.driverTypes, driverType => ({
          id: driverType.id,
          displayName: driverType.name,
          lowercaseName: driverType.name.toLowerCase(),
        }));
      },

      toSelectOptions: function toSelectOptions(data) {
        return _(data.driverTypes)
                    .map(driverType => ({
                      displayName: _.lowerCase(driverType.name),
                      value: driverType.id,
                    }))
                    .sortBy('displayName')
                    .value();
      },

      getAddFields: function getAddFields() {
        return nvModelUtils.pickFields(fields, ['name']);
      },

      getEditFields: function getEditFields(model) {
        return nvModelUtils.pickFields(fields, ['id', 'name'], { setValues: true, model: model });
      },

    };

    function mergeSuccessResponse(oldData, newData) {
      return _.merge(oldData, newData);
    }
  }
}());
