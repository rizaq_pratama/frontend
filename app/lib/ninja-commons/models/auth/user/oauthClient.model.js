(function model() {
  angular
        .module('nvCommons.models')
        .factory('OauthClient', OauthClient);

  OauthClient.$inject = ['$http', 'nvRequestUtils', 'nvModelUtils'];

  function OauthClient($http, nvRequestUtils, nvModelUtils) {
    const grantTypeOptions = [
            { id: 1, displayName: 'Google', value: 'GOOGLE_SSO', lowercaseName: 'google' },
            { id: 2, displayName: 'Password', value: 'PASSWORD', lowercaseName: 'password' },
            { id: 3, displayName: 'Client Credentials', value: 'CLIENT_CREDENTIALS', lowercaseName: 'client credentials' },
    ];

    const statusOptions = [
            { id: 1, displayName: 'Provisioned', value: 'PROVISIONED', lowercaseName: 'provisioned' },
            { id: 2, displayName: 'Linked', value: 'LINKED', lowercaseName: 'linked' },
            { id: 3, displayName: 'Registered', value: 'REGISTERED', lowercaseName: 'registered' },
            { id: 3, displayName: 'Invited', value: 'INVITED', lowercaseName: 'invited' },
            { id: 3, displayName: 'Revoked', value: 'REVOKED', lowercaseName: 'revoked' },
    ];

    const countryOptions = [
      'sg',
      'my',
      'id',
      'vn',
      'th',
      'ph',
    ];

    const applicationCodeOptions = [
            { displayName: 'DP', value: 'DPMS' },
            { displayName: 'Driver', value: 'DRIVER' },
            { displayName: 'Shipper', value: 'SHIPPER' },
    ];

    const fields = {
      id: { displayName: 'Auth Id', type: 'number' },
      service: { displayName: 'Service', type: 'select' },
      firstName: { displayName: 'First Name', type: 'text' },
      lastName: { displayName: 'Last Name', type: 'text' },
      status: { displayName: 'Account status', type: 'options', options: statusOptions },
      email: { displayName: 'Email', type: 'text' },
      grantType: { displayName: 'Grant Type', type: 'options', options: grantTypeOptions },
      country: { displayName: 'Country', type: 'options', options: countryOptions },
      applicationCode: { displayName: 'Application Code', type: 'options', options: applicationCodeOptions },
      applicationUserId: { displayName: 'Application User Id', type: 'number' },
      role: { displayName: 'Roles', type: 'options' },
    };

    const self = {

      getAddFields: function getAddFields() {
        return nvModelUtils.pickFields(fields, [
          'id', 'firstName', 'lastName', 'grantType', 'email', 'country',
          'applicationCode', 'applicationUserId',

        ], { setDefaultValues: true });
      },

      readOneById: function readOneById(id) {
        const url = `auth/2.0/users/${id}`;
        return $http.get(url).then(nvRequestUtils.success, nvRequestUtils.nvFailure);
      },

      readAll: function readAll() {
        const url = 'auth/users';
        return $http.get(url).then(nvRequestUtils.success, nvRequestUtils.nvFailure);
      },

      readAllByFilter: function readAllByFilter(page, noOfRecords, keyword, grantTypes, statuses) {
        const data = {
          keyword: keyword,
          grantTypes: grantTypes.join(),
          statuses: statuses.join(),
        };
        const url = `auth/2.0/users/${page}/${noOfRecords}?${nvRequestUtils.queryParam(data)}`;
        return $http.get(url).then(nvRequestUtils.success, nvRequestUtils.nvFailure);
      },

      createOne: function createOne(data) {
        const url = 'auth/2.0/users';
        return $http.post(url, data).then(nvRequestUtils.success, nvRequestUtils.nvFailure);
      },

      updateOne: function updateOne(data) {
        const url = 'auth/2.0/users';
        return $http.put(url, data).then(nvRequestUtils.success, nvRequestUtils.nvFailure);
      },

      activateUser: function activateUser(data) {
        const url = 'auth/2.0/users/activate';
        return $http.put(url, data).then(nvRequestUtils.success, nvRequestUtils.nvFailure);
      },

      deactivateUser: function deactivateUser(data) {
        const url = 'auth/2.0/users/de-activate';
        return $http.put(url, data).then(nvRequestUtils.success, nvRequestUtils.nvFailure);
      },

      forceLogout: function forceLogout(data) {
        const url = 'auth/2.0/users/force-logout';
        return $http.put(url, data).then(nvRequestUtils.success, nvRequestUtils.nvFailure);
      },

      /**
       * payload:
       * oauthClientId: @type {Number}
       */
      resendInvitations: payload => $http.post('auth/resendinvite', payload),
    };
    return self;
  }
}());
