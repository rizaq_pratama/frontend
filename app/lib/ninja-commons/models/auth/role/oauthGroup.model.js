(function model() {
  angular
        .module('nvCommons.models')
        .factory('OauthGroup', Group);

  Group.$inject = ['$q', '$http', 'nvRequestUtils', 'nvModelUtils'];

  function Group($q, $http, nvRequestUtils, nvModelUtils) {
    const fields = {
      id: { displayName: 'ID', type: 'number' },
      name: { displayName: 'Role Name', type: 'text' },
      description: { displayName: 'Description(Optional)', type: 'text' },
    };

    const self = {

      getAddFields: function getAddFields() {
        return nvModelUtils.pickFields(fields, [
          'id', 'name', 'description', 'scopes',
        ], { setDefaultValues: true });
      },

      readAll: function readAll() {
        const url = 'auth/usergroups';
        return $http.get(url).then(nvRequestUtils.success, nvRequestUtils.nvFailure);
      },

      readAllWithScopesCountAndGroupsCount: function readAllWithScopesCountAndGroupsCount() {
        const url = 'auth/2.0/usergroups';
        return $http.get(url).then(nvRequestUtils.success, nvRequestUtils.nvFailure);
      },

      readOneById: function readOneById(id) {
        const url = `auth/2.0/usergroups/${id}`;
        return $http.get(url).then(nvRequestUtils.success, nvRequestUtils.nvFailure);
      },

      toFilterOptions: function toFilterOptions(data) {
        return _.map(data, group =>
            ({
              id: group.id,
              displayName: group.name,
              lowercaseName: group.name.toLowerCase(),
            })
        );
      },

            // @TODO to update the endpoint
      forceLogoutRole: function forceLogoutRole(data) {
        const url = 'auth/forcelogoutgroup';
        return $http.post(url, data).then(nvRequestUtils.success, nvRequestUtils.nvFailure);
      },

      deleteRole: function deleteRole(id) {
        return $http.delete(`auth/usergroups/${id}`);
      },

      readAllScopes: function readAllScopes() {
        const url = 'auth/scopes';
        return $http.get(url).then(nvRequestUtils.success, nvRequestUtils.nvFailure);
      },

      createOne: function createOne(data) {
        const url = 'auth/2.0/usergroups';
        return $http.post(url, data).then(nvRequestUtils.success, nvRequestUtils.nvFailure);
      },

      updateOne: function updateOne(id, data) {
        const url = `auth/2.0/usergroups/${id}`;
        return $http.put(url, data).then(nvRequestUtils.success, nvRequestUtils.nvFailure);
      },

      getServiceIds: function getServiceIds() {
        const url = 'auth/serviceIds';
        return $http.get(url).then(nvRequestUtils.success, nvRequestUtils.nvFailure);
      },

      readOneByIdFilterByServiceId: function readOneByIdFilterByServiceId(id, serviceIds) {
        const data = {
          serviceIds: serviceIds.toLowerCase(),
        };
        const url = `auth/2.0/usergroups/${id}?${nvRequestUtils.queryParam(data)}`;
        return $http.get(url).then(nvRequestUtils.success, nvRequestUtils.nvFailure);
      },

      readScopesByGroupId: function readScopesByGroupId(groupId) {
        const url = `auth/scopes/${groupId}`;
        return $http.get(url).then(nvRequestUtils.success, nvRequestUtils.nvFailure);
      },

    };
    return self;
  }
}());
