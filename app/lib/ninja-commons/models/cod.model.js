(function model() {
  angular
    .module('nvCommons.models')
    .factory('COD', COD);

  COD.$inject = ['$http', 'nvRequestUtils', 'nvExtendUtils', 'nvModelUtils', 'nvDateTimeUtils', 'nvTimezone'];

  function COD($http, nvRequestUtils, nvExtendUtils, nvModelUtils, nvDateTimeUtils, nvTimezone) {
    /* jshint unused:false*/
    const fields = {
      id: { displayName: 'commons.id' },
      routeId: { displayName: 'commons.model.route-id' },
      amountCollected: { displayName: 'commons.model.amount-collected' },
      receiptNo: { displayName: 'commons.model.receipt-number' },
      inboundedBy: { displayName: 'commons.model.inbounded-by' },
    };

    return {
      getAll: (from, to) => {
        const dateRange = {
          start: nvDateTimeUtils.displayDateTime(nvDateTimeUtils.toSOD(from, nvTimezone.getOperatorTimezone()), 'utc'),
          end: nvDateTimeUtils.displayDateTime(nvDateTimeUtils.toEOD(to, nvTimezone.getOperatorTimezone()), 'utc'),
        };
        return $http.get(`core/codinbounds?${nvRequestUtils.queryParam(dateRange)}`).then(
                    nvRequestUtils.success,
                    nvRequestUtils.nvFailure
                );
      },
      getById: id =>
        $http.get(`core/codinbounds/${id}`).then(
          nvRequestUtils.success,
          nvRequestUtils.nvFailure
        ),
      create: cod =>
        $http.post('core/codinbounds', cod).then(
          nvRequestUtils.success,
          nvRequestUtils.nvFailure
        ),
      update: cod =>
        $http.put(`core/codinbounds/${cod.id}`, _.omit(cod, 'id')).then(
          nvRequestUtils.success,
          nvRequestUtils.nvFailure
        ),
      delete: cod =>
        $http.delete(`core/codinbounds/${cod.id}`).then(
          nvRequestUtils.success,
          nvRequestUtils.nvFailure
        ),

      // //////////////////////////////////////

      FIELDS: fields,

      getAddFields: () =>
        nvModelUtils.pickFields(fields, ['routeId', 'amountCollected', 'receiptNo']),

      getEditFields: editModel =>
        nvModelUtils.pickFields(fields, [
          'id', 'routeId', 'amountCollected', 'receiptNo',
        ], { setValues: true, model: editModel }),
    };
  }
}());
