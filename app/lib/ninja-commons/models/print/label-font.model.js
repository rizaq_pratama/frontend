(function model() {

  angular
    .module('nvCommons.models')
    .factory('LabelFont', LabelFont);

  function LabelFont() {
    const fontOptions = [
      {
        value: 1,
        displayName: 'English',
      },
      {
        value: 2,
        displayName: 'Thai',
      },
      {
        value: 3,
        displayName: 'Vietnamese',
      }
    ];

    return {
      getOptions: function getOptions() {
        return fontOptions;
      },

      getZpl: function getZpl(value, dimensions) {
        let zpl = '';
        // Default font
        if (value === 1) {
          zpl += '^AHN,';
        }
        // Specify special encoding
        else {
          zpl += '^A@N,';
        }

        // dimensions = [w,h]
        zpl += dimensions.join(',');

        // font to use
        if (value === 2) {
          zpl += ',E:THAI.FNT^FH';
        }
        else if (value === 3) {
          zpl += ',E:VIET.FNT^FH';
        }

        return zpl;
      }
    }
  }
})();