/* eslint-disable no-bitwise */
(function factory() {
  angular
    .module('nvCommons.models')
    .factory('Printer', Printer);

  Printer.$inject = ['$http', 'nvRequestUtils', 'nvModelUtils', '$q', '$rootScope', 'nvFileUtils'];
  function Printer($http, nvRequestUtils, nvModelUtils, $q, $rootScope, nvFileUtils) {
    const fields = {
      id: { displayName: 'ID', type: 'number' },
      name: { displayName: 'Printer Name', type: 'text' },
      ip_address: { displayName: 'IP Address', type: 'text' },
      version: { displayName: 'Version', type: 'number', defaultValue: 1 },
      is_default: { displayName: 'Is Default Printer?', type: 'switch', defaultValue: false },
      created_at: { displayName: 'Created On', type: 'date' },
      updated_at: { displayName: 'Updated On', type: 'date' },
      deleted_at: { displayName: 'Deleted On', type: 'date' },
    };

    const self = {

      /**
       * payload:
       * { "id":11453,
       *   "name":"maybe i'm foolish, maybe i'm blind",
       *   "ip_address":"172.30.5.7:8080",
       *   "version":1,
       *   "is_default":true }
       *
       * return promise that contains single printer
      */
      create: payload =>
        $http.post('core/printers/create', payload)
          .then(nvRequestUtils.success, nvRequestUtils.nvFailure),

      /**
       * return promise that contains array of printer
       */
      readAll: (options = {}) =>
        $http.get(`core/printers/get-all?${nvRequestUtils.queryParam(options)}`)
          .then(nvRequestUtils.success, nvRequestUtils.nvFailure),

      /**
       * payload:
       * { "id":11453,
       *   "name":"maybe i'm foolish, maybe i'm blind",
       *   "ip_address":"172.30.5.7:8080",
       *   "version":1,
       *   "is_default":true }
       * return promise that contains single printer
       */
      update: (id, payload) =>
        $http.put(`core/printers/${id}`, payload).then(
          nvRequestUtils.success, nvRequestUtils.nvFailure),

      /**
       * return promise that contains single printer
       */
      delete: id =>
        $http.delete(`core/printers/${id}`)
          .then(nvRequestUtils.success, nvRequestUtils.nvFailure),

      /**
       * return promise that contains single printer
       */
      getDefaultPrinter: function getDefaultPrinter() {
        return $http.get('core/printers/get-default')
          .then(nvRequestUtils.success, nvRequestUtils.nvFailure);
      },

      printByPost: function printByPost(data, printer) {
        return $q
          .all([
            angular.isUndefined(printer) ? getPrinter() : printer,
          ])
          .then((response) => {
            const thePrinter = response[0];

            _.forEach(data, (row) => {
              _.forEach(row, (value, key) => {
                if (row[key] === undefined || row[key] === null) {
                  row[key] = '';
                }
              });
            });

            let requestHeader = null;
            if (thePrinter.version === 3) {
              // send in NVCOUNTRY header if printer version is 3
              requestHeader = {
                headers: {
                  NVCOUNTRY: $rootScope.domain.toUpperCase(),
                },
              };
            }

            return $http.post(`http://${thePrinter.ip_address}/print`, data, requestHeader).then(
              nvRequestUtils.success,
              nvRequestUtils.nvFailure);
          });
      },

      printStamp: function printStamp(amount, template, printer) {
        return $q.all([
          searchPrinter(printer),
          $http.post('core/thermal-printer/print-stamps', {
            quantity: amount,
            template: template,
          }).then(nvRequestUtils.success, nvRequestUtils.nvFailure),
        ])
          .then((data) => {
            const thePrinter = data[0];
            const payload = data[1];
            const reqPayload = {
              stampPrefix: payload.prefix,
              stampStart: payload.stamp_start_seq_no,
              stampEnd: payload.stamp_end_seq_no,
              digitsLength: payload.digits_length,
              template: payload.template,
            };

            return $http.post(`http://${thePrinter.ip_address}/print`, reqPayload, {
              headers: {
                NVCOUNTRY: $rootScope.domain.toUpperCase(),
              },
            }).then(
              nvRequestUtils.success,
              nvRequestUtils.nvFailure);
          });
      },

      getTrackingIds: function getTrackingIds(quantity, printerTemplateId) {
        return $http
          .post('core/thermal-printer/print-stamps', {
            quantity: quantity,
            printer_template_id: printerTemplateId,
          })
          .then(nvRequestUtils.success, nvRequestUtils.nvFailure)
          .then((data) => {
            const stampPrefixId = data.prefix;
            const stampBegSeqNo = data.stamp_start_seq_no;
            const stampEndSeqNo = data.stamp_end_seq_no;
            const stampDigitsLn = data.digits_length;

            const trackingIds = [];
            for (let i = stampBegSeqNo; i <= stampEndSeqNo; ++i) {
              trackingIds.push(stampPrefixId + _.padStart(i, stampDigitsLn, '0'));
            }

            return trackingIds;
          });
      },

      searchPrinterTemplates: function searchPrinterTemplates() {
        return $http.get('core/thermal-printer/printer-templates')
          .then(nvRequestUtils.success, nvRequestUtils.nvFailure);
      },

      printByQueryParam: function printByQueryParam(order, printerOption) {
        return searchPrinter(printerOption).then(print);

        function print(printer) {
          return $http.get(`http://${printer.ip_address}?
          recipient_name=${order.to_name}
          &address1=${order.to_address1}
          &address2=${order.to_address2}
          &contact=${order.to_contact}
          &barcode=${order.tracking_id}
          &postcode=${order.to_postcode}
          &shipper_name=${order.shipper_name}
          `).then(nvRequestUtils.success, nvRequestUtils.nvFailure);
        }
      },

      printLabel: function printLabel(label, printerOption) {
        return searchPrinter(printerOption).then(print);

        function print(printer) {
          return $http.post(`http://${printer.ip_address}/print-zpl`,
            { zpl: label }).then(
            nvRequestUtils.success,
            nvRequestUtils.nvFailure);
        }
      },
      zplTemplate: function zplTemplate() {
        return `^XA^CI28^LH{{printer_x_offset}},{{printer_y_offset}}
                ^FO10,30{{font[20,10]}}
                ^FB500,8,3,C,0
                ^FH^FD{{recipient_name[uppercase,convert_to_zpl_hex]}}\\&{{address1[uppercase,convert_to_zpl_hex]}}\\&{{address2[uppercase,convert_to_zpl_hex]}}\\&{{postcode[uppercase,convert_to_zpl_hex]}}\\&{{contact[uppercase,convert_to_zpl_hex]}}\\&{{route_id[uppercase,convert_to_zpl_hex]}} {{driver_name[uppercase,convert_to_zpl_hex]}}
                ^FS
                ^FO10,200
                ^BQN,2,5
                ^FDQA,{{barcode[uppercase]}}^FS
                ^FO10,340^AHN,20,10
                ^FD{{barcode[uppercase]}}^FS
                ^FO290,210{{font[20,10]}}
                ^FB600,4,3,L,0
                ^FH^FDSHIPPER:\\&{{shipper_name[uppercase,convert_to_zpl_hex]}}
                ^FS
                ^XZ`;
      },
      getZpl: function getZpl(countryId, dimensions) {
        let zpl = '';
        // Default font
        if (countryId !== 'vn' && countryId !== 'th') {
          zpl += '^AHN,';
        } else {
          zpl += '^A@N,'; // Specify special encoding
        }

        // dimensions = [w,h]
        zpl += dimensions.join(',');
        // font to use
        if (countryId === 'th') {
          zpl += ',E:THAI.FNT^FH';
        } else if (countryId === 'vn') {
          zpl += ',E:VIET.FNT^FH';
        }

        return zpl;
      },

      printByZpl: function printByZpl(data, printerOption) {
        return searchPrinter(printerOption).then(print);

        function print(printer) {
          let printerIp = printer.ip_address;
          if (printerIp && printerIp.substr(-1) !== '/') {
            printerIp += '/';
          }
          const variablesMap = {};
          const template = self.zplTemplate();
          _.forEach(template.match(/{{[^{]+}}/g), (variable) => {
            const expression = variable.replace(/{{([^{]+)}}/, '$1');
            const displayName = expression.replace(/(.+)\[.*\]*/, '$1');
            let value = data[displayName] ? data[displayName] : '';
            let operations = expression.match(/\[([^\]]*)\]/);

            if (operations !== null) {
              operations = operations[1].split(',');
            }

            switch (displayName) {
              case 'printer_x_offset':
                value = 140;
                break;
              case 'printer_y_offset':
                value = 0;
                break;
              case 'font':
                value = 1;
                break;
              default:
                break;
            }
            /*
             * @id: expression to be replaced: e.g. {{font[18,10]}}
             * @displayName: field to be shown: e.g. font
             * @operations: operations to be done on the input-value: e.g. 18, 10, uppercase
             * @value: input-value
             **/
            const property = {
              id: variable,
              displayName: displayName,
              operations: operations,
            };

            // initialize
            if (variablesMap[displayName] == null) {
              variablesMap[displayName] = [];
              variablesMap[displayName].value = value;
            }

            variablesMap[displayName].push(property);
          });
          const label = compileLabel(template, variablesMap);
          return $http.post(`http://${printerIp}print-zpl`, { zpl: label });
        }

        function compileLabel(lab, variablesMap) {
          let label = _.cloneDeep(lab);

          _.forEach(variablesMap, (variableList, key) => {
            const value = variableList.value;

            _.forEach(variableList, (variable) => {
              let evaluatedVar;

              switch (key) {
                case 'font':
                  evaluatedVar = self.getZpl($rootScope.domain, variable.operations);
                  break;

                default:
                  evaluatedVar = doOperations(value, variable.operations);
                  break;
              }

              label = label.replace(variable.id, evaluatedVar);
            });
          });

          return label;
        }

        function doOperations(value, operations) {
          let evaluatedVar = value;

          _.forEach(operations, (op) => {
                // uppercase
            if (op === 'uppercase') {
              evaluatedVar = evaluatedVar.toUpperCase();
            }

                // limit
            if (op.indexOf('limit') !== -1) {
              evaluatedVar = evaluatedVar.substring(0, op.split(':')[1]);
            }

                // convert_to_zpl_hex
            if (op === 'convert_to_zpl_hex') {
              evaluatedVar = convertToZplHex(evaluatedVar);
            }
          });

          return evaluatedVar;
        }

        function toUTF8Array(str) {
          const utf8 = [];
          for (let i = 0; i < str.length; i++) {
            let charcode = str.charCodeAt(i);
            if (charcode < 0x80) {
              utf8.push(charcode);
            } else if (charcode < 0x800) {
              utf8.push(0xc0 | (charcode >> 6),
                            0x80 | (charcode & 0x3f));
            } else if (charcode < 0xd800 || charcode >= 0xe000) {
              utf8.push(0xe0 | (charcode >> 12),
                            0x80 | ((charcode >> 6) & 0x3f),
                            0x80 | (charcode & 0x3f));
            } else { // surrogate pair
              i += 1;
                    // UTF-16 encodes 0x10000-0x10FFFF by
                    // subtracting 0x10000 and splitting the
                    // 20 bits of 0x0-0xFFFFF into two halves
              charcode = 0x10000 + (((charcode & 0x3ff) << 10)
                            | (str.charCodexAt(i) & 0x3ff));
              utf8.push(0xf0 | (charcode >> 18),
                            0x80 | ((charcode >> 12) & 0x3f),
                            0x80 | ((charcode >> 6) & 0x3f),
                            0x80 | (charcode & 0x3f));
            }
          }
          return utf8;
        }

        function convertToZplHex(str) {
          let hex = '';

          _.forEach(str, (char) => {
            if (char === ' ') {
              hex += ' ';
            } else {
              const bytes = toUTF8Array(char);
              _.forEach(bytes, (byte) => {
                hex += `_${byte.toString(16)}`;
              });
            }
          });

          return hex;
        }
      },

      getPreview: function getPreview(label, options) {
        /* density in dpmm
         * width and height in inches
         **/
        options = _.defaults(options || {}, {
          density: 6,
          width: 4,
          height: 4,
        });

        return $http.post(`http://api.labelary.com/v1/printers/${options.density}dpmm/labels/${options.width}x${options.height}/0/`,
          label,
          {
            headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
            responseType: 'arraybuffer',
          }).then(nvRequestUtils.success, nvRequestUtils.nvFailure).then(response =>
            nvFileUtils.getURLFromData(response, nvFileUtils.FILETYPE_PNG));
      },

      getAddFields: function getAddFields() {
        return nvModelUtils.pickFields(fields, [
          'name', 'ip_address', 'version', 'is_default',
        ], { setDefaultValues: true });
      },

      getEditFields: function getEditFields(model) {
        return nvModelUtils.pickFields(fields, [
          'id', 'name', 'ip_address', 'version', 'is_default',
        ], { setValues: true, model: model });
      },

      printScannedOrder: function printScannedOrder(payload) {
        return $http.post('engine/nvoperator/print_scanned_order', payload)
          .then(nvRequestUtils.success, nvRequestUtils.failure);
      },

      searchPrinter: searchPrinter,

    };

    return self;

    function searchPrinter(printerOption) {
      if (angular.isUndefined(printerOption)) {
        if (angular.isDefined(localStorage.printer)) {
          return $q.when(JSON.parse(localStorage.printer));
        }

        return self.getDefaultPrinter();
      }
      return $q.when(printerOption);
    }

    function getPrinter() {
      return !angular.isUndefined(localStorage.printer)
        ? JSON.parse(localStorage.printer)
        : self.getDefaultPrinter();
    }
  }
}());
