(function() {
    'use strict';

    angular
        .module('nvCommons.models')
        .factory('PrintTemplate', PrintTemplate);

    PrintTemplate.$inject = ['$http', 'nvRequestUtils', 'nvModelUtils', '$q'];

    function PrintTemplate($http, nvRequestUtils) {

        /* jshint unused: false */
        var fields = {
            id:           { translateKey: 'commons.id',            displayName: 'ID',            type: 'number' },
            templateName: { translateKey: 'commons.template-name', displayName: 'Template Name', type: 'text'   },
            type:         { translateKey: 'commons.type',          displayName: 'Type',          type: 'text'   },
            created_at:   { translateKey: 'commons.created-on',    displayName: 'Created On',    type: 'date'   },
            updated_at:   { translateKey: 'commons.updated-on',    displayName: 'Updated On',    type: 'date'   },
            deleted_at:   { translateKey: 'commons.deleted-on',    displayName: 'Deleted On',    type: 'date'   }
        };

        return {

            read: function(filter) {
                return $http.get('core/thermal-printer/templates' +
                    (filter ? "?filter="+filter : "")).then(
                    nvRequestUtils.success,
                    nvRequestUtils.nvFailure
                );
            }

        };

    }

})();
