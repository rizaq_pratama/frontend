(function service() {
  angular
    .module('nvCommons.services')
    .factory('nvTimezone', nvTimezone);

  nvTimezone.$inject = ['$rootScope', '$http'];

  function nvTimezone($rootScope, $http) {
    const timezonesMap = {
      id: 'Asia/Jakarta',
      mm: 'Asia/Yangon',
      my: 'Asia/Kuala_Lumpur',
      ph: 'Asia/Manila',
      sg: 'Asia/Singapore',
      th: 'Asia/Bangkok',
      vn: 'Asia/Ho_Chi_Minh',
      fef: 'Asia/Singapore',
      hbl: 'Asia/Singapore',
      mbs: 'Asia/Jakarta',
      mnt: 'Asia/Jakarta',
      demo: 'Asia/Jakarta',
      msi: 'Asia/Jakarta',
      mmpg: 'Asia/Yangon',
      tkl: 'Asia/Singapore',
    };

    const timezones = {
      'Asia/Singapore':
      { shortname: 'SG - SST',
        name: 'Singapore Standard Time',
        iana: 'Asia/Singapore',
        utcOffset: 'UTC +08:00',
        country: 'Singapore',
      },
      'Asia/Kuala_Lumpur':
      { shortname: 'MY - MST',
        name: 'Malaysia Standard Time',
        iana: 'Asia/Kuala_Lumpur',
        utcOffset: 'UTC +08:00',
        country: 'Malaysia',
      },
      'Asia/Ho_Chi_Minh':
      { shortname: 'VN - ICT',
        name: 'Indochina Time',
        iana: 'Asia/Ho_Chi_Minh',
        utcOffset: 'UTC +07:00',
        country: 'Vietnam',
      },
      'Asia/Manila':
      { shortname: 'PH - PHT',
        name: 'Philippines Time',
        iana: 'Asia/Manila',
        utcOffset: 'UTC +08:00',
        country: 'Philippines',
      },
      'Asia/Bangkok':
      { shortname: 'TH - ICT',
        name: 'Indochina Time',
        iana: 'Asia/Bangkok',
        utcOffset: 'UTC +07:00',
        country: 'Thailand',
      },
      'Asia/Jayapura':
      { shortname: 'ID - WIT',
        name: 'Eastern Indonesian Time',
        iana: 'Asia/Jayapura',
        utcOffset: 'UTC +09:00',
        country: 'Indonesia',
      },
      'Asia/Makassar':
      { shortname: 'ID - WITA',
        name: 'Indonesian Central Time',
        iana: 'Asia/Makassar',
        utcOffset: 'UTC +08:00',
        country: 'Indonesia',
      },
      'Asia/Jakarta':
      { shortname: 'ID - WIB',
        name: 'Western Indonesian Time',
        iana: 'Asia/Jakarta',
        utcOffset: 'UTC +07:00',
        country: 'Indonesia',
      },
      'Asia/Yangon':
      { shortname: 'MM - MMT',
        name: 'Myanmar Time',
        iana: 'Asia/Yangon',
        utcOffset: 'UTC +06:30',
        country: 'Myanmar',
      },
    };

    return {
      init: init,
      getOperatorTimezone: getOperatorTimezone,
      setOperatorTimezone: setOperatorTimezone,
      setOperatorTimezoneByDomain: setOperatorTimezoneByDomain,
      guessBrowserLocation: guessBrowserLocation,
      getOperatorTimezoneInfo: getOperatorTimezoneInfo,
      getAllTimezones: getAllTimezones,
    };

    function init() {
      if ($rootScope.user) {
        setTimezoneHeader($rootScope.user.timezone);
      } else {
        removeTimezoneHeader();
      }
    }

    function getOperatorTimezone() {
      return $rootScope.user && $rootScope.user.timezone;
    }

    function setOperatorTimezone(timezone) {
      $rootScope.user.timezone = timezone;
      setTimezoneHeader(timezone);
    }

    function setOperatorTimezoneByDomain(domain) {
      setOperatorTimezone(timezonesMap[domain] || 'Asia/Singapore');
    }

    function guessBrowserLocation() {
      return moment.tz.guess();
    }

    function getOperatorTimezoneInfo() {
      return timezones[$rootScope.user.timezone];
    }

    function getAllTimezones() {
      return _.values(timezones);
    }

    function setTimezoneHeader(timezone) {
      $http.defaults.headers.common.timezone = timezone;
    }

    function removeTimezoneHeader() {
      $http.defaults.headers.common = _.omit($http.defaults.headers.common, 'timezone');
    }
  }
}());
