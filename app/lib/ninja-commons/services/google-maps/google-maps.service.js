(function() {
    'use strict';

    angular
        .module('nvCommons.services')
        .factory('nvGoogleMaps', googleMaps);

    googleMaps.$inject = ['$rootScope'];

    function googleMaps($rootScope) {

        var styledMaps = {};

        var self = {

            /**
             * Custom map type constants.
             */
            MapTypeId: {
                NO_POI: 'no_poi',
                ROADS_ONLY: 'roads_only'
            },

            MarkerColor: {
                RED: {text: 'RED', color: '#ba0c2f'},
                GREEN: {text: 'GREEN', color: '#257226'},
                BLUE: {text: 'BLUE', color: '#1d4f91'},
                ORANGE: {text: 'ORANGE', color: '#ffad00'}
            },

            /**
             * Adds the specified style to the given map.
             *
             * @param style - The style to add. See {@code self.MapTypeId}.
             * @param map   - The reference to the Google map instance.
             */
            addStyle: function(style, map) {
                // creates the styled map if it doesn't yet exist in the cache
                if (!styledMaps[style]) {
                    if (style === self.MapTypeId.NO_POI) {
                        styledMaps[style] = noPOIStyledMap();
                    } else if (style === self.MapTypeId.ROADS_ONLY) {
                        styledMaps[style] = roadsOnlyStyledMap();
                    } else {
                        // the styled map type doesn't exist, do nothing
                        return;
                    }
                }
                map.mapTypes.set(style, styledMaps[style]);
            },

            /**
             * Adds a pulsing marker to the given map.
             * See the corresponding google-maps SASS file for the styles.
             *
             * @param map        - The reference to the Google map instance.
             * @param lat        - The latitude of the marker.
             * @param lng        - The longitude of the marker.
             * @param title      - The title displayed when hovering over the marker.
             * @param options    -
             * @returns {Marker} - The Google marker instance.
             */
            addPulsingMarker: function(map, lat, lng, title, options) {
                options = _.defaults(options || {}, {
                    color: self.MarkerColor.RED
                });
                return new google.maps.Marker({
                    position: {
                        lat: lat,
                        lng: lng
                    },
                    icon: {
                        path: google.maps.SymbolPath.CIRCLE,
                        fillColor: options.color.color,
                        fillOpacity: 1.0,
                        scale: 1,
                        strokeColor: options.color.color,
                        strokeOpacity: 1.0,
                        strokeWeight: 4
                    },
                    title: '(NV-Marker-' + options.color.text + ') ' + title,
                    map: map
                });
            },

            clearMarkers: function(markers) {
                _.forEach(markers, function(marker) {
                    marker.setMap(null);
                });
                return [];
            },

            addSearchBox: function(map, markers, options) {
                options = _.defaults(options || {}, {
                    inputElementId: 'google-map-poi-input',
                    placesChangedCallback: angular.noop
                });

                var searchBox = null;
                var input = document.getElementById(options.inputElementId);

                if (input) {
                    // create the search box and link it to the UI element
                    searchBox = new google.maps.places.SearchBox(input);
                    map.controls[google.maps.ControlPosition.TOP_RIGHT].push(input);

                    // bias the search box results towards the current map's viewport
                    map.addListener('bounds_changed', function() {
                        searchBox.setBounds(map.getBounds());
                    });

                    // listen for when the user selects a prediction
                    searchBox.addListener('places_changed', function() {
                        var places = searchBox.getPlaces();

                        if (places.length === 0) {
                            return;
                        }

                        // clear the old markers
                        markers = self.clearMarkers(markers);

                        // create a marker for each new place
                        generateMarkers(places, markers, map);

                        // center the view on the markers
                        fitViewToMarkers(markers, map);

                        // invoke the user callback
                        options.placesChangedCallback(places, markers);
                    });
                }

                return searchBox;
            },

            /**
             * Gets the default latitude and longitude for the given domain.
             *
             * @param domain     - The domain string.
             * @returns {Object} - The latitude and longitude in an object.
             */
            getDefaultLatLngForDomain: function(domain) {
                domain = domain || $rootScope.domain;
                switch (domain) {
                    case 'sg' : return { lat: 1.3597220659709373, lng: 103.82701942695314 };
                    case 'my' : return { lat: 3.1333333,          lng: 101.697806         };
                    case 'id' :
                    case 'mbs':
                    case 'gpi': return { lat: -6.2141988,         lng: 106.8064186        };
                    case 'vn' : return { lat: 10.8046341,         lng: 106.6299261        };
                    default   : return { lat: 1.3,                lng: 103.8311393        };
                }
            },

            /**
             * Gets the default zoom level for the given domain.
             *
             * @param domain     - The domain string.
             * @returns {Number} - The zoom level.
             */
            getDefaultZoomLevelForDomain: function(domain) {
                domain = domain || $rootScope.domain;
                switch (domain) {
                    case 'sg' :
                    case 'id' :
                    case 'mbs':
                    case 'gpi': return 12;
                    case 'my' : return 13;
                    default   : return 11;
                }
            }

        };

        return self;

        function generateMarkers(places, markers, map) {
            _.forEach(places, function(place) {
                markers.push(new google.maps.Marker({
                    map: map,
                    title: place.name,
                    position: place.geometry.location
                }));
            });
            return markers;
        }

        function fitViewToMarkers(markers, map) {
            var bounds = new google.maps.LatLngBounds();

            _.forEach(markers, function(marker) {
                bounds.extend(marker.position);
            });

            map.fitBounds(bounds);

            // zoom in if there's only one marker
            if (markers.length === 1) {
                map.setZoom(17);
            }
        }

        /**
         * A map style with all POIs hidden.
         */
        function noPOIStyledMap() {
            return new google.maps.StyledMapType([{
                featureType: 'poi',
                elementType: 'labels',
                stylers: [{visibility: 'off'}]
            }], {name: 'No POI'});
        }

        /**
         * A map style with only roads brought into focus.
         */
        function roadsOnlyStyledMap() {
            return new google.maps.StyledMapType([{
                featureType: 'road',
                elementType: 'geometry',
                stylers: [{color: '#808080'}]
            }, {
                featureType: 'road',
                elementType: 'labels.text.stroke',
                stylers: [{color: '#000000'}, {weight: 3}]
            }, {
                featureType: 'road',
                elementType: 'labels.text.fill',
                stylers: [{color: '#ffffff'}]
            }, {
                featureType: 'transit.station.rail',
                elementType: 'labels.text.stroke',
                stylers: [{color: '#000000'}, {weight: 3}]
            }, {
                featureType: 'transit.station.rail',
                elementType: 'labels.text.fill',
                stylers: [{color: '#ffffff'}]
            }, {
                featureType: 'transit.line',
                elementType: 'all',
                stylers: [{visibility: 'off'}]
            }, {
                featureType: 'poi',
                elementType: 'all',
                stylers: [{visibility: 'off'}]
            }], {name: 'Roads'});
        }

    }

})();
