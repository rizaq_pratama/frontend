;(function() {
    'use strict';

    angular
        .module('nvCommons.services')
        .factory('nvClientStore', clientStore);

    clientStore.$inject = ['$window', '$rootScope'];

    function clientStore($window, $rootScope) {

        return {

            /**
             * Use the localStorage.
             * See the store function for available methods.
             */
            localStorage: store($window.localStorage),

            /**
             * Use the sessionStorage.
             * See the store function for available methods.
             */
            sessionStorage: store($window.sessionStorage)

        };

        function store(storage) {
            var self = {

                /**
                 * Retrieve the stored value from storage.
                 * By default, the key is namespaced with the current user's id.
                 * @param key - The stored key.
                 * @param options
                 *        options.namespace - Set to false if you don't want to namespace the key.
                 * @returns {*} The stored value.
                 */
                get: function(key, options) {
                    options = _.defaults(options || {}, {namespace: true});
                    var nameSpacedKey = getKey(key, options);
                    return storage[nameSpacedKey];
                },

                /**
                 * Save the value in the storage.
                 * By default, the key is namespaced with the current user's id.
                 * @param key - The stored key.
                 * @param val - The value to store.
                 * @param options
                 *        options.namespace - Set to false if you don't want to namespace the key.
                 * @returns {boolean} True if the value was successfully saved, false otherwise.
                 */
                set: function(key, val, options) {
                    options = _.defaults(options || {}, {namespace: true});
                    try {
                        var nameSpacedKey = getKey(key, options);
                        storage[nameSpacedKey] = val;
                        return true;
                    } catch (e) {
                        console.log('Exception writing to storage', e);
                        return false;
                    }
                },

                /**
                 * Retrieve the stored JSON from storage.
                 * By default, the key is namespaced with the current user's id.
                 * @param key - The stored key.
                 * @param options
                 *        options.namespace - Set to false if you don't want to namespace the key.
                 * @returns {*} The stored value.
                 */
                getJson: function(key, options) {
                    options = _.defaults(options || {}, {namespace: true});
                    var val = self.get(key, options);
                    return val && JSON.parse(val);
                },

                /**
                 * Save the JSON in the storage.
                 * By default, the key is namespaced with the current user's id.
                 * @param key - The stored key.
                 * @param val - The value to store.
                 * @param options
                 *        options.namespace - Set to false if you don't want to namespace the key.
                 * @returns {boolean} True if the value was successfully saved, false otherwise.
                 */
                setJson: function(key, val, options) {
                    options = _.defaults(options || {}, {namespace: true});
                    return self.set(key, val && JSON.stringify(val), options);
                },

                /**
                 * Clear the storage.
                 */
                clear: function() {
                    storage.clear();
                },

                /**
                 * Remove the given key from the storage.
                 * @param key - The stored key.
                 * @param options
                 *        options.namespace - Set to false if you don't want to namespace the key.
                 * @returns {*} The stored value that was just removed.
                 */
                remove: function(key, options) {
                    options = _.defaults(options || {}, {namespace: true});
                    var val = self.get(key, options);
                    var nameSpacedKey = getKey(key, options);
                    delete storage[nameSpacedKey];
                    return val;
                }

            };

            return self;
        }

        function getNamespace() {
            return $rootScope.user && $rootScope.user.thirdPartyId;
        }

        function getKey(key, options) {
            return key + (options.namespace ? '-' + getNamespace() : '');
        }

    }

})();
