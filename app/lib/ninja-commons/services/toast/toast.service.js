(function service() {
/**
 *
 * nvToast
 * A simple wrapper for the toastr library.
 * Requires jQuery to be included in the project.
 * Requires toastr.js and toastr.scss to be included in the project.
 *
 * 4 variant of creating a toast:
 * nvToast.success - showing a success toast
 * nvToast.error - showing an error toast
 * nvToast.info - showing an info toast
 * nvToast.warning - showing a warning toast
 *
 * 4 variants of function arguments:
 * nvToast.success(title)
 * nvToast.success(title, message)
 * nvToast.success(title, options)
 * nvToast.success(title, message, options)
 *
 * @param {string}        title          Title of the toast
 * @param {array|string}  message        Optional, message of the toast, can be an string or array
 *                                       of string, each represents a line in the message
 * @param {object}        options        Optional, options for the toast.
 *   @param {string}      icon           Optional, icon for the toast
 *   @param {string}      buttonTitle    Optional, the title of the button
 *   @param {function}    buttonCallback Optional, the callback function when the button was
 *                                       clicked, eg: function buttonCallback(toast, event){}
 *
 * @return {element} toast
 *
 * Meaning behind the colors
 * credit: Lucian
 *
 * Success (Green)
 *  - Creation of item
 *  - Show success of an operation
 *
 * Info (Blue)
 *  - Notify and inform the user
 *
 * Error (Yellow)
 *  - Displaying errors, ie. server errors
 *
 * Warning (Red)
 *  - Cancelation, Deletion, Removal of item
 *
 */
  angular
    .module('nvCommons.services')
    .factory('nvToast', nvToastService);

  nvToastService.$inject = ['$window'];

  function nvToastService($window) {
    if (!$window.toastr) {
      return console.error('toast.service.js',
        'toastr is not defined, have you included the lib files?');
    }

    // custom toastr options override
    $window.toastr.options.timeOut = 5000;
    $window.toastr.options.extendedTimeOut = 5000;
    $window.toastr.options.progressBar = true;

    const defaultIcon = {
      error: 'error',
      info: 'info',
      success: 'done',
      warning: 'warning',
    };
    const theme = {
      error: 'md-nvYellow-theme',
      info: 'md-nvBlue-theme',
      success: 'md-nvGreen-theme',
      warning: 'md-nvRed-theme',
    };

    const nvToast = {};

    nvToast.error = error;
    nvToast.info = info;
    nvToast.success = success;
    nvToast.warning = warning;
    nvToast.remove = $window.toastr.remove;
    nvToast.clear = $window.toastr.clear;

    return nvToast;

    // ///////////////////////

    function error(...args) {
      return showToast('error', args);
    }

    function info(...args) {
      return showToast('info', args);
    }

    function success(...args) {
      return showToast('success', args);
    }

    function warning(...args) {
      return showToast('warning', args);
    }

    function showToast(type, arg) {
      const options = {};
      if (arg.length >= 1) {
        options.title = arg[0];
      }
      if (arg.length >= 2) {
        if (angular.isString(arg[1])) {
          options.message = [arg[1]];
        } else if (angular.isArray(arg[1])) {
          options.message = arg[1];
        } else {
          options.options = arg[1];
        }
      }
      if (arg.length >= 3) {
        options.options = arg[2];
      }
      return showToastr(type, options);
    }

    function showToastr(type, { title = '', message = [], options = {} }) {
      const icon = options.icon || defaultIcon[type];
      // add button
      const toastMessage = createMessage(type, icon, title, message, options.buttonTitle);
      const toast = $window.toastr[type](toastMessage, '', options);
      if (angular.isDefined(options.buttonCallback)) {
        const button = toast.eq(0).find('button');
        button[button.length - 1].onclick = closeToast(toast);
        if (button.length > 1) {
          button[0].onclick = (event) => { options.buttonCallback(toast, event); };
        }
      }
      return toast;
    }

    function createMessage(type, icon, title, message, button) {
      const toastMessage = wrapDiv(wrapDiv(title, '') +
                            createButton(type, button) +
                            closeButton(type), 'toast-top') +
                           wrapDiv(joinMessage(message), 'toast-bottom');
      return `<i class="material-icons">${icon}</i>${wrapDiv(toastMessage, 'toast-right')}`;
    }

    function createButton(type, title) {
      return angular.isUndefined(title) ?
        '' :
        `<button class="dense raised dark md-button nv-button ${theme[type]}">${title}</button>`;
    }

    function closeButton(type) {
      return `<button class="nv-button md-button p dark flat dense ${theme[type]}"><i class="material-icons">close</i></button>`;
    }

    function wrapDiv(text, clz) {
      return (text === '') ? '' : `<div class="${clz}">${text}</div>`;
    }

    function joinMessage(message) {
      return message.join('<br />');
    }

    function closeToast(toast) {
      return (event) => {
        // force clear the toast
        // regardless of the toast is with focus or not
        $window.toastr.clear(toast, { force: true });
      };
    }
  }
}());
