(function customToast() {
  /**
   *  Patch nvToast with custom with undo function
   *
   * eg: nvToast.custom.withAction(undo).success(message, title, options);
   */
  angular
    .module('nvCommons.services')
    .config(PatchNVToastWithAction);

  PatchNVToastWithAction.$inject = ['$provide'];
  function PatchNVToastWithAction($provide) {
    $provide.decorator('nvToast', extendNVToast);
  }

  extendNVToast.$inject = ['$delegate'];
  function extendNVToast($delegate) {
    // nvToast
    const nvToast = $delegate;

    // add custom undo method
    nvToast.custom = angular.isDefined(nvToast.custom) ? nvToast.custom : {};
    nvToast.custom.withAction = withAction;

    return nvToast;

    // /////////////////////

    function withAction(callback, text = 'Undo Changes') {
      return {
        success: wrapAction('success', callback),
        info: wrapAction('info', callback),
        warning: wrapAction('warning', callback),
        error: wrapAction('error', callback),
      };

      function wrapAction(type, callbackFn) {
        return function toast(title, message, options) {
          // decorate toast
          const theMessage = angular.isDefined(message) ? message : '';
          const theOptions = angular.isDefined(options) ? options : {};
          theOptions.buttonTitle = text;
          theOptions.buttonCallback = callbackFn;

          // send toast
          return nvToast[type](title, theMessage, theOptions);
        };
      }
    }
  }
}());
