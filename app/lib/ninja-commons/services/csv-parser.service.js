;(function() {
    'use strict';

    angular
        .module('nvCommons.services')
        .factory('nvCsvParser', nvCsvParser);

    nvCsvParser.$inject = ['$window', '$q'];

    function nvCsvParser($window, $q) {
        
        var Papa = $window.Papa;
        /** 
         * var config = { complete: fn, error: fn}
         * Papa.parse(file, config);
         * 
         * https://github.com/mholt/PapaParse
         * http://papaparse.com/docs#config
        */

        return {
            /**
             * input can be one of:
             *      csv file
             *      csv string
             *      csv file from url
             * @param input - csv input
             * @param config - config object, must include complete field
             * for additional config, please check the website
             * returns promise
             */
            parse: function(input, config) {
                var deferred = $q.defer();
                config = config || {};
                config.complete = function onSuccess(result){
                    if (config.rejectOnError && result.errors.length) {
                        deferred.reject(result);
                        return;
                    }
                    deferred.resolve(result);
                };

                config.error = function onError(error) {
                    console.error(error.message);
                    deferred.reject(error);
                };

                Papa.parse(input, config);
                return deferred.promise;
            },

            /**
             * data can be one of:
             *      An array of arrays
             *      An array of objects
             *      An object explicitly defining fields and data
             * @param data - data to be converted into csv
             * @param config - config object, must include complete field
             * for additional config, please check the website'
             * returns promise
             */
            unparse: function(data, config) {
                config = config || {};
                try {
                    return $q.resolve(Papa.unparse(data, config));
                } catch (err) {
                    return $q.reject(err);
                }
            },
            unparseInstant: function(data, config = {}) {
                try {
                    return Papa.unparse(data, config);
                } catch (err) {
                    return '';
                }
            }
        };
    }
})();