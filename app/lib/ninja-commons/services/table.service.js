((function service() {
  angular
        .module('nvCommons.services')
        .factory('nvTable', nvTable);

  nvTable.$inject = ['nvUtils'];
  /* eslint no-underscore-dangle: ["error", { "allow": ["_isNew"] }]*/
  function nvTable(nvUtils) {
    return {
      /**
       * Get nvTableParams for nvTable
       * @param {object} fields model description object, available in most *.model.js
       * @return {object} nvTableParams
       */
      createTable: createTable,

      /**
       * Create a basic sort function
       * @param {string} columnName eg: 'firstName', 'driver.firstName', 'route.waypoint.id'
       * @type {function} function with method signature function(datas, order),
       *                  which will sort the `datas` based on the columnName provided
       *                  in the specified order and `data.id` in ascending order
       */
      getBasicSortFunction: getBasicSortFunction,

      getBasicFilterFunction: getBasicFilterFunction,

      /**
       * Default buffer size for the lazy-loading table
       * @type {Number} 30
       */
      DEFAULT_BUFFER_SIZE: 30,
    };

    function createTable(fields = {}, { newItemOnTop = false, isStripped = true,
        onSelectItem = angular.noop, onDeselectItem = angular.noop, totalItems = -1,
        showEndTableDescription = true } = {}) {
      const table = {};
      table.data = [];
      table.columns = createColumns(fields);
      table.tableData = table.data;
      table.selection = new Set();
      table.isShownAllSelected = false;
      table.localColumnFilter = {};
      table.highlightFn = null;
      table.showEndTableDescription = showEndTableDescription;
      // sorts
      table.currSortIdx = 0;
      table.currSortCol = null;
      table.sortByTypes = ['asc', 'desc'];
      table.newItemOnTop = newItemOnTop;
      table.isStripped = isStripped;
      table.onSelectItem = onSelectItem;
      table.onDeselectItem = onDeselectItem;
      // functions
      table.sortFunctions = createSortFunctions(table.columns);
      table.filterFunctions = [];
      table.prefilterValue = {};
      // flags
      table.ready = false;
      table.dirty = true;
      table.empty = true;
      // public methods
      // data
      table.setData = setData;
      table.setDataPromise = setDataPromise;
      table.mergeIn = mergeIn;
      table.mergeInPromise = mergeInPromise;
      table.remove = remove;
      table.getTableData = getTableData;
      table.getUnfilteredTableData = getUnfilteredTableData;
      table.getLength = getLength;
      table.getUnfilteredLength = getUnfilteredLength;
      table.refreshData = refreshData;
      table.totalItems = totalItems; // total items at server
      table.isMoreThanTotalItems = false; // flag to show more than total items returned by server
      // display
      table.getTableHeaderDisplayName = getTableHeaderDisplayName;
      table.addColumn = addColumn;
      // sort
      table.sort = sort;
      table.setSortFunction = setSortFunction;
      table.isSortActive = isSortActive;
      table.showSortIcon = showSortIcon;
      table.setNewItemOnTop = setNewItemOnTop;
      // filter
      table.addColumnFilter = addColumnFilter;
      table.updateColumnFilter = updateColumnFilter;
      table.addFilter = addFilter;
      table.removeFilter = removeFilter;
      table.hasFiltered = hasFiltered;
      // pre-filter
      table.prefilter = prefilter;
      table.getPrefilter = getPrefilter;
      // selection
      table.select = select;
      table.deselect = deselect;
      table.clearSelect = clearSelect;
      table.toggleSelect = toggleSelect;
      table.isSelected = isSelected;
      table.getSelectionCount = getSelectionCount;
      table.getSelection = getSelection;
      table.isCheckedAll = isCheckedAll;
      table.isSelectionIndeterminate = isSelectionIndeterminate;
      table.setHighlightFn = setHighlightFn;
      table.highlight = highlight;
      return table;

      function setData(data) {
        table.ready = true;
        table.data = data;
        table.empty = (table.data.length === 0);
      }

      function setDataPromise(promise) {
        table.ready = false;
        return promise.then(setData);
      }

      function mergeIn(datas, key) {
        table.data = nvUtils.mergeAllIn(table.data, _.castArray(datas), key);
        table.dirty = true;
      }

      function remove(datas, key) {
        const dataArray = _.castArray(datas);
        _.each(dataArray, data => table.selection.delete(data));
        table.data = _.pullAllBy(table.data, dataArray, key);
        table.dirty = true;
      }

      function mergeInPromise(promise, key) {
        return promise.then(_.partial(mergeIn, _, key));
      }

      function getUnfilteredTableData() {
        if (!table.ready) {
          return [];
        }
        if (!table.dirty) {
          return table.tableData;
        }
        return table.data;
      }

      function getTableData() {
        if (!table.ready) {
          return [];
        }
        if (!table.dirty) {
          return table.tableData;
        }
        const filteredData = _.reduce(table.filterFunctions, _.filter, table.data);
        if (!table.currSortCol) {
          if (!table.newItemOnTop) {
            table.tableData = filteredData;
          } else {
            table.tableData = _.orderBy(filteredData, ['_isNew'], ['asc']);
          }
        } else {
          const sortFunction = table.sortFunctions[table.currSortCol];
          const order = table.sortByTypes[table.currSortIdx];
          table.tableData = sortFunction(filteredData, order);
        }
        table.dirty = false;
        return table.tableData;
      }

      function getUnfilteredLength() {
        return table.data.length;
      }

      function getLength() {
        return getTableData().length;
      }

      function refreshData() {
        table.dirty = true;
        updateShownAllSelected();
      }

      function getTableHeaderDisplayName(column) {
        return table.columns[column] || '';
      }

      function addColumn(columnName, obj) {
        table.columns[columnName] = obj.displayName;
        table.sortFunctions[columnName] = obj.sortFunction ||
            getBasicSortFunction(columnName, table);
        return table;
      }

      function sort(column) {
        if (angular.isDefined(table.sortFunctions[column])) {
          table.currSortIdx = table.currSortCol === column ? (table.currSortIdx + 1) % 2 : 0;
          table.currSortCol = column;
          table.dirty = true;
        }
        return table;
      }

      function setSortFunction(column, sortFunction) {
        table.sortFunctions[column] = sortFunction;
      }

      function isSortActive(option) {
        return table.currSortCol === option;
      }

      function showSortIcon(option) {
        const sortIcon = (table.currSortIdx === 0) ?
          'lib/ninja-commons/assets/images/nv-sort-asc.svg' :
          'lib/ninja-commons/assets/images/nv-sort-desc.svg';
        return table.currSortCol === option ? sortIcon :
          'lib/ninja-commons/assets/images/nv-sort.svg';
      }

      function addColumnFilter(columnName, filterFunction, getter) {
        if (table.localColumnFilter[columnName]) {
          const filterFn = table.localColumnFilter[columnName].filterFunction(getter);
          table.localColumnFilter[columnName] = {
            filterFunction: filterFn,
            getter: getter,
          };
          table.addFilter(filterFn);
        } else {
          table.localColumnFilter[columnName] = {
            filterFunction: filterFunction,
            getter: getter,
          };
          table.addFilter(filterFunction);
        }
        return table;
      }

      function updateColumnFilter(columnName, fn) {
        if (table.localColumnFilter[columnName]) {
          table.removeFilter(table.localColumnFilter[columnName].filterFunction);
          const filterFunction = fn(table.localColumnFilter[columnName].getter);
          table.localColumnFilter[columnName].filterFunction = filterFunction;
          table.addFilter(filterFunction);
        } else {
          table.localColumnFilter[columnName] = {
            filterFunction: fn,
          };
        }
        return table;
      }

      function addFilter(...fns) {
        table.filterFunctions = _.concat(table.filterFunctions, fns);
        table.dirty = true;
        return table;
      }

      function removeFilter(...fns) {
        _.pullAll(table.filterFunctions, fns);
        table.dirty = true;
        return table;
      }

      function hasFiltered() {
        return table.data.length !== table.getTableData().length;
      }

      function prefilter(stateParam) {
        table.prefilterValue = _.mapKeys(stateParam, (value, key) =>
          key.replace(/\-/g, '.')
        );
      }

      function getPrefilter() {
        return table.prefilterValue;
      }

      function select(item) {
        if (item === undefined) {
          // select all
          _.each(getTableData(), data => table.selection.add(data));
        } else {
          table.selection.add(item);
        }
        table.onSelectItem();
        updateShownAllSelected();
      }

      function deselect(item) {
        if (item === undefined) {
          // deselect all
          const tableData = table.getTableData();
          if (tableData.length === table.data.length) {
            table.selection.clear();
          } else {
            _.each(tableData, data => table.selection.delete(data));
          }
        } else {
          table.selection.delete(item);
        }
        table.onDeselectItem();
        updateShownAllSelected();
      }

      function clearSelect() {
        table.selection.clear();
        updateShownAllSelected();
      }

      function toggleSelect(item) {
        if (item === undefined) {
          // toggle all
          if (table.isCheckedAll()) {
            table.deselect();
          } else {
            table.select();
          }
        } else if (table.isSelected(item)) {
          table.deselect(item);
        } else {
          table.select(item);
        }
        updateShownAllSelected();
      }

      function getSelectionCount() {
        return table.selection.size;
      }

      function getSelection() {
        return Array.from(table.selection.values());
      }

      function isSelected(item) {
        return table.selection.has(item);
      }

      function isCheckedAll() {
        return table.isShownAllSelected;
      }

      function isSelectionIndeterminate() {
        return table.selection.size !== 0 && !table.isCheckedAll();
      }

      function createColumns(field) {
        const names = getNames(field, '');
        // add default columns
        const defaultColumns = { actions: 'commons.actions' };
        return _.omit(_.merge(...names, defaultColumns), ['_deep', '_array']);

        function getNames(obj, prefix) {
          return _.flatMap(obj, (value, key) =>
            ((_.has(value, '_array') || _.has(value, '_deep')) ?
                getNames(value, `${prefix}${key}.`) :
              { [prefix + key]: value.displayName }
            ));
        }
      }

      function createSortFunctions(columns) {
        return _.mapValues(columns, (displayName, column) => getBasicSortFunction(column, table));
      }

      function updateShownAllSelected() {
        const tableData = getTableData();
        table.isShownAllSelected = tableData.length > 0 &&
                                    _.every(tableData, data => isSelected(data));
      }

      function highlight(alias) {
        const fn = table.highlightFn || _.stubFalse;
        return fn(alias);
      }

      function setHighlightFn(highlightFn) {
        table.highlightFn = highlightFn;
      }

      function setNewItemOnTop(isOnTop) {
        table.newItemOnTop = isOnTop;
      }
    }

    function getBasicSortFunction(columnName, table) {
      if (table.newItemOnTop) {
        if (columnName !== 'id') {
          return function sortFunction(tableData, orderBy) {
            return _.orderBy(tableData, ['_isNew', columnName, 'id'], ['asc', orderBy, 'asc']);
          };
        }
        return function sortFunction(tableData, orderBy) {
          return _.orderBy(tableData, ['_isNew', columnName], ['asc', orderBy]);
        };
      }
      if (columnName !== 'id') {
        return function sortFunction(tableData, orderBy) {
          return _.orderBy(tableData, [columnName, 'id'], [orderBy, 'asc']);
        };
      }
      return function sortFunction(tableData, orderBy) {
        return _.orderBy(tableData, [columnName], orderBy);
      };
    }

    function getBasicFilterFunction(columnName, strFn) {
      return (row) => {
        const lowercase = (strFn() || '').toLowerCase();
        const data = _.get(row, columnName);

        return lowercase.length === 0 ||
          _.toString(data).toLowerCase().indexOf(lowercase) > -1;
      };
    }
  }
})());
