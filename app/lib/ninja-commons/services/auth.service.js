(function service() {
  angular
        .module('nvCommons.services')
        .factory('nvAuth', auth);

  auth.$inject = [
    '$window', '$location', '$cookies', '$http', '$rootScope',
    'nvJson', 'nvClientStore', '$state', 'nvMigrationService',
  ];

  function auth(
    $window, $location, $cookies, $http, $rootScope,
    nvJson, nvClientStore, $state, nvMigrationService
  ) {
    const ACCESS_TOKEN = 'ninja_access_token';

    $rootScope.$watch('domain', () => {
      const accessToken = $cookies.get(ACCESS_TOKEN);
      updateAccessToken(accessToken);
    });

    return {

      /**
       * Initializes the auth service.
       * Sets some sane defaults on the scope based on the access token cookie.
       * Call this in a run block before using any of the other public methods.
       */
      init: function init() {
        updateUser($cookies.get('user'));
      },

      /**
       * Login the user into the app.
       * Redirects to the auth domain to verify credentials.
       */
      login: function login() {
        removeCookies();
        updateUser();
        updateAccessToken();

        if ($state.current.name !== 'login' &&
          _.isUndefined(nvClientStore.localStorage.get('previousUrl', { namespace: false }))) {
          nvClientStore.localStorage.set('previousUrl', $window.location.href, { namespace: false });
        }

        $window.location.href = getRedirectUrl($window.location.origin);
      },

      /**
       * Logout the user from the app.
       * Expires the access token cookies and clears the globals.
       * @param callback - The function to call after the user has been logged out.
       */
      logout: function logout(callback = angular.noop) {
        removeCookies();
        updateUser();
        updateAccessToken();
        callback();
      },

      /**
       * Checks if a valid user is currently logged-in.
       * @returns {boolean} - True if logged in, false otherwise.
       */
      isLoggedIn: function isLoggedIn() {
        return !!$cookies.get('user');
      },

      /**
       * Returns the currently logged-in user.
       * @returns {object} - The user who is currently logged-in, null otherwise.
       */
      user: function user() {
        return $rootScope.user;
      },

      /**
       * Returns the domains (e.g. 'sg', 'my', 'id', 'mbs', etc..)
       * based on the available access tokens.
       * @returns {Array} - The list of available domains.
       */
      getDomains: function getDomains() {
        return _(_.split(_.get($rootScope, 'user.serviceId', ''), ','))
          .map(_.lowerCase)
          .compact()
          .value();
      },

      getAccessToken: () => $cookies.get(ACCESS_TOKEN),
      getAuthUrl: getAuthUrl,

    };

    function getRedirectUrl(origin) {
      return `${getAuthUrl()}/signin?nv_redirect=${origin}`;
    }

    function getAuthUrl() {
      return nv.config.servers.authlogin;
    }

    function updateUser(user) {
      $rootScope.user = user ? nvJson.tryParse(user) : null;
    }

    function updateAccessToken(accessToken) {
      if (accessToken) {
        $http.defaults.headers.common.Authorization = `Bearer ${accessToken}`;
        nvMigrationService.setAuthToken(accessToken);
      } else {
        $http.defaults.headers.common = _.omit($http.defaults.headers.common, 'Authorization');
        nvMigrationService.removeAuthToken();
      }
    }

    function removeCookies() {
      const domain = $location.host().match(/\..+\..+$/);
      if (domain) {
        const options = { path: '/', domain: domain[0] };
        $cookies.remove('user', options);
        $cookies.remove(ACCESS_TOKEN, options);
      }
    }
  }
}());
