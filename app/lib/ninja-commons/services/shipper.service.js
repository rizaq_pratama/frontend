(function service() {
  angular
    .module('nvCommons.services')
    .factory('ShipperService', ShipperService);

  ShipperService.$inject = ['$interval', 'Shippers', 'Dash', 'nvEnv'];

  /**
   * created to handle background services that needed by shipper creation flow
   */
  function ShipperService($interval, Shippers, Dash, nvEnv) {
    return {
      /**
       * @param {Object} shipper
       * @param {Number} shipper.id
       * @param {Number} shipper.legacyId
       */
      verifyDashAccountAndDisableSdAccount: (shipper) => {
        if (nvEnv.isSaas(nv.config.env)) {
          // dash service not yet available in saas
          return;
        }

        const stop = $interval(task, 1000, 3);

        function task() {
          Dash.checkDashEligibility(shipper.id)
            .then((data) => {
              if (data.verified) {
                disableSdShipper();
              } else {
                // not eligible
                $interval.cancel(stop);
              }
            });

          function disableSdShipper() {
            Shippers.disableSdShipper(shipper.legacyId).then(() => {
              $interval.cancel(stop);
            });
          }
        }
      },
    };
  }
}());
