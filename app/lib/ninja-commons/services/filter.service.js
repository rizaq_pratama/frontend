;(function() {
    'use strict';

    angular
        .module('nvCommons.services')
        .factory('nvFilter', filter);

    function filter() {

        return {

            /**
             * Takes an array of lowercase strings and returns a new array of strings
             * @param arr
             * @param str
             * @returns {*}
             */
            lowercaseSubstring: function(arr, str) {
                return _.filter(arr, createFilterFor(str));

                function createFilterFor(str) {
                    var lowercaseStr = str.toLowerCase();
                    return function filterFn(arrStr) {
                        return arrStr.indexOf(lowercaseStr) !== -1;
                    };
                }
            }

        };

    }

})();
