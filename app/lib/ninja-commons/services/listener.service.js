(function service() {
  /**
   *
   * Observer Pattern
   *
   * Listener.create -> return a new listener object
   *
   * listener object
   * listener.on -> register callback, return deregister callback function
   * listener.off -> deregister callback
   * listener.notify -> fire events, call all callbacks registered
   *
   */

  angular
    .module('nvCommons.services')
    .factory('Listener', ListenerService);

  ListenerService.$inject = ['$timeout'];

  function ListenerService($timeout) {
    return {
      create: create,
    };
    function create() {
      return new Listener();
    }
    function Listener() {
      const listener = this;
      listener.cb = [];

      listener.on = on;
      listener.off = off;
      listener.notify = notify;
      listener.notifyChange = notifyChange;

            // ////////////////////
      function on(cb) {
        if (!angular.isFunction(cb)) {
          return angular.noop;
        }
        if (_.indexOf(listener.cb, cb) === -1) {
          listener.cb.push(cb);
        }
        return listener.off.bind(listener, cb);
      }

      function off(cb) {
        _.pull(listener.cb, cb);
      }

      function notify(...args) {
        _.each(listener.cb, (cb) => {
          $timeout(() => {
            cb(...args);
          });
        });
      }

      /**
       * same as notify, but will only notify listeners when there is change in arguments
       * prevent notify listeners multiple times with the same arguments
       * @return undefined
       */
      function notifyChange(...args) {
        _.each(listener.cb, (cb) => {
          $timeout(() => {
            if (angular.isUndefined(cb.lastArgs) || !_.isEqual(args, cb.lastArgs)) {
              cb.lastArgs = args;
              cb(...args);
            }
          });
        });
      }
    }
  }
}());
