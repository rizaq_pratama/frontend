(function service() {
  angular
    .module('nvCommons.services')
    .factory('Excel', Excel);

  Excel.$inject = ['$http', 'nvToast', 'nvRequestUtils', 'nvFileUtils', 'nvTranslate', '$q'];
  function Excel($http, nvToast, nvRequestUtils, nvFileUtils, nvTranslate, $q) {
    return {
      download: download,
      view: view,
      read: read,
      write: write,
      writeCsv: writeCsv,
      writeMultiSheet: writeMultiSheet,
    };

    /**
     * Download Excel / CSV file
     * @param  {String} url
     * @param  {String} filename
     * @return {Promise}
     */
    function download(url, filename) {
      const toast = nvToast.info(nvTranslate.instant('commons.service.file.attempt-download',
                                  { filename: filename }),
                                { timeOut: 0 });

      return $http.get(url, { responseType: 'arraybuffer' }).then(
                success,
                byteArrayError
            );
      // ////////////////////////////
      function success(response) {
        if (response.status === 200) {
          toast.remove();
          nvToast.success(nvTranslate.instant('commons.service.file.downloading', { filename: filename }));

          const urldata = nvFileUtils.getURLFromData(response.data, nvFileUtils.FILETYPE_EXCEL);
          nvFileUtils.download(urldata, filename || 'download.csv');
        } else {
          nvToast.error(nvTranslate.instant('commons.service.file.download-failed', { filename: filename }),
            nvTranslate.instant('commons.service.file.http-status', { status: response.status }));
        }
      }
    }

    /**
     * View Excel / CSV file
     * @param  {String} url
     * @return {Promise}
     */
    function view(url) {
      return $http.get(url, { responseType: 'arraybuffer' }).then(
                success,
                byteArrayError
            );

            // ///////////////////////////
      function success(response) {
        const dataurl = nvFileUtils.getURLFromData(response.data, nvFileUtils.FILETYPE_EXCEL);
        nvFileUtils.view(dataurl);
      }
    }

    /**
     * Read Excel / CSV file
     * @param  {File} file
     * @param {Boolean} hasHeadr
     * @return {Promise}
     * 
     * if hasHeader = false
     * data will returned as array of object with A,B,C as header
     * and cols will be an empty array
     * some limitation on XLSX.read, you shouldn't use header with space
     */
    function read(file, hasHeader = true) {
      const deferred = $q.defer();

      const reader = new FileReader();
      reader.onload = onLoad;
      reader.readAsBinaryString(file);

      return deferred.promise;

      function onLoad() {
        const bstr = reader.result;
        const wb = XLSX.read(bstr, { type: 'binary' });

        /* grab first sheet */
        const wsname = wb.SheetNames[0];
        const ws = wb.Sheets[wsname];

        /* grab first row and generate column headers */
        const option = {
          raw: true,
        };
        if (hasHeader) {
          option.header = 1;
        } else {
          option.header = 'A';
        }
        const aoa = XLSX.utils.sheet_to_json(ws, option);
        const cols = [];
        if (hasHeader) {
          _.forEach(aoa[0], (row, i) => {
            cols[i] = row;
          });
        }

        /* generate rest of the data */
        const data = [];
        _.forEach(aoa, (row, r) => {
          // skip first row if using header
          if (hasHeader && r === 0) {
            return;
          }
          if (hasHeader) {
            data[r - 1] = {};
          } else {
            data[r] = {};
          }

          _.forEach(aoa[r], (a, i) => {
            if (a === null) {
              return;
            }
            if (hasHeader) {
              data[r - 1][aoa[0][i]] = a;
            } else {
              data[r][i] = a;
            }
            
          });
        });

        deferred.resolve({
          data: data,
          cols: cols,
        });
      }
    }

    function write(data = [], fileName = 'download.xlsx', forceStr = false) {
      const wb = new Workbook();
      const ws = sheetFromArrayOfArrays(data, forceStr);

      /* add worksheet to workbook */
      const wsName = 'Sheet 1';
      wb.SheetNames.push(wsName);
      wb.Sheets[wsName] = ws;

      const wbout = XLSX.write(wb, { bookType: 'xlsx', bookSST: true, type: 'binary' });
      saveAs(new Blob([s2ab(wbout)], { type: 'application/octet-stream' }), fileName);    
    }
    /**
     * Write csv as File or to file system
     * @param {Array} cols Array of string containing the csv header
     * @param {Array} data Array of array or object containing the data, 
     * note that for object need to supply correct properties
     * @param {Boolean} toFile true to write it into filesystem
     * @param {String} fileName name of the file if we set the toFile true
     */
    function writeCsv(cols = [], data = [], toFile = false, fileName = 'download.csv') {
      let content = '';
      content += (`${_.join(cols, ',')}\r\n`);
      _.forEach(data, (d) => {
        if (_.isObject(d)) {
          const row = _(cols)
                      .map(col => (d[col]))
                      .join(',');
          content += `${row}\r\n`;
        } else {
          content += `${_.join(d, ',')}\r\n`;
        }
      });
      const file = new File([content], fileName, {
        type: 'text/csv',
        lastModified: new Date(),
      });
      if (toFile) {
        return saveAs(file, { type: 'text/csv' }, fileName);
      }
      return file;
    }

    /**
     * 
     * @param {*} datas 
     * @param {*} sheetNames 
     * @param {*} fileName 
     */
    function writeMultiSheet(datas = [], sheetNames = [], fileName = 'download.xlsx') {
      const wb = new Workbook();
      _.each(datas, (val, key) => {
        const ws = sheetFromArrayOfArrays(val);
      /* add worksheet to workbook */
        const wsName = sheetNames[key] || `Sheet ${key + 1}`;
        wb.SheetNames.push(wsName);
        wb.Sheets[wsName] = ws;
      });
      const wbout = XLSX.write(wb, { bookType: 'xlsx', bookSST: true, type: 'binary' });
      saveAs(new Blob([s2ab(wbout)], { type: 'application/octet-stream' }), fileName);
    }

    function Workbook() {
      if (!(this instanceof Workbook)) {
        return new Workbook();
      }
      this.SheetNames = [];
      this.Sheets = {};
    }

    function s2ab(s) {
      const buf = new ArrayBuffer(s.length);
      const theView = new Uint8Array(buf);

      _.forEach(s, (row, i) => {
        theView[i] = s.charCodeAt(i) & 0xFF;
      });

      return buf;
    }

    function datenum(v, date1904) {
      let theValue = v;
      if (date1904) {
        theValue += 1462;
      }
      const epoch = Date.parse(theValue);
      return (epoch - new Date(Date.UTC(1899, 11, 30))) / (24 * 60 * 60 * 1000);
    }

    function sheetFromArrayOfArrays(theData, forceStr = false) {
      const theWs = {};
      const range = { s: { c: 10000000, r: 10000000 }, e: { c: 0, r: 0 } };
      _.forEach(theData, (row, R) => {
        _.forEach(row, (rowInner, C) => {
          if (range.s.r > R) {
            range.s.r = R;
          }
          if (range.s.c > C) {
            range.s.c = C;
          }
          if (range.e.r < R) {
            range.e.r = R;
          }
          if (range.e.c < C) {
            range.e.c = C;
          }

          const cell = {
            v: rowInner,
          };

          if (cell.v == null) {
            return;
          }

          const cellRef = XLSX.utils.encode_cell({ c: C, r: R });

          if (forceStr) {
            cell.t = 's';
          } else if (typeof cell.v === 'number') {
            cell.t = 'n';
          } else if (typeof cell.v === 'boolean') {
            cell.t = 'b';
          } else if (cell.v instanceof Date) {
            cell.t = 'n'; cell.z = XLSX.SSF._table[14];
            cell.v = datenum(cell.v);
          } else {
            cell.t = 's';
          }

          theWs[cellRef] = cell;
        });
      });

      if (range.s.c < 10000000) {
        theWs['!ref'] = XLSX.utils.encode_range(range);
      }
      return theWs;
    }

    function byteArrayError(response) {
      const json = nvRequestUtils.byteArrayToJSON(response.data);
      response.data = json;
      return nvRequestUtils.nvFailure(response);
    }
  }
}());
