(function service() {
  angular
    .module('nvCommons.services')
    .factory('nvTranslate', nvTranslate);

  nvTranslate.$inject = ['$rootScope', '$translate', 'nvClientStore', 'Listener'];

  function nvTranslate($rootScope, $translate, nvClientStore, Listener) {
    const LOCAL_STORAGE_LANGUAGE_KEY = 'language';
    const LANGUAGES = [
      { code: 'en', displayName: 'English' },
      { code: 'ms', displayName: 'Malay' },
      { code: 'id', displayName: 'Indonesian' },
      { code: 'vn', displayName: 'Vietnam' },
    ];

    const listener = Listener.create();

    const self = {
      init: init,
      getAvailableLanguages: getAvailableLanguages,
      setLanguageCode: setLanguageCode,
      getLanguageCode: getLanguageCode,
      instant: $translate.instant,
      translate: $translate,
      onLanguageChanged: onLanguageChanged,
    };
    return self;

    function init() {
      $translate.use(self.getLanguageCode());
    }

    function getAvailableLanguages() {
      return LANGUAGES;
    }

    function setLanguageCode(code) {
      nvClientStore.localStorage.set(LOCAL_STORAGE_LANGUAGE_KEY, code);
      const promise = $translate.use(code);
      // $translate.use return a Promise with loaded language data or the language key
      // if a falsy param was given.
      if (angular.isString(promise)) {
        console.error(`${code} is not a valid language key`);
        return promise;
      }
      return promise.then(() => listener.notify());
    }

    function getLanguageCode() {
      return nvClientStore.localStorage.get(LOCAL_STORAGE_LANGUAGE_KEY) || ($rootScope.user && $rootScope.user.language) || 'en';
    }

    function onLanguageChanged(cb) {
      return listener.on(cb);
    }
  }
}());
