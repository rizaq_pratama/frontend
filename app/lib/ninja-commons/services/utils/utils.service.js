(function service() {
  angular
        .module('nvCommons.services')
        .factory('nvUtils', utils);

  function utils() {
    const self = {

      /**
       * Does safe, deep get-or-else of a property from an object,
       * returning a default if any value in the path is undefined.
       *
       * Path can be passed as single dot-separated string, e.g.
       *
       *   getOrElse({}, 'foo.bar', 'baz') will return 'baz'
       *   getOrElse({foo: {bar: 'qux'}}, 'foo.bar', 'baz') will return 'qux'
       *
       * @param base       - The object to do the get-or-else on.
       * @param props      - The path (a dot-separated string).
       * @param defaultVal - The default value to return should any value in the path be undefined.
       * @returns {*}      - The value in the object, or {@code defaultVal}
       *                     if any part of the path is undefined.
       */
      getOrElse: function getOrElse(base, props, defaultVal) {
        return _.get(base, props, defaultVal);
      },

      /**
       * Travels down an object tree with properties given by a dot-separated string.
       *
       * @param base  - The object to traverse.
       * @param props - The dot-separated string, e.g. 'custom.operatingHours'.
       * @returns {*} - If the deep property exists, return it, otherwise, returns undefined.
       */
      getDeep: function getDeep(base, props) {
        return _.get(base, props);
      },

      createSnapshot: function createSnapshot(obj) {
        return _.assign(angular.copy(obj), {
          _snapshot: angular.copy(obj),
          revert: function revert() {
            _.forOwn(angular.copy(obj), (val, key) => {
              this[key] = val;
            });
          },
        });
      },

      /**
       * Assigns the properties of source object(s) to the destination object for all destination
       * properties that resolve to falsy values.
       *
       *   defaults(<destination>, [<sources>]) -> function signature
       *
       * This method mutates the destination object.
       * Example usage:
       *
       *   defaults({foo: '', bar: 'monkey'}, {foo: 2}, {bar: 'banana'}, {baz: 'qux'})
       *   -> {foo: 2, bar: 'monkey', baz: 'qux'}
       */
      defaults: _.partialRight(_.assignWith, (value, other) => (!value ? other : value)),

      /**
       * Maps an array of objects to another array of objects with only some selected properties.
       *
       * @param array     - The array of objects to map.
       * @param props     - The properties to be picked.
       *                    Can be a single array or part of the argument list.
       * @returns {Array} - The transformed array of objects with only a subset of properties.
       */
      mapToObjectWith: function mapToObjectWith(array, ...props) {
        return _.map(array, obj => _.pick(obj, _.flatten(props)));
      },

      /**
       * Maps an array of objects to another array of objects without the listed properties.
       *
       * @param array     - The array of objects to map.
       * @param props     - The properties to omit.
       *                    Can be a single array or part of the argument list.
       * @returns {Array} - The transformed array of objects with only a subset of properties.
       */
      mapToObjectWithout: function mapToObjectWithout(array, ...props) {
        return _.map(array, obj => _.omit(obj, _.flatten(props)));
      },

      /**
       * - Merges an object o' into an array of objects, combining o' with the existing
       *   object o in the array.
       * - Objects o and o' are compared for equality based on the given {@code key}
       *   with {@code value}.
       * - If object o doesn't exist in the array of objects, then append object o' to
       *   the end of the array.
       *
       * Mutates the {@code array}.
       *
       * @param array     - The array of objects to merge into.
       * @param object    - The object o' to merge.
       * @param key       - The object key to use for object comparison.
       * @param value     - The first object in the array that has the key with this value
       *                    is chosen for merging.
       * @returns {Array} - The original array with the merging completed.
       */
      mergeIn: function mergeIn(array, object, key, value) {
        const existingObject = _.find(array, [key, value]);
        if (_.isUndefined(existingObject)) {
          array.push(object);
        } else {
          _.assign(existingObject, object);
        }
        return array;
      },

      /**
       * - Combines objects from {@code array2} into {@code array1}.
       * - Properties of the objects in {@code array1} are overwritten by the objects
       *   in {@code array2}.
       * - The objects are merged based on the value of the given {@code key}.
       * - If an object doesn't exist in one of the arrays, then it is appended to the
       *   resulting array.
       *
       * Mutates both {@code array1} and {@code array2}.
       *
       * @param array1    - The array of objects to merge into.
       * @param array2    - The array of objects to merge.
       * @param key       - The object key used for object comparison.
       * @returns {Array} - The array with the merging completed.
       */
      mergeAllIn: function mergeAllIn(array1, array2, key) {
        let bigArr;
        let smlArr;
        let mergeToBig;
        if (array1.length >= array2.length) {
          [bigArr, smlArr] = [array1, array2];
          mergeToBig = true;
        } else {
          [bigArr, smlArr] = [array2, array1];
          mergeToBig = false;
        }

        // always iterate the smaller array
        _.each(smlArr, (elem) => {
          const j = _.findIndex(bigArr, { [key]: elem[key] });
          if (j === -1) {
            // if the item doesn't exist in the big array, append the item
            bigArr.push(elem);
          } else {
            // if the item exists, merge the item from array2 into array1
            bigArr[j] = mergeToBig ? _.assign(bigArr[j], elem) : _.assign(elem, bigArr[j]);
          }
        });
        return bigArr;
      },

      /**
       * Combines objects from {@code array2} into {@code array1}.
       * The objects are merged based on the value of the given {@code key}.
       * If an object doesn't exist in {@code array1}, it is ignored.
       *
       * @param array1                - The array of objects to merge into.
       * @param array2                - The array of objects to merge.
       * @param key                   - The object key used for object comparison.
       * @param options
       * @param options.overrideFirst - If true, the object properties in {@code array1}
       *                                are overwritten, otherwise the object properties
       *                                in {@code array2} are overwritten.
       * @returns {Array}             - The new array with the merging completed.
       */
      mergeAllIntoFirst: function mergeAllIntoFirst(array1, array2, key,
                                                    { overrideFirst = true } = {}) {
        return _.map(array1, overrideFirst ? mergeAndOverride1 : mergeAndOverride2);

        function mergeAndOverride1(obj1) {
          return _.merge({}, obj1, findObj2(array2, obj1[key]));
        }

        function mergeAndOverride2(obj1) {
          return _.merge({}, findObj2(array2, obj1[key]), obj1);
        }

        function findObj2(arr, val) {
          return _.find(arr, [key, val]) || {};
        }
      },

      differenceWith: function differenceWith(array1, array2, key) {
        const array1Map = {};
        _.forEach(array1, (row) => {
          array1Map[row[key]] = row;
        });

        _.forEach(array2, (row) => {
          if (array1Map[row[key]]) {
            delete array1Map[row[key]];
          }
        });

        const differenceArray = [];
        _.forEach(array1Map, (row) => {
          differenceArray.push(row);
        });

        return differenceArray;
      },
      /**
       * get difference between two object and return the difference key in an object
       */
      getObjectDiff: function getObjectDiff(obj1, obj2) {
        return Object.keys(obj2).reduce((diff, key) => {
          if (obj1[key] === obj2[key]) {
            return diff;
          }
          diff[key] = obj2[key];
          return diff;
        }, {});
      },

      toThemeClass: function toThemeClass(theme) {
        return theme === 'default' ? '' : `md-${theme}-theme`;
      },

      clearArray: (array) => {
        while (array.length) {
          array.pop();
        }
      },

    };

    return self;
  }
}());
