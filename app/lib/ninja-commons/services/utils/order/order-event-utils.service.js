(function service() {
  angular
    .module('nvCommons.services')
    .factory('OrderEventUtils', OrderEventUtils);

  OrderEventUtils.$inject = ['$rootScope', 'nvDateTimeUtils', 'Events'];

  function OrderEventUtils($rootScope, nvDateTimeUtils, Events) {
    let hubs = [];
    const blackListedHubsId = [];
    const HUB_LOCATION_TYPE = {
      ORIGIN: 'ORIGIN',
      DESTINATION: 'DESTINATION',
      TRANSIT: 'TRANSIT',
      UNKNOWN: 'UNKNOWN',
    };

    return {

      setHub: function setHub(listHub) {
        hubs = listHub;
      },

      extendEvents: function extendEvents(events) {
        return _.map(events, event => (
            setCustomEventData(event)
        ));
      },

      processOrderEventData: function processOrderEventData(orders) {
        const listOrderEvent = [];
        _.forEach(orders, (order) => {
          const duped = duplicateDoubleInboundEvent(order);
          listOrderEvent.push(setCustomOrderEventData(duped[0]));
          if (duped[1]) {
            listOrderEvent.push(setCustomOrderEventData(duped[1]));
          }
        });
        return listOrderEvent;
      },

    };

    function duplicateDoubleInboundEvent(orderEvent) {
      if (orderEvent.data && (orderEvent.type === 'HUB_INBOUND_SCAN' || orderEvent.type === 'PARCEL_ROUTING_SCAN'
        || orderEvent.type === 'ROUTE_INBOUND_SCAN')) {
        // if hub_location_type contains both ORIGIN and DESTINATION
        // clone it and remove the extra hub_location_type
        const data = orderEvent.data;
        if (data.hub_location_types && data.hub_location_types.length === 2
          && _.includes(data.hub_location_types, HUB_LOCATION_TYPE.DESTINATION)
          && _.includes(data.hub_location_types, HUB_LOCATION_TYPE.ORIGIN)) {
          const clonedEvent = _.cloneDeep(orderEvent);
          clonedEvent.data.hub_location_types = [HUB_LOCATION_TYPE.DESTINATION];
          orderEvent.data.hub_location_types = [HUB_LOCATION_TYPE.ORIGIN];
          return [orderEvent, clonedEvent];
        }
        return _.castArray(orderEvent);
      }

      return _.castArray(orderEvent);
    }

    function isBlacklisted(hubId) {
      return _.includes(blackListedHubsId, hubId);
    }

    function getHubName(hubId) {
      const hubData = _.find(hubs, ['id', hubId]);
      return _.size(hubData) > 0 ? hubData.name : hubId;
    }

    function setCustomEventData(event) {
      event._event_time = event.event_time ? nvDateTimeUtils.displayDateTime(
            moment.utc(event.event_time)
        ) : '';

      event._tags = (event.tags && event.tags.join(', ')) || null;
      event._user = event.user_name && event.user_name.trim() ?
            event.user_name : event.user_email;
      event._scan_id = (event.data && event.data.scan_id) || null;
      event._route_id = (event.data && event.data.route_id) || null;
      event._hub_name = (event.data && event.data.curr_hub_id && getHubName(event.data.curr_hub_id))
          || null;

      event._event_name_and_date = event.name;
      if (event._event_time) {
        event._event_name_and_date += ` (${event._event_time})`;
      }

        // set extra event.data
      event.data = _.defaults(event.data, {});
      if (!_.isUndefined(event.data.driver_first_name) ||
            !_.isUndefined(event.data.driver_last_name)
        ) {
            // combine driver first_name and last_name
        event.data.driver_name = _.compact([event.data.driver_first_name, event.data.driver_last_name]).join(' ');
      }

      if (!_.isUndefined(event.data.failure_reasons) && _.size(event.data.failure_reasons) > 0) {
        const firstKey = Object.keys(event.data.failure_reasons)[0];
        event.data.failure_reason = event.data.failure_reasons[firstKey];

            // set to other language based on user preference (if available)
        if (!_.isUndefined(event.data.failure_reasons[$rootScope.user.language])) {
          event.data.failure_reason = event.data.failure_reasons[$rootScope.user.language];
        }
      }

        // set event description
      event._description = Events.getMessage(
            event.event_category, event.data.sub_category, event.data
        );

      return event;
    }

    function setCustomOrderEventData(event) {
      event._event_time = event.time ?
        nvDateTimeUtils.displayDateTime(moment.utc(event.time)) : null;
      event._tags = (event.tags && event.tags.map(_.startCase).join(', ')) || null;
      event._user = (event.user_name && event.user_name.trim()) || event.user_email;
      event._scan_id = (event.data && event.data.scan_id) || null;
      event._route_id = setRouteId(event.type, event.data);
      event._hub_name = setHubId(event.type, event.data);

      event.name = _.startCase(event.type);
      event.user_type = _.startCase(event.user_grant_type);

      // set extra event.data
      event.data = _.defaults(event.data, {});
      if (!_.isUndefined(event.data.driver_first_name) ||
        !_.isUndefined(event.data.driver_last_name)) {
        // combine driver first_name and last_name
        event.data.driver_name = _.compact([event.data.driver_first_name, event.data.driver_last_name]).join(' ');
      }
      if (!_.isUndefined(event.data.failure_reasons) && _.size(event.data.failure_reasons) > 0) {
        const firstKey = Object.keys(event.data.failure_reasons)[0];
        event.data.failure_reason = event.data.failure_reasons[firstKey];

        // set to other language based on user preference (if available)
        if (!_.isUndefined(event.data.failure_reasons[$rootScope.user.language])) {
          event.data.failure_reason = event.data.failure_reasons[$rootScope.user.language];
        }
      }

      // set event description
      event._description = mapEventTypeToDescription(event.type, event.data);

      return event;
    }

    function buildHubInboundInfo(data) {
      if (isBlacklisted(_.get(data, 'hub_id', -1))) {
        return '';
      }
      if (_.isUndefined(data.hub_location_types)) {
        return '';
      }
      const hubInboundType = data.hub_location_types.length > 0 ?
        data.hub_location_types[0] : HUB_LOCATION_TYPE.UNKNOWN;
      return `Arrived at ${_.capitalize(hubInboundType)} Hub: "Parcel inbound at ${_.capitalize(hubInboundType)} Hub - ${getHubName(data.hub_id)}"`;
    }

    function mapEventTypeToDescription(eventType, data) {
      let msg = null;

      switch (eventType) {
        case 'PRICING_CHANGE':
          return [
            `Script ID: <b>${(data && data.script_id) || '?'}</b> Version: <b>${(data && data.version) || '?'}</b>`,
            `Old Price: <b>${(data && data.old_price) || 0}</b> New Price: <b>${(data && data.new_price) || 0}</b>`,
          ].join('<br>');
        case 'CANCEL':
        case 'FORCED_SUCCESS':
          return (data && data.reason && `Reason: <b>${data.reason}</b>`) || '';
        case 'RESCHEDULE':
          return [
            (data && data.reason && `Reason: <b>${data.reason}</b>`) || '',
            (data && data.old_st_timestamp && `Old Start Time: <b>${nvDateTimeUtils.displayDateTime(moment.utc(data.old_st_timestamp))}</b>`) || '',
            (data && data.old_ed_timestamp && `Old End Time: <b>${nvDateTimeUtils.displayDateTime(moment.utc(data.old_ed_timestamp))}</b>`) || '',
            (data && data.new_st_timestamp && `New Start Time: <b>${nvDateTimeUtils.displayDateTime(moment.utc(data.new_st_timestamp))}</b>`) || '',
            (data && data.new_ed_timestamp && `New End Time: <b>${nvDateTimeUtils.displayDateTime(moment.utc(data.new_ed_timestamp))}</b>`) || '',
          ].filter(Boolean).join('<br>');
        case 'RESUME':
          return '';
        case 'RTS':
          return [
            (data && data.reason && `Reason: <b>${data.reason}</b>`) || '',
            (data && data.st_timestamp && `Start Time: <b>${nvDateTimeUtils.displayDateTime(moment.utc(data.st_timestamp))}</b>`) || '',
            (data && data.ed_timestamp && `End Time: <b>${nvDateTimeUtils.displayDateTime(moment.utc(data.ed_timestamp))}</b>`) || '',
          ].filter(Boolean).join('<br>');
        case 'VERIFY_ADDRESS':
          return [
            (data && data.formatted_address && `Address: <b>${data.formatted_address}</b>`) || '',
            (data && `Latitude: <b>${data.latitude}</b>`) || '',
            (data && `Longitude: <b>${data.longitude}</b>`) || '',
            (data && `Confidence Score: <b>${data.confidence_score}</b>`) || '',
          ].filter(Boolean).join('<br>');
        case 'ADDED_TO_SHIPMENT':
        case 'REMOVED_FROM_SHIPMENT':
          return [
            `Shipment <b>${data.shipment_id}</b>`,
            (data && data.dest_hub_id && `bound for Hub <b>${data.dest_hub_id}</b>`) || '',
            (data && data.dest_hub_id && `(${getHubName(data.dest_hub_id)})`) || '',
          ].filter(Boolean).join(' ');
        case 'SHIPMENT_COMPLETED':
          return `Shipment <b>${data.shipment_id}</b>`;
        case 'UPDATE_ADDRESS':
        case 'UPDATE_CONTACT_INFORMATION':
        case 'UPDATE_DISTRIBUTION_POINT':
        case 'UPDATE_INSTRUCTION':
        case 'UPDATE_CASH':
        case 'UPDATE_DIMENSION':
        case 'UPDATE_JOB_INFO':
          return _.map(data, (value, key) =>
            formatMessage(_.startCase(key), value.old_value, value.new_value))
            .filter(Boolean)
            .join('<br>');
        case 'UPDATE_SLA':
          return _.map(data, (value, key) => {
            switch (key) {
              case 'delivery_start_time':
              case 'delivery_end_time':
              case 'pickup_start_time':
              case 'pickup_end_time':
                return `<b>${_.startCase(key)}</b> changed from <i>${nvDateTimeUtils.displayDateTime(moment.utc(value.old_value))}</i> to <i>${nvDateTimeUtils.displayDateTime(moment.utc(value.new_value))}</i>`;
              case 'delivery_sla_time':
                return `<b>Delivery SLA Time</b> changed from <i>${nvDateTimeUtils.displayDateTime(moment.utc(value.old_value))}</i> to <i>${nvDateTimeUtils.displayDateTime(moment.utc(value.new_value))}</i>`;
              case 'delivery_priority_level':
                return `<b>${_.startCase(key)}</b> changed from <i>${value.old_value}</i> to <i>${value.new_value}</i>`;
              default:
                return '';
            }
          }).filter(Boolean).join('<br>');
        case 'ASSIGNED_TO_DP':
          msg = `${data.dp_name} (id: <b>${data.dp_id}</b>)`;
          if (data.drop_off_on_date) {
            msg += `<br>Drop off on: <b>${data.drop_off_on_date}</b>`;
          }
          if (data.collect_by_date) {
            msg += `<br>Collect by: <b>${data.collect_by_date}</b>`;
          }
          if (data.reason) {
            data.reason = `(${data.reason})`;
          } else {
            data.reason = '';
          }
          switch (data.authorized_by) {
            case 'CUSTOMER_CONFIRMED':
              msg += '<br>Confirmed by <b>End Customer (Consignee)</b>';
              break;
            case 'OPERATOR_CONFIRMED':
              msg += `<br>Confirmed by <b>Operator User ${data.reason}</b>`.trim();
              break;
            case 'SYSTEM_CONFIRMED':
              msg += `<br>Confirmed by <b>System ${data.reason}</b>`.trim();
              break;
            default:
          }
          return msg;
        case 'FROM_DP_TO_CUSTOMER':
          return `Collected by customer <b>${data.customer_name}</b>`;
        case 'FROM_SHIPPER_TO_DP':
          return `Dropoff at DP by shipper <b>${data.shipper_name}</b>`;
        case 'FROM_DP_TO_DRIVER':
          return `Collected by driver <b>${data.driver_name}</b> (id: <b>${data.driver_id}</b>) on route <b>${data.route_id}</b>`;
        case 'FROM_DRIVER_TO_DP':
          return `Dropoff at DP <b>${data.dp_id}</b> by driver <b>${data.driver_name}</b> (id: <b>${data.driver_id}</b>) on route <b>${data.route_id}</b>`;
        case 'DRIVER_INBOUND_SCAN':
          return `Inbounded by Driver <b>${data.driver_id}</b> on Route <b>${data.route_id}</b> at Hub <b>${data.hub_id}</b>`;
        case 'DRIVER_PICKUP_SCAN':
          return `Driver <b>${data.driver_id}</b> pickup from Shipper <b>${data.shipper_id}</b> on Route <b>${data.route_id}</b> at Waypoint <b>${data.waypoint_id}</b>`;
        case 'HUB_INBOUND_SCAN':
          return [
            `${buildHubInboundInfo(data)}`,
            `Inbounded at Hub <b>${data.hub_id}</b> from Shipper <b>${data.shipper_id}</b>`,
            (data && data.weight && formatMessage('Weight', data.weight.old_value, data.weight.new_value)) || '',
            (data && data.pricing_weight && formatMessage('Pricing Weight', data.pricing_weight.old_value, data.pricing_weight.new_value)) || '',
            (data && data.length && formatMessage('Length', data.length.old_value, data.length.new_value)) || '',
            (data && data.width && formatMessage('Width', data.width.old_value, data.width.new_value)) || '',
            (data && data.height && formatMessage('height', data.height.old_value, data.height.new_value)) || '',
            (data && data.parcel_size_id && formatMessage('Parcel Size ID', data.parcel_size_id.old_value, data.parcel_size_id.new_value)) || '',
            (data && !_.isUndefined(data.is_set_aside) && `<b>Set Aside:</b> ${data.is_set_aside ? 'true' : 'false'}`) || '',
          ].filter(Boolean).join('<br>');
        case 'UNSET_ASIDE':
          return [
            `From Shipper <b>${data.shipper_id}</b>`,
            (data && data.weight && formatMessage('Weight', data.weight.old_value, data.weight.new_value)) || '',
            (data && data.length && formatMessage('Length', data.length.old_value, data.length.new_value)) || '',
            (data && data.width && formatMessage('Width', data.width.old_value, data.width.new_value)) || '',
            (data && data.height && formatMessage('height', data.height.old_value, data.height.new_value)) || '',
            (data && data.parcel_size_id && formatMessage('Parcel Size ID', data.parcel_size_id.old_value, data.parcel_size_id.new_value)) || '',
          ].filter(Boolean).join('<br>');
        case 'PARCEL_ROUTING_SCAN':
          return [
            `${buildHubInboundInfo(data)}`,
            `Scanned at Hub <b>${data.hub_id}</b>`,
            (data && data.route_id && (`Route <b>${data.route_id}</b>`)) || '',
            (data && data.driver_id && (`Driver <b>${data.driver_id}</b>`)) || '',
          ].filter(Boolean).join('<br>');
        case 'ROUTE_INBOUND_SCAN':
          return `${buildHubInboundInfo(data)}<br/>Inbounded at Hub <b>${data.hub_id}</b> from Route <b>${data.route_id}</b> by Driver <b>${data.driver_id}</b>`;
        case 'ROUTE_TRANSFER_SCAN':
          return [
            (data && data.driver_id && `<b>Driver</b> changed from <i>${data.driver_id.old_value}</i> to <i>${data.driver_id.new_value}</i>`) || '',
            (data && data.route_id && `<b>Route</b> changed from <i>${data.route_id.old_value}</i> to <i>${data.route_id.new_value}</i>`) || '',
            (data && data.waypoint_id && `<b>Waypoint</b> changed from <i>${data.waypoint_id.old_value}</i> to <i>${data.waypoint_id.new_value}</i>`) || '',
          ].filter(Boolean).join('<br>');
        case 'FORCED_FAILURE':
          return (data && data.failure_reason_id && `Failure Reason Id: <b>${data.failure_reason_id}</b>`) || '';
        case 'OUTBOUND_SCAN':
          return [
            (data && data.hub_id && (`Scanned at Hub <b>${data.hub_id}</b>`)) || '',
            (data && data.route_id && (`Route <b>${data.route_id}</b>`)) || '',
          ].filter(Boolean).join('<br>');
        case 'ADD_TO_ROUTE':
          return [
            (data && data.route_id && (`Route <b>${data.route_id}</b>`)) || '',
            (data && data.old_waypoint_id && (`Old waypoint id <b>${data.old_waypoint_id}</b>`)) || '',
            (data && data.new_waypoint_id && (`New waypoint id <b>${data.new_waypoint_id}</b>`)) || '',
          ].filter(Boolean).join('<br>');
        case 'PULL_OUT_OF_ROUTE':
          return [
            (data && data.route_id && (`Route <b>${data.route_id}</b>`)) || '',
            (data && data.old_waypoint_id && (`Old waypoint id <b>${data.old_waypoint_id}</b>`)) || '',
            (data && data.new_waypoint_id && (`New waypoint id <b>${data.new_waypoint_id}</b>`)) || '',
          ].filter(Boolean).join('<br>');
        case 'UNASSIGNED_FROM_DP':
          return `${data.dp_name} (id: <b>${data.dp_id}</b>)`;
        case 'DRIVER_START_ROUTE':
          return [
            (data && data.driver_id && (`Driver <b>${data.driver_id}</b>`)) || '',
            (data && data.route_id && (`Route <b>${data.route_id}</b>`)) || '',
          ].filter(Boolean).join('<br>');
        case 'DELIVERY_SUCCESS':
          return [
            (data && data.driver_id && (`Driver <b>${data.driver_id}</b>`)) || '',
            (data && data.route_id && (`On Route <b>${data.route_id}</b>`)) || '',
            (data && data.waypoint_id && (`At Waypoint <b>${data.waypoint_id}</b>`)) || '',
            (data && data.cod && (`With COD <b>${data.cod}</b>`)) || '',
          ].filter(Boolean).join('<br>');
        case 'DELIVERY_FAILURE':
          return [
            (data && data.driver_id && (`Driver <b>${data.driver_id}</b>`)) || '',
            (data && data.route_id && (`On Route <b>${data.route_id}</b>`)) || '',
            (data && data.waypoint_id && (`At Waypoint <b>${data.waypoint_id}</b>`)) || '',
            (data && data.failure_reason_id && (`With Failure Reason <b>${data.failure_reason_id}</b>`)) || '',
          ].filter(Boolean).join('<br>');
        case 'PICKUP_SUCCESS':
          return [
            (data && data.driver_id && (`Driver <b>${data.driver_id}</b>`)) || '',
            (data && data.route_id && (`On Route <b>${data.route_id}</b>`)) || '',
            (data && data.waypoint_id && (`At Waypoint <b>${data.waypoint_id}</b>`)) || '',
            (data && data.reservation_id && (`For Reservation <b>${data.reservation_id}</b>`)) || '',
          ].filter(Boolean).join('<br>');
        case 'PICKUP_FAILURE':
          return [
            (data && data.driver_id && (`Driver <b>${data.driver_id}</b>`)) || '',
            (data && data.route_id && (`On Route <b>${data.route_id}</b>`)) || '',
            (data && data.waypoint_id && (`At Waypoint <b>${data.waypoint_id}</b>`)) || '',
            (data && data.reservation_id && (`For Reservation <b>${data.reservation_id}</b>`)) || '',
            (data && data.failure_reason_id && (`With Failure Reason <b>${data.failure_reason_id}</b>`)) || '',
          ].filter(Boolean).join('<br>');
        case 'TRANSFERRED_TO_THIRD_PARTY':
          return [
            (data && data.third_party_shipper_id && (`3PL id <b>${data.third_party_shipper_id}</b>`)) || '',
            (data && data.third_party_shipper_name && (`3PL name <b>${data.third_party_shipper_name}</b>`)) || '',
            (data && data.third_party_tracking_id && (`3PL Tracking id <b>${data.third_party_tracking_id}</b>`)) || '',
            (data && data.third_party_tracking_url && (`3PL Tracking URL <b>${data.third_party_tracking_url}</b>`)) || '',
            (data && data.third_party_transferred_timestamp && (`Transferred at <b>${nvDateTimeUtils.displayDateTime(moment.utc(data.third_party_transferred_timestamp))}</b>`)) || '',
          ].filter(Boolean).join('<br>');
        case 'TICKET_CREATED':
          msg = `Ticket ID: <b>${data.ticket_id}</b>` +
            `<br>Ticket Type: <b>${data.ticket_type}</b>`;
          if (!_.isUndefined(data.ticket_sub_type)) {
            msg += `<br>Ticket Sub Type: <b>${data.ticket_sub_type}</b>`;
          }
          msg += `<br>Investigating Dept.: <b>${data.investigating_dept}</b>` +
            `<br>Investigating Hub: <b>${getHubName(data.investigating_hub_id)}</b>`;
          return msg;
        case 'TICKET_UPDATED':
          return [
            (data && data.ticket_id && `Ticket ID: <b>${data.ticket_id}</b>`) || '',
            (data && `Ticket Status: <b>${data.status}</b>`) || '',
          ].filter(Boolean).join('<br>');
        case 'TICKET_RESOLVED':
          return [
            (data && data.ticket_id && `Ticket ID: <b>${data.ticket_id}</b>`) || '',
            (data && `Order Outcome: <b>${data.order_outcome}</b>`) || '',
          ].filter(Boolean).join('<br>');
        case 'REJECTED':
          return `Parcel was <b>Rejected at Pickup Location</b> <br/>
          Reason: <b>${data.reason}</b>`;
        case 'UPDATE_INSURANCE':
          return [
            (data && data.insurance && (`Insured value changed from <b>${(data.insurance.old_value) || 0}</b> to <b>${data.insurance.new_value || 0}</b>`)) || '',
          ].filter(Boolean).join('<br>');
        case 'UPDATE_TAGS':
          return [
            '<b>Tags updated</b>',
            (data && data.old_tags && (`from <b>${_.size(data.old_tags) > 0 ? _.join(data.old_tags, ', ') : 'none'}</b>`)) || '',
            (data && data.new_tags && (`To <b>${_.size(data.new_tags) > 0 ? _.join(data.new_tags, ', ') : 'none'}</b>`)) || '',
          ].filter(Boolean).join('<br>');
        default:
          return '';
      }

      function formatMessage(key, oldVal, newVal) {
        if (oldVal !== undefined && newVal !== undefined) {
          return `<b>${key}</b> changed from <i>${oldVal}</i> to <i>${newVal}</i>`;
        } else if (oldVal !== undefined) {
          return `<b>${key}</b> updated: removed old value <i>${oldVal}</i>`;
        } else if (newVal !== undefined) {
          return `<b>${key}</b> updated: assigned new value <i>${newVal}</i>`;
        }

        return '';
      }
    }

    function setRouteId(eventType, data) {
      switch (eventType) {
        case 'ROUTE_TRANSFER_SCAN':
          return (data && data.route_id && data.route_id.new_value) || null;
        default:
          return (data && data.route_id) || null;
      }
    }

    function setHubId(eventType, data) {
      switch (eventType) {
        case 'ADDED_TO_SHIPMENT':
        case 'REMOVED_FROM_SHIPMENT':
        case 'SHIPMENT_COMPLETED':
          return (data && data.curr_hub_id && getHubName(data.curr_hub_id)) || null;
        default:
          return (data && data.hub_id && getHubName(data.hub_id)) || null;
      }
    }
  }
}());
