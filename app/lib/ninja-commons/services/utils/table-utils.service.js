(function service() {
  angular
    .module('nvCommons.services')
    .factory('nvTableUtils', tableUtils);

  tableUtils.$inject = ['nvUtils', 'NgTableParams'];

  function tableUtils(nvUtils, NgTableParams) {
    return {

      /**
       * Generates a new NgTableParams object.
       *
       * @param source            - The array of objects to create a table for.
       * @param options
       *        options.count     - The initial page size.
       *        options.counts    - The page size buttons.
       *        options.filterOptions -
       *        options.columns   - The array of object keys in the {@code source} that we want columns for.
       *                            todo - currently does not support deep object keys, e.g. 'source.custom.deep'
       * @returns {NgTableParams} - The new NgTableParams object.
       */
      generate: (source, options) => {
        options = _.defaults(options || {}, {
          count: 5000,
          counts: [],
          columns: [],
        });

        const params = {
          count: options.count,
        };

        const settings = _.assign({
          counts: options.counts,
          dataset: nvUtils.mapToObjectWith(source, options.columns),
        }, _.isUndefined(options.filterOptions) ? {} : {
          filterOptions: options.filterOptions,
        });

        return new NgTableParams(params, settings);
      },

      /**
       * Adds a new object to the dataset on the NgTableParams object.
       * Reloads the NgTableParams object.
       *
       * @param ngTableParams     - The existing NgTableParams object.
       * @param source            - The source object to add to the dataset.
       * @param options
       *        options.columns   - The array of object keys in the {@code source} that we want columns for.
       *                            todo - currently does not support deep object keys, e.g. 'source.custom.deep'
       * @returns {NgTableParams} - The updated NgTableParams object.
       */
      addTo: (ngTableParams, source, options) => {
        options = _.defaults(options || {}, {
          columns: [],
        });

        ngTableParams.settings().dataset.push(_.pick(source, options.columns));
        ngTableParams.reload();
        return ngTableParams;
      },

      /**
       * Combines an object into the dataset on the NgTableParams object.
       * Reloads the NgTableParams object.
       *
       * @param ngTableParams     - The existing NgTableParams object.
       * @param source            - The source object to merge into the dataset.
       * @param options
       *        options.columns   - The array of object keys in the {@code source} that we want columns for.
       *                            todo - currently does not support deep object keys, e.g. 'source.custom.deep'
       *        options.mergeKey  - The object key used to compare the source object with objects in the dataset.
       *        options.mergeVal  - The value for which we decide to choose the object for merging.
       * @returns {NgTableParams} - The updated NgTableParams object.
       */
      mergeIn: (ngTableParams, source, options) => {
        options = _.defaults(options || {}, {
          columns: [],
          mergeKey: 'id',
          mergeVal: source.id,
        });

        nvUtils.mergeIn(
                    ngTableParams.settings().dataset,
                    _.pick(source, options.columns),
                    options.mergeKey,
                    options.mergeVal
                );
        ngTableParams.reload();
        return ngTableParams;
      },

      remove: (ngTableParams, key, value) => {
        _.remove(ngTableParams.settings().dataset, (obj) => {
          return obj[key] === value;
        });
        ngTableParams.reload();
        return ngTableParams;
      },

      /**
       * TODO Refactor this
       *
       * Currently this is a temporary util function that alleviates the pain of setting up the boilerplate codes
       * to have a custom filter/sorting table like in `driver-strength` page.
       *
       * @param  {array<object>}   data             pre-filter, pre-sorted raw table data
       * @param  {object}          sortOptions      sort options
       * @param  {array<function>} filterFunctions  filter functions that will be AND together, eg:
       *                                                element from data is present in the tableData,
       *                                                if and only if all the filterFunctions return true when the element is passed in as the parameter
       * @return {object}                     table param
       */
      getNVTableParam: (data, sortOptions, filterFunctions) => {
        const table = {};
        table._data = data;
        table._currSortIdx = 0;
        table._currSortOpt = null;
        table._sortByTypes = [undefined, 'asc', 'desc'], // no-sorting, ascending, descending
                table._sortOptions = sortOptions;
        table._filterFunctions = filterFunctions;

        table.tableData = table._data;
        table.isSortActive = isSortActive;
        table.showSortIcon = showSortIcon;
        table.sort = sort;
        table.filterAll = filterAll;

        return table;

                // /////////////////////////////////////////

        function isSortActive(option) {
          return table._currSortOpt === option && table._currSortIdx > 0;
        }
        function showSortIcon(option) {
          return table._currSortOpt === option ? (table._currSortIdx === 1 ? 'fa-sort-asc' : 'fa-sort-desc') : 'fa-sort';
        }
        function sort(option) {
          table._currSortIdx = table._currSortOpt === option ? (table._currSortIdx + 1) % 3 : 1;
          table._currSortOpt = table._currSortIdx === 0 ? null : option;
          table.tableData = sortTableData(table.tableData);
        }
        function sortTableData(tableData) {
          return !table._currSortOpt
                        ? _.orderBy(tableData, 'id')
                        : table._sortOptions[table._currSortOpt](tableData, table._sortByTypes[table._currSortIdx]);
        }
        function filterAll() {
          const filteredData = _.reduce(table._filterFunctions, reduceFn, table._data);
          table.tableData = sortTableData(filteredData);

          function reduceFn(acc, filterFn) {
            return _.filter(acc, filterFn);
          }
        }
      },

      getBasicSortFunction: function (columnName) {
        return function (tableData, orderBy) {
          return _.orderBy(tableData, columnName, orderBy);
        };
      },

    };
  }
}());
