(function() {
    'use strict';

    angular
        .module('nvCommons.services')
        .factory('nvCoupledListUtils', nvCoupledListUtils);

    function nvCoupledListUtils() {

        var self = {

            transfer: function(src, dst, predicate, options) {
                predicate = predicate || _.stubTrue;

                Array.prototype.push.apply(dst, _.remove(src, predicate));

                options = options || {};

                if (options.sort) {
                    dst.sort(options.sort);
                }
            },

            transferAll: function(src, dst, options) {
                self.transfer(src, dst, undefined, options);
            }

        };

        return self;

    }

})();
