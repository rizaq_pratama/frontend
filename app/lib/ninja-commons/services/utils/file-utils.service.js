(function fileUtils() {
  angular
        .module('nvCommons.services')
        .factory('nvFileUtils', nvFileUtils);

  nvFileUtils.$inject = ['$window', '$q', 'nvToast'];

  function nvFileUtils($window, $q, nvToast) {
    return {

      FILETYPE_EXCEL: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
      FILETYPE_PDF: 'application/pdf',
      FILETYPE_PNG: 'image/png',

      getBlobFromData: getBlobFromData,
      getURLFromData: getURLFromData,

      view: view,
      download: download,
      downloadJson: downloadJson,
      downloadCsv: downloadCsv,
      readJson: readJson,

    };

    // //////////////////////

    /**
     * Converts byte array to blob
     * @param  {ByteArray}  data    byte array, eg: response.data from $http request
     * @param  {String}     type    file type of the byte array, e.g: nvFileUtils.FILETYPE_PDF
     * @return {Blob}               blob object of the byte array
     */
    function getBlobFromData(data, type) {
      return new Blob([data], { type: type });
    }

    /**
     * Converts byte array to blob and return the DOMString of that blob.
     * @param  {ByteArray}  data    byte array, eg: response.data from $http request
     * @param  {String}     type    file type of the byte array, e.g: nvFileUtils.FILETYPE_PDF
     * @return {DOMString}          DOMString
     */
    function getURLFromData(data, type) {
      return $window.URL.createObjectURL(getBlobFromData(data, type));
    }

    /**
     * Open the url on a new window, @see getUrlFromData(data, type)
     * @param  {DOMString} url domstring to be opened
     */
    function view(url) {
      $window.open(url);
    }

    /**
     * Download the url with filename
     * @param  {DOMString} url      url domstring to be downloaded, @see getUrlFromData(data, type)
     * @param  {String}    filename file name of the downloaded file, default to `download`
     */
    function download(url, filename) {
            // create a dummy link to hold the pdf file
      const a = document.createElement('a');
      document.body.appendChild(a);
      a.style.display = 'none';
      a.download = filename || 'download';
      a.href = url;
      // check if use different origin
      // will open in new tab as download attribute only work on same origin resource
      // https://developers.google.com/web/updates/2018/02/chrome-65-deprecations#block_cross-origin_wzxhzdk5a_download
      if (!_.startsWith(url, window.location.origin)) {
        a.target = '_blank';
      }
      a.click();
            // clean up
      document.body.removeChild(a);
    }

    /**
     * Download the JSON object with filename.
     * @param obj The JSON object to be downloaded.
     * @param filename The name of the file to be downloaded.
     */
    function downloadJson(obj, filename) {
            // create a dummy link to hold the json file
      const a = document.createElement('a');
      document.body.appendChild(a);
      a.style.display = 'none';
      a.download = filename || `${moment().format('YYYYMMDD_HHmmss')}.json`;
      a.href = `data:text/json;charset=utf-8,${encodeURIComponent(JSON.stringify(obj))}`;
      a.click();
            // clean up
      document.body.removeChild(a);
    }

    /**
     * Download the csv with filename.
     * @param content The csv string content to be downloaded.
     * @param filename The name of the file to be downloaded.
     */
    function downloadCsv(content, filename) {
      const BOM = '\ufeff';
      const csv = BOM + content;
      const blob = new Blob([csv], {
        type: 'application/csv;charset=utf-8;',
      });
      if (window.navigator.msSaveBlob) {
        // FOR IE BROWSER
        navigator.msSaveBlob(blob, filename);
      } else {
        // FOR OTHER BROWSERS
        const link = document.createElement('a');
        const csvUrl = URL.createObjectURL(blob);
        link.href = csvUrl;
        link.style = 'visibility:hidden';
        link.download = filename;
        document.body.appendChild(link);
        link.click();
        document.body.removeChild(link);
      }
    }

    /**
     * Reads the File blob and parses it to a JSON object.
     * Normally used with the <nv-button-file-picker> directive.
     * @param file The File blob to read.
     * @returns {Promise} A promise object for handling the result of the read operation.
     */
    function readJson(file) {
      const deferred = $q.defer();
      const fileReader = new FileReader();

      fileReader.onload = function onload() {
        try {
          deferred.resolve(JSON.parse(fileReader.result));
        } catch (e) {
          nvToast.error(e, 'File Reading Error');
          deferred.reject(e);
        }
      };

      fileReader.onerror = function onerror() {
        nvToast.error(`Failed to read file: ${file.name}`, 'File Reading Error');
        deferred.reject(fileReader.error);
      };

      fileReader.onabort = function onabort() {
        nvToast.info(`Aborted reading file: ${file.name}`, 'File Reading Abort');
        deferred.reject();
      };

      fileReader.readAsText(file);

      return deferred.promise;
    }
  }
}());
