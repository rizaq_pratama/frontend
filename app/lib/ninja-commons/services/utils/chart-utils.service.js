(function() {
    'use strict';

    angular
        .module('nvCommons.services')
        .factory('nvChartUtils', chartUtils);

    function chartUtils() {

        var HRS_FORMAT = 'D MMM, ha';
        var DAY_FORMAT = 'D MMM, ddd';
        var WKS_FORMAT = 'wo [week of] YYYY';
        var MTH_FORMAT = 'MMM YYYY';

        var self = {

            HRS_FORMAT: HRS_FORMAT,
            DAY_FORMAT: DAY_FORMAT,
            WKS_FORMAT: WKS_FORMAT,
            MTH_FORMAT: MTH_FORMAT,

            generateHrsLabels: function(frMoment, toMoment) {
                return generateLabels(frMoment, toMoment, 'h', HRS_FORMAT);
            },

            generateDayLabels: function(frMoment, toMoment) {
                return generateLabels(frMoment, toMoment, 'd', DAY_FORMAT);
            },

            generateWksLabels: function(frMoment, toMoment) {
                return generateLabels(frMoment, toMoment, 'w', WKS_FORMAT);
            },

            generateMthLabels: function(frMoment, toMoment) {
                return generateLabels(frMoment, toMoment, 'M', MTH_FORMAT);
            },

            lineChartWithRawData: function(data, xAxisLabels, datasetProperties, datasetGroupingFunc, xAxisGroupingFunc) {
                var groupedData = self.groupData(data, xAxisLabels, datasetGroupingFunc, xAxisGroupingFunc);
                return self.lineChartWithGroupedData(groupedData, xAxisLabels, datasetProperties);
            },

            lineChartWithGroupedData: function(groupedData, xAxisLabels, datasetProperties) {
                return {
                    labels: xAxisLabels,
                    datasets: _(datasetProperties)
                        .map(function(dataset) {
                            return _.assign(dataset, {data: groupedData[dataset.label]});
                        })
                        .value()
                };
            },

            groupData: function(data, xAxisLabels, datasetGroupingFunc, xAxisGroupingFunc) {
                return _(data)
                    .groupBy(datasetGroupingFunc)
                    .mapValues(function(dataSubset) {
                        var countsOf = _.countBy(dataSubset, xAxisGroupingFunc);
                        return _.map(xAxisLabels, function(label) {
                            return countsOf[label] || 0;
                        });
                    }).value();
            }

        };

        return self;

        function generateLabels(frMoment, toMoment, unit, format) {
            var diff = toMoment.diff(frMoment, unit);
            // make sure that the resulting array includes the time at frMoment
            var currMoment = frMoment.clone().subtract(1, unit);
            return _.times(diff + 1, function() {
                return currMoment.add(1, unit).format(format);
            });
        }

    }

})();
