(function service() {
  /**
   * A set of methods used to extend an object.
   * Mostly useful for formatting fields for display purposes.
   * These methods mutate the original objects.
   */
  angular
    .module('nvCommons.services')
    .factory('nvExtendUtils', extendUtils);

  extendUtils.$inject = ['$filter', 'nvDateTimeUtils'];

  function extendUtils($filter, nvDateTimeUtils) {
    return {

      /**
       * Adds a field with the lowercase conversion of the given object property.
       *
       * Example usage:
       *
       *   addLowercase([
       *     {apple: 'Bear', cat: 'Dog'},
       *     {apple: 'Brown Monkey', cat: 'Donkey'}
       *   ], 'apple')
       *
       *   -> [{_lowercaseApple: 'bear', apple: 'Bear', cat: 'Dog'},
       *       {_lowercaseApple: 'brown monkey', apple: 'Brown Monkey', cat: 'Donkey'}]
       *
       * @param src - An object or a list of objects.
       * @param property - The property of the object to perform lowercase conversion.
       * @returns {*} - The mutated source with the added lowercase field.
       */
      addLowercase: (src, prop) => {
        return _.isPlainObject(src) ? extend([src])[0] : extend(src);

        function extend(list) {
          const key = `_lowercase${prop.charAt(0).toUpperCase()}${prop.slice(1)}`;
          return _.map(list, obj => _.assign(obj, { [key]: obj[prop].toLowerCase() }));
        }
      },

      /**
       * Adds a field with the concatenated address.
       *
       * Example usage:
       *
       *   addDisplayAddress([
       *     {address1: '30', address2: 'Jalan Kilang Barat', postcode: '159363',
       *     country: 'Singapore'},
       *     {address1: '91', address2: 'Cavenagh Rd', city: 'Newton', postcode: '229629',
       *     country: 'Singapore'}
       *   ])
       *
       *   -> [{_address: '30 Jalan Kilang Barat 159363 Singapore', address1: '30',
       *       address2: 'Jalan Kilang Barat', postcode: '159363', country: 'Singapore'},
       *       {_address: '91 Cavenagh Rd Newton 229629 Singapore', address1: '91',
       *       address2: 'Cavenagh Rd', city: 'Newton', postcode: '229629', country: 'Singapore'}]
       *
       * @param src - An object or a list of objects.
       * @returns {*} - The mutated source with the added address field.
       */
      addDisplayAddress: (src) => {
        return _.isPlainObject(src) ? extend([src])[0] : extend(src);

        function extend(list) {
          return _.map(list, obj =>
            _.assign(obj, { _address: getDisplayAddress(obj) })
          );
        }

        function getDisplayAddress(obj) {
          return _.compact([obj.address1, obj.address2, obj.neighbourhood, obj.locality, obj.region, obj.city, obj.postcode, obj.country]).join(' ');
        }
      },

      addValueForBoolean: (src, prop, options = {}) => {
        return _.isPlainObject(src) ? extend([src])[0] : extend(src);

        function extend(list) {
          const key = `_${prop}`;
          const trueObj = { [key]: options.true || true };
          const falseObj = { [key]: options.false || false };
          return _.map(list, obj =>
            _.assign(obj, obj[prop] ? trueObj : falseObj)
          );
        }
      },

      addFloat: (src, prop, fractionSize = 2) => {
        return _.isPlainObject(src) ? extend([src])[0] : extend(src);

        function extend(list) {
          const key = `_${prop}`;
          return _.map(list, obj =>
            _.assign(obj, { [key]: $filter('number')(obj[prop], fractionSize) })
          );
        }
      },

      addLatLng: (src) => {
        return _.isPlainObject(src) ? extend([src])[0] : extend(src);

        function extend(list) {
          return _.map(list, obj =>
            _.assign(obj, { _latlng: getDisplayLatLng(obj) })
          );
        }

        function getDisplayLatLng(obj) {
          if (_.isNull(obj.latitude) || angular.isUndefined(obj.latitude) ||
              _.isNull(obj.longitude) || angular.isUndefined(obj.longitude)) {
            return 'No Lat/Long';
          }
          return `(${obj.latitude}, ${obj.longitude})`;
        }
      },

      /**
       * Adds a field with the moment object of the given object property.
       * Always treat the date string of the object property as utc date.
       *
       * Example usage:
       *
       *   addMoment([
       *     {updatedAt: '2016-03-03T14:39:07Z', createdAt: '2016-02-28T14:50:32Z', cat: 'Dog'},
       *     {updatedAt: '2016-02-28T14:50:32Z', createdAt: '2014-12-03T14:39:07Z', cat: 'Donkey'}
       *   ], ['createdAt', 'updatedAt'])
       *
       *   -> [{_momentUpdatedAt: [moment object], updatedAt: '2016-03-03T14:39:07Z',
       *       _momentCreatedAt: [moment object], createdAt: '2016-02-28T14:50:32Z', cat: 'Dog'},
       *       {_momentUpdatedAt: [moment object], updatedAt: '2016-02-28T14:50:32Z',
       *       _momentCreatedAt: [moment object], createdAt: '2014-12-03T14:39:07Z', cat: 'Donkey'}]
       *
       * @param src - An object or a list of objects.
       * @param prop - The property or a list of properties of the object to
       *               perform moment conversion.
       */
      addMoment: (src, prop, timezone = 'utc') => {
        return _.isPlainObject(src) ? extend([src], prop)[0] : extend(src, prop);

        function extend(list) {
          const keys = _.map(_.castArray(prop), (p) => {
            const key = `_moment${p.charAt(0).toUpperCase()}${p.slice(1)}`;
            return { momentKey: key, strKey: p };
          });

          return _.map(list, (obj) => {
            const extension = {};
            _.forEach(keys, (key) => {
              extension[key.momentKey] = nvDateTimeUtils.toMoment(obj[key.strKey], timezone);
            });
            return _.assign(obj, extension);
          });
        }
      },

      map: (src, ...args) => {
        return _.isPlainObject(src) ? _.map([src], ...args)[0] : _.map(src, ...args);
      },

      /**
       * _.camelCase keys for deep nested objects
       * @param  {Object} [data={}] data
       * @return {Object}           data with keys -> camelCase keys
       */
      convertKeysToCamelCase: (data) => {
        return (function toCamelCase(data) {
          if (_.isArray(data)) {
            return _.map(data, toCamelCase);
          } else {
            return _(data)
              .mapValues(value => _.isObject(value) ? toCamelCase(value) : value)
              .mapKeys((value, key) => _.camelCase(key))
              .value();
          }
        })(data)
      },

      /**
       * _.camelCase keys for deep nested objects
       * @param  {Object} [data={}] data
       * @return {Object}           data with keys -> camelCase keys
       */
      convertKeysToSnakeCase: (data) => {
        return (function toSnakeCase(data) {
          if (!_.isObject(data)) {
            return data;
          } else if (_.isArray(data)) {
            return _.map(data, toSnakeCase);
          } else {
            return _(data)
              .mapValues(value => _.isObject(value) ? toSnakeCase(value) : value)
              .mapKeys((value, key) => _.snakeCase(key))
              .value();
          }
        })(data)
      },
    };
  }
}());
