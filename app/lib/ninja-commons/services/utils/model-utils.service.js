(function() {
    'use strict';

    angular
        .module('nvCommons.services')
        .factory('nvModelUtils', modelUtils);

    modelUtils.$inject = ['nvUtils'];

    function modelUtils(nvUtils) {

        return {

            fromBoolToYesNoBitString: function(bool) {
                return bool ? '10' : '01';
            },

            fromYesNoBitStringToBool: function(yesNoBitString) {
                return yesNoBitString === '10';
            },

            fromYesNoBitStringToNumBit: function(yesNoBitString) {
                return yesNoBitString === '10' ? 1 : 0;
            },

            pickFields: function(fields, props, options) {
                var pickedFields = _(fields).pick(props).cloneDeep();

                var callbacks = [];

                options = options || {};

                if (options.setDefaultValues) {
                    callbacks.push(setDefaultValueCallback);
                }

                if (options.setValues) {
                    callbacks.push(setValueCallback);
                }

                if (options.setArrayValues) {
                    callbacks.push(setArrayValueCallback);
                }

                if (options.setOverrideValues) {
                    callbacks.push(setOverrideValueCallback);
                }

                if (callbacks.length === 0) {
                    return pickedFields;
                }

                // invoke the selected callbacks for each of the picked fields
                _.forEach(pickedFields, function(field, key) {
                    _.forEach(callbacks, function(callback) {
                        callback.call(null, field, key, options.model);
                    });
                });

                return pickedFields;
            },
            /**
             * Recursively search through the fields description and assign default value to the objects
             * @param {[type]} fields  model description
             * @param {[type]} objects object or objects array to be modified
             */
            setDefaultsToObject: function(fields, objects){
                //get default to be set
                var defaults = {};
                var objectsArray = angular.isArray(objects) ? objects : [objects];
                findDefault(fields, '');

                _.each(defaults, function(defaultValue, key){
                    _.each(objectsArray, function(object){
                        _.set(object, key, _.get(object, key) || defaultValue);
                    });
                });

                return objects;

                function findDefault(fields, prefix) {
                    _.each(fields, function(v, k){
                        if(angular.isObject(v)){
                            if(angular.isDefined(v.defaultValue)){
                                defaults[prefix + k] = v.defaultValue;
                            }
                            findDefault(v, prefix + k + '.');
                        }
                    });
                }
            },

        };

        function setDefaultValueCallback(field) {
            // assign the field value based on the default value, if it exists
            if (!_.isUndefined(field.defaultValue)) {
                field.value = field.defaultValue;
            }

            if (field._deep) {
                _.forEach(field, function(value){
                    setDefaultValueCallback(value);
                });
            }
        }

        function setValueCallback(field, key, model) {
            // if we are at the leaf node of the object tree,
            // then simply assign the field value based on the value function
            if (!_.isUndefined(field.displayName)) {
                var valueFn = field.deepValueFn || defaultValueFn;
                var value = valueFn(model, key);
                field.value = _.isUndefined(value) || _.isNull(value) ? field.value : value;
                return;
            }

            if (!field._array) {
                // if we are not at the leaf node of the object tree,
                // then recursively traverse depth-first down the object tree
                _.forEach(field, function(deepValue, deepKey) {
                    setValueCallback(deepValue, key + '.' + deepKey, model);
                });
            }
        }

        function setArrayValueCallback(field, key, model) {
            if (field._array) {
                var valueFn = field.deepValueFn || defaultValueFn;
                field._values = _.cloneDeep(valueFn(model, key)) || field._values;
            }
        }

        function setOverrideValueCallback(field) {
            // assign the field value based on the override value, if it exists
            if (!_.isUndefined(field.overrideValue)) {
                field.value = field.overrideValue;
            }
        }

        function defaultValueFn(model, key) {
            return nvUtils.getOrElse(model, key, undefined);
        }

    }

})();
