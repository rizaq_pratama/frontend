(function service() {
  angular.module('nvCommons.services')
    .factory('nvBackendFilterUtils', filterUtils);

  filterUtils.$inject = [
    '$q',
    'nvDateTimeUtils',
    'nvCoupledListUtils',
    'nvFilterNumberBoxService',
    'nvTimezone',
  ];

  function filterUtils(
    $q,
    nvDateTimeUtils,
    nvCoupledListUtils,
    nvFilterNumberBoxService,
    nvTimezone
  ) {
    const BOOLEAN = 0;
    const DATE = 1;
    const OPTIONS = 2;
    const NUMBER = 3;
    const TEXT = 4;
    const TIME = 5;
    const RANGE = 6;
    const TEXT_FREE = 7;
    const AUTOCOMPLETE = 8;

    const util = {
      FILTER_BOOLEAN: BOOLEAN,
      FILTER_DATE: DATE,
      FILTER_OPTIONS: OPTIONS,
      FILTER_NUMBER: NUMBER,
      FILTER_TEXT: TEXT,
      FILTER_TIME: TIME,
      FILTER_RANGE: RANGE,
      FILTER_TEXT_FREE: TEXT_FREE,
      FILTER_AUTOCOMPLETE: AUTOCOMPLETE,

      constructParams: constructParams,
      getAddPresetFields: getAddPresetFields,
      convertPresetToFilter: convertPresetToFilter,
      getDescription: getDescription,
      toString: toString,
    };

    return util;

    // ////////////////////////////////////////////////
    // function details
    // ////////////////////////////////////////////////

    function constructParams(selectedFilters) {
      const params = {};

      _.forEach(selectedFilters, filter => handleFilter(
        filter,
        constructBoolean,
        constructDate,
        constructOption,
        constructNumber,
        constructText,
        constructTime,
        constructRange,
        constructTextFree,
        constructAutocomplete)
      );

      return params;

      // Function definitions
      function constructBoolean(filter) {
        // String Form: e.g. filter.key = 'archived'
        if (_.isString(filter.key)) {
          params[filter.key] = (filter.booleanModel === '10');
        }

        // Array Form: e.g. filter.key = ['archived', 'unarchived']
        if (_.isArray(filter.key)) {
          _.forEach(filter.key, (key, index) => {
            if (filter.booleanModel[index] === '1') {
              params[key] = true;
            }
          });
        }
      }

      function constructText(filter) {
        params[filter.key] = filter.model;
      }

      function constructTextFree(filter) {
        params[filter.key] = _.map(filter.model, mdl => (mdl.displayName));
      }

      function constructAutocomplete(filter) {
        if (isValidOption(filter)) {
          params[filter.key] = _(filter.selectedOptions)
            .flatMap(option => transformOption(filter, option))
            .concat(params[filter.key] || [])
            .value();
        }
      }

      function constructDate(filter) {
        params[filter.key.from] = filter.transformFn(filter.fromModel, 'from');

        if (!_.isUndefined(filter.key.to)) {
          params[filter.key.to] = filter.transformFn(filter.toModel, 'to');
        }
      }

      function constructOption(filter) {
        if (isValidOption(filter)) {
          params[filter.key] = _(filter.selectedOptions)
            .flatMap(option => transformOption(filter, option))
            .concat(params[filter.key] || [])
            .value();
        }
      }

      function constructNumber(filter) {
        if (isValidNumber(filter)) {
          params[filter.key] = filter.transformFn(filter.options);
        }
      }

      function constructTime(filter) {
        params[filter.key.from] = filter.transformFn(filter.fromModel, 'from');
        params[filter.key.to] = filter.transformFn(filter.toModel, 'to');
      }

      function constructRange(filter) {
        params[filter.key.from] = filter.fromModel;
        params[filter.key.to] = filter.toModel;
      }
    }

    function getAddPresetFields(name, selectedFilters) {
      const data = { name: name };

      _.forEach(selectedFilters, filter =>
        handleFilter(filter, getBoolean, getDate,
            getOption, getNumber, getText, getTime, getRange, getTextFree, getAutocomplete));

      return data;

      // Function definitions
      function getBoolean(filter) {
        data[filter.presetKey] = (filter.booleanModel === '10');
      }

      function getDate(filter) {
        data[filter.presetKey.from] = nvDateTimeUtils.displayISO(filter.fromModel, 'utc');
        data[filter.presetKey.to] = nvDateTimeUtils.displayISO(filter.toModel, 'utc');
      }

      function getOption(filter) {
        if (isValidOption(filter)) {
          data[filter.presetKey] = _(filter.selectedOptions)
            .flatMap(option => transformOption(filter, option))
            .concat(data[filter.presetKey] || [])
            .value();
        }
      }
      function getText(filter) {
        data[filter.presetKey] = filter.model;
      }

      function getTextFree(filter) {
        data[filter.presetKey] = filter.model;
      }

      function getAutocomplete(filter) {
        if (isValidOption(filter)) {
          data[filter.presetKey] = _(filter.selectedOptions)
            .flatMap(option => transformOption(filter, option))
            .concat(data[filter.presetKey] || [])
            .value();
        }
      }

      function getNumber(filter) {
        if (isValidNumber(filter)) {
          data[filter.key] = filter.transformFn(filter.options);
        }
      }

      function getTime(filter) {
        data[filter.presetKey.from] = nvDateTimeUtils.displayISO(filter.fromModel, 'utc');
        data[filter.presetKey.to] = nvDateTimeUtils.displayISO(filter.toModel, 'utc');
      }

      function getRange(filter) {
        data[filter.presetKey.from] = filter.fromModel;
        data[filter.presetKey.to] = filter.toModel;
      }
    }

    function convertPresetToFilter(possibleFilters, selectedFilters, preset) {
      let counter = 0;

      // move all selectedFilters back to possibleFilters
      nvCoupledListUtils.transferAll(selectedFilters, possibleFilters);

      _.forEach(possibleFilters, (filter) => {
        filter.isSelected = false;
        $q.when(handleFilter(
          filter,
          convertBooleanPreset,
          convertDatePreset,
          convertOptionPreset,
          convertNumberPreset,
          convertText,
          convertTimePreset,
          convertRangePreset,
          convertTextFree,
          convertAutocompletePreset
        )).then(transferFilters);
      });

      function transferFilters() {
        counter += 1;

        if (counter >= _.size(possibleFilters)) {
          nvCoupledListUtils.transfer(
            possibleFilters,
            selectedFilters,
            filter => filter.isSelected === true
          );
        }
      }

      // Function definitions
      function convertBooleanPreset(filter) {
        if (preset[filter.presetKey] != null) {
          if (preset[filter.presetKey] === true) {
            filter.booleanModel = '10';
          } else {
            filter.booleanModel = '01';
          }
          filter.isSelected = true;
        }
      }
      function convertText(filter) {
        const content = preset[filter.presetKey];
        if (content) {
          filter.model = content;
          filter.isSelected = true;
        }
      }

      function convertTextFree(filter) {
        const content = preset[filter.presetKey];
        if (content) {
          filter.model = content;
          filter.isSelected = true;
        }
      }

      function convertAutocompletePreset(filter) {
        const content = preset[filter.presetKey];
        filter.selectedOptions = [];

        if (_.size(content) > 0) {
          filter.isSelected = true;

          const param = {};
          param[filter.backendKey] = preset[filter.presetKey];
          return filter.callback(param).then(convertPromisePreset);
        }

        return null;

        function convertPromisePreset(options) {
          filter.selectedOptions = options;
        }
      }

      function convertDatePreset(filter) {
        if (preset[filter.presetKey.from] != null && preset[filter.presetKey.to] != null) {
          filter.fromModel = moment(preset[filter.presetKey.from]).toDate();
          filter.toModel = moment(preset[filter.presetKey.to]).toDate();
          filter.isSelected = true;
        }
      }

      function convertTimePreset(filter) {
        if (preset[filter.presetKey.from] != null && preset[filter.presetKey.to] != null) {
          filter.fromModel = moment(preset[filter.presetKey.from])
            .tz(nvTimezone.getOperatorTimezone());
          filter.toModel = moment(preset[filter.presetKey.to])
            .tz(nvTimezone.getOperatorTimezone());
          filter.isSelected = true;
        }
      }

      function convertOptionPreset(filter) {
        let possibleOptions = filter.possibleOptions;
        if (_.isFunction(filter.possibleOptions)) {
          possibleOptions = filter.possibleOptions();
        }

        const promise = $q.when(possibleOptions);
        return promise.then(convertPromisePreset);

        function convertPromisePreset(options) {
          if (!_.isEmpty(preset[filter.presetKey])) {
            filter.isSelected = true;
          }

          filter.possibleOptions = options;
          nvCoupledListUtils.transferAll(filter.selectedOptions, filter.possibleOptions);

          _.forEach(preset[filter.presetKey], (filterObject) => {
            nvCoupledListUtils.transfer(
              filter.possibleOptions,
              filter.selectedOptions,
              object => comparator(object, filterObject));
          });
        }

        function comparator(object, filterObject) {
          return _.isFunction(filter.comparator) ?
            filter.comparator(object, filterObject) : object.id === filterObject;
        }
      }

      function convertNumberPreset(filter) {
        if (preset[filter.presetKey]) {
          const p = preset[filter.presetKey];
          if (nvFilterNumberBoxService.isPresetValid(p)) {
            const options = filter.options;

            options.type = _.find(
              nvFilterNumberBoxService.FILTER_TYPES,
              t => (t.value.name.toUpperCase() === p.type.toUpperCase())
            ).value;

            if (options.type.name === nvFilterNumberBoxService.FILTER_TYPES.RANGE.value.name) {
              options.from = p.from;
              options.to = p.to;
            } else if (options.type.name ===
              nvFilterNumberBoxService.FILTER_TYPES.VALUE.value.name) {
              options.operator = _.find(
                nvFilterNumberBoxService.FILTER_OPERATORS,
                op => (op.value.name === p.operator)
              ).value;
              options.operand = p.operand;
            }
            filter.isSelected = true;
          }
        }
      }

      function convertRangePreset(filter) {
        if (preset[filter.presetKey.from] != null && preset[filter.presetKey.to] != null) {
          filter.fromModel = preset[filter.presetKey.from];
          filter.toModel = preset[filter.presetKey.to];
          filter.isSelected = true;
        }
      }
    }

    function getDescription(noDescFilter) {
      return handleFilter(
        noDescFilter,
        getBooleanDescription,
        getDateDescription,
        getOptionDescription,
        getNumberDescription,
        getTextDescription,
        getTimeDescription,
        getRangeDescription,
        getTextFreeDescription,
        getAutocompleteDescription
      );

      // Function definitions
      function getBooleanDescription(filter) {
        return `${filter.booleanModel === '10' ? 'Show' : 'Hide'}`;
      }

      function getDateDescription(filter) {
        if (filter.isRange) {
          return `${nvDateTimeUtils.displayDate(filter.fromModel)} to
            ${nvDateTimeUtils.displayDate(filter.toModel)}`;
        }
        return `${nvDateTimeUtils.displayDate(filter.fromModel)}`;
      }

      function getOptionDescription(filter) {
        if (isValidOption(filter)) {
          const getName = _.property(filter.searchBy || 'displayName');
          const names = _.map(filter.selectedOptions, option => getName(option)).join(', ');
          return `${names}`;
        }

        return '';
      }

      function getTextDescription(filter) {
        return filter.model;
      }

      function getTextFreeDescription(filter) {
        return _.join(_.map(filter.model, mdl => mdl.displayName), ',');
      }

      function getAutocompleteDescription(filter) {
        return _.join(_.map(filter.selectedOptions, mdl => mdl.displayName), ',');
      }

      function getNumberDescription(filter) {
        if (isValidNumber(filter)) {
          const code = filter.options.type.code;
          if (code === 0) {
            // range
            return `${filter.options.from} ${filter.options.unit || ''} to ${filter.options.to} ${filter.options.unit || ''}`;
          }
          // value
          return `${filter.options.operator.name} ${filter.options.operand} ${filter.options.unit || ''}`;
        }
        return '';
      }

      function getTimeDescription(filter) {
        return `${nvDateTimeUtils.displayDateTime(filter.fromModel)} to
          ${nvDateTimeUtils.displayDateTime(filter.toModel)}`;
      }

      function getRangeDescription(filter) {
        return `${filter.fromModel} to 
          ${filter.toModel}`;
      }
    }

    function toString(filter) {
      return `${filter.mainTitle}: ${getDescription(filter)}`;
    }

    // ////////////////////////////////////////////////
    // Helper Functions
    // ////////////////////////////////////////////////

    function handleFilter(
      filter,
      handleBoolean,
      handleDate,
      handleOption,
      handleNumber,
      handleText,
      handleTime,
      handleRange,
      handleTextFree,
      handleAutocomplete
    ) {
      if (filter == null) {
        return null;
      }

      if (filter.type === DATE) {
        return handleDate(filter);
      } else if (filter.type === BOOLEAN) {
        return handleBoolean(filter);
      } else if (filter.type === OPTIONS) {
        return handleOption(filter);
      } else if (filter.type === NUMBER) {
        return handleNumber(filter);
      } else if (filter.type === TEXT) {
        return handleText(filter);
      } else if (filter.type === TIME) {
        return handleTime(filter);
      } else if (filter.type === RANGE) {
        return handleRange(filter);
      } else if (filter.type === TEXT_FREE) {
        return handleTextFree(filter);
      } else if (filter.type === AUTOCOMPLETE) {
        return handleAutocomplete(filter);
      } return null;
    }

    function isValidOption(filter) {
      return !_.isEmpty(filter.selectedOptions);
    }

    function transformOption(filter, option) {
      if (_.isFunction(filter.transformFn)) {
        return filter.transformFn(option);
      } return option.id;
    }

    function isValidNumber(filter) {
      const p = _.defaults(filter.options, { type: { code: null } });

      if (p.type.code ===
        nvFilterNumberBoxService.FILTER_TYPES.RANGE.value.code) {
        if ((typeof p.from !== 'undefined') && (typeof p.to !== 'undefined')) {
          if (p.from > p.to) {
            const temp = p.from;
            p.from = p.to;
            p.to = temp;
          }
          return true;
        }
      } else if (p.type.code === nvFilterNumberBoxService.FILTER_TYPES.VALUE.value.code) {
        if (p.operator && (typeof p.operand !== 'undefined')) {
          if ((p.operator.name === '<' || p.operator.name === '=<') &&
          ((p.operand - filter.minValue) < Number.EPSILON)) {
            p.operand = filter.maxValue;
          }
          return true;
        }
        return false;
      }
      return false;
    }
  }
}());
