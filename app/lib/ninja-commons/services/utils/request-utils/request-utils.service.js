(function service() {
  angular
        .module('nvCommons.services')
        .factory('nvRequestUtils', requestUtils);

  requestUtils.$inject = ['$q', 'nvAuth', 'nvToast', 'nvUtils'];

  function requestUtils($q, nvAuth, nvToast, nvUtils) {
    const TOAST_TITLE = 'Network Request Error';
    const TOAST_STICK_OPTIONS = { timeOut: 0, extendedTimeOut: 0, tapToDismiss: false };
    const DEFAULT_ERR_CODE = '-1';
    const DEFAULT_ERR_NOTE = 'No error message returned by the server. ' +
                         'You may not be authorized to access this page. ' +
                         'Try logging in again.';
    const PROMISE_STATE = {
      FULFILLED: 'fulfilled',
      REJECTED: 'rejected',
    };

    return {

      PROMISE_STATE: PROMISE_STATE,

      /**
       * Transform object to query param string
       * @param obj   - The param object
       * @returns {String} - The param string
       *
       * @example
       * nvRequestUtils.queryParam({a:1, b:2, c:'asdf'}).equals('a=1&b=2&c=asdf')
       * nvRequestUtils.queryParam({a:1, b:[2,3]}).equals('a=1&b=2&b=3')
       */
      queryParam: function queryParam(obj) {
        return _.map(obj, mapFn).join('&');

        function mapFn(value, key) {
          if (_.isArray(value)) {
            return _.map(value, item => `${key}=${item}`)
                          .join('&');
          }

          return `${key}=${value}`;
        }
      },

      /**
       * Extracts only the data from the response object.
       *
       * @param response    - The response object.
       * @returns {Promise} - A promise containing the response data.
       */
      success: function success(response) {
        return $q.resolve(response.data);
      },

      // response = {status: 200, data: {nv_version: '1.0', data: ...}}
      nvSuccess: function nvSuccess(response) {
        return $q.resolve(_.get(response, 'data.data'));
      },

      // response = {status: 404, data: {nv_version: '1.0', data: {}, error_code: '404-000', error_message: 'Not Found'}}
      // response = {"code":3001,"messages":["java.lang.NullPointerException"],"application":"core","description":"UNHANDLED_EXCEPTION","data":{"message":"java.lang.NullPointerException"}}
      // https://confluence.ninjavan.co/display/NVE/Standard+Response+Formats
      // response = {"error":{"code": 400,"request_id": "aa07ad81-...","title": "Request Error ...","message": "...","details": [{"reason": "Validation Error","field": "search_param1","message": "Search param1 must be like this"}]}}
      nvFailure: function nvFailure(response) {
        const method = nvUtils.getDeep(response, 'config.method');
        const url = nvUtils.getDeep(response, 'config.url');
        const request = nvUtils.getDeep(response, 'config.data');
        const errorCode = nvUtils.getDeep(response, 'data.error.code') ||
                  nvUtils.getDeep(response, 'data.code');
        const errorMessage = nvUtils.getDeep(response, 'data.error.message') ||
                  nvUtils.getDeep(response, 'data.messages') ||
                  JSON.stringify(nvUtils.getDeep(response, 'data'));
        const errorMessageDetails = nvUtils.getDeep(response, 'data.error.details');

        const errorLine1 = getResponseStatus(response);
        const errorLine2 = getURL(method, url);
        const errorLine3 = getErrorCode(errorCode);
        const errorLine4 = getErrorMessage(errorMessage, errorMessageDetails);

        const errorLines = [errorLine1, errorLine2, errorLine3, errorLine4];
        const rejectWith = _.get(response, 'data.data') || _.get(response, 'data.error');

        if (response.status === 401) {
          toastWithReAuthenticateOpt(errorLines);
          return $q.reject(rejectWith);
        }

        if ((method === 'POST' || method === 'PUT') && !!request) {
          toastWithCopyRequestOpt(errorLines, request);
          return $q.reject(rejectWith);
        }

        toastWithNoOpt(errorLines);
        return $q.reject(rejectWith);
      },

      /**
       * Displays an error toastr notification of a network request that is not yet implemented.
       *
       * @returns {Promise} - A promise object containing the response.
       */
      undefined: function undefined() {
        const response = { data: {
          error: { code: -1, message: 'Support for this feature has not been implemented yet' },
        } };

        nvToast.info('Not Implemented', _.get(response, 'data.error.message'));

        return $q.reject(response);
      },

      byteArrayToJSON: function byteArrayToJSON(byteArray) {
        const uint8ByteArray = new Uint8Array(byteArray);
        const errorResponse = String.fromCharCode.apply(null, uint8ByteArray);
        return JSON.parse(errorResponse);
      },

    };

    function getResponseStatus(response) {
      return `Status: <strong>${response.status || '-1'} ${response.statusText || 'Unknown'}</strong>`;
    }

    function getURL(method, url) {
      return `URL: <strong>${method || ''} ${url || 'Unknown'}</strong>`;
    }

    function getErrorCode(code) {
      return `Error Code: <strong>${_.isUndefined(code) ? DEFAULT_ERR_CODE : code}</strong>`;
    }

    function getErrorMessage(message = [DEFAULT_ERR_NOTE], messageDetails = []) {
      let joinedMessage = _.castArray(message).join('<br/>');
      if (_.size(messageDetails) > 0) {
        joinedMessage += '<br/>';
        joinedMessage += generateMessageDetails(messageDetails).join('<br/>');
      }
      return `Error Message: <strong>${joinedMessage}</strong>`;

      function generateMessageDetails(theMessageDetails) {
        return _.map(theMessageDetails, 'message');
      }
    }

    function toastWithReAuthenticateOpt(errorLines) {
      nvToast.error(TOAST_TITLE, errorLines, { buttonTitle: 'Re-Authenticate', buttonCallback: nvAuth.login });
    }

    function toastWithCopyRequestOpt(errorLines, request) {
      const toastOption = _.extend(TOAST_STICK_OPTIONS, {
        buttonTitle: 'Copy',
        buttonCallback: onClickCopyReq(request),
      });
      errorLines.push('<input type="input" class="nv-request-error-options-hidden-input">');
      nvToast.error(TOAST_TITLE, errorLines, toastOption);

      function onClickCopyReq(theRequest) {
        return function fn(toast) {
          const input = toast[0].querySelector('.nv-request-error-options-hidden-input');
          input.value = angular.toJson(theRequest);
          input.select();

          try {
            const success = document.execCommand('copy');
            console.log(`Copying was ${success ? 'successful' : 'unsuccessful'}`);
            input.blur();
          } catch (err) {
            console.log('Oops, failed to copy', err);
          }
        };
      }
    }

    function toastWithNoOpt(errorLines) {
      nvToast.error(TOAST_TITLE, errorLines);
    }
  }
}());
