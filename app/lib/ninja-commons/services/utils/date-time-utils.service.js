(function service() {
  angular
        .module('nvCommons.services')
        .factory('nvDateTimeUtils', dateTimeUtils);

  dateTimeUtils.$inject = ['nvTimezone'];

  function dateTimeUtils(nvTimezone) {
    const warnMomentOnce = _.once(warnMoment);
    const warnDateOnce = _.once(warnDate);
    const ALLOWABLE_TIME_ZONE_FORMATS = ['YYYY-MM-DD HH:mm:ss', 'YYYY-MM-DDTHH:mm:ssZ', 'HH:mm:ss', 'YYYY-MM-DD', 'YYYY-MM-DDTHH:mm:ss.SSSZ', 'YYYY-MM-DDTHH:mm:ss.SSSZZ'];

    const util = {
      FORMAT: 'YYYY-MM-DD HH:mm:ss',
      FORMAT_DATE: 'YYYY-MM-DD',
      FORMAT_TIME: 'HH:mm:ss',
      FORMAT_12_HOUR_TIME: 'hh:mm A',
      FORMAT_INTERNATIONAL_TIME: 'HHmm',
      FORMAT_ISO: 'YYYY-MM-DDTHH:mm:ssZ',
      FORMAT_ISO_TZ: 'YYYY-MM-DDTHH:mm:ss[Z]',
      FORMAT_LONG: 'YYYY-MM-DDTHH:mm:ss.SSSZZ',
      FORMAT_LONG_TZ: 'YYYY-MM-DDTHH:mm:ss.SSS[Z]',

      compareDate: compareDate,
      displayFormat: displayFormat,
      displayDateTime: displayDateTime,
      displayDate: displayDate,
      displayTime: displayTime,
      display12HourTime: display12HourTime,
      displayInternationalTime: displayInternationalTime,
      displayISO: displayISO,
      toDate: toDate,
      toMoment: toMoment,
      dayBetweenDates: dayBetweenDates,
      isBeforeTimeslot: isBeforeTimeslot,
      isWithinTimeslot: isWithinTimeslot,
      isAfterTimeslot: isAfterTimeslot,
      toSOD: toSOD,
      toEOD: toEOD,
    };

    return util;

    // //////////////////////////

    /**
     * compare date to be same day
     * @param  {moment|date} d1
     * @param  {moment|date} d2
     * @return {boolean}    true if d1 and d2 is on the same day, false otherwise
     */
    function compareDate(d1, d2) {
      return (moment.isMoment(d1) || angular.isDate(d1)) &&
            (moment.isMoment(d2) || angular.isDate(d2)) &&
            (moment(d1).isSame(moment(d2), 'day'));
    }

    /**
     * Display time in the given format
     * @param  {date|Date|moment.Moment|moment} time       time.      @see nvDateTimeUtils.toMoment(time, timezone)
     * @param  {string}                 format     @see http://momentjs.com/docs/#/displaying/format/
     * @param  {timezone}               timezone   @optional The timezone to display to. Default to local browser time.
     *                                             @see http://momentjs.com/timezone/docs/#/using-timezones/converting-to-zone/
     *                                             @example
     *                                             var date = new Date('2015-12-31T16:00:00Z');
     *                                             // 2016-01-01 00:00:00 SGT
     *                                             nvDateTimeUtils.displayFormat(date, 'YYYY-MM-DDTHH:mm:ssZ', 'utc')
     *                                             // '2015-12-31T16:00:00+00:00'
     *                                             nvDateTimeUtils.displayFormat(date, 'YYYY-MM-DDTHH:mm:ssZ', 'Asia/Singapore')
     *                                             // '2016-01-01T00:00:00+08:00'
     *
     *                                             nvDateTimeUtils.displayFormat(date, 'YYYY-MM-DD HH:mm:ss', 'utc')
     *                                             // '2015-12-31 16:00:00'
     *                                             nvDateTimeUtils.displayFormat(date, 'YYYY-MM-DD HH:mm:ss', 'Asia/Singapore')
     *                                             // '2016-01-01 00:00:00'
     *                                             // For browser in singapore
     *                                             nvDateTimeUtils.displayFormat(date, 'YYYY-MM-DD HH:mm:ss')
     *                                             // '2016-01-01 00:00:00'
     *
     * @return {string}                            time in the given format.
     */
    function displayFormat(time, format, timezone = nvTimezone.getOperatorTimezone()) {
      if (!angular.isDate(time) && !moment.isMoment(time)) {
        console.warn(`nvDateTimeUtils.displayFormat(time,${format},timezone) accepts date object or moment object only.`);
        return '';
      }
      let momentTime = moment(time);
      if (isValidTimeZoneString(timezone)) {
        momentTime = momentTime.tz(timezone);
      }
      return momentTime.format(format);
    }

    /**
     *
     * Display time in 'YYYY-MM-DD HH:mm:ss' format
     * @param  {date|Date|moment.Moment|moment} time       time.      @see nvDateTimeUtils.toMoment(time, timezone)
     * @param  {string}                 timezone   Optional. The timezone to display to. @see nvDateTimeUtils.displayFormat(time, format, timezone)
     * @return {string}                            time in 'YYYY-MM-DD HH:mm:ss'
     */
    function displayDateTime(time, timezone = nvTimezone.getOperatorTimezone()) {
      return util.displayFormat(time, util.FORMAT, timezone);
    }

    /**
     *
     * Display time in 'YYYY-MM-DD' format
     * @param  {date|Date|moment.Moment|moment} time       time.      @see nvDateTimeUtils.toMoment(time, timezone)
     * @param  {string}                 timezone   Optional. The timezone to display to. @see nvDateTimeUtils.displayFormat(time, format, timezone)
     * @return {string}                            time in 'YYYY-MM-DD'
     *
     */
    function displayDate(time, timezone) {
      return util.displayFormat(time, util.FORMAT_DATE, timezone);
    }

    /**
     *
     * Display time in 'HH:mm:ss' format
     * @param  {date|Date|moment.Moment|moment} time       time.      @see nvDateTimeUtils.toMoment(time, timezone)
     * @param  {string}                 timezone   Optional. The timezone to display to. @see nvDateTimeUtils.displayFormat(time, format, timezone)
     * @return {string}                            time in 'HH:mm:ss'
     *
     */
    function displayTime(time, timezone) {
      return util.displayFormat(time, util.FORMAT_TIME, timezone);
    }

    /**
     *
     * Display time in 'hh:mm A' format
     * @param  {date|Date|moment.Moment|moment} time       time.      @see nvDateTimeUtils.toMoment(time, timezone)
     * @param  {string}                 timezone   Optional. The timezone to display to. @see nvDateTimeUtils.displayFormat(time, format, timezone)
     * @return {string}                            time in 'hh:mm A'
     *
     */
    function display12HourTime(time, timezone) {
      return util.displayFormat(time, util.FORMAT_12_HOUR_TIME, timezone);
    }

    /**
     *
     * Display time in 'HHmm' format
     * @param  {date|moment.Moment|moment} time       time.      @see nvDateTimeUtils.toMoment(time, timezone)
     * @param  {string}                 timezone   Optional. The timezone to display to. @see nvDateTimeUtils.displayFormat(time, format, timezone)
     * @return {string}                            time in 'HHmm'
     *
     */
    function displayInternationalTime(time, timezone = nvTimezone.getOperatorTimezone()) {
      return util.displayFormat(time, util.FORMAT_INTERNATIONAL_TIME, timezone);
    }

    /**
     *
     * Display time in 'YYYY-MM-DDTHH:mm:ssZ' format
     * @param  {date|Date|moment.Moment|moment} time       time.      @see nvDateTimeUtils.toMoment(time, timezone)
     * @param  {string}                 timezone   Optional. The timezone to display to. @see nvDateTimeUtils.displayFormat(time, format, timezone)
     * @return {string}                            time in 'YYYY-MM-DDTHH:mm:ssZ'
     *
     */
    function displayISO(time, timezone = nvTimezone.getOperatorTimezone()) {
      return util.displayFormat(time, util.FORMAT_ISO, timezone);
    }

    /**
     * Get the date object for the time
     * @param  {number|string|date|moment} time     time. @see nvDateTimeUtils.toMoment(time, timezone)
     * @return {date}                                     If the given time is invalid, return null.
     */
    function toDate(time, timezone) {
      warnDateOnce();
      const momentTime = util.toMoment(time, timezone);
      if (momentTime.isValid()) {
        return momentTime.toDate();
      }

      return null;
    }

    /**
     * Get moment object
     * @param  {number|string|date|moment} time     Time. Accept time in the following format:
     *                                              - number in milliseconds,
     *                                              - string in format: 'YYYY-MM-DD HH:mm:ss', 'YYYY-MM-DDTHH:mm:ssZ', 'HH:mm:ss', 'YYYY-MM-DD'
     *                                              - date object, or
     *                                              - moment object
     *
     * @param  {string} timezone                    @optional timezone of the resulted moment object.
     *                                              If not given, always take local browser timezone.
     *                                              <b>Timezone only takes into effect when the `time` string given does not contain timezone information.</b>
     *
     *                                              @example
     *
     *                                              //Browser in Singapore
     *                                              nvDateTimeUtils.toMoment('2016-01-01 00:00:00')
     *                                              // 2016-01-01 00:00:00 +0800
     *                                              nvDateTimeUtils.toMoment('2016-01-01 00:00:00', 'utc')
     *                                              // 2016-01-01 00:00:00 +0000
     *
     *                                              @example
     *
     *                                              nvDateTimeUtils.toMoment('2015-12-31T16:00:00Z')
     *                                              // 2016-01-01 00:00:00 +0800
     *                                              nvDateTimeUtils.toMoment('2015-12-31T16:00:00Z', 'utc')
     *                                              // 2016-01-01 00:00:00 +0000
     *                                              nvDateTimeUtils.toMoment('2015-12-31T16:00:00Z', 'Asia/Singapore')
     *                                              // 2016-01-01 00:00:00 +0800
     *
     * @return {moment}                             Moment object for the given time in the given timezone
     */
    function toMoment(time, timezone) {
      const theTime = angular.isDefined(time) ? time : '';

      // parse time
      if (moment.isMoment(theTime)) {
        return theTime;
      } else if (angular.isNumber(theTime) || angular.isDate(theTime)) {
        warnMomentOnce();
        return moment(theTime);
      } else if (angular.isString(theTime)) {
        if (isValidTimeZoneString(timezone)) {
          return moment.tz(theTime, ALLOWABLE_TIME_ZONE_FORMATS, true, timezone);
        }

        warnMomentOnce();
        return moment(theTime, ALLOWABLE_TIME_ZONE_FORMATS, true);
      }

      warnMomentOnce();
      return moment.invalid();
    }

    function dayBetweenDates(date1, date2) {
      return Math.ceil(moment(date1).diff(moment(date2), 'day', true));
    }

    function isBeforeTimeslot(frTime, m) {
      const fr = moment(m).startOf('day').hour(moment(frTime, 'ha').hour());
      return m.isBefore(fr);
    }

    /**
     * Checks if the specified moment is within the specified timeslot.
     * The calculations are all done in local (browser) time.
     *
     * @param {string} frTime Time in string format, e.g. '8AM', '3PM'.
     * @param {string} toTime Time in string format, e.g. '12PM', '10PM'
     * @param {moment} m The moment to check.
     * @returns {Boolean} True if the moment is within the timeslot, false otherwise.
     */
    function isWithinTimeslot(frTime, toTime, m) {
      const fr = moment(m).startOf('day').hour(moment(frTime, 'ha').hour());
      const to = moment(m).startOf('day').hour(moment(toTime, 'ha').hour());
      return m.isBetween(fr, to);
    }

    function isAfterTimeslot(toTime, m) {
      const to = moment(m).startOf('day').hour(moment(toTime, 'ha').hour());
      return m.isAfter(to);
    }

    function warnMoment() {
      console.warn('<b>Use nvDateTimeUtils.toMoment(time) with caution.</b>');
      console.warn('Use moment(time) in favor of nvDateTimeUtils.toMoment(time) unless you are handling with legacy endpoints.');
    }

    function warnDate() {
      console.warn('<b>Use nvDateTimeUtils.toDate(time) with caution.</b>');
      console.warn('Use moment(time).toDate() in favor of nvDateTimeUtils.toDate(time) unless you are handling with legacy endpoints.');
    }

    function isValidTimeZoneString(timezone) {
      return timezone && moment.tz.zone(String(timezone)) !== null;
    }

    /**
     * get start of day of the date in a specific timezone,
     * only year,month,date info of the date is maintained
     * @param  {moment|date} date     date
     * @param  {string} timezone      timezone
     * @param  {offset} number        offset in seconds
     * @return {moment}               moment object with time 00:00:00 at the specified timezone
     *
     * @note
     * For SG browser,
     * a = moment(new Date('2017-01-02 00:00:00')).tz('Asia/Jakarta').startOf('day')
     * b = moment(new Date('2017-01-02 00:00:00')).tz('Asia/Singapore').startOf('day')
     *
     * console.log(a.tz('utc').format('YYYY-MM-DD HH:mm:ss'))
     * //2016-12-31 17:00:00
     * console.log(b.tz('utc').format('YYYY-MM-DD HH:mm:ss'))
     * //2017-01-01 16:00:00
     *
     * c = nvDateTimeUtils.toSOD(moment(new Date('2017-01-02 00:00:00')), 'Asia/Jakarta')
     * d = nvDateTimeUtils.toSOD(moment(new Date('2017-01-02 00:00:00')), 'Asia/Singapore')
     *
     * console.log(c.tz('utc').format('YYYY-MM-DD HH:mm:ss'))
     * //2016-01-01 17:00:00
     * console.log(d.tz('utc').format('YYYY-MM-DD HH:mm:ss'))
     * //2017-01-01 16:00:00
     *
     * offset is indicating how many seconds from the start of date itself
     *
     */
    function toSOD(date, timezone = nvTimezone.getOperatorTimezone(), offset = 0) {
      const m = moment(date);
      let hours = 0;
      let minutes = 0;
      if (offset !== 0) {
        hours = Math.floor(offset / 3600);
        minutes = Math.floor((offset - (hours * 3600)) / 60);
      }
      return moment.tz({
        y: m.year(),
        M: m.month(),
        d: m.date(),
        h: hours,
        m: minutes,
        s: 0,
        ms: 0,
      }, timezone);
    }

    /**
     * get end of day of the date in a specific timezone,
     * only year,month,date info of the date is maintained
     * @param  {moment|date} date     date
     * @param  {string} timezone      timezone
     * @return {moment}               moment object with time 23:59:59 at the specified timezone
     *
     * @see nvDateTimeUtils.toSOD
     *
     */
    function toEOD(date, timezone = nvTimezone.getOperatorTimezone()) {
      const m = moment(date);
      return moment.tz({
        y: m.year(),
        M: m.month(),
        d: m.date(),
        h: 23,
        m: 59,
        s: 59,
        ms: 0, // the smallest time unit stored in the db is in seconds,
               // so we set this to 0 instead of 999 to avoid comparison errors
      }, timezone);
    }
  }
}());
