(function service() {
  angular
    .module('nvCommons.services')
    .factory('nvPanel', panelService);

  panelService.$inject = ['$q', '$log', '$mdPanel', '$templateRequest'];

  function panelService($q, $log, $mdPanel, $templateRequest) {
    return {
      show: function show(event, panelOptions) {
        const panel = _.defaults(panelOptions || {}, {
          controller: null,
          controllerAs: 'ctrl',
          templateUrl: '',
          openFrom: event,
          clickOutsideToClose: true,
          escapeToClose: true,
          focusOnOpen: false,
          zIndex: 2000,
          xPosition: 'ALIGN_START',
          yPosition: 'BELOW',
          darkTheme: false,
          panelClass: '',
          locals: {},
        });

        // some basic sanity checking
        if (!panel.templateUrl) {
          $log.error('[panel.service.js][show]', 'panel templateUrl not defined');
          return $q.reject();
        }

        const position = $mdPanel.newPanelPosition()
          .relativeTo(event.currentTarget)
          .addPanelPosition(
          $mdPanel.xPosition[panel.xPosition],
          $mdPanel.yPosition[panel.yPosition]
        );

        return $templateRequest(panel.templateUrl).then(templateHtml =>
            $mdPanel.open({
              attachTo: angular.element(document.body),
              openFrom: panel.openFrom,
              clickOutsideToClose: panel.clickOutsideToClose,
              escapeToClose: panel.escapeToClose,
              focusOnOpen: panel.focusOnOpen,
              zIndex: panel.zIndex,
              position: position,
              controller: panel.controller,
              controllerAs: panel.controllerAs,
              template: ['<div class="nv-panel md-whiteframe-4dp', (panel.darkTheme ? ' dark' : ''), '">', templateHtml, '</div>'].join(''),
              panelClass: panel.panelClass,
              locals: panel.locals,
            })
        );
      },
    };
  }
}());
