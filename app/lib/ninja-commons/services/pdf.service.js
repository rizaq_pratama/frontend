(function service() {
  angular
    .module('nvCommons.services')
    .factory('PDF', PDF);

  PDF.$inject = ['$http', 'nvToast', 'nvRequestUtils', 'nvFileUtils', '$q', 'nvTranslate'];
  function PDF($http, nvToast, nvRequestUtils, nvFileUtils, $q, nvTranslate) {
    return {
      download: download,
      downloadWithPost: downloadWithPost,
      view: view,
    };

    /**
     * Download PDF file
     * @param  {String} url
     * @param  {String} filename
     * @return {Promise}
     */
    function download(url, filename) {
      nvToast.info(nvTranslate.instant('commons.service.file.attempt-download', { filename: filename }));
      return $http
        .get(url, { responseType: 'arraybuffer' })
        .then(successDownload(filename), byteArrayError);
    }

    function downloadWithPost(url, data, filename) {
      nvToast.info(nvTranslate.instant('commons.service.file.attempt-download', { filename: filename }));
      return $http
        .post(url, data, { responseType: 'arraybuffer' })
        .then(successDownload(filename), byteArrayError);
    }

    /**
     * View PDF file
     * @param  {String} url
     * @return {Promise}
     */
    function view(url) {
      return $http.get(url, { responseType: 'arraybuffer' }).then(
                success,
                byteArrayError
            );

      // ///////////////////////////
      function success(response) {
        const dataURL = nvFileUtils.getURLFromData(response.data, nvFileUtils.FILETYPE_PDF);
        return $q.when(nvFileUtils.view(dataURL));
      }
    }

    function successDownload(filename) {
      return (response) => {
        if (response.status === 200) {
          nvToast.success(nvTranslate.instant('commons.service.file.downloading', {filename: filename}));

          const dataURL = nvFileUtils.getURLFromData(response.data, nvFileUtils.FILETYPE_PDF);
          return $q.when(nvFileUtils.download(dataURL, filename || 'download.pdf'));
        }
        nvToast.error(nvTranslate.instant('commons.service.file.download-failed', {filename: filename}),
          nvTranslate.instant('commons.service.file.http-status', {status: response.status}));
        return $q.reject();
      }
    }

    function byteArrayError(response) {
      const json = nvRequestUtils.byteArrayToJSON(response.data);
      response.data = json;
      return nvRequestUtils.nvFailure(response);
    }
  }
}());
