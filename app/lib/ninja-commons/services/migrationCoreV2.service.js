(function service() {
  angular
        .module('nvCommons.services')
        .factory('nvMigrationService', nvMigrationService);

  nvMigrationService.$inject = ['$window'];

  function nvMigrationService($window) {
    // This is created to facilitate tests
    return $window.Migration_corev2_Services || {
      setAuthToken: angular.noop,
      removeAuthToken: angular.noop,
      reports: {}
    };
  }
}());

