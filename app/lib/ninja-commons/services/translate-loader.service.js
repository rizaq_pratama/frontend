;(function() {
    'use strict';

    angular
        .module('nvCommons.services')
        .factory('nvTranslateLoader', translateLoader);

    translateLoader.$inject = ['$http', '$q', '$log'];

    function translateLoader($http, $q, $log) {

        /**
         * This custom loader was created to allow file revision on the locale files.
         * The standard loaders did not provide support for file revision.
         * See https://github.com/angular-translate/bower-angular-translate-loader-static-files/issues/11.
         *
         * Example usage:
         *   $translateProvider.useLoader('nvTranslateLoader', {
         *     files: {
         *       'en': 'resources/languages/locale-en.json',
         *       'ms': 'resources/languages/locale-ms.json',
         *       'id': 'resources/languages/locale-id.json'
         *     }
         *   });
         */
        return function(options) {
            var files = options.files || {},
                key = options.key,
                deferred = $q.defer(),
                localeFile = files[key];

            if (!localeFile) {
                deferred.reject(key);
            } else {
                $http.get(localeFile).then(success, failure);
            }

            return deferred.promise;

            function success(res) {
                return deferred.resolve(res.data);
            }

            function failure(err) {
                $log.error('[translate-loader.service.js] Failed to get the locale file \'' + localeFile + '\'', err);
                return deferred.reject(err.data);
            }
        };

    }

})();
