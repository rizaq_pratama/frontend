(function service() {
  angular
    .module('nvCommons.services')
    .factory('nvDOMManipulation', domManipulation);

  domManipulation.$inject = ['$document', 'nvEnv'];

  function domManipulation($document, nvEnv) {
    return {

      /**
       * Moves the classes from the given DOM element to the first child element.
       *
       * @param domElement - See https://developer.mozilla.org/en-US/docs/Web/API/Element.
       * @param all        - If true, move class to all children, otherwise, to the first child
       * @returns {Array}  - The list of moved class names.
       */
      moveClassesToFirstChild: (domElement, all) => {
        return _(domElement.classList)
                .difference(['ng-isolate-scope'])
                .forEach(moveClassToChild);

        function moveClassToChild(clazz) {
          domElement.classList.remove(clazz);
          if (all) {
            _.each(domElement.children, (elem) => {
              elem.classList.add(clazz);
            });
          } else {
            domElement.children[0].classList.add(clazz);
          }
        }
      },

      /**
       * Updates the favicon links so that the icon for the right domain is shown.
       *
       * @param domain - The domain that we are updating to.
       */
      reloadFavicon: (domain) => {
        if (nvEnv.isNinjavan(nv.config.env)) {
          replace('[rel=\'icon\']', domain);
          replace('[rel=\'shortcut icon\']', domain);
          replace('[rel=\'apple-touch-icon-precomposed\']', domain);
        }
      },

      removeAttrFromElement: (element, attrName) => {
        element.removeAttribute(attrName);
      },

    };

    function replace(selectors, domain) {
      _.forEach($document[0].head.querySelectorAll(selectors), (ele) => {
        ele.setAttribute('href', ele.getAttribute('href').replace(/ico\/\w+/, `ico/${domain}`));
      });
    }
  }
}());
