(function service() {
  angular
    .module('nvCommons.services')
    .factory('nvNavGuard', nvNavGuard);

  /**
   * Navigation Guard
   * When it is locked, this service prevents the user from
   * navigating away from the current page or closing the current window.
   * 
   * @method isLock() 
   * return boolean, whether is the nvNavGuard currently locked
   *
   * @method lock(message)
   * Optional message. Lock the current page
   *
   * @method unlock()
   * Unlock the current page
   *
   * @method testLock()
   * return false if nvNavGuard is not currently locked
   * return a promise that will be resolved if the user wants to leave
   * and rejected when the user decides to stay
   * 
   */
  nvNavGuard.$inject = ['$window', 'nvTranslate', 'nvDialog'];

  function nvNavGuard($window, nvTranslate, nvDialog) {
    let pageLocked = false;
    let pageLockMessage = '';

    const navGuard = {
      isLock: () => pageLocked,

      lock: (message) => {
        pageLocked = true;
        pageLockMessage = message;
        lockTab();
      },

      unlock: () => {
        pageLocked = false;
        unlockTab();
      },

      testLock: () => {
        if (pageLocked) {
          return showLeavingDialog().then(() => {
            navGuard.unlock();
          });
        }
        return false;
      },
    };

    return navGuard;

    function lockTab() {
      $window.onbeforeunload = () =>
        (pageLockMessage || nvTranslate.instant('commons.service.nav-guard.content'));
    }

    function unlockTab() {
      $window.onbeforeunload = undefined;
    }

    function showLeavingDialog() {
      return nvDialog.confirmDelete(null,
        { title: nvTranslate.instant('commons.service.nav-guard.title'),
          content: pageLockMessage || nvTranslate.instant('commons.service.nav-guard.content'),
          ok: nvTranslate.instant('commons.service.nav-guard.leave'),
          cancel: nvTranslate.instant('commons.service.nav-guard.stay'),
        });
    }
  }
}());
