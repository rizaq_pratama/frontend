(function service() {
    /**
     * Utility to create delegate-handle
     * Used to provide a handle for the controller to access or modify data in a directive
     * i.e. refer to nv-calendar directive
     *
     * In html:
     * <nv-calendar delegate-handle="handle_name"></nv-calendar>
     *
     * In controller:
     * CalendarDataService.getByHandle('handle_name').doSomething();
     *
     * In directive:
     * CalendarDataService.initHandle('handle_name', init, param, arg);
     * CalendarDataService.getByHandle('handle_name').doSomething();
     *
     * In service:
     * service('CalendarDataService', CalendarDataService)
     * function CalendarDataService(DelegateHandle) {
     *
     *    //prefix is used in a directives that did have `delegate-handle` attr,
     *    //it generate a id with that prefix
     *    return DelegateHandle(CalendarData, 'prefix');
     * }
     * CalendarData.$inject = [...]
     * function CalendarData(...){ ... };
     *
     */

  angular
    .module('nvCommons.services')
    .factory('DelegateHandle', DelegateHandle);

  DelegateHandle.$inject = ['$injector'];

  function DelegateHandle($injector) {
    return {
      create: create,
    };
    function create(clz, prefix) {
      return new DelegateHandleMap(clz, prefix);
    }
    function DelegateHandleMap(clz, prefix) {
      this.prefix = prefix;
      this.clz = clz;
      this.data = {};
      this.index = 0;

      this.initHandle = initHandle;
      this.getByHandle = getByHandle;
      this.generateHandle = generateHandle;
      this.removeHandle = removeHandle;

      // ////////////////////

      function initHandle(handle) {
        if (!angular.isString(handle) || handle === '') {
          throw new Error('handle is not a sring');
        }
        this.data[handle] = $injector.instantiate(this.clz);
        return this.data[handle];
      }

      function getByHandle(handle) {
        if (!angular.isString(handle) || handle === '') {
          throw new Error('handle is not a sring');
        }
        if (!_.has(this.data, handle)) {
          return this.initHandle.call(this, handle);
        }
        return this.data[handle];
      }

      function generateHandle() {
        return this.prefix + ((this.index += 1) % 1000000);
      }

      function removeHandle(handle) {
        if (!angular.isString(handle) || handle === '') {
          throw new Error('handle is not a sring');
        }
        _.unset(this.data, handle);
      }
    }
  }
}());
