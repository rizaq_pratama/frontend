(function service() {
  angular
    .module('nvCommons.services')
    .factory('nvGa', nvGa);

  nvGa.$inject = ['$window'];

  function nvGa($window) {
    return {
      event: (category, action, label, value) => {
        if ($window.ga) {
          const ga = $window.ga;
          ga('send', 'event', category, action, label, value);
        }
      },

      timing: (category, variance, value, label) => {
        if ($window.ga) {
          const ga = $window.ga;
          ga('send', 'timing', category, variance, value, label);
        }
      },
    };
  }
}());
