;(function() {
    'use strict';

    angular
        .module('nvCommons.services', [
            'ui.router',
            'ngCookies',
            'nv',
        ]);

})();
