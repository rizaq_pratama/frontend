(function service() {
  angular
        .module('nvCommons.services')
        .factory('nvMaps', maps);

  maps.$inject = ['$rootScope', 'nvMapbox', '$q'];

  function maps($rootScope, nvMapbox, $q) {
    const self = {
      mapType: {
        MAPBOX: 'mapbox',
        GOOGLEMAPS: 'googlemap',
      },

      initialize: function initialize(mapId, options, mode = nv.config.map.mode) {
        if (mode === self.mapType.MAPBOX) {
          const defaultLatLng = self.getDefaultLatLngForDomain();
          const mapboxOptions = _.defaultsDeep(options, {
            latitude: defaultLatLng.lat,
            longitude: defaultLatLng.lng,
            zoomLevel: self.getDefaultZoomLevelForDomain(),
            controlPosition: 'topleft',
          });

          return nvMapbox.initialize(
            mapId, mapboxOptions
          );
        }

        return null;
      },

      flyToLocation: function flyToLocation(map, options, mode = nv.config.map.mode) {
        /*
         * options:
         * - (Double) latitude
         * - (Double) longitude
         */

        if (mode === self.mapType.MAPBOX) {
          nvMapbox.flyToLocation(map, options);
        }
      },

      addPolygon: function addPolygon(map, polygonData, mode = nv.config.map.mode) {
        /*
         * polygonData:
         * - (Array[Double lat, Double lng]) latLngs
         * - (String) color (optional)
         * - (Any) user defined param(s)
         */

        if (mode === self.mapType.MAPBOX) {
          return nvMapbox.addPolygon(map, polygonData);
        }

        return null;
      },

      removePolygon: function removePolygon(map, polygon, mode = nv.config.map.mode) {
        if (mode === self.mapType.MAPBOX) {
          return nvMapbox.removePolygon(map, polygon);
        }

        return null;
      },

      addMarker: function addMarker(map, markerData, mode = nv.config.map.mode) {
        /*
         * markerData:
         * - (Double) latitude
         * - (Double) longitude
         * - (String) iconUrl
         * - (Array[Int width, Int height]) iconSize
         * - (Boolean) draggable
         * - (Int) zIndex (optional)
         * - (String) label (optional)
         * - (Any) user defined param(s)
         */

        if (mode === self.mapType.MAPBOX) {
          return nvMapbox.addMarker(map, markerData);
        }

        return null;
      },

      removeMarker: function removeMarker(map, marker, mode = nv.config.map.mode) {
        if (mode === self.mapType.MAPBOX) {
          return nvMapbox.removeMarker(map, marker);
        }

        return null;
      },

      getIconData: function getIconData(config) {
        const theConfig = _.defaultsDeep(config, {
          type: 'd_map_xpin_letter',
          style: 'pin',
          icon: '',
          text: '',
          textColor: '000000',
          fillColor: '000000',
          starFillColor: '000000',

          scaleFactor: 0.8,
          rotationDegree: 0,
          fontSize: 12,
          fontStyle: '_',

          size: [18, 29],
        });

        let iconUrlParams;
        switch (theConfig.type) {
          case 'd_map_xpin_icon':
            iconUrlParams = `${config.type}-${config.style}-${config.icon}-${config.fillColor}-${config.starFillColor}`;
            break;
          case 'd_map_xpin_letter':
            iconUrlParams = `${config.type}-${config.style}-${config.text}-${config.fillColor}-${config.textColor}-${config.starFillColor}`;
            break;
          case 'd_map_spin':
            iconUrlParams = `${config.type}-${config.scaleFactor}-${config.rotationDegree}-${config.fillColor}-${config.fontSize}-${config.fontStyle}-${config.text}`;
            break;
          default:
            iconUrlParams = '';
        }

        return {
          iconUrl: `lib/ninja-commons/assets/markers/${iconUrlParams}.png`,
          iconSize: config.size,
        };
      },

      markerAddEvent: function markerAddEvent(
        marker, eventName, callback, mode = nv.config.map.mode) {
        if (mode === self.mapType.MAPBOX) {
          nvMapbox.markerAddEvent(marker, eventName, callback);
        }
      },

      markerAddInfoWindow: function markerAddInfoWindow(
        map, marker, infoWindowData, mode = nv.config.map.mode) {
        /*
         * infoWindowData:
         * - (String) text
         * - (String) className
         */

        if (mode === self.mapType.MAPBOX) {
          nvMapbox.markerAddInfoWindow(marker, infoWindowData);
        }
      },

      markerShowInfoWindow: function markerShowInfoWindow(marker, mode = nv.config.map.mode) {
        if (mode === self.mapType.MAPBOX) {
          nvMapbox.markerShowInfoWindow(marker);
        }
      },

      markerHideInfoWindow: function markerHideInfoWindow(marker, mode = nv.config.map.mode) {
        if (mode === self.mapType.MAPBOX) {
          nvMapbox.markerHideInfoWindow(marker);
        }
      },

      markerSetZIndex: function markerSetZIndex(marker, zIndex, mode = nv.config.map.mode) {
        if (mode === self.mapType.MAPBOX) {
          nvMapbox.markerSetZIndex(marker, zIndex);
        }
      },

      markerSetIcon: function markerSetIcon(marker, options, mode = nv.config.map.mode) {
        if (mode === self.mapType.MAPBOX) {
          return nvMapbox.markerSetIcon(marker, options);
        }

        return null;
      },

      markerSetOpacity: function markerSetIcon(marker, opacityValue, mode = nv.config.map.mode) {
        if (mode === self.mapType.MAPBOX) {
          return nvMapbox.markerSetOpacity(marker, opacityValue);
        }

        return null;
      },

      markerSetLatLng: function markerSetLatLng(marker, options, mode = nv.config.map.mode) {
        /*
         * options:
         * - (Double) latitude
         * - (Double) longitude
         */

        if (mode === self.mapType.MAPBOX) {
          nvMapbox.markerSetLatLng(marker, options);
        }
      },

      /**
       * Gets the default latitude and longitude for the given domain.
       *
       * @param domain     - The domain string.
       * @returns {Object} - The latitude and longitude in an object.
       */
      getDefaultLatLngForDomain: function getDefaultLatLngForDomain(domain = $rootScope.domain) {
        switch (domain) {
          case 'sg' : return { lat: 1.3597220659709373, lng: 103.82701942695314 };
          case 'my' : return { lat: 3.1333333, lng: 101.697806 };
          case 'id' :
          case 'mbs':
          case 'mnt':
          case 'demo':
          case 'msi':
          case 'gpi': return { lat: -6.2141988, lng: 106.8064186 };
          case 'vn' : return { lat: 10.8046341, lng: 106.6299261 };
          case 'ph' : return { lat: 14.5901341, lng: 120.9804293 };
          case 'th' : return { lat: 13.7244417, lng: 100.5022261 };
          case 'mm':
          case 'mmpg' : return { lat: 16.772787, lng: 96.169810 };
          default : return { lat: 1.3, lng: 103.8311393 };
        }
      },

      /**
       * Gets the default zoom level for the given domain.
       *
       * @param domain     - The domain string.
       * @returns {Number} - The zoom level.
       */
      getDefaultZoomLevelForDomain: function getDefaultZoomLevelForDomain(
        domain = $rootScope.domain) {
        switch (domain) {
          case 'sg' :
          case 'id' :
          case 'mbs':
          case 'mnt':
          case 'demo':
          case 'gpi': return 12;
          case 'my' :
          case 'ph' : return 13;
          default : return 11;
        }
      },

      rgbTohex: function rgbTohex(rgb) {
        const theRgb = rgb.match(/^rgb\((\d+),\s*(\d+),\s*(\d+)\)$/);
        function hex(x) {
          return (`0${parseInt(x, 10).toString(16)}`).slice(-2);
        }
        return hex(theRgb[1]) + hex(theRgb[2]) + hex(theRgb[3]);
      },
      reverseGeocoding: function reverseGeocoding(lat, lng, mode = nv.config.map.mode) {
        if (mode === self.mapType.MAPBOX) {
          return nvMapbox.reverseGeocoding(lat, lng);
        }
        return $q.reject();
      },
      getDistanceFromLatLonInKm: function getDistanceFromLatLonInKm(lat1, lon1, lat2, lon2) {
        const R = 6371; // Radius of the earth in km
        const dLat = deg2rad(lat2 - lat1);  // deg2rad below
        const dLon = deg2rad(lon2 - lon1);
        const a =
            (Math.sin(dLat / 2) * Math.sin(dLat / 2)) +
            (Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) *
            Math.sin(dLon / 2) * Math.sin(dLon / 2))
          ;
        const c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        const d = R * c; // Distance in km
        return d;

        function deg2rad(deg) {
          return deg * (Math.PI / 180);
        }
      },
    };

    return self;
  }
}());
