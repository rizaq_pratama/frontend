/* eslint-disable new-cap */

(function service() {
  angular
    .module('nvCommons.services')
    .factory('nvMapboxGL', nvMapboxGL);

  nvMapboxGL.$inject = ['nvMaps'];

  function nvMapboxGL(nvMaps) {
    const STYLES = {
      STREETS: 'mapbox://styles/mapbox/streets-v9',
      OUTDOORS: 'mapbox://styles/mapbox/outdoors-v9',
      LIGHT: 'mapbox://styles/mapbox/light-v9',
      DARK: 'mapbox://styles/mapbox/dark-v9',
      SATELLITE: 'mapbox://styles/mapbox/satellite-v9',
      SATELLITE_STREETS: 'mapbox://styles/mapbox/satellite-streets-v9',
    };

    const LAYER_TYPES = {
      FILL: 'fill',
      LINE: 'line',
      SYMBOL: 'symbol',
      CIRCLE: 'circle',
      RASTER: 'raster',
      BACKGROUND: 'background',
    };

    const SOURCES = {
      BASE: 'base',
      PSEUDO_POLY_FILL_STROKE: 'pseudo-poly-fill-stroke',
      PSEUDO_POLY_FILL: 'pseudo-poly-fill',
      PSEUDO_POLY_VERT_STROKE: 'pseudo-poly-vert-stroke',
      PSEUDO_POLY_VERT: 'pseudo-poly-vert',
      STABLE_LINE: _.partial(suffix, '-edges'),
      ACTIVE_POLY: 'active-poly',
      ACTIVE_VERT: 'active-vert',
      ACTIVE_LINE: 'active-line',
      ACTIVE_MOCK_VERT: 'active-mock-vert',
      CREATE_LINE: 'create-line',
      CHOOSE_CIRCLE_AREA: 'choose-circle',
      OVRLAY: 'overlay',
      OVRLAY_EDGE_TOP: 'overlay-edge-top',
      OVRLAY_EDGE_RGT: 'overlay-edge-rgt',
      OVRLAY_EDGE_BTM: 'overlay-edge-btm',
      OVRLAY_EDGE_LFT: 'overlay-edge-lft',
      OVRLAY_CORNERS: 'overlay-corners',
    };

    const LAYERS = {
      BASE: 'base',
      PSEUDO_POLY_FILL_STROKE: 'pseudo-poly-fill-stroke',
      PSEUDO_POLY_FILL: 'pseudo-poly-fill',
      PSEUDO_POLY_VERT_STROKE: 'pseudo-poly-vert-stroke',
      PSEUDO_POLY_VERT: 'pseudo-poly-vert',
      STABLE_POLY_FILL_STROKE: _.partial(prefix, 'poly-fill-stroke-'),
      STABLE_POLY_FILL: _.partial(prefix, 'poly-fill-'),
      STABLE_POLY_VERT_STROKE: _.partial(prefix, 'poly-vert-stroke-'),
      STABLE_POLY_VERT: _.partial(prefix, 'poly-vert-'),
      ACTIVE_POLY_FILL_STROKE: 'active-poly-fill-stroke',
      ACTIVE_MOCK_VERT: 'active-mock-vert',
      ACTIVE_POLY_VERT_STROKE: 'active-poly-vert-stroke',
      ACTIVE_POLY_VERT: 'active-poly-vert',
      ACTIVE_LINE: 'active-line',
      ACTIVE_VERT_STROKE: 'active-vert-stroke',
      ACTIVE_VERT: 'active-vert',
      CREATE_LINE: 'create-line',
      CHOOSE_CIRCLE_AREA: 'choose-circle',
      OVRLAY_IMAGE: 'overlay-image',
      OVRLAY_IMAGE_EDGE_TOP: 'overlay-image-edge-top',
      OVRLAY_IMAGE_EDGE_RGT: 'overlay-image-edge-rgt',
      OVRLAY_IMAGE_EDGE_BTM: 'overlay-image-edge-btm',
      OVRLAY_IMAGE_EDGE_LFT: 'overlay-image-edge-lft',
      OVRLAY_IMAGE_VERT_STROKE: 'overlay-image-vert-stroke',
      OVRLAY_IMAGE_VERT: 'overlay-image-vert',
    };

    const STABLE_POLY_OPACITY_ACTIVE = 0.6;
    const STABLE_POLY_OPACITY_INACTIVE = 0.2;
    const COLOR_RED = '#ba0c2f';
    const COLOR_WHITE = '#ffffff';
    const COLOR_BLACK = '#000000';

    const COLORS = [
      '#da6037',
      '#c67b1e',
      '#59791d',
      '#3cb00b',
      '#3b79be',
      '#594673',
      '#cf64a7',
      '#dc506a',
      '#89421f',
      '#8d8c14',
      '#365218',
      '#279541',
      '#616ca7',
      '#ae67e8',
      '#8b3350',
    ];

    const LAYER_GROUPS = {
      POLY_FILL_STROKE: [],
      POLY_FILL: [],
      POLY_VERT_STROKE: [],
      POLY_VERT: [],
    };

    let VERT_CACHE = {
      // 0: {
      //    properties: {
      //        name: '0',
      //        /* various other properties */
      //    },
      //    geometry: {
      //        type: 'MultiPolygon',
      //        coordinates: [
      //            [
      //                [[0, 0], [1, 1], [2, 2], [0, 0]],
      //                []
      //            ],
      //            []
      //        ]
      //    },
      //    coordinates: {
      //        '0,0,0': [0, 0],
      //        '0,0,1': [1, 1],
      //        '0,0,2': [2, 2],
      //        '0,0,3': [0, 0]
      //    },
      //    features: [
      //        /* vFeature */
      //    ],
      // }
    };

    let EDGE_CACHE = {
      // 0: {
      //    features: [
      //        /* eFeature */
      //    ],
      // }
    };

    const self = {

      /** ************************************************
       * Constants
       **************************************************/

      STYLES: STYLES,

      SOURCES: SOURCES,

      LAYERS: LAYERS,

      /** ************************************************
       * Variables
       **************************************************/

      LAYER_GROUPS: LAYER_GROUPS,

      /** ************************************************
       * Map setup
       **************************************************/

      initMap: function initMap(options) {
        const center = nvMaps.getDefaultLatLngForDomain();

        _.defaults(options, {
          container: 'map',
          style: STYLES.STREETS,
          center: [center.lng, center.lat],
          zoom: nvMaps.getDefaultZoomLevelForDomain(),
        });

        mapboxgl.accessToken = nv.config.map.key;

        self.destroy();

        return new mapboxgl.Map(options);
      },

      initPolygonDrawing: function initPolygonDrawing(map) {
        self.addPointSource(map, SOURCES.BASE);
        map.addLayer(circle(LAYERS.BASE, SOURCES.BASE, COLOR_BLACK, 1));

        self.addPointSource(map, SOURCES.PSEUDO_POLY_FILL_STROKE);
        self.addPointSource(map, SOURCES.PSEUDO_POLY_FILL);
        self.addPointSource(map, SOURCES.PSEUDO_POLY_VERT_STROKE);
        self.addPointSource(map, SOURCES.PSEUDO_POLY_VERT);

        self.addMultiPolygonSource(map, SOURCES.ACTIVE_POLY);
        self.addFeatureCollectionSource(map, SOURCES.ACTIVE_MOCK_VERT);
        self.addFeatureCollectionSource(map, SOURCES.ACTIVE_VERT);
        self.addFeatureCollectionSource(map, SOURCES.ACTIVE_LINE);
        self.addLineStringSource(map, SOURCES.CREATE_LINE);

        self.addPseudoPolyFillStrokeLayer(map, SOURCES.PSEUDO_POLY_FILL_STROKE);
        self.addPseudoPolyFillLayer(map, SOURCES.PSEUDO_POLY_FILL);
        self.addPseudoPolyVertStrokeLayer(map, SOURCES.PSEUDO_POLY_VERT_STROKE);
        self.addPseudoPolyVertLayer(map, SOURCES.PSEUDO_POLY_VERT);

        self.addActivePolyFillStrokeLayer(map, LAYERS.ACTIVE_POLY_FILL_STROKE, SOURCES.ACTIVE_POLY);
        self.addActivePolyMockVertLayer(map, LAYERS.ACTIVE_MOCK_VERT, SOURCES.ACTIVE_MOCK_VERT);
        self.addActivePolyVertStrokeLayer(map, LAYERS.ACTIVE_POLY_VERT_STROKE, SOURCES.ACTIVE_VERT);
        self.addActivePolyVertLayer(map, LAYERS.ACTIVE_POLY_VERT, SOURCES.ACTIVE_VERT);
        self.addActiveDragLineLayer(map, LAYERS.ACTIVE_LINE, SOURCES.ACTIVE_LINE);
        self.addActiveDragVertStrokeLayer(map, LAYERS.ACTIVE_VERT_STROKE, SOURCES.ACTIVE_LINE);
        self.addActiveDragVertLayer(map, LAYERS.ACTIVE_VERT, SOURCES.ACTIVE_LINE);
        self.addCreateDragLineLayer(map, LAYERS.CREATE_LINE, SOURCES.CREATE_LINE);
      },

      initChooseCircleArea: function initChooseCircleArea(map, options) {
        const center = map.getCenter();

        _.defaults(options, {
          coordinates: [center.lng, center.lat],
          radius: 1000,
        });

        self.addPointSource(map, SOURCES.CHOOSE_CIRCLE_AREA, options.coordinates);
        self.addChooseCircleAreaLayer(
          map, LAYERS.CHOOSE_CIRCLE_AREA, SOURCES.CHOOSE_CIRCLE_AREA, options.radius
        );
      },

      initImageSource: (map, url, coords = [], opacity = 0.7) => {
        self.addImageSource(map, SOURCES.OVRLAY, url, coords);
        self.addLineStringSource(map, SOURCES.OVRLAY_EDGE_TOP, coords.slice(0, 2));
        self.addLineStringSource(map, SOURCES.OVRLAY_EDGE_RGT, coords.slice(1, 3));
        self.addLineStringSource(map, SOURCES.OVRLAY_EDGE_BTM, coords.slice(2, 4));
        self.addLineStringSource(map, SOURCES.OVRLAY_EDGE_LFT, [coords[0], coords[3]]);
        self.addMultiPointSource(map, SOURCES.OVRLAY_CORNERS, coords);

        self.addRasterLayer(map, LAYERS.OVRLAY_IMAGE, SOURCES.OVRLAY, opacity, LAYERS.BASE);
      },

      removeChooseCircleArea: function removeChooseCircleArea(map) {
        map.removeLayer(LAYERS.CHOOSE_CIRCLE_AREA);
        map.removeSource(SOURCES.CHOOSE_CIRCLE_AREA);
      },

      removeImageSource: (map) => {
        map.removeLayer(LAYERS.OVRLAY_IMAGE);
        map.removeSource(SOURCES.OVRLAY_CORNERS);
        map.removeSource(SOURCES.OVRLAY_EDGE_LFT);
        map.removeSource(SOURCES.OVRLAY_EDGE_BTM);
        map.removeSource(SOURCES.OVRLAY_EDGE_RGT);
        map.removeSource(SOURCES.OVRLAY_EDGE_TOP);
        map.removeSource(SOURCES.OVRLAY);
      },

      addImageAdjustLayers: (map) => {
        self.addActiveDragLineLayer(map, LAYERS.OVRLAY_IMAGE_EDGE_TOP, SOURCES.OVRLAY_EDGE_TOP);
        self.addActiveDragLineLayer(map, LAYERS.OVRLAY_IMAGE_EDGE_RGT, SOURCES.OVRLAY_EDGE_RGT);
        self.addActiveDragLineLayer(map, LAYERS.OVRLAY_IMAGE_EDGE_BTM, SOURCES.OVRLAY_EDGE_BTM);
        self.addActiveDragLineLayer(map, LAYERS.OVRLAY_IMAGE_EDGE_LFT, SOURCES.OVRLAY_EDGE_LFT);
        self.addActivePolyVertStrokeLayer(map,
          LAYERS.OVRLAY_IMAGE_VERT_STROKE, SOURCES.OVRLAY_CORNERS);
        self.addActivePolyVertLayer(map, LAYERS.OVRLAY_IMAGE_VERT, SOURCES.OVRLAY_CORNERS);
      },

      removeImageAdjustLayers: (map) => {
        map.removeLayer(LAYERS.OVRLAY_IMAGE_VERT);
        map.removeLayer(LAYERS.OVRLAY_IMAGE_VERT_STROKE);
        map.removeLayer(LAYERS.OVRLAY_IMAGE_EDGE_LFT);
        map.removeLayer(LAYERS.OVRLAY_IMAGE_EDGE_BTM);
        map.removeLayer(LAYERS.OVRLAY_IMAGE_EDGE_RGT);
        map.removeLayer(LAYERS.OVRLAY_IMAGE_EDGE_TOP);
      },

      getInitialImageBounds: (map, width) => {
        // there is a bug in mapbox where the image disappears if
        // its size is reduced beyond its initialization size
        const center = map.project(map.getBounds().getCenter());
        const topLft = map.unproject([center.x - (width / 2), center.y - (width / 2)]);
        const topRgt = map.unproject([center.x + (width / 2), center.y - (width / 2)]);
        const btmRgt = map.unproject([center.x + (width / 2), center.y + (width / 2)]);
        const btmLft = map.unproject([center.x - (width / 2), center.y + (width / 2)]);
        return [
          [topLft.lng, topLft.lat],
          [topRgt.lng, topRgt.lat],
          [btmRgt.lng, btmRgt.lat],
          [btmLft.lng, btmLft.lat],
        ];
      },

      getNewPolygonId: function getNewPolygonId() {
        const size = _.size(VERT_CACHE);
        for (let i = 0; i < size; ++i) {
          if (!VERT_CACHE[i]) {
            return i.toString();
          }
        }
        return size.toString();
      },

      getLargestNewPolygonId: function getLargestNewPolygonId() {
        const max = _(VERT_CACHE).keys().map(_.toNumber).max();
        return _.isUndefined(max) ? 0 : max + 1;
      },

      getVertGeometry: function getVertGeometry(pId) {
        return VERT_CACHE[pId].geometry;
      },

      getVertFeatures: function getVertFeatures(pId) {
        return VERT_CACHE[pId].features;
      },

      getEdgeFeatures: function getEdgeFeatures(pId) {
        return EDGE_CACHE[pId].features;
      },

      getAllPolyGeometries: function getAllPolyGeometries() {
        return _.map(VERT_CACHE, val => ({ properties: val.properties, geojson: val.geometry }));
      },

      existsPolygon: function existsPolygon(pId) {
        return !!VERT_CACHE[pId] && !!EDGE_CACHE[pId];
      },

      findRenderedIndexByFeatureProperties: function findRenderedIndexByFeatureProperties(fpId) {
        return _.findKey(VERT_CACHE, { properties: { id: `${fpId}` } });
      },

      hasAnyPolygon: function hasAnyPolygon() {
        return !_.isEmpty(VERT_CACHE) && !_.isEmpty(EDGE_CACHE);
      },

      /**
       * Adds an object with a multi polygon geometry object to the map.
       *
       * @param map The MapboxGL map object.
       * @param item The data item containing the GeoJSON information and other data.
       * @returns {String} The index of the new multi polygon.
       */
      addPolygon: function addPolygon(map, item) {
        const index = self.getNewPolygonId();
        addPolygonAt(map, item, index);
        return index;
      },

      /**
       * Adds a list of objects with multi polygon geometry objects to the map.
       *
       * @param map The MapboxGL map object.
       * @param items The array of data items containing the GeoJSON information and other data.
       */
      addPolygons: function addPolygons(map, items) {
        const startIndex = self.getLargestNewPolygonId();
        const indexes = _.map(items, (item, index) => {
          const idx = startIndex + index;
          addPolygonAt(map, item, idx);
          return idx;
        });
        return indexes;
      },

      removePolygon: function removePolygon(map, index) {
        const pId = index.toString();           // e.g. 0, 1, 2
        const eId = SOURCES.STABLE_LINE(index); // e.g. 0-edges, 1-edges, 2-edges

        removeLayer(LAYERS.STABLE_POLY_FILL_STROKE, LAYER_GROUPS.POLY_FILL_STROKE);
        removeLayer(LAYERS.STABLE_POLY_FILL, LAYER_GROUPS.POLY_FILL);
        removeLayer(LAYERS.STABLE_POLY_VERT_STROKE, LAYER_GROUPS.POLY_VERT_STROKE);
        removeLayer(LAYERS.STABLE_POLY_VERT, LAYER_GROUPS.POLY_VERT);

        map.removeSource(pId);
        map.removeSource(eId);

        delete VERT_CACHE[pId];
        delete EDGE_CACHE[pId];

        function removeLayer(layer, layerGroup) {
          const layerId = layer(index);

          _.remove(layerGroup, id => id === layerId);

          map.removeLayer(layerId);
        }
      },

      removeAllPolygons: function removeAllPolygons(map) {
        removeLayers(LAYER_GROUPS.POLY_FILL_STROKE);
        removeLayers(LAYER_GROUPS.POLY_FILL);
        removeLayers(LAYER_GROUPS.POLY_VERT_STROKE);
        removeLayers(LAYER_GROUPS.POLY_VERT);

        _.forEach(VERT_CACHE, (val, pId) => {
          map.removeSource(pId);
          map.removeSource(SOURCES.STABLE_LINE(pId));
        });

        VERT_CACHE = {};
        EDGE_CACHE = {};

        function removeLayers(layerGroup) {
          while (layerGroup.length > 0) {
            try {
              map.removeLayer(layerGroup[0]);
            } finally {
              layerGroup.shift();
            }
          }
        }
      },

      insertVertex: function insertVertex(mockVertexFeature) {
        const currCoords = mockVertexFeature.geometry.coordinates; // the new coordinates []
        const pId = mockVertexFeature.properties.pId;              // e.g. '0'
        const prevId = mockVertexFeature.properties.v1Id;          // e.g. '0,0,1'
        const nextId = mockVertexFeature.properties.v2Id;          // e.g. '0,0,2' (always > prevId)

        // e.g. ['0','0','1']
        const i = nextId.split(',');
        // number of vertices of the polygon
        const n = VERT_CACHE[pId].geometry.coordinates[i[0]][i[1]].length;
        // index of the last vertex of the polygon
        let endIdx = n - 1;
        // index in the geometry.coordinates array to splice
        const gSpliceIdx = +i[2];
        // index in the features array to splice
        const fSpliceIdx = self.getFeaturesIndex(VERT_CACHE[pId].features, 'vId', nextId);
        const offset = fSpliceIdx - gSpliceIdx;

        // update the ids of the geometry.coordinates and features that come after the splice
        while (endIdx >= gSpliceIdx) {
          const frId = [i[0], i[1], endIdx].join(',');     // we want to update from this current id
          const toId = [i[0], i[1], endIdx + 1].join(','); // to this new id

          VERT_CACHE[pId].coordinates[toId] = VERT_CACHE[pId].coordinates[frId];
          VERT_CACHE[pId].features[offset + endIdx].properties.vId = toId;

          endIdx -= 1;
        }

        // splice into geometry.coordinates
        VERT_CACHE[pId].geometry.coordinates[i[0]][i[1]].splice(gSpliceIdx, 0, currCoords);

        // update the coordinates object
        VERT_CACHE[pId].coordinates[nextId] = currCoords;

        // splice into features
        VERT_CACHE[pId].features.splice(fSpliceIdx, 0, self.vFeature(pId, nextId, currCoords));

        // ////////////////////////////////////////////////

        const eSpliceIdx = self.getFeaturesIndex(EDGE_CACHE[pId].features, 'v2Id', nextId);
        endIdx = eSpliceIdx + (n - 1 - gSpliceIdx);

        while (endIdx >= eSpliceIdx) {
          const feat = EDGE_CACHE[pId].features[endIdx];

          feat.properties.v1Id = feat.properties.v2Id;
          feat.properties.v2Id = self.incrementVertexId(feat.properties.v2Id);

          endIdx -= 1;
        }

        // save the first coordinates of the edge and update it to the new coordinates
        const prevCoords = EDGE_CACHE[pId].features[eSpliceIdx].geometry.coordinates[0];
        EDGE_CACHE[pId].features[eSpliceIdx].geometry.coordinates[0] = currCoords;

        // splice into features
        EDGE_CACHE[pId].features.splice(eSpliceIdx, 0, self.eFeature(
          pId, prevId, nextId, prevCoords, currCoords
        ));

        return nextId;
      },

      removeVertex: function removeVertex(map, vertexFeature) {
        const pId = vertexFeature.properties.pId;                          // e.g. '0'
        const currId = vertexFeature.properties.vId;                       // e.g. '0,0,1'
        const i = currId.split(',');                                       // e.g. ['0','0','1']
        const n = VERT_CACHE[pId].geometry.coordinates[i[0]][i[1]].length; // # vertices of polygon

        if (n <= 4) {
          VERT_CACHE[pId].geometry.coordinates.splice(+i[0], 1);
          if (VERT_CACHE[pId].geometry.coordinates.length === 0) {
            self.removePolygon(map, pId);
          } else {
            self.loadCaches(VERT_CACHE[pId].geometry, pId);
          }
          return;
        }

        // index of the last vertex of the polygon
        const endIdx = n - 1;
        // index in the geometry.coordinates array to splice
        const gSpliceIdx = +i[2];
        // index in the features array to splice
        const fSpliceIdx = self.getFeaturesIndex(VERT_CACHE[pId].features, 'vId', currId);
        const offset = fSpliceIdx - gSpliceIdx;
        let startIdx = gSpliceIdx;
        let thisId = currId;

        // update the ids of the geometry.coordinates and features that come after the splice
        while (startIdx < endIdx) {
          const nextId = [i[0], i[1], startIdx + 1].join(',');

          VERT_CACHE[pId].coordinates[thisId] = VERT_CACHE[pId].coordinates[nextId];
          VERT_CACHE[pId].features[offset + startIdx + 1].properties.vId = thisId;

          thisId = nextId;
          startIdx += 1;
        }

        // splice from geometry.coordinates
        VERT_CACHE[pId].geometry.coordinates[i[0]][i[1]].splice(gSpliceIdx, 1);

        // delete from the coordinates object
        delete VERT_CACHE[pId].coordinates[thisId];

        // splice from features
        VERT_CACHE[pId].features.splice(fSpliceIdx, 1);

        // handle updating the edge cases
        if (gSpliceIdx === 0) {
          VERT_CACHE[pId].geometry.coordinates[i[0]][i[1]][endIdx - 1] =
            VERT_CACHE[pId].geometry.coordinates[i[0]][i[1]][0];
          VERT_CACHE[pId].coordinates[[
            i[0],
            i[1],
            endIdx - 1,
          ].join(',')] = VERT_CACHE[pId].coordinates[[
            i[0],
            i[1],
            0,
          ].join(',')];
          VERT_CACHE[pId].features[(offset + endIdx) - 1].geometry.coordinates =
            VERT_CACHE[pId].features[offset].geometry.coordinates;
        } else if (gSpliceIdx === endIdx) {
          VERT_CACHE[pId].geometry.coordinates[i[0]][i[1]][0] =
            VERT_CACHE[pId].geometry.coordinates[i[0]][i[1]][endIdx - 1];
          VERT_CACHE[pId].coordinates[[i[0], i[1], 0].join(',')] = VERT_CACHE[pId].coordinates[[
            i[0],
            i[1],
            endIdx - 1,
          ].join(',')];
          VERT_CACHE[pId].features[offset].geometry.coordinates =
            VERT_CACHE[pId].features[(offset + endIdx) - 1].geometry.coordinates;
        }

        // ////////////////////////////////////////////////

        let eSpliceIdx;
        let feat;
        let x;

        if (gSpliceIdx === 0) {
          const id = [i[0], i[1], 1].join(',');
          eSpliceIdx = self.getFeaturesIndex(EDGE_CACHE[pId].features, 'v2Id', id);
          EDGE_CACHE[pId].features.splice(eSpliceIdx, 1);

          for (x = eSpliceIdx; x < (eSpliceIdx + endIdx) - gSpliceIdx; ++x) {
            feat = EDGE_CACHE[pId].features[x];
            feat.properties.v1Id = self.decrementVertexId(feat.properties.v1Id);
            feat.properties.v2Id = self.decrementVertexId(feat.properties.v2Id);
          }

          EDGE_CACHE[pId].features[(eSpliceIdx + endIdx) - gSpliceIdx - 1].geometry.coordinates[1] =
            EDGE_CACHE[pId].features[eSpliceIdx].geometry.coordinates[0];
        } else if (gSpliceIdx === endIdx) {
          eSpliceIdx = self.getFeaturesIndex(EDGE_CACHE[pId].features, 'v2Id', currId);
          EDGE_CACHE[pId].features.splice(eSpliceIdx, 1);

          EDGE_CACHE[pId].features[eSpliceIdx - (gSpliceIdx - 1)].geometry.coordinates[0] =
            EDGE_CACHE[pId].features[eSpliceIdx - 1].geometry.coordinates[1];
        } else {
          eSpliceIdx = self.getFeaturesIndex(EDGE_CACHE[pId].features, 'v2Id', currId);
          EDGE_CACHE[pId].features.splice(eSpliceIdx, 1);

          for (x = eSpliceIdx; x < (eSpliceIdx + endIdx) - gSpliceIdx; ++x) {
            feat = EDGE_CACHE[pId].features[x];
            feat.properties.v1Id = self.decrementVertexId(feat.properties.v1Id);
            feat.properties.v2Id = self.decrementVertexId(feat.properties.v2Id);
          }

          if (gSpliceIdx === 1) {
            EDGE_CACHE[pId].features[eSpliceIdx].geometry.coordinates[0] =
              EDGE_CACHE[pId].features[(eSpliceIdx + endIdx) - gSpliceIdx - 1].geometry.coordinates[1];
          } else {
            EDGE_CACHE[pId].features[eSpliceIdx].geometry.coordinates[0] =
              EDGE_CACHE[pId].features[eSpliceIdx - 1].geometry.coordinates[1];
          }
        }
      },


      createMarker: function createMarker(map, options) {
        const nOptions = _.defaults(options, {
          icon: 'location_on',
          class: 'marker',
          lat: 0,
          lng: 0,
        });

        const el = document.createElement('div');
        el.className = 'marker';
        const iEl = document.createElement('i');
        iEl.className = 'material-icons';
        iEl.appendChild(document.createTextNode(nOptions.icon));
        el.appendChild(iEl);
        const marker = new mapboxgl.Marker(el, { offset: [-30 / 2, -30] })
            .setLngLat([options.lng, options.lat])
            .addTo(map);
        return marker;
      },

      removeMarker: function removeMarker(marker) {
        return marker.remove();
      },

      createSquare: function createSquare(map, event, length) {
        const coordinates = [];
        const half = length / 2;

        const p = map.project([event.lngLat.lng, event.lngLat.lat]);
        const p0 = map.unproject([p.x - half, p.y - half]);
        const p1 = map.unproject([p.x - half, p.y + half]);
        const p2 = map.unproject([p.x + half, p.y + half]);
        const p3 = map.unproject([p.x + half, p.y - half]);

        coordinates.push([p0.lng, p0.lat]);
        coordinates.push([p1.lng, p1.lat]);
        coordinates.push([p2.lng, p2.lat]);
        coordinates.push([p3.lng, p3.lat]);
        coordinates.push(coordinates[0]); // push the reference

        return coordinates;
      },

      /**
     * Function to remove number of vertex
     * @param {*} coordinates
     */
      minimizeVertex: function minimizeVertex(coordinates) {
        const coords = _.map(coordinates, (v, k) => {
          return {
            key: k,
            coordinate: v,
          };
        });
        const radToDegree = 57.2958;
        let allMinimized = false;
        const deletedCoords = [];
        while (!allMinimized) {
          const markForDeletion = [];
          for (let i = 0; i < coords.length; i++) {
            if (!coords[i + 1] || !coords[i + 2]) {
              break;
            }
            if (findAngle(coords[i].coordinate, coords[i + 1].coordinate, coords[i + 2].coordinate) <= 3) {
              markForDeletion.push(coords[i + 1].key);
              deletedCoords.push(coords[i + 1].coordinate);
            }
          }
          if (markForDeletion.length == 0) {
            allMinimized = true;
          }
          _.remove(coords, (c) => {
            return markForDeletion.indexOf(c.key) >= 0;
          });
        }

        return {
          newCoords: _.map(coords, v => (v.coordinate)),
          deleteCoords: deletedCoords,
        };

        function findAngle(p0, p1, p2) {
          const b = Math.pow(p1[0] - p0[0], 2) + Math.pow(p1[1] - p0[1], 2);
          const a = Math.pow(p1[0] - p2[0], 2) + Math.pow(p1[1] - p2[1], 2);
          const c = Math.pow(p2[0] - p0[0], 2) + Math.pow(p2[1] - p0[1], 2);

          const degree = Math.acos((a + b - c) / Math.sqrt(4 * a * b)) * radToDegree;
          return Math.abs(180 - degree);
        }
      },

      setPolygonProperty: function setPolygonProperty(pId, prop, val) {
        if (VERT_CACHE[pId]) {
          if (!VERT_CACHE[pId].properties) {
            VERT_CACHE[pId].properties = {};
          }
          VERT_CACHE[pId].properties[prop] = val;
        }
      },

      setPolygonProperties: function setPolygonProperties(pId, properties) {
        if (VERT_CACHE[pId]) {
          VERT_CACHE[pId].properties = properties;
        }
      },

      destroy: function destroy() {
        _.forEach(LAYER_GROUPS, (key) => {
          LAYER_GROUPS[key] = [];
        });
        VERT_CACHE = {};
        EDGE_CACHE = {};
      },

      setStyle: function setStyle(map, style) {
        map.setStyle(style);
      },

      /**
       * Converts a list of geometry objects to a list of multi polygon geometry objects.
       *
       * @example
       * var x = [
       *   ...,
       *   { ..., geojson: { type: 'Polygon', coordinates: [] }, ..., },
       *   { ..., geojson: { type: 'MultiPolygon', coordinates: [] }, ..., },
       *   { ..., location: { type: 'Polygon', coordinates: [] }, ..., },
       *   { ..., location: { type: 'MultiPolygon', coordinates: [] }, ..., },
       *   ...,
       * ];
       * var y = [
       *   ...,
       *   { ..., geojson: { type: 'MultiPolygon', coordinates: [] }, ..., },
       *   { ..., geojson: { type: 'MultiPolygon', coordinates: [] }, ..., },
       *   { ..., location: { type: 'MultiPolygon', coordinates: [] }, ..., },
       *   { ..., location: { type: 'MultiPolygon', coordinates: [] }, ..., },
       *   ...,
       * ];
       * toMultiPolygonGeometries(x) == y;
       *
       * @param json An array or object containing GeoJSON geometry information.
       * @returns {Array} A list of objects with multi polygon geometry objects.
       * @throws Will throw an error for unhandled GeoJSON geometry types.
       */
      toMultiPolygonGeometries: function toMultiPolygonGeometries(json) {
        if (_.isArray(json)) {
          return _.map(json, (item) => {
            convertGeometry(item.geojson || item.location || item.geometry);
            return item;
          });
        } else if (_.isPlainObject(json)) {
          return _.map(json.features, (feature) => {
            convertGeometry(feature.geometry);
            return feature;
          });
        } else {
          return [];
        }
      },

      toMultiPolygonGeometry: function toMultiPolygonGeometry(json) {
        if (_.isPlainObject(json)) {
          convertGeometry(json.geojson || json.location || json.geometry);
          return json;
        }
        return json;
      },

      /**
       * fly to a specific coordinate on the map
       */
      flyTo: function flyTo(map, lng, lat) {
        map.flyTo({
          center: [lng, lat],
        });
      },

      /**
       * A vertex feature.
       *
       * @param pId The multi polygon id this vertex belongs to.
       * @param vId The vertex id.
       * @param coordinates The {@code [lng, lat]} coordinate array.
       * @returns {*} The vertex feature.
       */
      vFeature: function vFeature(pId, vId, coordinates) {
        return {
          type: 'Feature',
          properties: {
            pId: pId,
            vId: vId,
          },
          geometry: {
            type: 'Point',
            coordinates: coordinates,
          },
        };
      },

      /**
       * An edge feature.
       *
       * @param pId The multi polygon id this edge belongs to.
       * @param v1Id The 1st vertex id of this edge.
       * @param v2Id The 2nd vertex id of this edge.
       * @param coordinates1 The {@code [lng, lat]} coordinate array of the 1st vertex.
       * @param coordinates2 The {@code [lng, lat]} coordinate array of the 2nd vertex.
       * @returns {*} The edge feature.
       */
      eFeature: function eFeature(pId, v1Id, v2Id, coordinates1, coordinates2) {
        return {
          type: 'Feature',
          properties: {
            pId: pId,
            v1Id: v1Id,
            v2Id: v2Id,
          },
          geometry: {
            type: 'LineString',
            coordinates: [
              coordinates1,
              coordinates2,
            ],
          },
        };
      },

      /**
       * Setup the vertex and edge caches so that we can easily reference them.
       *
       * Also assigns the coordinates to reference the same objects so that when
       * we change one, the others are also automatically modified.
       *
       * @param geometry The multi polygon GeoJSON object.
       * @param id The cache id (i.e. the multi polygon id).
       */
      loadCaches: function loadCaches(geometry, id) {
        VERT_CACHE[id] = {
          geometry: geometry,
          coordinates: {},
          features: [],
        };

        EDGE_CACHE[id] = {
          features: [],
        };

        for (let i = 0; i < geometry.coordinates.length; ++i) {
          for (let j = 0; j < geometry.coordinates[i].length; ++j) {
            const length = geometry.coordinates[i][j].length;

            // assign the last coordinate set as a reference to the
            // first coordinate set, so that we can drag them together
            geometry.coordinates[i][j][length - 1] = geometry.coordinates[i][j][0];

            for (let k = 0; k < length; ++k) {
              const vert0Id = [i, j, k - 1].join(',');
              const vert1Id = [i, j, k].join(',');
              const coords0 = geometry.coordinates[i][j][k - 1];
              const coords1 = geometry.coordinates[i][j][k];

              VERT_CACHE[id].coordinates[vert1Id] = coords1;

              VERT_CACHE[id].features.push(self.vFeature(id, vert1Id, coords1));

              if (k > 0) {
                EDGE_CACHE[id].features.push(self.eFeature(id, vert0Id, vert1Id, coords0, coords1));
              }
            }
          }
        }
      },

      /** ************************************************
       * Source setup
       **************************************************/

      addSource: (map, id, name, geometry) => {
        map.addSource(id, {
          type: 'geojson',
          data: {
            type: 'Feature',
            properties: {
              id: id,
              name: name,
            },
            geometry: geometry,
          },
        });
      },

      addMultiPolygonSource: (map, id, coordinates) => {
        map.addSource(id, {
          type: 'geojson',
          data: {
            type: 'Feature',
            properties: {
              id: id,
            },
            geometry: {
              type: 'MultiPolygon',
              coordinates: coordinates || [],
            },
          },
        });
      },

      addLineStringSource: (map, id, coordinates) => {
        map.addSource(id, {
          type: 'geojson',
          data: {
            type: 'Feature',
            properties: {
              id: id,
            },
            geometry: {
              type: 'LineString',
              coordinates: coordinates || [[0, 0], [0, 0]],
            },
          },
        });
      },

      addPointSource: (map, id, coordinates) => {
        map.addSource(id, {
          type: 'geojson',
          data: {
            type: 'Feature',
            properties: {
              id: id,
            },
            geometry: {
              type: 'Point',
              coordinates: coordinates || [],
            },
          },
        });
      },

      addFeatureCollectionSource: (map, id, features) => {
        map.addSource(id, {
          type: 'geojson',
          data: {
            type: 'FeatureCollection',
            features: features || [],
          },
        });
      },

      addImageSource: (map, id, url, coordinates = []) => {
        map.addSource(id, {
          type: 'image',
          url: url,
          coordinates: coordinates,
        });
      },

      addMultiPointSource: (map, id, coordinates = []) => {
        map.addSource(id, {
          type: 'geojson',
          data: {
            type: 'FeatureCollection',
            features: _.map(coordinates, (c, idx) => ({
              type: 'Feature',
              properties: {
                id: idx,
              },
              geometry: {
                type: 'Point',
                coordinates: c,
              },
            })),
          },
        });
      },

      /** ************************************************
       * Layer setup
       **************************************************/

      addPseudoPolyFillStrokeLayer: (map, source) => {
        map.addLayer(circle(LAYERS.PSEUDO_POLY_FILL_STROKE, source, COLOR_BLACK, 1));
      },

      addPseudoPolyFillLayer: (map, source) => {
        map.addLayer(circle(LAYERS.PSEUDO_POLY_FILL, source, COLOR_BLACK, 1));
      },

      addPseudoPolyVertStrokeLayer: (map, source) => {
        map.addLayer(circle(LAYERS.PSEUDO_POLY_VERT_STROKE, source, COLOR_BLACK, 1));
      },

      addPseudoPolyVertLayer: (map, source) => {
        map.addLayer(circle(LAYERS.PSEUDO_POLY_VERT, source, COLOR_BLACK, 1));
      },

      addStablePolyFillStrokeLayer: (map, source, index) => {
        const layerId = LAYERS.STABLE_POLY_FILL_STROKE(index);
        map.addLayer(line(layerId, source, COLOR_BLACK, 1), LAYERS.PSEUDO_POLY_FILL_STROKE);
        LAYER_GROUPS.POLY_FILL_STROKE.push(layerId);
      },

      addStablePolyFillLayer: (map, source, index) => {
        const layerId = LAYERS.STABLE_POLY_FILL(index);
        map.addLayer(
          fill(layerId, source, COLORS[index % COLORS.length], STABLE_POLY_OPACITY_INACTIVE),
          LAYERS.PSEUDO_POLY_FILL
        );
        LAYER_GROUPS.POLY_FILL.push(layerId);
      },

      addStablePolyVertStrokeLayer: (map, source, index) => {
        const layerId = LAYERS.STABLE_POLY_VERT_STROKE(index);
        map.addLayer(circle(layerId, source, COLOR_BLACK, 3), LAYERS.PSEUDO_POLY_VERT_STROKE);
        LAYER_GROUPS.POLY_VERT_STROKE.push(layerId);
      },

      addStablePolyVertLayer: (map, source, index) => {
        const layerId = LAYERS.STABLE_POLY_VERT(index);
        map.addLayer(circle(layerId, source, COLOR_WHITE, 2), LAYERS.PSEUDO_POLY_VERT);
        LAYER_GROUPS.POLY_VERT.push(layerId);
      },

      addActivePolyFillStrokeLayer: (map, id, source) => {
        map.addLayer(line(id, source, COLOR_BLACK, 2));
      },

      addActivePolyMockVertLayer: (map, id, source) => {
        map.addLayer(circle(id, source, COLOR_BLACK, 3));
      },

      addActivePolyVertStrokeLayer: (map, id, source) => {
        map.addLayer(circle(id, source, COLOR_BLACK, 5));
      },

      addActivePolyVertLayer: (map, id, source) => {
        map.addLayer(circle(id, source, COLOR_WHITE, 3));
      },

      addActiveDragLineLayer: (map, id, source) => {
        map.addLayer(line(id, source, COLOR_BLACK, 1, [0.2, 2]));
      },

      addActiveDragVertStrokeLayer: (map, id, source) => {
        map.addLayer(circle(id, source, COLOR_BLACK, 7));
      },

      addActiveDragVertLayer: (map, id, source) => {
        map.addLayer(circle(id, source, COLOR_RED, 5));
      },

      addCreateDragLineLayer: (map, id, source) => {
        map.addLayer(line(id, source, COLOR_BLACK, 2, [0.2, 2]));
      },

      addChooseCircleAreaLayer: (map, id, source, radius) => {
        map.addLayer({
          id: id,
          source: source,
          type: LAYER_TYPES.CIRCLE,
          paint: {
            'circle-color': COLOR_RED,
            'circle-opacity': 0.5,
            'circle-radius': {
              base: 2,
              stops: [
                [0, 0],
                [20, metersToPixelsAtMaxZoom(radius || 1000, 0)],
              ],
            },
          },
        });
      },

      addRasterLayer: (map, id, source, opacity = 0.7, before = undefined) => {
        map.addLayer({
          id: id,
          source: source,
          type: 'raster',
          paint: {
            'raster-opacity': opacity,
            'raster-fade-duration': 0, // otherwise when resizing the image todo
          },
        }, before);
      },

      /** ************************************************
       * Layer controls
       **************************************************/

      setPolyActive: function setPolyActive(map, index) {
        map.setPaintProperty(LAYERS.STABLE_POLY_FILL(index), 'fill-opacity', STABLE_POLY_OPACITY_ACTIVE);
      },

      setPolyInactive: function setPolyInactive(map, index) {
        map.setPaintProperty(LAYERS.STABLE_POLY_FILL(index), 'fill-opacity', STABLE_POLY_OPACITY_INACTIVE);
      },

      setChooseCircleRadius: function setChooseCircleRadius(map, radius, latitude) {
        map.setPaintProperty(LAYERS.CHOOSE_CIRCLE_AREA, 'circle-radius', {
          base: 2,
          stops: [
            [0, 0],
            [20, metersToPixelsAtMaxZoom(radius, latitude)],
          ],
        });
      },

      setImageOpacity: (map, opacity) => {
        map.setPaintProperty(LAYERS.OVRLAY_IMAGE, 'raster-opacity', opacity);
      },

      updateStablePolyData: function updateStablePolyData(map, polySrcId, name) {
        map.getSource(polySrcId).setData({
          type: 'Feature',
          properties: {
            id: polySrcId,
            name: name || polySrcId,
          },
          geometry: VERT_CACHE[polySrcId].geometry,
        });
      },

      updateStableLineData: function updateStableLineData(map, polySrcId) {
        map.getSource(SOURCES.STABLE_LINE(polySrcId)).setData({
          type: 'FeatureCollection',
          features: EDGE_CACHE[polySrcId].features,
        });
      },

      updateImageData: (map, coords) => {
        map.getSource(SOURCES.OVRLAY).setCoordinates(coords);

        map.getSource(SOURCES.OVRLAY_EDGE_TOP).setData({
          type: 'Feature',
          properties: {
            id: SOURCES.OVRLAY_EDGE_TOP,
          },
          geometry: {
            type: 'LineString',
            coordinates: coords.slice(0, 2),
          },
        });

        map.getSource(SOURCES.OVRLAY_EDGE_RGT).setData({
          type: 'Feature',
          properties: {
            id: SOURCES.OVRLAY_EDGE_RGT,
          },
          geometry: {
            type: 'LineString',
            coordinates: coords.slice(1, 3),
          },
        });

        map.getSource(SOURCES.OVRLAY_EDGE_BTM).setData({
          type: 'Feature',
          properties: {
            id: SOURCES.OVRLAY_EDGE_BTM,
          },
          geometry: {
            type: 'LineString',
            coordinates: coords.slice(2, 4),
          },
        });

        map.getSource(SOURCES.OVRLAY_EDGE_LFT).setData({
          type: 'Feature',
          properties: {
            id: SOURCES.OVRLAY_EDGE_LFT,
          },
          geometry: {
            type: 'LineString',
            coordinates: [coords[0], coords[3]],
          },
        });

        map.getSource(SOURCES.OVRLAY_CORNERS).setData({
          type: 'FeatureCollection',
          features: _.map(coords, (c, idx) => ({
            type: 'Feature',
            properties: {
              id: idx,
            },
            geometry: {
              type: 'Point',
              coordinates: c,
            },
          })),
        });
      },

      setActivePolygonData: function setActivePolygonData(map, polySrcId) {
        setActivePolyData(map, polySrcId);
        setActiveVertData(map, polySrcId);
        setActiveMockVertData(map, polySrcId); // todo - optimize: update changes only
      },

      setActiveLineData: function setActiveLineData(map, features) {
        map.getSource(SOURCES.ACTIVE_LINE).setData({
          type: 'FeatureCollection',
          features: features,
        });
      },

      setCreateLineData: function setCreateLineData(map, coordinates) {
        map.getSource(SOURCES.CREATE_LINE).setData({
          type: 'Feature',
          geometry: {
            type: 'LineString',
            coordinates: coordinates,
          },
        });
      },

      setCircleData: function setCircleData(map, coordinates) {
        map.getSource(SOURCES.CHOOSE_CIRCLE_AREA).setData({
          type: 'Feature',
          properties: {
            id: SOURCES.CHOOSE_CIRCLE_AREA,
          },
          geometry: {
            type: 'Point',
            coordinates: coordinates || [],
          },
        });
      },

      /**
       * Clears the active multi polygon data sources.
       *
       * @param map The MapboxGL map object.
       */
      resetActivePolygonData: function resetActivePolygonData(map) {
        resetActivePolyData(map);
        resetActiveVertData(map);
        resetActiveMockVertData(map);
      },

      resetCreateLineData: function resetCreateLineData(map) {
        map.getSource(SOURCES.CREATE_LINE).setData({
          type: 'Feature',
          geometry: {
            type: 'LineString',
            coordinates: [[0, 0], [0, 0]],
          },
        });
      },

      /**
       *
       * @param pId The polygon id, e.g. '0'
       * @param currId The current vertex id, e.g. '0,0,1'
       * @returns {*[]} The new feature collection for rendering the dragging operation.
       */
      initDragFeatures: function initDragFeatures(pId, currId) {
        const i = currId.split(',');                                       // e.g. ['0','0','1']
        const n = VERT_CACHE[pId].geometry.coordinates[i[0]][i[1]].length; // # vertices of polygon

        let prevIndex = +i[2] - 1;
        if (prevIndex < 0) {   // we are dragging the first vertex in the polygon
          prevIndex = n - 2; // therefore the previous vertex is the 2nd-last vertex in the list
        }

        let nextIndex = +i[2] + 1;
        if (nextIndex >= n) { // we are dragging the last vertex in the polygon
          nextIndex = 1;    // therefore the next vertex is the 2nd vertex in the list
        }

        const prevId = [i[0], i[1], prevIndex].join(',');
        const nextId = [i[0], i[1], nextIndex].join(',');

        return [
          {
            // this feature is used by LAYER_ACTIVE_VERT_STROKE & LAYER_ACTIVE_VERT
            // to display the midpoint (we need this feature because mapbox optimizes
            // by ignoring the midpoints when points on the LineString are collinear)
            type: 'Feature',
            geometry: {
              type: 'Point',
              coordinates: VERT_CACHE[pId].coordinates[currId],
            },
          }, {
            // this feature is used by LAYER_ACTIVE_LINE to display the dotted
            // drag lines (we use this feature for visualizing the dragging operation)
            type: 'Feature',
            geometry: {
              type: 'LineString',
              coordinates: [
                VERT_CACHE[pId].coordinates[prevId],
                VERT_CACHE[pId].coordinates[currId],
                VERT_CACHE[pId].coordinates[nextId],
              ],
            },
          },
        ];
      },

      snapCoordinates: function snapCoordinates(map, event, point, features, pId, threshold) {
        const closest = {
          dist: Infinity,
        };

        _(features)
          .filter(feature => feature.properties.pId !== pId) // ignore self-edges
          .forEach((feature) => {
            const v1 = VERT_CACHE[feature.properties.pId].coordinates[feature.properties.v1Id];
            const v2 = VERT_CACHE[feature.properties.pId].coordinates[feature.properties.v2Id];
            const p1 = map.project(v1);
            const p2 = map.project(v2);
            const dist = L.LineUtil.pointToSegmentDistance(point, p1, p2);

            if (dist < closest.dist) {
              closest.dist = dist;
              closest.v1 = v1;
              closest.v2 = v2;
              closest.p1 = p1;
              closest.p2 = p2;
            }
          });

        if (closest.dist <= threshold) {
          const pClosest = L.LineUtil.closestPointOnSegment(point, closest.p1, closest.p2);
          const dist1 = closest.p1.dist(pClosest);
          const dist2 = closest.p2.dist(pClosest);

          if (dist1 < dist2 && dist1 <= threshold) {
            // snap to v1 (coordinates array)
            return closest.v1;
          }

          if (dist2 < dist1 && dist2 <= threshold) {
            // snap to v2 (coordinates array)
            return closest.v2;
          }

          // snap to the closest point on the edge (coordinates .lng and .lat object)
          return map.unproject(pClosest);
        }

        // don't snap to anything (coordinates .lng and .lat object)
        return event.lngLat;
      },

      computeBoundingBox: function computeBoundingBox(point, threshold) {
        return [
          [point.x - threshold, point.y - threshold],
          [point.x + threshold, point.y + threshold],
        ];
      },

      incrementVertexId: function incrementVertexId(id) {
        const i = id.split(',');
        return [i[0], i[1], +i[2] + 1].join(',');
      },

      decrementVertexId: function decrementVertexId(id) {
        const i = id.split(',');
        return [i[0], i[1], +i[2] - 1].join(',');
      },

      getFeaturesIndex: function getFeaturesIndex(features, key, value) {
        return _.findIndex(features, feature => feature.properties[key] === value);
      },

      convertToPolygonGeometry: function convertToPolygonGeometry(geometry) {
        switch (geometry.type) {
          case 'Polygon':
            break;
          case 'MultiPolygon':
            geometry.type = 'Polygon';
            geometry.coordinates = geometry.coordinates[0];
            break;
          default:
            throw new Error(`Unhandled GeoJSON type: ${geometry.type}`);
        }
      },

    };

    return self;

    function addPolygonAt(map, item, index) {
      const pId = index.toString();           // e.g. 0, 1, 2
      const eId = SOURCES.STABLE_LINE(index); // e.g. 0-edges, 1-edges, 2-edges
      const geometry = item.geojson || item.location || item.geometry;
      const properties = item.properties || _.omit(item, 'geojson', 'location');
      const location = _.compact([
        properties.city,
        properties.district,
        properties.province,
      ]).join(', ');
      const name = properties.id || properties.name || location || pId;

      self.loadCaches(geometry, pId);

      self.addSource(map, pId, name, geometry);
      self.addFeatureCollectionSource(map, eId, EDGE_CACHE[index].features);

      self.addStablePolyFillStrokeLayer(map, eId, index);
      self.addStablePolyFillLayer(map, pId, index);
      self.addStablePolyVertStrokeLayer(map, pId, index);
      self.addStablePolyVertLayer(map, pId, index);

      self.setPolygonProperties(pId, properties); // cache existing properties
      self.setPolygonProperty(pId, 'id', name);   // add the id property if it doesn't exist
    }

    function setActivePolyData(map, polySrcId) {
      map
        .getSource(SOURCES.ACTIVE_POLY)
        .setData({
          type: 'Feature',
          properties: {
            id: SOURCES.ACTIVE_POLY,
          },
          geometry: VERT_CACHE[polySrcId].geometry,
        });
    }

    function setActiveVertData(map, polySrcId) {
      map
        .getSource(SOURCES.ACTIVE_VERT)
        .setData({
          type: 'FeatureCollection',
          features: VERT_CACHE[polySrcId].features,
        });
    }

    function setActiveMockVertData(map, polySrcId) {
      map
        .getSource(SOURCES.ACTIVE_MOCK_VERT)
        .setData({
          type: 'FeatureCollection',
          features: _.map(EDGE_CACHE[polySrcId].features, (feature, index) => {
            const p1 = map.project(feature.geometry.coordinates[0]);
            const p2 = map.project(feature.geometry.coordinates[1]);
            const pMid = { x: (p1.x + p2.x) / 2, y: (p1.y + p2.y) / 2 };
            const coordinates = map.unproject(pMid);

            return {
              type: 'Feature',
              properties: {
                pId: feature.properties.pId,
                index: index,
                v1Id: feature.properties.v1Id,
                v2Id: feature.properties.v2Id,
              },
              geometry: {
                type: 'Point',
                coordinates: [coordinates.lng, coordinates.lat],
              },
            };
          }),
        });
    }

    function resetActivePolyData(map) {
      map
        .getSource(SOURCES.ACTIVE_POLY)
        .setData({
          type: 'Feature',
          properties: {
            id: SOURCES.ACTIVE_POLY,
          },
          geometry: {
            type: 'MultiPolygon',
            coordinates: [],
          },
        });
    }

    function resetActiveVertData(map) {
      map
        .getSource(SOURCES.ACTIVE_VERT)
        .setData({
          type: 'FeatureCollection',
          features: [],
        });
    }

    function resetActiveMockVertData(map) {
      map
        .getSource(SOURCES.ACTIVE_MOCK_VERT)
        .setData({
          type: 'FeatureCollection',
          features: [],
        });
    }

    function prefix(p, index) {
      return _.isUndefined(index) ? undefined : p + index;
    }

    function suffix(s, index) {
      return _.isUndefined(index) ? undefined : index + s;
    }

    function line(id, source, color, width, dasharray) {
      return {
        id: id,
        source: source,
        type: LAYER_TYPES.LINE,
        layout: {
          'line-cap': 'round',
          'line-join': 'round',
        },
        paint: _.omit({
          'line-color': color,
          'line-width': width,
          'line-dasharray': dasharray,
        }, dasharray ? undefined : 'line-dasharray'),
      };
    }

    function fill(id, source, color, opacity) {
      return {
        id: id,
        source: source,
        type: LAYER_TYPES.FILL,
        paint: {
          'fill-color': color,
          'fill-opacity': opacity,
        },
      };
    }

    function circle(id, source, color, radius) {
      return {
        id: id,
        source: source,
        type: LAYER_TYPES.CIRCLE,
        paint: {
          'circle-color': color,
          'circle-radius': radius,
        },
      };
    }

    function metersToPixelsAtMaxZoom(meters, latitude) {
      return meters / 0.075 / Math.cos(latitude * (Math.PI / 180));
    }

    function convertGeometry(geometry) {
      switch (geometry.type) {
        case 'Polygon':
          geometry.type = 'MultiPolygon';
          geometry.coordinates = [geometry.coordinates];
          break;
        case 'MultiPolygon':
          break;
        default:
          throw new Error(`Unhandled GeoJSON type: ${geometry.type}`);
      }
    }
  }
}());
