(function mapboxService() {
  angular
        .module('nvCommons.services')
        .factory('nvMapbox', mapbox);

  mapbox.$inject = ['$http'];

  function mapbox($http) {
    const self = {
      initialize: function initialize(mapId, options) {
        let map = null;
        L.mapbox.accessToken = nv.config.map.key;
        map = L.mapbox.map(mapId, 'mapbox.streets').setView([options.latitude, options.longitude], options.zoomLevel);

        map.zoomControl.setPosition(options.controlPosition);
        map.addControl(L.mapbox.geocoderControl('mapbox.places', {
          position: options.controlPosition,
        }));

        L.tileLayer(
                    `https://api.mapbox.com/styles/v1/mapbox/streets-v9/tiles/{z}/{x}/{y}?access_token=${L.mapbox.accessToken}`, {
                      tileSize: 512,
                      zoomOffset: -1,
                    }).addTo(map);

        setTimeout(() => {
          map.invalidateSize();
        }, 500);

        return map;
      },

      flyToLocation: function flyToLocation(map, options) {
        map.panTo(new L.LatLng(options.latitude, options.longitude));
      },

      addPolygon: function addPolygon(map, polygonData) {
        const options = _.cloneDeep(polygonData);
        const polygon = L.polygon(polygonData.latLngs, options);
        map.addLayer(polygon);

        return polygon;
      },

      removePolygon: function removePolygon(map, polygon) {
        return map.removeLayer(polygon);
      },

      addMarker: function addMarker(map, markerData) {
        const loc = new L.LatLng(markerData.latitude, markerData.longitude);

        const options = {
          position: loc,
        };

        _.forEach(markerData, (value, key) => {
          if (key === 'zIndex') {
            options.zIndexOffset = value;
          } else {
            options[key] = value;
          }
        });

        if (markerData.label) {
          options.icon = new L.DivIcon({
            className: 'div-icon',
            html: `<img class="div-image" src="${markerData.iconUrl}" width="${markerData.iconSize[0]}" height="${markerData.iconSize[1]}"/>` +
                        `<span class="div-span">${markerData.label}</span>`,
          });
        } else if (markerData.iconUrl) {
          options.icon = L.icon({
            iconUrl: markerData.iconUrl,
            iconSize: markerData.iconSize,
          });
        }

        const marker = new L.Marker(loc, options);
        map.addLayer(marker);

        return marker;
      },

      removeMarker: function removeMarker(map, marker) {
        return map.removeLayer(marker);
      },

      markerAddEvent: function markerAddEvent(marker, eventName, callback) {
        marker.on(eventName, function on(e) {
          callback(this, e);
        });
      },

      markerAddInfoWindow: function markerAddInfoWindow(marker, infoWindowData) {
        const options = _.cloneDeep(infoWindowData);
        marker.bindLabel(infoWindowData.text, options);
      },

      markerShowInfoWindow: function markerShowInfoWindow(marker) {
        return marker.showLabel();
      },

      markerHideInfoWindow: function markerHideInfoWindow(marker) {
        return marker.hideLabel();
      },

      markerSetZIndex: function markerSetZIndex(marker, zIndex) {
        return marker.setZIndexOffset(zIndex);
      },

      markerSetIcon: function markerSetIcon(marker, options) {
        return marker.setIcon(L.icon({
          iconUrl: options.iconUrl,
          iconSize: options.iconSize,
        }));
      },

      markerSetOpacity: function markerSetOpacity(marker, opacityValue) {
        return marker.setOpacity(opacityValue);
      },

      markerSetLatLng: function markerSetLatLng(marker, options) {
        return marker.setLatLng(L.latLng(options.latitude, options.longitude));
      },

      reverseGeocoding: (lat, lng) => (
        $http.get(`https://api.mapbox.com/geocoding/v5/mapbox.places/${lng},${lat}.json?access_token=${nv.config.map.key}`)
        .then(payload =>
          _.map(_.get(payload.data, 'features', {}), feature => ({
            address1: _.get(feature, 'place_name', ''),
            address2: _.get(feature, 'address', ''),
            latitude: _.get(feature, 'geometry.coordinates[1]', null),
            longitude: _.get(feature, 'geometry.coordinates[0]', null),
          }))
        )
      ),
    };

    return self;
  }
}());
