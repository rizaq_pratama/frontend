(function service() {
  angular
    .module('nvCommons.services')
    .factory('nvDomain', nvDomain);

  nvDomain.$inject = ['$rootScope', '$window', 'nvDOMManipulation', '$state', 'nvAuth', 'nvClientStore'];

  function nvDomain($rootScope, $window, nvDOMManipulation, $state, nvAuth, nvClientStore) {
    /**
     * List the currently supported countries. We should
     * update this list whenever we add a new country.
     */
    const countries = ['sg', 'my', 'id', 'vn', 'ph', 'th', 'mm'];
    const countryNames = {
      sg: 'Singapore',
      my: 'Malaysia',
      id: 'Indonesia',
      vn: 'Vietnam',
      ph: 'Philippines',
      th: 'Thailand',
      mm: 'Myanmar',
    };

    const saasCountryMap = {
      fef: 'sg',
      hbl: 'sg',
      mbs: 'id',
      mnt: 'id',
      mmpg: 'mm',
      tkl: 'sg',
      demo: 'id',
      msi: 'id',
    };

    const DOMAIN_REGEX = /#\/(\w+)\//;

    return {
      init: init,
      getDomain: getDomain,
      getCountryName: getCountryName,
      reloadDomain: reloadDomain,
      consumeStateParam: consumeStateParam,
      replaceDomain: replaceDomain,
    };

    function init() {
      /**
       * Get the authorized domains for the current user, e.g.
       * {@code $rootScope.domains = ['sg', 'my', 'id']}.
       */
      $rootScope.domains = nvAuth.getDomains();

      /**
       * No domains, might due to still using legacy auth cookie.
       */
      if ($rootScope.domains.length === 0) {
        if (_.get($state, 'current.name') !== 'login') {
          nvClientStore.localStorage.set('previousUrl', $window.location.href, { namespace: false });
        }

        $rootScope.logout();
        return;
      }

      /**
       * Default the current domain to the first available one,
       * e.g. {@code $rootScope.domain = 'mbs'}.
       */
      $rootScope.domain = $rootScope.domains[0];

      /**
       * Default the current country Id to the first available one,
       * e.g. {@code $rootScope.countryId = 'sg'}.
       */
      $rootScope.countryId = countries[0];

      $rootScope.countries = countries;

      /**
       * The value of {@code $rootScope.ninjasaas} is true if none of the
       * authorized domains is in the {@code $rootScope.countries} list.
       */
      $rootScope.ninjasaas = !_.some($rootScope.domains,
        _.partial(_.includes, $rootScope.countries));
    }

    function getDomain() {
      return {
        current: $rootScope.domain,
        currentCountry: _.get(saasCountryMap, $rootScope.domain, $rootScope.domain),
        systemIdFromUrl: _.get($window.location.hash.match(DOMAIN_REGEX), '[1]', null),
        ninjasaas: $rootScope.ninjasaas,
        all: $rootScope.domains,
      };
    }

    function getCountryName(country) {
      return countryNames[country];
    }

    function reloadDomain(domain, $stateParams) {
      $stateParams.domain = domain;
      $state.go($state.current, $stateParams, { reload: true, inherit: false, notify: true });
    }

    function consumeStateParam($stateParams) {
      if (!_.includes($rootScope.domains, $stateParams.domain)) {
        // default to the most recently assigned domain
        reloadDomain($rootScope.domain, $stateParams);
        return;
      }
      $rootScope.domain = $stateParams.domain; // update the current domain

      const domain = getDomain();
      $rootScope.countryId = domain.currentCountry;

      nvDOMManipulation.reloadFavicon($rootScope.domain);
    }

    function replaceDomain(url) {
      return url.replace('$country', $window.location.hash.match(DOMAIN_REGEX)[1]);
    }
  }
}());
