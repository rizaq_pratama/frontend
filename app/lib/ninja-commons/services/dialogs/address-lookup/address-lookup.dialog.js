;(function() {
    'use strict';

    angular
        .module('nvCommons.services')
        .factory('nvAddressLookupDialog', addressLookupDialog);

    addressLookupDialog.$inject = ['$rootScope', '$mdDialog', 'nvAddress', 'nvExtendUtils', 'NgTableParams'];

    function addressLookupDialog($rootScope, $mdDialog, nvAddress, nvExtendUtils, NgTableParams) {

        var self = {

            scopeTemplate: function(options) {
                options = _.defaults(options, {
                    onDone: $mdDialog.hide,
                    onCancel: $mdDialog.cancel
                });

                return {
                    addresses: [],
                    addressLookupPromise: null,
                    searchText: '',
                    onDone: function(address) {
                        this.searchText = '';
                        options.onDone.call(this, address);
                    },
                    onCancel: function($event) {
                        this.searchText = '';
                        options.onCancel.call(this, $event);
                    },
                    onAddressChange: function(searchText) {
                        var scope = this;
                        if (!!searchText) {
                            scope.addressLookupPromise = nvAddress.lookup(searchText, $rootScope.domain).then(success);
                        } else {
                            scope.addresses = [];
                        }

                        function success(results) {
                            if (noResults(results)) {
                                scope.addresses = [];
                            } else {
                                scope.addresses = nvExtendUtils.addFloat(results, 'jaro_winkler_score', 3);
                                scope.addressesTableParams = new NgTableParams({count: 100}, {counts: [], dataset: scope.addresses});
                            }
                        }

                        function noResults(results) {
                            return results.length === 0 || (results.length === 1 && results[0].jaro_winkler_score === 0);
                        }
                    },
                    getDisplayAddress: nvAddress.format
                };
            },

            show: function(options) {
                return $mdDialog.show(_.defaults(options || {}, {
                    templateUrl: 'lib/ninja-commons/services/dialogs/address-lookup/address-lookup.dialog.html',
                    parent: angular.element(document.body),
                    clickOutsideToClose: true,
                    controller: function($scope) {
                        _.assign($scope, self.scopeTemplate());
                    }
                }));
            }

        };

        return self;

    }

})();
