(function controller() {
  angular
    .module('nvCommons.services')
    .controller('BulkActionProgressDialogController', BulkActionProgressDialogController);

  BulkActionProgressDialogController.$inject = [
    '$mdDialog', 'payload', '$scope',
  ];

  function BulkActionProgressDialogController(
    $mdDialog, payload, $scope
  ) {
    // variables
    const STATE = {
      UPDATING: 'updating',
      UPDATED: 'updated',
    };
    const ctrl = this;
    ctrl.payload = _.defaults(payload, {
      totalCount: 0, // should more than 1
      currentIndex: 0, // should start from 1
      successCount: 0,
      errors: [], // push error message string here if item fail to process
      messages: {
        updating: 'dialog.updating-x-of-y-items',
        updated: 'dialog.updated-x-of-y-items',
        error: 'dialog.failed-to-update-x-items',
      },
    });

    ctrl.STATE = STATE;
    ctrl.currentState = STATE.UPDATING;

    // initialize
    $scope.$watch('[ctrl.payload.successCount, ctrl.payload.errors.length]', (newVal, oldVal) => {
      if (newVal !== oldVal) {
        // update currentState if finish processing all
        if ((ctrl.payload.successCount + ctrl.payload.errors.length) >= ctrl.payload.totalCount) {
          ctrl.currentState = STATE.UPDATED;

          // straight close progress dialog if no error
          if (ctrl.payload.errors.length <= 0) {
            onHide();
          }
        }
      }
    });

    // functions
    ctrl.onHide = onHide;
    ctrl.getProgressPercentage = getProgressPercentage;

    // functions details
    function getProgressPercentage() {
      return (ctrl.payload.currentIndex / ctrl.payload.totalCount) * 100;
    }

    function onHide() {
      $mdDialog.hide();
    }
  }
}());
