;(function() {
    'use strict';

    angular
        .module('nvCommons.services')
        .factory('nvEditModelDialog', editModelDialog);

    editModelDialog.$inject = ['$compile', '$log', '$q', '$mdDialog', 'nvToast', 'nvUtils', '$timeout', 'nvAddressLookupDialog', 'nvAddress'];

    function editModelDialog($compile, $log, $q, $mdDialog, nvToast, nvUtils, $timeout, addressLookupDialog, nvAddress) {

        // the different html input type templates
        var text = '<nv-input-text class="md-block" label="$displayName" model="fields[$idx].value" form="modelForm" disabled="$disabled" required="$required" help-text="$helpText"></nv-input-text>';
        var tel = '<nv-input-tel class="md-block" label="$displayName" model="fields[$idx].value" form="modelForm" disabled="$disabled" required="$required" help-text="$helpText"></nv-input-tel>';
        var email = '<nv-input-email class="md-block" label="$displayName" model="fields[$idx].value" form="modelForm" disabled="$disabled" required="$required" help-text="$helpText"></nv-input-email>';
        var number = '<nv-input-number class="md-block" label="$displayName" model="fields[$idx].value" form="modelForm" disabled="$disabled" required="$required" help-text="$helpText"></nv-input-number>';
        var date = '<nv-input-date class="md-block" label="$displayName" model="fields[$idx].value" form="modelForm" disabled="$disabled"></nv-input-date>';
        var checkbox = '<nv-input-checkbox class="md-block" label="$displayName" model="fields[$idx].value" form="modelForm" disabled="$disabled"></nv-input-checkbox>';
        var password = '<nv-input-password class="md-block" label="$displayName" model="fields[$idx].value" form="modelForm" disabled="$disabled" required="$required" help-text="$helpText"></nv-input-password>';
        var select = '<nv-input-select class="md-block" label="$displayName" model="fields[$idx].value" options="fields[$idx].options" form="modelForm" disabled="$disabled" required="$required" help-text="$helpText"></nv-input-select>';
        var address = '<nv-input-address class="md-block" label="$displayName" model="fields[$idx].value" form="modelForm" disabled="$disabled" required="$required" help-text="$helpText" open-dialog-callback="$openDialogCallback"></nv-input-address>';

        var self = {

            scopeTemplate: function(options) {
                options = _.defaults(options, {
                    onSave: angular.noop,
                    onCancel: angular.noop
                });

                return {
                    fields: [],
                    saveButtonState: 'clean',
                    cancelButtonDisabled: false,
                    onSave: function() {
                        var scope = this;
                        // todo - disable clickOutsideToClose and escapeToClose when saving
                        // https://github.com/angular/material/issues/1786
                        scope.saveButtonState = 'saving';
                        scope.cancelButtonDisabled = true;

                        // pass the new or modified fields to the onSave callback
                        $q.when(options.onSave(extractFields(scope.fields), scope.fields), $mdDialog.hide, function() {
                            scope.saveButtonState = 'clean';
                            scope.cancelButtonDisabled = false;
                            //onFailure('Save failed!', error);
                        });
                    },
                    onCancel: function() {
                        // todo - show a "confirm cancel?" dialog if the form is dirty
                        // https://github.com/angular/material/issues/698
                        $q.when(options.onCancel(), $mdDialog.cancel, function(error) {
                            onFailure('Cancel failed!', error);
                        });
                    }
                };

                function onFailure(title, error) {
                    $log.error('[edit-model.dialog.js][scopeTemplate]', error);
                    nvToast.error(
                        nvUtils.getOrElse(error, 'data.errorName', title),
                        nvUtils.getOrElse(error, 'data.errorMessage', 'Oops, something went wrong. Please try again later.')
                    );
                }
            },

            show: function(options) {
                return $mdDialog.show(_.defaults(options || {}, {
                    templateUrl: 'lib/ninja-commons/services/dialogs/edit-model/edit-model.dialog.html',
                    controller: self.createController(options.title, options.fields, {
                        onSave: options.onSave,
                        onCancel: options.onCancel
                    }),
                    parent: angular.element(document.body),
                    clickOutsideToClose: true,
                    onShowing: onShowing,
                    onSuccess: function() {},
                    onFailure: function() {}
                })).then(function(result) {
                    if (result && result._addressLookup) {
                        handleAddressLookup(options);
                    } else {
                        return $q.when(options.onSuccess(result), function() { return result; });
                    }
                }, $q.reject);
            },

            createController: function(title, fields, options) {
                options = _.defaults(options || {}, {
                    onSave: function() {},
                    onCancel: function() {}
                });

                // this is the actual controller used in the $mdDialog service
                controller.$inject = ['$scope'];
                function controller($scope) {
                    _.assign($scope, self.scopeTemplate(options));
                    $scope.title = title;
                    $scope.fields = fields;
                }
                return controller;
            }

        };

        return self;

        function onShowing(scope, dialogElement) {
            var htmlArr = [];

            // get all the input html elements to inject into the dialog
            _.forEach(scope.fields, function(field, idx) {
                if (field.hidden) { return; }
                switch (field.type) {
                    case 'text': htmlArr.push(getInputHtml(text, field, idx)); break;
                    case 'tel': htmlArr.push(getInputHtml(tel, field, idx)); break;
                    case 'email': htmlArr.push(getInputHtml(email, field, idx)); break;
                    case 'number': htmlArr.push(getInputHtml(number, field, idx)); break;
                    case 'date': htmlArr.push(getInputHtml(date, field, idx)); break;
                    case 'checkbox': htmlArr.push(getInputHtml(checkbox, field, idx)); break;
                    case 'password': htmlArr.push(getInputHtml(password, field, idx)); break;
                    case 'select': htmlArr.push(getInputHtml(select, field, idx)); break;
                    case 'address': htmlArr.push(getInputHtml(address, field, idx)); break;
                    default: break;
                }
            });

            // compile the input html elements with the current scope
            // and insert them into the correct position in the dialog
            var elements = $compile(htmlArr.join(''))(scope);
            var containerElement = dialogElement[0].getElementsByClassName('md-dialog-content')[0];
            _.forEach(elements, function(element) {
                containerElement.appendChild(element);
            });
        }

        function getInputHtml(template, field, idx) {
            return template
                .replace('$displayName', field.displayName)
                .replace(/\$idx/g, idx)
                .replace('$disabled', (!!field.disabled).toString())
                .replace('$required', (!!field.required).toString())
                .replace('$helpText', field.helpText || '')
                .replace('$openDialogCallback', field.openDialogCallback);
        }

        function extractFields(fields) {
            return _.reduce(fields, function(result, field, key) {
                if (field.value !== '' &&
                    field.value !== undefined) {
                    result[key] = field.value;
                } else if (field._deep) {
                    // a field marked as _deep is a JSON object, so we can recursively extract the fields
                    result[key] = extractFields(field);
                } else if (field._array) {
                    result[key] = field._values;
                    //result[key] = extractArray(field);
                }
                return result;
            }, {});
        }

        //function extractArray(field) {
        //    var arr = _.map(field, function(obj, key) {
        //        return _.map(obj.values, function(val) {
        //            var result = {}; result[key] = val; return result;
        //        });
        //    });
        //
        //    var maxLength = _.max(_.map(arr, function(a) { return a.length; }));
        //
        //    return _.map(_.range(maxLength), function(i) {
        //        return _.reduce(_.range(arr.length), function(result, j) {
        //            return _.merge(result, arr[j][i]);
        //        }, {});
        //    });
        //}

        function handleAddressLookup(options) {
            addressLookupDialog.show().then(onSuccess, onFailure);

            function onSuccess(address) {
                // todo - set form dirty
                options.fields[_.findIndex(options.fields, 'key', 'address1')].value = nvAddress.address1(address);
                options.fields[_.findIndex(options.fields, 'key', 'address2')].value = nvAddress.address2(address);
                options.fields[_.findIndex(options.fields, 'key', 'city')].value = nvAddress.city(address);
                options.fields[_.findIndex(options.fields, 'key', 'country')].value = nvAddress.country(address);
                options.fields[_.findIndex(options.fields, 'key', 'postcode')].value = address.postcode;
                options.fields[_.findIndex(options.fields, 'key', 'latitude')].value = address.latitude;
                options.fields[_.findIndex(options.fields, 'key', 'longitude')].value = address.longitude;
                self.show(options);
            }

            function onFailure() {
                self.show(options);
            }
        }

    }

})();
