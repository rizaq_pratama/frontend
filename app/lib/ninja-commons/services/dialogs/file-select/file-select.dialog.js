(function factory() {
  angular
    .module('nvCommons.services')
    .factory('nvFileSelectDialog', fileSelectDialog);

  fileSelectDialog.$inject = [
    '$interpolate',
    '$q',
    '$mdDialog',
    'nvDialog',
    'nvToast',
    'nvButtonFilePickerType',
    '$timeout',
  ];

  /**
   * nvFileSelectDialog
   * eg: nvFileSelectDialog.show($event, options)
   * options:
   *   title:                                 file select dialog title
   *   buttonChooseTitle:                     label for the buttonChoose
   *   buttonOkTitle:                         label for the buttonOk
   *   fileSelect.accept:                     file type the file select dialog accepts,
   *                                          eg: `nvButtonFilePickerType.IMAGE`
   *   fileSelect.maxFileCount:               maximum number of files can be selected
   *   fileSelect.maxFileSize:                maximum file size for each file that can be selected
   *   fileSelect.maxTotalSize:               maximum total file size that can be selected
   *   onFinishSelect(files):                 callback function when buttonOk is clicked
   *   onFileClicked($event, file, $index):   callback function when the selected file is clicked
   *   onDone($event):                        callback function after dialog has been closed
   *                                          after buttonOk is clicked
   *   onCancel($event):                      callback function after dialog has been closed
   *                                          after 'x' button is clicked
   */

  function fileSelectDialog(
    $interpolate,
    $q,
    $mdDialog,
    nvDialog,
    nvToast,
    nvButtonFilePickerType,
    $timeout
  ) {
    return {
      scopeTemplate: function scopeTemplate(params) {
        const options = _.defaultsDeep(params, {
          onFinishSelect: angular.noop,
          onFileClicked: angular.noop,
          onDone: angular.noop,
          onCancel: angular.noop,
          fileSelect: {
            accept: nvButtonFilePickerType.ANY,
            maxFileCount: Number.MAX_VALUE,
            maxFileSize: Number.MAX_VALUE,
            maxTotalSize: Number.MAX_VALUE,
          },
          title: 'File Upload',
          buttonChooseTitle: 'Choose',
          buttonOkTitle: 'OK',
          sampleCSV: null,
          showErrorToast: true,
          multiple: true,
          isImagePicker: false,
          imageMaxSize: [99999, 99999],
          skipHide: false,
        });

        return {
          fileSelected: [],
          fileSelectOptions: options.fileSelect,
          title: options.title,
          buttonChooseTitle: options.buttonChooseTitle,
          buttonOkTitle: options.buttonOkTitle,
          sampleCSV: options.sampleCSV,
          multiple: options.multiple,
          isImagePicker: options.isImagePicker,
          imageMaxSize: options.imageMaxSize,
          skipHide: options.skipHide,
          errorMessage: {
            fileSizeExceeded: $interpolate($interpolate('File size: \\\{\\\{ size | nvByte \\\}\\\}. Maximum file size ({{ maxFileSize | nvByte }}) allowed exceeded.')(options.fileSelect)),
            totalSizeExceeded: $interpolate($interpolate('Total file size: \\\{\\\{ size | nvByte \\\}\\\}. Maximum total file size ({{ maxTotalSize | nvByte }}) allowed exceeded.')(options.fileSelect)),
          },
          saveButtonState: 'clean',
          onFinishSelect: function onFinishSelect() {
            const self = this;
            this.saveButtonState = 'saving';
            $q.when(options.onFinishSelect.call(this, this.fileSelected))
              .then(self.onDone.bind(self))
              .catch((e) => {
                if (options.showErrorToast) {
                  nvToast.error(e || 'Error');
                }
              })
              .finally(() => {
                self.saveButtonState = 'clean';
              });
          },
          onDone: function onDone(...args) {
            this.fileSelected.length = 0;
            $mdDialog.hide()
              .then(_.partial(_.spread(options.onDone), args));
            this.modelForm.$setPristine();
          },
          onCancel: function onCancel($event) {
            this.fileSelected.length = 0;
            $mdDialog.cancel()
              .then(_.partial(options.onCancel, $event));
            this.modelForm.$setPristine();
          },
          onFileClicked: function onFileClicked($event, file, index) {
            options.onFileClicked.call(this, $event, file, index);
          },
          removeFile: function removeFile(event, file, index) {
            this.fileSelected.splice(index, 1);
            if (this.fileSelected.length === 0) {
              this.modelForm.$setPristine();
            }
          },
          onFileSelect: function onFileSelect(file) {
            if (this.multiple && this.fileSelected.length === this.fileSelectOptions.maxFileCount) {
              nvToast.error('Maximum number of files allowed exceeded.');
              return;
            }
            if (file.size > this.fileSelectOptions.maxFileSize) {
              nvToast.error(this.errorMessage.fileSizeExceeded(file));
              return;
            }
            const resultTotalSize = _.sumBy(this.fileSelected, 'size');
            if (resultTotalSize > this.fileSelectOptions.maxTotalSize) {
              nvToast.error(this.errorMessage.fileSizeExceeded({ size: resultTotalSize }));
              return;
            }

            this.modelForm.$setDirty();

            const index = _.findIndex(this.fileSelected, f => f.name === file.name);

            if (index === -1) {
              if (this.multiple) {
                this.fileSelected.push(file);
                if (options.isImagePicker) {                  
                  buildImage(file, this.fileSelected.length - 1, this.fileSelected);
                }
              } else {
                this.fileSelected[0] = file;
                if (options.isImagePicker) {
                  buildImage(file, 0, this.fileSelected);
                }
              }
              this.$parent.$apply();
            }

            function buildImage(fileToRead, id, selectedFiles) {
              const fr = new FileReader();
              fr.onload = () => {
                document.getElementById(`img-thumbnail-${id}`).src = fr.result;
                const img = new Image();
                img.onload = () => {
                  if (img.width > options.imageMaxSize[0] || img.height > options.imageMaxSize[1]) {
                    nvToast.error('Image size exceeding allowed resolution');
                    $timeout(() => {
                      _.pullAt(selectedFiles, id);
                    });
                  }
                }
                img.src = fr.result;
              };
              fr.readAsDataURL(fileToRead);
            }
          },
          onFileReject: function onFileReject(file) {
            nvToast.error(`"${file.name}" is not allowed.`);
          },
        };
      },

      show: function show($event, options) {
        const scope = this.scopeTemplate(options);
        return nvDialog.showSingle($event, {
          templateUrl: 'lib/ninja-commons/services/dialogs/file-select/file-select.dialog.html',
          cssClass: 'file-select',
          theme: 'nvGreen',
          scope: scope,
          skipHide: scope.skipHide,
        });
      },
    };
  }
}());
