(function service() {
  angular
        .module('nvCommons.services')
        .factory('nvDialog', dialogService);

  dialogService.$inject = ['$q', '$log', '$templateRequest', '$compile', '$mdDialog', '$translate'];

  function dialogService($q, $log, $templateRequest, $compile, $mdDialog, $translate) {
    const type = {
      dialog: 'nv-dialog',
      datasetDialog: 'nv-dataset-dialog',
    };

    const theme = {
      default: 'nvBlue',
      creation: 'nvGreen',
      deletion: 'nvRed',
    };

    return {
      /**
       * Confirm save
       * @param  {event} event
       *          The click event that trigger the open of the dialog,
       *          the location of the click will be used as the starting point for the
       *          opening animation of the the dialog.
       * @return {promise}
       *         A promise that can be resolved with 'save' or rejected with 'cancel'
       */
      confirmSave: function confirmSave(event, options) {
        const opt = _.defaults(options || {}, {
          title: $translate.instant('dialog.confirm-save.title'),
          content: $translate.instant('dialog.confirm-save.message'),
          ok: $translate.instant('commons.save'),
          cancel: $translate.instant('commons.cancel'),
          skipHide: false,
        });

        return $mdDialog.show(
                    $mdDialog.confirm({
                      skipHide: opt.skipHide,
                    })
                        .title(opt.title)
                        .content(opt.content)
                        .targetEvent(event)
                        .ok(opt.ok)
                        .cancel(opt.cancel)
                        .theme(theme.creation)
                );
      },

      /**
       * Alert with a save failure message
       * @param  {event} event
       *          The click event that trigger the open of the dialog,
       *          the location of the click will be used as the starting point
       *          for the opening animation of the the dialog.
       * @return {promise}      A promise then resolved when the user close the alert
       */
      alertSaveFailure: function alertSaveFailure(event) {
        return $mdDialog.show(
                    $mdDialog.alert()
                        .title($translate.instant('dialog.save-failed.title'))
                        .content($translate.instant('dialog.save-failed.message'))
                        .targetEvent(event)
                        .ok($translate.instant('commons.ok'))
                        .theme(theme.deletion)
                );
      },

      /**
       * Prompt a confirm dialog whether to delete an item
       * @param  {event} event
       *          The click event that trigger the open of the dialog,
       *          the location of the click will be used as the starting point
       *          for the opening animation of the the dialog.
       * @param  {object} options Options
       *          - {string} title, default to $translate('dialog.confirm-delete.title')
       *          - {string} content, default to $translate('dialog.confirm-delete.message')
       *          - {ok}     delete text, default to $translate('commons.delete')
       *          - {cancel} cancel text, default to $translate('commons.cancel')
       * @return {promise}
       *          A promise that can be resolved when the user confirms to delete,
       *          and rejected with cancel
       */
      confirmDelete: function confirmDelete(event, options) {
        const opt = _.defaults(options || {}, {
          title: $translate.instant('dialog.confirm-delete.title'),
          content: $translate.instant('dialog.confirm-delete.message'),
          ok: $translate.instant('commons.delete'),
          cancel: $translate.instant('commons.cancel'),
          skipHide: false,
          theme: theme.deletion,
        });

        return $mdDialog.show(
                    $mdDialog.confirm({
                      skipHide: opt.skipHide,
                    })
                        .targetEvent(event)
                        .title(opt.title)
                        .htmlContent(opt.content)
                        .ok(opt.ok)
                        .cancel(opt.cancel)
                        .theme(opt.theme)
                );
      },

      /**
       *
       * Show a single $mdDialog
       * @param  {event}   event
       *          The click event that trigger the open of the dialog,
       *          the location of the click will be used as the starting point
       *          for the opening animation of the the dialog.
       * @param  {object}  dialog the dialog options
       *          - {string}          cssClass
       *              The cssClass to be applied to the md-dialog element, default ''
       *          - {string}          theme
       *              The theme to be applied to the md-dialog element,
       *              eg: 'nvBlue', 'nvGreen', etc, default 'nvBlue'
       *          - {string}          templateUrl
       *              The url of a template that will be used as the content of the dialog.
       *          - {object}          scope*
       *              The plain object that all its property will be
       *              copied over to the $scope of the controller
       *          - {string|function} controller*
       *              The controller to associate with the dialog
       *          - {string}          controllerAs
       *              An alias to assign the controller to on the scope.
       *          - {object}          locals
       *              An object containing key/value pairs.
       *              The keys will be used as names of values to inject into the controller.
       *          - {string}          type
       *              The type of the dialog: 'nv-dialog' or default: 'nv-dataset-dialog'
       *          - {boolean}         skipHide
       *              Set true to enable open another dialog
       *          * `scope` is usually generated via `nvEditModelDialog.scopeTemplate`
       *          * `scope` and `controller` are used mutual exclusively,
       *             with `controller` given higher priority
       *
       * @return {promise}
       *          A promise that can be resolved with
       *          $mdDialog.hide() or rejected with $mdDialog.cancel()
       */
      showSingle: function showSingle(event, dialogOptions) {
        const dialog = _.defaults(dialogOptions || {}, {
          cssClass: '',
          theme: theme.default,
          type: type.datasetDialog,
          skipHide: false,
          clickOutsideToClose: true,
        });

        // some basic sanity checking
        if (!dialog.templateUrl) {
          $log.error('[dialog.service.js][showSingle]', 'dialog templateUrl not defined');
          return $q.reject();
        } else if (!_.isPlainObject(dialog.scope) &&
          !(_.isFunction(dialog.controller) ||
          _.isString(dialog.controller))) {
          $log.error('[dialog.service.js][showSingle]', 'invalid dialog scope');
          return $q.reject();
        }

        if (dialog.type !== type.datasetDialog && dialog.type !== type.dialog) {
          dialog.type = type.datasetDialog;
        }

        return $templateRequest(dialog.templateUrl).then(templateHtml =>
           $mdDialog.show({
             targetEvent: event,
             template: ['<md-dialog class="', dialog.cssClass, ' ', dialog.type, '" aria-label="Dialog" md-theme="', dialog.theme, '">', templateHtml, '</md-dialog>'].join(''),
             parent: angular.element(document.body),
             clickOutsideToClose: dialog.clickOutsideToClose,
             bindToController: true,
             skipHide: dialog.skipHide,
             controller: angular.isDefined(dialog.controller) ? dialog.controller :
               getDefaultController(dialog.scope),
             locals: angular.isDefined(dialog.locals) ? dialog.locals : {},
             controllerAs: angular.isDefined(dialog.controllerAs) ? dialog.controllerAs : 'dialog',
           })
        );

        function getDefaultController(scope) {
          controller.$inject = ['$scope'];
          function controller($scope) {
            _.assign($scope, scope);
          }
          return controller;
        }
      },

      /**
       *
       * Show a multiple $mdDialog
       * @param  {event}   event
       *          the click event that trigger the open of the dialog,
       *          the location of the click will be used as
       *          the starting point for the opening animation of the the dialog.
       * @param  {Array|[]}   dialogsOptions
       *            An array of dialog options, for each dialog option:
       *             - {string}          cssClass
       *                  The cssClass to be applied to the md-dialog element, default ''
       *             - {string}          theme
       *                  The theme to be applied to the md-dialog element,
       *                  eg: 'nvBlue', 'nvGreen', etc, default 'nvBlue'
       *             - {string}          templateUrl
       *                  The url of a template that will be used as the content of the dialog.
       *             - {object}          scope
       *                  The plain object that all its property
       *                  will be copied over to the $scope of the controller
       *             - {string}          type
       *                  The type of the dialog: 'nv-dialog' or default: 'nv-dataset-dialog'
       *
       *             * `scope` is usually generated via `nvEditModelDialog.scopeTemplate`
       *             * @TODO add `controller`, `controllerAs`, `locals`?
       *
       * @return {promise}
       *            A promise that can be resolved with $mdDialog.hide()
       *            or rejected with $mdDialog.cancel()
       *
       */
      showMultiple: function showMultiple(event, dialogsOptions) {
        // temporary variable to store the dialog element so that we can refer to it later
        let dialogElement = null;

        // some basic sanity checking
        if (!dialogsOptions || dialogsOptions.length === 0) {
          $log.error('[dialog.service.js][showMultiple]', 'no dialogs defined');
          return $q.reject();
        }

        // setting defaults to dialogs
        const dialogs = _.map(dialogsOptions, (dialog) => {
          if (dialog.type !== type.datasetDialog && dialog.type !== type.dialog) {
            dialog.type = type.datasetDialog;
          }
          return _.defaults(dialog || {}, { cssClass: '', theme: theme.default });
        });

        controller.$inject = ['$scope', '$mdDialog'];

        const promises = _(dialogs).map('templateUrl').map($templateRequest).value();

        return $q.all(promises).then((templates) => {
          return $mdDialog.show({
            targetEvent: event,
            template: ['<md-dialog class="',
                        dialogs[0].cssClass,
                        ` ${dialogs[0].type}" aria-label="Dialog" md-theme="{{ $dialogTheme }}">`,
                        templates[0], '</md-dialog>'].join(''),
            parent: angular.element(document.body),
            clickOutsideToClose: true,
            controller: controller,
            onShowing: onShowing,
          });

          function onShowing(scope, element) {
            dialogElement = element;
            scope.templates = templates;
          }
        });

        function controller($scope, mdDialog) {
          // default to the first dialog
          _.assign($scope, dialogs[0].scope, { $dialogTheme: dialogs[0].theme });

          // these are internal 'public' methods that can be called with
          // "this" when defining the dialog's scope methods, e.g. this._onCancel
          $scope.onCancel = mdDialog.cancel;
          $scope.showDialog = showDialog;

          function showDialog(idx) {
            // the container node is the <md-dialog></md-dialog> element
            // make sure that we don't select the md-dialog-focus-trap node
            $scope.onCancel = idx > 0 ? _.partial(showDialog, idx - 1) : mdDialog.cancel;
            const containerNode = _.find(dialogElement[0].children, node => node.nodeName === 'MD-DIALOG');

            const content = dialogs[idx];
            const templateHtml = $scope.templates[idx];

            // remove any existing css classes
            _.forEach(dialogs, (dialog) => {
              _.forEach(dialog.cssClass.split(' '), (cssClass) => { containerNode.classList.remove(cssClass); });
            });

            // add the css class for the new dialog content
            if (content.cssClass) {
              _.forEach(content.cssClass.split(' '), (cssClass) => { containerNode.classList.add(cssClass); });
            }

            // remove the existing contents from the container node
            while (containerNode.lastChild) {
              containerNode.removeChild(containerNode.lastChild);
            }

            // setup the new scope variables
            // note: make sure that the scope variables in the templates
            // don't conflict with each other;
            // by design, all the templates share the same scope so that data passing is easier
            _.assign($scope, content.scope, { $dialogTheme: content.theme });

            // compile the template html against the new scope
            const elements = $compile(templateHtml)($scope);

            // append the compiled elements into the container node
            _.forEach(elements, (element) => {
              containerNode.appendChild(element);
            });
          }
        }
      },

      alert: (event, options) => {
        const opt = _.defaults(options || {}, {
          title: $translate.instant('commons.warning'),
          content: $translate.instant('dialog.alert.message'),
          ok: $translate.instant('commons.ok'),
          skipHide: false,
        });

        return $mdDialog.show(
          $mdDialog.alert({
            skipHide: opt.skipHide,
          })
          .targetEvent(event)
          .title(opt.title)
          .htmlContent(opt.content)
          .ok(opt.ok)
          .theme(opt.theme || theme.deletion)
        );
      },

      cancel: $mdDialog.cancel,
    };
  }
}());
