(function service() {
  /**
   * A simple wrapper for the fuse.js library.
   * Requires fuse.js to be included in the project.
   */
  angular
    .module('nvCommons.services')
    .factory('nvFuse', fuse);

  fuse.$inject = ['$window', '$log'];

  function fuse($window, $log) {
    if ($window.Fuse) {
      return $window.Fuse;
    }
    $log.error('fuse.service.js', 'fuse.js is not defined, have you included the lib files?');
    return null;
  }
}());
