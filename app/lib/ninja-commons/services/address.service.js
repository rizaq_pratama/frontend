(function service() {
  angular
        .module('nvCommons.services')
        .factory('nvAddress', addressService);

  addressService.$inject = ['Addressing'];

  function addressService(Addressing) {
    return {

      lookup: function lookup(searchText, country) {
        return Addressing.filterSearch(country, searchText);
      },

      format: function format(address, country) {
        return _.compact([
          address.raw_address,
          address.building_no,
          address.building,
          address.street,
          address.community,
          address.district,
          address.area,
          country !== 'sg' ? address.city : undefined, // ignore city if country is Singapore
          address.state,
          address.province,
          address.country,
          address.postcode,
        ]).join(' ');
      },

      address1: function address1(address) {
        return _.compact([
          address.raw_address,
          address.building_no,
          address.street,
        ]).join(' ');
      },

      address2: function address2(address) {
        return _.compact([
          address.building,
          address.unit_no,
          address.community,
          address.district,
          address.area,
        ]).join(' ');
      },

      city: function city(address) {
        return _.compact([
          expand(address.city),
          address.city && address.city.indexOf(address.province) === -1 ?
            address.province : undefined, // ignore province if same as city
        ]).join(' ');
      },

      country: function country(address) {
        return expand(address.country);
      },

      extract: function extract(obj) {
        return _.compact([
          obj.address1,
          obj.address2,
          obj.city,
          obj.country,
          obj.postcode,
        ]).join(' ');
      },

      getCountryCode: function getCountryCode(countryName) {
        switch (_.lowerCase(countryName)) {
          case 'singapore': return 'SG';
          case 'malaysia': return 'MY';
          case 'indonesia': return 'ID';
          case 'thailand': return 'TH';
          case 'philippines': return 'PH';
          case 'myanmar': return 'MM';
          case 'vietnam': return 'VN';
          default: return countryName;
        }
      },

    };

    function expand(country) {
      switch (country) {
        case 'SG': return 'Singapore';
        case 'MY': return 'Malaysia';
        case 'ID': return 'Indonesia';
        default: return country;
      }
    }
  }
}());
