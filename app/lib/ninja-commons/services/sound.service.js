((function SoundService() {
  angular
        .module('nvCommons.services')
        .factory('nvSoundService', nvSoundService);

  function nvSoundService() {
    const soundFile = {
      en: {
        0: 'resources/sounds/en-0.mp3',
        1: 'resources/sounds/en-1.mp3',
        2: 'resources/sounds/en-2.mp3',
        3: 'resources/sounds/en-3.mp3',
        4: 'resources/sounds/en-4.mp3',
        5: 'resources/sounds/en-5.mp3',
        6: 'resources/sounds/en-6.mp3',
        7: 'resources/sounds/en-7.mp3',
        8: 'resources/sounds/en-8.mp3',
        9: 'resources/sounds/en-9.mp3',
        a: 'resources/sounds/en-A.mp3',
        b: 'resources/sounds/en-B.mp3',
        c: 'resources/sounds/en-C.mp3',
        d: 'resources/sounds/en-D.mp3',
        e: 'resources/sounds/en-E.mp3',
        f: 'resources/sounds/en-F.mp3',
        g: 'resources/sounds/en-G.mp3',
        h: 'resources/sounds/en-H.mp3',
        i: 'resources/sounds/en-I.mp3',
        j: 'resources/sounds/en-J.mp3',
        k: 'resources/sounds/en-K.mp3',
        l: 'resources/sounds/en-L.mp3',
        m: 'resources/sounds/en-M.mp3',
        n: 'resources/sounds/en-N.mp3',
        o: 'resources/sounds/en-O.mp3',
        p: 'resources/sounds/en-P.mp3',
        q: 'resources/sounds/en-Q.mp3',
        r: 'resources/sounds/en-R.mp3',
        s: 'resources/sounds/en-S.mp3',
        t: 'resources/sounds/en-T.mp3',
        u: 'resources/sounds/en-U.mp3',
        v: 'resources/sounds/en-V.mp3',
        w: 'resources/sounds/en-W.mp3',
        x: 'resources/sounds/en-X.mp3',
        y: 'resources/sounds/en-Y.mp3',
        z: 'resources/sounds/en-Z.mp3',
        '?': 'resources/sounds/unknown_zone.mp3',
      },
      id: {
        1: 'resources/sounds/id-1.mp3',
        0: 'resources/sounds/id-0.mp3',
        2: 'resources/sounds/id-2.mp3',
        3: 'resources/sounds/id-3.mp3',
        4: 'resources/sounds/id-4.mp3',
        5: 'resources/sounds/id-5.mp3',
        6: 'resources/sounds/id-6.mp3',
        7: 'resources/sounds/id-7.mp3',
        8: 'resources/sounds/id-8.mp3',
        9: 'resources/sounds/id-9.mp3',
        a: 'resources/sounds/id-A.mp3',
        b: 'resources/sounds/id-B.mp3',
        c: 'resources/sounds/id-C.mp3',
        d: 'resources/sounds/id-D.mp3',
        e: 'resources/sounds/id-E.mp3',
        f: 'resources/sounds/id-F.mp3',
        g: 'resources/sounds/id-G.mp3',
        h: 'resources/sounds/id-H.mp3',
        i: 'resources/sounds/id-I.mp3',
        j: 'resources/sounds/id-J.mp3',
        k: 'resources/sounds/id-K.mp3',
        l: 'resources/sounds/id-L.mp3',
        m: 'resources/sounds/id-M.mp3',
        n: 'resources/sounds/id-N.mp3',
        o: 'resources/sounds/id-O.mp3',
        p: 'resources/sounds/id-P.mp3',
        q: 'resources/sounds/id-Q.mp3',
        r: 'resources/sounds/id-R.mp3',
        s: 'resources/sounds/id-S.mp3',
        t: 'resources/sounds/id-T.mp3',
        u: 'resources/sounds/id-U.mp3',
        v: 'resources/sounds/id-V.mp3',
        w: 'resources/sounds/id-W.mp3',
        x: 'resources/sounds/id-X.mp3',
        y: 'resources/sounds/id-Y.mp3',
        z: 'resources/sounds/id-Z.mp3',
        '?': 'resources/sounds/unknown_zone.mp3',
      },
      error: 'resources/sounds/error.wav',
      success: 'resources/sounds/success.wav',
      duplicate: 'resources/sounds/duplicate.wav',
      wronghub: 'resources/sounds/wrong.mp3',
      mistake: 'resources/sounds/mistake.mp3',
      international: 'resources/sounds/international.mp3',
      setaside: 'resources/sounds/setaside.mp3',
      tone: 'resources/sounds/tone.mp3',
      fryingpan: 'resources/sounds/fryingpan.mp3',
      recovery: 'resources/sounds/recovery.mp3',
      ready: 'resources/sounds/ready.mp3',
      notFound: 'resources/sounds/-.mp3',
      siren: 'resources/sounds/siren-noise.mp3',
    };


    return {
      getRackSoundFile: function getRackSoundFile(char, lang = 'en') {
        if (!_.isUndefined(soundFile[lang])) {
          return soundFile[lang][char.toLowerCase()] || '';
        }
        return '';
      },
      getRackSoundFileWord: function getRackSoundFileWord(word = '', lang = 'en') {
        const soundFiles = _.split(word, '').map(c => (soundFile[lang][_.toLower(c)]));
        return soundFiles;
      },
      getErrorSoundFile: function getErrorSoundFile() {
        return soundFile.error;
      },
      getSuccessSoundFile: function getSuccessSoundFile() {
        return soundFile.success;
      },
      getWrongHubSoundFile: function getWrongHubSoundFile() {
        return soundFile.wronghub;
      },
      getDuplicateSoundFile: function getDuplicateSoundFile() {
        return soundFile.duplicate;
      },
      getMistakeSoundFile: function getMistakeSoundFile() {
        return soundFile.mistake;
      },
      getSetAsideSoundFile: function getSetAsideSoundFile() {
        return soundFile.setaside;
      },
      getInternationalSoundFile: function getInternationalSoundFile() {
        return soundFile.international;
      },
      getByName: function getByname(name) {
        return soundFile[name];
      },
      getRecoverySoundFile: function getRecoverySoundFile() {
        return soundFile.recovery;
      },
      getReadySoundFile: () => (soundFile.ready),
      getNotFoundSoundFile: () => (soundFile.notFound),
      getExceedWeightLimitSoundFile: () => soundFile.siren,

    };
  }
})());
