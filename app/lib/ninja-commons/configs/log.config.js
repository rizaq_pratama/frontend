;(function() {
    'use strict';

    /**
     * Intercepts the $log service so that we can enhance it with cool features.
     * Requires the nvToast service.
     */
    angular
        .module('nvCommons.configs')
        .config(logConfig);

    logConfig.$inject = ['$provide'];

    function logConfig($provide) {
        $provide.decorator('$log', extendLog);
    }

    extendLog.$inject = ['$delegate', 'nvToast'];

    function extendLog($delegate, nvToast) {
        $delegate.log = extend($delegate.log, nvToast.success);
        $delegate.info = extend($delegate.info, nvToast.info);
        $delegate.warn = extend($delegate.warn, nvToast.warning);
        $delegate.error = extend($delegate.error, nvToast.error);
        $delegate.debug = extend($delegate.debug);

        return $delegate;

        function extend(orig, toast) {
            return function() {
                var args = [].slice.call(arguments),
                    now = moment().format('\\[HH:mm:ss:SSS\\]');

                // prepend the timestamp
                args.unshift(now);

                // update the original method with the new args
                orig.apply(null, args);

                // show a toast while we are at it
                if (toast) { 
                    if (arguments[0] instanceof Error) {
                        toast(arguments[0].message, (arguments[0].stack) ? arguments[0].stack.split('\n') : []); 
                    } else {
                        toast(arguments[0], [].slice.call(arguments, 1));
                    }
                }
            };
        }
    }

})();
