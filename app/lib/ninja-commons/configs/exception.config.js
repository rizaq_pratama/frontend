;(function() {
    'use strict';

    /**
     * Intercepts the $exceptionHandler service so that we can
     * provide a consistent way to handle uncaught exceptions.
     */
    angular
        .module('nvCommons.configs')
        .config(exceptionConfig);

    exceptionConfig.$inject = ['$provide'];

    function exceptionConfig($provide) {
        $provide.decorator('$exceptionHandler', extendExceptionHandler);
    }

    extendExceptionHandler.$inject = ['$delegate'];

    function extendExceptionHandler($delegate) {
        return function(exception, cause) {
            $delegate(exception, cause);
            /**
             * Could add the error to a service's collection,
             * add errors to $rootScope, log errors to remote web server,
             * or log locally. Or throw hard.
             *
             * @param exception.message - The exception message.
             * @param exception.name - The name of the exception.
             * @param cause - The DOM element causing the exception.
             *
             * todo - add support to log exceptions remotely on production builds
             */
        };
    }

})();
