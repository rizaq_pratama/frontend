(function config() {
  /**
   * Make default angular currency filter default to use nvCurrency as symbol
   */
  angular
    .module('nvCommons.configs')
    .config(nvCurrencyConfig);

  nvCurrencyConfig.$inject = ['$provide'];

  function nvCurrencyConfig($provide) {
    $provide.decorator('currencyFilter', extendCurrency);
  }

  extendCurrency.$inject = ['$delegate', 'nvCurrency', '$rootScope'];

  function extendCurrency($delegate, nvCurrency, $rootScope) {
    return (amount, currencySymbol = nvCurrency.getSymbol($rootScope.countryId), fractionSize = _.noop()) =>
      ($delegate(amount, currencySymbol, fractionSize));
  }
}());
