(function module() {
  angular
    .module('nvOperatorApp.configs')
    .config(httpInterceptorConfig);

  httpInterceptorConfig.$inject = ['$httpProvider', '$windowProvider'];

  function httpInterceptorConfig($httpProvider, $windowProvider) {
    $httpProvider.interceptors.push(requestInterceptor);

    function requestInterceptor() {
      const NINJAVAN_DOMAIN = /^.*\.ninjavan\.co\/|^.*\.ninjasaas\.com\//;

      const DOMAIN_REGEX = /#\/(\w+)\//;
      const DPMS = /^dpms/;
      const ENGINE = /^engine/;
      const CORE = /^core/;
      const CORE_KAFKA = /^core-kafka/;
      const ROUTE = /^route/;
      const DRIVER = /^driver/;
      const SCRIPT_ENGINE = /^script-engine/;
      const SHIPPER = /^shipper\//; // distinguish from 'shipper-dashboard'
      const SHIPPER_SEARCH = /^shipper-search/;
      const IMAGE = /^image/;
      const TICKETING = /^ticketing/;
      const ADDRESSING = /^addressing/;
      const HUB = /^hub/;
      const VRP = /^vrp/;
      const SHIPPER_DASHBOARD = /^shipper-dashboard/;
      const AUTH = /^auth/;
      const RESERVATION = /^reservation/;
      const LIGHTHOUSE = /^lighthouse/;
      const OVERWATCH = /^overwatch/;
      const EVENTS = /^events/;
      const CHECKOUT = /^checkout/;
      const VAULT = /^vault/;
      const URL_SHORTENER = /^url-shortener/;
      const ORDER_CREATE = /^order-create/;
      const ORDER_SEARCH = /^order-search/;
      const SHIPPERPANEL = /^shipperpanel/;
      const DASH = /^dash/;
      const DP = /^dp/;
      const IOT = /^iot/;
      const WALLET = /^wallet/;
      const DWH = /^dwh/;
      const CLAIM = /^claim/;
      const ORDER = /^order-service/;

      return {

        request: (config) => {
          if (DPMS.test(config.url)) {
            config.url = config.url.replace(DPMS, replaceDomain(nv.config.servers.dpms));
          } else if (ENGINE.test(config.url)) {
            config.url = config.url.replace(ENGINE, replaceDomain(nv.config.servers.engine));
          } else if (CORE.test(config.url)) {
            config.url = config.url.replace(CORE, replaceDomain(nv.config.servers.core));
          } else if (CORE_KAFKA.test(config.url)) {
            config.url = config.url.replace(CORE_KAFKA, replaceDomain(nv.config.servers['core-kafka']));
          } else if (ROUTE.test(config.url)) {
            config.url = config.url.replace(ROUTE, replaceDomain(nv.config.servers.route));
          } else if (DRIVER.test(config.url)) {
            config.url = config.url.replace(DRIVER, replaceDomain(nv.config.servers.driver));
          } else if (SCRIPT_ENGINE.test(config.url)) {
            config.url = config.url.replace(SCRIPT_ENGINE, replaceDomain(nv.config.servers['script-engine']));
          } else if (SHIPPER.test(config.url)) {
            config.url = config.url.replace(SHIPPER, replaceDomain(nv.config.servers.shipper) + '/');
          } else if (SHIPPER_SEARCH.test(config.url)) {
            config.url = config.url.replace(SHIPPER_SEARCH, replaceDomain(nv.config.servers['shipper-search']));
          } else if (IMAGE.test(config.url)) {
            config.url = config.url.replace(IMAGE, replaceDomain(nv.config.servers.image));
          } else if (TICKETING.test(config.url)) {
            config.url = config.url.replace(TICKETING, replaceDomain(nv.config.servers.ticketing));
          } else if (ADDRESSING.test(config.url)) {
            config.url = config.url.replace(ADDRESSING, replaceDomain(nv.config.servers.addressing));
          } else if (HUB.test(config.url)) {
            config.url = config.url.replace(HUB, replaceDomain(nv.config.servers.hub));
          } else if (VRP.test(config.url)) {
            config.url = config.url.replace(VRP, replaceDomain(nv.config.servers.vrp));
          } else if (SHIPPER_DASHBOARD.test(config.url)) {
            config.url = config.url.replace(SHIPPER_DASHBOARD, replaceDomain(nv.config.servers['shipper-dashboard']));
          } else if (AUTH.test(config.url)) {
            config.url = config.url.replace(AUTH, replaceDomain(nv.config.servers.auth));
          } else if (RESERVATION.test(config.url)) {
            config.url = config.url.replace(RESERVATION, replaceDomain(nv.config.servers.reservation));
          } else if (LIGHTHOUSE.test(config.url)) {
            config.url = config.url.replace(LIGHTHOUSE, replaceDomain(nv.config.servers.lighthouse));
          } else if (OVERWATCH.test(config.url)) {
            config.url = config.url.replace(OVERWATCH, replaceDomain(nv.config.servers.overwatch));
          } else if (EVENTS.test(config.url)) {
            config.url = config.url.replace(EVENTS, replaceDomain(nv.config.servers.events));
          } else if (CHECKOUT.test(config.url)) {
            config.url = config.url.replace(CHECKOUT, replaceDomain(nv.config.servers.checkout));
          } else if (VAULT.test(config.url)) {
            config.url = config.url.replace(VAULT, replaceDomain(nv.config.servers.vault));
          } else if (URL_SHORTENER.test(config.url)) {
            config.url = config.url.replace(URL_SHORTENER, replaceDomain(nv.config.servers['url-shortener']));
          } else if (ORDER_CREATE.test(config.url)) {
            config.url = config.url.replace(ORDER_CREATE, replaceDomain(nv.config.servers['order-create']));
          } else if (ORDER_SEARCH.test(config.url)) {
            config.url = config.url.replace(ORDER_SEARCH, replaceDomain(nv.config.servers['order-search']));
          } else if (SHIPPERPANEL.test(config.url)) {
            config.url = config.url.replace(SHIPPERPANEL, replaceDomain(nv.config.servers.shipperpanel));
          } else if (DASH.test(config.url)) {
            config.url = config.url.replace(DASH, replaceDomain(nv.config.servers.dash));
          } else if (DP.test(config.url)) {
            config.url = config.url.replace(DP, replaceDomain(nv.config.servers.dp));
          } else if (IOT.test(config.url)) {
            config.url = config.url.replace(IOT, replaceDomain(nv.config.servers.iot));
          } else if (WALLET.test(config.url)) {
            config.url = config.url.replace(WALLET, replaceDomain(nv.config.servers.wallet));
          } else if (DWH.test(config.url)) {
            config.url = config.url.replace(DWH, replaceDomain(nv.config.servers.dwh));
          } else if (CLAIM.test(config.url)) {
            config.url = config.url.replace(CLAIM, replaceDomain(nv.config.servers.claim));
          } else if (ORDER.test(config.url)) {
            config.url = config.url.replace(ORDER, replaceDomain(nv.config.servers['order-service']));
          }

          // remove Authorization header for external request
          if (!NINJAVAN_DOMAIN.test(config.url)) {
            config.headers.Authorization = undefined;
          }
          // sanitize url for non local request
          if (_.startsWith(config.url, 'http')) {
            config.url = sanitizeUrl(config.url);
            const ga = {
              category: 'api',
              action: config.method,
              label: config.url,
              timing: {
                start: moment().valueOf(),
              },
            };
            config.ga = ga;
          }
          // sanitize body
          config.data = deepTrimPayload(config.data);
          config.param = trimParamFromConfig(config.param); // trimming param in config.param
          return config;
        },

        response: (response) => {
          const ga = _.get(response, 'config.ga');
          if (ga) {
            const start = ga.timing.start;
            const end = moment().valueOf();
            const duration = end - start;
            _.assign(ga.trimAndEncodeQueryString, { end: end, duration: duration });
            // DISABLED as in NV-6740
            // window.ga('send', 'event', ga.category, ga.action, ga.label);
            // window.ga('send', 'timing', ga.category, ga.action, ga.timing.duration, ga.label);
          }
          return response;
        },
      };

      function replaceDomain(url) {
        return url.replace('$country', $windowProvider.$get().location.hash.match(DOMAIN_REGEX)[1]);
      }

      function sanitizeUrl(urlDirty) {
        // handle url param
        if (urlDirty.indexOf('?') > -1) {
          let base = urlDirty.substring(0, urlDirty.indexOf('?')); // take the url part before query param
          let after = urlDirty.substring(urlDirty.indexOf('?'));
          after = trimAndEncodeQueryString(after); // trimming param in url
          base = base.replace(/ |\r?\n|\t/g, '');
          return `${base}${after}`;
        }
        return urlDirty.replace(/ |\r?\n|\t/g, '');
      }

      function trimParamFromConfig(params) {
        if (_.isObject(params) || _.isArray(params)) {
          return Object.keys(params).reduce((acc, key) => {
            acc[key.trim()] = _.isString(params[key]) ? encodeURIComponent(params[key].trim()) : encodeURIComponent(params[key]);
            return acc;
          }, _.isArray(params) ? [] : {});
        }
        return null;
      }

      function trimAndEncodeQueryString(queryString) {
        if (queryString.length > 1) {
          const params = _.split(queryString.substring(1), '&');
          const paramString = _.map(params, (p) => {
            const kv = _.split(p, '=');
            return `${kv[0]}=${encodeURIComponent(_.trim(kv[1]))}`;
          }).join('&');
          return `?${paramString}`;
        }
        return queryString;
      }

      function deepTrimPayload(data) {
        // for FormData return as is
        // FormData object is coming from http.upload run 
        if (data && data instanceof FormData) {
          return data;
        }

        // only trim for request that has data/payload
        if (data) {
          return deepTrim(data);
        }
        return data;
        function deepTrim(body) {
          if (!_.isArray(body) && !_.isObject(body)) {
            if (_.isString(body)) {
              return body.trim();
            }
            return body;
          }
          return Object.keys(body).reduce((acc, key) => {
            acc[key.trim()] = _.isString(body[key]) ? body[key].trim() : deepTrim(body[key]);
            return acc;
          }, _.isArray(body) ? [] : {});
        }
      }
    }
  }
}());
