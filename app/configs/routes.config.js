(function config() {
  angular
    .module('nvOperatorApp.configs')
    .config(routesConfig);

  routesConfig.$inject = ['$stateProvider', '$urlRouterProvider'];

  function routesConfig($stateProvider, $urlRouterProvider) {
    $urlRouterProvider.otherwise('/login');

    $stateProvider
      .state('login', {
        url: '/login',
        templateUrl: 'views/login/login.html',
        controller: 'LoginController',
        controllerAs: 'ctrl',
      })
      .state('container', {
        url: '/:domain',
        abstract: true,
        templateUrl: 'views/container/container.html',
        controller: 'ContainerController',
        controllerAs: 'ctrl',
      })
      .state('container.root', {
        url: '',
        data: {
          redirect: 'container.order.list',
        },
      })
      .state('container.landing-page', {
        url: '/',
        data: {
          redirect: 'container.order.list',
        },
      })
      .state('container.style-guide', {
        url: '/style-guide',
        abstract: true,
        templateUrl: 'views/container/style-guide/style-guide.html',
        controller: '',
        controllerAs: 'ctrl',
      })
      .state('container.style-guide.button', {
        url: '/button',
        templateUrl: 'views/container/style-guide/button/button.html',
        controller: 'ButtonDemoController',
        controllerAs: 'ctrl',
        nvHeader: 'container.style-guide.button-demo',
      })
      .state('container.style-guide.search-input', {
        url: '/search-input',
        templateUrl: 'views/container/style-guide/search-input/search-input.html',
        controller: 'SearchInputDemoController',
        controllerAs: 'ctrl',
        nvHeader: 'container.style-guide.search-input-demo',
      })
      .state('container.style-guide.table', {
        url: '/table',
        templateUrl: 'views/container/style-guide/table/table.html',
        controller: 'TableDemoController',
        controllerAs: 'ctrl',
        nvHeader: 'container.style-guide.table-demo',
      })
      .state('container.style-guide.table-lazy', {
        url: '/table-lazy',
        templateUrl: 'views/container/style-guide/table-lazy/table-lazy.html',
        controller: 'TableLazyDemoController',
        controllerAs: 'ctrl',
        nvHeader: 'container.style-guide.table-lazy-demo',
      })
      .state('container.style-guide.filter', {
        url: '/filter',
        templateUrl: 'views/container/style-guide/filter/filter.html',
        controller: 'FilterDemoController',
        controllerAs: 'ctrl',
        nvHeader: 'container.style-guide.filter-demo',
      })
      .state('container.settings', {
        url: '/settings',
        templateUrl: 'views/container/settings/settings.html',
        controller: 'SettingsController',
        controllerAs: 'ctrl',
        nvHeader: 'container.profile-nav.settings',
      })
      .state('container.zonal-routing', {
        url: '/zonal-routing',
        abstract: true,
        templateUrl: 'views/container/zonal-routing/zonal-routing.html',
        controller: 'ZonalRoutingController',
        controllerAs: 'ctrl',
        nvHeader: 'container.sidenav.routing.zonal-routing',
      })
      .state('container.zonal-routing.assignment', {
        url: '?route_group_ids',
        templateUrl: 'views/container/zonal-routing/assignment/assignment.html',
        controller: 'ZonalRoutingAssignmentController',
        controllerAs: 'ctrl',
        nvHeader: 'container.sidenav.routing.zonal-routing',
      })
      .state('container.zonal-routing.routing', {
        url: '/routing?toCluster',
        templateUrl: 'views/container/zonal-routing/routing/routing.html',
        controller: 'ZonalRoutingRoutingController',
        controllerAs: 'ctrl',
        nvHeader: 'container.sidenav.routing.zonal-routing',
      })
      .state('container.zonal-routing-improved', {
        url: '/zonal-routing-improved',
        abstract: true,
        templateUrl: 'views/container/zonal-routing/zonal-routing.html',
        controller: 'ZonalRoutingController',
        controllerAs: 'ctrl',
        nvHeader: 'container.sidenav.routing.zonal-routing-improved',
      })
      .state('container.zonal-routing-improved.assignment', {
        url: '?route_group_ids',
        templateUrl: 'views/container/zonal-routing/assignment-improved/assignment.html',
        controller: 'ZonalRoutingAssignmentImprovedController',
        controllerAs: 'ctrl',
        nvHeader: 'container.sidenav.routing.zonal-routing-improved',
      })
      .state('container.zonal-routing-improved.routing', {
        url: '/routing?toCluster',
        templateUrl: 'views/container/zonal-routing/routing-improved/routing.html',
        controller: 'ZonalRoutingRoutingImprovedController',
        controllerAs: 'ctrl',
        nvHeader: 'container.sidenav.routing.zonal-routing-improved',
      })
      .state('container.zonal-routing-improved.edit', {
        url: '/edit?ids&zones&unrouted&cluster',
        templateUrl: 'views/container/zonal-routing/routing-edit/routing-edit.html',
        controller: 'ZonalRoutingRoutingEditController',
        controllerAs: 'ctrl',
        nvHeader: 'container.zonal-routing.edit-route',
      })
      .state('container.zones', {
        url: '/zones',
        abstract: true,
        templateUrl: 'views/container/zones/zones.html',
        controller: 'ZonesController',
        controllerAs: 'ctrl',
        nvHeader: 'commons.zones',
      })
      .state('container.zones.all', {
        url: '?all',
        templateUrl: 'views/container/zones/table/zone-table.html',
        controller: 'ZoneTableController',
        controllerAs: 'ctrl',
        nvHeader: 'commons.zones',
        params: {
          refresh: false,
        },
      })
      .state('container.zones.polygon', {
        url: '?polygon',
        templateUrl: 'views/container/zones/zone-polygon/zone-polygon.html',
        controller: 'ZonePolygonController',
        controllerAs: 'ctrl',
        nvHeader: 'commons.zones',
      })
      .state('container.add-parcel-to-route', {
        url: '/add-parcel-to-route',
        templateUrl: 'views/container/add-parcel-to-route/add-parcel-to-route.html',
        controller: 'AddParcelToRouteController',
        controllerAs: 'ctrl',
        nvHeader: 'container.sidenav.routing.add-parcel',
      })
      .state('container.same-day-route-engine', {
        url: '/same-day-route-engine',
        templateUrl: 'views/container/same-day-route-engine/same-day-route-engine.html',
        controller: 'SameDayRouteEngineController',
        controllerAs: 'ctrl',
        nvHeader: 'container.sidenav.same-day-route-engine',
      })
      .state('container.blocked-dates', {
        url: '/blocked-dates',
        templateUrl: 'views/container/blocked-dates/blocked-dates.html',
        controller: 'BlockedDatesController',
        controllerAs: 'ctrl',
        nvHeader: 'commons.blocked-dates',
      })
      .state('container.reservations', {
        url: '/reservations',
        templateUrl: 'views/container/reservations/reservations.html',
        controller: 'ReservationsController',
        controllerAs: 'ctrl',
        nvHeader: 'container.sidenav.shipper-support.reservations',
      })
      .state('container.change-delivery-timings', {
        url: '/change-delivery-timings',
        templateUrl: 'views/container/change-delivery-timings/change-delivery-timings.html',
        controller: 'ChangeDeliveryTimeController',
        controllerAs: 'ctrl',
        nvHeader: 'commons.change-delivery-timings',
      })
      .state('container.success-billings', {
        url: '/order-billing',
        templateUrl: 'views/container/success-billings/success-billings.html',
        controller: 'SuccessBillingsController',
        controllerAs: 'ctrl',
        nvHeader: 'container.sidenav.shipper-support.order-billing',
      })
      .state('container.shipper-billing', {
        url: '/shipper-billing',
        templateUrl: 'views/container/shipper-billing/shipper-billing.html',
        controller: 'ShipperBillingController',
        controllerAs: 'ctrl',
        nvHeader: 'container.sidenav.shipper-support.shipper-billing',
      })
      .state('container.dp-administration', {
        url: '/dp-administration',
        abstract: true,
        templateUrl: 'views/container/dp-administration/dp-administration.html',
        controller: 'DPAdministrationController',
        controllerAs: 'ctrl',
      })
      .state('container.dp-administration.dp-partners', {
        url: '',
        templateUrl: 'views/container/dp-administration/dp-partners/dp-partners.html',
        controller: 'DPPartnersController',
        controllerAs: 'ctrl',
        nvHeader: 'container.sidenav.dp.dp-admin', // custom variable used in views/container/container.js
      })
      .state('container.dp-administration.dps', {
        url: '/{dpPartnerId}-{dpPartnerGlobalId}-{dpPartnerName}',
        templateUrl: 'views/container/dp-administration/dps/dps.html',
        controller: 'DPsController',
        controllerAs: 'ctrl',
        nvHeader: 'container.sidenav.dp.dp-admin',
      })
      .state('container.dp-administration.dp-users', {
        url: '/{dpPartnerId}-{dpPartnerGlobalId}-{dpPartnerName}/{dpId}-{dpGlobalId}-{dpName}',
        templateUrl: 'views/container/dp-administration/dp-users/dp-users.html',
        controller: 'DPUsersController',
        controllerAs: 'ctrl',
        nvHeader: 'container.sidenav.dp.dp-admin',
      })
      .state('container.dp-tagging', {
        url: '/dp-tagging',
        templateUrl: 'views/container/dp-tagging/dp-tagging.html',
        controller: 'DPTaggingController',
        controllerAs: 'ctrl',
        nvHeader: 'container.sidenav.dp.tagging',
      })
      .state('container.driver-strength', {
        url: '/driver-strength',
        abstract: true,
        templateUrl: 'views/container/driver-strength/driver-strength.html',
        controller: 'DriverStrengthController',
        controllerAs: 'ctrl',
        nvHeader: 'container.sidenav.fleet.driver-strength',
      })
      .state('container.driver-strength.filter', {
        url: '',
        templateUrl: 'views/container/driver-strength/filter/filter.html',
        controller: 'DriverStrengthFilterController',
        controllerAs: 'ctrl',
        nvHeader: 'container.sidenav.fleet.driver-strength',
      })
      .state('container.driver-strength.data', {
        url: '/filter?zone&type',
        templateUrl: 'views/container/driver-strength/data/data.html',
        controller: 'DriverStrengthDataController',
        controllerAs: 'ctrl',
        nvHeader: 'container.sidenav.fleet.driver-strength',
      })
      .state('container.driver-seeding', {
        url: '/driver-seeding',
        templateUrl: 'views/container/driver-seeding/driver-seeding.html',
        controller: 'DriverSeedingController',
        controllerAs: 'ctrl',
        nvHeader: 'container.sidenav.fleet.driver-seeding',
      })
      .state('container.driver-type-management', {
        url: '/driver-type-management',
        templateUrl: 'views/container/driver-type-management/driver-type-management.html',
        controller: 'DriverTypeManagementController',
        controllerAs: 'ctrl',
        nvHeader: 'container.sidenav.fleet.driver-type-management',
      })
      .state('container.driver-reports', {
        url: '/driver-reports',
        templateUrl: 'views/container/driver-reports/driver-reports.html',
        controller: 'DriverReportController',
        controllerAs: 'ctrl',
        nvHeader: 'container.sidenav.fleet.driver-report',
      })
      .state('container.cod-list', {
        url: '/cod',
        templateUrl: 'views/container/cod-list/cod-list.html',
        controller: 'CODListController',
        controllerAs: 'ctrl',
        nvHeader: 'container.sidenav.fleet.cod-list',
      })
      .state('container.contact-type-management', {
        url: '/contact-type-management',
        templateUrl: 'views/container/contact-type-management/contact-type-management.html',
        controller: 'ContactTypeManagementController',
        controllerAs: 'ctrl',
        nvHeader: 'commons.contact-type-management',
      })
      .state('container.failed-delivery-management', {
        url: '/failed-delivery-management',
        templateUrl: 'views/container/failed-delivery-management/failed-delivery-management.html',
        controller: 'FailedDeliveryManagementController',
        controllerAs: 'ctrl',
        nvHeader: 'container.sidenav.shipper-support.failed-delivery-management',
      })
      .state('container.failed-pickup-management', {
        url: '/failed-pickup-management',
        templateUrl: 'views/container/failed-pickup-management/failed-pickup-management.html',
        controller: 'FailedPickupManagementController',
        controllerAs: 'ctrl',
        nvHeader: 'container.sidenav.shipper-support.failed-pickup-management',
      })
      .state('container.aged-parcel-management', {
        url: '/aged-parcel-management',
        templateUrl: 'views/container/aged-parcel-management/aged-parcel-management.html',
        controller: 'AgedParcelManagementController',
        controllerAs: 'ctrl',
        nvHeader: 'container.sidenav.shipper-support.aged-parcel-management',
      })
      .state('container.vehicle-type-management', {
        url: '/vehicle-type-management',
        templateUrl: 'views/container/vehicle-type-management/vehicle-type-management.html',
        controller: 'VehicleTypeManagementController',
        controllerAs: 'ctrl',
        nvHeader: 'commons.vehicle-type-management',
      })
      .state('container.rf-prebidding-management', {
        url: '/rf-prebidding-management',
        templateUrl: 'views/container/rf-prebidding-management/rf-prebidding-management.html',
        controller: 'RFPreBiddingManagementController',
        controllerAs: 'ctrl',
        nvHeader: 'commons.rf-prebidding-management',
      })
      .state('container.shippers', {
        url: '/shippers',
        abstract: true,
        templateUrl: 'views/container/shippers/shippers.html',
        controller: 'ShippersController',
        controllerAs: 'ctrl',
        nvHeader: 'container.sidenav.shipper.all-shippers',
      })
      .state('container.shippers.list', {
        url: '',
        templateUrl: 'views/container/shippers/list/list.html',
        controller: 'ShippersListController',
        controllerAs: 'ctrl',
        nvHeader: 'container.sidenav.shipper.all-shippers',
        params: {
          filter: null,
        },
      })
      .state('container.shippers.edit', {
        url: '/{shipperId:int}',
        templateUrl: 'views/container/shippers/shipper-edit/shipper-edit.html',
        controller: 'ShipperEditController',
        controllerAs: 'ctrl',
        nvHeader: 'container.sidenav.shipper.edit',
        params: {
          filter: null,
        },
      })
      .state('container.shippers.create', {
        url: '/create',
        templateUrl: 'views/container/shippers/shipper-create/shipper-create.html',
        controller: 'ShipperCreateController',
        controllerAs: 'ctrl',
        nvHeader: 'container.sidenav.shipper.create',
        params: {
          filter: null,
        },
      })
      .state('container.v2-pricing-scripts-active', {
        url: '/pricing-scripts-v2/active-scripts',
        templateUrl: 'views/container/pricing-scripts-v2/list/pricing-script-list.html',
        controller: 'PricingV2Controller',
        controllerAs: 'ctrl',
        nvHeader: 'commons.pricing-scripts',
        params: {
          status: 'Active',
        },
      })
      .state('container.v2-pricing-scripts-drafts', {
        url: '/pricing-scripts-v2/drafts',
        templateUrl: 'views/container/pricing-scripts-v2/list/pricing-script-list.html',
        controller: 'PricingV2Controller',
        controllerAs: 'ctrl',
        nvHeader: 'commons.pricing-scripts',
        params: {
          status: 'Draft',
        },
      })
      .state('container.v2-pricing-scripts-create', {
        url: '/pricing-scripts-v2/create?type',
        templateUrl: 'views/container/pricing-scripts-v2/manage/pricing-script-manage.html',
        controller: 'PricingScriptManageController',
        controllerAs: 'ctrl',
        nvHeader: 'commons.pricing-scripts',
      })
      .state('container.v2-pricing-scripts-create-time-bounded', {
        url: '/pricing-scripts-v2/{parentId:int}/create?type',
        templateUrl: 'views/container/pricing-scripts-v2/manage/pricing-script-manage.html',
        controller: 'PricingScriptManageController',
        controllerAs: 'ctrl',
        nvHeader: 'commons.pricing-scripts',
      })
      .state('container.v2-pricing-scripts-list-time-bounded', {
        url: '/pricing-scripts-v2/{parentId:int}/time-bounded',
        templateUrl: 'views/container/pricing-scripts-v2/time-bounded/pricing-script-time-bounded.html',
        controller: 'PricingScriptTimeBoundedController',
        controllerAs: 'ctrl',
        nvHeader: 'commons.pricing-scripts',
      })
      .state('container.v2-pricing-scripts-edit', {
        url: '/pricing-scripts-v2/{scriptId:int}?type',
        templateUrl: 'views/container/pricing-scripts-v2/manage/pricing-script-manage.html',
        controller: 'PricingScriptManageController',
        controllerAs: 'ctrl',
        nvHeader: 'commons.pricing-scripts',
      })
      .state('container.v2-pricing-scripts-verify', {
        url: '/pricing-scripts-v2/verify',
        templateUrl: 'views/container/pricing-scripts-v2/verify/pricing-script-verify.html',
        controller: 'PricingScriptVerifyController',
        controllerAs: 'ctrl',
        nvHeader: 'commons.pricing-scripts',
        params: {
          activeParameters: null,
          script: null,
        },
      })
      .state('container.sales-person', {
        url: '/sales-person',
        templateUrl: 'views/container/sales-person/sales-person.html',
        controller: 'SalesPersonController',
        controllerAs: 'ctrl',
        nvHeader: 'container.sidenav.shipper.sales',
      })
      .state('container.hub-list', {
        url: '/hub',
        templateUrl: 'views/container/hub-list/hub-list.html',
        controller: 'HubListController',
        controllerAs: 'ctrl',
        nvHeader: 'container.sidenav.hubs.admin',
      })
      .state('container.hub-group-management', {
        url: '/hub-group',
        templateUrl: 'views/container/hub-group-management/hub-group-management.html',
        controller: 'HubGroupManagementController',
        controllerAs: 'ctrl',
        nvHeader: 'container.sidenav.hubs-group-management',
      })
      .state('container.printers', {
        url: '/printers',
        templateUrl: 'views/container/printers/printers.html',
        controller: 'PrintersController',
        controllerAs: 'ctrl',
        nvHeader: 'container.sidenav.system-settings.printer',
      })
      .state('container.relabel-stamp', {
        url: '/relabel-stamp',
        templateUrl: 'views/container/relabel-stamp/relabel-stamp.html',
        controller: 'RelabelStampController',
        controllerAs: 'ctrl',
        nvHeader: 'container.sidenav.thermal-printing.relabel-stamp',
      })
      .state('container.label-printer', {
        url: '/label-printer',
        templateUrl: 'views/container/label-printer/label-printer.html',
        controller: 'LabelPrinterController',
        controllerAs: 'ctrl',
        nvHeader: 'container.sidenav.label-printer',
      })
      .state('container.printer-templates', {
        url: '/printer-templates',
        templateUrl: 'views/container/printer-templates/printer-templates.html',
        controller: 'PrinterTemplatesController',
        controllerAs: 'ctrl',
        nvHeader: 'container.sidenav.thermal-printing.printer-templates',
      })
      .state('container.route-group', {
        url: '/route-group',
        templateUrl: 'views/container/route-group/route-group.html',
        controller: 'RouteGroupController',
        controllerAs: 'ctrl',
        nvHeader: 'container.sidenav.routing.route-groups',
      })
      .state('container.route-logs', {
        url: '/route-logs',
        templateUrl: 'views/container/route-logs/route-logs.html',
        controller: 'RouteLogsController',
        controllerAs: 'ctrl',
        nvHeader: 'container.sidenav.routing.route-logs',
      })
      .state('container.route-manifest', {
        url: '/route-manifest/:routeId',
        templateUrl: 'views/container/route-manifest/route-manifest.html',
        controller: 'RouteManifestController',
        controllerAs: 'ctrl',
        nvHeader: 'commons.route-manifest',
      })
      .state('container.route-monitoring-paged', {
        url: '/route-monitoring-paged',
        templateUrl: 'views/container/route-monitoring-paged/route-monitoring-paged.html',
        controller: 'RouteMonitoringPagedController',
        controllerAs: 'ctrl',
        nvHeader: 'container.sidenav.routing.route-monitoring',
      })
      .state('container.route-monitoring', {
        url: '/route-monitoring',
        abstract: true,
        templateUrl: 'views/container/route-monitoring/route-monitoring.html',
        controller: 'RouteMonitoringController',
        controllerAs: 'ctrl',
        nvHeader: 'container.sidenav.routing.route-monitoring',
      })
      .state('container.route-monitoring.filter', {
        url: '',
        templateUrl: 'views/container/route-monitoring/filter/filter.html',
        controller: 'RouteMonitoringFilterController',
        controllerAs: 'ctrl',
        nvHeader: 'container.sidenav.routing.route-monitoring',
      })
      .state('container.route-monitoring.result', {
        url: '/result?date&hub_ids&with_tag_ids&zone_ids',
        templateUrl: 'views/container/route-monitoring/result/result.html',
        controller: 'RouteMonitoringResultController',
        controllerAs: 'ctrl',
        nvHeader: 'container.sidenav.routing.route-monitoring',
      })
      .state('container.transactions-v2', {
        url: '/transactions/v2',
        templateUrl: 'views/container/transactions-v2/transactions-v2.html',
        controller: 'RoutableTransactionsControllerV2',
        controllerAs: 'ctrl',
        nvHeader: 'container.sidenav.routing.transactions-v2',
      })
      .state('container.recovery-tickets', {
        url: '/recovery-tickets',
        templateUrl: 'views/container/recovery-tickets/recovery-tickets.html',
        controller: 'RecoveryTicketsController',
        controllerAs: 'ctrl',
        nvHeader: 'commons.recovery-tickets',
      })
      .state('container.recovery-ticket-scanning', {
        url: '/recovery-ticket-scanning',
        templateUrl: 'views/container/recovery-ticket-scanning/recovery-ticket-scanning.html',
        controller: 'RecoveryTicketScanningController',
        controllerAs: 'ctrl',
        nvHeader: 'container.sidenav.recovery.tickets-scanning',
      })
      .state('container.route-cleaning-report', {
        url: '/route-cleaning-report',
        templateUrl: 'views/container/route-cleaning-report/route-cleaning-report.html',
        controller: 'RouteCleaningReportController',
        controllerAs: 'ctrl',
        nvHeader: 'container.sidenav.recovery.route-cleaning-report',
      })
      .state('container.cod-report', {
        url: '/cod-report',
        templateUrl: 'views/container/cod-report/cod-report.html',
        controller: 'CODReportController',
        controllerAs: 'ctrl',
        nvHeader: 'container.sidenav.analytics.cod-report',
      })
      .state('container.order', {
        url: '/order',
        templateUrl: 'views/container/order/order.html',
        abstract: true,
        controller: 'OrderController',
        controllerAs: 'ctrl',
      })
      .state('container.order.list', {
        url: '',
        templateUrl: 'views/container/order/order-list/order-list.html',
        controller: 'OrderListController',
        controllerAs: 'ctrl',
        nvHeader: 'container.sidenav.order.all-orders',
      })
      .state('container.order.create-combine', {
        url: '/create-combine',
        templateUrl: 'views/container/order/order-create-combine/order-create-combine.html',
        controller: 'OrderCreateCombineController',
        controllerAs: 'ctrl',
        nvHeader: 'container.sidenav.order.create-combine',
      })
      .state('container.order.create-v4', {
        url: '/create-v4',
        templateUrl: 'views/container/order/order-create-v4/order-create-v4.html',
        controller: 'OrderCreateV4Controller',
        controllerAs: 'ctrl',
        nvHeader: 'container.sidenav.order.create-v4',
      })
      .state('container.order.edit', {
        url: '/{orderId:int}',
        templateUrl: 'views/container/order/order-edit/order-edit.html',
        controller: 'OrderEditController',
        controllerAs: 'ctrl',
        nvHeader: 'container.sidenav.order.edit',
      })
      .state('container.shipment-scanning', {
        url: '/shipment-scanning',
        templateUrl: 'views/container/shipment-scanning/shipment-scanning.html',
        controller: 'ShipmentScanningController',
        controllerAs: 'ctrl',
        nvHeader: 'container.sidenav.interhub.shipment-scanning',
      })
      .state('container.shipment-management', {
        url: '/shipment-management',
        templateUrl: 'views/container/shipment-management/shipment-management.html',
        controller: 'ShipmentManagementController',
        controllerAs: 'ctrl',
        nvHeader: 'container.sidenav.interhub.shipper-management',
      })
      .state('container.shipment-details', {
        url: '/shipment-details/{shipmentId:int}',
        templateUrl: 'views/container/shipment-management/page/shipment-details/shipment-details.html',
        controller: 'ShipmentDetailsController',
        controllerAs: 'ctrl',
        nvHeader: 'container.sidenav.interhub.shipment-details',
      })
      .state('container.shipment-global-inbound', {
        url: '/shipment-global-inbound',
        templateUrl: 'views/container/shipment-global-inbound/shipment-global-inbound.html',
        controller: 'ShipmentGlobalInboundController',
        controllerAs: 'ctrl',
        nvHeader: 'container.sidenav.interhub.shipment-global-inbound',
      })
      .state('container.tag-management', {
        url: '/tag-management',
        templateUrl: 'views/container/tag-management/tag-management.html',
        controller: 'TagManagementController',
        controllerAs: 'ctrl',
        nvHeader: 'container.sidenav.tag-management',
      })
      .state('container.third-party-shipper', {
        url: '/third-party-shipper?id&name',
        templateUrl: 'views/container/third-party-shipper/third-party-shipper.html',
        controller: 'ThirdPartyShipperController',
        controllerAs: 'ctrl',
        nvHeader: 'container.sidenav.third-party.third-party-shipper',
      })
      .state('container.third-party-order', {
        url: '/third-party-order?thirdPartyOrder-thirdPartyShipper-name',
        templateUrl: 'views/container/third-party-order/third-party-order.html',
        controller: 'ThirdPartyOrderController',
        controllerAs: 'ctrl',
        nvHeader: 'container.sidenav.third-party.third-party-order',
      })
      .state('container.shipper-pickups', {
        url: '/shipper-pickups',
        templateUrl: 'views/container/shipper-pickups/shipper-pickups.html',
        controller: 'ShipperPickupsController',
        controllerAs: 'ctrl',
        nvHeader: 'container.sidenav.pick-ups.shipper-pickups',
      })
      .state('container.polygon-drawing', {
        url: '/polygon-drawing',
        templateUrl: 'views/container/polygon-drawing/polygon-drawing.html',
        controller: 'PolygonDrawingController',
        controllerAs: 'ctrl',
        nvHeader: 'container.sidenav.utilities.polygon-drawing',
      })
      .state('container.lat-lng-cleanup', {
        url: '/lat-lng-cleanup',
        templateUrl: 'views/container/lat-lng-cleanup/lat-lng-cleanup.html',
        controller: 'LatLngCleanupController',
        controllerAs: 'ctrl',
        nvHeader: 'container.sidenav.utilities.lat-lng-cleanup',
      })
      .state('container.ninja-hub-provisioning', {
        url: '/ninja-hub-provisioning',
        templateUrl: 'views/container/ninja-hub-provisioning/ninja-hub-provisioning.html',
        controller: 'NinjaHubProvisioningController',
        controllerAs: 'ctrl',
        nvHeader: 'container.sidenav.utilities.ninja-hub-provisioning',
      })
      .state('container.rollover', {
        url: '/rollover',
        templateUrl: 'views/container/rollover/rollover.html',
        controller: 'RolloverController',
        controllerAs: 'ctrl',
        nvHeader: 'container.sidenav.utilities.rollover',
      })
      .state('container.shipment-inbound-scanning', {
        url: '/shipment-inbound-scanning',
        templateUrl: 'views/container/inbound-scanning/inbound-scanning.html',
        controller: 'InboundScanningController',
        controllerAs: 'ctrl',
        nvHeader: 'container.sidenav.interhub.shipment-inbound-scanning',
      })
      .state('container.van-inbound', {
        url: '/van-inbound',
        templateUrl: 'views/container/van-inbound/van-inbound.html',
        controller: 'VanInboundController',
        controllerAs: 'ctrl',
        nvHeader: 'container.sidenav.van-inbound',
      })
      .state('container.global-inbound', {
        url: '/global-inbound',
        templateUrl: 'views/container/global-inbound/global-inbound.html',
        controller: 'GlobalInboundController',
        controllerAs: 'ctrl',
        nvHeader: 'container.sidenav.global-inbound',
      })
      .state('container.shipment-inbound', {
        url: '/shipment-inbound',
        templateUrl: 'views/container/shipment-inbound/shipment-inbound.html',
        controller: 'ShipmentInboundController',
        controllerAs: 'ctrl',
        nvHeader: 'container.sidenav.shipment-inbound',
      })
      .state('container.parcel-sweeper', {
        url: '/parcel-sweeper',
        templateUrl: 'views/container/parcel-sweeper/parcel-sweeper.html',
        controller: 'ParcelSweeperController',
        controllerAs: 'ctrl',
        nvHeader: 'container.sidenav.parcel-sweeper',
      })
      .state('container.parcel-sweeper-with-hub', {
        url: '/parcel-sweeper-by-hub',
        templateUrl: 'views/container/parcel-sweeper-with-hub/parcel-sweeper-with-hub.html',
        controller: 'ParcelSweeperWithHubController',
        controllerAs: 'ctrl',
        nvHeader: 'container.sidenav.parcel-sweeper-by-hub',
      })
      .state('container.parcel-sweeper-live', {
        url: '/parcel-sweeper-live',
        templateUrl: 'views/container/parcel-sweeper-live/parcel-sweeper-live.html',
        controller: 'ParcelSweeperLiveController',
        controllerAs: 'ctrl',
        nvHeader: 'container.sidenav.parcel-sweeper-live',
      })
      .state('container.reservation-rejection', {
        url: '/reservation-rejection',
        templateUrl: 'views/container/reservation-rejection/reservation-rejection.html',
        controller: 'ReservationRejectionController',
        controllerAs: 'ctrl',
        nvHeader: 'container.sidenav.pick-ups.reservation-rejection',
      })
      .state('container.route-inbound', {
        url: '/route-inbound',
        abstract: true,
        templateUrl: 'views/container/route-inbound/route-inbound.html',
        controller: 'RouteInboundController',
        controllerAs: 'ctrl',
        nvHeader: 'container.sidenav.route-inbound',
      })
      .state('container.route-inbound.filter', {
        url: '',
        templateUrl: 'views/container/route-inbound/filter/filter.html',
        controller: 'RouteInboundFilterController',
        controllerAs: 'ctrl',
        nvHeader: 'container.sidenav.route-inbound',
      })
      .state('container.route-inbound.result', {
        url: '/result',
        templateUrl: 'views/container/route-inbound/result/result.html',
        controller: 'RouteInboundResultController',
        controllerAs: 'ctrl',
        nvHeader: 'container.sidenav.route-inbound',
      })
      .state('container.route-inbound.inbounding', {
        url: '/inbounding',
        templateUrl: 'views/container/route-inbound/inbounding/inbounding.html',
        controller: 'RouteInboundInboundingController',
        controllerAs: 'ctrl',
        nvHeader: 'container.sidenav.route-inbound',
      })
      .state('container.reservation-preset-management', {
        url: '/reservation-preset-management',
        abstract: true,
        templateUrl: 'views/container/reservation-preset-management/reservation-preset-management.html',
        controller: 'ReservationPresetManagementController',
        controllerAs: 'ctrl',
        nvHeader: 'container.sidenav.pick-ups.reservation-preset-management',
      })
      .state('container.reservation-preset-management.overview', {
        url: '?refresh',
        templateUrl: 'views/container/reservation-preset-management/overview/overview.html',
        controller: 'ReservationPresetManagementOverviewController',
        controllerAs: 'ctrl',
        nvHeader: 'container.sidenav.pick-ups.reservation-preset-management',
      })
      .state('container.reservation-preset-management.pending', {
        url: '/pending?refresh',
        templateUrl: 'views/container/reservation-preset-management/pending/pending.html',
        controller: 'ReservationPresetManagementPendingController',
        controllerAs: 'ctrl',
        nvHeader: 'container.sidenav.pick-ups.reservation-preset-management',
      })
      .state('container.reservation-preset-management.edit-groupings', {
        url: '/edit-groupings?ids',
        templateUrl: 'views/container/reservation-preset-management/edit-groupings/edit-groupings.html',
        controller: 'ReservationPresetManagementEditGroupingsController',
        controllerAs: 'ctrl',
        nvHeader: 'container.sidenav.pick-ups.reservation-preset-management',
      })
      .state('container.reservation-preset-management.edit-pickup-sequence', {
        url: '/edit-pickup-sequence?{id:int}',
        templateUrl: 'views/container/reservation-preset-management/edit-pickup-sequence/edit-pickup-sequence.html',
        controller: 'ReservationPresetManagementEditPickupSequenceController',
        controllerAs: 'ctrl',
        nvHeader: 'container.sidenav.pick-ups.reservation-preset-management',
      })
      .state('container.user-management', {
        url: '/user-management',
        templateUrl: 'views/container/user-management/user-management.html',
        controller: 'UserManagementController',
        controllerAs: 'ctrl',
        nvHeader: 'container.sidenav.access-control.user-management',
      })
      .state('container.role-management', {
        url: '/role-management',
        templateUrl: 'views/container/role-management/role-management.html',
        controller: 'RoleManagementController',
        controllerAs: 'ctrl',
        nvHeader: 'container.sidenav.access-control.role-management',
      })
      .state('container.linehaul', {
        url: '/linehaul',
        templateUrl: 'views/container/linehaul/linehaul.html',
        controller: 'LinehaulController',
        controllerAs: 'ctrl',
        nvHeader: 'Linehaul Management',
      })
      .state('container.linehaul.date', {
        url: '/date',
        views: {
          'linehaul.date': {
            templateUrl: 'views/container/linehaul/tabs/linehaul-date/linehaul-date.html',
            controller: 'LinehaulDateController',
            controllerAs: 'ctrl',
          },
        },
        nvHeader: 'Linehaul Management',

      })
      .state('container.linehaul.entries', {
        url: '/entries',
        views: {
          'linehaul.entries': {
            templateUrl: 'views/container/linehaul/tabs/linehaul-entries/linehaul-entries.html',
            controller: 'LinehaulEntriesController',
            controllerAs: 'ctrl',
          },
        },
        nvHeader: 'Linehaul Management',
      })
      .state('container.bulk-address-verification', {
        url: '/bulk-address-verification',
        templateUrl: 'views/container/bulk-address-verification/bulk-address-verification.html',
        controller: 'BulkAddressVerificationController',
        controllerAs: 'ctrl',
        nvHeader: 'container.sidenav.utilities.bulk-address-verification',
      })
      .state('container.address-verification', {
        url: '/address-verification',
        templateUrl: 'views/container/address-verification/address-verification.html',
        controller: 'AddressVerificationController',
        controllerAs: 'ctrl',
        nvHeader: 'container.sidenav.utilities.address-verification',
      })
      .state('container.claim', {
        url: '/claim',
        templateUrl: 'views/container/claim/claim.html',
        controller: 'ClaimController',
        controllerAs: 'ctrl',
        nvHeader: 'container.sidenav.utilities.claim',
      })
      .state('container.warehousing', {
        url: '/warehousing',
        templateUrl: 'views/container/warehousing/warehousing.html',
        controller: 'WarehousingController',
        controllerAs: 'ctrl',
        nvHeader: 'container.sidenav.utilities.warehousing',
      })
      .state('container.qrcode-printing', {
        url: '/qrcode-printing',
        templateUrl: 'views/container/qrcode-printing/qrcode-printing.html',
        controller: 'QrcodePrintingController',
        controllerAs: 'ctrl',
        nvHeader: 'container.sidenav.utilities.qrcode-printing',
      })
      .state('container.ninja-pack-tid-generator', {
        url: '/ninja-pack-tid-generator',
        templateUrl: 'views/container/ninja-pack-tid-generator/ninja-pack-tid-generator.html',
        controller: 'NinjaPackTidGeneratorController',
        controllerAs: 'ctrl',
        nvHeader: 'container.sidenav.utilities.ninja-pack-tid-generator',
      })
      .state('container.payment-processing', {
        url: '/payment-processing',
        templateUrl: 'views/container/payment-processing/payment-processing.html',
        controller: 'PaymentProcessingController',
        controllerAs: 'ctrl',
        nvHeader: 'container.sidenav.payment-processing',
      })
      .state('container.addressing', {
        url: '/addressing',
        templateUrl: 'views/container/addressing/addressing.html',
        controller: 'AddressingController',
        controllerAs: 'ctrl',
        nvHeader: 'container.sidenav.addressing.addressing',
      })
      .state('container.unverified-address-assignment', {
        url: '/unverified-address-assignment',
        templateUrl: 'views/container/unverified-address-assignment/unverified-address-assignment.html',
        controller: 'UnverifiedAddressAssignmentController',
        controllerAs: 'ctrl',
        nvHeader: 'container.sidenav.addressing.unverified-address-assignment',
      })
      .state('container.bulky-batch', {
        url: '/bulky-batch',
        templateUrl: 'views/container/bulky-batch/bulky-batch.html',
        controller: 'BulkyBatchController',
        controllerAs: 'ctrl',
        nvHeader: 'container.sidenav.special-pages.bulky-batch',
      })
      .state('container.lazada-3pl', {
        url: '/lazada-3pl',
        templateUrl: 'views/container/lazada-3pl/lazada-3pl.html',
        controller: 'Lazada3plController',
        controllerAs: 'ctrl',
        nvHeader: 'container.sidenav.special-pages.lazada-3pl',
      })
      .state('container.amazon-parcel-status', {
        url: '/amazon-parcel-status',
        templateUrl: 'views/container/amazon-parcel-status/amazon-parcel-status.html',
        controller: 'AmazonParcelStatusController',
        controllerAs: 'ctrl',
        nvHeader: 'container.sidenav.special-pages.amazon-parcel-status',
      })
      .state('container.order-weight-update', {
        url: '/order-weight-update',
        templateUrl: 'views/container/order-weight-update/order-weight-update.html',
        controller: 'OrderWeightUpdateController',
        controllerAs: 'ctrl',
        nvHeader: 'container.sidenav.special-pages.order-weight-update',
      })
      .state('container.singtel-parcel-status', {
        url: '/singtel-parcel-status',
        templateUrl: 'views/container/singtel-parcel-status/singtel-parcel-status.html',
        controller: 'SingtelParcelStatusController',
        controllerAs: 'ctrl',
        nvHeader: 'container.sidenav.special-pages.singtel-parcel-status',
      })
      .state('container.cmi-global-inbound', {
        url: '/global-inbound-cmi-override',
        templateUrl: 'views/container/global-inbound-cmi/global-inbound-cmi.html',
        controller: 'GlobalInboundCmiController',
        controllerAs: 'ctrl',
        nvHeader: 'container.sidenav.special-pages.global-inbound-cmi-override',
      })
      .state('container.sms', {
        url: '/sms',
        templateUrl: 'views/container/sms/sms.html',
        controller: 'SmsController',
        controllerAs: 'ctrl',
        nvHeader: 'container.sidenav.mass-communication.messaging',
      })
      .state('container.stamp-disassociation', {
        url: '/stamp-disassociation',
        templateUrl: 'views/container/stamp-disassociation/stamp-disassociation.html',
        controller: 'StampDisassociationController',
        controllerAs: 'ctrl',
        nvHeader: 'container.sidenav.new-features.stamp-disassociation',
      })
      .state('container.implanted-manifest', {
        url: '/implanted-manifest',
        templateUrl: 'views/container/implanted-manifest/implanted-manifest.html',
        controller: 'ImplantedManifestController',
        controllerAs: 'ctrl',
        nvHeader: 'container.sidenav.new-features.implanted-manifest',
      })
      .state('container.outbound-route-load', {
        url: '/outbound-route-load',
        abstract: true,
        templateUrl: 'views/container/outbound-route-load-monitoring/outbound-route-load-monitoring.html',
        controller: 'OutboundRouteLoadController',
        controllerAs: 'ctrl',
      })
      .state('container.outbound-route-load.route-load-monitoring', {
        url: '/route-load-monitoring',
        templateUrl: 'views/container/route-load-monitoring/route-load-monitoring.html',
        controller: 'RouteLoadMonitoring',
        controllerAs: 'ctrl',
        nvHeader: 'container.sidenav.new-features.outbound-route-load-monitoring',
      })
      .state('container.outbound-route-load.outbound-monitoring', {
        url: '/outbound-monitoring',
        templateUrl: 'views/container/outbound-monitoring/outbound-monitoring.html',
        controller: 'OutboundMonitoringController',
        controllerAs: 'ctrl',
        nvHeader: 'container.sidenav.new-features.outbound-route-load-monitoring',
      })
      .state('container.global-settings', {
        url: '/global-settings',
        templateUrl: 'views/container/global-settings/global-settings.html',
        controller: 'GlobalSettingsController',
        controllerAs: 'ctrl',
        nvHeader: 'container.sidenav.system-settings.global-settings',
      })
      .state('container.non-inbounded-list', {
        url: '/non-inbounded-list',
        templateUrl: 'views/container/non-inbounded-list/non-inbounded-list.html',
        controller: 'NonInboundedListController',
        controllerAs: 'ctrl',
        nvHeader: 'container.sidenav.new-features.non-inbounded-list',
      })
      .state('container.outbound-breakroute', {
        url: '/outbound-breakroute/{routeId:int}',
        templateUrl: 'views/container/outbound-breakroute/outbound-breakroute.html',
        controller: 'OutboundBreakRouteController',
        controllerAs: 'ctrl',
        nvHeader: 'container.sidenav.outbound-breakroute',
      })
      .state('container.bulk-order', {
        url: '/bulk-order?{id:int}',
        templateUrl: 'views/container/bulk-order/bulk-order.html',
        controller: 'BulkOrderController',
        controllerAs: 'ctrl',
        nvHeader: 'container.sidenav.new-features.bulk-order',
      })
      .state('container.batch-order', {
        url: '/batch-order?{id:int}',
        templateUrl: 'views/container/batch-order/batch-order.html',
        controller: 'BatchOrderController',
        controllerAs: 'ctrl',
        nvHeader: 'container.sidenav.new-features.batch-order',
      })
      .state('container.priority-levels', {
        url: '/priority-levels',
        templateUrl: 'views/container/priority-levels/priority-levels.html',
        controller: 'PriorityLevelController',
        controllerAs: 'ctrl',
        nvHeader: 'container.sidenav.new-features.priority-levels',
      })
      .state('container.set-aside', {
        url: '/set-aside',
        templateUrl: 'views/container/set-aside/set-aside.html',
        controller: 'SetAsideController',
        controllerAs: 'ctrl',
        nvHeader: 'container.sidenav.new-features.set-aside',
      })
      .state('container.reports', {
        url: '/reports',
        templateUrl: 'views/container/reports/reports.html',
        controller: 'ReportsController',
        controllerAs: 'ctrl',
        nvHeader: 'Reports',
      })
      .state('container.order-level-tag-management', {
        url: '/order-level-tag-management',
        abstract: true,
        templateUrl: 'views/container/order-level-tag-management/order-level-tag-management.html',
        controller: 'OrderLevelTagManagementController',
        controllerAs: 'ctrl',
        nvHeader: 'container.sidenav.order-level-tag-management',
      })
      .state('container.order-level-tag-management.filter', {
        url: '?use_csv&shipper_ids&master_shipper_ids&order_types&statuses&granular_statuses',
        templateUrl: 'views/container/order-level-tag-management/filter/filter.html',
        controller: 'OrderLevelTagManagementFilterController',
        controllerAs: 'ctrl',
        nvHeader: 'container.sidenav.order-level-tag-management',
      })
      .state('container.order-level-tag-management.result', {
        url: '/result?use_csv&shipper_ids&master_shipper_ids&order_types&statuses&granular_statuses',
        templateUrl: 'views/container/order-level-tag-management/result/result.html',
        controller: 'OrderLevelTagManagementResultController',
        controllerAs: 'ctrl',
        nvHeader: 'container.sidenav.order-level-tag-management',
      })
      ;
  }
}());
