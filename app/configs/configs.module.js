(function() {
    'use strict';

    angular
        .module('nvOperatorApp.configs', [
            'ui.router',
            'ngMaterial',
            'pascalprecht.translate',
            'nvCommons.services',
        ]);

})();
