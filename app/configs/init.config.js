(function() {
    'use strict';

    angular
        .module('nvOperatorApp.configs')
        .config(initConfig);

    initConfig.$inject = ['$compileProvider', '$logProvider', '$mdDateLocaleProvider'];

    function initConfig($compileProvider, $logProvider, $mdDateLocaleProvider) {
        if ('local' !== nv.config.env) {
            // disable on deployments for a significant performance boost
            $compileProvider.debugInfoEnabled(false);

            // disable debug messages on deployments
            $logProvider.debugEnabled(false);
        }

        // parse and display the given javascript Date object in local time
        $mdDateLocaleProvider.formatDate = function(date) {
            var m = moment(date);
            return m.isValid() ? m.format('YYYY-MM-DD') : null;
        };
    }

})();
