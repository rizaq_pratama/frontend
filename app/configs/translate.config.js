(function config() {
  angular
    .module('nvOperatorApp.configs')
    .config(translateConfig);

  translateConfig.$inject = ['$translateProvider'];

  function translateConfig($translateProvider) {
    // todo - use the partial loader when the app becomes more complex in the future
    // todo - see http://angular-translate.github.io/docs/#/guide/12_asynchronous-loading
    // we use a custom static file loader so that we can run filerev on the locale files
    $translateProvider.useLoader('nvTranslateLoader', {
      files: {
        en: 'resources/languages/locale-en.json',
        ms: 'resources/languages/locale-ms.json',
        id: 'resources/languages/locale-id.json',
        vn: 'resources/languages/locale-vn.json',
      },
    });

    $translateProvider.preferredLanguage('en');
    $translateProvider.fallbackLanguage('en');

    // enable escaping of HTML
    // see http://angular-translate.github.io/docs/#/guide/19_security
    $translateProvider.useSanitizeValueStrategy('sanitizeParameters');
  }
}());
