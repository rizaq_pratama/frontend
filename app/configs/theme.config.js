;(function() {
    'use strict';

    angular
        .module('nvOperatorApp.configs')
        .config(themeConfig);

    themeConfig.$inject = ['$mdThemingProvider'];

    function themeConfig($mdThemingProvider) {

        $mdThemingProvider.definePalette('nvBlue',
            $mdThemingProvider.extendPalette('blue', {
                '500': '1d4f91', // primary (default)
                '300': '477ced', // highlight (hue-1)
                '800': '98b6e4'  // secondary (hue-2)
            })
        );

        $mdThemingProvider.definePalette('nvYellow',
            $mdThemingProvider.extendPalette('yellow', {
                '500': 'ffad00', // primary (default)
                '300': 'f7c757', // highlight (hue-1)
                '800': '73531d'  // secondary (hue-2)
            })
        );

        $mdThemingProvider.definePalette('nvRed',
            $mdThemingProvider.extendPalette('red', {
                '500': 'ba0c2f', // primary (default)
                '300': 'e00d38', // highlight (hue-1)
                '800': 'f8a3bc'  // secondary (hue-2)
            })
        );

        $mdThemingProvider.definePalette('nvGreen',
            $mdThemingProvider.extendPalette('green', {
                '500': '257226', // primary (default)
                '300': '3f9922', // highlight (hue-1)
                '800': '79d97c'  // secondary (hue-2)
            })
        );

        $mdThemingProvider.theme('nvBlue').primaryPalette('nvBlue');
        $mdThemingProvider.theme('nvRed').primaryPalette('nvRed');
        $mdThemingProvider.theme('nvYellow').primaryPalette('nvYellow');
        $mdThemingProvider.theme('nvGreen').primaryPalette('nvGreen');

    }

})();
