(function (window) {
    'use strict';

    /**
     * This file defines nv-namespace configuration variables on the window object.
     * These variables will be replaced by Ansible scripts during build / deployment.
     */
    var nv = window.nv = {};
    nv.config = {
        env: 'local',
        servers: {
            authlogin: 'https://auth-qa.ninjavan.co',
            dpms: 'http://operatorv2-local.ninjavan.co/api-qa/$country/dpms',
            engine: 'http://operatorv2-local.ninjavan.co/api-qa/$country/engine',
            core: 'http://operatorv2-local.ninjavan.co/api-qa/$country/core',
            'core-kafka': 'http://operatorv2-local.ninjavan.co/api-qa/$country/core-kafka',
            route: 'http://operatorv2-local.ninjavan.co/api-qa/$country/route',
            driver: 'http://operatorv2-local.ninjavan.co/api-qa/$country/driver',
            'script-engine': 'http://operatorv2-local.ninjavan.co/api-qa/$country/script-engine',
            shipper: 'http://operatorv2-local.ninjavan.co/api-qa/$country/shipper',
            'shipper-search': 'http://operatorv2-local.ninjavan.co/api-qa/$country/shipper-search',
            image: 'http://operatorv2-local.ninjavan.co/api-qa/$country/image',
            ticketing: 'http://operatorv2-local.ninjavan.co/api-qa/$country/ticketing',
            addressing: 'http://operatorv2-local.ninjavan.co/api-qa/$country/addressing',
            hub: 'http://operatorv2-local.ninjavan.co/api-qa/$country/hub',
            vrp: 'http://operatorv2-local.ninjavan.co/api-qa/$country/vrp',
            'shipper-dashboard': 'http://operatorv2-local.ninjavan.co/api-qa/$country/shipper-dashboard',
            auth: 'http://operatorv2-local.ninjavan.co/api-qa/$country/auth',
            reservation: 'http://operatorv2-local.ninjavan.co/api-qa/$country/reservation',
            lighthouse: 'https://api-dev.ninjavan.co/$country/lighthouse',
            overwatch: 'http://operatorv2-local.ninjavan.co/api-qa/$country/overwatch',
            events: 'http://operatorv2-local.ninjavan.co/api-qa/$country/events',
            checkout: 'http://operatorv2-local.ninjavan.co/api-qa/$country/checkout',
            vault: 'http://operatorv2-local.ninjavan.co/api-qa/$country/vault',
            'url-shortener': 'http://qa.nnj.vn',
            'order-create': 'http://operatorv2-local.ninjavan.co/api-qa/$country/order-create',
            'order-search': 'http://operatorv2-local.ninjavan.co/api-qa/$country/order-search',
            shipperpanel: 'http://operatorv2-local.ninjavan.co/api-qa/$country/shipperpanel',
            dash: 'http://operatorv2-local.ninjavan.co/api-qa/$country/dash',
            dp: 'http://operatorv2-local.ninjavan.co/api-qa/$country/dp',
            iot: 'https://iot-qa.ninjavan.co/',
            reports: 'http://operatorv2-local.ninjavan.co/api-qa/$country/reports',
            wallet: 'http://operatorv2-local.ninjavan.co/api-qa/$country/wallet',
            'operator-react': 'http://localhost:3000',
            dwh: 'http://operatorv2-local.ninjavan.co/api-qa/$country/dwh',
            claim: 'https://api-hackday.lixionary.com',
            'order-service': 'http://operatorv2-local.ninjavan.co/api-qa/$country/order',
        },
        map: {
            mode: 'mapbox',
            key: 'pk.eyJ1IjoibmluamF2YW4iLCJhIjoiY2ludmFkbzNsMTQ2dHVrbTNscGMwdmRjdSJ9.cw0OWMamjLCuLB2kw6yFPg'
        },
        gaTrackingId: 'UA-106082721-3'
    };

    nv.load = {
        googleAnalytics: function () {
            /* jshint ignore:start */
            (function (i, s, o, g, r, a, m) {
                i['GoogleAnalyticsObject'] = r; i[r] = i[r] || function () {
                    (i[r].q = i[r].q || []).push(arguments)
                }, i[r].l = 1 * new Date(); a = s.createElement(o),
                    m = s.getElementsByTagName(o)[0]; a.async = 1; a.src = g; m.parentNode.insertBefore(a, m)
            })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');
            /* jshint ignore:end */
        },
        favicon: function () {
            /* jshint -W030 */
            document.head || (document.head = document.getElementsByTagName('head')[0]);
            /* jshint +W030 */
            if ('local' === nv.config.env) {
                addFavicon('icon', 'lib/ninja-commons/assets/ico/dev/favicon-local.png', '32x32');
                return;
            } else if ('dev' === nv.config.env) {
                addFavicon('icon', 'lib/ninja-commons/assets/ico/dev/favicon-dev.png', '32x32');
                return;
            }
            addFavicon('shortcut icon', 'lib/ninja-commons/assets/ico/default/favicon.ico');
            addFavicon('icon', 'lib/ninja-commons/assets/ico/default/favicon32.png', '32x32');
            function addFavicon(rel, href, sizes) {
                var link = document.createElement('link');
                link.rel = rel;
                link.href = href;
                if (sizes) {
                    link.setAttribute('sizes', sizes);
                }
                document.head.appendChild(link);
            }
        }
    };

    /**
     * For corev2 migration, dummy global vars
     */
    window.process = {
        env: {
            NODE_ENV: nv.config.env
        }
    }
    window.__CONFIG__ = {}
})(window);
