;(function() {
    'use strict';

    angular
        .module('nvOperatorApp.directives')
        .directive('appSideNavDriverMessaging', directive);

    directive.$inject = ['appSidenav'];

    function directive(appSidenav) {

        return {
            restrict: 'E',
            templateUrl: 'directives/side-nav-driver-messaging/side-nav-driver-messaging.directive.html',
            scope: {},
            controller: controller,
            controllerAs: 'ctrl',
            link: link
        };

        function link() {
            appSidenav.init.driverMessaging();
        }

    }

    controller.$inject = [
        '$q',
        '$scope',
        '$timeout',
        'nvAuth',
        'nvCoupledListUtils',
        'appSidenav',
        'DriverType',
        'Driver',
        'DriverMessage',
        'OperatorMessage'
    ];

    function controller(
        $q,
        $scope,
        $timeout,
        nvAuth,
        nvCoupledListUtils,
        appSidenav,
        DriverType,
        Driver,
        DriverMessage,
        OperatorMessage) {
        /* jshint -W040 */
        var ctrl = this;
        /* jshint +W040 */

        ctrl.isSideNavOpen = false;
        ctrl.loadingPromise = null;
        ctrl.message = '';
        ctrl.isHighPriorityMessage = false;

        ctrl.recipients = {
            driverTypes: {
                searchText: '',
                possibleOptions: [],
                selectedOptions: [],
                onAppendOneFn: onDriverTypeAppendOneFn,
                onRemoveOneFn: onDriverTypeRemoveOneFn,
                onUnloadAllFn: onDriverTypeUnloadAllFn
            },
            drivers: {
                searchText: '',
                possibleOptions: [],
                selectedOptions: [],
                onAppendOneFn: onDriverAppendOneFn
            }
        };

        ctrl.onClearAll = onClearAll;

        ctrl.sendState = 'default';
        ctrl.sendIsDisabled = true;
        ctrl.onSend = onSend;
        ctrl.isSendDisabled = isSendDisabled;

        //////////////////////////////////////////////////

        ctrl.messageHistoryHeader = {
            sentTime : "Date",//$translate.instant('date'),
            priority : "P",//$translate.instant('priority'),
            text : "Message",//$translate.instant('message'),
            seenNo : "Seen No."//$translate.instant('seenNo'),
        };

        ctrl.onSearchReceipts = onSearchReceipts;
        ctrl.onFetchPagePromise = onFetchPagePromise;
        ctrl.onGetItem = onGetItem;
        //////////////////////////////////////////////////

        $scope.$watch(appSidenav.isOpen.driverMessaging, watchIsOpen);

        //////////////////////////////////////////////////

        function watchIsOpen(isOpen) {
            ctrl.isSideNavOpen = isOpen;
            if (!isOpen) {
                return;
            }

            ctrl.isHighPriorityMessage = false;
            ctrl.recipients.driverTypes.selectedOptions = [];
            ctrl.recipients.driverTypes.searchText = '';
            ctrl.recipients.drivers.selectedOptions = [];
            ctrl.recipients.drivers.searchText = '';

            ctrl.sendState = 'default';
            ctrl.sendIsDisabled = true;

            ctrl.loadingPromise = $q.all([DriverType.searchAll(), Driver.searchAll()]).then(success, $q.reject);

            function success(results) {
                var driverTypeResults = results[0],
                    driverResults = results[1];

                ctrl.recipients.driverTypes.possibleOptions = DriverType.toFilterOptions(driverTypeResults);
                ctrl.recipients.drivers.possibleOptions = Driver.toFilterOptions(driverResults, {optionsInclude: ['driverTypeId']});
                ctrl.filterMessage = filterMessage;

                ctrl.sendIsDisabled = false;

                $timeout(function () {
                    // needed for the nv-pagination inside side nav (hidden view) to work correctly
                    angular.element(window).triggerHandler('resize');
                });
            }
        }

        function onDriverTypeAppendOneFn(driverType) {
            ctrl.sendState = 'default';

            nvCoupledListUtils.transfer(
                ctrl.recipients.drivers.possibleOptions,
                ctrl.recipients.drivers.selectedOptions,
                matchFn(driverType), {sort: sortFn}
            );

            // select all drivers for the newly added driver type
            _.forEach(ctrl.recipients.drivers.selectedOptions, function(driver) {
                if (matchFn(driverType)(driver)) { driver._selected = true; }
            });
        }

        function onDriverTypeRemoveOneFn(driverType) {
            ctrl.sendState = 'default';

            // un-select all drivers for the newly added driver type
            _.forEach(ctrl.recipients.drivers.selectedOptions, function(driver) {
                if (matchFn(driverType)(driver)) { driver._selected = false; }
            });

            nvCoupledListUtils.transfer(
                ctrl.recipients.drivers.selectedOptions,
                ctrl.recipients.drivers.possibleOptions,
                matchFn(driverType), {sort: sortFn}
            );
        }

        function onDriverTypeUnloadAllFn(driverTypes) {
            ctrl.sendState = 'default';

            _.forEach(driverTypes, onDriverTypeRemoveOneFn);
        }

        function onDriverAppendOneFn(driver) {
            ctrl.sendState = 'default';

            // select the newly added driver
            driver._selected = true;
        }

        function onClearAll() {
            ctrl.sendState = 'default';

            nvCoupledListUtils.transferAll(
                ctrl.recipients.driverTypes.selectedOptions,
                ctrl.recipients.driverTypes.possibleOptions,
                {sort: sortFn}
            );

            // un-select all drivers
            _.forEach(ctrl.recipients.drivers.selectedOptions, function(driver) {
                driver._selected = false;
            });

            nvCoupledListUtils.transferAll(
                ctrl.recipients.drivers.selectedOptions,
                ctrl.recipients.drivers.possibleOptions,
                {sort: sortFn}
            );
        }

        function onSend() {
            ctrl.sendState = 'working';

            var from = nvAuth.user() || {firstName: 'Ninja Van'};
            var priority = ctrl.isHighPriorityMessage ? 1 : 0;
            var selectedDrivers = _.filter(ctrl.recipients.drivers.selectedOptions, filterFn);

            var selectedIds = _.map(selectedDrivers, function (driver) {
                return driver.id;
            });

            var promise = DriverMessage.notifyMany(selectedIds, from.firstName, ctrl.message, priority);
            promise.then(success, failure);

            function success() {
                onClearAll();
                ctrl.isHighPriorityMessage = false;
                ctrl.recipients.driverTypes.searchText = '';
                ctrl.recipients.drivers.searchText = '';
                ctrl.message = '';
                ctrl.sendState = 'success';
                ctrl.refreshMessage();
            }

            function failure() {
                ctrl.sendState = 'failure';
            }
        }

        function isSendDisabled() {
            return ctrl.sendIsDisabled || !ctrl.message || ctrl.message.trim() === '' || !_.some(ctrl.recipients.drivers.selectedOptions, someFn);
        }

        function matchFn(driverType) {
            return function(driver) {
                return driver.driverTypeId === driverType.id;
            };
        }

        function sortFn(a, b) {
            if (a.displayName < b.displayName) { return -1; }
            if (a.displayName > b.displayName) { return  1; }
            return 0;
        }

        function filterFn(driver) {
            return driver._selected;
        }

        function someFn(driver) {
            return driver._selected;
        }

        function onSearchReceipts($mdOpenMenu, event, message) {
            OperatorMessage.searchReceipts(message.id).then(success);

            function success(receipt) {
                ctrl.receiptsList = _.sortBy(receipt.messageReceipts,'driverName');
                $mdOpenMenu(event);
            }
        }

        function filterMessage(searchText) {
            searchText = searchText.trim().toLowerCase();

            if(!searchText) {
                var messages = ctrl.messages;

                if(messages.loadedItems.length) {
                    messages.numFilteredItems = messages.numItems;
                    for (var i = 0; i < messages.loadedItems.length; i += messages.itemsPerPage) {
                        messages.loadedPages[i / messages.itemsPerPage + 1] =
                            _.slice(messages.loadedItems, i, i + messages.itemsPerPage);
                    }
                }
                else {
                    //refresh
                    ctrl.refreshMessage();
                }
                return;
            }

            var filteredPages = [];
            var filteredData = filterLoadedPages(ctrl.messages.loadedItems);
            _.orderBy(filteredData, 'sendTime');

            /*jshint -W004 */
            for(var i = 0; i < filteredData.length; i += ctrl.messages.itemsPerPage) {
                filteredPages.push(_.slice(filteredData, i, i + ctrl.messages.itemsPerPage));
            }
            /*jshint +W004 */

            ctrl.messages.numFilteredItems = filteredData.length;

            var numLoadedPages = ctrl.messages.loadedItems.length/ctrl.messages.itemsPerPage + 1;

            for(i = 0; i < numLoadedPages; i++) {
                if(filteredPages[i]) {
                    ctrl.messages.loadedPages[i+1] = filteredPages[i];
                } else {
                    ctrl.messages.loadedPages[i+1] = [];
                }
            }

            function filterLoadedPages(loadedPage) {
                return _.filter(loadedPage, filterByMessage);
            }

            function filterByMessage(message) {
                return message.message && message.message.toLowerCase().indexOf(searchText) >= 0;
            }
        }

        function onFetchPagePromise(options) {
            return OperatorMessage.searchAll(options);
        }

        function onGetItem(response) {
            var messages = response.messages;
            // Format the date
            messages = _.map(messages, function(message) {
                message.sentTime = moment(Number(message.sentTime)).format('YYYY-MM-DD HH:mm:ss');
                return message;
            });

            return messages;
        }

    }

})();
