;(function() {
    'use strict';

    angular
        .module('nvOperatorApp.directives')
        .directive('nvSectionHeader', SectionHeader);

    /**
     * Section Header
     * 
     * @param {boolean} header     the header name of the section header
     * @param {string}  title      translatable resource id
     */

    SectionHeader.$inject = ['nvSectionService', 'nvTranslate'];

    function SectionHeader(SectionService, nvTranslate) {

        return {
            restrict: 'E',
            transclude: true,
            templateUrl: 'directives/sidenav-section/section-header/section-header.directive.html',
            scope: {
                header: '@',
                title: '@sectionTitle',
            },
            link: link
        };

        function link(scope, elem) {
            //check and warnings
            if(angular.isUndefined(scope.title)){
                console.error('No title given for section item: ');
                console.error(elem);
                return;
            }

            //temporary title
            scope.translatedTitle = scope.title;

            //translate the title
            translateTitle();

            //watch for language change
            var offLanguageChange = nvTranslate.onLanguageChanged(translateTitle);

            //expand
            scope.toggleExpand = toggleExpand;

            //clean up listeners
            scope.$on('$destroy', function(){
                SectionService.removeSectionHeader(scope.header);
                offLanguageChange();
            });

            /////////////////////
            function toggleExpand(){
                SectionService.toggle(scope.header);
            }

            function translateTitle(){
                nvTranslate.translate(scope.title).then(setTranslatedTitle);
            }

            function setTranslatedTitle(translatedTitle) {
                scope.translatedTitle = translatedTitle;
                //check and warnings
                if(scope.translatedTitle === scope.title){
                    console.error('Unable to translate ' + scope.title + ' for section item: ');
                    console.error(elem);
                    return;
                }

                var section = {
                    title: scope.translatedTitle,
                    element: elem,
                };
                scope.state = SectionService.addSectionHeader(scope.header, section);
            }
        }
    }

})();
