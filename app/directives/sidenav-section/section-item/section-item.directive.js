(function directive() {
  angular
    .module('nvOperatorApp.directives')
    .directive('nvSectionItem', SectionItem);

    /**
     * Section Item
     *
     * @param {boolean} isActive   boolean whether this item is active
     * @param {string}  parent     the header name of the section header that this item belongs to
     * @param {string}  title      translatable resource id
     * @param {string}  searchText comma-separated keywords, one time binding
     */

  SectionItem.$inject = ['nvSectionService', 'nvTranslate', '$q', '$log'];

  function SectionItem(SectionService, nvTranslate, $q, $log) {
    return {
      restrict: 'E',
      transclude: true,
      templateUrl: 'directives/sidenav-section/section-item/section-item.directive.html',
      scope: {
        isActive: '=',
        parent: '@',
        searchText: '@?',
        title: '@sectionTitle',
      },
      link: link,
    };

    function link(scope, elem) {
      // check and warnings
      if (angular.isUndefined(scope.title)) {
        $log.error('No title given for section item: ');
        $log.error(elem);
        return;
      }

      // temporary title
      scope.translatedTitle = scope.title;
      scope.keywords = angular.isDefined(scope.searchText) ? _.map(scope.searchText.split(','), trimAndLowerCase) : [];

      // translate the title
      translateTitle();

      // watch for language change
      const offLanguageChange = nvTranslate.onLanguageChanged(translateTitle);

      // if it is active, expand parent
      scope.$watch('isActive', expandParentIfActive);

      // clean up listeners
      scope.$on('$destroy', () => {
        offLanguageChange();
      });

      // ///////////
      function expandParentIfActive() {
        if (scope.isActive) {
          SectionService.expand(scope.parent);
        }
      }

      function trimAndLowerCase(text) {
        return _.trim(text).toLowerCase();
      }

      function translateTitle() {
        $q.all([
          nvTranslate.translate(scope.title),
          $q.all(mapTranslatePromise(scope.keywords)),
        ]).then(setTranslatedTitle);
        nvTranslate.translate(scope.title).then(setTranslatedTitle);
      }

      function setTranslatedTitle(translated) {
        // spread
        scope.translatedTitle = translated[0];
        const translatedSearchTexts = translated[1];

        // check and warnings
        if (scope.translatedTitle === scope.title) {
          $log.error(`Unable to translate ${scope.title} for section item: `);
          $log.error(elem);
          return;
        }

        // update to section service
        const section = {
          element: elem,
          search: translatedSearchTexts,
          title: scope.translatedTitle,
        };
        scope.state = SectionService.addSectionItem(scope.parent, section);
      }

      function mapTranslatePromise(texts) {
        return _.map(texts, text =>
          ((text.indexOf('.') > -1) ?
            nvTranslate.translate(text) :
            $q.when(text))
        );
      }
    }
  }
}());
