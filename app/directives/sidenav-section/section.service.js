(function service() {
  angular
    .module('nvOperatorApp.directives')
    .factory('nvSectionService', SectionService);

  SectionService.$inject = ['nvFuse', 'nvTranslate'];
  function SectionService(Fuse, nvTranslate) {
    const section = {};

    // private variables
    section.sections = {};
    section.globalState = {
      isSearching: false,
    };
    section.fuse = null;

    // public methods
    section.addSectionHeader = addSectionHeader;
    section.removeSectionHeader = removeSectionHeader;
    section.addSectionItem = addSectionItem;
    section.isExpanded = isExpanded;
    section.expandAll = expandAll;
    section.expand = expand;
    section.closeAll = closeAll;
    section.close = close;
    section.toggle = toggle;
    section.search = search;
    section.restoreSectionItem = restoreSectionItem;

    // listener
    nvTranslate.onLanguageChanged(destroyFuse);

    return section;

    // /////////////////////////
    function addSectionHeader(name, header) {
      // Didn't do any check, because can't solve the issue of having 2 different element
      // registering the same section header
      // but only 1 present in the ui, the previous one didn't destroy scope.
      // A more common-sense of checking for duplicated section name is that
      // the section will expand together with with its duplicated copy
      // if(angular.isDefined(section.sections[name]) &&
      // angular.isObject(section.sections[name].header) &&
      // section.sections[name].header.element !== header.element){
      //     console.error('Duplicated section name: "' + name + '"');
      // }
      section.sections[name] = angular.isDefined(section.sections[name]) ?
                                        section.sections[name] :
                                        { items: [],
                                                state: {
                                                  isExpanded: false,
                                                  global: section.globalState,
                                                },
                                            };
      section.sections[name].header = header;
      return section.sections[name].state;
    }

    function removeSectionHeader(name) {
      if (angular.isDefined(section.sections[name])) {
        section.sections[name].header = null;
      }
    }

    function addSectionItem(parentName, item) {
      checkSectionExist(parentName);
      const index = _.findIndex(section.sections[parentName].items, compareElement);
      if (index === -1) {
        section.sections[parentName].items.push(item);
      } else {
        section.sections[parentName].items[index] = item;
      }
      return section.sections[parentName].state;

      // /////
      function compareElement(i) {
        return i.element === item.element;
      }
    }

    function isExpanded(name) {
      return section.sections[name] && section.sections[name].state.isExpanded;
    }

    function expandAll() {
      _.each(section.sections, (s) => {
        s.state.isExpanded = true;
      });
    }

    function expand(name) {
      if (checkSectionExist(name)) {
        section.sections[name].state.isExpanded = true;
      }
    }

    function closeAll() {
      _.each(section.sections, (s) => {
        s.state.isExpanded = false;
      });
    }

    function close(name) {
      if (checkSectionExist(name)) {
        section.sections[name].state.isExpanded = false;
      }
    }

    function toggle(name) {
      if (checkSectionExist(name)) {
        section.sections[name].state.isExpanded = !section.sections[name].state.isExpanded;
      }
    }

    function search(text) {
      if (section.fuse === null) {
        const items = _.flatten(_.map(section.sections, (s, name) =>
          _.map(s.items, item =>
            ({
              name: name,
              header: s.header,
              item: item,
            })
          )
        ));
        const options = {
          shouldSort: true,
          threshold: 0.2,
          keys: [
            {
              name: 'header.title',
              weight: 0.3,
            },
            {
              name: 'item.title',
              weight: 0.4,
            },
            {
              name: 'item.search',
              weight: 0.3,
            },
          ],
        };
        section.fuse = new Fuse(items, options);
      }

      if (angular.isDefined(text) && text !== '') {
        section.globalState.isSearching = true;
        return section.fuse.search(text);
      }
      section.globalState.isSearching = false;
      return [];
    }

    function restoreSectionItem(items) {
      const categoryToRestore = _(items).map(item => item.name).uniq().value();
      _.each(categoryToRestore, (category) => {
        const categorySection = section.sections[category];
        let lastElement = categorySection.header.element;
        const j = categorySection.items.length;
        for (let i = 0; i < j; i++) {
          lastElement.after(categorySection.items[i].element);
          lastElement = categorySection.items[i].element;
        }
      });
    }

    function destroyFuse() {
      section.fuse = null;
    }

    function checkSectionExist(name) {
      if (angular.isUndefined(section.sections[name])) {
        // might be of a race condition, create section item first
        // mainly, the translation file isn't ready, therefore, 
        // wasn't registered into the section service
        section.sections[name] = createDefaultSection();
      }
      return true;
    }

    function createDefaultSection() {
      return {
        items: [],
        state: {
          isExpanded: false,
          isSearching: false,
        },
      };
    }
  }
}());
