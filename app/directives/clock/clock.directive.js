(function directive() {
  angular
    .module('nvOperatorApp.directives')
    .directive('appClock', appClock);

  appClock.$inject = ['$interval', 'nvTimezone', '$mdMenu', '$mdSelect'];
  function appClock($interval, nvTimezone, $mdMenu, $mdSelect) {
    return {
      restrict: 'E',
      templateUrl: 'directives/clock/clock.directive.html',
      link: link,
      scope: true,
    };

    function link(scope) {
      const ctrl = {};
      scope.ctrl = ctrl;

      ctrl.time = moment();
      ctrl.nvTimezone = nvTimezone;

      ctrl.location = nvTimezone.guessBrowserLocation();
      ctrl.timezone = nvTimezone.getOperatorTimezone();
      ctrl.timezones = nvTimezone.getAllTimezones();
      ctrl.timezoneInfo = nvTimezone.getOperatorTimezoneInfo();
      ctrl.selectedTimezone = ctrl.timezoneInfo;
      ctrl.setTimezoneToCurrentLocation = setTimezoneToCurrentLocation;
      ctrl.updateTimezone = updateTimezone;

      const stop = $interval(() => { ctrl.time = moment(); }, 1000);

      scope.$on('$destroy', () => $interval.cancel(stop));

      scope.$on('$mdMenuOpen', () => {
        $mdMenu.hide();
        $mdSelect.hide();
      });

      scope.$on('$mdMenuClose', () => {
        $mdSelect.hide();
      });

      function setTimezoneToCurrentLocation() {
        nvTimezone.setOperatorTimezone(ctrl.location);
      }

      function updateTimezone() {
        nvTimezone.setOperatorTimezone(ctrl.selectedTimezone.iana);
        ctrl.timezoneInfo = ctrl.selectedTimezone;
        ctrl.timezone = nvTimezone.getOperatorTimezone();
      }
    }
  }
}());
