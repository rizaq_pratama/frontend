(function() {
    'use strict';

    angular
        .module('nvOperatorApp.directives')
        .directive('appFilterUp', filterUp);

    filterUp.$inject = [];

    function filterUp() {

        return {
            restrict: 'E',
            templateUrl: 'directives/filter-up/filter-up.directive.html',
            replace: true,
            scope: {
                mainTitle: '@',
                optionTitles: '=',
                model: '='
            },
            link: link
        };

        
        function link (scope){

            scope.titles = scope.optionTitles || [];


            if (!angular.isDefined(scope.model)) {
                scope.model = _.repeat('0', scope.titles.length); // default all to false if undefined
            }
            scope.onClick = function(index) {
                var str = scope.model;
                scope.model = str.substr(0, index) + ((+str[index] + 1) % 2) + str.substr(index + 1);

                if (angular.isDefined(scope.form)) {
                    scope.form.$setDirty();
                }
            };

            scope.onClearAll = function() {
                scope.model = _.repeat('0', scope.model.length);
            };

            scope.$watch('optionTitles', function (newVal) {
                if(newVal){
                    scope.titles= newVal || [];
                }
            });

        }
        

    }

})();
