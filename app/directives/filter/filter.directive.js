(function() {
    'use strict';

    angular
        .module('nvOperatorApp.directives')
        .directive('appFilter', filter);

    function filter() {

        return {
            restrict: 'E',
            templateUrl: 'directives/filter/filter.directive.html',
            replace: true,
            scope: {
                mainTitle: '@',
                model: '='
            },
            compile: compile
        };

        function compile(tElement, tAttrs) {
            var index = 0;
            var attrName = 'optionTitle';
            var title = tAttrs[attrName + index];
            while (!_.isUndefined(title)) {
                tElement[0].children[1].innerHTML +=
                    '<md-button' +
                    ' type="button"' +
                    ' class="filter-button"' +
                    ' aria-label="' + title + '"' +
                    ' ng-click="onClick(' + index + ')"' +
                    ' ng-class="{\'active\': +model[' + index + ']}">' +
                    title +
                    '</md-button>';
                title = tAttrs[attrName + (++index)];
            }
            return linkWrapper(index);
        }

        function linkWrapper(count) {
            return function link(scope) {
                if (!angular.isDefined(scope.model)) {
                    scope.model = _.repeat('0', count); // default all to false if undefined
                }

                scope.onClick = function(index) {
                    var str = scope.model;
                    scope.model = str.substr(0, index) + ((+str[index] + 1) % 2) + str.substr(index + 1);

                    if (angular.isDefined(scope.form)) {
                        scope.form.$setDirty();
                    }
                };

                scope.onClearAll = function() {
                    scope.model = _.repeat('0', scope.model.length);
                };
            };
        }

    }

})();
