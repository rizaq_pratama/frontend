(function() {
    'use strict';

    angular
        .module('nvOperatorApp.directives')
        .directive('appFilterDynamic', filterDynamic);

    filterDynamic.$inject = ['$compile'];

    function filterDynamic($compile) {

        return {
            restrict: 'E',
            template: '<div class="filter"></div>',
            replace: true,
            scope: {
                mainTitle: '@',
                optionTitles: '&',
                model: '='
            },
            compile: compile
        };

        function compile(tElement) {
            // save the pre-compiled DOM which we will use later to replace the angular-compiled DOM
            var tElem = tElement[0].cloneNode(true);
            tElem.innerHTML +=
                '<div layout="row" layout-align="space-between center">' +
                ' <div class="main-title nv-text-ellipsis">{{mainTitle}}</div>' +
                ' <md-button class="clear-button" ng-click="onClearAll()">Clear All</md-button>' +
                '</div>' +
                '<div class="filter-buttons-container"></div>';
            return linkWrapper(tElem);
        }

        function linkWrapper(tElem) {
            return function link(scope, elem) {
                var titles = scope.optionTitles() || [];

                _.forEach(titles, function(title, index) {
                    tElem.children[1].innerHTML +=
                        '<md-button' +
                        ' type="button"' +
                        ' class="filter-button"' +
                        ' aria-label="' + title + '"' +
                        ' ng-click="onClick(' + index + ')"' +
                        ' ng-class="{\'active\': +model[' + index + ']}">' +
                        title +
                        '</md-button>';
                });

                if (!angular.isDefined(scope.model)) {
                    scope.model = _.repeat('0', titles.length); // default all to false if undefined
                }

                scope.onClick = function(index) {
                    var str = scope.model;
                    scope.model = str.substr(0, index) + ((+str[index] + 1) % 2) + str.substr(index + 1);

                    if (angular.isDefined(scope.form)) {
                        scope.form.$setDirty();
                    }
                };

                scope.onClearAll = function() {
                    scope.model = _.repeat('0', scope.model.length);
                };

                elem.replaceWith($compile(tElem)(scope));
            };
        }

    }

})();
