(function directive() {
  angular
        .module('nvOperatorApp.directives')
        .directive('appSidenav', appSidenavDirective);

  appSidenavDirective.$inject = ['appSidenav'];

  function appSidenavDirective(appSidenav) {
    return {
      restrict: 'E',
      templateUrl: 'directives/sidenav/sidenav.directive.html',
      scope: {},
      controller: controller,
      controllerAs: 'ctrl',
      link: link,
    };

    function link(scope, elem) {
      appSidenav.init.mainMenu();

      scope.searchInput = elem.find('input');
      scope.container = elem.find('md-list').children().eq(0);
    }
  }

  controller.$inject = ['$rootScope', '$scope', '$state', '$window', '$timeout',
                          'nvURL', 'appSidenav', 'hotkeys', 'nvEnv'];

  function controller($rootScope, $scope, $state, $window, $timeout,
                      nvURL, appSidenav, hotkeys, nvEnv) {
    const ctrl = this;

    ctrl.isConfigLockedOpen = appSidenav.isConfigLockedOpen;
    ctrl.navigateTo = navigateTo;
    ctrl.navigateToV1 = navigateToV1;
    ctrl.navigateToParams = navigateToParams;
    ctrl.$state = $state;
    ctrl.currentDomainIn = currentDomainIn;
    ctrl.sidenavSection = appSidenav.mainMenuSection;
    ctrl.onKeydown = onKeydown;
    ctrl.isSaas = isSaas;

    // bind global search
    hotkeys.bindTo($scope)
            .add({
              combo: 'f f',
              description: 'Search Function or ID',
              callback: focusSearch,
            });

    // search within sidenav
    let searchSelectIndex = -1;
    let searchSelectSections = [];
    let lastResult = [];
    $scope.$watch('ctrl.search', (value, prevValue) => {
      // dont' trigger at the first when it got '', trigger after someone really typed something
      if (angular.isDefined(value) && angular.isDefined(prevValue)) {
        // Search SideNav Sections
        appSidenav.mainMenuSection.restoreSectionItem(lastResult);
        lastResult = appSidenav.mainMenuSection.search(value);
        _.eachRight(lastResult, (r) => {
          $scope.container.prepend(r.item.element);
        });
        // index
        removeActive(searchSelectIndex);
        searchSelectIndex = 0;
        searchSelectSections = $scope.container.find('nv-section-item');
        addActive(searchSelectIndex);
      }
    });

    appSidenav.setConfigLockedOpen(true);

    function navigateTo(state, $event) {
      if (!appSidenav.isLockedOpen.mainMenu()) {
        appSidenav.close.mainMenu();
      }
      if ($state.current.name !== state) {
        // if the command / ctrl key is down, open the URL in a new tab
        if ($event.metaKey || $event.ctrlKey) {
          $window.open($state.href(state, null, { absolute: true }), '_blank');
        } else {
          $state.go(state);
        }
      }
    }

    function navigateToParams(state, $event, param) {
      if ($event.metaKey || $event.ctrlKey) {
        $window.open($state.href(state, param, { absolute: true }), '_blank');
      } else {
        $state.go(state, param);
        ctrl.search = '';
      }
    }

    function navigateToV1() {
      $window.location.href = nvURL.buildV1Url(nv.config.env, $rootScope.domain);
    }

    function currentDomainIn(domains) {
      return domains.indexOf($rootScope.domain) !== -1;
    }

    function focusSearch(event) {
      event.preventDefault();
      $timeout(() => {
        $scope.searchInput.focus();
      });
    }

    function onKeydown(event) {
      if (event.keyCode === 38 || event.keyCode === 40) {
        removeActive(searchSelectIndex);
        const dir = (event.keyCode === 38) ? -1 : 1;
        searchSelectIndex = (searchSelectIndex + searchSelectSections.length + dir)
                               % searchSelectSections.length;
        addActive(searchSelectIndex);
        event.preventDefault();
      } else if (event.keyCode === 13) {
        if (searchSelectIndex > -1 && searchSelectIndex < searchSelectSections.length) {
          $timeout(() => {
            searchSelectSections[searchSelectIndex].click();
            $scope.searchInput.blur();
          });
        }
        event.preventDefault();
      }
    }

    function isSaas() {
      return nvEnv.isSaas(nv.config.env);
    }

    function removeActive(index) {
      if (index !== -1 && searchSelectSections.length > index) {
        searchSelectSections.eq(index).removeClass('active');
      }
    }
    function addActive(index) {
      if (angular.isNumber(index) && index !== -1 && searchSelectSections.length > index) {
        searchSelectSections.eq(index).addClass('active');
      }
    }
  }
}());
