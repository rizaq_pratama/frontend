(function directive() {
  angular
    .module('nvCommons.directives')
    .directive('nvWaypointBarContainer', nvWaypointBarContainer);

  nvWaypointBarContainer.$inject = ['$timeout'];

  function nvWaypointBarContainer($timeout) {
    return {
      restrict: 'E',
      templateUrl: 'directives/waypoint-bar-container/waypoint-bar-container.directive.html',
      scope: {
        waypoints: '=',
        idx: '=',
      },
      link: link,
    };

    function link($scope) {
      $scope.ready = true;
      // cancel the timer when element re rendered.
      $scope.$watch('idx', () => {
        if ($scope.timer) {
          $timeout.cancel($scope.timer);
        }
        $scope.ready = false;
        // render after timeout
        $scope.timer = $timeout(() => ($scope.ready = true), 3000);
      });
    }
  }
}());
