;(function() {
  'use strict';

  angular
    .module('nvOperatorApp.directives')
    .directive('nvIframeContainer', ['$window', iframeContainer]);

  function iframeContainer($window) {
    function link(scope, element, attrs, controller) {
      element.css({
        display: 'block', 
        height: '100%',
        width: '100%',
        overflow: 'hidden'
      });

      const iframe = element.find('iframe')[0]
      iframe.src = scope.url

      let iframeStyle = `
        overflow:hidden;
        width:100%;
        border:none;
      `;

      iframeStyle += 'height: ' + element[0].clientHeight + 'px';
      iframe.setAttribute('style', iframeStyle);

      function onResize() {
        iframe.style.height = element[0].clientHeight + 'px';
      }

      $window.addEventListener('resize', onResize);

      controller.$onDestroy = function () {
        $window.removeEventListener('resize', onResize);
      }
    }

    return {
      restrict: 'E',
      template: `
        <iframe></iframe>
      `,
      scope: {
        url: '='
      },
      controller: function () {},
      link: link
    };
  }

})();
