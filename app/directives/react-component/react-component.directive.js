/* global Migration_corev2_IFrameHelpers */

;(function() {
  'use strict';

  angular
    .module('nvOperatorApp.directives')
    .directive('nvReactComponent', nvReactComponent);

  nvReactComponent.$inject = ['$window', 'Driver', 'Address', 'Zone', 'RouteGroup', 'nvDomain', '$rootScope', 'Route', 'Hub'];

  function nvReactComponent($window, Driver, Address, Zone, RouteGroup, nvDomain, $rootScope, Route, Hub) {
      /**
       *  All exported services should be written here
       *  Include
       *  - model function
       *  - command to invoke angular UI
       *  - request for information
       */
    const exportedServices = {
      driver_searchOne: {
        serviceFn: Driver.searchOne,
      },
      address_verifyJaroScore: {
        serviceFn: Address.verifyJaroScore,
      },
      address_getReverifiedAddresses: {
        serviceFn: Address.getReverifiedAddresses,
      },
      address_getAddressesByRouteGroup: {
        serviceFn: Address.getAddressesByRouteGroup,
      },
      address_createAddressEvent: {
        serviceFn: Address.createAddressEvent,
      },
      address_initializeAddressVerification: {
        serviceFn: Address.initializeAddressVerification,
      },
      address_fetchAddressVerification: {
        serviceFn: Address.fetchAddressVerification,
      },
      address_fetchAddressVerificationByrouteGroup: {
        serviceFn: Address.fetchAddressVerificationByrouteGroup,
      },
      address_getUnverifiedAddressStats: {
        serviceFn: Address.getUnverifiedAddressStats,
      },
      address_searchAddress: {
        serviceFn: Address.searchAddress,
      },
      address_preAvAssign: {
        serviceFn: Address.preAvAssign,
        transformFn: payload => ([payload.zoneId, payload.waypointIds]),
      },
      address_archiveOrderJaroScore: {
        serviceFn: Address.archiveOrderJaroScore,
      },
      addreess_updateShipperAddress: {
        serviceFn: Address.updateShipperAddress,
      },
      zone_getZones: {
        serviceFn: Zone.read,
      },
      hub_getHubs: {
        serviceFn: Hub.read,
      },
      route_getPagedRouteMonitoring: {
        serviceFn: Route.getPagedRouteMonitoring,
      },
      routeGroup_read: {
        serviceFn: RouteGroup.read,
      },
      address_allShipperAddress: {
        serviceFn: Address.allShipperAddress,
      },
      settings_getUserSetting: {
        serviceFn: () => ($rootScope.user),
      },
      settings_getDomain: {
        serviceFn: nvDomain.getDomain,
      },
    };

    return {
      restrict: 'E',
      templateUrl: 'directives/react-component/react-component.directive.html',
      controller: function () {},
      scope: {
        url: '@',
      },
      link,
    };

    function link(scope, element, attrs, controller) {

      const host = initHost();

      for (const serviceName in exportedServices) {
        const { serviceFn, transformFn } = exportedServices[serviceName] 
        host.registerService(serviceName, serviceFn, transformFn)
      }

      controller.$onDestroy = function () {
        host.destroy();
      };
    }

    function initHost() {
      return new Migration_corev2_IFrameHelpers.IFrameHost($window, {
        allowedClientOrigins: [nv.config.servers['operator-react']],
      });
    }
  }
})();

