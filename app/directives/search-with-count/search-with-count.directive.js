(function() {
    'use strict';

    angular
        .module('nvOperatorApp.directives')
        .directive('appSearchWithCount', searchWithCount);

    function searchWithCount() {

        return {
            restrict: 'E',
            templateUrl: 'directives/search-with-count/search-with-count.directive.html',
            replace: true,
            scope: {
                onSearch: '&',
                count: '&',
                when: '&',
                placeholder: '@'
            },
            link: link
        };

        function link(scope) {
            scope.searchText = '';
            scope.$watch('searchText', function(searchText) {
                scope.onSearch({searchText: searchText});
            });
        }

    }

})();
