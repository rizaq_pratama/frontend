(function directive() {
  angular
    .module('nvOperatorApp.directives')
    .directive('appProfilenav', profilenav);

  profilenav.$inject = [
    '$rootScope',
    '$window',
    '$state',
    '$stateParams',
    'nvAuth',
    '$mdSelect',
    'nvDomain',
  ];

  function profilenav($rootScope, $window, $state, $stateParams, nvAuth, $mdSelect, nvDomain) {
    return {
      restrict: 'E',
      templateUrl: 'directives/profilenav/profilenav.directive.html',
      scope: {
        user: '=',
      },
      link: link,
    };

    function link(scope) {
      scope.navigateTo = navigateTo;
      scope.logout = $rootScope.logout;

      scope.domain = nvDomain.getDomain();
      scope.getCountryName = nvDomain.getCountryName;

      scope.$watch('domain.current', (domain, prevDomain) => {
        if (domain !== prevDomain) {
          nvDomain.reloadDomain(domain, $stateParams);
        }
      });

      scope.accessToken = nvAuth.getAccessToken();
      scope.clipboard = { copied: false };

      scope.$on('$mdMenuClose', () => {
        scope.clipboard.copied = false;
        $mdSelect.hide();
      });

      // ///////////////////////

      function navigateTo(state, $event) {
        if ($state.current.name !== state) {
          // if the command / ctrl key is down, open the URL in a new tab
          if ($event.metaKey || $event.ctrlKey) {
            $window.open($state.href(state, null, { absolute: true }), '_blank');
          } else {
            $state.go(state);
          }
        }
      }
    }
  }
}());
