(function() {
    'use strict';

    angular
        .module('nvOperatorApp.directives')
        .directive('appDataHeader', dataHeader);

    function dataHeader() {

        return {
            restrict: 'E',
            templateUrl: 'directives/data-header/data-header.directive.html',
            replace: true,
            transclude: true,
            scope: {
                mainTitle: '@',
                addTitle: '@',
                onAdd: '&',
                data: '=',
                csvHeader: '@',
                csvColumnOrder: '@',
                filename: '@'
            }
        };

    }

})();
