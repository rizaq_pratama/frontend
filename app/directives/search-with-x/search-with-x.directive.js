(function() {
    'use strict';

    angular
        .module('nvOperatorApp.directives')
        .directive('appSearchWithX', searchWithX);

    function searchWithX() {

        return {
            restrict: 'E',
            templateUrl: 'directives/search-with-x/search-with-x.directive.html',
            replace: true,
            scope: {
                onSearch: '&',
                placeholder: '@'
            },
            link: link
        };

        function link(scope) {
            scope.searchText = '';

            scope.$watch('searchText', function(searchText) {
                scope.onSearch({searchText: searchText});
            });

            scope.onClearSearchText = function() {
                scope.searchText = '';
            };
        }

    }

})();
