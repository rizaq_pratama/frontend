;(function() {
    'use strict';

    angular
        .module('nvOperatorApp.directives')
        .directive('appBrandnav', brandnav);

    brandnav.$inject = ['appSidenav'];

    function brandnav(appSidenav) {

        return {
            restrict: 'E',
            templateUrl: 'directives/brandnav/brandnav.directive.html',
            scope: {
                title: '@'
            },
            link: link
        };

        function link(scope) {
            scope.openSidenav = openSidenav;
            scope.showLogo = showLogo;

            function openSidenav() {
                if (!showLogo()) { appSidenav.open.mainMenu(); }
            }

            function showLogo() {
                return appSidenav.isLockedOpen.mainMenu();
            }
        }

    }

})();
