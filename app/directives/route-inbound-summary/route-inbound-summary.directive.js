(function directive() {
  angular.module('nvOperatorApp.directives')
    .directive('appRouteInboundSummary', appRouteInboundSummary);
  appRouteInboundSummary.$inject = [];

  function appRouteInboundSummary() {
    return {
      restrict: 'E',
      templateUrl: 'directives/route-inbound-summary/route-inbound-summary.directive.html',
      scope: {
        routeId: '@',
        driverName: '@',
        hubName: '@',
        routeDate: '@',
      },
    };
  }
}());
