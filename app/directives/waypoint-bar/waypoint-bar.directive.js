(function directive() {
  angular
    .module('nvCommons.directives')
    .directive('nvWaypointBar', nvWaypointBar);

  nvWaypointBar.$inject = [];

  function nvWaypointBar() {
    return {
      restrict: 'E',
      templateUrl: 'directives/waypoint-bar/waypoint-bar.directive.html',
      scope: {
        main: '@',
        overlay: '@?',
        overlayBottom: '@?',
        tooltip: '@?',
        tooltipParam: '=?',
      },
      link: link,
    };

    function link(scope) {
      scope.overlay = scope.overlay ? scope.overlay : 'none';
      scope.overlayBottom = scope.overlayBottom ? scope.overlayBottom : 'none';
    }
  }
}());
