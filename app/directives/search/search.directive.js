(function() {
    'use strict';

    angular
        .module('nvOperatorApp.directives')
        .directive('appSearch', search);

    function search() {

        var idx = 0;

        return {
            restrict: 'E',
            templateUrl: 'directives/search/search.directive.html',
            replace: true,
            scope: {
                onSearch: '&',
                placeholder: '@',
                fieldId: '@?',
                onEnterPress: '&?',
                disabled: '=?',
            },
            link: link
        };

        function link(scope) {
            scope.disabled = scope.disabled || false;
            scope.searchText = '';
            scope.$watch('searchText', function(searchText) {
                scope.onSearch({searchText: searchText});
            });

            scope.onKeypressed = function(event){
                if(!_.isUndefined(scope.onEnterPress)){
                    if(event.charCode===13){
                        scope.onEnterPress();
                    }
                }else{
                }

            };

            if (angular.isUndefined(scope.fieldId)){
                scope.id = "field-search_" + idx++;
            } else {
                scope.id = scope.buttonId;
            }
        }

    }

})();
