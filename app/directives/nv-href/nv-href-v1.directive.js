(function() {
    'use strict';

    angular
        .module('nvOperatorApp.directives')
        .directive('nvHrefV1', nvHrefV1);

    /**
     *
     * href to Operator V1 pages based on environment
     *
     * @param {string} nvHrefV1         The string
     * @param {string} nvHrefV1Target   The target for the link to open at, eg: '_blank', '_self', '_parent', '_top', 'framename'.
     *
     * If the element is an anchor tag, it will only update the `href` based on `nvHrefV1`.
     * If the element is not an anchor tag, it will add a onclick listener, and open the link based on `nvHrefV1` and `nvHrefV1Target`
     *
     */

    nvHrefV1.$inject = ['$window', 'nvURL'];

    function nvHrefV1($window, nvURL) {

        return {
            restrict: 'A',
            link: link
        };

        function link(scope, elem, attr) {
            var DOMAIN_REGEX = /#\/(\w+)\//;
            var operatorURL = getOperatorUrl(nv.config.env);

            var tagName = elem[0].tagName.toLowerCase();

            //if it is an anchor tag, update the href, or else add a onclick event
            if(tagName === 'a') {
                attr.$observe('nvHrefV1', updateHref);
            } else {
                elem.on('click', function(){
                    var url = attr.nvHrefV1;
                    var target = angular.isDefined(attr.nvHrefV1Target) ? attr.nvHrefV1Target : '_blank';
                    $window.open(operatorURL + stripForwardSlash(url), target);
                });
            }

            function getOperatorUrl(env){
                return nvURL.buildV1Url(env, window.location.hash.match(DOMAIN_REGEX)[1]) + '/ng#';
            }

            function updateHref(url) {
                elem.attr('href', operatorURL + stripForwardSlash(url));
            }

            function stripForwardSlash(str) {
                return angular.isDefined(str) ? str.replace(/^\//, '') : '';
            }
        }

    }

})();
