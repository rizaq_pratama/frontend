(function() {
    'use strict';

    angular
        .module('nvOperatorApp.runs', [
            'ui.router',
            'pascalprecht.translate',
            'nvCommons.services',
            'nv',
        ]);

})();
