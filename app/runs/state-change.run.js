(function() {
    'use strict';

    angular
        .module('nvOperatorApp.runs')
        .run(stateChangeRun);

    stateChangeRun.$inject = ['$rootScope', '$state', '$log', '$window', '$location', 'nvEnv', 'nvAuth', 'nvNavGuard', 'nvClientStore', '$mdDialog'];

    function stateChangeRun($rootScope, $state, $log, $window, $location, nvEnv, nvAuth, nvNavGuard, nvClientStore, $mdDialog) {

        $rootScope.$on('$stateChangeStart', function(event, toState, toParams, fromState, fromParams, options) {
            const isLock = nvNavGuard.testLock();
            if (isLock) {
                event.preventDefault();
                isLock.then(() => {
                    $state.go(toState, toParams, options);
                });
                return;
            }
            // close all dialog   
            $mdDialog.cancel(); // ignore the returned promise
            if (!nvAuth.isLoggedIn() && toState.url !== '/login') {
                event.preventDefault(); // prevents the transition from happening

                nvClientStore.localStorage.set('previousUrl', $window.location.href, { namespace: false });

                $state.go('login');
                $log.error('Sorry, your session has expired. Please try logging in again.');
            }
            if(toState.data && toState.data.redirect){
                event.preventDefault();
                $state.go(toState.data.redirect, toParams);
            }
        });

        if ($window.ga && !nvEnv.isLocal(nv.config.env)) {
            $window.ga('create', nv.config.gaTrackingId, 'auto'); // initialize google analytics

            $rootScope.$on('$stateChangeSuccess', function() {
                $window.ga('send', 'pageview', $location.path()); // track pageview on state change
            });
        }

    }

})();
