(function run() {
  angular
    .module('nvOperatorApp.runs')
    .run(initRun);

  initRun.$inject = ['$rootScope', 'nvAuth', 'nvTimezone', 'nvTranslate', 'nvClientStore', 'nvDomain'];

  function initRun($rootScope, nvAuth, nvTimezone, nvTranslate, nvClientStore, nvDomain) {
    nvAuth.init();
    nvDomain.init();
    nvTimezone.init();
    nvTranslate.init();
    checkEnv($rootScope, nvClientStore);
  }

  function checkEnv($rootScope, nvClientStore) {
    const currEnv = nvClientStore.localStorage.get('env', { namespace: false });
    if (!currEnv) {
      // set the current environment if it hasn't been set
      nvClientStore.localStorage.set('env', nv.config.env, { namespace: false });
    } else if (currEnv !== nv.config.env) {
      // the environments don't match, so we logout and start over, so that we
      // authenticate with the correct server make API calls with the correct credentials
      $rootScope.logout();
    }
  }
}());
