(function run() {
  /**
   * This file is used to define the global
   * methods or variables on the {@code $rootScope}.
   */
  angular
    .module('nvOperatorApp.runs')
    .run(globalsRun);

  globalsRun.$inject = ['$rootScope', '$state', 'nvClientStore', 'nvAuth'];

  function globalsRun($rootScope, $state, nvClientStore, nvAuth) {
    /**
     * Make the logout method {@code $rootScope.logout()}
     * global, so that we can use it anywhere in the app.
     */
    $rootScope.logout = _.partial(logout, nvAuth, $state, nvClientStore);

  }

  function logout(nvAuth, $state, nvClientStore) {
    nvAuth.logout(() => {
      $state.go('login');
      nvClientStore.localStorage.set('env', nv.config.env, { namespace: false });
    });
  }
}());
