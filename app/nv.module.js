;(function() {
  'use strict';

  angular
    .module('nv', [
      'nvCurrency',
      'nvDistance',
      'nvEnv',
      'nvJson',
      'nvPrint',
      'nvQrcode',
      'nvByte',
      'nvCamelCase2HyphenCase',
      'nvCamelCase2NaturalCase',
      'nvFilterByYear',
      'nvLimitTo',
      'nvMonthName',
      'nvNaturalCase2CamelCase',
      'nvNaturalCase2HyphenCase',
      'nvSmartDistance',
      'nvSnakeCase2NaturalCase',
      'nvURL',
    ]);

})();
