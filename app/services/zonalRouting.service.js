((function ZonalRoutingService() {
  angular
    .module('nvOperatorApp.services')
    .factory('nvZonalRouting', nvZonalRouting);

  nvZonalRouting.$inject = [
    'nvMaps', 'Waypoint', 'Driver', 'Transaction', 'Order',
    'nvDateTimeUtils', 'nvTimezone', 'nvTranslate', '$timeout', 'nvAddress',
  ];
  function nvZonalRouting(
    nvMaps, Waypoint, Driver, Transaction, Order,
    nvDateTimeUtils, nvTimezone, nvTranslate, $timeout, nvAddress
  ) {
    const self = {
      isDebug: function isDebug() {
        return false;
      },
      initializeVariables: function initializeVariables() {
        // main vars
        self.routeTags = [];
        self.hubs = [];
        self.routeGroups = [];
        self.driverTypes = [];

        // assignment vars
        self.contentHeight = 0;
        self.zonesResult = [];
        self.driversResult = null;
        self.driverTemporaryZonesResult = null;
        self.zoneTransactionsResult = null;
        self.currentRoutesResult = null;

        self.isEditFetchTransactionsSetting = true;
        self.fetchTransactionsMode = '10';
        self.routeDate = new Date();
        self.orderCreationDate = new Date();
        self.routeGroupIds = [];
        self.zoneIds = [];
        self.zones = [];
        self.zonesTableParams = null;
        self.driversByZones = [];
        self.driversByZonesTable = {
          filteredDriversByZones: [],
          dndAllowedTypes: [self.getDndDriverFromZoneAssignment()],
          disableIf: self.driversByZonesDisableIf,
        };
        self.driversTable = {
          all: [], // all drivers
          available: [], // all drivers (that are not in route)
          dndAllowedTypes: [self.getDndDriverFromZoneAssignment()],
          filteredDrivers: [], // all drivers from self.driversByZonesTable.filteredDriversByZones
          filter: {
            searchText: '',
            filterFunctions: [
              self.filterByDriverId,
              self.filterByDriverName,
              self.filterByDriverType,
              self.filterByDriverZoneName,
            ],
            onChange: self.driversTableKeywordFiltering,
          },
          currSortIdx: 0,
          currSortOpt: null,
          sortByTypes: [undefined, 'asc', 'desc'], // no-sorting, ascending, descending
          sortOptions: {
            byZone: self.sortDriversByZone,
            byName: self.sortDriversByName,
            byVehicleCapacity: self.sortDriversByVehicleCapacity,
          },
          isDriversSortActive: self.isDriversSortActive,
          showDriversSortIcon: self.showDriversSortIcon,
          driversSort: self.driversSort,
        }; // listing in driver list -> drivers table
        self.driverPreferredZonesSelection = {
          displayName: 'container.zonal-routing.driver-preferred-zones',
          value: [],
          options: [],
        };
        self.hubsSelection = {
          displayName: 'commons.hub',
          value: [],
          options: [],
        };
        self.driverTypesSelection = {
          displayName: 'container.zonal-routing.driver-types',
          value: [],
          options: [],
        };
        self.showRFDriverCheckbox = {
          displayName: 'container.zonal-routing.show-rf-drivers',
          value: true,
        };
        self.unassignedDriversFilter = {
          zoneIds: [],
          hubIds: [],
          driverTypeIds: [],
          showRFDriver: true,
        };
        self.zoneTransactions = null;
        self.driversWithTemporaryZones = [];
        self.originalDriversWithTemporaryZones = [];
        self.currentRoutes = [];
        self.chosenZone = {};
        self.waypointTransactionsToRoute = {};
        self.algoRouteAssignments = [];

        // routing vars
        self.MAP_VIEW_OPTIONS = {
          ALL: 'container.zonal-routing.all-types',
          ROUTED: 'container.zonal-routing.routed',
          UNROUTED: 'container.zonal-routing.unrouted',
        };
        self.POLYGON_TOOLS_APPLY_ACTION = {
          ACTIVE_ROUTE: 'container.zonal-routing.active-route',
          ALL_POLYGONS: 'container.zonal-routing.all-polygons',
        };
        self.POLYGON_TOOLS_ADD_ACTION = {
          UNROUTED: 'container.zonal-routing.unrouted-waypoints',
          UNROUTED_PRIORITY: 'container.zonal-routing.unrouted-priority',
          UNROUTED_NON_PRIORITY: 'container.zonal-routing.unrouted-non-priority',
          ALL: 'container.zonal-routing.all-waypoints',
        };
        self.POLYGON_TOOLS_REMOVE_ACTION = {
          ROUTED: 'container.zonal-routing.all-under-this-route',
          ROUTED_PRIORITY: 'commons.priority',
          ROUTED_NON_PRIORITY: 'commons.non-priority',
        };
        self.ALL_ROUTES_PANEL_ACTION = {
          SET_HUB_FOR_ROUTES: 'container.zonal-routing.set-hub-for-routes',
          PURGE_ROUTES: 'container.zonal-routing.purge-routes',
          DELETE_ROUTES: 'container.zonal-routing.delete-routes',
        };
        self.ROUTE_PANEL_ACTION = {
          PURGE_ROUTE: 'container.zonal-routing.purge-route',
          DELETE_ROUTE: 'container.zonal-routing.delete-route',
        };
        self.MAP_CONTROL_POSITION = 'topright';
        self.clusterGroup = null;
        self.colors = [
          { primary: '#DA6037', secondary: '#e9a087' },
          { primary: '#8D8C14', secondary: '#bbba72' },
          { primary: '#616CA7', secondary: '#a0a7ca' },
          { primary: '#3CB00B', secondary: '#8ad06d' },
          { primary: '#CF64A7', secondary: '#e2a2ca' },
          { primary: '#89421F', secondary: '#b88e79' },
          { primary: '#279541', secondary: '#7dbf8d' },
          { primary: '#594673', secondary: '#9b90ab' },
          { primary: '#DC506A', secondary: '#ea96a6' },
          { primary: '#365218', secondary: '#869774' },
          { primary: '#C67B1E', secondary: '#ddb078' },
          { primary: '#AE67E8', secondary: '#cea4f1' },
          { primary: '#3B79BE', secondary: '#89afd8' },
          { primary: '#8B3350', secondary: '#b98596' },
          { primary: '#59791D', secondary: '#9baf77' },
        ];
        self.routingFilters = {};
        self.assignedRoutes = [];
        self.routesSubmitted = false;
      },
      getAllZonesId: function getAllZonesId() {
        return 2;
      },
      getNullZoneData: function getNullZoneData() {
        return { id: -1, name: 'No Zone', hubId: null };
      },
      getUnavailableZoneData: function getUnavailableZoneData() {
        return { id: -2, name: 'Unavailable', hubId: null };
      },
      getReservationZoneData: function getReservationZoneData() {
        return {
          id: -3, // fake zone id
          name: 'Reservations | Returns | RTS',
          hubId: null,
          zoneId: 3, // real zone id to use when submitting route
        };
      },
      getDndDriverFromDriverList: function getDndDriverFromDriverList() {
        return 'driver-from-driver-list-table';
      },
      getDndDriversByZone: function getDndDriversByZone() {
        return 'drivers-by-zone';
      },
      getDndDriverFromZoneAssignment: function getDndDriverFromZoneAssignment() {
        return 'driver-from-zone-assignment-table';
      },

      // assignment page
      getDriverByZoneLabel: function getDriverByZoneLabel(zoneObj) {
        return `${"<div class='driver-by-zone-item-label'>" +
                    "<span class='zone-name nv-text-ellipsis nv-text-left'>"}${zoneObj.name}</span>` +
                      '<span class=\'driver-count nv-text-ellipsis nv-text-center\'>' +
                      '<i class=\'fa fa-user\'></i>' +
                      `<span>${zoneObj.count}</span>` +
                    '</span>' +
                    '<span class=\'max-capacity nv-text-ellipsis nv-text-center\'>' +
                      '<i class=\'material-icons\'>tab</i>' +
                      `<span>${zoneObj.max}</span>` +
                    '</span>' +
                  '</div>';
      },
      addDriverToZone: function addDriverToZone(driverId, zoneId) {
        const driver = self.getDriverData(driverId);
        if (!driver) {
          return;
        }

        driver.dndType = self.getDndDriverFromZoneAssignment();
        driver.dndSelected = false;

        // handle drivers in self.zones
        const zone = self.getZoneData(zoneId);
        const driverFound = _.find(zone.drivers, ['id', driver.id]);
        if (!_.isUndefined(driverFound)) {
          return;
        }

        zone.drivers.push(driver);

        // handle self.driversByZones
        self.removeDriverFromDriversByZones(driver);
      },
      addDriverToDriversByZones: function addDriverToDriversByZones(
        driver, proceedIfAssigned = true
      ) {
        const zone = self.getAssignedZoneForDriver(driver);
        if (zone && !proceedIfAssigned) {
          return;
        } else if (zone && proceedIfAssigned) {
          // remove driver from zone before add to driversByZones
          _.remove(zone.drivers, ['id', driver.id]);
        }

        const zoneId = self.getDriverZoneId(driver);

        if (isRFDriverFilter(driver) && isHubFilter(driver) && isDriverTypeFilter(driver)) {
          driver.dndType = self.getDndDriverFromDriverList();
          driver.dndSelected = false;

          const driversByZone = self.getDriversByZone(zoneId);
          driversByZone.drivers.push(driver);
          driversByZone.count += 1;
          driversByZone.min += driver.min;
          driversByZone.max += driver.max;
          driversByZone.label = self.getDriverByZoneLabel(driversByZone);
        }

        function isRFDriverFilter(theDriver) {
          return self.showRFDriverCheckbox.value ||
            (!self.showRFDriverCheckbox.value && !Driver.isReserveFleetDriver(theDriver));
        }

        function isHubFilter(theDriver) {
          return _.size(self.hubsSelection.value) <= 0 ||
            (_.indexOf(self.hubsSelection.value, theDriver.hubId) >= 0);
        }

        function isDriverTypeFilter(theDriver) {
          return _.size(self.driverTypesSelection.value) <= 0 ||
            (_.indexOf(self.driverTypesSelection.value, theDriver.driverTypeId) >= 0);
        }
      },
      addDriverToDriversByZonesImproved: (
        driver, proceedIfAssigned = true
      ) => {
        const zone = self.getAssignedZoneForDriver(driver);
        if (zone && !proceedIfAssigned) {
          return;
        } else if (zone && proceedIfAssigned) {
          // remove driver from zone before add to driversByZones
          _.remove(zone.drivers, ['id', driver.id]);
        }

        const zoneId = self.getDriverZoneId(driver);

        if (isRFDriverFilter(driver) && isHubFilter(driver) && isDriverTypeFilter(driver)) {
          driver.dndType = self.getDndDriverFromDriverList();
          driver.dndSelected = false;

          const driversByZone = self.getDriversByZone(zoneId);
          driversByZone.drivers.push(driver);
          driversByZone.count += 1;
          driversByZone.min += driver.min;
          driversByZone.max += driver.max;
          driversByZone.label = self.getDriverByZoneLabel(driversByZone);
        }

        function isRFDriverFilter(theDriver) {
          return self.unassignedDriversFilter.showRFDriver ||
            (!self.unassignedDriversFilter.showRFDriver && !Driver.isReserveFleetDriver(theDriver));
        }

        function isHubFilter(theDriver) {
          return _.size(self.unassignedDriversFilter.hubIds) <= 0 ||
            (_.indexOf(self.unassignedDriversFilter.hubIds, theDriver.hubId) >= 0);
        }

        function isDriverTypeFilter(theDriver) {
          return _.size(self.unassignedDriversFilter.driverTypeIds) <= 0 ||
            (_.indexOf(self.unassignedDriversFilter.driverTypeIds, theDriver.driverTypeId) >= 0);
        }
      },
      removeDriverFromDriversByZones: function removeDriverFromDriversByZones(driver) {
        const zoneId = self.getDriverZoneId(driver);

        const driversByZone = self.getDriversByZone(zoneId);
        _.forEach(driversByZone.drivers, (driverRow, index) => {
          if (driverRow.id === driver.id) {
            driversByZone.drivers.splice(index, 1);
            return false;
          }

          return undefined;
        });

        driversByZone.count -= 1;
        driversByZone.min -= driver.min;
        driversByZone.max -= driver.max;
        driversByZone.label = self.getDriverByZoneLabel(driversByZone);
      },
      getAssignedZoneForDriver: function getAssignedZoneForDriver(driver) {
        let theZone = null;
        _.forEach(self.zones, (zone) => {
          if (theZone !== null) {
            return false;
          }

          _.forEach(zone.drivers || [], (driverRow) => {
            if (driverRow.id === driver.id) {
              theZone = zone;
              return false;
            }

            return undefined;
          });

          return undefined;
        });

        return theZone;
      },
      isAssignedToZone: function isAssignedToZone(driver) {
        const zone = self.getAssignedZoneForDriver(driver);
        return !!zone;
      },
      driversByZonesDisableIf: function driversByZonesDisableIf(item) {
        return item.count <= 0;
      },
      filterDriversTable: function filterDriversTable(enableSearchTextFiltering = true) {
        let drivers = [];

        _.forEach(self.driversByZonesTable.filteredDriversByZones, (driversByZone) => {
          drivers = drivers.concat(driversByZone.drivers);
        });

        if (enableSearchTextFiltering && self.driversTable.filter.searchText) {
          drivers = _.filter(drivers, driver =>
            _.some(self.driversTable.filter.filterFunctions, filter =>
              filter.call(null, self.driversTable.filter.searchText, driver)
            )
          );
        }

        return self.sortDriversTableData(drivers);
      },
      driversTableKeywordFiltering: function driversTableKeywordFiltering() {
        $timeout(() => {
          self.driversTable.filteredDrivers = self.filterDriversTable();
        });
      },
      filterByDriverId: function filterByDriverId(searchText, driver) {
        return driver.id && driver.id.toString().indexOf(
            _.toString(searchText).toLowerCase()
          ) >= 0;
      },
      filterByDriverName: function filterByDriverName(searchText, driver) {
        return driver.name && driver.name.toLowerCase().indexOf(
            _.toString(searchText).toLowerCase()
          ) >= 0;
      },
      filterByDriverType: function filterByDriverType(searchText, driver) {
        return driver.driverType && driver.driverType.toLowerCase().indexOf(
            _.toString(searchText).toLowerCase()
          ) >= 0;
      },
      filterByDriverZoneName: function filterByDriverZoneName(searchText, driver) {
        return driver.zoneName && driver.zoneName.toLowerCase().indexOf(
            _.toString(searchText).toLowerCase()
          ) >= 0;
      },
      sortDriversByZone: function sortDriversByZone(tableData, orderBy) {
        return _.orderBy(tableData, 'zoneName', orderBy);
      },
      sortDriversByName: function sortDriversByName(tableData, orderBy) {
        return _.orderBy(tableData, ['name', 'id'], [orderBy, 'asc']);
      },
      sortDriversByVehicleCapacity: function sortDriversByVehicleCapacity(tableData, orderBy) {
        return _.orderBy(tableData, ['vehicleCapacity', 'id'], [orderBy, 'asc']);
      },
      isDriversSortActive: function isDriversSortActive(option) {
        return self.driversTable.currSortOpt === option && self.driversTable.currSortIdx > 0;
      },
      showDriversSortIcon: function showDriversSortIcon(option) {
        if (self.driversTable.currSortOpt === option) {
          return (self.driversTable.currSortIdx === 1 ? 'fa-sort-asc' : 'fa-sort-desc');
        }
        return 'fa-sort';
      },
      driversSort: function driversSort(option) {
        if (self.driversTable.currSortOpt === option) {
          self.driversTable.currSortIdx = (self.driversTable.currSortIdx + 1) % 3;
        } else {
          self.driversTable.currSortIdx = 1;
        }

        self.driversTable.currSortOpt = self.driversTable.currSortIdx === 0 ? null : option;
        self.driversTable.filteredDrivers = self.sortDriversTableData(
          self.driversTable.filteredDrivers
        );
      },
      sortDriversTableData: function sortDriversTableData(tableData) {
        return !self.driversTable.currSortOpt
          ? _.sortBy(tableData, 'id')
          : self.driversTable.sortOptions[self.driversTable.currSortOpt](
          tableData, self.driversTable.sortByTypes[self.driversTable.currSortIdx]
        );
      },
      getMaxBuffer: function getMaxBuffer(zone) {
        return zone.nonPriority;
      },
      isFetchByRouteGroup: function isFetchByRouteGroup() {
        return self.fetchTransactionsMode === '01';
      },
      getRouteGroupNames: function getRouteGroupNames() {
        const names = _.map(self.routeGroupIds, (id) => {
          const routeGroup = _.find(self.routeGroups, ['id', id]);
          return routeGroup ? routeGroup.name : null;
        });

        return _.compact(names).join(', ') || '-';
      },

      // routing page
      getClusterRadius: function getClusterRadius() {
        return 60;
      },
      updateClustering: function updateClustering(map, zoomLevel) {
        const theClusterGroup = new L.MarkerClusterGroup({
          disableClusteringAtZoom: zoomLevel,
          maxClusterRadius: self.getClusterRadius(),
        });

        if (self.clusterGroup) {
          map.addLayer(theClusterGroup);
          map.removeLayer(self.clusterGroup);

          _.forEach(self.clusterGroup.getLayers(), (layer) => {
            theClusterGroup.addLayer(layer);
          });
        }

        self.clusterGroup = theClusterGroup;
        map.addLayer(self.clusterGroup);
      },
      addClusterGroupControl: function addClusterGroupControl(map, scope, compile) {
        L.Control.ClusterGroup = L.Control.extend({
          onAdd: function onAdd() {
            const container = L.DomUtil.create('div', 'leaflet-bar leaflet-control leaflet-control-cluster-group');

            // === show-clusters button ===
            const showClustersBtn = L.DomUtil.create('a', 'show-clusters-btn active', container);
            L.DomUtil.create('i', 'fa fa-bullseye', showClustersBtn);

            // tooltip
            const showClustersTooltip = L.DomUtil.create('md-tooltip', '', showClustersBtn);
            showClustersTooltip.setAttribute('md-direction', 'right');
            showClustersTooltip.insertAdjacentHTML(
              'beforeend',
              nvTranslate.instant('container.zonal-routing.show-clusters')
            );
            compile(showClustersTooltip)(scope);

            // === show-waypoints button ===
            const showWaypointsBtn = L.DomUtil.create('a', 'show-waypoints-btn', container);
            L.DomUtil.create('i', 'fa fa-map-marker', showWaypointsBtn);

            // tooltip
            const showWaypointsTooltip = L.DomUtil.create('md-tooltip', '', showWaypointsBtn);
            showWaypointsTooltip.setAttribute('md-direction', 'right');
            showWaypointsTooltip.insertAdjacentHTML(
              'beforeend',
              nvTranslate.instant('container.zonal-routing.show-waypoints')
            );
            compile(showWaypointsTooltip)(scope);

            // === btns onclick event ===
            showClustersBtn.onclick = function onclick() {
              L.DomUtil.addClass(showClustersBtn, 'active');
              L.DomUtil.removeClass(showWaypointsBtn, 'active');

              self.updateClustering(map, 0);
            };

            showWaypointsBtn.onclick = function onclick() {
              L.DomUtil.addClass(showWaypointsBtn, 'active');
              L.DomUtil.removeClass(showClustersBtn, 'active');

              self.updateClustering(map, map.getZoom());
            };

            return container;
          },
        });

        L.control.clusterGroup = function clusterGroupControl(opts) {
          return new L.Control.ClusterGroup(opts);
        };
        L.control.clusterGroup({ position: self.MAP_CONTROL_POSITION }).addTo(map);
      },
      toggleVisibilityMarker: function toggleVisibilityMarker(marker, isVisible) {
        const opacityValue = isVisible ? 1 : 0;

        if (self.clusterGroup) {
          if (isVisible) {
            self.clusterGroup.addLayer(marker);
          } else {
            self.clusterGroup.removeLayer(marker);
          }
        }

        marker.setOpacity(opacityValue);
      },
      discardRoutingPageChanges: function discardRoutingPageChanges() {
        self.chosenZone = {};
        self.waypointTransactionsToRoute = {};
        self.algoRouteAssignments = [];

        self.clusterGroup = null;
        self.routingFilters = {};
        self.assignedRoutes = [];

        const reservationZone = self.getZoneData(self.getReservationZoneData().id);
        if (reservationZone) {
          reservationZone.drivers = [];
        }
      },
      getAssignedRouteData: function getAssignedRouteData(routeId) {
        return _.find(self.assignedRoutes, ['id', routeId]);
      },
      getRouteName: function getRouteName(routeId) {
        return `R${routeId}`;
      },
      getChosenZoneDriverMaxCapacity: function getChosenZoneDriverMaxCapacity(driverId) {
        const driver = self.getChosenZoneDriverData(driverId);

        if (driver) {
          return driver.max;
        }

        return null;
      },
      getChosenZoneDriverName: function getChosenZoneDriverName(driverId) {
        const driver = self.getChosenZoneDriverData(driverId);

        if (driver) {
          return driver.name;
        }

        return null;
      },
      getHubName: function getHubName(hubId) {
        const hub = self.getHubData(hubId);

        if (hub) {
          return hub.name;
        }

        return null;
      },

      // helper function
      getDriverZoneId: function getDriverZoneId(driver) {
        if (!driver.availability) {
          return self.getUnavailableZoneData().id;
        }
        return driver.zoneId;
      },
      getDriverData: function getDriverData(driverId) {
        return _.find(self.driversTable.all, ['id', +driverId]);
      },
      getZoneData: function getZoneData(zoneId) {
        return _.find(self.zones, ['id', +zoneId]);
      },
      getDriversByZone: function getDriversByZone(zoneId) {
        return _.find(self.driversByZones, ['id', +zoneId]);
      },
      getChosenZoneDriverData: function getChosenZoneDriverData(driverId) {
        return _.find(self.chosenZone.drivers, ['id', driverId]);
      },
      getHubData: function getHubData(hubId) {
        return _.find(self.hubs, ['id', hubId]);
      },
      isDPPickup: function isDPPickup(transactions) {
        let isDPPickupBool = true;
        _.forEach(transactions, (transaction) => {
          if (!(transaction.distributionPointId > 0 &&
            Transaction.isPickup(transaction)
            && !transaction.transit)
          ) {
            isDPPickupBool = false;
            return false;
          }

          return undefined;
        });

        return isDPPickupBool;
      },
      addWaypointMarker: function addWaypointMarker(map, waypoint, transactions, toCluster = true) {
        const options = {
          id: waypoint.id,
          latitude: waypoint.latitude,
          longitude: waypoint.longitude,
          draggable: false,
          zIndex: 1000,
          opacity: 1,
          transactions: transactions,
          waypoint: waypoint,
          extraInfo: {}, // mostly contain useful data for filtering use
        };

        let marker;
        if (toCluster) {
          marker = nvMaps.addMarker(self.clusterGroup, options);
        } else {
          marker = nvMaps.addMarker(map, options);
        }
        marker.options.extraInfo = generateExtraInfo(marker);
        self.setWaypointMarkerIcon(marker);

        return marker;

        function generateExtraInfo(theMarker) {
          let hasBulkMoveQty = false;
          let hasC2C = false;
          let hasCOD = false;
          let hasDP = false;
          let hasDPReservation = false;
          let hasReschedule = false;
          let hasReturnPickup = false;
          let hasRouteComments = false;
          let hasSameday = false;
          let isPriority = false;
          let priorityLevel = 0;
          const isReservation = Waypoint.isReservation(theMarker.options.waypoint);

          const routeDate = moment(self.routeDate).startOf('day');
          _.forEach(theMarker.options.transactions, (transaction) => {
            // ========== vars ==========
            const isoTxnEndTime = nvDateTimeUtils.displayISO(
              new Date(transaction.endTime), nvTimezone.getOperatorTimezone()
            );
            const txnEndDate = moment(isoTxnEndTime).startOf('day');

            // ===== start checking =====
            if (Order.isTypeEnum(transaction.order, Order.TYPE.C2C)) {
              hasC2C = true;
            }

            // if txn end date is before the route date, then is priority
            // (compare to date only, not datetime)
            if (nvDateTimeUtils.dayBetweenDates(txnEndDate, routeDate) <= 0) {
              isPriority = true;
            }

            if (transaction.rescheduled) {
              hasReschedule = true;
            }

            if (Transaction.isPickup(transaction) &&
              Order.isTypeEnum(transaction.order, Order.TYPE.RETURN)) {
              hasReturnPickup = true;
            }

            if (transaction.distributionPointId > 0) {
              hasDP = true;
            }

            if (isReservation && transaction.distributionPointId > 0) {
              hasDPReservation = true;
            }

            if (transaction.order && transaction.order.bulkMoveQty > 0) {
              hasBulkMoveQty = true;
            }

            if (transaction.order && _.size(transaction.order.cod) > 0) {
              hasCOD = true;
            }

            if (transaction.routeComments) {
              hasRouteComments = true;
            }

            if (!isReservation && (Order.isDeliverySameDay(transaction.order) ||
              Order.isC2CSameDay(transaction.order))) {
              hasSameday = true;
            }

            // get highest priority level in transactions
            if (transaction.priorityLevel && priorityLevel < transaction.priorityLevel) {
              priorityLevel = transaction.priorityLevel;
            }
          });

          return {
            isHideWhenOutsideBound: false,
            isHideByMapViewSettings: false,
            isHideByMapViewOption: false,
            address: nvAddress.extract(theMarker.options.waypoint),
            isPriority: isPriority,
            isReservation: isReservation,
            hasReschedule: hasReschedule,
            hasReturnPickup: hasReturnPickup,
            hasBulkMoveQty: hasBulkMoveQty,
            hasRouteComments: hasRouteComments,
            hasCOD: hasCOD,
            hasC2C: hasC2C,
            hasDP: hasDP,
            hasDPReservation: hasDPReservation,
            hasSameday: hasSameday,
            priorityLevel: priorityLevel,
            timewindowId: +waypoint.timewindowId,
          };
        }
      },
      setWaypointMarkerIcon: function setWaypointMarkerIcon(marker) {
        const timewindowsColorMap = {
          '-3': '000000', // black
          '-2': 'ffffff', // white
          '-1': 'acacac', // grey
          0: 'cf0000', // red
          1: 'f2ff5e', // yellow
          2: '16ff0a', // green
          3: '2a1fff', // blue
        };

        const color = timewindowsColorMap[marker.options.extraInfo.timewindowId];
        let config = {};
        if (marker.options.extraInfo.isReservation) {
          config = {
            type: 'd_map_xpin_icon',
            style: 'pin_star',
            icon: 'parking',
            fillColor: color,
            starFillColor: '1ff7ff',
          };
        } else if (marker.options.extraInfo.isPriority) {
          if (marker.options.extraInfo.hasReschedule) {
            config = {
              type: 'd_map_xpin_icon',
              style: 'pin_star',
              icon: 'shoppingbag',
              fillColor: color,
              starFillColor: '1ff7ff',
            };
          } else if (marker.options.extraInfo.hasReturnPickup) {
            config = {
              type: 'd_map_xpin_icon',
              style: 'pin_star',
              icon: 'repair',
              fillColor: color,
              starFillColor: '1ff7ff',
            };
          } else if (marker.options.extraInfo.hasBulkMoveQty) {
            config = {
              type: 'd_map_xpin_icon',
              style: 'pin_star',
              icon: 'truck',
              fillColor: color,
              starFillColor: '1ff7ff',
            };
          } else if (marker.options.extraInfo.hasRouteComments) {
            config = {
              type: 'd_map_xpin_icon',
              style: 'pin_star',
              icon: 'caution',
              fillColor: color,
              starFillColor: '1ff7ff',
            };
          } else if (marker.options.extraInfo.hasCOD) {
            config = {
              type: 'd_map_xpin_icon',
              style: 'pin_star',
              icon: 'bank-dollar',
              fillColor: color,
              starFillColor: '1ff7ff',
            };
          } else if (marker.options.extraInfo.hasC2C) {
            config = {
              type: 'd_map_xpin_letter',
              style: 'pin_star',
              text: 'C',
              fillColor: color,
              starFillColor: '1ff7ff',
            };
          } else if (marker.options.extraInfo.hasDP) {
            config = {
              type: 'd_map_xpin_letter',
              style: 'pin_star',
              text: 'D',
              fillColor: color,
              starFillColor: '1ff7ff',
            };
          } else if (marker.options.extraInfo.hasSameday) {
            config = {
              type: 'd_map_xpin_letter',
              style: 'pin_star',
              text: 'SD',
              fillColor: color,
              starFillColor: '1ff7ff',
            };
          } else {
            config = {
              type: 'd_map_xpin_letter',
              style: 'pin_star',
              text: '',
              fillColor: color,
              starFillColor: '1ff7ff',
            };
          }
        } else if (!marker.options.extraInfo.isPriority) {
          if (marker.options.extraInfo.hasReschedule) {
            config = {
              type: 'd_map_xpin_icon',
              style: 'pin',
              icon: 'shoppingbag',
              fillColor: color,
            };
          } else if (marker.options.extraInfo.hasReturnPickup) {
            config = {
              type: 'd_map_xpin_icon',
              style: 'pin',
              icon: 'repair',
              fillColor: color,
            };
          } else if (marker.options.extraInfo.hasBulkMoveQty) {
            config = {
              type: 'd_map_xpin_icon',
              style: 'pin',
              icon: 'truck',
              fillColor: color,
            };
          } else if (marker.options.extraInfo.hasRouteComments) {
            config = {
              type: 'd_map_xpin_icon',
              style: 'pin',
              icon: 'caution',
              fillColor: color,
            };
          } else if (marker.options.extraInfo.hasCOD) {
            config = {
              type: 'd_map_xpin_icon',
              style: 'pin',
              icon: 'bank-dollar',
              fillColor: color,
            };
          } else if (marker.options.extraInfo.hasC2C) {
            config = {
              type: 'd_map_xpin_letter',
              style: 'pin',
              text: 'C',
              fillColor: color,
            };
          } else if (marker.options.extraInfo.hasDP) {
            config = {
              type: 'd_map_xpin_letter',
              style: 'pin',
              text: 'D',
              fillColor: color,
            };
          } else if (marker.options.extraInfo.hasSameday) {
            config = {
              type: 'd_map_xpin_letter',
              style: 'pin',
              text: 'SD',
              fillColor: color,
            };
          } else {
            config = {
              type: 'd_map_xpin_letter',
              style: 'pin',
              text: '',
              fillColor: color,
            };
          }
        }

        nvMaps.markerSetIcon(marker, nvMaps.getIconData(config));
        nvMaps.markerSetZIndex(marker, getZIndex(marker));

        function getZIndex(theMarker) {
          if (theMarker.options.extraInfo.isPriority) {
            if (theMarker.options.extraInfo.hasRouteComments) {
              return 29;
            } else if (theMarker.options.extraInfo.hasReschedule) {
              return 28;
            } else if (theMarker.options.extraInfo.isReservation) {
              return 27;
            } else if (theMarker.options.extraInfo.timewindowId === 0) {
              return 26;
            } else if (theMarker.options.extraInfo.timewindowId === 1) {
              return 25;
            } else if (theMarker.options.extraInfo.timewindowId === 2) {
              return 24;
            } else if (theMarker.options.extraInfo.timewindowId === 3) {
              return 23;
            } else if (theMarker.options.extraInfo.timewindowId === -2) {
              return 22;
            } else if (theMarker.options.extraInfo.timewindowId === -3) {
              return 21;
            }
            return 20;
          }

          // non-priority
          if (theMarker.options.extraInfo.hasRouteComments) {
            return 19;
          } else if (theMarker.options.extraInfo.hasReschedule) {
            return 18;
          } else if (theMarker.options.extraInfo.isReservation) {
            return 17;
          } else if (theMarker.options.extraInfo.timewindowId === 0) {
            return 16;
          } else if (theMarker.options.extraInfo.timewindowId === 1) {
            return 15;
          } else if (theMarker.options.extraInfo.timewindowId === 2) {
            return 14;
          } else if (theMarker.options.extraInfo.timewindowId === 3) {
            return 13;
          } else if (theMarker.options.extraInfo.timewindowId === -2) {
            return 12;
          } else if (theMarker.options.extraInfo.timewindowId === -3) {
            return 11;
          }
          return 10;
        }
      },
      setRoutedWaypointMarkerIcon: function setRoutedWaypointMarkerIcon(marker, route) {
        // substring to remove hash
        let color = route.color.primary.substring(1);
        if (!marker.options.extraInfo.isPriority) {
          color = route.color.secondary.substring(1);
        }

        const config = {
          type: 'd_map_spin',
          fillColor: _.toLower(color),
          text: route.seqNo > 100 ? '' : route.seqNo,
          size: [28, 48],
        };

        nvMaps.markerSetIcon(marker, nvMaps.getIconData(config));
      },
      setHoveredWaypointMarkerIcon: function setHoveredWaypointMarkerIcon(marker) {
        const config = {
          type: 'd_map_xpin_letter',
          style: 'pin_sleft',
          fillColor: '1ff7ff',
        };
        nvMaps.markerSetIcon(marker, nvMaps.getIconData(config));
      },
      getRouteColor: function getRouteColor(routeId) {
        return self.colors[(routeId - 1) % self.colors.length];
      },
      spiderfy: function spiderfy(map) {
        return new OverlappingMarkerSpiderfier(map, {
          markersWontMove: true,
          markersWontHide: true,
          keepSpiderfied: true,
        });
      },
      inPolygon: function inPolygon(polygonLatLngs, latLng) {
        const x = latLng.lat;
        const y = latLng.lng;

        let inside = false;
        for (let i = 0, j = polygonLatLngs.length - 1; i < polygonLatLngs.length; j = i++) {
          const xi = polygonLatLngs[i].lat;
          const yi = polygonLatLngs[i].lng;
          const xj = polygonLatLngs[j].lat;
          const yj = polygonLatLngs[j].lng;

          const intersect = ((yi > y) !== (yj > y))
            && (x < (((xj - xi) * (y - yi)) / (yj - yi)) + xi);
          if (intersect) {
            inside = !inside;
          }
        }

        return inside;
      },
    };

    return self;
  }
})());
