((function ZonesService() {
  angular.module('nvOperatorApp.services')
        .factory('nvZonesService', nvZonesService);

  nvZonesService.$inject = ['nvMaps'];

  function nvZonesService(nvMaps) {
    const self = {
      initializeVariables: function initializeVariables() {
        self.zones = [];
        self.hubs = [];

        self.contentHeight = 0;
        self.selectedZones = [];
      },
      addToSelectedZones: function addToSelectedZones(zones) {
        self.selectedZones = _.concat(self.selectedZones, zones);
      },
      clearSelectedZones: function clearSelectedZones(){
        self.selectedZones.length = 0 ;
      },

    };

    return self;
  }
})());
