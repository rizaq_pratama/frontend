;(function() {
  'use strict';

  angular
    .module('nvOperatorApp.services')
    .factory('nvReports', nvReports);

  nvReports.$inject = ['nvMigrationService', 'nvRequestUtils']

  function nvReports(nvMigrationService, nvRequestUtils) {
    const { Reports } = nvMigrationService;
    Reports.BASE_URL = nv.config.servers.reports;

    const apis = _.pick(Reports, [
      'subscribeCODReport',
      'subscribeCODDriverReport',
      'subscribeDriverReport',
      'subscribeRouteCleanParcel',
      'subscribeRouteCleanReservation',
      'subscribeRouteCleanCOD',
    ])

    const self = new Proxy(apis, {
      get(obj, key) {
        const fn = obj[key];
        if (fn) {
          return function () {
            // promise is resolved with a apisauce response (always resolved)
            return fn.call(null, ...arguments)
              .then(data => {
                if (data.status >= 400) {
                  nvRequestUtils.nvFailure(data)
                }
                return data;
              })
          }
        }
      }
    })
    return self;
  }
})();

