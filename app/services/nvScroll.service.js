(function() {
    'use strict';

    /**
     * Read docs: https://github.com/oblador/angular-scroll
     * This service returns the scroll container that allows scroll, 
     * use nvScroll instead of $document as mentioned in the documentation.
     *
     * Some example:
     * nvScroll.scrollTo( left, top [, duration [, easing]])
     * nvScroll.scrollToElement( element [, offset, [, duration [, easing]]])
     * nvScroll.scrollToElementAnimated( element [, offset, [, duration [, easing]]])
     */

    angular
        .module('nvOperatorApp.services')
        .factory('nvScroll', nvScroll);

    nvScroll.$inject = [];

    function nvScroll() {
        return angular.element($('.view-container > md-content[du-scroll-container]'));
    }

})();
