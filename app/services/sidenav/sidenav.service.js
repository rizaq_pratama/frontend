(function() {
    'use strict';

    angular
        .module('nvOperatorApp.services')
        .factory('appSidenav', sidenav);

    sidenav.$inject = ['$timeout', '$mdSidenav', 'nvClientStore', 'nvSectionService'];

    function sidenav($timeout, $mdSidenav, nvClientStore, nvSectionService) {

        // these are the ids assigned to md-component-id on <md-sidenav></md-sidenav>
        var componentIdMainMenu = 'sidenav-main-menu',
            componentIdDriverMessaging = 'sidenav-driver-messaging';

        // we need to track the ready state ourselves
        var isReadyState = {};
        isReadyState[componentIdMainMenu] = false;
        isReadyState[componentIdDriverMessaging] = false;

        var localStorageKey = 'isSidenavLeftMenuLockedOpen',
            isConfigLockedOpen = nvClientStore.localStorage.getJson(localStorageKey);

        return {

            init: {
                mainMenu: _.partial(init, componentIdMainMenu),
                driverMessaging: _.partial(init, componentIdDriverMessaging)
            },

            isOpen: {
                mainMenu: _.partial(isOpen, componentIdMainMenu),
                driverMessaging: _.partial(isOpen, componentIdDriverMessaging)
            },

            open: {
                mainMenu: _.partial(openComponent, componentIdMainMenu),
                driverMessaging: _.partial(openComponent, componentIdDriverMessaging)
            },

            close: {
                mainMenu: _.partial(closeComponent, componentIdMainMenu),
                driverMessaging: _.partial(closeComponent, componentIdDriverMessaging)
            },

            toggle: {
                mainMenu: _.partial(toggle, componentIdMainMenu),
                driverMessaging: _.partial(toggle, componentIdDriverMessaging)
            },

            isReady: {
                mainMenu: _.partial(isReady, componentIdMainMenu),
                driverMessaging: _.partial(isReady, componentIdDriverMessaging)
            },

            isLockedOpen: {
                mainMenu: _.partial(isLockedOpen, componentIdMainMenu),
                driverMessaging: _.partial(isLockedOpen, componentIdDriverMessaging)
            },

            // todo - refactor
            setConfigLockedOpen: function(isLockedOpen) {
                isConfigLockedOpen = isLockedOpen;
                if (isLockedOpen) {
                    nvClientStore.localStorage.setJson(localStorageKey, true);
                } else {
                    nvClientStore.localStorage.remove(localStorageKey, true);
                }
            },

            // todo - refactor
            isConfigLockedOpen: function() {
                return isConfigLockedOpen;
            },

            mainMenuSection: nvSectionService,

        };

        function init(componentId) {
            return $mdSidenav(componentId).then(function() {
                $timeout(function() {
                    isReadyState[componentId] = true;
                });
            });
        }

        function isOpen(componentId) {
            return isReadyState[componentId] && $mdSidenav(componentId).isOpen();
        }

        function openComponent(componentId) {
            return $mdSidenav(componentId).open();
        }

        function closeComponent(componentId) {
            return $mdSidenav(componentId).close();
        }

        function toggle(componentId) {
            return $mdSidenav(componentId).toggle();
        }

        function isReady(componentId) {
            return isReadyState[componentId];
        }

        function isLockedOpen(componentId) {
            return isReadyState[componentId] && $mdSidenav(componentId).isLockedOpen();
        }

    }

})();
