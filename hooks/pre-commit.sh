#!/bin/sh

files=$(git diff --cached --name-only --diff-filter=ACM | grep "\.js$")
if [ "$files" = "" ]; then
    exit 0
fi

pass=true

echo "\nValidating JavaScript:\n"

for file in ${files}; do
    result=$(./node_modules/.bin/eslint ${file})
    if [ "$result" == "" ]; then
        echo "\t\033[32mESLint Passed: ${file}\033[0m"
    else
        echo "\t\033[31mESLint Failed: ${file}\033[0m"
        pass=false
    fi
done

echo "\nJavaScript validation complete\n"

if ! ${pass}; then
    echo "\033[31mWARNING: Your commit contains files that should pass ESLint but do not. Please fix the ESLint errors.\n\033[0m"
    exit 0
else
    echo "\033[32mSUCCESS: Thank you for following good coding standards!\033[0m\n"
    exit 0
fi
