(function() {

    'use strict';

    var fs     = require('fs');
    var exec   = require('child_process').exec;
    var path   = require('path');
    var Q      = require('q');
    var rimraf = require('rimraf');
    var mkdirp = require('mkdirp');
    var _      = require('lodash');
    var config = require('../package.json');

    removeDirec()
        .then(createDirec, failure)
        .then(createSymLk, failure)
        .then(buildMapbox, failure);

    function removeDirec() {
        var deferred = Q.defer();
        console.log('Removing directory app/lib/node_modules..');
        rimraf('./app/lib/node_modules', function(error) {
            if (error) {
                deferred.reject(new Error(error));
            } else {
                console.log('Successfully removed directory app/lib/node_modules\n');
                deferred.resolve();
            }
        });
        return deferred.promise;
    }

    function createDirec() {
        var deferred = Q.defer();
        console.log('Creating directory app/lib/node_modules..');
        mkdirp('./app/lib/node_modules', function(error) {
            if (error) {
                deferred.reject(new Error(error));
            } else {
                console.log('Successfully created directory app/lib/node_modules');
                mkdirp('./app/lib/node_modules/@nv', function(error) {
                    if (error) {
                        deferred.reject(new Error(error));
                    } else {
                      console.log('Successfully created directory app/lib/node_modules/@nv\n');
                      deferred.resolve();
                    }
                })
            }
        });
        return deferred.promise;
    }

    function createSymLk() {
        console.log('Creating symlinks..');
        return Q.all(_.map(config.dependencies, createSymlink)).then(function() {
            console.log('Successfully created all symlinks\n');
        });

        function createSymlink(version, name) {
            var deferred = Q.defer(),
                target   = path.normalize(__dirname + '/../node_modules/' + name),
                link     = path.normalize(__dirname + '/../app/lib/node_modules/' + name);
            fs.symlink(target, link, 'dir', function(error) {
                if (error) {
                    deferred.reject(new Error(error));
                } else {
                    console.log('Successfully created symlink to ' + target + ' (v' + version + ')');
                    deferred.resolve();
                }
            });
            return deferred.promise;
        }
    }

    function buildMapbox() {
        var deferred = Q.defer();
        console.log('Building mapbox.js..\n');
        exec('cd node_modules/mapbox.js/ && rm -rf node_modules && make && rm -rf node_modules', function(error, stdout, stderr) {
            if (error) {
                console.error(stderr);
                deferred.reject(new Error(error));
            } else {
                console.log(stdout);
                console.log("Successfully built mapbox.js\n");
                deferred.resolve();
            }
        });
        return deferred.promise;
    }

    function failure(error) {
        console.error(error);
    }

})();
