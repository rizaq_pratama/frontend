(function test() {
  describe('routes.config', () => {
    const module = angular.mock.module;
    const inject = angular.mock.inject;

    let $stateProvider;
    let $urlRouterProvider;

    // define a completely new module so that we can inject the same
    // providers used in the actual config and create a spy on these
    // providers that we want to test later
    angular.module('fake.routes.config', ['ui.router'])
            .config((_$stateProvider_, _$urlRouterProvider_) => {
              $stateProvider = _$stateProvider_;
              $urlRouterProvider = _$urlRouterProvider_;
              spyOn($urlRouterProvider, 'otherwise');
              spyOn($stateProvider, 'state').and.callThrough(); // because of chaining
            });

    beforeEach(() => {
      module('fake.routes.config');
      module('nvOperatorApp.configs');
      inject();
    });

    it('should default unrecognized routes to \'/login\'', () => {
      expect($urlRouterProvider.otherwise).toHaveBeenCalledWith('/login');
    });

    it('should generate application states', () => {
      expect($stateProvider.state).toHaveBeenCalledWith('login', jasmine.any(Object));
      expect($stateProvider.state).toHaveBeenCalledWith('container', jasmine.any(Object));
      expect($stateProvider.state).toHaveBeenCalledWith('container.add-parcel-to-route', jasmine.any(Object));
      expect($stateProvider.state).toHaveBeenCalledWith('container.blocked-dates', jasmine.any(Object));
      expect($stateProvider.state).toHaveBeenCalledWith('container.change-delivery-timings', jasmine.any(Object));
      expect($stateProvider.state).toHaveBeenCalledWith('container.dp-administration', jasmine.any(Object));
      expect($stateProvider.state).toHaveBeenCalledWith('container.dp-administration.dp-partners', jasmine.any(Object));
      expect($stateProvider.state).toHaveBeenCalledWith('container.dp-administration.dp-users', jasmine.any(Object));
      expect($stateProvider.state).toHaveBeenCalledWith('container.dp-administration.dps', jasmine.any(Object));
      expect($stateProvider.state).toHaveBeenCalledWith('container.driver-seeding', jasmine.any(Object));
      expect($stateProvider.state).toHaveBeenCalledWith('container.driver-strength', jasmine.any(Object));
      expect($stateProvider.state).toHaveBeenCalledWith('container.driver-type-management', jasmine.any(Object));
      expect($stateProvider.state).toHaveBeenCalledWith('container.hub-list', jasmine.any(Object));
      expect($stateProvider.state).toHaveBeenCalledWith('container.shipment-inbound-scanning', jasmine.any(Object));
      expect($stateProvider.state).toHaveBeenCalledWith('container.lat-lng-cleanup', jasmine.any(Object));
      expect($stateProvider.state).toHaveBeenCalledWith('container.order', jasmine.any(Object));
      expect($stateProvider.state).toHaveBeenCalledWith('container.order.create-combine', jasmine.any(Object));
      expect($stateProvider.state).toHaveBeenCalledWith('container.polygon-drawing', jasmine.any(Object));
      expect($stateProvider.state).toHaveBeenCalledWith('container.printers', jasmine.any(Object));
      expect($stateProvider.state).toHaveBeenCalledWith('container.recovery-ticket-scanning', jasmine.any(Object));
      expect($stateProvider.state).toHaveBeenCalledWith('container.recovery-tickets', jasmine.any(Object));
      expect($stateProvider.state).toHaveBeenCalledWith('container.relabel-stamp', jasmine.any(Object));
      expect($stateProvider.state).toHaveBeenCalledWith('container.reservations', jasmine.any(Object));
      expect($stateProvider.state).toHaveBeenCalledWith('container.route-group', jasmine.any(Object));
      expect($stateProvider.state).toHaveBeenCalledWith('container.same-day-route-engine', jasmine.any(Object));
      expect($stateProvider.state).toHaveBeenCalledWith('container.settings', jasmine.any(Object));
      expect($stateProvider.state).toHaveBeenCalledWith('container.shipment-management', jasmine.any(Object));
      expect($stateProvider.state).toHaveBeenCalledWith('container.shipment-scanning', jasmine.any(Object));
      expect($stateProvider.state).toHaveBeenCalledWith('container.transactions-v2', jasmine.any(Object));
      expect($stateProvider.state).toHaveBeenCalledWith('container.user-management', jasmine.any(Object));
      expect($stateProvider.state).toHaveBeenCalledWith('container.role-management', jasmine.any(Object));
      expect($stateProvider.state).toHaveBeenCalledWith('container.linehaul', jasmine.any(Object));
      expect($stateProvider.state).toHaveBeenCalledWith('container.linehaul.date', jasmine.any(Object));
      expect($stateProvider.state).toHaveBeenCalledWith('container.linehaul.entries', jasmine.any(Object));
    });

    it('should configure application states with at least \'url\', \'templateUrl\', \'controller\' and \'controllerAs\' properties', () => {
      _.forEach($stateProvider.state.calls.allArgs(), (args) => {
        if (args[1].views) {
          expect(args[1]).toEqual(jasmine.objectContaining({
            url: jasmine.any(String),
            views: jasmine.any(Object),
          }));
        } else if (args[1].data) {
          expect(args[1]).toEqual(jasmine.objectContaining({
            url: jasmine.any(String),
            data: jasmine.any(Object),
          }));
        } else {
          expect(args[1]).toEqual(jasmine.objectContaining({
            url: jasmine.any(String),
            templateUrl: jasmine.any(String),
            controller: jasmine.any(String),
            controllerAs: 'ctrl',
          }));
        }
      });
    });
  });
}());
