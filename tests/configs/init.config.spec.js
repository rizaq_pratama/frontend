(function() {
    'use strict';

    describe('init.config', function() {

        var module = angular.mock.module;
        var inject = angular.mock.inject;

        var env,
            $compileProvider,
            $logProvider,
            $mdDateLocaleProvider;

        // define a completely new module so that we can inject the same
        // providers used in the actual config and create a spy on these
        // providers that we want to test later
        angular.module('fake.init.config', ['ngMaterial'])
            .config(function(_$compileProvider_, _$logProvider_, _$mdDateLocaleProvider_) {
                $compileProvider = _$compileProvider_;
                $logProvider = _$logProvider_;
                $mdDateLocaleProvider = _$mdDateLocaleProvider_;
                spyOn($compileProvider, 'debugInfoEnabled');
                spyOn($logProvider, 'debugEnabled');
                spyOn($mdDateLocaleProvider, 'formatDate').and.callThrough();
            });

        beforeEach(function() {
            // save the current env and switch to production
            env = nv.config.env;
            nv.config.env = 'prod';

            // load and run the modules
            module('fake.init.config');
            module('nvOperatorApp.configs');
            inject();
        });

        afterEach(function() {
            // restore the env
            nv.config.env = env;
        });

        it('should disable various debug runtime information in deployments', function() {
            expect($compileProvider.debugInfoEnabled).toHaveBeenCalledWith(false);
        });

        it('should disable debug level logs in deployments', function() {
            expect($logProvider.debugEnabled).toHaveBeenCalledWith(false);
        });

        it('should configure angular material to format dates in local time as YYYY-MM-DD for valid dates', function() {
            checkFormat(Date.UTC(2016, 9, 2, 0,  0, 0));
            checkFormat(Date.UTC(2016, 9, 2, 1,  0, 0));
            checkFormat(Date.UTC(2016, 9, 2, 2,  0, 0));
            checkFormat(Date.UTC(2016, 9, 2, 3,  0, 0));
            checkFormat(Date.UTC(2016, 9, 2, 4,  0, 0));
            checkFormat(Date.UTC(2016, 9, 2, 5,  0, 0));
            checkFormat(Date.UTC(2016, 9, 2, 6,  0, 0));
            checkFormat(Date.UTC(2016, 9, 2, 7,  0, 0));
            checkFormat(Date.UTC(2016, 9, 2, 8,  0, 0));
            checkFormat(Date.UTC(2016, 9, 2, 9,  0, 0));
            checkFormat(Date.UTC(2016, 9, 2, 10, 0, 0));
            checkFormat(Date.UTC(2016, 9, 2, 11, 0, 0));
            checkFormat(Date.UTC(2016, 9, 2, 12, 0, 0));
            checkFormat(Date.UTC(2016, 9, 2, 13, 0, 0));
            checkFormat(Date.UTC(2016, 9, 2, 14, 0, 0));
            checkFormat(Date.UTC(2016, 9, 2, 15, 0, 0));
            checkFormat(Date.UTC(2016, 9, 2, 16, 0, 0));
            checkFormat(Date.UTC(2016, 9, 2, 17, 0, 0));
            checkFormat(Date.UTC(2016, 9, 2, 18, 0, 0));
            checkFormat(Date.UTC(2016, 9, 2, 19, 0, 0));
            checkFormat(Date.UTC(2016, 9, 2, 20, 0, 0));
            checkFormat(Date.UTC(2016, 9, 2, 21, 0, 0));
            checkFormat(Date.UTC(2016, 9, 2, 22, 0, 0));
            checkFormat(Date.UTC(2016, 9, 2, 23, 0, 0));

            function checkFormat(milliseconds) {
                var actually = $mdDateLocaleProvider.formatDate(new Date(milliseconds));
                var expected = moment(milliseconds).format('YYYY-MM-DD');
                expect(actually).toBe(expected);
            }
        });

        it('should configure angular material to format dates as null for invalid dates', function() {
            var warn = console.warn;
            console.warn = angular.noop; // suppress warnings for clean test reporting
            expect($mdDateLocaleProvider.formatDate(null)).toBe(null);
            expect($mdDateLocaleProvider.formatDate('not a real date')).toBe(null);
            console.warn = warn; // restore warnings
        });

    });

})();
