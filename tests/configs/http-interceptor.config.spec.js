(function() {
    'use strict';

    describe('http-interceptor.config', function() {

        var module = angular.mock.module;
        var inject = angular.mock.inject;

        var env,
            servers,
            $httpProvider,
            $windowProvider;

        // define a completely new module so that we can inject the same
        // providers used in the actual config and create a spy on these
        // providers that we want to test later
        angular.module('fake.http-interceptor.config', [])
            .config(function(_$httpProvider_, _$windowProvider_) {
                $httpProvider = _$httpProvider_;
                $windowProvider = _$windowProvider_;
                spyOn($windowProvider, '$get').and.returnValue({location: {hash: '#/sg/'}});
            });

        beforeEach(function() {
            // save the current config
            env = nv.config.env;
            servers = nv.config.servers;

            // switch to production config
            nv.config.env = 'prod';
            nv.config.servers.dpms = 'https://api.ninjavan.co/$country/dpms';
            nv.config.servers.locations = 'https://locations.ninjavan.sg/2.0/search';
            nv.config.servers.engine = 'https://api.ninjavan.co/$country/engine';
            nv.config.servers.core = 'https://api.ninjavan.co/$country/core';
            nv.config.servers.route = 'https://api.ninjavan.co/$country/route';
            nv.config.servers.driver = 'https://api.ninjavan.co/$country/driver';
            nv.config.servers['script-engine'] = 'https://api.ninjavan.co/$country/script-engine';
            nv.config.servers.shipper = 'https://api.ninjavan.co/$country/shipper';
            nv.config.servers.image = 'https://api.ninjavan.co/$country/image';
            nv.config.servers.ticketing = 'https://api.ninjavan.co/$country/ticketing';
            nv.config.servers.addressing = 'https://api.ninjavan.co/$country/addressing';
            nv.config.servers.hub = 'https://api.ninjavan.co/$country/hub';
            nv.config.servers.vrp = 'https://api.ninjavan.co/$country/vrp';
            nv.config.servers['shipper-dashboard'] = 'https://api.ninjavan.co/$country/shipper-dashboard';

            // load and run the modules
            module('fake.http-interceptor.config');
            module('nvOperatorApp.configs');
            inject();
        });

        afterEach(function() {
            // restore the config
            nv.config.env = env;
            nv.config.servers = servers;
        });

        it('should configure a request interceptor', function() {
            expect($httpProvider.interceptors.length).toBe(1);
        });

        it('should replace the request url with the actual endpoint if it matches any configuration in the request interceptor', function() {
            expect(requestWith('dpms/*').url).toBe('https://api.ninjavan.co/sg/dpms/*');
            expect(requestWith('engine/*').url).toBe('https://api.ninjavan.co/sg/engine/*');
            expect(requestWith('core/*').url).toBe('https://api.ninjavan.co/sg/core/*');
            expect(requestWith('route/*').url).toBe('https://api.ninjavan.co/sg/route/*');
            expect(requestWith('driver/*').url).toBe('https://api.ninjavan.co/sg/driver/*');
            expect(requestWith('script-engine/*').url).toBe('https://api.ninjavan.co/sg/script-engine/*');
            expect(requestWith('shipper/*').url).toBe('https://api.ninjavan.co/sg/shipper/*');
            expect(requestWith('image/*').url).toBe('https://api.ninjavan.co/sg/image/*');
            expect(requestWith('ticketing/*').url).toBe('https://api.ninjavan.co/sg/ticketing/*');
            expect(requestWith('addressing/*').url).toBe('https://api.ninjavan.co/sg/addressing/*');
            expect(requestWith('hub/*').url).toBe('https://api.ninjavan.co/sg/hub/*');
            expect(requestWith('vrp/*').url).toBe('https://api.ninjavan.co/sg/vrp/*');
            expect(requestWith('shipper-dashboard/*').url).toBe('https://api.ninjavan.co/sg/shipper-dashboard/*');
        });

        it('should not replace the request url if it does not match any configuration in the request interceptor', function() {
            expect(requestWith('https://www.example.com/*').url).toBe('https://www.example.com/*');
        });

        it('should not have Authorization header for external api request', function() {
            expect(requestWith('https://api.example.com/*').headers.Authorization).toBe(undefined);
        });

        it('should trim whitespace in url', function() {
            expect(requestWith('core/route/inbound/  1000  /van-inbound').url).toBe('https://api.ninjavan.co/sg/core/route/inbound/1000/van-inbound');
            expect(requestWith('core/route/inbound/  1000\r\n/van-inbound').url).toBe('https://api.ninjavan.co/sg/core/route/inbound/1000/van-inbound');
            expect(requestWith('core/route/inbound/  1000  /van-inbound?name=oke').url).toBe('https://api.ninjavan.co/sg/core/route/inbound/1000/van-inbound?name=oke');
            expect(requestWith('core/route/inbound/  1000  /van-inbound?name=oke&space=true').url).toBe('https://api.ninjavan.co/sg/core/route/inbound/1000/van-inbound?name=oke&space=true');
            expect(requestWith('core/route/inbound/  1000  /van-inbound?name=oke#keo&space=true').url).toBe('https://api.ninjavan.co/sg/core/route/inbound/1000/van-inbound?name=oke%23keo&space=true');
            expect(requestWith('core/route/inbound/  1000  /van-inbound?name=oke   &space=  true').url).toBe('https://api.ninjavan.co/sg/core/route/inbound/1000/van-inbound?name=oke&space=true');
        });

        it ('should trim whitespace in payload', () => {
            expect(requestWithPayload('core/*', { name: '  joni  ', address: '  somewhere' }).data).toEqual({ name: 'joni', address: 'somewhere' });
            expect(requestWithPayload('core/*', { name: '  joni  ', order: { type: ' C2C Sameday    ' } }).data).toEqual({ name: 'joni', order: { type: 'C2C Sameday' } });
            expect(requestWithPayload('core/*', [{ name: '   joni' }, { name: 'juno  ' }]).data).toEqual([{ name: 'joni' }, { name: 'juno' }]);
            expect(requestWithPayload('core/*', null, { name: '  joni  ', order: ' C2C Sameday    ' }).param).toEqual({ name: 'joni', order: 'C2C%20Sameday' });
            expect(requestWithPayload('core/*', ' juno and joni   ').data).toBe('juno and joni');
            expect(requestWithPayload('core/*', { a: [{ b: 1, c: [{ d: 2 }] }] }).data).toEqual({ a: [{ b: 1, c: [{ d: 2 }] }] });
            expect(requestWithPayload('core/*', { a: [{ b: '  joni', c: [{ d: 'juno  ' }] }] }).data).toEqual({ a: [{ b: 'joni', c: [{ d: 'juno' }] }] });
        });

        it ('should not trim payload from FormData', () => {
            var formData = new FormData();
            formData.append('username', '  joni  ');
            expect(requestWithPayload('core/*', formData).data).toBe(formData);
            expect(requestWithPayload('core/*', formData).data instanceof FormData).toBe(true);
        });

        function requestWith(url) {
            return $httpProvider.interceptors[0]().request({url: url, headers: {Authorization: 'Bearer 12345678'}});
        }

        function requestWithPayload(url, payload, param) {
            return $httpProvider.interceptors[0]().request({url: url, data: payload, param: param, headers: {Authorization: 'Bearer 12345678'}});
        }

    });

})();
