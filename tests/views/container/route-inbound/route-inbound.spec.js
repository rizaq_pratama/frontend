(function test() {
  describe('RouteInboundController', () => {
    let ctrl;

    beforeEach(() => {
      let $controller = null;

      module('nvOperatorApp.controllers');
      module('nvCommons.services');
      module('nvCommons.models');

      inject((_$controller_) => {
        $controller = _$controller_;
      });

      ctrl = $controller('RouteInboundController', { });
    });

    it('ctrl.IS_DEBUG should be false when push to server', () => {
      expect(ctrl.IS_DEBUG).toBeFalsy();
    });
  });
}());
