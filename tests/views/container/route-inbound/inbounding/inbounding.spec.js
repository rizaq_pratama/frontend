(function test() {
  describe('RouteInboundInboundingController', () => {
    jasmine.getJSONFixtures().fixturesPath = 'base/tests/mock';
    const SAMPLE = window.getJSONFixture('inbound-summary-result.json');

    let parentCtrl;
    let ctrl;
    let $rootScope;
    let $controller;
    let $scope;

    beforeEach(() => {
      module('ui.router', ($provide) => {
        $provide.value('$state', {
          go: jasmine.createSpy(),
        });
      });
      module('ngMaterial');
      module('pascalprecht.translate')
      module('nvOperatorApp.controllers');
      module('nvCommons.services');
      module('nvCommons.models');

      inject((_$controller_, _$rootScope_) => {
        $controller = _$controller_;
        $rootScope = _$rootScope_;
      });

      parentCtrl = $controller('RouteInboundController', { });
      parentCtrl.selectedHub = {
        id: 1,
        name: 'DEBUG HUB',
      };
      parentCtrl.extendInboundSummaryResult(SAMPLE);

      $scope = $rootScope.$new();
      $scope.$parent.ctrl = parentCtrl;

      ctrl = $controller('RouteInboundInboundingController', {
        $scope: $scope,
      });
    });

    describe('ctrl.expectedScans["container.route-inbound.reservation-pickups"]', () => {
      it('UI will show -> 0 / 6 if reservation(s) has 5 orders pick up scanned and inputs 6 on the POD', () => {
        const expectedScan = _.find(ctrl.expectedScans, [
          'type', ctrl.VIEW_DETAILS_MODE.RESERVATION,
        ]);

        // UI will show -> 0 / 6 in Reservation Pickups
        expect(ctrl.getScannedOrders(expectedScan.all).length).toBe(0);
        expect(expectedScan.count).toBe(6);
      });

      it('UI will show -> 1 / 6 when 1 pick up scanned order gets route inbounded', inject((Inbound) => {
        // 1 pick up scanned
        const scanTrackingId = 'NVSGBAQ16000001162';
        const order = _.get(parentCtrl.inboundSummaryOrdersMap, scanTrackingId);
        order.scan_details = {
          id: null,
          type: Inbound.getTypeEnum(Inbound.TYPE.SORTING_HUB),
          scan: scanTrackingId,
        };

        // process output
        const expectedScan = _.find(ctrl.expectedScans, [
          'type', ctrl.VIEW_DETAILS_MODE.RESERVATION,
        ]);

        // UI will show -> 1 / 6 in Reservation Pickups
        expect(ctrl.getScannedOrders(expectedScan.all).length).toBe(1);
        expect(expectedScan.count).toBe(6);
      }));

      it('UI will show -> 1 / 6 | +1 when 1 pick up scanned and 1 non-pick up scanned order gets route inbounded', inject((Inbound) => {
        // 1 pick up scanned
        const scanTrackingId = 'NVSGBAQ16000001162';
        const order = _.get(parentCtrl.inboundSummaryOrdersMap, scanTrackingId);
        order.scan_details = {
          id: null,
          type: Inbound.getTypeEnum(Inbound.TYPE.SORTING_HUB),
          scan: scanTrackingId,
        };

        // 1 non-pick up scanned
        const scanNonPickupTrackingId = 'NVSGNONPICKUP';
        const extraOrderDetails = _.get(parentCtrl.inboundSummaryResult, 'extra_order_details');

        extraOrderDetails.push({
          shipper_id: 1,
          shipper_name: 'Test Shipper Name',
          tracking_id: scanNonPickupTrackingId,
          scan_details: {
            id: null,
            type: Inbound.getTypeEnum(Inbound.TYPE.SORTING_HUB),
            scan: scanNonPickupTrackingId,
          },
        });

        // process output
        const expectedScan = _.find(ctrl.expectedScans, [
          'type', ctrl.VIEW_DETAILS_MODE.RESERVATION,
        ]);

        // UI will show -> 1 / 6 | +1 in Reservation Pickups
        expect(
          _.size(_.get(ctrl.parentCtrl.inboundSummaryResult, 'extra_order_details'))
        ).toBe(1);
        expect(ctrl.getScannedOrders(expectedScan.all).length).toBe(1);
        expect(expectedScan.count).toBe(6);
      }));
    });

    describe('ctrl.hasComments()', () => {
      it('return true if `comments` field is not empty', () => {
        parentCtrl.inboundSummaryResult.comments = ['test comment'];
        expect(ctrl.hasComments()).toBeTruthy();
      });

      it('return false if `comments` field is empty', () => {
        parentCtrl.inboundSummaryResult.comments = [];
        expect(ctrl.hasComments()).toBeFalsy();
      });
    });

    describe('ctrl.getLatestComment()', () => {
      it('should get the last comment', () => {
        parentCtrl.inboundSummaryResult.comments = [
          'test comment 1', 'test comment 2',
        ];
        expect(ctrl.getLatestComment()).toBe('test comment 2');
      });
    });

    describe('ctrl.getParcelsProcessedCount()', () => {
      it('get total number of inbounded orders', () => {
        expect(ctrl.getParcelsProcessedCount()).toBe(4);
      });
    });

    describe('ctrl.getParcelsProcessedTotal()', () => {
      it('get total number of expectedScan.all, for reservation get expectedScan.count', () => {
        expect(ctrl.getParcelsProcessedTotal()).toBe(10);
      });
    });

    describe('ctrl.scanInbound()', () => {
      it('scan inbound with empty tracking id entered', () => {
        let failed = false;
        ctrl.scanInbound({ which: 13 }).catch(() => {
          // should reject resolved promise when fail
          failed = true;
        });

        $rootScope.$apply();
        expect(failed).toBe(true);
      });

      it('when event is not "enter" event', () => {
        let failed = false;

        ctrl.formTrackingId = 'TEST';
        ctrl.scanInbound({ which: 0 }).catch(() => {
          // should reject resolved promise
          failed = true;
        });

        $rootScope.$apply();
        expect(failed).toBe(true);
      });

      it('scan inbound success with valid reservation tracking id', inject((Inbound) => {
        spyOn(Inbound, 'scanInbound').and.returnValue(Promise.resolve({
          international: false,
          status: 'SUCCESSFUL_INBOUND',
          sector: 'GG',
          destination_hub: 'EASTGW',
          destination_hub_id: 3,
          priority_level: 0,
          is_international: false,
          failure_reason_color_code: null,
          set_aside: null,
          weight_difference: null,
          shipper: null,
        }));

        const expectedScan = _.find(ctrl.expectedScans, [
          'type', ctrl.VIEW_DETAILS_MODE.RESERVATION,
        ]);
        const originalProcessed = ctrl.getScannedOrders(expectedScan.all).length;
        const originalInboundingHistory = _.size(ctrl.inboundingHistory);

        ctrl.formTrackingId = 'NVSGBAQ16000001162';
        ctrl.scanInbound({ which: 13 }).then(() => {
          const newProcessed = ctrl.getScannedOrders(expectedScan.all).length;
          const newInboundingHistory = _.size(ctrl.inboundingHistory);

          expect(newProcessed).toBe(originalProcessed + 1);
          expect(newInboundingHistory).toBe(originalInboundingHistory + 1);
        });
      }));

      it('scan inbound success with invalid tracking id', inject((Inbound) => {
        spyOn(Inbound, 'scanInbound').and.returnValue(Promise.resolve({
          international: false,
          status: 'ORDER_NOT_FOUND',
          sector: null,
          destination_hub: null,
          destination_hub_id: null,
          priority_level: 0,
          is_international: false,
          failure_reason_color_code: null,
          set_aside: null,
          weight_difference: null,
          shipper: null,
        }));

        const originalInboundingHistory = _.size(ctrl.inboundingHistory);

        ctrl.formTrackingId = 'TEST';
        ctrl.scanInbound({ which: 13 }).then(() => {
          const newInboundingHistory = _.size(ctrl.inboundingHistory);
          expect(newInboundingHistory).toBe(originalInboundingHistory + 1);
        });
      }));

      it('scan inbound success with duplicate tracking id', inject((Warehousing) => {
        spyOn(Warehousing, 'sweep').and.returnValue(Promise.resolve({}));

        const originalInboundingHistory = _.size(ctrl.inboundingHistory);

        ctrl.formTrackingId = 'NVSGBAQ16000001156';
        ctrl.scanInbound({ which: 13 }).catch(() => {
          const newInboundingHistory = _.size(ctrl.inboundingHistory);
          expect(newInboundingHistory).toBe(originalInboundingHistory + 1);
        });
      }));

      it('scan inbound failure', inject((Inbound) => {
        spyOn(Inbound, 'scanInbound').and.returnValue(Promise.reject({
          status: 404,
          statusText: 'Not found',
        }));

        const originalInboundingHistory = _.size(ctrl.inboundingHistory);

        ctrl.formTrackingId = 'TEST';
        ctrl.scanInbound({ which: 13 }).then(() => {
          const newInboundingHistory = _.size(ctrl.inboundingHistory);
          expect(newInboundingHistory).toBe(originalInboundingHistory + 1);
        });
      }));
    });
  });
}());
