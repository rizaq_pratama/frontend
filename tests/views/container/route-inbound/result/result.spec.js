(function test() {
  describe('RouteInboundResultController', () => {
    jasmine.getJSONFixtures().fixturesPath = 'base/tests/mock';
    const SAMPLE = window.getJSONFixture('inbound-summary-result.json');

    let parentCtrl;
    let ctrl;
    let $rootScope;
    let $controller;
    let $scope;

    beforeEach(() => {
      module('ui.router', ($provide) => {
        $provide.value('$state', {
          go: jasmine.createSpy(),
        });
      });
      module('ngMaterial');
      module('pascalprecht.translate');
      module('nvOperatorApp.controllers');
      module('nvCommons.services');
      module('nvCommons.models');

      inject((_$controller_, _$rootScope_) => {
        $controller = _$controller_;
        $rootScope = _$rootScope_;
      });

      parentCtrl = $controller('RouteInboundController', { });
      parentCtrl.extendInboundSummaryResult(SAMPLE);

      $scope = $rootScope.$new();
      $scope.$parent.ctrl = parentCtrl;

      ctrl = $controller('RouteInboundResultController', {
        $scope: $scope,
      });
    });

    describe('ctrl.collectionSummaryCount.reservation', () => {
      it('UI will show -> 6 if reservation(s) has 5 orders pick up scanned and inputs 6 on the POD', () => {
        expect(ctrl.collectionSummaryCount.reservation).toBe(6);
      });

      it('UI will show -> 5 when 1 pick up scanned order gets route inbounded', inject((Inbound) => {
        // 1 pick up scanned
        const scanTrackingId = 'NVSGBAQ16000001162';
        const order = _.get(parentCtrl.inboundSummaryOrdersMap, scanTrackingId);
        order.scan_details = {
          id: null,
          type: Inbound.getTypeEnum(Inbound.TYPE.SORTING_HUB),
          scan: scanTrackingId,
        };

        ctrl.processInboundSummaryData();
        expect(ctrl.collectionSummaryCount.reservation).toBe(5);
      }));

      it('UI will show -> 4 when 1 pick up scanned and 1 non-pick up scanned order gets route inbounded', inject((Inbound) => {
        // 1 pick up scanned
        const scanTrackingId = 'NVSGBAQ16000001162';
        const order = _.get(parentCtrl.inboundSummaryOrdersMap, scanTrackingId);
        order.scan_details = {
          id: null,
          type: Inbound.getTypeEnum(Inbound.TYPE.SORTING_HUB),
          scan: scanTrackingId,
        };

        // 1 non-pick up scanned
        const scanNonPickupTrackingId = 'NVSGNONPICKUP';
        const extraOrderDetails = _.get(parentCtrl.inboundSummaryResult, 'extra_order_details');

        extraOrderDetails.push({
          shipper_id: 1,
          shipper_name: 'Test Shipper Name',
          tracking_id: scanNonPickupTrackingId,
          scan_details: {
            id: null,
            type: Inbound.getTypeEnum(Inbound.TYPE.SORTING_HUB),
            scan: scanNonPickupTrackingId,
          },
        });

        ctrl.processInboundSummaryData();
        expect(ctrl.collectionSummaryCount.reservation).toBe(4);
      }));

      it('UI will show -> 0 when 5 pick up scanned and 2 non-pick up scanned order gets route inbounded', inject((Inbound) => {
        // 5 pick up scanned
        addOrderScanDetail('NVSGBAQ16000001162');
        addOrderScanDetail('NVSGBAQ16000001161');
        addOrderScanDetail('NVSGBAQ16000001164');
        addOrderScanDetail('NVSGBAQ16000001049');
        addOrderScanDetail('NVSGBAQ16000001048');

        // 2 non-pick up scanned
        addExtraOrderScanDetail('NVSGNONPICKUP1');
        addExtraOrderScanDetail('NVSGNONPICKUP2');

        ctrl.processInboundSummaryData();
        expect(ctrl.collectionSummaryCount.reservation).toBe(0);

        function addOrderScanDetail(scanTrackingId) {
          const order = _.get(parentCtrl.inboundSummaryOrdersMap, scanTrackingId);
          order.scan_details = {
            id: null,
            type: Inbound.getTypeEnum(Inbound.TYPE.SORTING_HUB),
            scan: scanTrackingId,
          };
        }

        function addExtraOrderScanDetail(scanTrackingId) {
          const extraOrderDetails = _.get(parentCtrl.inboundSummaryResult, 'extra_order_details');

          extraOrderDetails.push({
            shipper_id: 1,
            shipper_name: 'Test Shipper Name',
            tracking_id: scanTrackingId,
            scan_details: {
              id: null,
              type: Inbound.getTypeEnum(Inbound.TYPE.SORTING_HUB),
              scan: scanTrackingId,
            },
          });
        }
      }));
    });
  });
}());
