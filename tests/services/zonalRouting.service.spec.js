(function test() {
  describe('nvZonalRouting.service', () => {
    const module = angular.mock.module;
    const inject = angular.mock.inject;
    const ZONES_SAMPLE = [{
      id: 1,
      name: 'Z-Out of Zone',
      hubId: 1,
      min: 0,
      max: 0,
      priority: 0,
      nonPriority: 0,
      totalParcels: 0,
      buffer: 0,
      parcelsToDo: 0,
      condition: 'container.zonal-routing.sufficient',
      capacity: 0,
      driversTabExpanded: true,
      drivers: [],
      isCustomZone: false,
    }];
    const DRIVERS_SAMPLE = [{
      id: 100001,
      uuid: '47525730-312e-11e6-a130-062f578a5ed1',
      firstName: 'OPS - Ninja Bo',
      lastName: '',
      licenseNumber: '001463794G',
      driverType: 'Ops',
      driverTypeId: 100001,
      availability: true,
      codLimit: 99999,
      vehicles: [
        {
          id: 104005,
          vehicleType: 'Car',
          vehicleNo: 'OPS - Ninja Bo',
          ownVehicle: false,
          active: true,
          capacity: 40,
        },
      ],
      contacts: [
        {
          type: 'Mobile Phone',
          details: '98753063',
          active: true,
        },
      ],
      zonePreferences: [
        {
          id: 311454,
          longitude: 106.5234375,
          latitude: 3.71078200434872,
          minWaypoints: 5,
          maxWaypoints: 50,
          cost: 0,
          zoneId: 1,
          rank: 1,
        },
      ],
      username: 'boxian',
      password: null,
      comments: '',
      systemId: 'sg',
      tags: {
        RESUPPLY: false,
      },
      maxBiddableWaypoints: 100,
      hubId: null,
      name: 'OPS - Ninja Bo ',
      zoneId: 1,
      zoneName: 'Z-Out of Zone',
      min: 5,
      max: 50,
      cost: 0,
      seedLatitude: 3.71078200434872,
      seedLongitude: 106.5234375,
      vehicleId: 104005,
      vehicleType: 'Car',
      vehicleCapacity: 40,
      label: '',
      dndType: 'driver-from-driver-list-table',
      dndSelected: false,
    }];
    const DRIVERS_BY_ZONES_SAMPLE = [{
      id: 1,
      label: '',
      name: 'Z-Out of Zone',
      count: 1,
      min: 5,
      max: 50,
      dndType: 'drivers-by-zone',
      drivers: [],
    }];

    let nvZonalRouting;

    beforeEach(() => {
      module('ui.router', ($provide) => {
        $provide.value('$state', {
          go: jasmine.createSpy(),
        });
      });
      module('ngMaterial');
      module('nvOperatorApp.services');
      module('nvOperatorApp.runs');
      module('nvCommons.models');

      inject((_nvZonalRouting_) => {
        nvZonalRouting = _nvZonalRouting_;
      });
    });

    describe('nvZonalRouting.isDebug()', () => {
      it('debug should be false when push to server', () => {
        expect(nvZonalRouting.isDebug()).toBeFalsy();
      });
    });

    describe('nvZonalRouting.initializeVariables()', () => {
      beforeEach(() => {
        nvZonalRouting.initializeVariables();
      });

      it('variables should be defined', () => {
        expect(nvZonalRouting.routeTags).toEqual([]);
        expect(nvZonalRouting.hubs).toEqual([]);
        expect(nvZonalRouting.routeGroups).toEqual([]);
        expect(nvZonalRouting.driverTypes).toEqual([]);

        // and so on..
      });
    });

    describe('nvZonalRouting.getAllZonesId()', () => {
      // refer to `zones` database table
      it('should return 2', () => {
        expect(nvZonalRouting.getAllZonesId()).toEqual(2);
      });
    });

    describe('nvZonalRouting.getNullZoneData()', () => {
      it('should return object with id < 0 and hubId is null', () => {
        // because zones with id > 0 are real zones (refer to `zones` database table)
        expect(nvZonalRouting.getNullZoneData().id).toBeLessThan(0);
        expect(nvZonalRouting.getNullZoneData().hubId).toBeNull();
      });
    });

    describe('nvZonalRouting.getUnavailableZoneData()', () => {
      it('should return object with id < 0 and hubId is null', () => {
        // because zones with id > 0 are real zones (refer to `zones` database table)
        expect(nvZonalRouting.getUnavailableZoneData().id).toBeLessThan(0);
        expect(nvZonalRouting.getUnavailableZoneData().hubId).toBeNull();
      });
    });

    describe('nvZonalRouting.getReservationZoneData()', () => {
      it('should return object with id < 0, hubId is null, and zoneId is 3', () => {
        // because zones with id > 0 are real zones (refer to `zones` database table)
        expect(nvZonalRouting.getReservationZoneData().id).toBeLessThan(0);
        expect(nvZonalRouting.getReservationZoneData().hubId).toBeNull();

        // (refer to `zones` database table)
        expect(nvZonalRouting.getReservationZoneData().zoneId).toEqual(3);
      });
    });

    describe('nvZonalRouting.addDriverToZone()', () => {
      let driver;
      let driversByZone;
      let zone;

      beforeEach(() => {
        nvZonalRouting.initializeVariables();

        nvZonalRouting.zones = _.cloneDeep(ZONES_SAMPLE);
        nvZonalRouting.driversTable.all = _.cloneDeep(DRIVERS_SAMPLE);
        nvZonalRouting.driversByZones = _.cloneDeep(DRIVERS_BY_ZONES_SAMPLE);

        driver = nvZonalRouting.driversTable.all[0];
        driversByZone = nvZonalRouting.driversByZones[0];
        zone = nvZonalRouting.zones[0];

        driversByZone.drivers.push(driver);
      });

      it('expect this driver in nvZonalRouting.driversByZones is being pulled out', () => {
        nvZonalRouting.addDriverToZone(driver.id, zone.id);
        expect(_.size(driversByZone.drivers)).toEqual(0);
      });

      it('expect this driver is inside nvZonalRouting.zones', () => {
        nvZonalRouting.addDriverToZone(driver.id, zone.id);

        const driverResult = _.find(zone.drivers, ['id', driver.id]);
        expect(driverResult).toBeDefined();
      });

      it('driver dndType and dndSelected will change to correct value', () => {
        nvZonalRouting.addDriverToZone(driver.id, zone.id);

        const driverResult = _.find(zone.drivers, ['id', driver.id]);
        expect(driverResult).toEqual(jasmine.objectContaining({
          dndType: nvZonalRouting.getDndDriverFromZoneAssignment(),
          dndSelected: false,
        }));
      });

      it('do nothing if driver not found', () => {
        nvZonalRouting.addDriverToZone(999999, zone.id);
        expect(_.size(driversByZone.drivers)).toEqual(1);
        expect(_.size(zone.drivers)).toEqual(0);
      });
    });

    describe('nvZonalRouting.addDriverToDriversByZones()', () => {
      let driver;
      let zone;

      beforeEach(() => {
        nvZonalRouting.initializeVariables();

        nvZonalRouting.zones = _.cloneDeep(ZONES_SAMPLE);
        nvZonalRouting.driversTable.all = _.cloneDeep(DRIVERS_SAMPLE);
        nvZonalRouting.driversByZones = _.cloneDeep(DRIVERS_BY_ZONES_SAMPLE);

        driver = nvZonalRouting.driversTable.all[0];
        zone = nvZonalRouting.zones[0];
        zone.drivers.push(driver);
      });

      it('expect this driver in nvZonalRouting.zones is being pulled out', () => {
        nvZonalRouting.addDriverToDriversByZones(driver);

        const driverResult = _.find(zone.drivers, ['id', driver.id]);
        expect(driverResult).toBeUndefined();
      });

      it('expect this driver is inside nvZonalRouting.driversByZones', () => {
        nvZonalRouting.addDriverToDriversByZones(driver);

        const driversByZone = nvZonalRouting.getDriversByZone(
          nvZonalRouting.getDriverZoneId(driver)
        );
        const driverResult = _.find(driversByZone.drivers, ['id', driver.id]);
        expect(driverResult).toEqual(jasmine.objectContaining({
          id: driver.id,
        }));
      });

      it('do nothing if driver is already assigned to zone and proceedIfAssigned is false', () => {
        nvZonalRouting.addDriverToDriversByZones(driver, false);
        expect(_.size(zone.drivers)).toEqual(1);

        const driversByZone = nvZonalRouting.getDriversByZone(
          nvZonalRouting.getDriverZoneId(driver)
        );
        expect(_.size(driversByZone.drivers)).toEqual(0);
      });
    });

    describe('nvZonalRouting.removeDriverFromDriversByZones()', () => {
      let driver;
      let driversByZone;

      beforeEach(() => {
        nvZonalRouting.initializeVariables();

        nvZonalRouting.driversTable.all = _.cloneDeep(DRIVERS_SAMPLE);
        nvZonalRouting.driversByZones = _.cloneDeep(DRIVERS_BY_ZONES_SAMPLE);

        driver = nvZonalRouting.driversTable.all[0];
        driversByZone = nvZonalRouting.driversByZones[0];

        driversByZone.drivers.push(driver);
      });

      it('driver from nvZonalRouting.driversByZones will be removed', () => {
        nvZonalRouting.removeDriverFromDriversByZones(driver);
        expect(_.size(driversByZone.drivers)).toEqual(0);
      });
    });

    describe('nvZonalRouting.getAssignedZoneForDriver()', () => {
      let driver;
      let zone;

      beforeEach(() => {
        nvZonalRouting.initializeVariables();

        nvZonalRouting.zones = _.cloneDeep(ZONES_SAMPLE);
        nvZonalRouting.driversTable.all = _.cloneDeep(DRIVERS_SAMPLE);
        nvZonalRouting.driversByZones = _.cloneDeep(DRIVERS_BY_ZONES_SAMPLE);

        driver = nvZonalRouting.driversTable.all[0];
        zone = nvZonalRouting.zones[0];
      });

      it('should return zone object if driver is assigned to zone', () => {
        zone.drivers.push(driver);
        expect(nvZonalRouting.getAssignedZoneForDriver(driver)).toEqual(zone);
      });

      it('return null if driver not yet assigned to zone', () => {
        expect(nvZonalRouting.getAssignedZoneForDriver(driver)).toBeNull();
      });
    });

    describe('nvZonalRouting.isAssignedToZone()', () => {
      let driver;
      let zone;

      beforeEach(() => {
        nvZonalRouting.initializeVariables();

        nvZonalRouting.zones = _.cloneDeep(ZONES_SAMPLE);
        nvZonalRouting.driversTable.all = _.cloneDeep(DRIVERS_SAMPLE);
        nvZonalRouting.driversByZones = _.cloneDeep(DRIVERS_BY_ZONES_SAMPLE);

        driver = nvZonalRouting.driversTable.all[0];
        zone = nvZonalRouting.zones[0];
      });

      it('true if assigned', () => {
        zone.drivers.push(driver);
        expect(nvZonalRouting.isAssignedToZone(driver)).toBeTruthy();
      });

      it('false if not yet assigned', () => {
        expect(nvZonalRouting.isAssignedToZone(driver)).toBeFalsy();
      });
    });

    describe('nvZonalRouting.filterDriversTable()', () => {
      beforeEach(() => {
        nvZonalRouting.initializeVariables();

        nvZonalRouting.driversTable.all = _.cloneDeep(DRIVERS_SAMPLE);
        nvZonalRouting.driversByZones = _.cloneDeep(DRIVERS_BY_ZONES_SAMPLE);
        nvZonalRouting.driversByZonesTable.filteredDriversByZones = nvZonalRouting.driversByZones;

        const driver = nvZonalRouting.driversTable.all[0];
        const driversByZone = nvZonalRouting.driversByZones[0];
        driversByZone.drivers.push(driver);
      });

      it('filter drivers table', () => {
        nvZonalRouting.driversTable.filter.searchText = ' ';

        expect(_.size(nvZonalRouting.filterDriversTable())).toEqual(1);
      });
    });
  });
}());
