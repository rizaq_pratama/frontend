(function() {
    'use strict';

    describe('globals.run', function() {

        var module = angular.mock.module;
        var inject = angular.mock.inject;

        var $rootScope,
            $state,
            nvClientStore,
            nvAuth;

        describe('with country domains', function() {

            beforeEach(function() {
                module('ngMaterial');
                module('nvOperatorApp.services');

                module('nvOperatorApp.runs', function($provide) {
                    // mock the nvAuth service for the test
                    $provide.value('nvAuth', {
                        init: jasmine.createSpy(), // called in init.run.js
                        logout: jasmine.createSpy(),
                        getDomains: jasmine.createSpy().and.returnValue(['sg', 'my'])
                    });
                });

                inject(function(_$rootScope_, _$state_, _nvClientStore_, _nvAuth_) {
                    $rootScope = _$rootScope_;
                    $state = _$state_;
                    nvClientStore = _nvClientStore_;
                    nvAuth = _nvAuth_;
                    spyOn($state, 'go');
                    spyOn(nvClientStore.localStorage, 'set');
                });
            });

            it('should set $rootScope.logout', function() {
                expect($rootScope.logout).toEqual(jasmine.any(Function));
            });

            describe('$rootScope.logout', function() {

                it('should call nvAuth.logout', function() {
                    $rootScope.logout();
                    expect(nvAuth.logout).toHaveBeenCalled();
                });

                it('should transition to the \'login\' state', function() {
                    $rootScope.logout();
                    nvAuth.logout.calls.first().args[0]();
                    expect($state.go).toHaveBeenCalledWith('login');
                });

                it('should set the env variable on localStorage', function() {
                    $rootScope.logout();
                    nvAuth.logout.calls.first().args[0]();
                    expect(nvClientStore.localStorage.set).toHaveBeenCalledWith('env', nv.config.env, {namespace: false});
                });

            });

            it('should set $rootScope.domains', function() {
                expect(nvAuth.getDomains).toHaveBeenCalled();
                expect($rootScope.domains).toEqual(['sg', 'my']);
            });

            it('should set $rootScope.domain to the first available domain', function() {
                expect($rootScope.domain).toEqual('sg');
            });

            it('should set $rootScope.countries to supported countries', function() {
                expect($rootScope.countries).toEqual(['sg', 'my', 'id', 'vn', 'ph', 'th', 'mm']);
            });

            it('should set $rootScope.ninjasaas to false', function() {
                expect($rootScope.ninjasaas).toBe(false);
            });

        });

        describe('with ninjasaas domains', function() {

            beforeEach(function() {
                module('ngMaterial');
                module('nvOperatorApp.services');

                module('nvOperatorApp.runs', function($provide) {
                    // mock the nvAuth service for the test
                    $provide.value('nvAuth', {
                        init: jasmine.createSpy(), // called in init.run.js
                        logout: jasmine.createSpy(),
                        getDomains: jasmine.createSpy().and.returnValue(['mbs'])
                    });
                });

                inject(function(_$rootScope_, _$state_, _nvClientStore_, _nvAuth_) {
                    $rootScope = _$rootScope_;
                    $state = _$state_;
                    nvClientStore = _nvClientStore_;
                    nvAuth = _nvAuth_;
                    spyOn($state, 'go');
                    spyOn(nvClientStore.localStorage, 'set');
                });
            });

            it('should set $rootScope.logout', function() {
                expect($rootScope.logout).toEqual(jasmine.any(Function));
            });

            describe('$rootScope.logout', function() {

                it('should call nvAuth.logout', function() {
                    $rootScope.logout();
                    expect(nvAuth.logout).toHaveBeenCalled();
                });

                it('should transition to the \'login\' state', function() {
                    $rootScope.logout();
                    nvAuth.logout.calls.first().args[0]();
                    expect($state.go).toHaveBeenCalledWith('login');
                });

                it('should set the env variable on localStorage', function() {
                    $rootScope.logout();
                    nvAuth.logout.calls.first().args[0]();
                    expect(nvClientStore.localStorage.set).toHaveBeenCalledWith('env', nv.config.env, {namespace: false});
                });

            });

            it('should set $rootScope.domains', function() {
                expect(nvAuth.getDomains).toHaveBeenCalled();
                expect($rootScope.domains).toEqual(['mbs']);
            });

            it('should set $rootScope.domain to the first available domain', function() {
                expect($rootScope.domain).toEqual('mbs');
            });

            it('should set $rootScope.countries to supported countries', function() {
                expect($rootScope.countries).toEqual(['sg', 'my', 'id', 'vn', 'ph', 'th', 'mm']);
            });

            it('should set $rootScope.ninjasaas to true', function() {
                expect($rootScope.ninjasaas).toBe(true);
            });

        });

    });

})();
