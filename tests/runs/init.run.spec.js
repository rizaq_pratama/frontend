(function test() {
  describe('init.run', () => {
    const module = angular.mock.module;
    const inject = angular.mock.inject;

    let $rootScope;
    let nvAuth;
    let nvTranslate;
    let nvClientStore;
    let nvDomain;
    let nvTimezone;

    describe('with undefined env', () => {
      beforeEach(() => {
        module('ngMaterial');
        module('nvOperatorApp.services');

        module('nvOperatorApp.runs', ($provide) => {
          // mock the nvAuth service for the test
          $provide.value('nvAuth', {
            init: jasmine.createSpy(),
            logout: jasmine.createSpy(),
            getDomains: jasmine.createSpy().and.returnValue(['sg']), // called in globals.run.js
          });

          // mock the nvTranslate service for the test
          $provide.value('nvTranslate', {
            init: jasmine.createSpy(),
          });

          $provide.value('nvTimezone', {
            init: jasmine.createSpy(),
          });

          // mock the nvClientStore service for the test
          $provide.value('nvClientStore', {
            localStorage: {
              get: jasmine.createSpy('get'),
              set: jasmine.createSpy('set'),
            },
          });
        });

        inject((_nvAuth_, _nvTranslate_, _nvClientStore_, _nvTimezone_, _nvDomain_) => {
          nvAuth = _nvAuth_;
          nvTranslate = _nvTranslate_;
          nvClientStore = _nvClientStore_;
          nvTimezone = _nvTimezone_;
          nvDomain = _nvDomain_;
        });
      });

      it('should initialize the nvAuth service', () => {
        expect(nvAuth.init).toHaveBeenCalled();
      });

      it('should initialize the nvTranslate service', () => {
        expect(nvTranslate.init).toHaveBeenCalled();
      });

      it('should initialize the nvTimezone service', () => {
        expect(nvTimezone.init).toHaveBeenCalled();
      });

      it('should set the env variable on localStorage', () => {
        expect(nvClientStore.localStorage.get).toHaveBeenCalledWith('env', { namespace: false });
        expect(nvClientStore.localStorage.set).toHaveBeenCalledWith('env', nv.config.env, { namespace: false });
      });
    });

    describe('with existing env', () => {
      beforeEach(() => {
        module('ngMaterial');
        module('nvOperatorApp.services');

        module('nvOperatorApp.runs', ($provide) => {
          // mock the nvAuth service for the test
          $provide.value('nvAuth', {
            init: jasmine.createSpy(),
            logout: jasmine.createSpy(), // called in globals.run.js
            getDomains: jasmine.createSpy().and.returnValue(['sg']), // called in globals.run.js
          });

          // mock the nvTranslate service for the test
          $provide.value('nvTranslate', {
            init: jasmine.createSpy(),
          });

          // mock the nvTranslate service for the test
          $provide.value('nvTimezone', {
            init: jasmine.createSpy(),
          });

          // mock the nvClientStore service for the test
          $provide.value('nvClientStore', {
            localStorage: {
              get: jasmine.createSpy('get').and.returnValue('prod'),
              set: jasmine.createSpy('set'),
            },
          });
        });

        inject((_$rootScope_, _nvAuth_, _nvTranslate_, _nvClientStore_, _nvTimezone_, _nvDomain_) => {
          $rootScope = _$rootScope_;
          nvAuth = _nvAuth_;
          nvTranslate = _nvTranslate_;
          nvClientStore = _nvClientStore_;
          nvTimezone = _nvTimezone_;
          nvDomain = _nvDomain_;

          // inject is called after the run blocks have been executed, so $rootScope.logout
          // has already been called once at this point; which means that in order to test
          // that the $rootScope.logout method will be called in this scenario, we need to
          // invoke the run block once more with the injected variables -- the side effect
          // is that this test has caused the run block to be called twice, but that doesn't
          // really matter because we are mocking all the services
          spyOn($rootScope, 'logout');
          angular.module('nvOperatorApp.runs')._runBlocks[1]($rootScope, nvAuth, nvTimezone, nvTranslate, nvClientStore, nvDomain);
        });
      });

      it('should call $rootScope.logout if the env variable on localStorage is different from the current env', () => {
        expect(nvClientStore.localStorage.get).toHaveBeenCalledWith('env', { namespace: false });
        expect($rootScope.logout).toHaveBeenCalled();
      });
    });
  });
}());
