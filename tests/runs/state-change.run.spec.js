(function() {
    'use strict';

    describe('state-change.run', function() {

        var module = angular.mock.module;
        var inject = angular.mock.inject;

        var ga,
            $rootScope,
            $state,
            $log,
            $window,
            $location,
            nvEnv,
            nvAuth;

        describe('with invalid user login', function() {

            beforeEach(function() {
                module('ngMaterial');
                module('nvOperatorApp.services');

                module('nvOperatorApp.runs', function($provide) {
                    $provide.value('nvAuth', {
                        init: jasmine.createSpy('init'), // called in init.run.js
                        logout: jasmine.createSpy('logout'), // called in globals.run.js
                        isLoggedIn: jasmine.createSpy('isLoggedIn').and.returnValue(false),
                        getDomains: jasmine.createSpy('getDomains').and.returnValue(['sg']) // called in globals.run.js
                    });
                    $provide.value('nvEnv', {
                        isLocal: jasmine.createSpy('isLocal').and.returnValue(false)
                    });
                });

                inject(function(_$rootScope_, _$state_, _nvAuth_) {
                    $rootScope = _$rootScope_;
                    $state = _$state_;
                    nvAuth = _nvAuth_;
                    spyOn($state, 'go');
                });
            });

            it('should call $state.go(\'login\') if the next state is not \'/login\'', function() {
                $rootScope.$broadcast('$stateChangeStart', {url: '/something'});
                expect(nvAuth.isLoggedIn).toHaveBeenCalled();
                expect($state.go).toHaveBeenCalledTimes(1);
                expect($state.go).toHaveBeenCalledWith('login');
            });

            it('should not call $state.go(\'login\') if the next state is \'/login\'', function() {
                $rootScope.$broadcast('$stateChangeStart', {url: '/login'});
                expect(nvAuth.isLoggedIn).toHaveBeenCalled();
                expect($state.go).not.toHaveBeenCalled();
            });

        });

        describe('with valid login', function() {

            beforeEach(function() {
                module('ngMaterial');
                module('nvOperatorApp.services');

                module('nvOperatorApp.runs', function($provide) {
                    $provide.value('nvAuth', {
                        init: jasmine.createSpy('init'), // called in init.run.js
                        logout: jasmine.createSpy('logout'), // called in globals.run.js
                        isLoggedIn: jasmine.createSpy('isLoggedIn').and.returnValue(true),
                        getDomains: jasmine.createSpy('getDomains').and.returnValue(['sg']) // called in globals.run.js
                    });
                    $provide.value('nvEnv', {
                        isLocal: jasmine.createSpy('isLocal').and.returnValue(false)
                    });
                });

                inject(function(_$rootScope_, _$state_, _nvAuth_) {
                    $rootScope = _$rootScope_;
                    $state = _$state_;
                    nvAuth = _nvAuth_;
                    spyOn($state, 'go');
                });
            });

            it('should not call $state.go(\'login\') if the next state is not \'/login\'', function() {
                $rootScope.$broadcast('$stateChangeStart', {url: '/something'});
                expect(nvAuth.isLoggedIn).toHaveBeenCalled();
                expect($state.go).not.toHaveBeenCalled();
            });

            it('should not call $state.go(\'login\') if the next state is \'/login\'', function() {
                $rootScope.$broadcast('$stateChangeStart', {url: '/login'});
                expect(nvAuth.isLoggedIn).toHaveBeenCalled();
                expect($state.go).not.toHaveBeenCalled();
            });

        });

        describe('with valid google analytics object', function() {

            beforeEach(function() {
                module('ngMaterial');
                module('nvOperatorApp.services');

                module('nvOperatorApp.runs', function($provide) {
                    $provide.value('nvAuth', {
                        init: jasmine.createSpy('init'), // called in init.run.js
                        logout: jasmine.createSpy('logout'), // called in globals.run.js
                        isLoggedIn: jasmine.createSpy('isLoggedIn'),
                        getDomains: jasmine.createSpy('getDomains').and.returnValue(['sg']) // called in globals.run.js
                    });
                    $provide.value('nvEnv', {
                        isLocal: jasmine.createSpy('isLocal').and.returnValue(false)
                    });
                });

                inject(function(_$rootScope_, _$state_, _$log_, _$window_, _$location_, _nvEnv_, _nvAuth_) {
                    $rootScope = _$rootScope_;
                    $state = _$state_;
                    $log = _$log_;
                    $window = _$window_;
                    $location = _$location_;
                    nvEnv = _nvEnv_;
                    nvAuth = _nvAuth_;

                    // inject is called after the run blocks have been executed, so $window.ga has already
                    // been called once at this point; which means that in order to test that the $window.ga
                    // method will be called in this scenario, we need to invoke the run block once more with the
                    // injected variables -- the side effect is that this test has caused the run block to be called
                    // twice, but that doesn't really matter because we are mocking all the services
                    ga = $window.ga;
                    $window.ga = jasmine.createSpy('ga');
                    angular.module('nvOperatorApp.runs')._runBlocks[2]($rootScope, $state, $log, $window, $location, nvEnv, nvAuth);
                });
            });

            afterEach(function() {
                $window.ga = ga;
            });

            it('should initialize google analytics', function() {
                expect($window.ga).toHaveBeenCalledWith('create', nv.config.gaTrackingId, 'auto');
            });

            it('should send pageview to google analytics when the state changes', function() {
                $rootScope.$broadcast('$stateChangeSuccess');
                expect($window.ga).toHaveBeenCalledWith('send', 'pageview', $location.path());
            });

        });

    });

})();
