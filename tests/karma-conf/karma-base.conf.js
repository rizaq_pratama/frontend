'use strict';

var _ = require('lodash');

var base = {};

base.mergeFn = mergeFn;

base.config = function(config) {
    return {
        // base path that will be used to resolve all patterns (eg. files, exclude)
        basePath: '../../',

        // testing frameworks to use
        // available frameworks: https://npmjs.org/browse/keyword/karma-adapter
        frameworks: [
            'jasmine'
        ],

        // list of files / patterns to load in the browser
        // these need to be LOADED IN THE PROPER ORDER !!
        files: [

            // config file
            'app/config.js',

            // vendor files - angular
            'app/lib/node_modules/angular/angular.min.js',
            'node_modules/angular-mocks/angular-mocks.js', // only used in testing
            'app/lib/node_modules/angular/angular.min.js',
            'app/lib/node_modules/angular-animate/angular-animate.min.js',
            'app/lib/node_modules/angular-aria/angular-aria.min.js',
            'app/lib/node_modules/angular-cookies/angular-cookies.min.js',
            'app/lib/node_modules/angular-messages/angular-messages.min.js',
            'app/lib/node_modules/angular-sanitize/angular-sanitize.min.js',
            'app/lib/node_modules/angular-material/angular-material.min.js',
            'app/lib/node_modules/angular-ui-router/release/angular-ui-router.min.js',
            'app/lib/node_modules/angular-translate/dist/angular-translate.min.js',
            'app/lib/node_modules/angular-translate-loader-static-files/angular-translate-loader-static-files.min.js',
            'app/lib/node_modules/ng-csv/build/ng-csv.min.js',
            'app/lib/node_modules/ng-table/dist/ng-table.min.js',
            'app/lib/node_modules/angular-scroll/angular-scroll.min.js',
            'app/lib/node_modules/angular-media-player/dist/angular-media-player.min.js',
            'app/lib/node_modules/ace-builds/src-min-noconflict/ace.js',
            'app/lib/node_modules/angular-ui-ace/src/ui-ace.js',
            'app/lib/node_modules/angular-promise-extras/angular-promise-extras.js',
            'app/lib/node_modules/babel-polyfill/dist/polyfill.js',

            // vendor files - mapbox
            'app/lib/node_modules/mapbox-gl/dist/mapbox-gl.js',
            'app/lib/node_modules/mapbox.js/dist/mapbox.js',
            'app/lib/node_modules/leaflet.label/dist/leaflet.label.js',
            'app/lib/ninja-commons/utils/leaflet/Icon.Label.js',
            'app/lib/ninja-commons/utils/leaflet/Icon.Label.Default.js',

            // vendor files - utilities
            'app/lib/node_modules/jquery/dist/jquery.min.js',
            'node_modules/jasmine-jquery/lib/jasmine-jquery.js', // only used in testing
            'app/lib/node_modules/lodash/lodash.min.js',
            'app/lib/node_modules/moment/min/moment.min.js',
            'app/lib/node_modules/moment-timezone/builds/moment-timezone-with-data.min.js',
            'app/lib/node_modules/toastr/build/toastr.min.js',
            'app/lib/node_modules/papaparse/papaparse.min.js',

            // vendor files - nv
            'app/lib/node_modules/@nv/angular-commons/dist/directives/nv-qrcode-directive.min.js',
            'app/lib/node_modules/@nv/angular-commons/dist/filters/nv-byte-filter.min.js',
            'app/lib/node_modules/@nv/angular-commons/dist/filters/nv-camel-case-2-hyphen-case-filter.min.js',
            'app/lib/node_modules/@nv/angular-commons/dist/filters/nv-camel-case-2-natural-case-filter.min.js',
            'app/lib/node_modules/@nv/angular-commons/dist/filters/nv-filter-by-year-filter.min.js',
            'app/lib/node_modules/@nv/angular-commons/dist/filters/nv-limit-to-filter.min.js',
            'app/lib/node_modules/@nv/angular-commons/dist/filters/nv-month-name-filter.min.js',
            'app/lib/node_modules/@nv/angular-commons/dist/filters/nv-natural-case-2-camel-case-filter.min.js',
            'app/lib/node_modules/@nv/angular-commons/dist/filters/nv-natural-case-2-hyphen-case-filter.min.js',
            'app/lib/node_modules/@nv/angular-commons/dist/filters/nv-smart-distance-filter.min.js',
            'app/lib/node_modules/@nv/angular-commons/dist/filters/nv-snake-case-2-natural-case-filter.min.js',
            'app/lib/node_modules/@nv/angular-commons/dist/services/nv-currency-service.min.js',
            'app/lib/node_modules/@nv/angular-commons/dist/services/nv-distance-service.min.js',
            'app/lib/node_modules/@nv/angular-commons/dist/services/nv-env-service.min.js',
            'app/lib/node_modules/@nv/angular-commons/dist/services/nv-json-service.min.js',
            'app/lib/node_modules/@nv/angular-commons/dist/services/nv-print-service.min.js',
            'app/lib/node_modules/@nv/angular-commons/dist/services/nv-url-service.min.js',

            // app files - init
            'app/nv.module.js',
            'app/app.module.js',
            'app/lib/ninja-commons/configs/configs.module.js',
            'app/lib/ninja-commons/configs/log.config.js',
            'app/lib/ninja-commons/configs/exception.config.js',
            'app/lib/ninja-commons/configs/currency.config.js',
            'app/configs/configs.module.js',
            'app/configs/init.config.js',
            'app/configs/routes.config.js',
            'app/configs/http-interceptor.config.js',
            'app/configs/theme.config.js',
            'app/configs/translate.config.js',
            'app/runs/runs.module.js',
            'app/runs/globals.run.js',
            'app/runs/init.run.js',
            'app/runs/state-change.run.js',
            'app/templates/templates.module.js',
            'app/lib/ninja-commons/runs/runs.module.js',
            'app/lib/ninja-commons/runs/http-upload.run.js',
            'app/lib/ninja-commons/runs/http-delete.run.js',
        ],

        // list of files to exclude
        exclude: [

        ],

        // pre-process matching files before serving them to the browser
        // available preprocessors: https://npmjs.org/browse/keyword/karma-preprocessor
        preprocessors: {
            'app/lib/ninja-commons/configs/**/*.js': ['babel', 'coverage'],
            'app/lib/ninja-commons/directives/**/*.js': ['babel', 'coverage'],
            'app/lib/ninja-commons/filters/**/*.js': ['babel', 'coverage'],
            'app/lib/ninja-commons/models/**/*.js': ['babel', 'coverage'],
            'app/lib/ninja-commons/runs/**/*.js': ['babel', 'coverage'],
            'app/lib/ninja-commons/services/**/*.js': ['babel', 'coverage'],
            'app/lib/ninja-commons/tests/**/*.js': ['babel'],
            'app/configs/**/*.js': ['babel', 'coverage'],
            'app/directives/**/*.js': ['babel', 'coverage'],
            'app/runs/**/*.js': ['babel', 'coverage'],
            'app/services/**/*.js': ['babel', 'coverage'],
            'app/templates/**/*.js': ['babel', 'coverage'],
            'app/views/**/*.js': ['babel', 'coverage'],
            'tests/**/*.js': ['babel'],
            'app/views/**/*.html': ['ng-html2js'],
            'app/directives/**/*.html': ['ng-html2js'],
            'app/lib/ninja-commons/directives/**/*.html': ['ng-html2js'],
            'app/lib/ninja-commons/services/**/*.html': ['ng-html2js'],
            'app/lib/ninja-commons/templates/**/*.html': ['ng-html2js'],
        },

        // test results reporter to use
        // possible values: 'dots', 'progress'
        // available reporters: https://npmjs.org/browse/keyword/karma-reporter
        reporters: [
            //'progress',
            'mocha',
            'coverage', // use code coverage with Istanbul
            'junit'
        ],

        // web server port
        port: 9876,

        // enable / disable colors in the output (reporters and logs)
        colors: true,

        // level of logging
        // possible values: config.LOG_DISABLE || config.LOG_ERROR || config.LOG_WARN || config.LOG_INFO || config.LOG_DEBUG
        logLevel: config.LOG_INFO,

        // enable / disable watching file and executing tests whenever any file changes
        autoWatch: true,

        // start these browsers
        // available browser launchers: https://npmjs.org/browse/keyword/karma-launcher
        // uncomment these browsers to launch them on your local machine
        browsers: [
            //'Chrome',
            //'Firefox',
            //'Safari',
            //'IE',
            'PhantomJS'
        ],

        // continuous integration mode
        // if true, Karma captures browsers, runs the tests and exits
        singleRun: true,

        // concurrency level
        // how many browser should be started simultaneously
        concurrency: Infinity,

        // so that we don't get false failures on bamboo
        browserDisconnectTimeout: 30000,
        browserDisconnectTolerance: 3,
        browserNoActivityTimeout: 30000,

        // configures the Istanbul code coverage reporter
        // available options: https://github.com/karma-runner/karma-coverage/blob/master/docs/configuration.md
        coverageReporter: {
            reporters: [
                {
                    type: 'html',
                    dir: 'reports/coverage/',
                    subdir: '.'
                },
                {
                    type: 'clover',
                    dir: 'reports/coverage/',
                    subdir: '.',
                    file: 'clover.xml'
                }
            ]
        },

        // configures the junit code coverage reporter required by bamboo to display the test results
        junitReporter: {
            outputDir: 'reports/coverage/',
            outputFile: 'junit.xml',
            useBrowserName: false
        },

        ngHtml2JsPreprocessor: {
            stripPrefix: 'app/',
            moduleName: 'nvCommons.templates'
        },

        babelPreprocessor: {
            options: {
                sourceMap: true,
                presets: ['babel-preset-env'],
            },
        }
    };
};

module.exports = base;

function mergeFn(objVal, srcVal){
    if(_.isArray(objVal)) {
        return _.union(objVal, srcVal);
    }
}