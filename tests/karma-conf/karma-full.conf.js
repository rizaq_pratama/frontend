'use strict';

module.exports = function(config) {
    var _ = require('lodash');
    var base = require('./karma-base.conf.js');
    var appConfig = require('./karma-app.conf.js');
    var directiveConfig = require('./karma-directive.conf.js');
    var controllerConfig = require('./karma-controller.conf.js');

    config.set(_.mergeWith(
        base.config(config),
        appConfig.config,
        directiveConfig.config,
        controllerConfig.config,
        base.mergeFn
    ));
};
