'use strict';

var _config = {
  basePath: '../../',

  files: [
    // app files - controllers
    'app/views/controllers.module.js',

    'app/views/container/route-inbound/route-inbound.js',
    'app/views/container/route-inbound/inbounding/inbounding.js',
    'app/views/container/route-inbound/result/result.js',

    // test files - controllers
    'tests/views/**/*.js',
  ],

  exclude: [
    'app/index.html'
  ],
};

var karmaConfig = function(config) {
  var _ = require('lodash');
  var base = require('./karma-base.conf.js');

  config.set(_.mergeWith(base.config(config), _config, base.mergeFn));
};
karmaConfig.config = _config;

module.exports = karmaConfig;
