'use strict';

var _config = {
    basePath: '../../',

    files: [
        // app files - services
        'app/lib/ninja-commons/services/services.module.js',
        'app/lib/ninja-commons/services/dom-manipulation.service.js',
        'app/lib/ninja-commons/services/ga.service.js',

        // app files - directives
        'app/lib/ninja-commons/directives/directives.module.js',
        'app/lib/ninja-commons/directives/buttons/icon-button/icon-button.directive.js',
        'app/lib/ninja-commons/directives/buttons/icon-text-button/icon-text-button.directive.js',
        'app/lib/ninja-commons/directives/tables/nv-table/nv-table.directive.js',

        // app files - style
        '.tmp/styles/vendor.css',
        '.tmp/styles/app.css',

        // app files - html
        'app/lib/ninja-commons/directives/**/*.html',

        // test files - directives
        'app/lib/ninja-commons/tests/directives/**/*.js',
    ],

    exclude: [
        'app/index.html'
    ],
};

var karmaConfig = function(config) {
    var _ = require('lodash');
    var base = require('./karma-base.conf.js');

    config.set(_.mergeWith(base.config(config), _config, base.mergeFn));
};
karmaConfig.config = _config;

module.exports = karmaConfig;
