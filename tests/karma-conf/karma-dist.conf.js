'use strict';

module.exports = function(config) {

    /**
     * This configuration is for testing the minified, distribution-ready code.
     * The dist folder must exist to run the tests (run 'grunt build').
     */
    config.set({

        // base path that will be used to resolve all patterns (eg. files, exclude)
        basePath: '../../',

        // testing frameworks to use
        // available frameworks: https://npmjs.org/browse/keyword/karma-adapter
        frameworks: [
            'jasmine'
        ],

        // list of files / patterns to load in the browser
        // these need to be loaded in proper order
        files: [
            'dist/config.*.js',
            'dist/scripts/**/*.js',
            'node_modules/angular-mocks/angular-mocks.js', // only used in testing
            'app/lib/ninja-commons/tests/**/*.js',
            'tests/**/*.js'
        ],

        // list of files to exclude
        exclude: [

        ],

        // pre-process matching files before serving them to the browser
        // available preprocessors: https://npmjs.org/browse/keyword/karma-preprocessor
        preprocessors: {

        },

        // test results reporter to use
        // possible values: 'dots', 'progress'
        // available reporters: https://npmjs.org/browse/keyword/karma-reporter
        reporters: [
            'progress'
        ],

        // web server port
        port: 9876,

        // enable / disable colors in the output (reporters and logs)
        colors: true,

        // level of logging
        // possible values: config.LOG_DISABLE || config.LOG_ERROR || config.LOG_WARN || config.LOG_INFO || config.LOG_DEBUG
        logLevel: config.LOG_INFO,

        // enable / disable watching file and executing tests whenever any file changes
        autoWatch: true,

        // start these browsers
        // available browser launchers: https://npmjs.org/browse/keyword/karma-launcher
        // uncomment these browsers to launch them on your local machine
        browsers: [
            //'Chrome',
            //'Firefox',
            //'Safari',
            //'IE',
            'PhantomJS'
        ],

        // continuous integration mode
        // if true, Karma captures browsers, runs the tests and exits
        singleRun: true,

        // concurrency level
        // how many browser should be started simultaneously
        concurrency: Infinity,

        // so that we don't get false failures on bamboo
        browserDisconnectTimeout: 15000,
        browserDisconnectTolerance: 3,
        browserNoActivityTimeout: 20000

    });

};
