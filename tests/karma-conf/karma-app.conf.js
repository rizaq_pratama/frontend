const _ = require('lodash');
const base = require('./karma-base.conf.js');

const appConfig = {
  basePath: '../../',

  files: [
    // app files - filters
    'app/lib/ninja-commons/filters/filters.module.js',
    'app/lib/ninja-commons/filters/date/time.filter.js',
    'app/lib/ninja-commons/filters/highlight.filter.js',

    // app files - directives
    'app/lib/ninja-commons/directives/directives.module.js',
    'app/lib/ninja-commons/directives/filter-box/filter-number-box/filter-number-box.service.js',

    // app files - services
    'app/services/services.module.js',
    'app/services/zonalRouting.service.js',
    'app/lib/ninja-commons/services/services.module.js',
    'app/lib/ninja-commons/services/address.service.js',
    'app/lib/ninja-commons/services/auth.service.js',
    'app/lib/ninja-commons/services/client-store.service.js',
    'app/lib/ninja-commons/services/dialogs/dialog.service.js',
    'app/lib/ninja-commons/services/dom-manipulation.service.js',
    'app/lib/ninja-commons/services/domain.service.js',
    'app/lib/ninja-commons/services/delegate-handle.service.js',
    'app/lib/ninja-commons/services/excel.service.js',
    'app/lib/ninja-commons/services/listener.service.js',
    'app/lib/ninja-commons/services/migrationCoreV2.service.js',
    'app/lib/ninja-commons/services/nav-guard.service.js',
    'app/lib/ninja-commons/services/pdf.service.js',
    'app/lib/ninja-commons/services/maps/mapbox/mapbox.service.js',
    'app/lib/ninja-commons/services/maps/maps.service.js',
    'app/lib/ninja-commons/services/table.service.js',
    'app/lib/ninja-commons/services/timezone.service.js',
    'app/lib/ninja-commons/services/toast/toast.service.js',
    'app/lib/ninja-commons/services/translate.service.js',
    'app/lib/ninja-commons/services/translate-loader.service.js',
    'app/lib/ninja-commons/services/utils/backend-filter-utils.service.js',
    'app/lib/ninja-commons/services/utils/coupled-list-utils.service.js',
    'app/lib/ninja-commons/services/utils/date-time-utils.service.js',
    'app/lib/ninja-commons/services/utils/extend-utils.service.js',
    'app/lib/ninja-commons/services/utils/file-utils.service.js',
    'app/lib/ninja-commons/services/utils/model-utils.service.js',
    'app/lib/ninja-commons/services/utils/request-utils/request-utils.service.js',
    'app/lib/ninja-commons/services/utils/utils.service.js',
    'app/lib/ninja-commons/services/sound.service.js',

    // app files - models
    'app/lib/ninja-commons/models/models.module.js',
    'app/lib/ninja-commons/models/addressing/addressing.model.js',
    'app/lib/ninja-commons/models/address.model.js',
    'app/lib/ninja-commons/models/cod.model.js',
    'app/lib/ninja-commons/models/driver.model.js',
    'app/lib/ninja-commons/models/hub/hub.model.js',
    'app/lib/ninja-commons/models/inbound/inbound.model.js',
    'app/lib/ninja-commons/models/orders/order.model.js',
    'app/lib/ninja-commons/models/recovery/route-cleaning-report.model.js',
    'app/lib/ninja-commons/models/route/route.model.js',
    'app/lib/ninja-commons/models/route/tag.model.js',
    'app/lib/ninja-commons/models/route/zone.model.js',
    'app/lib/ninja-commons/models/shipper/shippers.model.js',
    'app/lib/ninja-commons/models/shipper/industry.model.js',
    'app/lib/ninja-commons/models/shipper/reservations.model.js',
    'app/lib/ninja-commons/models/third-party/third-party-order.model.js',
    'app/lib/ninja-commons/models/transaction/transaction.model.js',
    'app/lib/ninja-commons/models/addressing/unverified-address.model.js',
    'app/lib/ninja-commons/models/warehousing.model.js',
    'app/lib/ninja-commons/models/waypoint/timewindow.model.js',
    'app/lib/ninja-commons/models/waypoint/waypoint.model.js',

    // test files
    { pattern: 'tests/mock/*.json', watched: true, served: true, included: false },
    'tests/configs/**/*.js',
    'tests/runs/**/*.js',
    'tests/services/**/*.js',
    'app/lib/ninja-commons/tests/configs/**/*.js',
    'app/lib/ninja-commons/tests/filters/**/*.js',
    'app/lib/ninja-commons/tests/models/**/*.js',
    'app/lib/ninja-commons/tests/runs/**/*.js',
    'app/lib/ninja-commons/tests/services/**/*.js',
  ],
};

const karmaConfig = (config) => {
  config.set(_.mergeWith(base.config(config), appConfig, base.mergeFn));
};
karmaConfig.config = appConfig;

module.exports = karmaConfig;
